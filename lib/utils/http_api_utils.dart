import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/advertise/pp_distinct_entity_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup/chambers_signup_body.dart';
import 'package:pocketpills/core/request/telehealth/chambers_active_body.dart';
import 'package:pocketpills/core/response/advertise/pp_distinct_entity_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/chambers/chambers_activate_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/referral/contact_response.dart';
import 'package:pocketpills/core/response/referral/item_response.dart';
import 'package:pocketpills/core/response/referral/postal_address_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/response/transfer/ip_location_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/string_utils.dart';

import 'chambers/chambers_redirect.dart';
import 'navigation_service.dart';

class HttpApiUtils {
  final HttpApi _api = locator<HttpApi>();
  final LoginService loginService = locator<LoginService>();
  final DataStoreService dataStore = locator<DataStoreService>();

  Future<Location?> getLocationUsingIp() async {
    try {
      BaseResponse<IpLocationResponse>? baseResponse = await getGeoIp();
      if (baseResponse!.response!.response!.location == null) {
        return null;
      }
      Location location = new Location(lat: baseResponse.response!.response!.location!.latitude, lng: baseResponse.response!.response!.location!.longitude);
      return location;
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  Future<List<String>?> getProvinceAndPostalCode() async {
    List<String>? loc = [];
    try {
      BaseResponse<IpLocationResponse>? baseResponse = await getGeoIp();
      if (baseResponse!.response!.response!.subdivisions == null ||
          baseResponse.response!.response!.subdivisions.length == 0 ||
          baseResponse.response!.response!.postal == null) {
        return null;
      }
      String name = baseResponse.response!.response!.subdivisions[0]!.names!.en;
      String pincode = baseResponse.response!.response!.postal!.code!;
      loc.addAll([name, pincode]);
      return loc;
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  Future<String?> getSubdivisionName() async {
    try {
      BaseResponse<IpLocationResponse>? baseResponse = await getGeoIp();
      if (baseResponse!.response!.response!.subdivisions == null || baseResponse.response!.response!.subdivisions.length == 0) {
        return null;
      }
      String name = baseResponse.response!.response!.subdivisions[0]!.names!.en;
      String city = baseResponse.response!.response!.city!.names!.en ?? "";
      PPApplication.province = name;
      PPApplication.city = city;
      if (dataStore.readString(DataStoreService.PROVINCE) == null && name.toLowerCase() == "quebec") {
        await dataStore.writeBoolean(DataStoreService.QUEBEC, true);
        PPApplication.quebecCheck = true;
      } else if (dataStore.readString(DataStoreService.PROVINCE) != null &&
          dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase() == "quebec") {
        await dataStore.writeBoolean(DataStoreService.QUEBEC, true);
        PPApplication.quebecCheck = true;
      } else {
        await dataStore.writeBoolean(DataStoreService.QUEBEC, false);
        PPApplication.quebecCheck = false;
      }
      await dataStore.writeString(DataStoreService.GEO_IP_PROVINCE, name);
      await dataStore.writeString(DataStoreService.GEO_IP_CITY, city);
      if (baseResponse.response!.response!.traits!.ip != null) {
        PPApplication.ip = baseResponse.response!.response!.traits!.ip;
        await dataStore.writeString(DataStoreService.IP, PPApplication.ip!);
      } else {
        PPApplication.ip = '';
        await dataStore.writeString(DataStoreService.IP, PPApplication.ip!);
      }
      if (baseResponse.response!.response!.postal!.code != null) {
        PPApplication.value = baseResponse.response!.response!.postal!.code;
        await dataStore.writeString(DataStoreService.USER_POSTAL_CODE, PPApplication.value!);
      } else {
        PPApplication.value = '';
        await dataStore.writeString(DataStoreService.USER_POSTAL_CODE, PPApplication.value!);
      }
      return name;
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    if (response != null) {
      BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
      if (!res.status!) {
        return null;
      }
      List<TelehealthProvinceItem> listItems = [];
      for(int i =0 ; i< res.response!.items.length;i++){
        if(res.response!.items[i].defaultFlowEnabled == true){
          listItems.add(res.response!.items[i]);
        }
      }
      return listItems;
    } else {
      return null;
    }
  }

  Future<BaseResponse<IpLocationResponse>?> getGeoIp() async {
    Response response = await _api.getLocationUsingIp();
    if (response != null) {
      BaseResponse<IpLocationResponse> baseResponse = BaseResponse<IpLocationResponse>.fromJson(response.data);
      if (!baseResponse.status! || baseResponse.response == null || baseResponse.response!.response == null) {
        return null;
      }
      return baseResponse;
    } else {
      return null;
    }
  }

  Future<void> sendAdvertiseParams() async {
    final DataStoreService dataStore = locator<DataStoreService>();
    final Analytics analytics = locator<Analytics>();
    String mixPanelDistinctID = await analytics.getMixpanelDistinctId();
    PPDistinctEntityRequest? request = getPPDistinctEntityRequestParam(dataStore, mixPanelDistinctID);
    Response? response = await _api.sendAdvertiseParams(RequestWrapper(body: request));
    if (response != null) {
      setPPDistinctId(response, dataStore);
    }
  }

  Future<void> updateAdvertiseParams() async {
    final DataStoreService dataStore = locator<DataStoreService>();
    final Analytics analytics = locator<Analytics>();
    String mixPanelDistinctID = await analytics.getMixpanelDistinctId();
    PPDistinctEntityRequest request = getPPDistinctEntityRequestParam(dataStore, mixPanelDistinctID);
    Response response = await _api.updateAdvertiseParams(RequestWrapper(body: request), dataStore.readString(DataStoreService.PP_DISTINCT_ID)!);
    if (response != null) {
      setPPDistinctId(response, dataStore);
    }
  }

  Future<void> chambersActivate(String group, String token, String? userIdentifier) async {
    print('AMANBODY:$token ');
    dataStore.writeBoolean(DataStoreService.CHAMBERS_FLOW, true);
    ChambersRequest request = ChambersRequest(token: token, group: group, userIdentifier: userIdentifier);
    Response response = await _api.ChambersActivate(RequestWrapper(body: request));
    if (response != null) {
      BaseResponse<ChambersResponse> baseResponse = BaseResponse<ChambersResponse>.fromJson(response.data);
      if (baseResponse.status == true) {
        if (null != baseResponse.response!.userId || "SIGNUP" == baseResponse.response!.redirect) {
          await dataStore.writeInt(DataStoreService.USERID, baseResponse.response!.userId!);
          try {
            await dataStore.saveAuthorizationToken(response.headers['Authorization']![0]);
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
          ChambersSignupRequest signupRequest =
              ChambersSignupRequest(employerLandingPageRef: "CHAMBERS", userIdentifier: baseResponse.response!.userIdentifier);

          await getChambersSignupRequest(signupRequest);
          dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, true);
          print('AMANROUTEREDIRECT:${ChambersRedirect.routeName()}');
          if (!ChambersRedirect.routeName()!.contains('dashboard')) {
            locator<NavigationService>().pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, chambersFlow: true, source: BaseStepperSource.NEW_USER);
          } else {
            locator<NavigationService>().pushNamedAndRemoveUntil(ChambersRedirect.routeName()!);
          }
        } else if (baseResponse.response!.redirect == ('ACTIVATE_ASK_USER_IDENTIFIER')) {
          locator<NavigationService>().pushNamedAndRemoveUntil(StartView.routeName, carouselIndex: 0, chambersFlow: true);
        }
      }
    }
  }

  Future<bool> getChambersSignupRequest(ChambersSignupRequest signupRequest) async {
    var response = await _api.updateUserInfo(RequestWrapper(body: signupRequest));

    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        return false;
      }
      return await loginService.registerUser(response);
    } else {
      return false;
    }
  }

  void setPPDistinctId(Response response, DataStoreService dataStore) {
    BaseResponse<PPDistinctEntityResponse> baseResponse = BaseResponse<PPDistinctEntityResponse>.fromJson(response.data);
    if (baseResponse.status == true && baseResponse.response!.id != null) {
      dataStore.writeString(DataStoreService.PP_DISTINCT_ID, baseResponse.response!.id);
    }
  }

  PPDistinctEntityRequest getPPDistinctEntityRequestParam(DataStoreService dataStore, String mixPanelDistinctID) {
    PPDistinctEntityRequest request = PPDistinctEntityRequest(
        advertisingId: dataStore.readString(DataStoreService.ADVERTISING_ID),
        utmCampaign: dataStore.readString(DataStoreService.UTM_CAMPAIGN),
        utmContent: dataStore.readString(DataStoreService.UTM_CONTENT),
        utmMedium: dataStore.readString(DataStoreService.UTM_MEDIUM),
        utmTerm: dataStore.readString(DataStoreService.UTM_TERM),
        utmSource: dataStore.readString(DataStoreService.UTM_SOURCE),
        idfa: dataStore.readString(DataStoreService.IDFA_ID),
        mixpanelDistinctId: mixPanelDistinctID);
    return request;
  }

  Future<void> postUserContact(var contactList) async {
    List<ContactResponse> contactListResponse = [];

    contactList.forEach((contact) {
      List<ItemResponse> phoneItemResponse = [];
      List<ItemResponse> emailItemResponse = [];
      List<PostalAddressResponse> postalItemResponse = [];
      contact.phones.forEach((value) {
        phoneItemResponse.add(ItemResponse(label: value.label, value: value.value));
      });

      contact.postalAddresses.forEach((value) {
        postalItemResponse
            .add(PostalAddressResponse(label: value.label, city: value.city, postcode: value.postcode, region: value.region, country: value.country));
      });

      contact.emails.forEach((value) {
        phoneItemResponse.add(ItemResponse(label: value.label, value: value.value));
      });

      ContactResponse contactResponse = ContactResponse.fromJson(contact.toMap());
      contactResponse.phones = phoneItemResponse;
      contactResponse.emails = emailItemResponse;
      contactResponse.postalAddresses = postalItemResponse;
      contactListResponse.add(contactResponse);
    });

    ClientDetails clientDetails = ClientDetails();
    Map<String, dynamic> toJson() {
      final Map<String, dynamic> jsonMap = Map();
      jsonMap['id'] = StringUtils.generateRandomRequestId();
      jsonMap['body'] = contactListResponse;
      jsonMap['clientDetails'] = clientDetails.toJson();
      return jsonMap;
    }

    await _api.postUserContact(toJson());
  }
}
