import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/core/viewmodels/order_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_health_model.dart';
import 'package:pocketpills/core/viewmodels/signup/sign_up_stepper_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/verification_model.dart';
import 'package:pocketpills/core/viewmodels/stepper_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_stepper_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins_model.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = [
  ...independentServices,
  ...dependentServices,
  ...uiConsumableProviders,
];

List<SingleChildWidget> independentServices = [Provider.value(value: NavigationService()), Provider.value(value: HttpApiUtils())];

List<SingleChildWidget> dependentServices = [];

List<SingleChildWidget> uiConsumableProviders = [
  ChangeNotifierProvider<DashboardModel>(create: (_) => DashboardModel()),
  ChangeNotifierProvider<PillReminderDayModel>(create: (_) => PillReminderDayModel()),
  ChangeNotifierProvider<VerificationModel>(create: (_) => VerificationModel()),
  ChangeNotifierProvider<HomeModel>(create: (_) => HomeModel()),
  ChangeNotifierProvider<MedicationsModel>(create: (_) => MedicationsModel()),
  ChangeNotifierProvider<PrescriptionModel>(create: (_) => PrescriptionModel()),
  ChangeNotifierProvider<OrderModel>(create: (_) => OrderModel()),
  ChangeNotifierProvider<VitaminsModel>(create: (_) => VitaminsModel()),
  ChangeNotifierProvider<VitaminsCatalogModel>(create: (_) => VitaminsCatalogModel()),
  ChangeNotifierProvider<VitaminsSubscriptionModel>(create: (_) => VitaminsSubscriptionModel()),
  ChangeNotifierProvider<StepperModel>(create: (_) => StepperModel()),
  ChangeNotifierProvider<VitaminsStepperModel>(create: (_) => VitaminsStepperModel()),
  ChangeNotifierProvider<SignupStepperModel>(create: (_) => SignupStepperModel()),
  ChangeNotifierProvider<ProfileHealthModel>(create: (_) => ProfileHealthModel()),
  ChangeNotifierProvider<SignUpModel>(create: (_) => SignUpModel()),
  ChangeNotifierProvider<AppointmentDashboardModel>(create: (_) => AppointmentDashboardModel()),
];
