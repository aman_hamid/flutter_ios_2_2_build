import 'dart:async';
import 'package:flutter/services.dart';

const MethodChannel _channel =
    MethodChannel('plugins.flutter.io/facebook_analytics');

class FacebookAnalytics {

  Future<void> initialize() async {
    await _channel.invokeMethod<void>('initialize');
  }

  Future<void> logEvent(String name, Map<String, dynamic> parameters) async {
    await _channel.invokeMethod<void>('logEvent', <String, dynamic>{
      'name': name,
      'parameters': parameters,
    });
  }

  Future<void> setUserData(Map<String, dynamic> parameters) async {
    await _channel.invokeMethod<void>('userData', <String, dynamic>{
      'userData': parameters,
    });
  }

  Future<void> logCompleteRegistrationEvent() async {
    await _channel.invokeMethod<void>('completeRegistration');
  }

  Future<void> setUserIdentifier(Map<String, dynamic> parameters) async {
    await _channel.invokeListMethod("identifier", <String, dynamic>{
      'identifier': parameters,
    });
  }

  Future<void> logInitiatedEvent(Map<String, dynamic> parameters) async {
    await _channel.invokeMethod<void>('initiatedEvent', <String, dynamic>{
      'initiatedEvent': parameters,
    });
  }
}
