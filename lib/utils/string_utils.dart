import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:intl/intl.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:uuid/uuid.dart';

class StringUtils {
  static String getFormattedPhoneNumber(String? number) {
    if (number != null) {
      String formattedNumber =
          number.replaceAll("+91", "").replaceAll("+1", "").replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      return formattedNumber;
    } else {
      return "";
    }
  }

  static String referralShareMessage(String referralCode) {
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      return "Hey, I am using Pocketpills to fill my prescription online and it is super easy! Visit: https://pocketpills.app.link/yyXOnrr200?referralCode=$referralCode Sign up and use my referral code to get a \$10 Amazon Gift Card. T&C apply";
    } else {
      return "Hé, j'utilise Pocketpills pour remplir ma prescription en ligne et c'est super facile! Visitez https://pocketpills.app.link/yyXOnrr200?referralCode=$referralCode inscrivez-vous et utilisez mon code de parrainage pour obtenir une carte-cadeau Amazon de 10 \$. Les CGV s’appliquent.";
    }
  }

  static String? getSubscriptionDate(String date) {
    try {
      var parsedDate = DateTime.parse(date);
      String datetime = new DateFormat.yMMMd().format(parsedDate);
      return datetime;
    } catch (ex) {
      return null;
    }
  }

  static String generateRandomRequestId() {
    var uuid = new Uuid();
    return uuid.v4();
  }

  static bool isNumericUsingRegularExpression(String string) {
    final numericRegex = RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$');
    return numericRegex.hasMatch(string);
  }

  static bool isEmail(String email) {
    String emailValidation =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(emailValidation);
    return regExp.hasMatch(email);
  }

  static String capitalize(String s) {
    return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
  }

  static String capitalizeFirstOfEach(String s) {
    return s.split(" ").map((str) => capitalize(str)).join(" ");
  }

  static String getFormattedTimeHHMMNN(String time) {
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      return DateFormat.jm().format(DateFormat("HH:mm:ss").parse(time));
    } else {
      return DateFormat.Hm().format(DateFormat("HH:mm:ss").parse(time)).replaceAll(":", "") + " He";
    }
  }

  static String getFormattedDateEEEEddMMMM(String date) {
    try {
      if (getSelectedLanguage() == ViewConstants.languageIdEn) {
        return DateFormat('MMMM dd, EEEE', "en").format(DateTime.parse(date.split('/').reversed.join()));
      } else {
        return DateFormat('MMMM dd, EEEE', "fr").format(DateTime.parse(date.split('/').reversed.join()));
      }
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
      return "";
    }
  }
}

String convertToTitleCase(String text) {
  if (text == null) {
    return "";
  }

  if (text.length <= 1) {
    return text.toUpperCase();
  }

  // Split string into multiple words
  final List<String> words = text.split(' ');

  // Capitalize first letter of each words
  final capitalizedWords = words.map((word) {
    if (word.trim().isNotEmpty) {
      final String firstLetter = word.trim().substring(0, 1).toUpperCase();
      final String remainingLetters = word.trim().substring(1).toLowerCase();

      return '$firstLetter$remainingLetters';
    }
    return '';
  });

  // Join/Merge all words back to one String
  return capitalizedWords.join(' ');
}

extension CapitalizedStringExtension on String {
  String toTitleCase() {
    return convertToTitleCase(this);
  }
}
