import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';

class ToastWidget extends StatelessWidget {
  String? msg;
  bool error = false;

  ToastWidget({this.msg, this.error = false});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0.0, -0.74),
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        decoration: !error
            ? BoxDecoration(color: Colors.green.shade400, shape: BoxShape.rectangle, borderRadius: BorderRadius.circular(10.0), border: Border.all(color: Colors.green))
            : BoxDecoration(color: Colors.red.shade400, shape: BoxShape.rectangle, borderRadius: BorderRadius.circular(10.0), border: Border.all(color: Colors.red)),
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Text(
            msg!,
            textAlign: TextAlign.center,
            style: TextStyle(color: headerColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium", decoration: TextDecoration.none),
          ),
        ),
      ),
    );
  }
}
