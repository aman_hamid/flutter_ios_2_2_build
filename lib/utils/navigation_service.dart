import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/start/startview_arguments.dart';
import 'analytics.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  final Analytics analyticsEvents = locator<Analytics>();

  Future<dynamic> navigateTo(String routeName, Object argu) {
    return navigatorKey.currentState!.pushNamed(routeName, arguments: argu);
  }

  Future<dynamic> pushNamed(String routeName, {Object? argument, String? viewPushEvent, String? viewPopEvent}) async {
    if (viewPushEvent != null) analyticsEvents.sendAnalyticsEvent(viewPushEvent);
    await navigatorKey.currentState!.pushNamed(routeName, arguments: argument);
    if (viewPopEvent != null) analyticsEvents.sendAnalyticsEvent(viewPopEvent);
  }

  Future<dynamic> pushNamedAndRemoveUntil(String routeName, {String? deepLinkRouteName, int? carouselIndex, bool? chambersFlow, BaseStepperSource? source}) {
    if (!routeName.contains('start')) {
      if (routeName.contains('dashboardview')) {
        return navigatorKey.currentState!.pushNamedAndRemoveUntil(
          routeName,
          (Route<dynamic> route) => false,
        );
      } else if (routeName.contains('signup')) {
        return navigatorKey.currentState!
            .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: source, chambersFlow: chambersFlow));
      } else if (routeName.contains('transfer')) {
        return navigatorKey.currentState!
            .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false, arguments: TransferArguments(source: source, chambersFlow: chambersFlow));
      } else if (routeName.contains('signUpAlmostDone')) {
        return navigatorKey.currentState!
            .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: source, chambersFlow: chambersFlow));
      } else if (routeName.contains('logout')) {
        final DataStoreService dataStore = locator<DataStoreService>();
        dataStore.logOut();
        return navigatorKey.currentState!.pushNamedAndRemoveUntil(SplashView.routeName, (Route<dynamic> route) => false);
      }
    }
    return navigatorKey.currentState!.pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false,
        arguments: new StartViewArguments(deepLinkRouteName: deepLinkRouteName, carouselIndex: carouselIndex, chambersFlow: chambersFlow));
  }
}
