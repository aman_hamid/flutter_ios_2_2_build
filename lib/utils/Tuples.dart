import 'dart:core';

class ResultHandle<T1, T2> {
  final T1 status;
  final T2 message;

  ResultHandle({
    required this.status,
    required this.message,
  });

  factory ResultHandle.fromJson(Map<String, dynamic> json) {
    return ResultHandle(
      status: json['status'],
      message: json['message'],
    );
  }
}