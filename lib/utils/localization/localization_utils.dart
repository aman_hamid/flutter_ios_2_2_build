import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/response/telehealth/localization_update_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/main.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class LocalizationUtils {
  static final dataStore = locator<DataStoreService>();

  static fetchLocale() {
    Locale _appLocale =
        Locale(dataStore.readString(DataStoreService.LANGUAGE)! == null ? ViewConstants.languageId : dataStore.readString(DataStoreService.LANGUAGE)!);
    return _appLocale;
  }

  static String? getLocalizedStringPreference() {
    return dataStore.readString(DataStoreService.LANGUAGE_KEYS);
  }

  static Map<String, dynamic>? getPageWiseData(String page) {
    if (getLocalizedStringPreference() == null) {
      return null;
    } else {
      Map<String, dynamic> data = json.decode(getLocalizedStringPreference()!);
      if (data.containsKey(page)) {
        return data[page];
      } else {
        return null;
      }
    }
  }

  static bool isProvinceQuebec() {
    if (dataStore.readBoolean(DataStoreService.QUEBEC) != null && dataStore.readBoolean(DataStoreService.QUEBEC) == true) {
      return true;
    } else if (PPApplication.quebecCheck == true) {
      return true;
    } else {
      return false;
    }
  }

  static List<String>? getPageWiseDataList(List<String> page) {
    if (getLocalizedStringPreference() == null) {
      return page;
    } else {
      Map<String, dynamic> data = json.decode(getLocalizedStringPreference()!);
      List<String> notAvailableKey = [];
      page.forEach((element) {
        if (!data.containsKey(element)) {
          notAvailableKey.add(element);
        }
      });
      return notAvailableKey.isEmpty ? null : notAvailableKey;
    }
  }

  static Map<String, dynamic>? saveNewPageData2(List<String> page, Map<dynamic, dynamic>? newData) {
    Map<String, dynamic>? data;
    page.forEach((pageName) {
      if (getLocalizedStringPreference() == null) {
        data = Map<String, dynamic>();
        Map<String, dynamic> filterData = Map<String, dynamic>();
        for (MapEntry e in newData!.entries) {
          if (e.key.toString().startsWith(pageName)) {
            filterData[e.key] = e.value;
          }
        }
        data![pageName] = filterData;
        dataStore.writeString(DataStoreService.LANGUAGE_KEYS, json.encode(data));
      } else {
        data = json.decode(getLocalizedStringPreference()!);
        Map<String, dynamic> filterData = Map<String, dynamic>();
        for (MapEntry e in newData!.entries) {
          if (e.key.toString().startsWith(pageName)) {
            filterData[e.key] = e.value;
          }
        }
        data![pageName] = filterData;
        dataStore.writeString(DataStoreService.LANGUAGE_KEYS, json.encode(data));
      }
    });
    return data;
  }

  static String getSingleValueString(String page, String key) {
    Map<String, dynamic>? pageMap = getPageWiseData(page);

    if (pageMap != null) {
      Map<String, dynamic>? localizedMap = pageMap[key];
      var locale = dataStore.readString(DataStoreService.LANGUAGE) == null ? ViewConstants.languageId : dataStore.readString(DataStoreService.LANGUAGE)!;

      return localizedMap == null
          ? ""
          : isProvinceQuebec()
              ? locale == "en"
                  ? localizedMap["contentEnQc"]
                  : localizedMap["contentFrQc"]
              : locale == "en"
                  ? localizedMap["contentEn"]
                  : localizedMap["contentFr"];
    } else {
      return "";
    }
  }

  static String? getLocalizedDatePreference() {
    return dataStore.readString(DataStoreService.LANGUAGE_UPDATE);
  }

  static List<LocalizationUpdateResponse> saveUpdateDate(List<LocalizationUpdateResponse>? newData) {
    String jsonData = jsonEncode(newData!.map((e) => e.toJson()).toList());
    print(jsonData);
    if (getLocalizedDatePreference() == null) {
      dataStore.writeString(DataStoreService.LANGUAGE_UPDATE, jsonData);
      return newData;
    } else {
      List<LocalizationUpdateResponse> data = (jsonDecode(getLocalizedDatePreference()!) as List).map((i) => LocalizationUpdateResponse.fromJson(i)).toList();
      Map<String, dynamic>? storedData = json.decode(getLocalizedStringPreference().toString());
      for (var i = 0; i < newData.length; i++) {
        for (var j = 0; j < data.length; j++) {
          if (newData[i].moduleKey == data[j].moduleKey) {
            if (newData[j].updatedDateTimeInUTC != data[j].updatedDateTimeInUTC) {
              if (storedData!.containsKey(newData[i].moduleKey)) {
                storedData.remove(newData[i].moduleKey);
              }
              dataStore.writeString(DataStoreService.LANGUAGE_KEYS, json.encode(storedData));
              break;
            }
          }
        }
      }
      dataStore.writeString(DataStoreService.LANGUAGE_UPDATE, jsonData);
      return newData;
    }
  }
}

Locale setLocale(String languageCode) {
  final dataStore = locator<DataStoreService>();
  dataStore.writeString(DataStoreService.LANGUAGE, languageCode);
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  return languageCode != null && languageCode.isNotEmpty ? Locale(languageCode, '') : Locale('en', '');
}

changeLanguage(BuildContext context, String selectedLanguageCode) async {
  ViewConstants.languageId = selectedLanguageCode;
  var _locale = await setLocale(selectedLanguageCode);
  MyApp.setLocale(context, _locale);
}

Future<Locale> getLocale() async {
  final dataStore = locator<DataStoreService>();
  String? languageCode = dataStore.readString(DataStoreService.LANGUAGE) == null ? ViewConstants.languageId : dataStore.readString(DataStoreService.LANGUAGE)!;
  return _locale(languageCode);
}

String getSelectedLanguage() {
  final dataStore = locator<DataStoreService>();
  return dataStore.readString(DataStoreService.LANGUAGE) == null ? ViewConstants.languageId : dataStore.readString(DataStoreService.LANGUAGE)!;
}

String getServerSendLanguage() {
  final dataStore = locator<DataStoreService>();
  var data = dataStore.readString(DataStoreService.LANGUAGE) == null
      ? (LocalizationUtils.isProvinceQuebec())
          ? ViewConstants.languageId == ViewConstants.languageIdEn
              ? ViewConstants.languageEnglishQC
              : ViewConstants.languageFrenchQC
          : ViewConstants.languageId == ViewConstants.languageIdEn
              ? ViewConstants.languageEnglish
              : ViewConstants.languageFrench
      : (LocalizationUtils.isProvinceQuebec())
          ? dataStore.readString(DataStoreService.LANGUAGE) == ViewConstants.languageIdEn
              ? ViewConstants.languageEnglishQC
              : ViewConstants.languageFrenchQC
          : dataStore.readString(DataStoreService.LANGUAGE) == ViewConstants.languageIdEn
              ? ViewConstants.languageEnglish
              : ViewConstants.languageFrench;
  return data;
}

String getLogoImage() {
  if (getSelectedLanguage() == ViewConstants.languageIdEn) {
    return "graphics/logo-horizontal-dark.png";
  } else {
    return "graphics/logo-horizontal-dark.png";
  }
}

String? newCardId;

bool isIntegration = false;
