class Events {
  String? old_name;
  String? new_name;
  String? group;
  String? action;

  Events({this.old_name, this.new_name, this.group, this.action});
}
