import 'dart:core';

class Triple<T1, T2,T3> {
  final T1 item1;
  final T2 item2;
  final T3 item3;

  Triple({
    required this.item1,
    required this.item2,
    required this.item3,
  });

  factory Triple.fromJson(Map<String, dynamic> json) {
    return Triple(
      item1: json['item1'],
      item2: json['item2'],
      item3: json['item3'],
    );
  }
}