import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/router.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/localization/localizations_delegate.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/screen_util.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';
import 'app_config.dart';
import 'locator.dart';
import 'utils/provider_setup.dart';

bool _initialUriIsHandled = false;

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: brandColor, // Color for Android
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
      ));
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await setupSharedPreferences();
  await initPlatformState();
  await Firebase.initializeApp();
  setupLocator();
  var configuredApp = AppConfig(
    appName: 'STGPocketPills',
    flavorName: 'staging',
    apiBaseUrl: 'https://c9.api.pocketpills.com',
    webviewBaseUrl: 'https://stgapp.pocketpills.com',
    mixPanelToken: "6a4886648afb69fca0a05c2649c6149d",
    oneSignalToken: 'cbb37f0e-8394-4c2c-aa36-041a471abade',
    child: MyApp(),
  );
  setupAppConfig(configuredApp);
  runApp(configuredApp);
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale newLocale) {
    var state = context.findAncestorStateOfType<_MyAppState>();
    state!.setLocale(newLocale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Uri? _initialUri;
  Uri? _latestUri;
  Object? _err;

  StreamSubscription? _sub;
  static final DataStoreService dataStore = locator<DataStoreService>();

  Locale? _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _handleIncomingLinks();
    _handleInitialUri();
  }

  @override
  void dispose() {
    _sub?.cancel();
    super.dispose();
  }

  void _handleIncomingLinks() {
    _sub = uriLinkStream.listen((Uri? uri) async {
      if (!mounted) return;
      print('got uri: $uri');
      setState(() {
        _latestUri = uri;
        _err = null;
      });
      if (uri.toString().toLowerCase().contains('chambers') == true) {
        print('AMANCALLINGCHAMBERS');
        await extractArguments(uri.toString());
      }
    }, onError: (Object err) {
      if (!mounted) return;
      print('got err: $err');
      setState(() {
        _latestUri = null;
        if (err is FormatException) {
          _err = err;
        } else {
          _err = null;
        }
      });
    });
  }

  Future<void> _handleInitialUri() async {
    // In this example app this is an almost useless guard, but it is here to
    // show we are not going to call getInitialUri multiple times, even if this
    // was a weidget that will be disposed of (ex. a navigation route change).
    if (!_initialUriIsHandled) {
      _initialUriIsHandled = true;
      try {
        final uri = await getInitialUri();
        if (uri == null) {
          print('no initial uri');
        } else {
          print('got initial uri: $uri');
          if (uri.toString().toLowerCase().contains('chambers') == true) {
            dataStore.writeBoolean(DataStoreService.CHAMBERS_FLOW, true);
            print('AMANCALLINGCHAMBERS');
            await extractArguments(uri.toString());
          }
        }
        if (!mounted) return;
        setState(() => _initialUri = uri);
      } on PlatformException {
        // Platform messages may fail but we ignore the exception
        print('falied to get initial uri');
      } on FormatException catch (err) {
        if (!mounted) return;
        print('malformed initial uri');
        setState(() => _err = err);
      }
    }
  }

  Future<void> extractArguments(String value) async {
    print('AMANEXTRACTARGCALLED');
    List<String> args = value.split('&');
    List<String> params = args[0].split('?');
    List<String> token = args[1].split('=');
    List<String> group = params[1].split('=');

    await dataStore.writeString(DataStoreService.GROUP, group[1]);
    await dataStore.writeString(DataStoreService.TOKEN, token[1]);

    print('groups:${group[1]} + token:${token[1]}');
    await HttpApiUtils().chambersActivate(group[1], token[1], '');
  }

  @override
  Widget build(BuildContext context) {
    AppConfig.of(context);
    return MultiProvider(
        providers: providers,
        child: MaterialApp(
          locale: _locale,
          builder: (context, child) {
            ScreenUtil.init(context);
            return MediaQuery(
              child: child as Widget,
              data: MediaQuery.of(context).copyWith(
                textScaleFactor: ScreenUtil().getTextScale(),
              ),
            );
          },
          supportedLocales: [
            const Locale('en', ''),
            const Locale('fr', ''),
          ],
          localizationsDelegates: [
            const AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          localeResolutionCallback: (locale, supportedLocales) {
            for (var supportedLocale in supportedLocales) {
              if (supportedLocale.languageCode == locale?.languageCode && supportedLocale.countryCode == locale?.countryCode) {
                return supportedLocale;
              }
            }
            return supportedLocales.first;
          },
          title: 'PocketPills',
          theme: ThemeData(primarySwatch: primaryColorMain, backgroundColor: Colors.white, scaffoldBackgroundColor: Colors.white, buttonTheme: ButtonThemeData(height: 48)),
          navigatorKey: locator<NavigationService>().navigatorKey,
          initialRoute: SplashView.routeName,
          onGenerateRoute: PPRouter.generateRoute,
          debugShowCheckedModeBanner: false,
        ));
  }
}
