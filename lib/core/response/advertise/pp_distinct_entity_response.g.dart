// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_distinct_entity_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PPDistinctEntityResponse _$PPDistinctEntityResponseFromJson(
        Map<String, dynamic> json) =>
    PPDistinctEntityResponse(
      json['id'] as String,
    );

Map<String, dynamic> _$PPDistinctEntityResponseToJson(
        PPDistinctEntityResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
