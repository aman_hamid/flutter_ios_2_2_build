// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ip_location_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IpLocationResponse _$IpLocationResponseFromJson(Map<String, dynamic> json) =>
    IpLocationResponse(
      json['response'] == null
          ? null
          : LocationResponse.fromJson(json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$IpLocationResponseToJson(IpLocationResponse instance) =>
    <String, dynamic>{
      'response': instance.response,
    };
