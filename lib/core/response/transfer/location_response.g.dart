// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocationResponse _$LocationResponseFromJson(Map<String, dynamic> json) =>
    LocationResponse(
      json['location'] == null
          ? null
          : Location.fromJson(json['location'] as Map<String, dynamic>),
      (json['subdivisions'] as List<dynamic>)
          .map((e) => e == null
              ? null
              : Subdivisions.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['postal'] == null
          ? null
          : Postal.fromJson(json['postal'] as Map<String, dynamic>),
      json['traits'] == null
          ? null
          : Traits.fromJson(json['traits'] as Map<String, dynamic>),
      json['city'] == null
          ? null
          : City.fromJson(json['city'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LocationResponseToJson(LocationResponse instance) =>
    <String, dynamic>{
      'location': instance.location,
      'subdivisions': instance.subdivisions,
      'postal': instance.postal,
      'traits': instance.traits,
      'city': instance.city,
    };
