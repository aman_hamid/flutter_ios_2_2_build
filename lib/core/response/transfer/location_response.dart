import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer/cities.dart';
import 'package:pocketpills/core/models/transfer/location.dart';
import 'package:pocketpills/core/models/transfer/postals.dart';
import 'package:pocketpills/core/models/transfer/subdivisions.dart';
import 'package:pocketpills/core/models/transfer/traits.dart';
part 'location_response.g.dart';

@JsonSerializable()
class LocationResponse {
  Location? location;
  List<Subdivisions?> subdivisions;
  Postal? postal;
  Traits? traits;
  City? city;

  LocationResponse(this.location, this.subdivisions, this.postal, this.traits,this.city);

  factory LocationResponse.fromJson(Map<String, dynamic> json) => _$LocationResponseFromJson(json);
}
