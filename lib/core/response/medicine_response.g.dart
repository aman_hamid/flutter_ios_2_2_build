// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicine_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicineResponse _$MedicineResponseFromJson(Map<String, dynamic> json) =>
    MedicineResponse(
      status: json['success'] as bool,
      errMessage: json['userMessage'] as String,
      apiMessage: json['message'] as String,
      medicine: Medicine.fromJson(json['medicine'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MedicineResponseToJson(MedicineResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'medicine': instance.medicine,
    };
