import 'package:json_annotation/json_annotation.dart';
part 'signedin_verify_response.g.dart';

@JsonSerializable()
class SignedinVerifyResponse {
  @JsonKey(name: "resetKey")
  String resetKey;

  @JsonKey(name: "isPasswordSet")
  bool isPasswordSet;

  @JsonKey(name: "setPasswordToken", nullable: true)
  String setPasswordToken;

  SignedinVerifyResponse({required this.resetKey, required this.isPasswordSet, required this.setPasswordToken});

  factory SignedinVerifyResponse.fromJson(Map<String, dynamic> json) => _$SignedinVerifyResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SignedinVerifyResponseToJson(this);
}
