// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthVerifyResponse _$AuthVerifyResponseFromJson(Map<String, dynamic> json) =>
    AuthVerifyResponse(
      status: json['success'] as bool?,
      errMessage: json['userMessage'] as String?,
      apiMessage: json['message'] as String?,
      response: json['details'] == null
          ? null
          : AuthVerification.fromJson(json['details'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AuthVerifyResponseToJson(AuthVerifyResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.response,
    };
