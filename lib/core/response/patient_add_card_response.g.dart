// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_add_card_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientAddCardResponse _$PatientAddCardResponseFromJson(
        Map<String, dynamic> json) =>
    PatientAddCardResponse(
      cardIds:
          (json['cardIds'] as List<dynamic>?)?.map((e) => e as int).toList(),
    );

Map<String, dynamic> _$PatientAddCardResponseToJson(
        PatientAddCardResponse instance) =>
    <String, dynamic>{
      'cardIds': instance.cardIds,
    };
