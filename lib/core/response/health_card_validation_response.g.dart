// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_card_validation_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ValidHealthCardResponse _$ValidHealthCardResponseFromJson(
        Map<String, dynamic> json) =>
    ValidHealthCardResponse(
      json['isValidHealthCard'] as bool,
    );

Map<String, dynamic> _$ValidHealthCardResponseToJson(
        ValidHealthCardResponse instance) =>
    <String, dynamic>{
      'isValidHealthCard': instance.isValidHealthCard,
    };
