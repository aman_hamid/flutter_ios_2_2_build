// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_card_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientCardResponse _$PatientCardResponseFromJson(Map<String, dynamic> json) =>
    PatientCardResponse(
      cards: (json['cards'] as List<dynamic>?)
          ?.map((e) => PaymentCard.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PatientCardResponseToJson(
        PatientCardResponse instance) =>
    <String, dynamic>{
      'cards': instance.cards,
    };
