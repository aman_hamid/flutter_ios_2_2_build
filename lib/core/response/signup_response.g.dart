// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupResponse _$SignupResponseFromJson(Map<String, dynamic> json) =>
    SignupResponse(
      userId: json['userId'] as int?,
      patientId: json['patientId'] as int?,
      prescription: json['prescription'] == null
          ? null
          : TransferPrescription.fromJson(
              json['prescription'] as Map<String, dynamic>),
      signupDto: json['signupDto'] == null
          ? null
          : SignupDto.fromJson(json['signupDto'] as Map<String, dynamic>),
      prescriptionList: (json['prescriptionList'] as List<dynamic>?)
          ?.map((e) => Prescription.fromJson(e as Map<String, dynamic>))
          .toList(),
      documentsList: (json['prescriptionDocumentList'] as List<dynamic>?)
          ?.map((e) => PrescriptionDocument.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..status = json['success'] as bool?
      ..errMessage = json['userMessage'] as String?
      ..apiMessage = json['message'] as String?
      ..response = json['details'];

Map<String, dynamic> _$SignupResponseToJson(SignupResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.response,
      'userId': instance.userId,
      'patientId': instance.patientId,
      'prescription': instance.prescription,
      'signupDto': instance.signupDto,
      'prescriptionList': instance.prescriptionList,
      'prescriptionDocumentList': instance.documentsList,
    };
