// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prescription_create_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrescriptionCreateResponse _$PrescriptionCreateResponseFromJson(
        Map<String, dynamic> json) =>
    PrescriptionCreateResponse(
      json['omsPrescriptionId'] as int,
      SuccessDetails.fromJson(json['successDetails'] as Map<String, dynamic>),
      (json['revenue'] as num?)?.toDouble(),
      json['totalPrescriptionsCount'] as int?,
    );

Map<String, dynamic> _$PrescriptionCreateResponseToJson(
        PrescriptionCreateResponse instance) =>
    <String, dynamic>{
      'omsPrescriptionId': instance.omsPrescriptionId,
      'successDetails': instance.successDetails,
      'revenue': instance.revenue,
      'totalPrescriptionsCount': instance.totalPrescriptionsCount,
    };
