// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_delete_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentDeleteResponse _$PaymentDeleteResponseFromJson(
        Map<String, dynamic> json) =>
    PaymentDeleteResponse(
      deleted: json['deleted'] as bool,
    );

Map<String, dynamic> _$PaymentDeleteResponseToJson(
        PaymentDeleteResponse instance) =>
    <String, dynamic>{
      'deleted': instance.deleted,
    };
