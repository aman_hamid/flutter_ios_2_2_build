import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/payment_card.dart';

part 'patient_add_card_response.g.dart';

@JsonSerializable()
class PatientAddCardResponse {
  List<int>? cardIds;

  PatientAddCardResponse({this.cardIds});

  factory PatientAddCardResponse.fromJson(Map<String, dynamic> json) => _$PatientAddCardResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PatientAddCardResponseToJson(this);
}
