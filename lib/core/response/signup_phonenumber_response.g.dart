// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_phonenumber_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupPhoneNumberResponse _$SignupPhoneNumberResponseFromJson(
        Map<String, dynamic> json) =>
    SignupPhoneNumberResponse(
      userId: json['userId'] as int,
    );

Map<String, dynamic> _$SignupPhoneNumberResponseToJson(
        SignupPhoneNumberResponse instance) =>
    <String, dynamic>{
      'userId': instance.userId,
    };
