// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Medication _$MedicationFromJson(Map<String, dynamic> json) => Medication(
      rxId: json['rxId'] as int?,
      medicationId: json['medicationId'] as int?,
      drug: json['drug'] as String,
      drugId: json['drugId'] as String,
      refillsLeft: json['refillsLeft'] as int?,
      drugType: json['drugType'] as String?,
      quantityLeft: json['quantityLeft'] as num?,
      quantityFilled: json['quantityFilled'] as num?,
      quantityPerFill: json['quantityPerFill'] as num?,
      daysSupply: json['daysSupply'] as int?,
      totalQuantity: json['totalQuantity'] as num?,
      initialDispensedQuantity: json['initialDispensedQuantity'] as num?,
      sig: json['sig'] as String?,
      validUntil: json['validUntil'] as String?,
      doctor: json['doctor'] as String?,
      message: json['message'] as String?,
      statusText: json['statusText'] as String?,
      type: json['type'] as int?,
      dgType: json['dgType'] as String?,
      refillQuantity: json['refillQuantity'] as String?,
      variantId: json['variantId'] as int?,
      isSmartPack: json['isSmartPack'] as int?,
    )..id = json['id'] as int?;

Map<String, dynamic> _$MedicationToJson(Medication instance) =>
    <String, dynamic>{
      'id': instance.id,
      'rxId': instance.rxId,
      'medicationId': instance.medicationId,
      'drug': instance.drug,
      'drugId': instance.drugId,
      'refillsLeft': instance.refillsLeft,
      'drugType': instance.drugType,
      'quantityLeft': instance.quantityLeft,
      'quantityFilled': instance.quantityFilled,
      'quantityPerFill': instance.quantityPerFill,
      'daysSupply': instance.daysSupply,
      'totalQuantity': instance.totalQuantity,
      'initialDispensedQuantity': instance.initialDispensedQuantity,
      'sig': instance.sig,
      'validUntil': instance.validUntil,
      'doctor': instance.doctor,
      'message': instance.message,
      'statusText': instance.statusText,
      'type': instance.type,
      'dgType': instance.dgType,
      'refillQuantity': instance.refillQuantity,
      'variantId': instance.variantId,
      'isSmartPack': instance.isSmartPack,
    };
