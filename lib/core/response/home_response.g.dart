// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeResponse _$HomeResponseFromJson(Map<String, dynamic> json) => HomeResponse(
      freeVitamins: FreeVitaminsResponse.fromJson(
          json['freeVitamins'] as Map<String, dynamic>),
      lastOrder: (json['lastOrder'] as List<dynamic>)
          .map((e) => Order.fromJson(e as Map<String, dynamic>))
          .toList(),
      hasAddress: json['hasAddress'] as bool,
      hasFreeVitaminsOrder: json['hasFreeVitaminsOrder'] as bool,
      hasHealthInformation: json['hasHealthInformation'] as bool,
      hasCreditCard: json['hasCreditCard'] as bool,
      hasPrescription: json['hasPrescription'] as bool,
      hasInsurance: json['hasInsurance'] as bool,
    );

Map<String, dynamic> _$HomeResponseToJson(HomeResponse instance) =>
    <String, dynamic>{
      'freeVitamins': instance.freeVitamins,
      'lastOrder': instance.lastOrder,
      'hasFreeVitaminsOrder': instance.hasFreeVitaminsOrder,
      'hasPrescription': instance.hasPrescription,
      'hasHealthInformation': instance.hasHealthInformation,
      'hasInsurance': instance.hasInsurance,
      'hasAddress': instance.hasAddress,
      'hasCreditCard': instance.hasCreditCard,
    };
