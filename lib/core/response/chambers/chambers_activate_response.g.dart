// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chambers_activate_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChambersResponse _$ChambersResponseFromJson(Map<String, dynamic> json) =>
    ChambersResponse(
      json['redirect'] as String?,
      json['signUpFlow'] as String?,
      json['signUpType'] as String?,
      json['userId'] as int?,
      json['userIdentifier'] as String?,
      json['hint'] as String?,
    );

Map<String, dynamic> _$ChambersResponseToJson(ChambersResponse instance) =>
    <String, dynamic>{
      'redirect': instance.redirect,
      'signUpFlow': instance.signUpFlow,
      'signUpType': instance.signUpType,
      'userId': instance.userId,
      'userIdentifier': instance.userIdentifier,
      'hint': instance.hint,
    };
