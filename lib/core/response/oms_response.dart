import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/response/order_list_response.dart';
import 'package:pocketpills/core/response/order_response.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_list_response.dart';

import 'appointment_slot_list_response.dart';

part 'oms_response.g.dart';

@JsonSerializable()
class OMSResponse<T> {
  @JsonKey(name: 'success')
  bool? status;

  @JsonKey(name: 'userMessage')
  String? errMessage;

  @JsonKey(name: 'message')
  String? apiMessage;

  @JsonKey(name: 'error')
  String? error;

  @JsonKey(name: 'data')
  @_Converter()
  final T? response;

  OMSResponse({this.status, this.errMessage, this.apiMessage, this.response});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage!;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory OMSResponse.fromJson(Map<String, dynamic> json) => _$OMSResponseFromJson<T>(json);

  Map<String, dynamic> toJson() => _$OMSResponseToJson(this);
}

class _Converter<T extends Object?> implements JsonConverter<T, Object?> {
  const _Converter();

  Type myFunc<T>() => T;

  @override
  T fromJson(Object? json) {
//    print(T.runtimeType);
//    T a;
//    print(a is LoginResponse);
    // print(myFunc() is LoginResponse);
    //if (json == null) return null;
    switch (T.toString()) {
      case "OrderListResponse?":
        return OrderListResponse.fromJson(json as Map<String, dynamic>) as T;
      case "Prescription?":
        return Prescription.fromJson(json as Map<String, dynamic>) as T;
      case "OrderResponse?":
        return OrderResponse.fromJson(json as Map<String, dynamic>) as T;
      case "TelehealthAppointmentResponse?":
        return TelehealthAppointmentResponse.fromJson(json as Map<String, dynamic>) as T;
      case "AppointmentSlotListResponse?":
        return AppointmentSlotListResponse.fromJson(json as Map<String, dynamic>) as T;
      case "AppointmentFirstResponse?":
        return AppointmentFirstResponse.fromJson(json as Map<String, dynamic>) as T;
      case "TelehealthCreateResponse?":
        return TelehealthCreateResponse.fromJson(json as Map<String, dynamic>) as T;
/*      case "PastAppointmentListResponse?":
        return PastAppointmentListResponse.fromJson(json as Map<String, dynamic>) as T;*/
      default:
        return json as T;
    }
  }

  @override
  Object toJson(T? object) {
    // This will only work if `object` is a native JSON type:
    //   num, String, bool, null, etc
    // Or if it has a `toJson()` function`.
    return object!;
  }
}
