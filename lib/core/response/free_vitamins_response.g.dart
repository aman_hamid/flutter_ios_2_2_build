// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'free_vitamins_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FreeVitaminsResponse _$FreeVitaminsResponseFromJson(
        Map<String, dynamic> json) =>
    FreeVitaminsResponse(
      valid: json['valid'] as bool,
    );

Map<String, dynamic> _$FreeVitaminsResponseToJson(
        FreeVitaminsResponse instance) =>
    <String, dynamic>{
      'valid': instance.valid,
    };
