import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/response/prescription/prescription_document.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';

import 'package:pocketpills/core/response/base_response.dart';
part 'signup_response.g.dart';

@JsonSerializable()
class SignupResponse extends BaseResponse {
  @JsonKey(name: 'userId')
  int? userId;

  @JsonKey(name: 'patientId')
  int? patientId;

  @JsonKey(name: 'prescription')
  TransferPrescription? prescription;

  @JsonKey(name: 'signupDto')
  SignupDto? signupDto;

  @JsonKey(name: 'prescriptionList')
  List<Prescription>? prescriptionList;

  @JsonKey(name: 'prescriptionDocumentList')
  List<PrescriptionDocument>? documentsList;

  SignupResponse({this.userId, this.patientId, this.prescription, this.signupDto, this.prescriptionList,this.documentsList});

  factory SignupResponse.fromJson(Map<String, dynamic> json) => _$SignupResponseFromJson(json);
}
