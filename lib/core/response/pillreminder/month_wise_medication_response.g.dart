// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'month_wise_medication_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MonthWiseMedicationResponse _$MonthWiseMedicationResponseFromJson(
        Map<String, dynamic> json) =>
    MonthWiseMedicationResponse(
      (json['dayWiseMedications'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, DayWiseMedications.fromJson(e as Map<String, dynamic>)),
      ),
    );

Map<String, dynamic> _$MonthWiseMedicationResponseToJson(
        MonthWiseMedicationResponse instance) =>
    <String, dynamic>{
      'dayWiseMedications': instance.dayWiseMedications,
    };
