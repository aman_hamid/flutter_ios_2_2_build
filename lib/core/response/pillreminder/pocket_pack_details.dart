import 'package:enum_to_string/enum_to_string.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/pillreminder/medication_details.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
part 'pocket_pack_details.g.dart';

@JsonSerializable()
class PocketPackDetails {
  String? time;
  String? partOfDay;
  String? status;
  String? type;
  List<int>? pocketPackIds;
  List<MedicationDetails>? medicationDetails;

  PillStatusType? getPillStatusType() {
    return EnumToString.fromString(PillStatusType.values, status!);
  }

  PocketPackDetails(this.time, this.partOfDay, this.status, this.type, this.pocketPackIds, this.medicationDetails);

  factory PocketPackDetails.fromJson(Map<String, dynamic> json) => _$PocketPackDetailsFromJson(json);
}
