// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'day_wise_medications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DayWiseMedications _$DayWiseMedicationsFromJson(Map<String, dynamic> json) =>
    DayWiseMedications(
      date: json['date'] as String?,
      firstName: json['firstName'] as String?,
      title: json['title'] as String?,
      missedCount: json['missedCount'] as int?,
      takenCount: json['takenCount'] as int?,
      pendingCount: json['pendingCount'] as int?,
      pocketPackDetails: (json['pocketPackDetails'] as List<dynamic>?)
          ?.map((e) => PocketPackDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DayWiseMedicationsToJson(DayWiseMedications instance) =>
    <String, dynamic>{
      'date': instance.date,
      'firstName': instance.firstName,
      'title': instance.title,
      'missedCount': instance.missedCount,
      'takenCount': instance.takenCount,
      'pendingCount': instance.pendingCount,
      'pocketPackDetails': instance.pocketPackDetails,
    };
