// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationDetails _$MedicationDetailsFromJson(Map<String, dynamic> json) =>
    MedicationDetails(
      (json['applicableDaysOfWeek'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      json['applicableForAllDays'] as bool?,
      json['medication'] as String?,
      json['quantity'] as num?,
      json['quantityUnit'] as String?,
      json['type'] as String?,
    );

Map<String, dynamic> _$MedicationDetailsToJson(MedicationDetails instance) =>
    <String, dynamic>{
      'applicableDaysOfWeek': instance.applicableDaysOfWeek,
      'applicableForAllDays': instance.applicableForAllDays,
      'medication': instance.medication,
      'quantity': instance.quantity,
      'quantityUnit': instance.quantityUnit,
      'type': instance.type,
    };
