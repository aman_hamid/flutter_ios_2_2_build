// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pocket_pack_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PocketPackDetails _$PocketPackDetailsFromJson(Map<String, dynamic> json) =>
    PocketPackDetails(
      json['time'] as String?,
      json['partOfDay'] as String?,
      json['status'] as String?,
      json['type'] as String?,
      (json['pocketPackIds'] as List<dynamic>?)?.map((e) => e as int).toList(),
      (json['medicationDetails'] as List<dynamic>?)
          ?.map((e) => MedicationDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PocketPackDetailsToJson(PocketPackDetails instance) =>
    <String, dynamic>{
      'time': instance.time,
      'partOfDay': instance.partOfDay,
      'status': instance.status,
      'type': instance.type,
      'pocketPackIds': instance.pocketPackIds,
      'medicationDetails': instance.medicationDetails,
    };
