// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_transfer_image_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupTransferImageResponse _$SignupTransferImageResponseFromJson(
        Map<String, dynamic> json) =>
    SignupTransferImageResponse(
      json['originalPath'] as String,
      json['resizedPath'] as String,
    );

Map<String, dynamic> _$SignupTransferImageResponseToJson(
        SignupTransferImageResponse instance) =>
    <String, dynamic>{
      'originalPath': instance.originalPath,
      'resizedPath': instance.resizedPath,
    };
