import 'package:dio/dio.dart';
import 'package:pocketpills/utils/Tuples.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import 'old_base_response.dart';

Future<ResultHandle<bool, String>> ResponseHandle(Response? response, {bool checkValue = false}) async {
  if (response != null && response.data != null && response.data['success'] == true) {
    OldBaseResponse oldResponse = OldBaseResponse.fromJson(response.data);
    if(true){
      print("PPMedicine Hit");
    }
    return ResultHandle(status: oldResponse.status!, message: oldResponse.apiMessage ?? "");
  } else if (response != null && response.data != null && response.data['success'] == false) {
    OldBaseResponse oldResponse = OldBaseResponse.fromJson(response.data);
    String errorMessage = oldResponse.getErrorMessage();
    return ResultHandle(status: false, message: errorMessage);
  } else {
    String errorMessage = LocalizationUtils.getSingleValueString("common", "common.label.api-error");
    return ResultHandle(status: false, message: errorMessage);
  }
}
