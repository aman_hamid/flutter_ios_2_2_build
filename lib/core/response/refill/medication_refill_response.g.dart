// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_refill_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationRefillResponse _$MedicationRefillResponseFromJson(
        Map<String, dynamic> json) =>
    MedicationRefillResponse(
      json['omsOrderId'] as int?,
      json['successDetails'] == null
          ? null
          : SuccessDetails.fromJson(
              json['successDetails'] as Map<String, dynamic>),
    )
      ..status = json['success'] as bool?
      ..errMessage = json['userMessage'] as String?
      ..apiMessage = json['message'] as String?
      ..response = json['details'];

Map<String, dynamic> _$MedicationRefillResponseToJson(
        MedicationRefillResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.response,
      'omsOrderId': instance.omsOrderId,
      'successDetails': instance.successDetails,
    };
