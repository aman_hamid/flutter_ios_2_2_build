// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_slot_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentSlotListResponse _$AppointmentSlotListResponseFromJson(
        Map<String, dynamic> json) =>
    AppointmentSlotListResponse(
      items: (json['items'] as List<dynamic>)
          .map((e) => AppointmentTime.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AppointmentSlotListResponseToJson(
        AppointmentSlotListResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
