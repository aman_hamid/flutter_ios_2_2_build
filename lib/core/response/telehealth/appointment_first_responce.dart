import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealthdoctor_details_response.dart';

part 'appointment_first_responce.g.dart';

@JsonSerializable()
class AppointmentFirstResponse {
  TelehealthAppointmentResponse? prescriptionDTO;
  String? timeSlotEn;
  String? slotDateEn;
  String? timeSlot;
  String? slotDate;
  String? timeSlotFr;
  String? slotDateFr;
  String? prescriptionMedicalConditions;
  String? status;
  TeleHealthDoctorResponse? teleHealthDoctor;
  String? prescriptionComment;
  String? healthCardUploadTimeEn;
  String? healthCardUploadTimeFr;

  AppointmentFirstResponse(
      {required this.prescriptionDTO,
      required this.timeSlot,
      required this.slotDate,
        required this.timeSlotEn,
      required this.slotDateEn,
      required this.timeSlotFr,
      required this.slotDateFr,
      required this.prescriptionMedicalConditions,
      required this.teleHealthDoctor,
      required this.status,
      this.prescriptionComment,
      this.healthCardUploadTimeEn,
      this.healthCardUploadTimeFr});

  factory AppointmentFirstResponse.fromJson(Map<String, dynamic> json) => _$AppointmentFirstResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentFirstResponseToJson(this);
}
