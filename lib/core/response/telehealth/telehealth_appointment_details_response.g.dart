// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_appointment_details_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthAppointmentResponse _$TelehealthAppointmentResponseFromJson(
        Map<String, dynamic> json) =>
    TelehealthAppointmentResponse(
      json['prescriptionRequestReason'] as String?,
      json['prescriptionRequestCategory'] as String?,
      PPDateUtils.fromStr(json['appointmentTime'] as String?),
      json['prescriptionMedicalConditions'] as String?,
      json['prescriptionComment'] as String?,
      json['telehealthClinic'] as String?,
      json['telehealthRequestedMedications'] as String?,
      json['prescriptionFilledByExternalPharmacy'] as bool?,
      json['doctor'] == null
          ? null
          : DoctorResponse.fromJson(json['doctor'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TelehealthAppointmentResponseToJson(
        TelehealthAppointmentResponse instance) =>
    <String, dynamic>{
      'prescriptionRequestReason': instance.prescriptionRequestReason,
      'prescriptionFilledByExternalPharmacy':
          instance.prescriptionFilledByExternalPharmacy,
      'prescriptionRequestCategory': instance.prescriptionRequestCategory,
      'appointmentTime': PPDateUtils.toStr(instance.appointmentTime),
      'telehealthRequestedMedications': instance.telehealthRequestedMedications,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'prescriptionComment': instance.prescriptionComment,
      'telehealthClinic': instance.telehealthClinic,
      'doctor': instance.doctor,
    };
