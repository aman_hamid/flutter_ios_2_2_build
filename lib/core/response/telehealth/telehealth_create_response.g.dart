// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_create_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthCreateResponse _$TelehealthCreateResponseFromJson(
        Map<String, dynamic> json) =>
    TelehealthCreateResponse(
      json['omsPrescriptionId'] as int?,
      json['successDetails'] == null
          ? null
          : SuccessDetails.fromJson(
              json['successDetails'] as Map<String, dynamic>),
      (json['revenue'] as num?)?.toDouble(),
      json['totalPrescriptionsCount'] as int?,
    );

Map<String, dynamic> _$TelehealthCreateResponseToJson(
        TelehealthCreateResponse instance) =>
    <String, dynamic>{
      'omsPrescriptionId': instance.omsPrescriptionId,
      'successDetails': instance.successDetails,
      'revenue': instance.revenue,
      'totalPrescriptionsCount': instance.totalPrescriptionsCount,
    };
