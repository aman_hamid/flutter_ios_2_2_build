// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctor_details_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoctorResponse _$DoctorResponseFromJson(Map<String, dynamic> json) =>
    DoctorResponse(
      json['doctorNameWithDr'] as String?,
    );

Map<String, dynamic> _$DoctorResponseToJson(DoctorResponse instance) =>
    <String, dynamic>{
      'doctorNameWithDr': instance.doctorNameWithDr,
    };
