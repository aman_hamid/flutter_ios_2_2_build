import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';

import 'medicalCondition_suggestion.dart';

part 'telehealth_province_responce.g.dart';

@JsonSerializable()
class ProvinceResponse {
  @JsonKey(name: 'items')
  List<TelehealthProvinceItem> items;

  ProvinceResponse(this.items);

  factory ProvinceResponse.fromJson(Map<String, dynamic> json) => _$ProvinceResponseFromJson(json);
}
