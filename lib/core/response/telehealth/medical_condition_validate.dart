import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';

part 'medical_condition_validate.g.dart';

@JsonSerializable()
class MedicalConditionValidate {
  String? message;
  List<String>? keywordsMatched;
  bool? restricted;

  MedicalConditionValidate(
    this.message,
    this.keywordsMatched,
    this.restricted,
  );

  factory MedicalConditionValidate.fromJson(Map<String, dynamic> json) => _$MedicalConditionValidateFromJson(json);
}
