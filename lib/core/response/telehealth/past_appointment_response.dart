import 'package:json_annotation/json_annotation.dart';

part 'past_appointment_response.g.dart';

@JsonSerializable()
class PastAppointment {
  String? slotDate;
  String? timeSlot;
  String? status;

  PastAppointment({this.slotDate, this.timeSlot, this.status});

  factory PastAppointment.fromJson(Map<String, dynamic> json) => _$PastAppointmentFromJson(json);

  Map<String, dynamic> toJson() => _$PastAppointmentToJson(this);
}
