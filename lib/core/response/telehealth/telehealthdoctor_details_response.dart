import 'package:json_annotation/json_annotation.dart';

part 'telehealthdoctor_details_response.g.dart';

@JsonSerializable()
class TeleHealthDoctorResponse {
  String? telehealthPharmacyName;

  TeleHealthDoctorResponse({this.telehealthPharmacyName});

  factory TeleHealthDoctorResponse.fromJson(Map<String, dynamic> json) => _$TeleHealthDoctorResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TeleHealthDoctorResponseToJson(this);
}
