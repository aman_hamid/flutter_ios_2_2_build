// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medical_condition_validate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicalConditionValidate _$MedicalConditionValidateFromJson(
        Map<String, dynamic> json) =>
    MedicalConditionValidate(
      json['message'] as String?,
      (json['keywordsMatched'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      json['restricted'] as bool?,
    );

Map<String, dynamic> _$MedicalConditionValidateToJson(
        MedicalConditionValidate instance) =>
    <String, dynamic>{
      'message': instance.message,
      'keywordsMatched': instance.keywordsMatched,
      'restricted': instance.restricted,
    };
