// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_first_responce.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentFirstResponse _$AppointmentFirstResponseFromJson(
        Map<String, dynamic> json) =>
    AppointmentFirstResponse(
      prescriptionDTO: json['prescriptionDTO'] == null
          ? null
          : TelehealthAppointmentResponse.fromJson(
              json['prescriptionDTO'] as Map<String, dynamic>),
      timeSlot: json['timeSlot'] as String?,
      slotDate: json['slotDate'] as String?,
      timeSlotEn: json['timeSlotEn'] as String?,
      slotDateEn: json['slotDateEn'] as String?,
      timeSlotFr: json['timeSlotFr'] as String?,
      slotDateFr: json['slotDateFr'] as String?,
      prescriptionMedicalConditions:
          json['prescriptionMedicalConditions'] as String?,
      teleHealthDoctor: json['teleHealthDoctor'] == null
          ? null
          : TeleHealthDoctorResponse.fromJson(
              json['teleHealthDoctor'] as Map<String, dynamic>),
      status: json['status'] as String?,
      prescriptionComment: json['prescriptionComment'] as String?,
      healthCardUploadTimeEn: json['healthCardUploadTimeEn'] as String?,
      healthCardUploadTimeFr: json['healthCardUploadTimeFr'] as String?,
    );

Map<String, dynamic> _$AppointmentFirstResponseToJson(
        AppointmentFirstResponse instance) =>
    <String, dynamic>{
      'prescriptionDTO': instance.prescriptionDTO,
      'timeSlotEn': instance.timeSlotEn,
      'slotDateEn': instance.slotDateEn,
      'timeSlot': instance.timeSlot,
      'slotDate': instance.slotDate,
      'timeSlotFr': instance.timeSlotFr,
      'slotDateFr': instance.slotDateFr,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'status': instance.status,
      'teleHealthDoctor': instance.teleHealthDoctor,
      'prescriptionComment': instance.prescriptionComment,
      'healthCardUploadTimeEn': instance.healthCardUploadTimeEn,
      'healthCardUploadTimeFr': instance.healthCardUploadTimeFr,
    };
