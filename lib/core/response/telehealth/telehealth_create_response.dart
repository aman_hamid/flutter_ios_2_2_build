import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'telehealth_create_response.g.dart';

@JsonSerializable()
class TelehealthCreateResponse {
  int? omsPrescriptionId;
  SuccessDetails? successDetails;
  double? revenue;
  int? totalPrescriptionsCount;

  TelehealthCreateResponse(this.omsPrescriptionId, this.successDetails, this.revenue, this.totalPrescriptionsCount);

  factory TelehealthCreateResponse.fromJson(Map<String, dynamic> json) => _$TelehealthCreateResponseFromJson(json);
}
