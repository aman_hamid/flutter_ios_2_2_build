// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'past_appointment_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PastAppointmentListResponse _$PastAppointmentListResponseFromJson(
        Map<String, dynamic> json) =>
    PastAppointmentListResponse(
      items: (json['items'] as List<dynamic>)
          .map((e) => PastAppointment.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PastAppointmentListResponseToJson(
        PastAppointmentListResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
