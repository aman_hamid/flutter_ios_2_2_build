// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'past_appointment_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PastAppointment _$PastAppointmentFromJson(Map<String, dynamic> json) =>
    PastAppointment(
      slotDate: json['slotDate'] as String?,
      timeSlot: json['timeSlot'] as String?,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$PastAppointmentToJson(PastAppointment instance) =>
    <String, dynamic>{
      'slotDate': instance.slotDate,
      'timeSlot': instance.timeSlot,
      'status': instance.status,
    };
