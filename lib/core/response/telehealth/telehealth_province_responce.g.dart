// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_province_responce.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProvinceResponse _$ProvinceResponseFromJson(Map<String, dynamic> json) =>
    ProvinceResponse(
      (json['items'] as List<dynamic>)
          .map(
              (e) => TelehealthProvinceItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProvinceResponseToJson(ProvinceResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
