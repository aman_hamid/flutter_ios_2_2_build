// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_province.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthProvinceItem _$TelehealthProvinceItemFromJson(
        Map<String, dynamic> json) =>
    TelehealthProvinceItem(
      json['value'] as String,
      json['label'] as String,
      appointmentSlotsEnabled: json['appointmentSlotsEnabled'] as bool?,
      smsCommunicationEnabled: json['smsCommunicationEnabled'] as bool?,
      showHealthCardScreen: json['showHealthCardScreen'] as bool?,
      ehrEnabled: json['ehrEnabled'] as bool?,
      defaultFlowEnabled: json['defaultFlowEnabled'] as bool?,
      alesseFlowEnabled: json['alesseFlowEnabled'] as bool?,
    );

Map<String, dynamic> _$TelehealthProvinceItemToJson(
        TelehealthProvinceItem instance) =>
    <String, dynamic>{
      'value': instance.value,
      'label': instance.label,
      'appointmentSlotsEnabled': instance.appointmentSlotsEnabled,
      'smsCommunicationEnabled': instance.smsCommunicationEnabled,
      'showHealthCardScreen': instance.showHealthCardScreen,
      'ehrEnabled': instance.ehrEnabled,
      'defaultFlowEnabled': instance.defaultFlowEnabled,
      'alesseFlowEnabled': instance.alesseFlowEnabled,
    };
