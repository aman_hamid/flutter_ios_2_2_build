import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'telehealth_province.g.dart';

@JsonSerializable()
class TelehealthProvinceItem {
  @JsonKey(name: 'value')
  String value;

  @JsonKey(name: 'label')
  String label;

  @JsonKey(name: 'appointmentSlotsEnabled')
  bool? appointmentSlotsEnabled;

  @JsonKey(name: 'smsCommunicationEnabled')
  bool? smsCommunicationEnabled;

  @JsonKey(name: 'showHealthCardScreen')
  bool? showHealthCardScreen;

  @JsonKey(name: 'ehrEnabled')
  bool? ehrEnabled;

  @JsonKey(name: 'defaultFlowEnabled')
  bool? defaultFlowEnabled;

  @JsonKey(name: 'alesseFlowEnabled')
  bool? alesseFlowEnabled;

  TelehealthProvinceItem(
    this.value,
    this.label,
      {this.appointmentSlotsEnabled,
        this.smsCommunicationEnabled,
        this.showHealthCardScreen,
        this.ehrEnabled,
        this.defaultFlowEnabled,
        this.alesseFlowEnabled,
      }
  );

  factory TelehealthProvinceItem.fromJson(Map<String, dynamic> json) => _$TelehealthProvinceItemFromJson(json);
}
