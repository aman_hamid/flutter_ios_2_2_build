// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealthdoctor_details_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeleHealthDoctorResponse _$TeleHealthDoctorResponseFromJson(
        Map<String, dynamic> json) =>
    TeleHealthDoctorResponse(
      telehealthPharmacyName: json['telehealthPharmacyName'] as String?,
    );

Map<String, dynamic> _$TeleHealthDoctorResponseToJson(
        TeleHealthDoctorResponse instance) =>
    <String, dynamic>{
      'telehealthPharmacyName': instance.telehealthPharmacyName,
    };
