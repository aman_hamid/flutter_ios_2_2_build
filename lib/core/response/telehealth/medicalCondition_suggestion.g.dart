// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicalCondition_suggestion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicalConditionSuggestion _$MedicalConditionSuggestionFromJson(
        Map<String, dynamic> json) =>
    MedicalConditionSuggestion(
      json['conditionType'] as String?,
      json['symptom'] as String?,
      json['restrictedToPrescribe'] as bool,
    );

Map<String, dynamic> _$MedicalConditionSuggestionToJson(
        MedicalConditionSuggestion instance) =>
    <String, dynamic>{
      'conditionType': instance.conditionType,
      'symptom': instance.symptom,
      'restrictedToPrescribe': instance.restrictedToPrescribe,
    };
