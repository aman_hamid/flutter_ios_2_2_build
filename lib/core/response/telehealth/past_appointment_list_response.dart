import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_response.dart';

part 'past_appointment_list_response.g.dart';

@JsonSerializable()
class PastAppointmentListResponse {
  @JsonKey(name: "items")
  List<PastAppointment> items;

  PastAppointmentListResponse({required this.items});

  factory PastAppointmentListResponse.fromJson(Map<String, dynamic> json) => _$PastAppointmentListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PastAppointmentListResponseToJson(this);
}
