import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'pincode_suggestion.g.dart';

@JsonSerializable()
class PinCodeSuggestion {
  @JsonKey(name: 'suggestionId')
  String? suggestionId;

  @JsonKey(name: 'text')
  String? text;
  @JsonKey(name: 'description')
  String? description;

  PinCodeSuggestion(
    this.suggestionId,
    this.text,
    this.description,
  );

  factory PinCodeSuggestion.fromJson(Map<String, dynamic> json) => _$PinCodeSuggestionFromJson(json);
}
