// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pincode_suggestion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PinCodeSuggestionResponse _$PinCodeSuggestionResponseFromJson(
        Map<String, dynamic> json) =>
    PinCodeSuggestionResponse(
      (json['items'] as List<dynamic>)
          .map((e) => PinCodeSuggestion.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PinCodeSuggestionResponseToJson(
        PinCodeSuggestionResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
