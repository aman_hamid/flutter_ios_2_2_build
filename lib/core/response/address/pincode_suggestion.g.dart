// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pincode_suggestion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PinCodeSuggestion _$PinCodeSuggestionFromJson(Map<String, dynamic> json) =>
    PinCodeSuggestion(
      json['suggestionId'] as String?,
      json['text'] as String?,
      json['description'] as String?,
    );

Map<String, dynamic> _$PinCodeSuggestionToJson(PinCodeSuggestion instance) =>
    <String, dynamic>{
      'suggestionId': instance.suggestionId,
      'text': instance.text,
      'description': instance.description,
    };
