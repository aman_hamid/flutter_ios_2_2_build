// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_patient_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddPatientResponse _$AddPatientResponseFromJson(Map<String, dynamic> json) =>
    AddPatientResponse(
      json['patientId'] as int?,
      (json['patientList'] as List<dynamic>?)
          ?.map((e) => UserPatient.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AddPatientResponseToJson(AddPatientResponse instance) =>
    <String, dynamic>{
      'patientId': instance.patientId,
      'patientList': instance.patientList,
    };
