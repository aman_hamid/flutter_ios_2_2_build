// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card_registration_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCardRegistrationResponse _$CreditCardRegistrationResponseFromJson(
        Map<String, dynamic> json) =>
    CreditCardRegistrationResponse(
      json['userMessage'] as String?,
      json['success'] as bool?,
      json['message'] as String?,
      json['html'] as String?,
    );

Map<String, dynamic> _$CreditCardRegistrationResponseToJson(
        CreditCardRegistrationResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'html': instance.html,
    };
