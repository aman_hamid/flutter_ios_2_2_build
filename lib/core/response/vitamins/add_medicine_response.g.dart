// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_medicine_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddMedicineResponse _$AddMedicineResponseFromJson(Map<String, dynamic> json) =>
    AddMedicineResponse(
      json['id'] as int?,
      json['disabled'] as bool?,
      json['patientId'] as int?,
      json['couponCode'] == null
          ? null
          : CouponCode.fromJson(json['couponCode'] as Map<String, dynamic>),
      json['addressId'] as int?,
      json['ccId'] as int?,
      json['listPrice'] as num?,
      json['totalPrice'] as num?,
      (json['shoppingCartItems'] as List<dynamic>?)
          ?.map((e) => ShoppingCartItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AddMedicineResponseToJson(
        AddMedicineResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'patientId': instance.patientId,
      'couponCode': instance.couponCode,
      'addressId': instance.addressId,
      'ccId': instance.ccId,
      'listPrice': instance.listPrice,
      'totalPrice': instance.totalPrice,
      'shoppingCartItems': instance.shoppingCartItems,
    };
