// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopping_cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoppingCartItem _$ShoppingCartItemFromJson(Map<String, dynamic> json) =>
    ShoppingCartItem(
      patientId: json['patientId'] as int?,
      disabled: json['disabled'] as bool?,
      variantId: json['variantId'] as int?,
      monthsSupply: json['monthsSupply'] as int?,
      quantity: json['quantity'] as num?,
      userSigCode: json['userSigCode'] as String?,
      totalPrice: json['totalPrice'] as num?,
      listPrice: json['listPrice'] as num?,
    )..drugDTO = json['drugDTO'] == null
        ? null
        : Medicine.fromJson(json['drugDTO'] as Map<String, dynamic>);

Map<String, dynamic> _$ShoppingCartItemToJson(ShoppingCartItem instance) =>
    <String, dynamic>{
      'patientId': instance.patientId,
      'disabled': instance.disabled,
      'variantId': instance.variantId,
      'monthsSupply': instance.monthsSupply,
      'quantity': instance.quantity,
      'userSigCode': instance.userSigCode,
      'totalPrice': instance.totalPrice,
      'listPrice': instance.listPrice,
      'drugDTO': instance.drugDTO,
    };
