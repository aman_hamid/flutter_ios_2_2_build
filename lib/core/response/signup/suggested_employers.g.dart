// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'suggested_employers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuggestedEmployers _$SuggestedEmployersFromJson(Map<String, dynamic> json) =>
    SuggestedEmployers(
      id: json['id'] as int?,
      disabled: json['disabled'] as bool?,
      employerName: json['employerName'] as String?,
      invitationCodeDataSet: (json['invitationCodeDataSet'] as List<dynamic>?)
          ?.map(
              (e) => InvitationCodeDataSet.fromJson(e as Map<String, dynamic>))
          .toList(),
      employerDisplayName: json['employerDisplayName'] as String?,
    );

Map<String, dynamic> _$SuggestedEmployersToJson(SuggestedEmployers instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'employerName': instance.employerName,
      'employerDisplayName': instance.employerDisplayName,
      'invitationCodeDataSet': instance.invitationCodeDataSet,
    };
