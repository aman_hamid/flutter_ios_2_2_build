// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signupEDDitem.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupEddItem _$SignupEddItemFromJson(Map<String, dynamic> json) =>
    SignupEddItem(
      json['value'] as String,
      json['label'] as String,
    );

Map<String, dynamic> _$SignupEddItemToJson(SignupEddItem instance) =>
    <String, dynamic>{
      'value': instance.value,
      'label': instance.label,
    };
