// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employer_suggestion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployerSuggestionResponse _$EmployerSuggestionResponseFromJson(
        Map<String, dynamic> json) =>
    EmployerSuggestionResponse(
      suggestedEmployers: (json['suggestedEmployers'] as List<dynamic>?)
          ?.map((e) => SuggestedEmployers.fromJson(e as Map<String, dynamic>))
          .toList(),
      alreadyMappedEmployers: json['alreadyMappedEmployers'] as int?,
      suggestionsValidCount: json['suggestionsValidCount'] as int?,
    );

Map<String, dynamic> _$EmployerSuggestionResponseToJson(
        EmployerSuggestionResponse instance) =>
    <String, dynamic>{
      'suggestedEmployers': instance.suggestedEmployers,
      'alreadyMappedEmployers': instance.alreadyMappedEmployers,
      'suggestionsValidCount': instance.suggestionsValidCount,
    };
