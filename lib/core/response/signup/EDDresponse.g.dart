// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'EDDresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EDDresponse _$EDDresponseFromJson(Map<String, dynamic> json) => EDDresponse(
      (json['items'] as List<dynamic>)
          .map((e) => SignupEddItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EDDresponseToJson(EDDresponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
