import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';
import 'package:pocketpills/core/response/signup/signupEDDitem.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';


part 'EDDresponse.g.dart';

@JsonSerializable()
class EDDresponse {
  @JsonKey(name: 'items')
  List<SignupEddItem> items;

  EDDresponse(this.items);

  factory EDDresponse.fromJson(Map<String, dynamic> json) => _$EDDresponseFromJson(json);
}
