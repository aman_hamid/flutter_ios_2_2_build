// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insurance_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InsuranceResponse _$InsuranceResponseFromJson(Map<String, dynamic> json) =>
    InsuranceResponse(
      insurance: json['insurance'] == null
          ? null
          : Insurance.fromJson(json['insurance'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InsuranceResponseToJson(InsuranceResponse instance) =>
    <String, dynamic>{
      'insurance': instance.insurance,
    };
