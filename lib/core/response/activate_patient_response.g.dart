// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activate_patient_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivatePatientResponse _$ActivatePatientResponseFromJson(
        Map<String, dynamic> json) =>
    ActivatePatientResponse(
      json['addedAddress'] as bool,
      Patient.fromJson(json['insertedPatient'] as Map<String, dynamic>),
      json['addedDependents'] as bool,
      json['addedInsurance'] as bool,
      json['createdUser'] as bool,
    );

Map<String, dynamic> _$ActivatePatientResponseToJson(
        ActivatePatientResponse instance) =>
    <String, dynamic>{
      'addedAddress': instance.addedAddress,
      'addedInsurance': instance.addedInsurance,
      'addedDependents': instance.addedDependents,
      'createdUser': instance.createdUser,
      'insertedPatient': instance.patient,
    };
