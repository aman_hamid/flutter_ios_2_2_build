// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'referral_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReferralResponse _$ReferralResponseFromJson(Map<String, dynamic> json) =>
    ReferralResponse(
      json['id'] as int?,
      json['disabled'] as bool?,
      json['userId'] as int?,
      json['appliedReferralCode'] as String?,
      json['referralCode'] as String?,
    );

Map<String, dynamic> _$ReferralResponseToJson(ReferralResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'userId': instance.userId,
      'appliedReferralCode': instance.appliedReferralCode,
      'referralCode': instance.referralCode,
    };
