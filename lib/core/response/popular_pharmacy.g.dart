// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_pharmacy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PopularPharmacy _$PopularPharmacyFromJson(Map<String, dynamic> json) =>
    PopularPharmacy(
      success: json['success'] as bool?,
      details:
          (json['details'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$PopularPharmacyToJson(PopularPharmacy instance) =>
    <String, dynamic>{
      'success': instance.success,
      'details': instance.details,
    };
