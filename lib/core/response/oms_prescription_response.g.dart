// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oms_prescription_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OMSPrescriptionResponse _$OMSPrescriptionResponseFromJson(
        Map<String, dynamic> json) =>
    OMSPrescriptionResponse(
      status: json['success'] as bool?,
      errMessage: json['userMessage'] as String?,
      apiMessage: json['message'] as String?,
      response: (json['data'] as List<dynamic>?)
          ?.map((e) => Prescription.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..error = json['error'] as String?;

Map<String, dynamic> _$OMSPrescriptionResponseToJson(
        OMSPrescriptionResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'error': instance.error,
      'data': instance.response,
    };
