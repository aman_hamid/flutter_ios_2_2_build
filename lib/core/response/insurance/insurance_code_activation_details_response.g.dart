// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insurance_code_activation_details_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InsuranceCodeActivationDetailsResponse
    _$InsuranceCodeActivationDetailsResponseFromJson(
            Map<String, dynamic> json) =>
        InsuranceCodeActivationDetailsResponse(
          InsuranceCodeActivationDetail.fromJson(
              json['insuranceCodeActivationDetails'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$InsuranceCodeActivationDetailsResponseToJson(
        InsuranceCodeActivationDetailsResponse instance) =>
    <String, dynamic>{
      'insuranceCodeActivationDetails': instance.insuranceCodeActivationDetails,
    };
