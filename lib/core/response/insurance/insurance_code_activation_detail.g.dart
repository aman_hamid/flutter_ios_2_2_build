// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insurance_code_activation_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InsuranceCodeActivationDetail _$InsuranceCodeActivationDetailFromJson(
        Map<String, dynamic> json) =>
    InsuranceCodeActivationDetail(
      json['updated'] as bool,
      json['insertedPatient'] as bool,
      json['createdUser'] as bool,
      json['addedInsurance'] as bool,
      json['addedAddress'] as bool,
      json['addedDependents'] as bool,
    );

Map<String, dynamic> _$InsuranceCodeActivationDetailToJson(
        InsuranceCodeActivationDetail instance) =>
    <String, dynamic>{
      'updated': instance.updated,
      'insertedPatient': instance.insertedPatient,
      'createdUser': instance.createdUser,
      'addedInsurance': instance.addedInsurance,
      'addedAddress': instance.addedAddress,
      'addedDependents': instance.addedDependents,
    };
