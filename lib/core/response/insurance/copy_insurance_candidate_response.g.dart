// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'copy_insurance_candidate_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CopyInsuranceCandidateResponse _$CopyInsuranceCandidateResponseFromJson(
        Map<String, dynamic> json) =>
    CopyInsuranceCandidateResponse(
      (json['primaryInsuranceCandidates'] as List<dynamic>)
          .map((e) => Patient.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['secondaryInsuranceCandidates'] as List<dynamic>)
          .map((e) => Patient.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CopyInsuranceCandidateResponseToJson(
        CopyInsuranceCandidateResponse instance) =>
    <String, dynamic>{
      'primaryInsuranceCandidates': instance.primaryInsuranceCandidates,
      'secondaryInsuranceCandidates': instance.secondaryInsuranceCandidates,
    };
