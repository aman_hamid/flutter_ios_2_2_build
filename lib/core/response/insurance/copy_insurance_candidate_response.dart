import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'copy_insurance_candidate_response.g.dart';

@JsonSerializable()
class CopyInsuranceCandidateResponse {
  List<Patient> primaryInsuranceCandidates;
  List<Patient> secondaryInsuranceCandidates;

  CopyInsuranceCandidateResponse(this.primaryInsuranceCandidates, this.secondaryInsuranceCandidates);

  factory CopyInsuranceCandidateResponse.fromJson(Map<String, dynamic> json) =>
      _$CopyInsuranceCandidateResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CopyInsuranceCandidateResponseToJson(this);
}
