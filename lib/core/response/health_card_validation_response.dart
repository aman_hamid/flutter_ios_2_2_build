import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'health_card_validation_response.g.dart';

@JsonSerializable()
class ValidHealthCardResponse {
  @JsonKey(name: 'isValidHealthCard')
  bool isValidHealthCard;


  ValidHealthCardResponse( this.isValidHealthCard);

  factory ValidHealthCardResponse.fromJson(Map<String, dynamic> json) => _$ValidHealthCardResponseFromJson(json);
}
