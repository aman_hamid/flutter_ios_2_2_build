
import 'package:json_annotation/json_annotation.dart';
part 'popular_pharmacy.g.dart';

@JsonSerializable()
class PopularPharmacy {
  @JsonKey(name: 'success')
  bool? success;
  @JsonKey(name: 'details')
  List<String>? details;

  PopularPharmacy({this.success, this.details});

  factory PopularPharmacy.fromJson(Map<String, dynamic> json) => _$PopularPharmacyFromJson(json);

}