import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
part 'appointment_slot_list_response.g.dart';

@JsonSerializable()
class AppointmentSlotListResponse {
  @JsonKey(name: "items")
  List<AppointmentTime> items;

  AppointmentSlotListResponse({required this.items});

  factory AppointmentSlotListResponse.fromJson(Map<String, dynamic> json) => _$AppointmentSlotListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentSlotListResponseToJson(this);
}
