// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactResponse _$ContactResponseFromJson(Map<String, dynamic> json) =>
    ContactResponse(
      identifier: json['identifier'] as String,
      displayName: json['displayName'] as String,
      givenName: json['givenName'] as String,
      middleName: json['middleName'] as String,
      prefix: json['prefix'] as String,
      suffix: json['suffix'] as String,
      familyName: json['familyName'] as String,
      company: json['company'] as String,
      jobTitle: json['jobTitle'] as String,
      androidAccountTypeRaw: json['androidAccountTypeRaw'] as String,
      androidAccountName: json['androidAccountName'] as String,
      androidAccountType: json['androidAccountType'] as String,
      birthday: json['birthday'] as String,
    )
      ..emails = (json['emails'] as List<dynamic>)
          .map((e) => ItemResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..phones = (json['phones'] as List<dynamic>)
          .map((e) => ItemResponse.fromJson(e as Map<String, dynamic>))
          .toList()
      ..postalAddresses = (json['postalAddresses'] as List<dynamic>)
          .map((e) => PostalAddressResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ContactResponseToJson(ContactResponse instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'givenName': instance.givenName,
      'middleName': instance.middleName,
      'prefix': instance.prefix,
      'suffix': instance.suffix,
      'familyName': instance.familyName,
      'company': instance.company,
      'jobTitle': instance.jobTitle,
      'androidAccountTypeRaw': instance.androidAccountTypeRaw,
      'androidAccountName': instance.androidAccountName,
      'androidAccountType': instance.androidAccountType,
      'emails': instance.emails,
      'phones': instance.phones,
      'postalAddresses': instance.postalAddresses,
      'birthday': instance.birthday,
    };
