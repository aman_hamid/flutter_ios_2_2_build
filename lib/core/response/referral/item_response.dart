import 'package:json_annotation/json_annotation.dart';
part 'item_response.g.dart';

@JsonSerializable()
class ItemResponse {
  String label, value;

  ItemResponse({required this.label, required this.value});

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'label': this.label,
        'value': this.value,
      };

  factory ItemResponse.fromJson(Map<String, dynamic> json) => _$ItemResponseFromJson(json);
}
