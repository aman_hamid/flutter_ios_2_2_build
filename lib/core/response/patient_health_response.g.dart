// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_health_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientHealthResponse _$PatientHealthResponseFromJson(
        Map<String, dynamic> json) =>
    PatientHealthResponse(
      healthInfo: json['healthInfo'] == null
          ? null
          : HealthInfo.fromJson(json['healthInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PatientHealthResponseToJson(
        PatientHealthResponse instance) =>
    <String, dynamic>{
      'healthInfo': instance.healthInfo,
    };
