import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/insurance.dart';

part 'insurance_response.g.dart';

@JsonSerializable()
class InsuranceResponse {
  Insurance? insurance;

  InsuranceResponse({required this.insurance});

  factory InsuranceResponse.fromJson(Map<String, dynamic> json) => _$InsuranceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InsuranceResponseToJson(this);
}
