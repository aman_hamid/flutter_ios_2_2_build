import 'dart:async';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/about_you_request.dart';
import 'package:pocketpills/core/request/add_phone_request.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/setpassword_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signedin_verify_response.dart';
import 'package:pocketpills/core/response/signup/EDDresponse.dart';
import 'package:pocketpills/core/response/signup/signupEDDitem.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:sms_autofill/sms_autofill.dart';

class SignUpAboutYouModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();
  ConnectivityResult? connectivityResult;
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<bool?> _memoizer = AsyncMemoizer();
  List<SignupEddItem> eddList = [];

  SignUpAboutYouModel() {
    startTimer();
  }

  Timer? timer;
  int start = 30;
  bool stopTimer = false;

  int get getStartTime => start;

  void setStartTime(int value) {
    this.start = value;
  }

  Future<bool> updateAboutYou(
      {String? province,
      bool? selfMedication,
      bool? isCaregiver,
      bool? pocketPacks,
      String? password,
      String? gender,
      String? email,
      String? referral,
      String? phone,
      String? latestPrescriptionDeliveryBy}) async {
    setState(ViewState.Busy);
    AboutYouRequest abtRequest = AboutYouRequest(
        province: province != null ? province.toUpperCase() : province,
        hasDailyMedication: selfMedication,
        isCaregiver: isCaregiver,
        pocketPacks: pocketPacks,
        gender: gender,
        password: password,
        email: email,
        invitationCode: referral,
        referralCode: referral,
        latestPrescriptionDeliveryBy: latestPrescriptionDeliveryBy,
        phone: phone);
    var response = await _api.updateUserInfo(RequestWrapper(body: abtRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return await loginService.registerUser(response);
  }

  Future<bool> getVerifyRegistrationFlow(String identifier, String otps) async {
    Response response = await getVerifyRegistration(identifier, otps, true);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await handleRegistrationResponse(response, identifier);
  }

  Future<bool> reSignupOtpVerify(String identifier, String otps) async {
    Response response = await getVerifyReRegistration(identifier, otps);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await handleRegistrationResponse(response, identifier);
  }

  Future<SignedinVerifyResponse?> otpVerifyIdentifier(String userIdentifier, String otps, bool isRegistration) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    Response response = await _api.otpVerifyIdentifier(RequestWrapper(body: deviceBody), userIdentifier, otps, isRegistration.toString());
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    return await handleSignedInResponse(response, userIdentifier);
  }

  Future<bool> setPassword(String password) async {
    setState(ViewState.Busy);
    String? setToken = dataStore.readString(DataStoreService.SET_PASSWORD_TOKEN);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    var response = await _api.setPassword(RequestWrapper(body: SetPasswordRequest(password: password, token: setToken, loginBody: deviceBody)));
    setState(ViewState.Idle);
    try {
      await dataStore.saveAuthorizationToken(response.headers['Authorization']![0]);
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
    return handleLoginResponse(response);
  }

  bool handleLoginResponse(Response response) {
    if (response != null) {
      BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);

      return true;
    } else {
      return false;
    }
  }

  Future<bool?> getUserInfo({bool listenable = false}) async {
    return this._memoizer.runOnce(() async {
      listenable ? setState(ViewState.Busy) : null;
      var response = await _api.getUserInfo();
      listenable ? setState(ViewState.Idle) : null;
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
        OldBaseResponse res = OldBaseResponse.fromJson(response.data);
        BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
        signUpResponse = resSignUp.response!;
        return await loginService.registerUser(response);
    });
  }

  Future<bool?> getEDDList(String postalcode, String province) async {
    Response response = await _api.getEDDlist(province, postalcode);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<EDDresponse> res = BaseResponse<EDDresponse>.fromJson(response.data);
      eddList = res.response!.items;
      return true;
  }

  Future<Response> getVerifyRegistration(String identifier, String otps, bool isRegistration) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    int? userid = dataStore.getUserId();
    await deviceBody.init();
    Response response = await _api.getVerifyRegistrationFlowIdentifier(RequestWrapper(body: deviceBody), identifier, otps, isRegistration.toString(), userid!);
    setState(ViewState.Idle);
    return response;
  }

  Future<Response> getVerifyReRegistration(String identifier, String otps) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    Response response = await _api.getVerifyReRegistrationIdentifier(
      RequestWrapper(body: deviceBody),
      identifier,
      otps,
    );
    setState(ViewState.Idle);
    return response;
  }

  Future<bool> handleRegistrationResponse(response, String phoneNo) async {
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    signUpResponse = resSignUp.response!;
    return await loginService.registerUser(response);
  }

  Future<SignedinVerifyResponse?> handleSignedInResponse(response, String userIdentifier) async {
    BaseResponse<SignedinVerifyResponse> res = BaseResponse<SignedinVerifyResponse>.fromJson(response.data);
    if (!res.status!) {
      errorMessage = res.getErrorMessage();
      return null;
    }
    await dataStore.writeString(DataStoreService.SET_PASSWORD_TOKEN, res.response!.setPasswordToken);
    await dataStore.saveResetToken(res.response!.resetKey);
    if (StringUtils.isNumericUsingRegularExpression(userIdentifier)) {
      await dataStore.writeString(DataStoreService.PHONE, userIdentifier);
    } else {
      await dataStore.writeString(DataStoreService.EMAIL, userIdentifier);
    }
    return res.response;
  }

  Future<bool> getOtp(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.getOtp(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return true;
  }

  Future<bool> getOtpByIdentifier(String userIdentifier) async {
    setState(ViewState.Busy);
    Response response = await _api.getOtpByUserIdentifier(userIdentifier);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return true;
  }

  Future<bool> resendOtp(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    var response = await _api.resendOtp(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);

    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return true;
  }

  Future<bool> resendOtpByIdentifier(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    var response = await _api.resendOtpByIdentifier(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);

    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return true;
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (Timer timer) {
      if (start < 1 || stopTimer == true) {
        timer.cancel();
      } else {
        start--;
      }
    });
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
