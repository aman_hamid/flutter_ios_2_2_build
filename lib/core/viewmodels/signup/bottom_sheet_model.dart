import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/transfer/transfer_search_selection_model.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_member_transfer_prescription_request.dart';
import 'package:pocketpills/core/request/add_prescription_flow_request.dart';
import 'package:pocketpills/core/request/appointment_time_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup/add_province_request.dart';
import 'package:pocketpills/core/request/signup_transfer_prescription_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request_skip.dart';
import 'package:pocketpills/core/request/transfer_prescription_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/popular_pharmacy.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/prescription_create_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/core/request/telehealth_province_request.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:uuid/uuid.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';

class BottomSheetModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();


  List<TelehealthProvinceItem> provinceArrayList = [];

  late Future<List<TelehealthProvinceItem>?> provinceArrayListMemo;

  ConnectivityResult? connectivityResult;

  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();

  late bool isDialogDisplayed;
  List<Prescription>? prescriptionList;
  bool revenue = false;
  int? totalPrescriptionsCount;

  Future<List<TelehealthProvinceItem>?> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceArrayListMemo = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceArrayListMemo;
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
    List<TelehealthProvinceItem> listItems = [];
    for(int i =0 ; i< res.response!.items.length;i++){
      if(res.response!.items[i].defaultFlowEnabled == true){
        listItems.add(res.response!.items[i]);
      }
    }
    provinceArrayList = listItems;
    updateProvinceSlotDetails(provinceArrayList);
    return listItems;
  }

  void updateProvinceSlotDetails(List<TelehealthProvinceItem> provinceArrayList) {
    final DataStoreService dataStore = locator<DataStoreService>();
    if(dataStore.readString(DataStoreService.PROVINCE)!=null && dataStore.readString(DataStoreService.PROVINCE).toString().isNotEmpty) {
      for (int i = 0; i < provinceArrayList.length; i++) {
        TelehealthProvinceItem item = provinceArrayList[i];
        if (item.value.toLowerCase() == dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_")) {
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, item.appointmentSlotsEnabled!);
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, item.showHealthCardScreen!);
          break;
        }
      }
    }
  }



  Future<bool> sendPreferencesSignUp(TelehealthSignUpPreferenceRequest? telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    prescriptionList = resSignUp.response!.prescriptionList;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> sendPreferencesSignUpSkip(TelehealthSignUpPreferenceRequestSkip? telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    prescriptionList = resSignUp.response!.prescriptionList;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> skipTransferSignUp() async {
    // int prescriptionId = await _getPrescriptionId();
    SignupTransferPrescriptionRequest requestSignUp = SignupTransferPrescriptionRequest(
        type: PrescriptionTypeConstant.TRANSFER, pharmacyName: null, pharmacyAddress: null, pharmacyPhone: null, isTransferAll: false, prescriptionState: "PENDING");
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: requestSignUp));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }

    setState(ViewState.Idle);
    return true;
  }


   Future<bool> updateProvince(String province) async {
    AddProvinceRequest addProvinceRequest = AddProvinceRequest(province: province.toUpperCase(),signupFlow: "REGULAR");
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }

  Future<bool> telehealthProvince(String province) async {
    TelehealthProvinceRequest addProvinceRequest = TelehealthProvinceRequest(province: province.toUpperCase().replaceAll(" ", "_"), signupFlow: "TELEHEALTH");
    setState(ViewState.Busy);
    var response = await _api.telehealthProvinceAdd(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }


  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        Response? response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  Future<bool> updatePrescriptionFlow(String? prescriptionFlow) async {
    AddPrescriptionRequest request = AddPrescriptionRequest(prescriptionFlow: prescriptionFlow.toString());
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }
}
