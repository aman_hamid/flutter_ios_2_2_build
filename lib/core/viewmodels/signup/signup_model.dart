import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_prescription_flow_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/insurance/insurance_code_activation_details_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/response/phone_verify_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/services/signup_service.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/signup/customised_signup.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/sign_up_stepper.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_email_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SignUpModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final SignupService service = locator<SignupService>();
  final LoginService loginService = locator<LoginService>();
  static final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<bool> _memoizer = AsyncMemoizer();
  AsyncMemoizer<bool> _reMemoizer = AsyncMemoizer();
  AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();
  late Future<List<TelehealthProvinceItem>?> provinceTelehealthList;
  List<TelehealthProvinceItem> provinceArrayList = [];

  EmployerSuggestionResponse? employerSuggestionResponse;
  InsuranceCodeActivationDetailsResponse? insuranceCodeActivationDetailsResponse;
  ConnectivityResult? connectivityResult;

  HttpApiUtils? httpApiUtils;
  bool quebecCheck = false;
  late String rowValue;
  String discountText = "";

  bool isProvinceOntario = false;
  bool provinceAgeCheck = false;
  bool errorState = false;

  SignUpModel({this.httpApiUtils}) {
    this.httpApiUtils = httpApiUtils;
    fetchPrescriptProvinceList();
    getProvinceName();
  }

  clearAsyncMemoizer() {
    _memoizer = AsyncMemoizer();
    _reMemoizer = AsyncMemoizer();
  }

  cleanReMemoizer() {
    _reMemoizer = AsyncMemoizer();
  }

  void getProvinceName() async {
    return this._geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          String? subdivisionName = await httpApiUtils!.getSubdivisionName();
          checkIsProvinceOntario(subdivisionName!);
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
    });
  }

  void checkIsProvinceOntario(String subdivisionName) {
    if (subdivisionName.toLowerCase().contains(ViewConstants.ONTARIO)) {
      isProvinceOntario = true;
    }
  }

  Future<bool> signup({
    String? firstName,
    String? lastName,
    String? date,
    String? month,
    String? year,
  }) async {
    SignupRequest signupRequest = SignupRequest(firstName: firstName, lastName: lastName, dob: year! + "-" + month! + "-" + date!);
    return await getSignupRequest(signupRequest);
  }

  Future<bool> getSignupRequest(SignupRequest signupRequest) async {
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: signupRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }

  Future<bool> signupWithoutPhoneNumber({
    String? firstName,
    String? lastName,
    String? date,
    String? month,
    String? year,
    String? gender,
  }) async {
    SignupRequest signupRequest = SignupRequest(firstName: firstName, lastName: lastName, dob: year! + "-" + month! + "-" + date!, gender: gender);
    return await getSignupRequestWithoutPhoneNumber(signupRequest);
  }

  Future<bool> getSignupRequestWithoutPhoneNumber(SignupRequest signupRequest) async {
    setState(ViewState.Busy);
    var response = await _api.signup(RequestWrapper(body: signupRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }

  markSummarySeen() {
    _api.updateUserMarkSummary();
  }

  Future<PhoneVerification?> verifyIdentifier(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.checkUserIdentifier(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    PhoneVerifyResponse res = PhoneVerifyResponse.fromJson(response.data);
    return res.response;
  }

  Future<bool> getUserInfo({bool listenable = false}) async {
    return this._memoizer.runOnce(() async {
      listenable ? setState(ViewState.Busy) : null;
      Response response = await _api.getUserInfo();
      listenable ? setState(ViewState.Idle) : null;
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      BaseResponse<SignupResponse?> resSignUp = BaseResponse<SignupResponse?>.fromJson(response.data);
      signUpResponse = resSignUp.response!;
      return await loginService.registerUser(response);
    });
  }

  Future<bool> getReSignup(String userIdentifier, {bool listenable = false}) async {
    return this._reMemoizer.runOnce(() async {
      var response = await _api.reSignUp(userIdentifier);
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return true;
    });
  }

  Widget? getSignUpFlow2(BuildContext context) {
    SignupResponse? data = signUpResponse;
    TransferPrescription? prescription = data!.prescription;
    SignupDto signUpDetails = data.signupDto!;

    String? signupFlow;
    if (signUpDetails.signupFlow == "TELEHEALTH" || signUpDetails.prescriptionFlow != null && signUpDetails.prescriptionFlow == "TELEHEALTH") {
      signupFlow = "TELEHEALTH";
    } else if (signUpDetails.signupFlow == "TRANSFER" || signUpDetails.prescriptionFlow != null && signUpDetails.prescriptionFlow == "TRANSFER") {
      signupFlow = "TRANSFER";
    } else if (signUpDetails.signupFlow == "UPLOAD" || signUpDetails.prescriptionFlow != null && signUpDetails.prescriptionFlow == "UPLOAD") {
      signupFlow = "UPLOAD";
    } else {
      signupFlow = signUpDetails.signupFlow;
    }

    if (["", null].contains(signUpDetails.firstName) || ["", null].contains(signUpDetails.lastName) || ["", null].contains(signUpDetails.birthDate)) {
      Navigator.pushNamed(context, SignupWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (signupFlow != null && prescription == null && signUpDetails.prescriptionFlow == null) {
      Navigator.pushNamed(context, CustomisedSignUpWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (signupFlow != null && signupFlow == "TRANSFER" && prescription == null) {
      Navigator.pushNamed(context, TransferWidget.routeName, arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
    } else if (signupFlow != null && signupFlow == "UPLOAD" && prescription == null) {
      Navigator.pushNamed(context, UploadPrescription.routeName, arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
    } else if (signupFlow != null && signupFlow == "TELEHEALTH" && prescription == null) {
      Navigator.pushNamed(context, TelehealthPreference.routeName, arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null && signupFlow == "TELEHEALTH" && prescription != null && prescription.prescriptionState == "INCOMPLETE") {
      Navigator.pushNamed(context, TelehealthPreference.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth", bookingId: prescription.id!));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.type == "TELEHEALTH" &&
        prescription.prescriptionState == "INCOMPLETE" &&
        prescription.appointmentTime == null &&
        dataStore.readBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED) == true &&
        signUpDetails.hasHealthCard == false &&
        signUpDetails.phn == null) {
      Navigator.pushNamed(context, TelehealthPreference.routeName, arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.appointmentTime == null &&
        dataStore.readBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED) == false &&
        prescription.type == "TELEHEALTH" &&
        prescription.prescriptionState == "FILED" &&
        signUpDetails.hasHealthCard == false &&
        signUpDetails.phn == null) {
      dataStore.writeString(DataStoreService.PROVINCE, signUpDetails.province!);
      dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, signUpDetails.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.appointmentTime != null &&
        prescription.prescriptionState == "FILED" &&
        prescription.type == "TELEHEALTH" &&
        signUpDetails.hasHealthCard == false &&
        signUpDetails.phn == null) {
      dataStore.writeString(DataStoreService.PROVINCE, signUpDetails.province!);
      dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, signUpDetails.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.appointmentTime != null &&
        prescription.type == "TELEHEALTH" &&
        prescription.prescriptionState == "FILED" &&
        signUpDetails.hasHealthCard == true &&
        signUpDetails.phn == null &&
        signUpDetails.patientAddress == null) {
      dataStore.writeString(DataStoreService.PROVINCE, signUpDetails.province!);
      dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, signUpDetails.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.appointmentTime != null &&
        prescription.type == "TELEHEALTH" &&
        prescription.prescriptionState == "FILED" &&
        signUpDetails.hasHealthCard == false &&
        signUpDetails.phn != null &&
        signUpDetails.patientAddress == null) {
      dataStore.writeString(DataStoreService.PROVINCE, signUpDetails.province!);
      dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, signUpDetails.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signupFlow != null &&
        signupFlow == "TELEHEALTH" &&
        prescription != null &&
        prescription.appointmentTime != null &&
        prescription.prescriptionState == "FILED" &&
        prescription.type == "TELEHEALTH" &&
        signUpDetails.hasHealthCard == true &&
        signUpDetails.phn != null &&
        signUpDetails.patientAddress == null) {
      dataStore.writeString(DataStoreService.PROVINCE, signUpDetails.province!);
      dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, signUpDetails.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
          arguments: TelehealthArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else if (signUpDetails.signUpType == "PHONE_BASED" &&
        (["", null].contains(signUpDetails.email) &&
            !["", null].contains(signUpDetails.province) &&
            (["", null].contains(signUpDetails.gender) &&
                prescription != null &&
                prescription.type == "TELEHEALTH" &&
                prescription.prescriptionState == "FILED"))) {
      Navigator.pushNamed(context, SignUpOtpEmailWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (signUpDetails.signUpType == "EMAIL_BASED" &&
        (["", null].contains(signUpDetails.phone) ||
            ["", null].contains(signUpDetails.province) ||
            (["", null].contains(signUpDetails.gender)) &&
                prescription != null &&
                prescription.type != "TELEHEALTH" &&
                prescription.prescriptionState == "FILED")) {
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: dataStore.readString(DataStoreService.PRESCRIPTION_TYPE) ?? ""));
    } else if (signUpDetails.signUpType == "PHONE_BASED" &&
        (["", null].contains(signUpDetails.email) || ["", null].contains(signUpDetails.province) || (["", null].contains(signUpDetails.gender))) &&
        prescription != null &&
        prescription.type != "TELEHEALTH" &&
        prescription.prescriptionState == "FILED") {
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: dataStore.readString(DataStoreService.PRESCRIPTION_TYPE) ?? ""));
    } else if (signUpDetails.signUpType == "PHONE_BASED" &&
        (["", null].contains(signUpDetails.email) || ["", null].contains(signUpDetails.province) || (["", null].contains(signUpDetails.gender))) &&
        prescription != null &&
        prescription.type == "TRANSFER" &&
        prescription.prescriptionState == "PENDING") {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "add others"));
    } else if (signUpDetails.signUpType == "PHONE_BASED" &&
        (["", null].contains(signUpDetails.email) || ["", null].contains(signUpDetails.province) || (["", null].contains(signUpDetails.gender))) &&
        prescription != null &&
        prescription.type == "TELEHEALTH" &&
        prescription.prescriptionState == "PENDING") {
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: dataStore.readString(DataStoreService.PRESCRIPTION_TYPE) ?? ""));
    } else if ((signUpDetails.signUpType == "PHONE_BASED" || signUpDetails.signUpType == "EMAIL_BASED") &&
        (["", null].contains(signUpDetails.email) || ["", null].contains(signUpDetails.province) || (["", null].contains(signUpDetails.gender))) &&
        signUpDetails.prescriptionFlow == "NONE" &&
        signUpDetails.signupFlow == "REGULAR") {
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "add others"));
    } else {
      handleLoginSuccess(context);
      Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
    }
  }

  setValue(String value) {
    rowValue = value;
    notifyListeners();
  }

  void setErrorState(bool param) {
    errorState = param;
    notifyListeners();
  }

  Future<bool> getSuggestedEmployers() async {
    setState(ViewState.Busy);
    Response response = await _api.getSuggestedEmployers();
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<EmployerSuggestionResponse> res = BaseResponse<EmployerSuggestionResponse>.fromJson(response.data);
    employerSuggestionResponse = res.response;
    notifyListeners();
    return true;
  }

  Future<bool> updateInvitationCode(String invitationCode) async {
    setState(ViewState.Busy);
    var response = await _api.activateInvitationCode(invitationCode);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<InsuranceCodeActivationDetailsResponse> res = BaseResponse<InsuranceCodeActivationDetailsResponse>.fromJson(response.data);
    if (res.response == null || res.response!.insuranceCodeActivationDetails.updated == null || res.response!.insuranceCodeActivationDetails.updated == false) {
      return false;
    }
    insuranceCodeActivationDetailsResponse = res.response;
    return true;
  }

  handleLoginSuccess(BuildContext context) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.success_login);
    analyticsEvents.mixPanelIdentifier();
    Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
  }

  handleResetPasswordSuccess(BuildContext context) {
    handleLoginSuccess(context);
    clearAsyncMemoizer();
  }

  handleSignupSuccess(BuildContext context) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_otp_verification);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.signup);
    analyticsEvents.logCompleteRegistrationEvent();
    analyticsEvents.mixPanelIdentifier();
  }

  handleSetPasswordSuccess(BuildContext context) {
    handleSignupSuccess(context);
    Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
    clearAsyncMemoizer();
  }

  Future<bool> logout() async {
    setState(ViewState.Busy);
    Response response = await _api.logOut();
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    clearAsyncMemoizer();
    return await dataStore.logOut();
  }

  updateDeviceDetails() {
    _getDevicenfo();
    _initPackageInfo();
  }
  Future<void> _getDevicenfo() async {
    String _deviceId = "";
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _deviceId = androidInfo.androidId;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _deviceId = iosInfo.identifierForVendor;
    }
    dataStore.writeString(DataStoreService.DEVICE_ID, _deviceId);
  }
  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    dataStore.writeString(DataStoreService.APP_VERSION, info.version);
  }

  checkProvinceDiscount(String month, String date, String year) {
    if (date.isNotEmpty && month.isNotEmpty && year.isNotEmpty && isProvinceOntario == true) {
      final birthdayDate = DateTime(int.parse(year), int.parse(month), int.parse(date));
      final currentDate = DateTime.now();
      final difference = currentDate.difference(birthdayDate).inDays;
      if (difference < SignUpModuleConstant.daysInTwentyFiveYear) {
        provinceAgeCheck = true;
        discountText = LocalizationUtils.getSingleValueString("signup", "signup.about.help-provincetext");
        notifyListeners();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.show_ontario_discount_text);
        return true;
      }
      if (difference >= SignUpModuleConstant.daysInSixtyFiveYear && difference <= SignUpModuleConstant.daysInHundredYear) {
        provinceAgeCheck = true;
        discountText = LocalizationUtils.getSingleValueString("signup", "signup.about.help-provincetext-65");
        notifyListeners();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.show_ontario_discount_text);
        return true;
      } else {
        provinceAgeCheck = false;
        notifyListeners();
        return false;
      }
    } else {
      provinceAgeCheck = false;
      notifyListeners();
      return false;
    }
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  Future<List<TelehealthProvinceItem>?> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceTelehealthList = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceTelehealthList;
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
    List<TelehealthProvinceItem> listItems = [];
    for (int i = 0; i < res.response!.items.length; i++) {
      if (res.response!.items[i].defaultFlowEnabled == true) {
        listItems.add(res.response!.items[i]);
      }
    }
    provinceArrayList = listItems;
    updateProvinceSlotDetails(provinceArrayList);
    return listItems;
  }

  void updateProvinceSlotDetails(List<TelehealthProvinceItem> provinceArrayList) {
    final DataStoreService dataStore = locator<DataStoreService>();
    if (dataStore.readString(DataStoreService.PROVINCE) != null && dataStore.readString(DataStoreService.PROVINCE).toString().isNotEmpty) {
      for (int i = 0; i < provinceArrayList.length; i++) {
        TelehealthProvinceItem item = provinceArrayList[i];
        if (item.value.toLowerCase() == dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_")) {
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, item.appointmentSlotsEnabled!);
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, item.showHealthCardScreen!);
          break;
        }
      }
    }
  }

  Future<bool> updatePrescriptionFlow(String? prescriptionFlow) async {
    AddPrescriptionRequest request = AddPrescriptionRequest(prescriptionFlow: prescriptionFlow.toString());
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }
}
