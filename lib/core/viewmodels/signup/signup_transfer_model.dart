import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/transfer/transfer_search_selection_model.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_member_transfer_prescription_request.dart';
import 'package:pocketpills/core/request/add_others_request.dart';
import 'package:pocketpills/core/request/add_prescription_flow_request.dart';
import 'package:pocketpills/core/request/appointment_time_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup/add_province_request.dart';
import 'package:pocketpills/core/request/signup_transfer_prescription_request.dart';
import 'package:pocketpills/core/request/transfer_prescription_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/popular_pharmacy.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/prescription_create_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/core/request/telehealth_province_request.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:uuid/uuid.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';

class SignUpTransferModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();

  final places = new GoogleMapsPlaces(apiKey: ApplicationConstant.kGoogleApiKey);

  List<Pharmacy> pharmacyPredictions = [];
  List<Pharmacy> autoPlaces = [];
  List<Pharmacy> nearPlaces = [];
  List<TelehealthProvinceItem> provinceArrayList = [];
  List<String> popularPharmacyList = [];
  late Future<List<TelehealthProvinceItem>?> provinceArrayListMemo;
  File? frontImage;
  File? backImage;
  List<String> popularPharmacies = ["Shoppers drug mart", "Costco", "Rexall", "Walmart Pharmacy"];

  bool? _pharmacySubmitted = false;
  bool? searchNearBy = false;
  int? omsPrescriptionId;
  int? id;

  String? _pharmacyName;
  String? _pharmacyPhoneNumber;
  String? _pharmacyAddress;
  String? _postal_code;
  String? _province;
  String prevPharmacyName = "";
  Location? location;
  BaseStepperSource? source;
  SuccessDetails? successDetails;
  TransferSearchSelectModel? transferQuebecModel;
  int provinceValue = -1;
  int appointmentId = -1;
  String provinceClick = "";
  List<Component> components = [Component('country', 'ca')];
  ConnectivityResult? connectivityResult;

  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();

  late bool isDialogDisplayed;
  List<Prescription>? prescriptionList;
  bool revenue = false;
  int? totalPrescriptionsCount;

  SignUpTransferModel({BaseStepperSource? source = BaseStepperSource.UNKNOWN_SCREEN, this.searchNearBy, this.isDialogDisplayed = false}) {
    this.source = source;
    getIpAddress();
  }

  getIpAddress() async {
    try {
      location = await HttpApiUtils().getLocationUsingIp();
      _province = await HttpApiUtils().getSubdivisionName();
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
    searchNearBy == true ? await getPlaceSuggestionsList() : null;
  }

  void updateDialogDisplayed(bool val) {
    isDialogDisplayed = val;
    notifyListeners();
  }

  Future<List<TelehealthProvinceItem>?> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceArrayListMemo = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceArrayListMemo;
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
    List<TelehealthProvinceItem> listItems = [];
    for(int i =0 ; i< res.response!.items.length;i++){
      if(res.response!.items[i].defaultFlowEnabled == true){
        listItems.add(res.response!.items[i]);
      }
    }
    provinceArrayList = listItems;
    updateProvinceSlotDetails(provinceArrayList);
    return listItems;
  }

  void updateProvinceSlotDetails(List<TelehealthProvinceItem> provinceArrayList) {
    final DataStoreService dataStore = locator<DataStoreService>();
    if(dataStore.readString(DataStoreService.PROVINCE)!=null && dataStore.readString(DataStoreService.PROVINCE).toString().isNotEmpty) {
      for (int i = 0; i < provinceArrayList.length; i++) {
        TelehealthProvinceItem item = provinceArrayList[i];
        if (item.value.toLowerCase() == dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_")) {
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, item.appointmentSlotsEnabled!);
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, item.showHealthCardScreen!);
          break;
        }
      }
    }
  }

  Future<List<String>?> getPopularPharmacy() async {
    Response response = await _api.getPopularPharmacy(dataStore.readString(DataStoreService.GEO_IP_PROVINCE) ?? "", dataStore.readString(DataStoreService.GEO_IP_CITY) ?? "");
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    popularPharmacyList.clear();
    PopularPharmacy popularPharmacy = PopularPharmacy.fromJson(response.data);
    print(popularPharmacy.details);
    popularPharmacyList = popularPharmacy.details!;
    return popularPharmacyList;
  }

  Future<PlaceDetails> getPlaceDetails(String placeId) async {
    PlacesDetailsResponse response = await places.getDetailsByPlaceId(placeId);
    return response.result;
  }

  Future<void> getPlaceSuggestionsList() async {
    try {
      if (location != null) {
        PlacesSearchResponse response =
            await places.searchNearbyWithRankBy(location!, "distance", type: 'pharmacy', keyword: 'pharmacy', name: 'pharmacy', language: getSelectedLanguage());
        await handlePharmacyNearByResult(response);
      }

      notifyListeners();
    } catch (e) {
      FirebaseCrashlytics.instance.log(e.toString());
    }
  }

  getPlaceSuggestions(String searchKeyword) async {
    if (prevPharmacyName != searchKeyword) {
      prevPharmacyName = searchKeyword;
      if (pharmacySubmitted) {
        pharmacyPredictions.clear();
      } else {
        try {
          if (location != null) {
            PlacesSearchResponse response1 = await places.searchNearbyWithRankBy(location!, "distance", name: searchKeyword, type: 'pharmacy', language: getSelectedLanguage());
            PlacesAutocompleteResponse response2 = await places.autocomplete(searchKeyword, components: components, types: ['establishment']);
            await handlePharmacyNearByAutoComplete(response1, response2, searchKeyword.toLowerCase());
          } else {
            PlacesAutocompleteResponse response = await places.autocomplete(searchKeyword, components: components, types: ['establishment']);
            await handleAutoCompleteResult(response);
          }

          notifyListeners();
        } catch (e) {}
      }
    }
  }

  Future<void> handlePharmacyNearByResult(PlacesSearchResponse response) async {
    if (response != null && response.results != null && response.results.length > 0) {
      pharmacyPredictions.clear();
      response.results.forEach((value) {
        Pharmacy pharmacy = Pharmacy(pharmacyName: value.name, pharmacyAddress: value.vicinity!, pharmacyPlaceId: value.placeId);
        pharmacyPredictions.add(pharmacy);
      });
    }
    notifyListeners();
  }

  Future<void> handlePharmacyNearByAutoComplete(PlacesSearchResponse searchResponse, PlacesAutocompleteResponse autoCompleteResponse, String searchKey) async {
    pharmacyPredictions.clear();
    autoPlaces.clear();
    nearPlaces.clear();
    if (searchResponse != null && searchResponse.results != null && searchResponse.results.length > 0) {
      searchResponse.results.forEach((value) {
        Pharmacy pharmacy = Pharmacy(pharmacyName: value.name, pharmacyAddress: value.vicinity!, pharmacyPlaceId: value.placeId);
        nearPlaces.add(pharmacy);
      });

      nearPlaces.forEach((element) {
        int? searchIndex = -1;
        var list = element.pharmacyName.toString().split(" ");
        if (element.pharmacyName.toString().toLowerCase().startsWith(searchKey)) {
          searchIndex = 0;
        } else {
          for (int i = 0; i < list.length; i++) {
            if (list[i].toLowerCase().startsWith(searchKey)) {
              searchIndex = i;
              break;
            }
          }
        }
        Pharmacy pharmacy =
            Pharmacy(pharmacyName: element.pharmacyName, pharmacyAddress: element.pharmacyAddress, pharmacyPlaceId: element.pharmacyPlaceId, searchIndex: searchIndex);

        if (searchIndex != -1) {
          pharmacyPredictions.add(pharmacy);
        }
        if (pharmacyPredictions.length > 1) {
          pharmacyPredictions.sort((a, b) => a.searchIndex!.compareTo(b.searchIndex!));
        }
      });
    }

    if (autoCompleteResponse != null) {
      autoCompleteResponse.predictions.forEach((value) {
        if (value.types[0] == "pharmacy") {
          Pharmacy pharmacy =
              Pharmacy(pharmacyName: value.structuredFormatting!.mainText, pharmacyAddress: value.structuredFormatting!.secondaryText, pharmacyPlaceId: value.placeId!);
          autoPlaces.add(pharmacy);
        }
      });

      pharmacyPredictions.forEach((element) {
        for (int i = 0; i < autoPlaces.length; i++) {
          if (element.pharmacyPlaceId == autoPlaces[i].pharmacyPlaceId) {
            autoPlaces.removeAt(i);
            break;
          }
        }
      });
      pharmacyPredictions.addAll(autoPlaces);
    }
    notifyListeners();
  }

  Future<void> handleAutoCompleteResult(PlacesAutocompleteResponse response) async {
    if (response != null && response != null) {
      pharmacyPredictions.clear();
      response.predictions.forEach((value) {
        Pharmacy pharmacy =
            Pharmacy(pharmacyName: value.structuredFormatting!.mainText, pharmacyAddress: value.structuredFormatting!.secondaryText, pharmacyPlaceId: value.placeId!);
        pharmacyPredictions.add(pharmacy);
      });
    }
    notifyListeners();
  }

  Future<bool> uploadPrescription(
      {UserPatient? userPatient,
      String? placeId,
      String? pharmacyName,
      String? pharmacyAddress,
      int? pharmacyPhone,
      bool? isTransferAll,
      String? comments,
      bool? isSignUpFlow,
      bool? isPrescriptionEdited,
      String? prescriptionState,
      String? province,
      String? medicineName,
      double? quantity,
      int? omsPrescriptionId,
      int? id}) async {
    setState(ViewState.Busy);

    TransferPrescriptionRequest transferRequest = TransferPrescriptionRequest(
        pharmacyName: pharmacyName,
        pharmacyAddress: pharmacyAddress,
        placeId: placeId,
        pharmacyPhone: pharmacyPhone,
        isTransferAll: isTransferAll,
        comment: comments,
        prescriptionState: prescriptionState != null ? prescriptionState : 'FILED',
        prescriptionType: PrescriptionTypeConstant.TRANSFER,
        quantity: quantity,
        medicationName: medicineName);

    var response;

    if (isSignUpFlow == true) {
      SignupTransferPrescriptionRequest signupTransferRequest = SignupTransferPrescriptionRequest();
      signupTransferRequest.pharmacyName = pharmacyName;
      signupTransferRequest.pharmacyAddress = pharmacyAddress;
      signupTransferRequest.placeId = placeId;
      signupTransferRequest.pharmacyPhone = pharmacyPhone;
      signupTransferRequest.prescriptionComment = comments;
      signupTransferRequest.isTransferAll = isTransferAll;
      signupTransferRequest.prescriptionState = prescriptionState != null ? prescriptionState : 'FILED';
      signupTransferRequest.type = 'TRANSFER';
      if (isPrescriptionEdited != null && isPrescriptionEdited) {
        signupTransferRequest.id = id;
        signupTransferRequest.isPrescriptionEdited = true;
        signupTransferRequest.omsPrescriptionId = omsPrescriptionId;
      }

      if (province != null && province != "") {
        bool successUpdateProvince = await updateProvince(province);
        if (successUpdateProvince == true) {
          setState(ViewState.Idle);
          return await handleNewUserTransferRefill(signupTransferRequest);
        } else {
          setState(ViewState.Idle);
          FirebaseCrashlytics.instance.log("Province not updated in transfer refill flow");
          return false;
        }
      } else {
        setState(ViewState.Idle);
        return await handleNewUserTransferRefill(signupTransferRequest);
      }
    } else {
      int? prescriptionId = await _getPrescriptionId();
      response = await _api.updatePrescription(prescriptionId!, transferRequest);
      setState(ViewState.Idle);
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      BaseResponse<PrescriptionCreateResponse> baseResponse = BaseResponse<PrescriptionCreateResponse>.fromJson(response.data);
      totalPrescriptionsCount = baseResponse.response!.totalPrescriptionsCount;
      revenue = baseResponse.response!.revenue != null ? true : false;
      this.successDetails = baseResponse.response!.successDetails;
      GlobalVariable().prescriptionId = baseResponse.response!.omsPrescriptionId;
      GlobalVariable().userFlow = PrescriptionTypeConstant.TRANSFER;
      setState(ViewState.Idle);
      return true;
    }
  }

  Future<bool> uploadPrescriptionAdd(
      UserPatient? userPatient,
      String? placeId,
      String? pharmacyName,
      String? pharmacyAddress,
      int? pharmacyPhone,
      bool? isTransferAll,
      String? comments,
      bool? isSignUpFlow,
      String? prescriptionState,
      String? province,
      String? medicineName,
      num? quantity,
      int? id,
      bool? isPrescriptionEdited,
      int? omsPrescriptionId) async {
    setState(ViewState.Busy);

    TransferPrescriptionRequest transferRequest = TransferPrescriptionRequest(
        pharmacyName: pharmacyName,
        pharmacyAddress: pharmacyAddress,
        pharmacyPhone: pharmacyPhone,
        isTransferAll: isTransferAll,
        prescriptionState: prescriptionState != null ? prescriptionState : 'FILED',
        prescriptionType: PrescriptionTypeConstant.TRANSFER);

    var response;

    if (isSignUpFlow == true) {
      AddMemberTransferPrescriptionRequest signupTransferRequest = AddMemberTransferPrescriptionRequest();
      signupTransferRequest.pharmacyName = pharmacyName;
      signupTransferRequest.pharmacyAddress = pharmacyAddress;
      signupTransferRequest.pharmacyPhone = pharmacyPhone;
      signupTransferRequest.isTransferAll = isTransferAll;
      signupTransferRequest.prescriptionState = prescriptionState != null ? prescriptionState : 'FILED';
      signupTransferRequest.prescriptionType = PrescriptionTypeConstant.TRANSFER;

      if (province != null && province != "") {
        bool successUpdateProvince = await updateProvince(province);
        if (successUpdateProvince == true) {
          return await handleAddMemberTransferRefill(userPatient!.patientId!, signupTransferRequest);
        } else {
          FirebaseCrashlytics.instance.log("Province not updated in transfer refill flow");
          return false;
        }
      } else {
        return await handleAddMemberTransferRefill(userPatient!.patientId!, signupTransferRequest);
      }
    } else {
      int? prescriptionId = await _getPrescriptionId();
      response = await _api.updatePrescription(prescriptionId!, transferRequest);
      setState(ViewState.Idle);
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      BaseResponse<PrescriptionCreateResponse> baseResponse = BaseResponse<PrescriptionCreateResponse>.fromJson(response.data);
      this.successDetails = baseResponse.response!.successDetails;
      GlobalVariable().prescriptionId = baseResponse.response!.omsPrescriptionId;
      GlobalVariable().userFlow = PrescriptionTypeConstant.TRANSFER;
      return true;
    }
  }

  Future<bool> uploadPrescriptionSkip(
      UserPatient? userPatient,
      String? placeId,
      String? pharmacyName,
      String? pharmacyAddress,
      int? pharmacyPhone,
      bool? isTransferAll,
      String? comments,
      bool? isSignUpFlow,
      String? prescriptionState,
      String? province,
      String? medicineName,
      num? quantity,
      int? id,
      bool? isPrescriptionEdited,
      int? omsPrescriptionId) async {
    setState(ViewState.Busy);

    TransferPrescriptionRequest transferRequest = TransferPrescriptionRequest(
        pharmacyName: pharmacyName,
        pharmacyAddress: pharmacyAddress,
        pharmacyPhone: pharmacyPhone,
        isTransferAll: isTransferAll,
        prescriptionState: prescriptionState != null ? prescriptionState : 'PENDING',
        prescriptionType: PrescriptionTypeConstant.TRANSFER);

    var response;

    return await handleAddMemberTransferSkip(userPatient!.patientId!, transferRequest);
  }

  Future<bool> uploadPrescriptionSignUp() async {
    // int prescriptionId = await _getPrescriptionId();
    SignupTransferPrescriptionRequest requestSignUp = SignupTransferPrescriptionRequest(
        type: PrescriptionTypeConstant.UPLOAD, pharmacyName: null, pharmacyAddress: null, pharmacyPhone: null, isTransferAll: false, prescriptionState: "PENDING");
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: requestSignUp));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }

    setState(ViewState.Idle);
    return true;
  }

  Future<bool> sendPreferencesSignUp(TelehealthSignUpPreferenceRequest? telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    prescriptionList = resSignUp.response!.prescriptionList;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> skipTransferSignUp() async {
    // int prescriptionId = await _getPrescriptionId();
    SignupTransferPrescriptionRequest requestSignUp = SignupTransferPrescriptionRequest(
        type: PrescriptionTypeConstant.TRANSFER, pharmacyName: null, pharmacyAddress: null, pharmacyPhone: null, isTransferAll: false, prescriptionState: "PENDING");
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: requestSignUp));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }

    setState(ViewState.Idle);
    return true;
  }

  Future<bool> signupOthersFlow(String? prescriptionFlow,String signupFlow) async {
    AddOthersRequest request = AddOthersRequest(prescriptionFlow: prescriptionFlow.toString(),signupFlow: signupFlow);
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }

  /*Future<bool> sendPreferencesSignUp(TelehealthSignUpPreferenceRequest telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    setState(ViewState.Idle);
    if (response != null) {
    } else {
      return false;
    }
    setState(ViewState.Idle);
    return true;
  }*/

  Future<bool> handleNewUserTransferRefill(SignupTransferPrescriptionRequest signupTransferRequest) async {
    bool success = await newUserTransferRefill(signupTransferRequest);
    if (success == true) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> handleAddMemberTransferRefill(int patientId, AddMemberTransferPrescriptionRequest signupTransferRequest) async {
    bool success = await addMemberUserTransferRefill(patientId, signupTransferRequest);
    if (success == true) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> handleAddMemberTransferSkip(int patientId, TransferPrescriptionRequest signupTransferRequest) async {
    bool success = await addMemberUserTransferSkip(patientId, signupTransferRequest);
    if (success == true) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateProvince(String province) async {
    print("from Update hit");
    AddProvinceRequest addProvinceRequest = AddProvinceRequest(province: province.toUpperCase(),signupFlow: "REGULAR");
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }

  Future<bool> telehealthProvince(String province) async {
    print("from Update hit2");
    TelehealthProvinceRequest addProvinceRequest = TelehealthProvinceRequest(province: province.toUpperCase().replaceAll(" ", "_"), signupFlow: "TELEHEALTH");
    setState(ViewState.Busy);
    var response = await _api.telehealthProvinceAdd(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await loginService.registerUser(response);
  }

  Future<bool> newUserTransferRefill(SignupTransferPrescriptionRequest signupTransferRequest) async {
    Response response = await _api.addSignUpPrescription(RequestWrapper(body: signupTransferRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    appointmentId = resSignUp.response!.prescription!.id!;
    id = resSignUp.response!.prescription!.id!;
    omsPrescriptionId = resSignUp.response!.prescription!.omsPrescriptionId!;
    prescriptionList = resSignUp.response!.prescriptionList;
    totalPrescriptionsCount = prescriptionList!.length;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    return true;
  }

  Future<bool> addMemberUserTransferRefill(int patientId, AddMemberTransferPrescriptionRequest signupTransferRequest) async {
    Response response = await _api.addPrescription(patientId, RequestWrapper(body: signupTransferRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse oldBaseResponse = OldBaseResponse.fromJson(response.data);
    return true;
  }

  Future<bool> addMemberUserTransferSkip(int patientId, TransferPrescriptionRequest signupTransferRequest) async {
    Response response = await _api.addPrescription(patientId, RequestWrapper(body: signupTransferRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse oldBaseResponse = OldBaseResponse.fromJson(response.data);
    return true;
  }

  Future<int?> _getPrescriptionId() async {
    var response = await _api.getPrescriptionId(PrescriptionTypeConstant.TRANSFER);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    if (response != null && response.data != null && response.data['details'] != null && response.data['details']['prescriptionId'] != null)
      return response.data['details']['prescriptionId'];
    return null;
  }

  String generateRandomRequestId() {
    var uuid = new Uuid();
    return uuid.v4();
  }

  clearPharmacyInfo() {
    pharmacyAddress = "";
    pharmacyPhoneNumber = "";
    pharmacyName = "";
    _province = "";
    updateDialogDisplayed(false);
    notifyListeners();
  }

  set pharmacyName(String value) {
    _pharmacyName = value;
    notifyListeners();
  }

  set quebecModel(TransferSearchSelectModel selectedModel) {
    transferQuebecModel = selectedModel;
    notifyListeners();
  }

  String get province => _province!;

  set province(String value) {
    _province = value;
  }

  TransferSearchSelectModel get quebecSelected => transferQuebecModel ?? TransferSearchSelectModel();

  String get pharmacyName => _pharmacyName ?? "";

  String get pharmacyPhoneNumber => _pharmacyPhoneNumber ?? "";

  String get pharmacyAddress => _pharmacyAddress ?? "";

  String get postalCode => _postal_code ?? "";

  set pharmacyPhoneNumber(String value) {
    _pharmacyPhoneNumber = value;
    notifyListeners();
  }

  set pharmacyAddress(String value) {
    _pharmacyAddress = value;
    notifyListeners();
  }

  set postalCode(String value) {
    _postal_code = value;
    notifyListeners();
  }

  bool checkSignupFlow(BaseStepperSource source) {
    return source == BaseStepperSource.NEW_USER;
  }

  bool get pharmacySubmitted => _pharmacySubmitted!;

  set pharmacySubmitted(bool value) {
    _pharmacySubmitted = value;
    notifyListeners();
  }

  void setProvinceValue(int value) {
    provinceValue = value;
    notifyListeners();
  }

  void setProvincePop(String value) {
    provinceClick = value;
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        Response? response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  Future<bool> updatePrescriptionFlow(String? prescriptionFlow) async {
    AddPrescriptionRequest request = AddPrescriptionRequest(prescriptionFlow: prescriptionFlow.toString());
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }
}
