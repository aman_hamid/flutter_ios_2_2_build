import 'dart:async';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/about_you_request.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/setpassword_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signedin_verify_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/services/signup_service.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class VerificationModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final SignupService service = locator<SignupService>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  ConnectivityResult? connectivityResult;

  VerificationModel() {
    startTimer();
  }

  Timer? timer;
  int start = 30;
  bool stopTimer = false;

  int get getStartTime => start;

  void setStartTime(int value) {
    this.start = value;
  }

  Future<void> resendOtp(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    await _api.resendOtp(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);
  }

  Future<bool> resendOtpByIdentifierNew(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    var response = await _api.resendOtpByIdentifier(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);

    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return true;
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (Timer timer) {
      if (start < 1 || stopTimer == true) {
        timer.cancel();
      } else {
        start--;
      }
    });
  }

  Future<void> resendOtpByIdentifier(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    await _api.resendOtpByIdentifier(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);
  }

  Future<Response> otpVerify(String phoneNo, String otps, bool isRegistration) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    Response response = await _api.otpVerifyIdentifier(RequestWrapper(body: deviceBody), phoneNo, otps, isRegistration.toString());
    setState(ViewState.Idle);
    return response;
  }

  Future<Response> getVerifyRegistration(String phoneNo, String otps, bool isRegistration, int userID) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    Response response = await _api.getVerifyRegistrationFlow(RequestWrapper(body: deviceBody), phoneNo, otps, isRegistration.toString(), userID);
    setState(ViewState.Idle);
    return response;
  }

  Future<SignedinVerifyResponse?> getVerifyLoginFlow(String phoneNo, String otps) async {
    Response response = await otpVerify(phoneNo, otps, false);

    return await handleSignedInResponse(response, phoneNo);
  }

  Future<bool> getVerifyRegistrationFlow(String phoneNo, String otps, int userId) async {
    Response response = await getVerifyRegistration(phoneNo, otps, true, userId);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return await handleRegistrationResponse(response, phoneNo);
  }

  Future<SignedinVerifyResponse?> handleSignedInResponse(response, String phoneNo) async {
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<SignedinVerifyResponse> res = BaseResponse<SignedinVerifyResponse>.fromJson(response.data);
    await dataStore.writeString(DataStoreService.SET_PASSWORD_TOKEN, res.response!.setPasswordToken);
    await dataStore.saveResetToken(res.response!.resetKey);
    await dataStore.writeString(DataStoreService.PHONE, phoneNo);
    return res.response!;
  }

  Future<bool> setPassword(String password) async {
    setState(ViewState.Busy);
    String? setToken = dataStore.readString(DataStoreService.SET_PASSWORD_TOKEN);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    var response = await _api.setPassword(RequestWrapper(body: SetPasswordRequest(password: password, token: setToken, loginBody: deviceBody)));
    setState(ViewState.Idle);
    return handleLoginResponse(response);
  }

  Future<bool> handleLoginResponse(Response response) async {
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
      dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
      return await loginService.loginUser(response);
  }

  Future<bool> handleRegistrationResponse(response, String phoneNo) async {
    return await loginService.registerUser(response);
  }

  Future<bool> updateAboutYou(
      {String? province, bool? selfMedication, bool? isCaregiver, bool? pocketPacks, String? password, String? gender, String? email, String? referral, String? phone}) async {
    setState(ViewState.Busy);
    AboutYouRequest abtRequest = AboutYouRequest(
        province: province != null ? province.toUpperCase() : province,
        hasDailyMedication: selfMedication,
        isCaregiver: isCaregiver,
        pocketPacks: pocketPacks,
        gender: gender,
        password: password,
        email: email,
        invitationCode: referral,
        referralCode: referral,
        phone: phone);
    var response = await _api.updateUserInfo(RequestWrapper(body: abtRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return await loginService.registerUser(response);
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
