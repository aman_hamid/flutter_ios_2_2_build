import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/request/add_phone_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup/chambers_signup_body.dart';
import 'package:pocketpills/core/request/telehealth/chambers_active_body.dart';
import 'package:pocketpills/core/response/advertise/pp_distinct_entity_response.dart';
import 'package:pocketpills/core/response/auth_verify_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/chambers/chambers_activate_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/phone_verify_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/localization_list_response.dart';
import 'package:pocketpills/core/response/telehealth/localization_update_response.dart';
import 'package:pocketpills/core/response/version_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/views/start/authentication.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';

class LoginModel extends BaseModel {
  final LoginService loginService = locator<LoginService>();
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();

  AsyncMemoizer<VersionResponse?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<LocalizationUpdateResponse>?> contentUpdateMemoizer = AsyncMemoizer();

  ConnectivityResult? connectivityResult;
  bool quebecCheck = false;
  String? group;
  String? token;
  bool chambersWait = false;
  bool fireLandingEvent = true;

  Future<bool> login(String phoneNumber, String password) async {
    setState(ViewState.Busy);
    Response response = await loginService.login(phoneNumber, password);
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  LoginModel() {
    getProvinceName();
  }

  cleanMemorizer() {
    _memoizer = AsyncMemoizer();
    contentUpdateMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  void setPPDistinctId(Response response, DataStoreService dataStore) {
    BaseResponse<PPDistinctEntityResponse> baseResponse = BaseResponse<PPDistinctEntityResponse>.fromJson(response.data);
    if (baseResponse.status == true && baseResponse.response!.id != null) {
      dataStore.writeString(DataStoreService.PP_DISTINCT_ID, baseResponse.response!.id);
    }
  }

  Future<AuthVerification?> useGoogleAuthentication() async {
    Authentication.signOutGoogle();
    final List<dynamic> result = await Authentication.signInWithGoogle();
    return await _handleAuthenticationResponse(result, "GOOGLE");
  }

  Future<AuthVerification?> useAppleAuthentication() async {
    final List<dynamic> result = await Authentication.signInWithApple();
    return await _handleAuthenticationResponse(result, "APPLE");
  }

  Future<AuthVerification?> _handleAuthenticationResponse(List<dynamic> googleDetails, String authProvider) async {
    if (googleDetails != null) {
      print('authResult.Success:${googleDetails}');
      return checkAuthProvider(googleDetails, authProvider);
    } else {
      setState(ViewState.Idle);
      print('Authentication Failed: ');
      return null;
    }
  }

  Future<AuthVerification?> checkAuthProvider(List<dynamic> googleDetails, String authProvider) async {
    setState(ViewState.Busy);
    var response = await _api.checkAuthProvider(googleDetails, authProvider);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    print("ServerData - " + response.toString());
    AuthVerifyResponse res = AuthVerifyResponse.fromJson(response.data);
    try {
      await dataStore.saveAuthorizationToken(response.headers['Authorization']![0]);
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }

    if (res.response != null) {
      if (res.response!.userId != null) {
        await dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
      } else if (res.response!.user != null && res.response!.user!.id != null) {
        await dataStore.writeInt(DataStoreService.USERID, res.response!.user!.id!);
      }
    }
    await analyticsEvents.mixPanelIdentifier();
    return res.response;
  }

  Future<bool> updatePhoneYou(String? phone) async {
    AddPhoneRequest request = AddPhoneRequest(phone: phone.toString());
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }

  Future<PhoneVerification?> verifyPhone(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.checkUserIdentifier(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    PhoneVerifyResponse res = PhoneVerifyResponse.fromJson(response.data);
    await dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
    await dataStore.writeString(DataStoreService.PHONE, phoneNo);
    await analyticsEvents.mixPanelIdentifier();
    return res.response;
  }

  Future<bool> getUserInfo({bool listenable = false}) async {
    listenable ? setState(ViewState.Busy) : null;
    var response = await _api.getUserInfo();
    listenable ? setState(ViewState.Idle) : null;
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    signUpResponse = resSignUp.response!;
    return await loginService.registerUser(response);
  }

  Future<PhoneVerification?> checkUserIdentifier(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.checkUserIdentifier(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    PhoneVerifyResponse res = PhoneVerifyResponse.fromJson(response.data);
    if (res.response!.userId != null) {
      await dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
    }

    if (StringUtils.isEmail(phoneNo)) {
      await dataStore.writeString(DataStoreService.EMAIL, phoneNo);
    } else {
      await dataStore.writeString(DataStoreService.PHONE, phoneNo);
    }
    await analyticsEvents.mixPanelIdentifier();
    return res.response;
  }

  Future<PhoneVerification?> checkUserIdentifierAuth(String? userIdentifier, String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.checkUserIdentifierWithAuth(userIdentifier!, phoneNo);
    print("response phone" + response.toString());
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    PhoneVerifyResponse res = PhoneVerifyResponse.fromJson(response.data);
    if (res.response!.userId != null) {
      await dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
    }

    if (StringUtils.isEmail(phoneNo)) {
      await dataStore.writeString(DataStoreService.EMAIL, phoneNo);
    } else {
      await dataStore.writeString(DataStoreService.PHONE, phoneNo);
    }
    await analyticsEvents.mixPanelIdentifier();
    return res.response;
  }

  Future<SignupPhoneNumberResponse?> phoneNumberSignup(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.phoneNumberSignup(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<SignupPhoneNumberResponse> res = BaseResponse<SignupPhoneNumberResponse>.fromJson(response.data);
    await dataStore.writeInt(DataStoreService.USERID, res.response!.userId);
    loginService.registerUser(response);
    return res.response!;
  }

  Future<bool> getOtp(String phoneNo) async {
    setState(ViewState.Busy);
    await SmsAutoFill().listenForCode;
    var response = await _api.getOtp(phoneNo);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
    return true;
  }

  void getProvinceName() async {
    return this._geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          String? subdivisionName = await httpApiUtils.getSubdivisionName();
          quebecCheck = subdivisionName!.toLowerCase().contains(ViewConstants.QUEBEC) ? true : false;
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
    });
  }

  Future<bool> getOtpByIdentifier(String userIdentifier) async {
    setState(ViewState.Busy);
    await SmsAutoFill().listenForCode;
    var response = await _api.getOtpByUserIdentifier(userIdentifier);
    setState(ViewState.Idle);
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<VersionResponse?> onVersionCallCheck() async {
    return this._memoizer.runOnce(() async {
      PackageInfo version = await PackageInfo.fromPlatform();
      String platform = Platform.isAndroid ? "android" : "ios";
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      Response response = await _api.checkVersion(version.version, platform);
      if (response != null) {
        VersionResponse res = VersionResponse.fromJson(response.data);
        return res;
      }
      return null;
    });
  }

  Future<List<LocalizationUpdateResponse>?> checkContentEngineUpdates() async {
    return this.contentUpdateMemoizer.runOnce(() async {
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      Response response = await _api.getUpdatesContentEngine();
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return null;
      }
      BaseResponse<LocalizationListResponse> res = BaseResponse<LocalizationListResponse>.fromJson(response.data);
      var data = res.response;
      return LocalizationUtils.saveUpdateDate(data!.dataList);
    });
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  handleLoginSuccess(BuildContext context) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.success_login);
    analyticsEvents.mixPanelIdentifier();
    Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
  }

  Future<bool> getSignUpAuthRedirection() async {
    bool status;
    SignupResponse? data = signUpResponse;
    TransferPrescription prescription = data!.prescription!;
    SignupDto signUpDetails = data.signupDto!;
    if (signUpDetails.firstName == null ||
        signUpDetails.birthDate == null ||
        prescription == null ||
        signUpDetails.phone == null ||
        signUpDetails.province == null ||
        signUpDetails.gender == null) {
      status = false;
    } else {
      status = true;
    }

    return status;
  }
}
