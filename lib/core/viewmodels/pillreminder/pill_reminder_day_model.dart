import 'dart:convert';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/pillreminder/update_pill_reminder_status_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/month_wise_medication_response.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/views/pillreminder/component/month_picker_dialog.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PillReminderDayModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<DayWiseMedications?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<DayWiseMedications?> _futureDayWiseResponse;
  late DayWiseMedications dayWiseMedications;

  ConnectivityResult? connectivityResult;

  late DateTime _dateTime;

  PillReminderDayModel() {
    dateTime = DateTime.now();
  }

  Future<DayWiseMedications?> fetchPillReminderDayData() async {
    _futureDayWiseResponse = this._memoizer.runOnce(() async {
      return await this.getPillReminderDayData();
    });
    return _futureDayWiseResponse;
  }

  void cleanAsyncMemoizer() {
    _memoizer = AsyncMemoizer();
  }

  Future<DayWiseMedications?> getPillReminderDayData() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPillReminderDayWise(dateTime.day, dateTime.month, dateTime.year);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<DayWiseMedications> res = BaseResponse<DayWiseMedications>.fromJson(response.data);
    this.dayWiseMedications = res.response!;
    return res.response;
  }

  Future<bool> updatePillReminderStatus(PillStatusType statusType, List<int> pocketPacksIds) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return false;
    }

    UpdatePillReminderStatusRequest request = UpdatePillReminderStatusRequest(
        status: statusType == PillStatusType.TAKEN ? "TAKEN" : "MISSED",
        pocketPackIds: pocketPacksIds,
        day: dateTime.day,
        month: dateTime.month,
        year: dateTime.year);
    Response response = await _api.updatePillReminderStatus(new RequestWrapper(body: request));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<DayWiseMedications> res = BaseResponse<DayWiseMedications>.fromJson(response.data);
    this.dayWiseMedications = res.response!;
    notifyListeners();
    return true;
  }

  DateTime get dateTime => _dateTime;

  set dateTime(DateTime value) {
    _dateTime = value;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
