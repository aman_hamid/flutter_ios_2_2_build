import 'dart:convert';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/pillreminder/update_pill_reminder_status_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/month_wise_medication_response.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/views/pillreminder/component/month_picker_dialog.dart';

class PillReminderModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<MonthWiseMedicationResponse?> _memoizer = AsyncMemoizer();

  late int prevPatientId;
  bool _showDayView = false;

  Map<String, int> prevPatient = Map();

  late Future<MonthWiseMedicationResponse?> _futureMonthWiseResponse;
  late MonthWiseMedicationResponse monthWiseMedicationResponse;

  ConnectivityResult? connectivityResult;
  bool copyFaxNumber = false;

  late DateTime _dateTime;
  int beginMonthPadding = 0;
  late String formattedDate;
  final int numWeekDays = 7;

  late int _selectedDay;

  PillReminderModel() {
    dateTime = DateTime.now();
    formattedDate = getFormattedDateMMMYYYY(dateTime);
    setMonthPadding();
  }

  void onDateTapped(int day) {
    dateTime = new DateTime(_dateTime.year, _dateTime.month, day);
    selectedDay = day;
    notifyListeners();
  }

  Future<Null> selectDate(BuildContext context) async {
    showMonthPicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 1, 1),
      lastDate: DateTime(DateTime.now().year + 1, 12),
      initialDate: dateTime,
      locale: Locale("en"),
    ).then((date) {
      if (date != null) {
        dateTime = date;
        formattedDate = getFormattedDateMMMYYYY(dateTime);
        _memoizer = AsyncMemoizer();
        notifyListeners();
      }
    });
  }

  void setMonthPadding() {
    beginMonthPadding = new DateTime(dateTime.year, dateTime.month, 1).weekday;
    beginMonthPadding == 7 ? (beginMonthPadding = 0) : beginMonthPadding;
  }

  String getFormattedDateMMMYYYY(DateTime dateTime) {
    return DateFormat('MMM yyyy').format(dateTime);
  }

  String getFormattedDateDDMMMYYYY() {
    return DateFormat('MMM dd, EEEE').format(new DateTime(dateTime.year, dateTime.month, dateTime.day));
  }

  void goToToday() {
    print("trying to go to the month of today");
    dateTime = DateTime.now();
    setMonthPadding();
    notifyListeners();
  }

  void previousMonthSelected() {
    if (dateTime.year >= DateTime.now().year - 1) {
      if (dateTime.month == DateTime.january)
        dateTime = new DateTime(dateTime.year - 1, DateTime.december);
      else
        dateTime = new DateTime(dateTime.year, dateTime.month - 1);

      formattedDate = getFormattedDateMMMYYYY(dateTime);
      setMonthPadding();
      _memoizer = AsyncMemoizer();
      notifyListeners();
    }
  }

  void nextMonthSelected() {
    if (dateTime.year + 1 <= DateTime.now().year + 1) {
      if (dateTime.month == DateTime.december)
        dateTime = new DateTime(dateTime.year + 1, DateTime.january);
      else
        dateTime = new DateTime(dateTime.year, dateTime.month + 1);

      formattedDate = getFormattedDateMMMYYYY(dateTime);
      setMonthPadding();
      _memoizer = AsyncMemoizer();
      notifyListeners();
    }
  }

  void previousDaySelected() {
    if (selectedDay == 1) {
      dateTime = new DateTime(dateTime.year, dateTime.month - 1, getNumberOfDaysInMonth(dateTime.month - 1) - beginMonthPadding);
      _memoizer = AsyncMemoizer();
    } else
      dateTime = new DateTime(dateTime.year, dateTime.month, dateTime.day - 1);

    selectedDay = dateTime.day;
    formattedDate = getFormattedDateMMMYYYY(dateTime);
    setMonthPadding();
    notifyListeners();
  }

  void nextDaySelected() {
    if (selectedDay == getNumberOfDaysInMonth(dateTime.month)) {
      dateTime = new DateTime(dateTime.year, dateTime.month + 1, 1);
      _memoizer = AsyncMemoizer();
    } else
      dateTime = new DateTime(dateTime.year, dateTime.month, dateTime.day + 1);

    selectedDay = dateTime.day;
    formattedDate = getFormattedDateMMMYYYY(dateTime);
    setMonthPadding();
    notifyListeners();
  }

  Future<MonthWiseMedicationResponse?> fetchPillReminderData() async {
    _futureMonthWiseResponse = this._memoizer.runOnce(() async {
      return await this.getPillReminderMonthData();
    });
    return _futureMonthWiseResponse;
  }

  Future<MonthWiseMedicationResponse> fetchPillReminderLocalData(BuildContext context) async {
//    _futureMonthWiseResponse = this._memoizer.runOnce(() async {
    String data = await DefaultAssetBundle.of(context).loadString("graphics/pills.json");
    final jsonResult = json.decode(data);
    MonthWiseMedicationResponse monthWiseMedicationResponse = MonthWiseMedicationResponse.fromJson(jsonResult);
    return monthWiseMedicationResponse;
//    });
    // return _futureMonthWiseResponse;
  }

  clearData() {
    prevPatient = Map();
    this.copyFaxNumber = false;
    notifyListeners();
  }

  Future<MonthWiseMedicationResponse?> getPillReminderMonthData() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    setState(ViewState.Busy);
    Response response = await _api.getPillReminderMonthWise(dateTime.month, dateTime.year);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<MonthWiseMedicationResponse> res = BaseResponse<MonthWiseMedicationResponse>.fromJson(response.data);
      this.monthWiseMedicationResponse = res.response!;
      return res.response;

  }

  Future<bool?> updatePillReminderStatus(PillStatusType statusType, List<int> pocketPacksIds) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }

    UpdatePillReminderStatusRequest request = UpdatePillReminderStatusRequest(
        status: statusType == PillStatusType.TAKEN ? "TAKEN" : "MISSED", pocketPackIds: pocketPacksIds, day: dateTime.day, month: dateTime.month, year: dateTime.year);
    Response response = await _api.updatePillReminderStatus(new RequestWrapper(body: request));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<DayWiseMedications> res = BaseResponse<DayWiseMedications>.fromJson(response.data);
      monthWiseMedicationResponse.dayWiseMedications[selectedDay.toString()] = res.response!;
      notifyListeners();
      return true;
  }

  int getNumberOfDaysInMonth(final int month) {
    int numDays = 28;

    // Months are 1, ..., 12
    switch (month) {
      case 1:
        numDays = 31;
        break;
      case 2:
        numDays = 28;
        break;
      case 3:
        numDays = 31;
        break;
      case 4:
        numDays = 30;
        break;
      case 5:
        numDays = 31;
        break;
      case 6:
        numDays = 30;
        break;
      case 7:
        numDays = 31;
        break;
      case 8:
        numDays = 31;
        break;
      case 9:
        numDays = 30;
        break;
      case 10:
        numDays = 31;
        break;
      case 11:
        numDays = 30;
        break;
      case 12:
        numDays = 31;
        break;
      default:
        numDays = 28;
    }
    return numDays + beginMonthPadding;
  }

  String getMonthName(final int month) {
    switch (month) {
      case 1:
        return "January";
      case 2:
        return "February";
      case 3:
        return "March";
      case 4:
        return "April";
      case 5:
        return "May";
      case 6:
        return "June";
      case 7:
        return "July";
      case 8:
        return "August";
      case 9:
        return "September";
      case 10:
        return "October";
      case 11:
        return "November";
      case 12:
        return "December";
      default:
        return "Unknown";
    }
  }

  DateTime get dateTime => _dateTime;

  set dateTime(DateTime value) {
    DateTime currentDateTime = DateTime.now();
    _dateTime = value;
    if (value.month == currentDateTime.month && value.year == currentDateTime.year) {
      selectedDay = value.day;
      notifyListeners();
    } else {
      selectedDay = 1;
      notifyListeners();
    }
  }

  int get selectedDay => _selectedDay;

  set selectedDay(int value) {
    _selectedDay = value;
    notifyListeners();
  }

  bool get showDayView => _showDayView;

  set showDayView(bool value) {
    _showDayView = value;
    notifyListeners();
  }
}
