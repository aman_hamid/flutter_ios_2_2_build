import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/vitamins/item_group_filter_list.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_benefit_filter_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamin_filter_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';

class VitaminsCatalogFilterModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<List<ItemGroupFilterList>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<List<ItemGroupFilterList>?> _vitaminsCatalogFutureList;
  List<ItemGroupFilterList>? itemsGroupFilterList;
  ConnectivityResult? connectivityResult;
  List<int> filterId = [];

  String? itemId;

  VitaminsCatalogFilterModel({String? itemId = ""}) {
    this.itemId = itemId;
  }

  Future<List<ItemGroupFilterList>?> fetchVitaminsFilter() async {
    _vitaminsCatalogFutureList = this._memoizer.runOnce(() async {
      return await this.getVitaminsFilters();
    });
    return _vitaminsCatalogFutureList;
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<List<ItemGroupFilterList>?> getVitaminsFilters() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getVitaminsFilter();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<VitaminsBenefitFilterResponse> baseResponse = BaseResponse<VitaminsBenefitFilterResponse>.fromJson(response.data);
      setState(ViewState.Idle);
      itemsGroupFilterList = baseResponse.response!.itemGroupFilterList;
      if (itemId != null && itemId != "") {
        itemsGroupFilterList!.forEach((filter) => {
              if (filter.itemGroupName!.contains(itemId!)) {addMedicineToShoppingCart(filter)}
            });
      }

      return baseResponse.response!.itemGroupFilterList;
  }

  Future<void> addMedicineToShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_add_vitamin_category);
    itemGroupFilter.isInVitaminFilter = true;
    filterId.add(itemGroupFilter.id!);
    notifyListeners();
  }

  Future<void> removeMedicineFromShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_remove_vitamin_category);
    itemGroupFilter.isInVitaminFilter = false;
    filterId.remove(itemGroupFilter.id);
    notifyListeners();
  }

  onClickContinue(bool isShowAll) async {
    locator<NavigationService>()
        .navigateTo(VitaminsCatalogWidget.routeName, VitaminFilterArguments(source: BaseStepperSource.VITAMINS_SCREEN, filterArgu: isShowAll == true ? "" : filterId.join(",")));
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
