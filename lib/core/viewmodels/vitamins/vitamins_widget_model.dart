import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/vitamins/item_group_filter_list.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_subscription_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamin_filter_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_widget.dart';
import 'package:pocketpills/utils/navigation_service.dart';

class VitaminsWidgetModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<VitaminsSubscriptionResponse?> _memoizer = AsyncMemoizer();

  late Future<VitaminsSubscriptionResponse?> _vitaminsCatalogFutureList;
  List<ItemGroupFilterList>? itemsGroupFilterList;
  List<int> filterId = [];

  Future<VitaminsSubscriptionResponse?> fetchVitaminsSubscription() async {
    _vitaminsCatalogFutureList = this._memoizer.runOnce(() async {
      return await this.getVitaminsSubscription();
    });
    return _vitaminsCatalogFutureList;
  }

  Future<VitaminsSubscriptionResponse?> getVitaminsSubscription() async {
    Response response = await _api.getVitaminssubscriptionDetails();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<VitaminsSubscriptionResponse> baseResponse = BaseResponse<VitaminsSubscriptionResponse>.fromJson(response.data);
    return baseResponse.response;
  }

  Future<void> addMedicineToShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    itemGroupFilter.isInVitaminFilter = true;
    filterId.add(itemGroupFilter.id!);
    notifyListeners();
  }

  Future<void> removeMedicineFromShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    itemGroupFilter.isInVitaminFilter = false;
    filterId.remove(itemGroupFilter.id);
    notifyListeners();
  }

  onClickContinue(bool isShowAll) {
    locator<NavigationService>().navigateTo(VitaminsCatalogWidget.routeName,
        VitaminFilterArguments(source: BaseStepperSource.VITAMINS_SCREEN, filterArgu: isShowAll == true ? "" : filterId.join(",")));
  }
}
