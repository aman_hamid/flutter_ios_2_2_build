import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/vitamins/item_group_filter_list.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_subscription_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class VitaminsSubscriptionModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<VitaminsSubscriptionResponse?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<VitaminsSubscriptionResponse?> _vitaminsCatalogFutureList;

  VitaminsSubscriptionResponse? vitaminsSubscriptionResponse;

  ConnectivityResult? connectivityResult;

  List<int> filterId = [];

  Future<VitaminsSubscriptionResponse?> fetchVitaminsSubscription() async {
    _vitaminsCatalogFutureList = this._memoizer.runOnce(() async {
      return await this.getVitaminsSubscription();
    });
    return _vitaminsCatalogFutureList;
  }

  clearVitaminList() {
    _memoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<VitaminsSubscriptionResponse?> getVitaminsSubscription() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getVitaminssubscriptionDetails();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<VitaminsSubscriptionResponse> baseResponse = BaseResponse<VitaminsSubscriptionResponse>.fromJson(response.data);
      if (!baseResponse.status! || baseResponse.response == null) {
        errorMessage = baseResponse.getErrorMessage();
        return null;
      }
      vitaminsSubscriptionResponse = baseResponse.response;
      return baseResponse.response;
  }

  Future<void> addMedicineToShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    itemGroupFilter.isInVitaminFilter = true;
    filterId.add(itemGroupFilter.id!);
    notifyListeners();
  }

  Future<void> removeMedicineFromShoppingCart(ItemGroupFilterList itemGroupFilter) async {
    itemGroupFilter.isInVitaminFilter = false;
    filterId.remove(itemGroupFilter.id);
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
