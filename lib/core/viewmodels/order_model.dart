import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/order.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/order_list_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class OrderModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  List<Order>? orderList;
  int? _orderIndex = 0;
  int? orderId;
  Order? selectedOrder;
  AsyncMemoizer<List<Order>?> _memoizer = AsyncMemoizer();
  int? prevPatientId;
  late Future<List<Order>?> _futureOrderList;
  ConnectivityResult? connectivityResult;

  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  Future<List<Order>?> fetchOrders(int patientId) async {
    if (prevPatientId != null && prevPatientId == patientId) return _futureOrderList;
    this._memoizer = AsyncMemoizer();
    _futureOrderList = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      return await this.getOrders();
    });
    return _futureOrderList;
  }

  clearData() {
    prevPatientId = null;
    notifyListeners();
  }

  Order? getOrder() {
    Order? order;
    for (int i = 0; i < this.orderList!.length; i++) {
      if (this.orderList!.elementAt(i).id == this.orderId) {
        order = this.orderList!.elementAt(i);
        break;
      }
    }
    return order;
  }

  Future<List<Order>?> getOrders() async {
    Response response = await _api.getOrders();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      OMSResponse<OrderListResponse> res = OMSResponse<OrderListResponse>.fromJson(response.data);
      orderList = res.response!.orderList;
      return res.response!.orderList;
  }

  void setOrderIndex(int index) {
    _orderIndex = index;
  }

  void setOrder(Order order) {
    selectedOrder = order;
    orderId = order.id;
    notifyListeners();
  }

  void setOrderId(int orderId) {
    this.orderId = orderId;
    selectedOrder = getOrder();
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
