class GlobalVariable {
  static GlobalVariable? _instance;

  factory GlobalVariable() => _instance ??= new GlobalVariable._();

  GlobalVariable._();

  int? _prescriptionId;
  String? _userFlow;

  int get prescriptionId => _prescriptionId!;

  set prescriptionId(int? value) {
    _prescriptionId = value;
  }

  String get userFlow => _userFlow!;

  set userFlow(String value) {
    _userFlow = value;
  }

  String _healthCardNumber = "";
  String _gender = "";
  String _address1 = "";
  String _address2 = "";
  String _city = "";
  String _postalCode = "";
  String _healthCadExpiryDD = "";
  String _healthCadExpiryMM = "";
  String _healthCadExpiryYY = "";
  String _selectedEddDay = "";
  String _selectedProvince = "";
  String _emailId = "";
  String _phone = "";

  String get healthCardNumber => _healthCardNumber;

  set healthCardNumber(String value) {
    _healthCardNumber = value;
  }
  String get gender => _gender;

  set gender(String value) {
    _gender = value;
  }
  String get address1 => _address1;

  set address1(String value) {
    _address1 = value;
  }
  String get address2 => _address2;

  set address2(String value) {
    _address2 = value;
  }
  String get city => _city;

  set city(String value) {
    _city = value;
  }
  String get postalCode => _postalCode;

  set postalCode(String value) {
    _postalCode = value;
  }
  String get healthCadExpiryDD => _healthCadExpiryDD;

  set healthCadExpiryDD(String value) {
    _healthCadExpiryDD = value;
  }String get healthCadExpiryMM => _healthCadExpiryMM;

  set healthCadExpiryMM(String value) {
    _healthCadExpiryMM = value;
  }String get healthCadExpiryYY => _healthCadExpiryYY;

  set healthCadExpiryYY(String value) {
    _healthCadExpiryYY = value;
  }
  String get selectedEddDay => _selectedEddDay;

  set selectedEddDay(String value) {
    _selectedEddDay = value;
  }
  String get selectedProvince => _selectedProvince;

  set selectedProvince(String value) {
    _selectedProvince = value;
  }
  String get emailId => _emailId;

  set emailId(String value) {
    _emailId = value;
  }

  String get phone => _phone;

  set phone(String value) {
    _phone = value;
  }

}
