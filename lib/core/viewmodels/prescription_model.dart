import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/oms_prescription_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PrescriptionModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  List<Prescription>? prescriptionList;
  Prescription? prescription;
  final LoginService loginService = locator<LoginService>();
  AsyncMemoizer<bool?> _userMemoizer = AsyncMemoizer();

  int prescriptionIndex = 0;
  int? prevPatientId;

  AsyncMemoizer<List<Prescription>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<List<Prescription>?> _futurePrescriptionList;

  ConnectivityResult? connectivityResult;

  Future<List<Prescription>?> fetchPrescriptions(int patientId) async {
    if (prevPatientId != null && prevPatientId == patientId) return _futurePrescriptionList;
    this._memoizer = AsyncMemoizer();
    _futurePrescriptionList = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      return await this.getPrescriptions();
    });
    return _futurePrescriptionList;
  }

  clearData() {
    prevPatientId = null;
    notifyListeners();
  }

  Future<List<Prescription>?> getPrescriptions() async {
    Response response = await _api.getPrescriptions();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      OMSPrescriptionResponse res = OMSPrescriptionResponse.fromJson(response.data);
      prescriptionList = res.response!;
      return res.response;
  }

  void setPrescriptionIndex(int index) {
    prescriptionIndex = index;
  }


  Future<bool?> getUserInfo({bool listenable = false}) async {
    return this._userMemoizer.runOnce(() async {
      listenable ? setState(ViewState.Busy) : null;
      var response = await _api.getUserInfo();
      listenable ? setState(ViewState.Idle) : null;
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
      signUpResponse = resSignUp.response!;
      return await loginService.registerUser(response);
    });
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
