import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PrescriptionDashboardModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();

  AsyncMemoizer<Prescription?> _prescriptionDetailMemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  late Future<Prescription?> _futurePrescription;
  ConnectivityResult? connectivityResult;

  Future<Prescription?> fetchPrescriptionById(int prescriptionId) async {
    _futurePrescription = this._prescriptionDetailMemoizer.runOnce(() async {
      return await this.getPrescriptionsById(prescriptionId);
    });
    return _futurePrescription;
  }

  clearAsyncMemoizer() {
    _prescriptionDetailMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<Prescription?> getPrescriptionsById(int prescriptionId) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPrescriptionsById(prescriptionId);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      OMSResponse omsResponse = OMSResponse<Prescription>.fromJson(response.data);
      return omsResponse.response;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData!;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
