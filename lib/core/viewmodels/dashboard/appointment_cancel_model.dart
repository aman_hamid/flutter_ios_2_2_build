import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_doctor_time_slots.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/telehealth/cancel_appointment_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/response/appointment_slot_list_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_list_response.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class AppointmentCancelModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  ConnectivityResult? connectivityResult;
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  Future<bool> cancelAppointment(CancelAppointmentRequest cancelRequest, int prescriptionId) async {
    setState(ViewState.Busy);
    Response response = await _api.cancelAppointment(cancelRequest, prescriptionId);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    setState(ViewState.Idle);
    BaseResponse res = BaseResponse.fromJson(response.data);
    return res.status!;
  }

  Future<bool> cancelAppointment2(CancelAppointmentRequest cancelRequest, int prescriptionId) async {
    setState(ViewState.Busy);

    return true;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData!;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
