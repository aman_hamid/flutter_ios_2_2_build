import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_doctor_time_slots.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/telehealth/cancel_appointment_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/response/appointment_slot_list_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_list_response.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class AppointmentRescheduleModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  ConnectivityResult? connectivityResult;
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  List<AppointmentTime> appointmentsList = [];
  String? errMsg;

  //New Logic
  Map<String, AppointmentTime> time = Map<String, AppointmentTime>();
  String? selectedDate;
  int? currentItem;
  AsyncMemoizer<List<AppointmentTime>>? _appointmentMemoizer = AsyncMemoizer();
  Future<List<AppointmentTime>?>? _appointment;
  Map<int, int> appointListToActualIndex = Map<int, int>();
  Map<String, String> availableTimings = Map();

  //AppointmentTime defaultAppointment = AppointmentTime(displayDate: 'Choose a value');
  List<String> appointmentDisplayDates = [];
  List<String>? requestedMedication;
  bool showGrid = false;
  bool selected = false;
  String index = "-1";
  AppointmentTime? selectedItem;
  AppointmentDoctorSlots? appointmentDoctorSlots;
  List<TelehealthProvinceItem> provinceArrayList = [];

  late Future<List<TelehealthProvinceItem>?> provinceArrayListMemo;

  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();

  //List<PastAppointment>? pastAppointments;


  Future<List<AppointmentTime>?> getAppointmentDate() async {
    if (_appointment != null) return _appointment;

    _appointment = this._appointmentMemoizer!.runOnce(() async {
      Response response = await _api.getAppointmentSlots();
      if (response != null) {
        OMSResponse<AppointmentSlotListResponse> res = OMSResponse<AppointmentSlotListResponse>.fromJson(response.data);
        if (!res.status!) {
          errorMessage = res.getErrorMessage();
          errMsg = res.getErrorMessage();
          return [];
        }
        for (var items in res.response!.items) {
          if (items.available != null && items.available!) {
            appointmentsList.add(items);
            appointmentDisplayDates.add(items.displayDate!);
            time.putIfAbsent(items.displayDate!, () => items);
            appointListToActualIndex.putIfAbsent(items.index!, () => appointmentsList.length - 1);
          }
        }

        return res.response!.items;
      } else {
        return [];
      }
    });

    return _appointment;
  }


  clearAsyncMemoizer() {
    contentMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<bool> sendPreferences(TelehealthRequestUpdate telehealthRequest, int? prescriptionId) async {
    setState(ViewState.Busy);
    Response response = await _api.updateTelehealthAppointment(prescriptionId, telehealthRequest);
    setState(ViewState.Idle);
    if (response != null && response.data["data"] != null) {
      OMSResponse<TelehealthCreateResponse> baseResponse = OMSResponse<TelehealthCreateResponse>.fromJson(response.data);
      setState(ViewState.Idle);
      if (!baseResponse.status!) {
        errorMessage = baseResponse.getErrorMessage();
        return false;
      } else if (response.data["data"] == null) {
        errorMessage = baseResponse.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }


  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData!;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

}
