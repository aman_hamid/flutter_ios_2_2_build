import 'dart:convert';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/services/user_service.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/http_api_utils.dart';

class DashboardModel extends BaseModel {
  final DataStoreService dataStore = locator<DataStoreService>();
  UserService userService = locator<UserService>();
  int? _memberPatientId;
  int _dashboardIndex = 0;
  int showBottomBar = 1;
  List<int>? tabPosition = [];
  UserPatient? _selectedPatient;
  DayWiseMedications? _dayWiseMedications;
  bool _applicationStart = false;
  List<UserPatient>? _patientList;
  ConnectivityResult? connectivityResult;
  AsyncMemoizer<List<UserPatient>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();
  bool quebecCheck = false;
  String? pincode;
  String? province;
  DashboardModel() {
    getProvinceNameAndPincode();
  }
  Future<void> getProvinceNameAndPincode() async {
    return this._geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          List<String>? ipLocation = await httpApiUtils.getProvinceAndPostalCode();
          quebecCheck = ipLocation![0].toUpperCase().contains('QUEBEC') ? true : false;
          province = ipLocation[0];
          pincode = ipLocation[1];
          //notifyListeners();
        } catch (ex) {}
      }
    });
  }

  int get selectedPatientId {
    if (_selectedPatient != null)
      return _selectedPatient!.patientId!;
    else
      return dataStore.getPatientId()!;
  }

  UserPatient? getPatientDetails() {
    if (_selectedPatient != null)
      return _selectedPatient;
    else {
      String? userPatient = dataStore.readString(DataStoreService.SELECTED_PATIENT);
      if (userPatient == null) {
        return _selectedPatient;
      } else {
        Map<String, dynamic>? userMap = jsonDecode(userPatient);
        _selectedPatient = UserPatient.fromJson(userMap!);
        return _selectedPatient;
      }
    }
  }

  int get dashboardIndex => _dashboardIndex;
  set dashboardIndex(int value) {
    checknetworkConnectivity();
    _dashboardIndex = value;
    notifyListeners();
  }

  UserPatient? get selectedPatient => getPatientDetails();
  List<UserPatient>? get patientList => _patientList;
  void displayBottomBar() {
    showBottomBar = 1;
    notifyListeners();
  }

  void hideBottomBar() {
    showBottomBar = 0;
    notifyListeners();
  }

  setEmail(String emailID) async {
    if (selectedPatient != null) {
      selectedPatient!.patient!.email = emailID;
      notifyListeners();
    }
  }
  setGender(String gender) async {
    if (_selectedPatient != null && gender.isNotEmpty) {
      _selectedPatient!.patient!.gender = gender;
      notifyListeners();
    }
    if (selectedPatient != null) {
      selectedPatient!.patient!.gender = gender;
      notifyListeners();
    }
  }


  Future<List<UserPatient>?> getAndSetUserPatientList() async {
    if (dataStore.readBoolean(DataStoreService.REFRESH_DASHBOARD)!) {
      clearAsyncMemoizer();
      await dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, false);
    }
    return this._memoizer.runOnce(() async {
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      List<UserPatient>? ret = await userService.getUserPatientList();
      if (ret != null && ret.length == 0) return ret;
      _patientList = ret!;
      UserPatient? p;
      for (int i = 0; i < ret.length; i++) {
        if (ret.elementAt(i).primary == true) p = ret.elementAt(i);
        if (ret.elementAt(i).patientId == dataStore.getPatientId()) {
          p = ret.elementAt(i);
          break;
        }
      }
      if (p != null) await setSelectedPatient(p);
      return ret;
    });
  }

  clearUserPatientList() {
    clearAsyncMemoizer();
    _selectedPatient = null;
    notifyListeners();
  }

  clearAsyncMemoizer() {
    _memoizer = AsyncMemoizer();
    _dashboardIndex = 0;
    _selectedPatient = null;
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    checknetworkConnectivity();
    _selectedPatient = null;
    notifyListeners();
  }

  Future<void> setSelectedPatient(UserPatient? patient) async {
    _selectedPatient = patient;
    await dataStore.writeInt(DataStoreService.PATIENTID, _selectedPatient!.patientId!);
    await dataStore.savePatient(_selectedPatient!);
    if (patient!.userPatientLocale != null && patient.userPatientLocale!.contains('QC')) {
      await dataStore.writeBoolean(DataStoreService.QUEBEC, true);
      PPApplication.quebecCheck = true;
    } else {
      await dataStore.writeBoolean(DataStoreService.QUEBEC, false);
      PPApplication.quebecCheck = false;
    }
    if (patient.primary == true)
      await dataStore.markSelectedPatientAsCaregiver(true);
    else
      await dataStore.markSelectedPatientAsCaregiver(false);
    notifyListeners();
  }

  Future<void> setSelectedPatientId(int? patientId) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_member_item);
    if (patientList == null) _patientList = await userService.getUserPatientList();
    for (int i = 0; i < patientList!.length; i++) {
      if (patientList!.elementAt(i).patientId == patientId) {
        _selectedPatient = patientList!.elementAt(i);

        await dataStore.writeInt(DataStoreService.PATIENTID, _selectedPatient!.patientId!);
        await dataStore.savePatient(_selectedPatient!);
        if (_selectedPatient!.primary == true)
          await dataStore.markSelectedPatientAsCaregiver(true);
        else
          await dataStore.markSelectedPatientAsCaregiver(false);
        notifyListeners();
      }
    }
  }

  String getCurrentScreenName() {
    String? eventName;
    switch (dashboardIndex) {
      case 0:
        eventName = AnalyticsEventConstant.dashboard;
        break;
      case 1:
        eventName = AnalyticsEventConstant.medications;
        break;
      case 2:
        eventName = AnalyticsEventConstant.click_prescriptions;
        break;
      case 3:
        eventName = AnalyticsEventConstant.click_orders;
        break;
      default:
        eventName = AnalyticsEventConstant.dashboard;
        break;
    }
    return eventName;
  }

  checknetworkConnectivity() async {
    connectivityResult = await checkInternet();
    notifyListeners();
  }

  int? get memberPatientId => _memberPatientId;
  set memberPatientId(int? value) {
    _memberPatientId = value!;
  }

  bool get applicationStart => _applicationStart;
  set applicationStart(bool value) {
    _applicationStart = value;
    notifyListeners();
  }

  DayWiseMedications? get dayWiseMedications => _dayWiseMedications;
  set dayWiseMedications(DayWiseMedications? value) {
    _dayWiseMedications = value!;
  }
}
