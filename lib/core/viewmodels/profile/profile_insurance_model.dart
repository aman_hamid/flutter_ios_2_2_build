import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/request/add_phn_address_request.dart';
import 'package:pocketpills/core/request/edit_insurance_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/update_health_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/health_card_validation_response.dart';
import 'package:pocketpills/core/response/insurance/copy_insurance_candidate_response.dart';
import 'package:pocketpills/core/response/insurance_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/patient_health_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/Tuple.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileInsuranceModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();
  AsyncMemoizer<Insurance?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();

  int? prevPatientId;
  late Future<Insurance?> _futureInsurance;
  late List<Patient?> copyPrimaryInsuranceList;
  late List<Patient?> copySecondaryInsuranceList;
  late Future<List<TelehealthProvinceItem>?> provinceTelehealthList;
  List<TelehealthProvinceItem> provinceArrayList = [];

  Insurance? curInsurance;
  String curImageUploading = "";
  ConnectivityResult? connectivityResult;
  bool? fstHealth = false;
  bool? scndHealth = false;
  bool healthRetry = false;
  int healthRetryCount = 0;

  bool _secondaryInsurance = false, _tertiaryInsurance = false, _quaternaryInsurance = false, _allInsuranceAvailable = true;

  Future<Insurance?> fetchInsuranceData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId) return _futureInsurance;
    this._memoizer = AsyncMemoizer();
    _futureInsurance = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      curInsurance = await this.getPatientInsurance();
      return curInsurance;
    });
    return _futureInsurance;
  }

  Future<bool> setPatientInsurance(List<String> allergies, List<String> vitamins) async {
    setState(ViewState.Busy);
    Response response = await _api.setPatientInsurance(
        RequestWrapper(body: UpdateHealthRequest(allergies: allergies, vitamins: vitamins, patientId: dataStore.readInteger(DataStoreService.PATIENTID))));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<PatientHealthResponse> res = BaseResponse<PatientHealthResponse>.fromJson(response.data);
    prevPatientId = null;
    notifyListeners();
    return true;
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    prevPatientId = null;
    notifyListeners();
  }

  Future<Insurance?> getPatientInsurance() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }

    Response response = await _api.getPatientInsurance();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<InsuranceResponse> res = BaseResponse<InsuranceResponse>.fromJson(response.data);
    curInsurance = res.response!.insurance;
    updateInsuranceUploadButton();
    notifyListeners();
    return curInsurance;
  }

  void updateInsuranceUploadButton() {
    (curInsurance!.secondaryInsuranceFrontImage == null && curInsurance!.secondaryInsuranceBackImage == null)
        ? secondaryInsurance = false
        : secondaryInsurance = true;
    (curInsurance!.tertiaryInsuranceFrontImage == null && curInsurance!.tertiaryInsuranceBackImage == null)
        ? tertiaryInsurance = false
        : secondaryInsurance = true;
    (curInsurance!.quaternaryInsuranceFrontImage == null && curInsurance!.quaternaryInsuranceBackImage == null)
        ? quaternaryInsurance = false
        : secondaryInsurance = true;
  }

  addInsuranceImage(File image, String insuranceType, bool isInsuranceFront) async {
    Response response = await _api.addInsuranceImage(image, insuranceType, isInsuranceFront, dataStore.readInteger(DataStoreService.PATIENTID).toString());
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    Insurance? ins = await getPatientInsurance();
    if (ins == null) return false;
    notifyListeners();
    return true;
  }

  addInsuranceHealthImage(File image, String insuranceType, bool isInsuranceFront) async {
    Response response =
        await _api.addHealthCard(image, insuranceType, isInsuranceFront, dataStore.readInteger(DataStoreService.PATIENTID).toString(), healthRetry);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      notifyListeners();
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    notifyListeners();
    return true;
  }

  Future<String?> addImage(File image, String source) async {
    var uploadError = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-error");
    var uploadSuccess = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-success");
    switch (source) {
      case "provincialInsuranceFrontImage":
        curImageUploading = "provincialInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "provincialInsuranceBackImage":
        curImageUploading = "provincialInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "primaryInsuranceFrontImage":
        curImageUploading = "primaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PRIMARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "primaryInsuranceBackImage":
        curImageUploading = "primaryInsuranceBackImage";
        notifyListeners();

        bool res = await addInsuranceImage(image, "PRIMARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "secondaryInsuranceFrontImage":
        curImageUploading = "secondaryInsuranceFrontImage";
        notifyListeners();

        bool res = await addInsuranceImage(image, "SECONDARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "secondaryInsuranceBackImage":
        curImageUploading = "secondaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "SECONDARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");

      case "tertiaryInsuranceFrontImage":
        curImageUploading = "tertiaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "TERTIARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "tertiaryInsuranceBackImage":
        curImageUploading = "tertiaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "TERTIARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "quaternaryInsuranceFrontImage":
        curImageUploading = "quaternaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "QUATERNARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "quaternaryInsuranceBackImage":
        curImageUploading = "quaternaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "QUATERNARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError, "");
      case "provincialInsuranceHealthFrontImage":
        curImageUploading = "provincialInsuranceHealthFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", true);
        return handleUploadHealthImageFile(res, uploadSuccess, errorMessage, "health1");
      case "provincialInsuranceHealthBackImage":
        curImageUploading = "provincialInsuranceHealthBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", false);
        return handleUploadHealthImageFile(res, uploadSuccess, errorMessage, "health2");
      default:
        return uploadError;
    }
  }

  Future<Tuple<String, bool>> addHealthImage(File image, String source) async {
    var uploadError = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-error");
    var uploadSuccess = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-success");
    switch (source) {
      case "provincialInsuranceHealthFrontImage":
        curImageUploading = "provincialInsuranceHealthFrontImage";
        notifyListeners();
        bool res = await addInsuranceHealthImage(image, "PROVINCIAL", true);
        res == false ? healthRetryCount = healthRetryCount + 1 : healthRetryCount = 0;
        return Tuple(item1: handleUploadHealthImageFile(res, uploadSuccess, errorMessage, "health1"), item2: res);
      case "provincialInsuranceHealthBackImage":
        curImageUploading = "provincialInsuranceHealthBackImage";
        notifyListeners();
        bool res = await addInsuranceHealthImage(image, "PROVINCIAL", false);
        res == false ? healthRetryCount = healthRetryCount + 1 : healthRetryCount = 0;
        return Tuple(item1: handleUploadHealthImageFile(res, uploadSuccess, errorMessage, "health2"), item2: res);
      default:
        return Tuple(item1: uploadError, item2: false);
    }
  }

  String handleUploadImageFile(bool res, String uploadSuccess, String uploadError, String from) {
    if (res == true) {
      if (from == "health1") {
        fstHealth = true;
        if (fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
        }
      }
      if (from == "health2") {
        scndHealth = true;
        if (fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
        }
      }
      return notifyAfterUploadFile(uploadSuccess);
    }
    curImageUploading = "";
    notifyListeners();
    return uploadError;
  }

  String handleUploadHealthImageFile(bool res, String uploadSuccess, String uploadError, String from) {
    if (res == true) {
      if (from == "health1") {
        fstHealth = true;
        if (fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
        }
      }
      if (from == "health2") {
        scndHealth = true;
        if (fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
        }
      }
      return notifyAfterUploadFile(uploadSuccess);
    }
    curImageUploading = "";
    notifyListeners();
    return uploadError;
  }

  String notifyAfterUploadFile(String uploadSuccess) {
    curImageUploading = "";
    prevPatientId = null;
    notifyListeners();
    return uploadSuccess;
  }

  deleteImage(String source) async {
    switch (source) {
      case "provincialInsuranceFrontImage":
        bool res = await deleteInsuranceImage("PROVINCIAL", true);
        if (res == true) {
          curInsurance!.provincialInsuranceFrontImage = null;
          curInsurance!.provincialInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "provincialInsuranceBackImage":
        bool res = await deleteInsuranceImage("PROVINCIAL", false);
        if (res == true) {
          curInsurance!.provincialInsuranceBackImage = null;
          curInsurance!.provincialInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "primaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("PRIMARY", true);
        if (res == true) {
          curInsurance!.primaryInsuranceFrontImage = null;
          curInsurance!.primaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "primaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("PRIMARY", false);
        if (res == true) {
          curInsurance!.primaryInsuranceBackImage = null;
          curInsurance!.primaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "secondaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("SECONDARY", true);
        if (res == true) {
          curInsurance!.secondaryInsuranceFrontImage = null;
          curInsurance!.secondaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "secondaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("SECONDARY", false);
        if (res == true) {
          curInsurance!.secondaryInsuranceBackImage = null;
          curInsurance!.secondaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "tertiaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("TERTIARY", true);
        if (res == true) {
          curInsurance!.tertiaryInsuranceFrontImage = null;
          curInsurance!.tertiaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "tertiaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("TERTIARY", false);
        if (res == true) {
          curInsurance!.tertiaryInsuranceBackImage = null;
          curInsurance!.tertiaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "quaternaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("QUATERNARY", true);
        if (res == true) {
          curInsurance!.quaternaryInsuranceFrontImage = null;
          curInsurance!.quaternaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "quaternaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("QUATERNARY", false);
        if (res == true) {
          curInsurance!.quaternaryInsuranceBackImage = null;
          curInsurance!.quaternaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      default:
        return;
    }
  }

  Future<bool> getCopyInsuranceCandidate() async {
    setState(ViewState.Busy);
    Response response = await _api.getCopyInsuranceCandidate();
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<CopyInsuranceCandidateResponse> res = BaseResponse<CopyInsuranceCandidateResponse>.fromJson(response.data);
    copyPrimaryInsuranceList = res.response!.primaryInsuranceCandidates;
    copySecondaryInsuranceList = res.response!.secondaryInsuranceCandidates;
    notifyListeners();
    return true;
  }

  Future<bool> getCopyInsuranceCardFromCandidate(int sourcePatientId, String fromInsuranceType, String toInsuranceType) async {
    setState(ViewState.Busy);
    Response response = await _api.getCopyInsuranceCardFromCandidate(sourcePatientId, fromInsuranceType, toInsuranceType);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_copy_insurance);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.getErrorMessage();
    await getPatientInsurance();
    return true;
  }

  Future<bool> deleteInsuranceImage(String insuranceType, bool isInsuranceFront) async {
    setState(ViewState.Busy);
    Response response = await _api.deleteInsuranceImage(RequestWrapper(
        body: EditInsuranceRequest(
            insuranceType: insuranceType, isInsuranceFront: isInsuranceFront, patientId: dataStore.readInteger(DataStoreService.PATIENTID).toString())));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    notifyListeners();
    return true;
  }

  get quaternaryInsurance => _quaternaryInsurance;

  set quaternaryInsurance(value) {
    _quaternaryInsurance = value;
    notifyListeners();
  }

  get tertiaryInsurance => _tertiaryInsurance;

  set tertiaryInsurance(value) {
    _tertiaryInsurance = value;
    notifyListeners();
  }

  bool get secondaryInsurance => _secondaryInsurance;

  set secondaryInsurance(bool value) {
    _secondaryInsurance = value;
    notifyListeners();
  }

  get allInsuranceAvailable => _allInsuranceAvailable;

  set allInsuranceAvailable(value) {
    _allInsuranceAvailable = value;
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  validateHealthCardNumber(String healthCardNumber, String provinceStorage) async {
    setState(ViewState.Busy);
    Response response = await _api.validateHealthNumber(healthCardNumber, dataStore.readString(DataStoreService.PROVINCE).toString(),
        dataStore.readInteger(DataStoreService.USERID).toString(), dataStore.readInteger(DataStoreService.PATIENTID).toString());
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<ValidHealthCardResponse> res = BaseResponse<ValidHealthCardResponse>.fromJson(response.data);
    if (res.status == true && res.response!.isValidHealthCard == true) {
      setState(ViewState.Idle);
      return true;
    } else {
      errorMessage = res.apiMessage ?? "";
      setState(ViewState.Idle);
      return false;
    }
  }

  Future<Tuple<String, bool>> addHealthCardNumber(String healthCardNumber, String insuranceType) async {
    setState(ViewState.Busy);
    Response response = await _api.addHealthNumber(healthCardNumber, insuranceType, dataStore.readInteger(DataStoreService.USERID).toString(),
        dataStore.readInteger(DataStoreService.PATIENTID).toString());
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return Tuple(item1: errorMessage, item2: false);
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    dataStore.writeString(DataStoreService.PHN, healthCardNumber);
    setState(ViewState.Idle);
    return Tuple(item1: res.apiMessage!, item2: true);
  }

  Future<Tuple<String, bool>> addPHN(String healthCardNumber) async {
    Response response = await _api.addPHN(
        healthCardNumber, dataStore.readInteger(DataStoreService.USERID).toString(), dataStore.readInteger(DataStoreService.PATIENTID).toString());
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return Tuple(item1: errorMessage, item2: false);
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return Tuple(item1: res.apiMessage!, item2: true);
  }

  Future<Tuple<String, bool>> addPHNWithAddress(AddPHNAndAddress addPHNAndAddress) async {
    setState(ViewState.Busy);
    Response response = await _api.addPHNWithAddress(addPHNAndAddress, dataStore.readInteger(DataStoreService.USERID).toString());
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return Tuple(item1: errorMessage, item2: false);
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    setState(ViewState.Idle);
    return Tuple(item1: res.apiMessage!, item2: true);
  }

  Future<bool> getUserInfo({bool listenable = false}) async {
    listenable ? setState(ViewState.Busy) : null;
    var response = await _api.getUserInfo();
    listenable ? setState(ViewState.Idle) : null;
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    signUpResponse = resSignUp.response!;
    return await loginService.registerUser(response);
  }

  Future<List<TelehealthProvinceItem>?> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceTelehealthList = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceTelehealthList;
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
    List<TelehealthProvinceItem> listItems = [];
    for(int i =0 ; i< res.response!.items.length;i++){
      if(res.response!.items[i].defaultFlowEnabled == true){
        listItems.add(res.response!.items[i]);
      }
    }
    provinceArrayList = listItems;
    updateProvinceSlotDetails(provinceArrayList);
    return listItems;
  }

  void updateProvinceSlotDetails(List<TelehealthProvinceItem> provinceArrayList) {
    final DataStoreService dataStore = locator<DataStoreService>();
    if(dataStore.readString(DataStoreService.PROVINCE)!=null && dataStore.readString(DataStoreService.PROVINCE).toString().isNotEmpty) {
      for (int i = 0; i < provinceArrayList.length; i++) {
        TelehealthProvinceItem item = provinceArrayList[i];
        if (item.value.toLowerCase() == dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_")) {
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, item.appointmentSlotsEnabled!);
          dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, item.showHealthCardScreen!);
          break;
        }
      }
    }
  }
}
