import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/payment_card.dart';
import 'package:pocketpills/core/request/credit_card_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/patient_card_response.dart';
import 'package:pocketpills/core/response/payment_delete_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfilePaymentModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<List<PaymentCard>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<List<PaymentCard>?> _futureCardsList;
  List<PaymentCard>? cards;

  int selectedCardId = -1;
  int? prevPatientId;

  ConnectivityResult? connectivityResult;

  fetchPaymentData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId) return _futureCardsList;
    this._memoizer = AsyncMemoizer();
    _futureCardsList = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      cards = await this.getPatientCard();
      setDefaultCardId();
      return cards;
    });
    setDefaultCardId();
    return _futureCardsList;
  }

  clearPaymentData() {
    _memoizer = AsyncMemoizer();
    prevPatientId = null;
    notifyListeners();
  }

  clearAsyncMemorizer() {
    clearPaymentData();
  }

  void setSelectedCard(int cardId) {
    selectedCardId = cardId;
    notifyListeners();
  }

  void setDefaultCardId() {
    this.selectedCardId = -1;
    if (cards != null && cards!.length > 0) {
      cards!.forEach((value) => {if (value.isDefault == true) this.selectedCardId = value.id!});
    }
  }

  Future<List<PaymentCard>?> getPatientCard() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPatientCard();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      BaseResponse<PatientCardResponse> res = BaseResponse<PatientCardResponse>.fromJson(response.data);
      cards = res.response!.cards;

      List<PaymentCard> sortedCardList = [];
      List<PaymentCard> seleted = [];
      List<PaymentCard> unSeleted = [];

      cards!.forEach((value) => {
            if (value.isDefault == true) {seleted.add(value)} else {unSeleted.add(value)}
          });
      sortedCardList.addAll(seleted);
      sortedCardList.addAll(unSeleted);

      return sortedCardList;
  }

  Future<bool?> deletePatientCard(int cardId) async {
    setState(ViewState.Busy);
    Response response = await _api.deletePatientCard(cardId);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      BaseResponse<PaymentDeleteResponse> res = BaseResponse<PaymentDeleteResponse>.fromJson(response.data);
      setDefaultCardId();
      return res.response!.deleted;
  }

  Future<bool?> addPatientCard({String? token, String? postalCode}) async {
    setState(ViewState.Busy);
    CreditCardRequest ccRequest =
        CreditCardRequest(token: token, postalCode: postalCode, isDefault: true, patientId: dataStore.readInteger(DataStoreService.PATIENTID).toString());
    Response response = await _api.addPatientCard(RequestWrapper(body: ccRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return res.status;
  }

  Future<bool?> setPatientCardDefault(int cardId) async {
    Response response = await _api.setPatientCardDefault(cardId);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return res.status;
  }

  Future<bool> zeroCopayOption() async {
    Response response = await _api.postZeroCopayOption();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return true;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
