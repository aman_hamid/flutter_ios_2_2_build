import 'dart:convert';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user_language.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/about_you_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/update_email_request.dart';
import 'package:pocketpills/core/request/update_gender_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/services/user_service.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileAboutModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();
  AsyncMemoizer<EmployerSuggestionResponse?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  UserService userService = locator<UserService>();

  late ConnectivityResult connectivityResult;

  String language = ViewConstants.languageIdEn;
  late String email;

  Future<String?> updateInvitationCode(String invitationCode) async {
    setState(ViewState.Busy);
    var response = await _api.activateInvitationCode(invitationCode);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return null;
    }
    setState(ViewState.Idle);
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    return res.errMessage;
  }

  Future<EmployerSuggestionResponse?> getSuggestedEmployers() async {
    return this._memoizer.runOnce(() async {
      Response response = await _api.getSuggestedEmployers();
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return null;
      }
      BaseResponse<EmployerSuggestionResponse> res = BaseResponse<EmployerSuggestionResponse>.fromJson(response.data);
      return res.response;
    });
  }

  String getStoredLanguage() {
    var data = getSelectedLanguage();
    return data;
  }

  UserPatient userDetails = UserPatient();

  UserPatient? getSelectedPatient() {
    if (userDetails.patient == null) {
      String? userPatient = dataStore.readString(DataStoreService.SELECTED_PATIENT);
      Map<String, dynamic>? userMap = jsonDecode(userPatient!);
      userDetails.patient = Patient.fromJson(userMap!);
      return userDetails;
    } else {
      return userDetails;
    }
  }

  void setLanguage(BuildContext context, String locale) async {
    changeLanguage(context, locale);
    notifyListeners();
  }

  Future<bool?> updateLanguage(String locale) async {
    setState(ViewState.Busy);
    LanguageRequest languageRequest = LanguageRequest();
    languageRequest.locale = locale == "en" ? "en_CA" : "fr_CA";
    var response = await _api.updateLanguage(languageRequest);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return true;
  }

  Future<bool> updateMemberEmail(int patient_id, String email) async {
    setState(ViewState.Busy);
    EmailUpdateRequest emailUpdateRequest = EmailUpdateRequest();
    emailUpdateRequest.email = email;
    var response = await _api.updateMemberEmail(patient_id, emailUpdateRequest);

    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      return true;
  }

  Future<bool> updateUserGender(int userId, String email) async {
    setState(ViewState.Busy);
    GenderUpdateRequest genderUpdateRequest = GenderUpdateRequest();
    genderUpdateRequest.gender = email;
    var response = await _api.updateSignupGender(userId, genderUpdateRequest);

    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      notifyListeners();
      return true;
  }

  Future<bool> updateAboutYou(
      {String? province,
      bool? selfMedication,
      bool? isCaregiver,
      bool? pocketPacks,
      String? password,
      String? gender,
      String? email,
      String? referral,
      String? phone,
      String? latestPrescriptionDeliveryBy}) async {
    setState(ViewState.Busy);
    AboutYouRequest abtRequest = AboutYouRequest(
        province: province != null ? province.toUpperCase() : province,
        hasDailyMedication: selfMedication,
        isCaregiver: isCaregiver,
        pocketPacks: pocketPacks,
        gender: gender,
        password: password,
        email: email,
        invitationCode: referral,
        referralCode: referral,
        latestPrescriptionDeliveryBy: latestPrescriptionDeliveryBy,
        phone: phone);
    var response = await _api.updateUserInfo(RequestWrapper(body: abtRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      errorMessage = res.apiMessage!;
      return await loginService.registerUser(response);
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  void setLanguageDropDown(String language) {
    if (ViewConstants.languageMap.containsKey(language)) {
      this.language = language;
      notifyListeners();
    } else {
      this.language = ViewConstants.languageId;
      notifyListeners();
    }
  }
}
