import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/ui/views/profile/profile_view.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';

class ProfileModel extends BaseModel {
  TabController? _tabController;
  int? _activeTabIndex;
  int previousNode = -1, currentNode = -1;

  ProfileModel(int index) {
    this._activeTabIndex = index;
  }

  void setTabController(TabController tabController) {
    _tabController = tabController;
  }

  void setTabIndex(int index) {
    _tabController!.animateTo(index);
    notifyListeners();
  }

  void _setActiveTabIndex() {
    _activeTabIndex = _tabController!.index;
    currentNode = _activeTabIndex!;
    switch (_activeTabIndex) {
      case 0:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_personal_details);
        previousNode = 0;
        break;
      case 1:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_health_card);
        previousNode = 1;
        break;
      case 2:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_insurance);
        previousNode = 2;
        break;
      case 3:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_address);

        previousNode = 3;
        break;
      case 4:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_payment);

        previousNode = 4;
        break;
      case 5:
        if (currentNode == previousNode) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_health_information);
        previousNode = 5;
        break;
    }
  }

  TabController getTabController(ProfileState viewObj) {
    if (_tabController == null) {
      _tabController = TabController(length: 6, vsync: viewObj);
      _tabController!.addListener(_setActiveTabIndex);
      _tabController!.index = _activeTabIndex!;
    }
    return _tabController!;
  }
}
