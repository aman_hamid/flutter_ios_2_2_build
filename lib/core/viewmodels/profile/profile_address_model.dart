import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/address.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/address/address_complete.dart';
import 'package:pocketpills/core/response/address/address_complete_response.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion_response.dart';
import 'package:pocketpills/core/response/address_delete_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/patient_address_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileAddressModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<List<Address>?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  late Future<List<Address>?> _futureAddressList;

  List<Address>? addresses;
  List<Address>? addressSuggestions;

  int selectedAddressId = -1;
  int? prevPatientId;
  late int prevPatientIdAddressSuggestion;

  bool _singleAddress = false;

  ConnectivityResult? connectivityResult;

  fetchAddressData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId) return _futureAddressList;
    this._memoizer = AsyncMemoizer();
    _futureAddressList = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      addresses = await this.getPatientAddress();
      addressSuggestions = await this.getPatientAddressSuggestions();
      setDefaultAddressId();
      return addresses;
    });
    setDefaultAddressId();
    return _futureAddressList;
  }

  clearAddressData() {
    _memoizer = AsyncMemoizer();
    prevPatientId = null;
  }

  clearAsyncMemorizer() {
    clearAddressData();
    notifyListeners();
  }

  Future<List<PinCodeSuggestion>?> searchPostalCode(String keyword) async {
    Response response = await _api.searchPostalCode(keyword);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<PinCodeSuggestionResponse> res = BaseResponse<PinCodeSuggestionResponse>.fromJson(response.data);
    return res.response!.items;
  }

  Future<AddressComplete?> getCompleteAddress(String keyword) async {
    Response response = await _api.getCompleteAddress(keyword);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    AddressCompleteResponse? res = AddressCompleteResponse.fromJson(response.data);
    return res.addressComplete;
  }

  void setSelectedAddress(int addressId) {
    selectedAddressId = addressId;
    notifyListeners();
  }

  Future<List<Address>?> getPatientAddress() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    setState(ViewState.Busy);
    Response response = await _api.getPatientAddress();
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<PatientAddressResponse> res = BaseResponse<PatientAddressResponse>.fromJson(response.data);
    addresses = res.response!.addresses;
    (addresses != null && addresses!.length == 1 && (addresses![0].nickname!= null &&addresses![0].nickname!.contains("TBC"))) ? singleAddress = true : singleAddress = false;

    setDefaultAddressId();

    List<Address> sortedAddress = [];
    List<Address> seleted = [];
    List<Address> unSeleted = [];

    addresses!.forEach((value) => {
          if (value.isDefault == true) {seleted.add(value)} else {unSeleted.add(value)}
        });
    sortedAddress.addAll(seleted);
    sortedAddress.addAll(unSeleted);

    notifyListeners();
    return sortedAddress;
  }

  Future<List<Address>?> getPatientAddressSuggestions() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    setState(ViewState.Busy);
    Response response = await _api.getPatientAddressSuggestion();
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<PatientAddressResponse> res = BaseResponse<PatientAddressResponse>.fromJson(response.data);
    addressSuggestions = res.response!.addresses;
    setDefaultAddressId();
    notifyListeners();
    return addressSuggestions;
  }

  Future<bool> deletePatientAddress(int addressId) async {
    setState(ViewState.Busy);
    Response response = await _api.deletePatientAddress(addressId);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<AddressDeleteResponse> res = BaseResponse<AddressDeleteResponse>.fromJson(response.data);
    errorMessage = res.apiMessage!;
    await updateAddressList();
    setDefaultAddressId();
    return res.response!.isDeleted;
  }

  void setDefaultAddressId() {
    this.selectedAddressId = -1;
    if (addresses != null && addresses!.length > 0) {
      addresses!.forEach((value) => {if (value.isDefault == true) this.selectedAddressId = value.id!});
    }

    if ((addresses != null && addresses!.length == 1 && addresses![0].nickname!=null && addresses![0].nickname!.contains("TBC")) ||
        (addresses != null && addresses!.length == 0 && addressSuggestions != null && addressSuggestions!.length == 0)) {
      singleAddress = true;
    } else {
      singleAddress = false;
    }
  }

  Future<bool> addPatientAddress({String? name, String? sal1, String? sal2, String? city, String? province, String? postalCode, bool? isDefault}) async {
    setState(ViewState.Busy);
    Address newAddress = Address(
        nickname: name,
        streetAddress: sal1,
        streetAddressLineTwo: sal2,
        city: city,
        province: province,
        postalCode: postalCode,
        isDefault: isDefault,
        patientId: dataStore.readInteger(DataStoreService.PATIENTID));
    Response response = await _api.addPatientAddress(RequestWrapper(body: newAddress));
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_copy_address);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage ?? "";
    await updateAddressList();
    notifyListeners();
    return res.status!;
  }

  Future<bool> editPatientAddress(
      {String? name, String? sal1, String? sal2, String? city, String? province, String? postalCode, bool? isDefault, int? addressId}) async {
    setState(ViewState.Busy);
    Address newAddress = Address(
        nickname: name,
        streetAddress: sal1,
        streetAddressLineTwo: sal2,
        city: city,
        province: province,
        postalCode: postalCode,
        isDefault: isDefault,
        patientId: dataStore.getPatientId());
    Response response = await _api.editPatientAddress(RequestWrapper(body: newAddress), addressId!);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      setState(ViewState.Busy);
      addresses = await this.getPatientAddress();
      setState(ViewState.Idle);
      return res.status!;
  }

  Future updateAddressList() async {
    setState(ViewState.Busy);
    addresses = await this.getPatientAddress();
    addressSuggestions = await this.getPatientAddressSuggestions();
    setState(ViewState.Idle);
  }

  Future<bool> setPatientAddressDefault(int addressId) async {
    setState(ViewState.Busy);
    Response response = await _api.setPatientAddressDefault(addressId);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      this.selectedAddressId = addressId;
      notifyListeners();
      return res.status!;
  }

  bool get singleAddress => _singleAddress;

  set singleAddress(bool value) {
    _singleAddress = value;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
