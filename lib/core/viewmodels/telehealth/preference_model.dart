import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_medical_need.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_others_request.dart';
import 'package:pocketpills/core/request/appointment_time_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_restricted_condition_request.dart';
import 'package:pocketpills/core/response/appointment_slot_list_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion_response.dart';
import 'package:pocketpills/core/response/telehealth/medical_condition_validate.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference_bottom_sheet.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:async/async.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/insurance_response.dart';

class PreferenceModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final LoginService loginService = locator<LoginService>();
  List<AppointmentTime> appointmentsList = [];
  List<AppointmentTime> appointmentsListWithoutFilter = [];
  static List<MedicalNeeds>? players;
  ConnectivityResult? connectivityResult;
  SuccessDetails? successDetails;
  int? appointmentId = -1;
  List<Prescription>? prescriptionList;
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  bool revenue = false;
  int? totalPrescriptionsCount;

  //New logic
  List<String> appointmentDisplayDates = [];
  Map<String, AppointmentTime> time = Map<String, AppointmentTime>();
  String? selectedDate;
  int? currentItem = -1;
  AsyncMemoizer<List<AppointmentTime>>? _appointmentMemoizer = AsyncMemoizer();
  Future<List<AppointmentTime>?>? _appointment;
  Map<int, int> appointListToActualIndex = Map<int, int>();

  //AppointmentTime defaultAppointment = AppointmentTime(displayDate: 'Choose a Date');
  String? prescriptionMedicalCondition;
  String? telehealthRequestedMedications;
  String? prescriptionComment;
  List<MedicalConditionSuggestion> medicationSearchResponse = [];
  bool chipRestrictedMedicaiton = false;

  List<TelehealthProvinceItem> provinceArrayList = [];

  late Future<List<TelehealthProvinceItem>?> provinceArrayListMemo;

  AsyncMemoizer<List<TelehealthProvinceItem>?> provinceMemoizer = AsyncMemoizer();

  List<String> popularSearches = [
    "Refill Medication",
    "Birth Control",
    "Anxiety/Depression",
    "ED",
    "Acne",
    "Seasonal Allergies",
    "Hair Loss",
  ];

  Future<bool> sendPreferences(TelehealthRequest telehealthRequest) async {
    setState(ViewState.Busy);
    int? prescriptionId = await _getPrescriptionId();
    Response response = await _api.updateTelehealth(prescriptionId!, telehealthRequest);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    BaseResponse<TelehealthCreateResponse> baseResponse = BaseResponse<TelehealthCreateResponse>.fromJson(response.data);
    totalPrescriptionsCount = baseResponse.response!.totalPrescriptionsCount;
    revenue = baseResponse.response!.revenue != null ? true : false;
    this.successDetails = baseResponse.response!.successDetails;
    GlobalVariable().prescriptionId = baseResponse.response!.omsPrescriptionId;
    GlobalVariable().userFlow = PrescriptionTypeConstant.TELEHEALTH;
    return true;
  }

  AsyncMemoizer<Insurance> _memoizer = AsyncMemoizer();

  int? prevPatientId;
  Future<Insurance>? _futureInsurance;
  List<Patient>? copyPrimaryInsuranceList;
  List<Patient>? copySecondaryInsuranceList;

  Insurance? curInsurance;
  String curImageUploading = "";

  Future<Insurance> fetchInsuranceData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId) return _futureInsurance!;
    this._memoizer = AsyncMemoizer();
    _futureInsurance = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      curInsurance = await this.getPatientInsurance();
      return curInsurance!;
    });
    return _futureInsurance!;
  }

  Future<List<AppointmentTime>?> getAppointmentDate() async {
    if (_appointment != null) return _appointment;

    _appointment = this._appointmentMemoizer!.runOnce(() async {
      Response response = await _api.getAppointmentSlots();
      if (response != null) {
        OMSResponse<AppointmentSlotListResponse> res = OMSResponse<AppointmentSlotListResponse>.fromJson(response.data);
        if (!res.status!) {
          errorMessage = res.getErrorMessage();
          return []; // return null;
        }
        appointmentsListWithoutFilter.clear();
        for (var items in res.response!.items) {
          appointmentsListWithoutFilter.add(items);
          if (items.available != null && items.available!) {
            appointmentsList.add(items);
            appointmentDisplayDates.add(items.displayDate!);
            time.putIfAbsent(items.displayDate!, () => items);
            appointListToActualIndex.putIfAbsent(items.index!, () => appointmentsList.length - 1);
          }
        }
        return res.response!.items;
      } else {
        return []; //return null
      }
    });
    return _appointment;
  }

  clearMemoizer() {
    _appointmentMemoizer = AsyncMemoizer();
  }

  Future<bool> sendPreferencesTime(AppointmentTimeRequest appointmentTimeRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: appointmentTimeRequest));
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    appointmentId = resSignUp.response!.prescription!.id!;
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> getAppointmentId() async {
    int? prescriptionId = await _getPrescriptionId();
    appointmentId = prescriptionId!;
    return true;
  }

  Future<bool> sendPreferencesSignUp(TelehealthSignUpPreferenceRequest? telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    prescriptionList = resSignUp.response!.prescriptionList;
    totalPrescriptionsCount = resSignUp.response!.prescriptionList==null ? null : resSignUp.response!.prescriptionList!.length;
    PPApplication.prescriptionID = resSignUp.response!.prescription!.id!;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> signupOthersFlow(String? prescriptionFlow,String signupFlow) async {
    AddOthersRequest request = AddOthersRequest(prescriptionFlow: prescriptionFlow.toString(),signupFlow: signupFlow);
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: request));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return await loginService.registerUser(response);
  }

  Future<bool> sendRestrictedConditionSignUp(TelehealthSignUpRestrictedRequest? telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      setState(ViewState.Idle);
      return false;
    }
    BaseResponse<SignupResponse> resSignUp = BaseResponse<SignupResponse>.fromJson(response.data);
    prescriptionList = resSignUp.response!.prescriptionList;
    revenue = resSignUp.response!.prescription!.revenue != null ? true : false;
    setState(ViewState.Idle);
    return true;
  }

  showCallAndChatBottomSheet(
    BuildContext context,
    TelehealthSignUpPreferenceRequest telehealthRequestSignUp,
    TelehealthRequest telehealthRequest,
    PreferenceModel model,
    String from,
    SignUpTransferModel modelSignUp,
    UserPatient? userPatient,
    BaseStepperSource? source,
  ) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return PreferenceBottomSheet(context, telehealthRequest, model, telehealthRequestSignUp, from, modelSignUp, source, userPatient);
        });
  }

  Future<Insurance?> getPatientInsurance() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }

    Response response = await _api.getPatientInsurance();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<InsuranceResponse> res = BaseResponse<InsuranceResponse>.fromJson(response.data);
    curInsurance = res.response!.insurance;
    notifyListeners();
    return curInsurance;
  }

  Future<String> setValue(String keyword) async {
    return keyword;
  }

  Future<List<MedicalConditionSuggestion>?> searchMedicalConditions(String keyword) async {
    setState(ViewState.Busy);
    Response response = await _api.searchMedicalConditions(condition: keyword, includeMedication: true);
    var result = await ResponseHandle(response);
    setState(ViewState.Idle);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<MedicalConditionSuggestionResponse> res = BaseResponse<MedicalConditionSuggestionResponse>.fromJson(response.data);
    if (medicationSearchResponse.isNotEmpty && medicationSearchResponse.length > 0) {
      medicationSearchResponse.clear();
    }
    for (var items in res.response!.items) {
      medicationSearchResponse.add(items);
      /*if (items.conditionType != 'MEDICATION') {
        medicationSearchResponse.add(items);
      }*/
    }
    return medicationSearchResponse;
    //return res.response!.items;
  }

  Future<List<MedicalConditionSuggestion>?> searchMedicalConditionsAndMedication(String keyword) async {
    setState(ViewState.Busy);
    Response response = await _api.searchMedicalConditions(condition: keyword, includeMedication: true);
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<MedicalConditionSuggestionResponse> res = BaseResponse<MedicalConditionSuggestionResponse>.fromJson(response.data);
    return res.response!.items;
  }

  Future<MedicalConditionValidate?> validateMedicalConditionsAndComment(String? condition, String? comment) async {
    setState(ViewState.Busy);
    Response response = await _api.validateMedicalConditions(condition: condition ?? "", prescriptionComment: comment ?? "");
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<MedicalConditionValidate> res = BaseResponse<MedicalConditionValidate>.fromJson(response.data);
    return res.response!;
  }

  Future<int?> _getPrescriptionId() async {
    var response = await _api.getPrescriptionId(PrescriptionTypeConstant.TELEHEALTH);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    if (response != null && response.data != null && response.data['details'] != null && response.data['details']['prescriptionId'] != null)
      return response.data['details']['prescriptionId'];
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  Future<List<TelehealthProvinceItem>?> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceArrayListMemo = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceArrayListMemo;
  }

  Future<List<TelehealthProvinceItem>?> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
    List<TelehealthProvinceItem> listItems = [];
    for(int i =0 ; i< res.response!.items.length;i++){
      if(res.response!.items[i].defaultFlowEnabled == true){
        listItems.add(res.response!.items[i]);
      }
    }
    provinceArrayList = listItems;
    updateProvinceSlotDetails(provinceArrayList);
    return listItems;
  }

  void updateProvinceSlotDetails(List<TelehealthProvinceItem> provinceArrayList) {
    final DataStoreService dataStore = locator<DataStoreService>();
    for(int i=0 ; i<provinceArrayList.length ; i++){
      TelehealthProvinceItem item = provinceArrayList[i];
      if(item.value.toLowerCase() == dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_")){
        dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, item.appointmentSlotsEnabled!);
        dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, item.showHealthCardScreen!);
        break;
      }
    }
  }
}
