import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/contact.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup/EDDresponse.dart';
import 'package:pocketpills/core/response/signup/signupEDDitem.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/chat_view.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class UserContactModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  String day = ViewConstants.day;
  String time = ViewConstants.time;
  String language = ViewConstants.languageIdEn;
  String deliveryBy = ViewConstants.deliveryBy;
  String preference = ViewConstants.preference;
  ConnectivityResult? connectivityResult;
  bool _preferenceData = false;
  bool _deliveryByData = false;
  int index = 0;
  bool showedPharmacy = false;
  AsyncMemoizer<Contact?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Contact?> _contactMemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  int? prevPatientId;
  Future<Contact?>? _futureContact;
  Contact? contact;
  List<SignupEddItem> eddList = [];

  Future<Contact?> fetchContact() async {
    if ((day == null || time == null)) {
      day = ViewConstants.day;
      time = ViewConstants.time;
    }
    if (_futureContact == null) {
      if (contact != null) {
        prevPatientId = dataStore.getPatientId();
        _futureContact = Future<Contact?>.value(contact);
        return _futureContact;
      }
    }
    if (prevPatientId != null && prevPatientId == dataStore.getPatientId()) return _futureContact;
    this._memoizer = AsyncMemoizer();
    _futureContact = this._memoizer.runOnce(() async {
      prevPatientId = dataStore.getPatientId();
      return await this.getContact();
    });
    return _futureContact;
  }

  Future<Contact?> fetchContactDetails() async {
    if (prevPatientId != null && prevPatientId == dataStore.getPatientId()) return _futureContact;
    this._contactMemoizer = AsyncMemoizer();
    _futureContact = this._contactMemoizer.runOnce(() async {
      prevPatientId = dataStore.getPatientId();
      return await this.getContact();
    });
    return _futureContact;
  }

  Future<Contact?> getContact() async {
    Response contactRes = await _api.getContact();
    var result = await ResponseHandle(contactRes);
    if (!result.status) {
      errorMessage = result.message;
      return Contact();
    }
    if (contactRes.data["details"] == null) {
      return Contact();
    }
    BaseResponse<Contact> contactDetails = BaseResponse<Contact>.fromJson(contactRes.data);
    if (contactDetails.response != null) {
      setDay(contactDetails.response!.day!);
      setTime(contactDetails.response!.timeLow! + " - " + contactDetails.response!.timeHigh!);
      return contactDetails.response;
    } else
      return null;
  }

  void setDay(String day) {
    if (getDayMap().containsKey(day)) {
      this.day = day;
      notifyListeners();
    } else {
      this.day = ViewConstants.day;
      notifyListeners();
    }
  }

  void setTime(String time) {
    if (getTimeMap().containsKey(time)) {
      this.time = time;
      notifyListeners();
    } else {
      this.time = ViewConstants.time;
      notifyListeners();
    }
  }

  void setDeliveryTime(String deliveryBy) {
    if (ViewConstants.deliveryTimeMap.containsKey(deliveryBy)) {
      this.deliveryBy = deliveryBy;
      notifyListeners();
    } else {
      this.deliveryBy = eddList[0].value;
      for (int i = 0; i < eddList.length; i++) {
        if (eddList[i].value == deliveryBy) {
          this.index = i;
          this.deliveryBy = eddList[i].value;
          notifyListeners();
        }
      }
    }
  }

  void setPrefrence(String prefrence) {
    if (getFaxPreferenceMap().containsKey(prefrence)) {
      this.preference = prefrence;
      notifyListeners();
    } else {
      this.preference = ViewConstants.preference;
      notifyListeners();
    }
  }

  Future<bool> putContact({SuccessDetails? successDetails}) async {
    List<String> arr = getContactTime();
    Response response = await updateContactInformation(arr);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    return true;
  }

  List<String> getContactTime() {
    List<String> arr = [];
    if (this.time == "Any Time") {
      String daytime = "00 - 00";
      arr = daytime.split(" - ");
    } else {
      arr = this.time.split(" - ");
    }
    return arr;
  }

  Future<Response> updateContactInformation(List<String> arr) async {
    setState(ViewState.Busy);
    Contact contact = Contact(
        userId: dataStore.getUserId(),
        day: this.day,
        timeLow: arr[0],
        timeHigh: arr[1],
        prescriptionPreference: _preferenceData ? preference : null,
        userFlow: GlobalVariable().userFlow,
        entityId: GlobalVariable().prescriptionId,
        deliveryBy: _deliveryByData ? deliveryBy : null);

    Response contactRes = await _api.putContact(RequestWrapper(body: contact));
    setState(ViewState.Idle);
    return contactRes;
  }

  Future<bool> updateContactInfo() async {
    List<String> arr = getContactTime();
    Response response = await updateContactInformation(arr);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    errorMessage = res.apiMessage!;
    prevPatientId = null;
    _contactMemoizer = AsyncMemoizer();
    notifyListeners();
    return true;
  }

  handleSuccessViewRoute(TransactionSuccessActionType actionType, BuildContext context) {
    switch (actionType) {
      case TransactionSuccessActionType.MEDICATIONS: // transfer
        Provider.of<DashboardModel>(context).dashboardIndex = 1;
        Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        break;
      case TransactionSuccessActionType.CUSTOMER_CARE:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_chat);
        Provider.of<HomeModel>(context).showCallAndChatBottomSheet(context);
        break;
      case TransactionSuccessActionType.OPEN_CHAT:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_chat);
        Navigator.pushNamed(context, ChatWidget.routeName);
        break;
      case TransactionSuccessActionType.DASHBOARD:
        Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
        Provider.of<HomeModel>(context, listen: false).clearData();
        dataStore.readBoolean(DataStoreService.APPOINTMENT_DASHBOARD) == true
            ? dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD_POPUP, true)
            : dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD_POPUP, false);
        Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        break;
    }
  }

  bool get deliveryByData => _deliveryByData;

  set deliveryByData(bool value) {
    _deliveryByData = value;
  }

  bool get preferenceData => _preferenceData;

  set preferenceData(bool value) {
    _preferenceData = value;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }

  Future<bool?> getEDDList(String postalcode, String province) async {
    Response response = await _api.getEDDlist(province, postalcode);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<EDDresponse> res = BaseResponse<EDDresponse>.fromJson(response.data);
    if (!res.status!) {
      errorMessage = res.getErrorMessage();
      return false;
    }
    eddList = res.response!.items;
    deliveryBy = eddList[0].value;
    return true;
  }
}
