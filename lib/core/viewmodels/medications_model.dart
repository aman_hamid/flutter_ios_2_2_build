import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/editable_text.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/medication_request.dart';
import 'package:pocketpills/core/request/refill_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/medication.dart';
import 'package:pocketpills/core/response/medications_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/refill/medication_refill_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/Tuples.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class MedicationsModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  late Future<MedicationsResponse?> _futureMedicationsList;
  AsyncMemoizer<MedicationsResponse?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  MedicationsResponse? medicationsResponse;

  ConnectivityResult? connectivityResult;

  Map<int, bool> selectedMedicationsForMedicationId = new Map();
  Map<int, Medication> medicationForMedicationId = new Map();
  Map<int, TextEditingController> medicationIdTextEditingControllerMap = new Map();
  Map<int, String> medicationIdSelectedQuantity = new Map();

  TextEditingController reasonController = TextEditingController();

  int? selectedMedicinesCount = 0;
  int? prevPatientId;

  bool showRenewalPopup = true;
  bool showRefillPopup = true;
  bool quantityValidation = false;
  SuccessDetails? successDetails;

  void setShowRenewalPopup(bool val) {
    showRenewalPopup = val;
    notifyListeners();
  }

  void setShowRefillPopup(bool val) {
    showRefillPopup = val;
    reasonController.clear();
    notifyListeners();
  }

  Future<MedicationsResponse?> fetchMedications() async {
    if (prevPatientId != null && prevPatientId == dataStore.getPatientId()) return _futureMedicationsList;
    this._memoizer = AsyncMemoizer();
    _futureMedicationsList = this._memoizer.runOnce(() async {
      prevPatientId = dataStore.getPatientId();
      return await this.getMedications();
    });
    return _futureMedicationsList;
  }

  clearData() {
    prevPatientId = null;
    contentMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  bool isOrderedQuantityMore() {
    bool res = false;
    try {
      medicationIdSelectedQuantity.forEach((key, value) {
        if (medicationForMedicationId[key]!.dgType != 'O' && value != null && num.tryParse(value) != null && int.parse(value) > medicationForMedicationId[key]!.quantityLeft!) {
          res = true;
          return;
        }
      });
    } catch (e) {
      print(e.toString());
    }
    return res;
  }

  Future<bool> refillMedications() async {
    setState(ViewState.Busy);
    List<MedicationRequest> reqMedications = [];
    List<Medication> medicationList = getMarkedMedications();

    medicationList.forEach((medicationList) {
      if (medicationIdSelectedQuantity.containsKey(medicationList.id)) {
        try {
          if (medicationIdSelectedQuantity[medicationList.id] != "" && double.parse(medicationIdSelectedQuantity[medicationList.id]!) != 0.0) {
            quantityValidation = false;
            reqMedications.add(
              MedicationRequest(
                  medicationId: medicationList.medicationId,
                  quantity: double.parse(medicationIdSelectedQuantity[medicationList.id]!),
                  drugName: medicationList.drug,
                  din: medicationList.medicationId != null ? null : medicationList.drugId,
                  variantId: medicationList.variantId,
                  plan: "REGULAR"),
            );
          } else {
            quantityValidation = true;
          }
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
    });
    if (quantityValidation == false) {
      Response response = await _api.refillMedications(RequestWrapper(body: RefillRequest(comment: reasonController.text, medications: reqMedications)));
      setState(ViewState.Idle);
      var result = await ResponseHandle(response);
      if (!result.status) {
        errorMessage = result.message;
        return false;
      }
      BaseResponse<MedicationRefillResponse> baseResponse = BaseResponse.fromJson(response.data);
      GlobalVariable().prescriptionId = baseResponse.response!.omsOrderId;
      GlobalVariable().userFlow = PrescriptionTypeConstant.REFILL;
      this.successDetails = baseResponse.response!.successDetails;
      return true;
    } else {
      setState(ViewState.Idle);
      errorMessage = LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity-left");
      return false;
    }
  }

  Future<MedicationsResponse?> getMedications() async {
    if (dataStore.getPatientId() == null) return null;
    Response response = await _api.getMedications();
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return null;
    }
    BaseResponse<MedicationsResponse> res = BaseResponse<MedicationsResponse>.fromJson(response.data);
    medicationsResponse = res.response;
    int id = 0;
    medicationsResponse!.medicines!
        .forEach((type, medicationList) => medicationList.forEach((medication) => {medicationForMedicationId[id] = medication, medication.id = id, id++}));

    return res.response;
  }

  void markMedicationSelected(int id) {
    selectedMedicationsForMedicationId[id] = true;
    selectedMedicinesCount = selectedMedicinesCount! + 1;
    notifyListeners();
  }

  void unmarkMedicationSelected(int id) {
    if (selectedMedicationsForMedicationId[id] != null) {
      selectedMedicationsForMedicationId.remove(id);
      medicationIdSelectedQuantity.remove(id);
      medicationIdTextEditingControllerMap.remove(id);
      selectedMedicinesCount = selectedMedicinesCount! - 1;
      notifyListeners();
    }
  }

  bool? isSelected(int id) {
    return selectedMedicationsForMedicationId[id] != null ? selectedMedicationsForMedicationId[id] : false;
  }

  List<Medication> getMarkedMedications() {
    List<Medication> markedMedications = [];
    selectedMedicationsForMedicationId.forEach((id, isSelected) => isSelected == true ? markedMedications.add(medicationForMedicationId[id]!) : () {});

    // markedMedications.forEach((medication)=>);
    return markedMedications;
  }

  void unmarkAllMedications() {
    selectedMedicationsForMedicationId.clear();
    medicationIdSelectedQuantity.clear();
    medicationIdTextEditingControllerMap.clear();
    selectedMedicinesCount = 0;
    notifyListeners();
  }

  TextEditingController getControllerForMedication(int id, Medication medication) {
    if (!medicationIdSelectedQuantity.containsKey(id)) medicationIdSelectedQuantity[id] = Utils.removeDecimalZeroFormat(medication.quantityPerFill!);
    if (!medicationIdTextEditingControllerMap.containsKey(id)) {
      medicationIdTextEditingControllerMap[id] = TextEditingController(text: medicationIdSelectedQuantity[id]);
      medicationIdTextEditingControllerMap[id]!.addListener(() {
        updateQuantityForMedication(id, medicationIdTextEditingControllerMap[id]!.text);
      });
    }
    return medicationIdTextEditingControllerMap[id]!;
  }

  String getSelectedQuantityForMedication(int id, Medication medication) {
    medicationIdSelectedQuantity.putIfAbsent(id, () => Utils.removeDecimalZeroFormat(medication.quantityPerFill!));
    return medicationIdSelectedQuantity[id].toString();
  }

  void updateQuantityForMedication(int id, String text) {
    if (medicationIdSelectedQuantity[id] != text) {
      medicationIdSelectedQuantity[id] = text;
      //notifyListeners();
    }
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
        BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
        print("Called api  $keys");
        return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
