import 'dart:io';
import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/transfer_prescription_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/prescription_create_response.dart';
import 'package:pocketpills/core/response/response_handle.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/services/notification_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/rotate_compress_image.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/request/signup_prescription_request.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';

class ImageUploadModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final NotificationService notificationService = locator<NotificationService>();
  SuccessDetails? successDetails;
  final LoginService loginService = locator<LoginService>();
  ConnectivityResult? connectivityResult;
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();
  List<Prescription>? prescriptionList;
  bool revenue = false;
  int? totalPrescriptionsCount;

  Future<bool> uploadPrescription(List<File> images, String? medicineName, double? quantity) async {
    setState(ViewState.Busy);
    int? prescriptionId = await _getPrescriptionId();
    TransferPrescriptionRequest request =
        TransferPrescriptionRequest(prescriptionState: 'FILED', prescriptionType: PrescriptionTypeConstant.UPLOAD, medicationName: medicineName, quantity: quantity);

    if (prescriptionId == null) return false;
    Response? response = await _api.updatePrescription(prescriptionId, request);
    if (response == null) return false;
    BaseResponse<PrescriptionCreateResponse> baseResponse = BaseResponse<PrescriptionCreateResponse>.fromJson(response.data);
    if (!baseResponse.status!) return false;
    revenue = baseResponse.response!.revenue != null ? true : false;
    totalPrescriptionsCount = baseResponse.response!.totalPrescriptionsCount;
    this.successDetails = baseResponse.response!.successDetails;
    GlobalVariable().prescriptionId = baseResponse.response!.omsPrescriptionId;
    GlobalVariable().userFlow = PrescriptionTypeConstant.UPLOAD;
    int i = 0;
    images.forEach((image) async {
      var finalImage = await RotateAndCompressImage().rotateAndCompressAndSaveImage(image);
      await _api.uploadImage(prescriptionId, finalImage!, i);
      i++;
    });

    setState(ViewState.Idle);
    return true;
  }

  Future<bool> uploadPrescriptionSignUp(List<File> images) async {
    // int prescriptionId = await _getPrescriptionId();
    SignupTransferPrescriptionRequest requestSignUp = SignupTransferPrescriptionRequest(
        type: PrescriptionTypeConstant.UPLOAD, pharmacyName: null, pharmacyAddress: null, pharmacyPhone: null, isTransferAll: false, prescriptionState: "FILED");
    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: requestSignUp));
    setState(ViewState.Idle);
    var result = await ResponseHandle(response);
    if (!result.status) {
      errorMessage = result.message;
      return false;
    }
      BaseResponse<SignupResponse> res = BaseResponse<SignupResponse>.fromJson(response.data);
      print('MANAHERE');
      totalPrescriptionsCount = res.response!.prescriptionList!.length;
      prescriptionList = res.response!.prescriptionList;
      revenue = res.response!.prescription!.revenue != null ? true : false;
      int i = 0;
      images.forEach((image) async {
        var finalImage = await RotateAndCompressImage().rotateAndCompressAndSaveImage(image);
        await _api.signUpUploadImage(finalImage!, i);
        i++;
      });
    setState(ViewState.Idle);
    return true;
  }

  Future<int?> _getPrescriptionId() async {
    var response = await _api.getPrescriptionId(PrescriptionTypeConstant.UPLOAD);
    if (response != null && response.data != null && response.data['details'] != null && response.data['details']['prescriptionId'] != null)
      return response.data['details']['prescriptionId'];
    return null;
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        var result = await ResponseHandle(response);
        if (!result.status) {
          errorMessage = result.message;
          return null;
        }
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response!.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
      }
    });
  }
}
