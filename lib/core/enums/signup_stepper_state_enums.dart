class SignupStepperStateEnums {
  static const int SIGN_UP_ABOUT_YOU = 0;
  static const int SIGN_UP_TRANSFER = 1;
  static const int SIGN_UP_ALMOST_DONE = 2;
  static const int SIGN_UP_OTP_PASSWORD = 3;
  static const int SIGN_UP_TELEHEALTH_CARD = 4;
  static const int SIGN_UP_PHONE_ADD = 5;
  static const int CUSTOMISED_SIGN_UP = 6;
  static const int SIGN_UP_UPLOAD_PRESCRIPTION = 7;
  static const int SIGN_UP_TELEHEALTH = 8;
  static const int SIGN_UP_COMPLETED = 9;
}
