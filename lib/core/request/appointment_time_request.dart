import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

class AppointmentTimeRequest extends BaseRequest {
  String? appointmentTime;

  AppointmentTimeRequest({this.appointmentTime});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'appointmentTime': (this.appointmentTime),
      };
}
