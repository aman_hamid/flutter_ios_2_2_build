import 'package:pocketpills/core/request/base_request.dart';

class TransferPrescriptionRequest extends BaseRequest {
  String? prescriptionType;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  String? comment;
  bool? isTransferAll;
  String? prescriptionState;
  String? placeId;
  String? medicationName;
  double? quantity;

  TransferPrescriptionRequest(
      {this.prescriptionType,
      this.pharmacyName,
      this.pharmacyPhone,
      this.comment,
      this.isTransferAll,
      this.prescriptionState,
      this.pharmacyAddress,
      this.placeId,
      this.quantity,
      this.medicationName});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'prescriptionType': this.prescriptionType,
        'pharmacyName': this.pharmacyName,
        'pharmacyPhone': this.pharmacyPhone,
        'pharmacyAddress': this.pharmacyAddress,
        'comment': this.comment,
        'isTransferAll': this.isTransferAll.toString(),
        'prescriptionState': this.prescriptionState,
        'pharmacyGooglePlaceId': this.placeId,
        'quantity': this.quantity,
        'medicationName': this.medicationName
      };
}
