import 'package:pocketpills/core/request/base_request.dart';

class AddMemberTransferPrescriptionRequest extends BaseRequest {
  String? prescriptionType;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  bool? isTransferAll;
  String? prescriptionState;

  AddMemberTransferPrescriptionRequest({
    this.prescriptionType,
    this.pharmacyName,
    this.pharmacyPhone,
    this.isTransferAll,
    this.prescriptionState,
    this.pharmacyAddress,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'prescriptionType': this.prescriptionType,
        'pharmacyName': this.pharmacyName,
        'pharmacyPhone': this.pharmacyPhone,
        'pharmacyAddress': this.pharmacyAddress,
        'isTransferAll': this.isTransferAll.toString(),
        'prescriptionState': this.prescriptionState,
      };
}
