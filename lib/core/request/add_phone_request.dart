import 'package:pocketpills/core/request/base_request.dart';

class AddPhoneRequest extends BaseRequest {
  String? phone;

  AddPhoneRequest({this.phone});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'phone': this.phone,
      };
}
