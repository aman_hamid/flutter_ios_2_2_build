// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reset_password_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResetPasswordRequest _$ResetPasswordRequestFromJson(
        Map<String, dynamic> json) =>
    ResetPasswordRequest(
      password: json['password'] as String,
      resetKey: json['resetKey'] as String,
      phone: json['phone'] as int,
      loginBody: DeviceBody.fromJson(json['loginBody'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ResetPasswordRequestToJson(
        ResetPasswordRequest instance) =>
    <String, dynamic>{
      'password': instance.password,
      'resetKey': instance.resetKey,
      'phone': instance.phone,
      'loginBody': instance.loginBody,
    };
