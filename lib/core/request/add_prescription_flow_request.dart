import 'package:pocketpills/core/request/base_request.dart';

class AddPrescriptionRequest extends BaseRequest {
  String? prescriptionFlow;

  AddPrescriptionRequest({this.prescriptionFlow});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'prescriptionFlow': this.prescriptionFlow,
      };
}
