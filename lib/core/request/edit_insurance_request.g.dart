// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_insurance_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditInsuranceRequest _$EditInsuranceRequestFromJson(
        Map<String, dynamic> json) =>
    EditInsuranceRequest(
      insuranceType: json['insuranceType'] as String,
      patientId: json['patientId'] as String,
      isInsuranceFront: json['isInsuranceFront'] as bool,
    );

Map<String, dynamic> _$EditInsuranceRequestToJson(
        EditInsuranceRequest instance) =>
    <String, dynamic>{
      'insuranceType': instance.insuranceType,
      'isInsuranceFront': instance.isInsuranceFront,
      'patientId': instance.patientId,
    };
