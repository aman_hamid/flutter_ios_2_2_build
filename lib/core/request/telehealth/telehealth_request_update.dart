import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_request_update.g.dart';

@JsonSerializable()
class TelehealthRequestUpdate extends BaseRequest {
  int? id;

  String? appointmentTime;

  int? doctorId;

  int? teleHealthDoctorDayPlanId;

  int? telehealthAppointmentSlotId;

  TelehealthRequestUpdate({this.id, this.appointmentTime, this.doctorId,this.teleHealthDoctorDayPlanId,this.telehealthAppointmentSlotId});

  factory TelehealthRequestUpdate.fromJson(Map<String, dynamic> data) => _$TelehealthRequestUpdateFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthRequestUpdateToJson(this);
}
