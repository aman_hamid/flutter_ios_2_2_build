// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cancel_appointment_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CancelAppointmentRequest _$CancelAppointmentRequestFromJson(
        Map<String, dynamic> json) =>
    CancelAppointmentRequest(
      omsPrescriptionId: json['id'] as int?,
      cancellationSmsFlag: json['cancellationSmsFlag'] as bool?,
      createNewTelehealthPrescription:
          json['createNewTelehealthPrescription'] as bool?,
      reasonForCancellation: json['reasonForCancellation'] as String?,
      sendCancellationFax: json['sendCancellationFax'] as bool?,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$CancelAppointmentRequestToJson(
        CancelAppointmentRequest instance) =>
    <String, dynamic>{
      'id': instance.omsPrescriptionId,
      'cancellationSmsFlag': instance.cancellationSmsFlag,
      'createNewTelehealthPrescription':
          instance.createNewTelehealthPrescription,
      'reasonForCancellation': instance.reasonForCancellation,
      'sendCancellationFax': instance.sendCancellationFax,
      'status': instance.status,
    };
