import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_signup_preference_request.g.dart';

@JsonSerializable()
class TelehealthSignUpPreferenceRequest extends BaseRequest {
  int? id;
  String? type;
  String? appointmentTime;
  String? prescriptionComment;
  bool? prescriptionFilledByExternalPharmacy;
  bool? isPrescriptionEdited;
  String? prescriptionState;
  String? prescriptionRequestReason;
  String? prescriptionRequestCategory;
  String? telehealthRequestedMedications;
  String? prescriptionMedicalConditions;
  int? doctorId;
  int? teleHealthDoctorDayPlanId;
  int? telehealthAppointmentSlotId;

  TelehealthSignUpPreferenceRequest({
    this.id,
    this.type,
    this.appointmentTime,
    this.prescriptionComment,
    this.prescriptionFilledByExternalPharmacy,
    this.isPrescriptionEdited,
    this.prescriptionState,
    this.prescriptionRequestReason,
    this.prescriptionRequestCategory,
    this.telehealthRequestedMedications,
    this.prescriptionMedicalConditions,
    this.doctorId,
    this.teleHealthDoctorDayPlanId,
    this.telehealthAppointmentSlotId,
  });

  factory TelehealthSignUpPreferenceRequest.fromJson(Map<String, dynamic> data) => _$TelehealthSignUpPreferenceRequestFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthSignUpPreferenceRequestToJson(this);
}
