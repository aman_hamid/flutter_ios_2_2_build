import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_signup_preference_request_skip.g.dart';

@JsonSerializable()
class TelehealthSignUpPreferenceRequestSkip extends BaseRequest {
  String? prescriptionState;
  String? appointmentTime;
  String? type;
  int? doctorId;
  int? teleHealthDoctorDayPlanId;
  int? telehealthAppointmentSlotId;

  TelehealthSignUpPreferenceRequestSkip({
    this.prescriptionState,
    this.doctorId,
    this.type,
    this.appointmentTime,
    this.teleHealthDoctorDayPlanId,
    this.telehealthAppointmentSlotId,
  });

  factory TelehealthSignUpPreferenceRequestSkip.fromJson(Map<String, dynamic> data) => _$TelehealthSignUpPreferenceRequestSkipFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthSignUpPreferenceRequestSkipToJson(this);
}
