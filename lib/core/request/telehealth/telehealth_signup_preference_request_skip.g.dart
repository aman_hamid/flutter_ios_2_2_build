// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_signup_preference_request_skip.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthSignUpPreferenceRequestSkip
    _$TelehealthSignUpPreferenceRequestSkipFromJson(
            Map<String, dynamic> json) =>
        TelehealthSignUpPreferenceRequestSkip(
          prescriptionState: json['prescriptionState'] as String?,
          doctorId: json['doctorId'] as int?,
          type: json['type'] as String?,
          appointmentTime: json['appointmentTime'] as String?,
          teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
          telehealthAppointmentSlotId:
              json['telehealthAppointmentSlotId'] as int?,
        );

Map<String, dynamic> _$TelehealthSignUpPreferenceRequestSkipToJson(
        TelehealthSignUpPreferenceRequestSkip instance) =>
    <String, dynamic>{
      'prescriptionState': instance.prescriptionState,
      'appointmentTime': instance.appointmentTime,
      'type': instance.type,
      'doctorId': instance.doctorId,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
    };
