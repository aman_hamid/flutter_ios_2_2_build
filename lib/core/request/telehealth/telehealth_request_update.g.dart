// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_request_update.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthRequestUpdate _$TelehealthRequestUpdateFromJson(
        Map<String, dynamic> json) =>
    TelehealthRequestUpdate(
      id: json['id'] as int?,
      appointmentTime: json['appointmentTime'] as String?,
      doctorId: json['doctorId'] as int?,
      teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
      telehealthAppointmentSlotId: json['telehealthAppointmentSlotId'] as int?,
    );

Map<String, dynamic> _$TelehealthRequestUpdateToJson(
        TelehealthRequestUpdate instance) =>
    <String, dynamic>{
      'id': instance.id,
      'appointmentTime': instance.appointmentTime,
      'doctorId': instance.doctorId,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
    };
