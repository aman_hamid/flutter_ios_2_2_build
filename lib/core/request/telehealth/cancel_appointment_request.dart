import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'cancel_appointment_request.g.dart';

@JsonSerializable()
class CancelAppointmentRequest extends BaseRequest {
  @JsonKey(name: 'id')
  int? omsPrescriptionId;
  bool? cancellationSmsFlag;
  bool? createNewTelehealthPrescription;
  String? reasonForCancellation;
  bool? sendCancellationFax;
  String? status;

  CancelAppointmentRequest(
      {this.omsPrescriptionId, this.cancellationSmsFlag, this.createNewTelehealthPrescription, this.reasonForCancellation, this.sendCancellationFax, this.status});

  factory CancelAppointmentRequest.fromJson(Map<String, dynamic> data) => _$CancelAppointmentRequestFromJson(data);

  Map<String, dynamic> toJson() => _$CancelAppointmentRequestToJson(this);
}
