// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chambers_active_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChambersRequest _$ChambersRequestFromJson(Map<String, dynamic> json) =>
    ChambersRequest(
      token: json['token'] as String?,
      group: json['group'] as String?,
      userIdentifier: json['userIdentifier'] as String?,
    );

Map<String, dynamic> _$ChambersRequestToJson(ChambersRequest instance) =>
    <String, dynamic>{
      'token': instance.token,
      'group': instance.group,
      'userIdentifier': instance.userIdentifier,
    };
