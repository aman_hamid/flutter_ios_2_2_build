// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_signup_preference_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthSignUpPreferenceRequest _$TelehealthSignUpPreferenceRequestFromJson(
        Map<String, dynamic> json) =>
    TelehealthSignUpPreferenceRequest(
      id: json['id'] as int?,
      type: json['type'] as String?,
      appointmentTime: json['appointmentTime'] as String?,
      prescriptionComment: json['prescriptionComment'] as String?,
      prescriptionFilledByExternalPharmacy:
          json['prescriptionFilledByExternalPharmacy'] as bool?,
      isPrescriptionEdited: json['isPrescriptionEdited'] as bool?,
      prescriptionState: json['prescriptionState'] as String?,
      prescriptionRequestReason: json['prescriptionRequestReason'] as String?,
      prescriptionRequestCategory:
          json['prescriptionRequestCategory'] as String?,
      telehealthRequestedMedications:
          json['telehealthRequestedMedications'] as String?,
      prescriptionMedicalConditions:
          json['prescriptionMedicalConditions'] as String?,
      doctorId: json['doctorId'] as int?,
      teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
      telehealthAppointmentSlotId: json['telehealthAppointmentSlotId'] as int?,
    );

Map<String, dynamic> _$TelehealthSignUpPreferenceRequestToJson(
        TelehealthSignUpPreferenceRequest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'appointmentTime': instance.appointmentTime,
      'prescriptionComment': instance.prescriptionComment,
      'prescriptionFilledByExternalPharmacy':
          instance.prescriptionFilledByExternalPharmacy,
      'isPrescriptionEdited': instance.isPrescriptionEdited,
      'prescriptionState': instance.prescriptionState,
      'prescriptionRequestReason': instance.prescriptionRequestReason,
      'prescriptionRequestCategory': instance.prescriptionRequestCategory,
      'telehealthRequestedMedications': instance.telehealthRequestedMedications,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'doctorId': instance.doctorId,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
    };
