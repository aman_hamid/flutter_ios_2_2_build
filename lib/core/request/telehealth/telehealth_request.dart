import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_request.g.dart';

@JsonSerializable()
class TelehealthRequest extends BaseRequest {
  String? prescriptionType;
  bool? prescriptionFilledByExternalPharmacy;
  String? prescriptionRequestReason;
  String? prescriptionState;
  String? prescriptionRequestCategory;
  String? prescriptionMedicalConditions;
  String? telehealthRequestedMedications;
  String? prescriptionComment;
  String? appointmentTime;
  bool? isPrescriptionEdited;
  int? doctorId;
  int? teleHealthDoctorDayPlanId;
  int? telehealthAppointmentSlotId;

  TelehealthRequest(
      {this.prescriptionType,
      this.prescriptionFilledByExternalPharmacy,
      this.prescriptionRequestReason,
      this.prescriptionState,
      this.prescriptionRequestCategory,
      this.prescriptionMedicalConditions,
      this.telehealthRequestedMedications,
      this.prescriptionComment,
      this.appointmentTime,
      this.isPrescriptionEdited,
      this.doctorId,
      this.telehealthAppointmentSlotId,
      this.teleHealthDoctorDayPlanId});

  factory TelehealthRequest.fromJson(Map<String, dynamic> data) => _$TelehealthRequestFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthRequestToJson(this);
}
