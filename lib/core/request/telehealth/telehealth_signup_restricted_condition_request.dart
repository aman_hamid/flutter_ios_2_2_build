import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
part 'telehealth_signup_restricted_condition_request.g.dart';

@JsonSerializable()
class TelehealthSignUpRestrictedRequest extends BaseRequest {
  int? id;
  String? type;
  bool? isPrescriptionEdited;
  String? prescriptionState;
  int? doctorId;
  int? teleHealthDoctorDayPlanId;
  int? telehealthAppointmentSlotId;
  String? appointmentTime;

  TelehealthSignUpRestrictedRequest({
    this.id,
    this.type,
    this.isPrescriptionEdited,
    this.prescriptionState,
    this.doctorId,
    this.teleHealthDoctorDayPlanId,
    this.telehealthAppointmentSlotId,
    this.appointmentTime
  });

  factory TelehealthSignUpRestrictedRequest.fromJson(Map<String, dynamic> data) => _$TelehealthSignUpRestrictedRequestFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthSignUpRestrictedRequestToJson(this);
}
