// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthRequest _$TelehealthRequestFromJson(Map<String, dynamic> json) =>
    TelehealthRequest(
      prescriptionType: json['prescriptionType'] as String?,
      prescriptionFilledByExternalPharmacy:
          json['prescriptionFilledByExternalPharmacy'] as bool?,
      prescriptionRequestReason: json['prescriptionRequestReason'] as String?,
      prescriptionState: json['prescriptionState'] as String?,
      prescriptionRequestCategory:
          json['prescriptionRequestCategory'] as String?,
      prescriptionMedicalConditions:
          json['prescriptionMedicalConditions'] as String?,
      telehealthRequestedMedications:
          json['telehealthRequestedMedications'] as String?,
      prescriptionComment: json['prescriptionComment'] as String?,
      appointmentTime: json['appointmentTime'] as String?,
      isPrescriptionEdited: json['isPrescriptionEdited'] as bool?,
      doctorId: json['doctorId'] as int?,
      telehealthAppointmentSlotId: json['telehealthAppointmentSlotId'] as int?,
      teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
    );

Map<String, dynamic> _$TelehealthRequestToJson(TelehealthRequest instance) =>
    <String, dynamic>{
      'prescriptionType': instance.prescriptionType,
      'prescriptionFilledByExternalPharmacy':
          instance.prescriptionFilledByExternalPharmacy,
      'prescriptionRequestReason': instance.prescriptionRequestReason,
      'prescriptionState': instance.prescriptionState,
      'prescriptionRequestCategory': instance.prescriptionRequestCategory,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'telehealthRequestedMedications': instance.telehealthRequestedMedications,
      'prescriptionComment': instance.prescriptionComment,
      'appointmentTime': instance.appointmentTime,
      'isPrescriptionEdited': instance.isPrescriptionEdited,
      'doctorId': instance.doctorId,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
    };
