// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_signup_restricted_condition_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthSignUpRestrictedRequest _$TelehealthSignUpRestrictedRequestFromJson(
        Map<String, dynamic> json) =>
    TelehealthSignUpRestrictedRequest(
      id: json['id'] as int?,
      type: json['type'] as String?,
      isPrescriptionEdited: json['isPrescriptionEdited'] as bool?,
      prescriptionState: json['prescriptionState'] as String?,
      doctorId: json['doctorId'] as int?,
      teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
      telehealthAppointmentSlotId: json['telehealthAppointmentSlotId'] as int?,
      appointmentTime: json['appointmentTime'] as String?,
    );

Map<String, dynamic> _$TelehealthSignUpRestrictedRequestToJson(
        TelehealthSignUpRestrictedRequest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'isPrescriptionEdited': instance.isPrescriptionEdited,
      'prescriptionState': instance.prescriptionState,
      'doctorId': instance.doctorId,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
      'appointmentTime': instance.appointmentTime,
    };
