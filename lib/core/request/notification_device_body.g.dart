// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_device_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationDeviceBody _$NotificationDeviceBodyFromJson(
        Map<String, dynamic> json) =>
    NotificationDeviceBody(
      userId: json['userId'] as int?,
      deviceType: json['deviceType'] as String?,
      deviceId: json['deviceId'] as String?,
      deviceName: json['deviceName'] as String?,
      token: json['token'] as String?,
      oneSignalToken: json['oneSignalToken'] as String?,
      oneSignalId: json['oneSignalId'] as String?,
    );

Map<String, dynamic> _$NotificationDeviceBodyToJson(
    NotificationDeviceBody instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('userId', instance.userId);
  val['deviceType'] = instance.deviceType;
  val['deviceId'] = instance.deviceId;
  val['deviceName'] = instance.deviceName;
  val['token'] = instance.token;
  val['oneSignalToken'] = instance.oneSignalToken;
  val['oneSignalId'] = instance.oneSignalId;
  return val;
}
