import 'package:pocketpills/core/request/base_request.dart';

class SendInvitationMessageRequest extends BaseRequest {
  List<String>? phoneNumbers;

  SendInvitationMessageRequest({this.phoneNumbers});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'phoneNumbers': this.phoneNumbers,
      };
}
