// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refill_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefillRequest _$RefillRequestFromJson(Map<String, dynamic> json) =>
    RefillRequest(
      comment: json['comment'] as String?,
      medications: (json['medications'] as List<dynamic>?)
          ?.map((e) => MedicationRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RefillRequestToJson(RefillRequest instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'medications': instance.medications?.map((e) => e.toJson()).toList(),
    };
