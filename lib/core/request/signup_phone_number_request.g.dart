// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_phone_number_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupPhoneNumberRequest _$SignupPhoneNumberRequestFromJson(
        Map<String, dynamic> json) =>
    SignupPhoneNumberRequest(
      userIdentifier: json['userIdentifier'] as String,
    );

Map<String, dynamic> _$SignupPhoneNumberRequestToJson(
        SignupPhoneNumberRequest instance) =>
    <String, dynamic>{
      'userIdentifier': instance.userIdentifier,
    };
