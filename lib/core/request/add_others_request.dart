import 'package:pocketpills/core/request/base_request.dart';

class AddOthersRequest extends BaseRequest {
  String? prescriptionFlow;
  String? signupFlow;

  AddOthersRequest({this.prescriptionFlow,this.signupFlow});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'prescriptionFlow': this.prescriptionFlow,
        'signupFlow': this.signupFlow,
      };
}
