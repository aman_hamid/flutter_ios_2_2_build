// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_gender_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenderUpdateRequest _$GenderUpdateRequestFromJson(Map<String, dynamic> json) =>
    GenderUpdateRequest(
      gender: json['gender'] as String?,
    );

Map<String, dynamic> _$GenderUpdateRequestToJson(
        GenderUpdateRequest instance) =>
    <String, dynamic>{
      'gender': instance.gender,
    };
