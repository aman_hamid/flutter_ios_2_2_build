import 'package:pocketpills/core/request/add_phn_patient_address_request.dart';
import 'package:pocketpills/core/request/base_request.dart';

class AddPHNAndAddress extends BaseRequest {

  String? phn;
  String? healthCardExpiryDate;
  String? phone;
  String? gender;
  PatientPhnAddress? patientPhnAddress;

  AddPHNAndAddress({this.phn,this.healthCardExpiryDate,this.patientPhnAddress,this.phone,this.gender});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'phn': this.phn,
        'gender': this.gender,
        'phone': this.phone,
        'healthCardExpiryDate': this.healthCardExpiryDate == null ? null : healthCardExpiryDate,
        'patientAddress': this.patientPhnAddress == null ? null : patientPhnAddress!.toJson(),
      };
}
