// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_shopping_cart_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateShoppingCartRequest _$UpdateShoppingCartRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateShoppingCartRequest(
      addressId: json['addressId'] as int?,
      ccId: json['ccId'] as int?,
      coupon: json['coupon'] as String?,
      removeCoupon: json['removeCoupon'] as bool?,
      comment: json['comment'] as String?,
    );

Map<String, dynamic> _$UpdateShoppingCartRequestToJson(
        UpdateShoppingCartRequest instance) =>
    <String, dynamic>{
      'addressId': instance.addressId,
      'ccId': instance.ccId,
      'coupon': instance.coupon,
      'removeCoupon': instance.removeCoupon,
      'comment': instance.comment,
    };
