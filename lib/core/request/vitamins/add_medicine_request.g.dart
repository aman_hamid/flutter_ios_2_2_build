// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_medicine_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddMedicineRequest _$AddMedicineRequestFromJson(Map<String, dynamic> json) =>
    AddMedicineRequest(
      variantId: json['variantId'] as int?,
      monthsSupply: json['monthsSupply'] as int?,
      userSigCode: json['userSigCode'] as String?,
    );

Map<String, dynamic> _$AddMedicineRequestToJson(AddMedicineRequest instance) =>
    <String, dynamic>{
      'variantId': instance.variantId,
      'monthsSupply': instance.monthsSupply,
      'userSigCode': instance.userSigCode,
    };
