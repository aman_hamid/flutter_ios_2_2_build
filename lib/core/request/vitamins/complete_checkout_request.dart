import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'complete_checkout_request.g.dart';

@JsonSerializable(explicitToJson: true)
class CompleteCheckoutRequest extends BaseRequest {

  CompleteCheckoutRequest();

  factory CompleteCheckoutRequest.fromJson(Map<String, dynamic> json) =>
      _$CompleteCheckoutRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CompleteCheckoutRequestToJson(this);

}
