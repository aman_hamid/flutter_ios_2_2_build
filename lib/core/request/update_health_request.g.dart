// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_health_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateHealthRequest _$UpdateHealthRequestFromJson(Map<String, dynamic> json) =>
    UpdateHealthRequest(
      allergies: (json['allergies'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      patientId: json['patientId'] as int?,
      vitamins: (json['vitamins'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$UpdateHealthRequestToJson(
        UpdateHealthRequest instance) =>
    <String, dynamic>{
      'allergies': instance.allergies,
      'patientId': instance.patientId,
      'vitamins': instance.vitamins,
    };
