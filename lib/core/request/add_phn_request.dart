import 'package:pocketpills/core/request/base_request.dart';

class AddPHNNumber extends BaseRequest {

  String? phn;

  AddPHNNumber({this.phn});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'phn': this.phn,
      };
}
