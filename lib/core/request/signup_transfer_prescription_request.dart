import 'package:pocketpills/core/request/base_request.dart';

class SignupTransferPrescriptionRequest extends BaseRequest {
  int? id;
  String? type;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  int? omsPrescriptionId;
  bool? isPrescriptionEdited;
  String? prescriptionComment;
  bool? isTransferAll;
  String? prescriptionState;
  String? placeId;

  SignupTransferPrescriptionRequest(
      {this.type, this.pharmacyName, this.pharmacyPhone, this.isTransferAll, this.prescriptionState, this.prescriptionComment, this.pharmacyAddress, this.placeId,this.id,this.omsPrescriptionId,this.isPrescriptionEdited});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': this.id,
        'type': this.type,
        'pharmacyName': this.pharmacyName,
        'prescriptionComment': this.prescriptionComment,
        'pharmacyPhone': this.pharmacyPhone,
        'pharmacyAddress': this.pharmacyAddress,
        'isTransferAll': this.isTransferAll.toString(),
        'prescriptionState': this.prescriptionState,
        'pharmacyGooglePlaceId': this.placeId,
        'omsPrescriptionId': this.omsPrescriptionId,
        'isPrescriptionEdited': this.isPrescriptionEdited
      };
}
