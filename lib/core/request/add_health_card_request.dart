import 'package:pocketpills/core/request/base_request.dart';

class AddHealthCardNumber extends BaseRequest {

  String? userId;
  String? patientId;
  String? insuranceType;
  String? cardNumber;

  AddHealthCardNumber({this.userId,this.patientId, this.insuranceType,this.cardNumber, });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'userId': this.userId,
        'patientId': this.patientId,
        'insuranceType': this.insuranceType,
        'cardNumber': this.cardNumber,
      };
}
