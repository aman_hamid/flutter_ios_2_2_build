// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceBody _$DeviceBodyFromJson(Map<String, dynamic> json) => DeviceBody(
      userId: json['userId'] as int?,
      deviceType: json['deviceType'] as String?,
      deviceId: json['deviceId'] as String?,
      deviceName: json['deviceName'] as String?,
      notificationToken: json['notificationToken'] as String?,
    );

Map<String, dynamic> _$DeviceBodyToJson(DeviceBody instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('userId', instance.userId);
  val['deviceType'] = instance.deviceType;
  val['deviceId'] = instance.deviceId;
  val['deviceName'] = instance.deviceName;
  val['notificationToken'] = instance.notificationToken;
  return val;
}
