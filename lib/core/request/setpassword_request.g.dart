// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setpassword_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SetPasswordRequest _$SetPasswordRequestFromJson(Map<String, dynamic> json) =>
    SetPasswordRequest(
      password: json['password'] as String?,
      token: json['token'] as String?,
      dob: json['dob'] as String?,
      loginBody: json['loginBody'] == null
          ? null
          : DeviceBody.fromJson(json['loginBody'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SetPasswordRequestToJson(SetPasswordRequest instance) =>
    <String, dynamic>{
      'password': instance.password,
      'token': instance.token,
      'dob': instance.dob,
      'loginBody': instance.loginBody?.toJson(),
    };
