import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'update_gender_request.g.dart';

@JsonSerializable()
class GenderUpdateRequest extends BaseRequest {
  String? gender;

  GenderUpdateRequest({this.gender});

  factory GenderUpdateRequest.fromJson(Map<String, dynamic> data) => _$GenderUpdateRequestFromJson(data);

  Map<String, dynamic> toJson() => _$GenderUpdateRequestToJson(this);
}
