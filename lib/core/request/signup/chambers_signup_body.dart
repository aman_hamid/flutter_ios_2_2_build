import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'chambers_signup_body.g.dart';

@JsonSerializable()
class ChambersSignupRequest extends BaseRequest {
  String? employerLandingPageRef;
  String? userIdentifier;

  ChambersSignupRequest({
    this.employerLandingPageRef,
    this.userIdentifier,
  });

  factory ChambersSignupRequest.fromJson(Map<String, dynamic> data) => _$ChambersSignupRequestFromJson(data);

  Map<String, dynamic> toJson() => _$ChambersSignupRequestToJson(this);
}
