import 'package:pocketpills/core/request/base_request.dart';

class AddProvinceRequest extends BaseRequest {
  String? province;
  String? signupFlow;

  AddProvinceRequest({
    this.province,
    this.signupFlow,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'province': this.province,
        'signupFlow': this.signupFlow,
      };
}
