import 'package:pocketpills/core/request/base_request.dart';

class PatientPhnAddress extends BaseRequest {

  String? city;
  String? postalCode;
  String? province;
  String? streetAddress;
  String? streetAddressLineTwo;

  PatientPhnAddress({this.city,this.postalCode,this.province,this.streetAddress,this.streetAddressLineTwo});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'city': this.city,
        'postalCode': this.postalCode,
        'province': this.province,
        'streetAddress': this.streetAddress,
        'streetAddressLineTwo': this.streetAddressLineTwo,
      };
}
