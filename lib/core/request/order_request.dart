import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/request/medicine_request.dart';

part 'order_request.g.dart';

@JsonSerializable(explicitToJson: true)
class OrderRequest extends BaseRequest {
  List<MedicineRequest> dinMedications;

  OrderRequest({required this.dinMedications});

  Map<String, dynamic> toJson() => _$OrderRequestToJson(this);
}
