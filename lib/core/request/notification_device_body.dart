import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

import 'package:device_info/device_info.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';

part 'notification_device_body.g.dart';

@JsonSerializable(explicitToJson: true)
class NotificationDeviceBody extends BaseRequest {
  @JsonKey(includeIfNull: false)
  int? userId;

  String? deviceType;
  String? deviceId;

  String? deviceName;
  String? token;
  String? oneSignalToken;
  String? oneSignalId;

  final DataStoreService dataStore = locator<DataStoreService>();

  init() async {
    if (Platform.isAndroid) {
      await _getAndroidInfo();
    } else if (Platform.isIOS) {
      await _getIOSInfo();
    }
  }

  NotificationDeviceBody(
      {required this.userId,
      required this.deviceType,
      required this.deviceId,
      required this.deviceName,
      required this.token,
      required this.oneSignalToken,
      required this.oneSignalId});

  factory NotificationDeviceBody.fromJson(Map<String, dynamic> json) => _$NotificationDeviceBodyFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationDeviceBodyToJson(this);

  _getAndroidInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    this.deviceType = 'ANDROID';
    this.deviceId = androidInfo.androidId;
    this.deviceName = androidInfo.manufacturer + ' ' + androidInfo.model;
    this.token = dataStore.readString(DataStoreService.FIREBASE_TOKEN)!;
  }

  _getIOSInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    this.deviceType = 'IOS';
    this.deviceId = iosInfo.identifierForVendor;
    this.deviceName = iosInfo.localizedModel;
    this.token = dataStore.readString(DataStoreService.FIREBASE_TOKEN)!;
  }
}
