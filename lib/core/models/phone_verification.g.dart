// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_verification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneVerification _$PhoneVerificationFromJson(Map<String, dynamic> json) =>
    PhoneVerification(
      hint: json['hint'] as String?,
      userMessage: json['userMessage'] as String?,
      redirect: json['redirect'] as String?,
      signUpType: json['signUpType'] as String?,
      userId: json['userId'] as int?,
    );

Map<String, dynamic> _$PhoneVerificationToJson(PhoneVerification instance) =>
    <String, dynamic>{
      'hint': instance.hint,
      'userMessage': instance.userMessage,
      'redirect': instance.redirect,
      'signUpType': instance.signUpType,
      'userId': instance.userId,
    };
