import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/telehealth_doctor_time_slots.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'tele_health_appointment_time.g.dart';

@JsonSerializable()
class AppointmentTime {
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime? actualAppointmentTime;
  String? appointmentDate;
  int? daysFromCurrentDate;
  bool? available;
  String? clinicName;
  String? displayDate;
  String? doctorName;
  int? index;
  List<AppointmentDoctorSlots>? doctorTimeSlots;
  String? status;
  String? timeSlot;
  String? slotDate;
  int? doctorId;
  Map<String, dynamic>? timeSlots;

  AppointmentTime({
    this.actualAppointmentTime,
    this.appointmentDate,
    this.daysFromCurrentDate,
    this.available,
    this.clinicName,
    this.displayDate,
    this.doctorName,
    this.index,
    this.doctorTimeSlots,
    this.timeSlot,
    this.doctorId,
    this.timeSlots,
    this.status,
    this.slotDate,
  });

  factory AppointmentTime.fromJson(Map<String, dynamic> json) => _$AppointmentTimeFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentTimeToJson(this);
}
