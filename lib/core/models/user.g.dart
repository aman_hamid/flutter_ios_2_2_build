// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      phone: json['phone'] as int?,
      email: json['email'] as String?,
      countryCode: json['countryCode'] as int?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      gender: json['gender'] as String?,
      isMailVerified: json['isMailVerified'] as bool?,
      birthDate: json['birthDate'] as int?,
      locale: json['locale'] as String?,
      province: json['province'] as String?,
      isCaregiver: json['isCaregiver'] as bool?,
      hasDailyMedication: json['hasDailyMedication'] as bool?,
      passwordRequestToken: json['passwordRequestToken'] as String?,
      userContact: json['userContact'] as Map<String, dynamic>?,
      id: json['id'] as int?,
      provider: json['provider'] as String?,
      disabled: json['disabled'] as bool?,
      verified: json['verified'] as bool?,
      hasPassword: json['hasPassword'] as bool?,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'phone': instance.phone,
      'email': instance.email,
      'countryCode': instance.countryCode,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'gender': instance.gender,
      'isMailVerified': instance.isMailVerified,
      'birthDate': instance.birthDate,
      'locale': instance.locale,
      'province': instance.province,
      'isCaregiver': instance.isCaregiver,
      'hasDailyMedication': instance.hasDailyMedication,
      'passwordRequestToken': instance.passwordRequestToken,
      'provider': instance.provider,
      'userContact': instance.userContact,
      'id': instance.id,
      'disabled': instance.disabled,
      'verified': instance.verified,
      'hasPassword': instance.hasPassword,
    };
