// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentCard _$PaymentCardFromJson(Map<String, dynamic> json) => PaymentCard(
      id: json['id'] as int?,
      isDefault: json['isDefault'] as bool?,
      patientId: json['patientId'] as int?,
      disabled: json['disabled'] as bool?,
      pharmacyCode: json['pharmacyCode'] as String?,
      userId: json['userId'] as int?,
      cardType: json['cardType'] as String?,
      expiry: json['expiry'] as String?,
      maskedNumber: json['maskedNumber'] as String?,
      paymentMode: json['paymentMode'] as String?,
    );

Map<String, dynamic> _$PaymentCardToJson(PaymentCard instance) =>
    <String, dynamic>{
      'id': instance.id,
      'cardType': instance.cardType,
      'disabled': instance.disabled,
      'expiry': instance.expiry,
      'paymentMode': instance.paymentMode,
      'isDefault': instance.isDefault,
      'maskedNumber': instance.maskedNumber,
      'patientId': instance.patientId,
      'pharmacyCode': instance.pharmacyCode,
      'userId': instance.userId,
    };
