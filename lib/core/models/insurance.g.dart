// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insurance.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Insurance _$InsuranceFromJson(Map<String, dynamic> json) => Insurance(
      id: json['id'] as int?,
      patientId: json['patientId'] as int?,
      disabled: json['disabled'] as bool?,
      primaryInsuranceBackImage: json['primaryInsuranceBackImage'] as String?,
      primaryInsuranceBackImageOriginal:
          json['primaryInsuranceBackImageOriginal'] as String?,
      primaryInsuranceFrontImage: json['primaryInsuranceFrontImage'] as String?,
      primaryInsuranceFrontImageOriginal:
          json['primaryInsuranceFrontImageOriginal'] as String?,
      provincialInsuranceBackImage:
          json['provincialInsuranceBackImage'] as String?,
      provincialInsuranceBackImageOriginal:
          json['provincialInsuranceBackImageOriginal'] as String?,
      provincialInsuranceFrontImage:
          json['provincialInsuranceFrontImage'] as String?,
      provincialInsuranceFrontImageOriginal:
          json['provincialInsuranceFrontImageOriginal'] as String?,
      secondaryInsuranceBackImage:
          json['secondaryInsuranceBackImage'] as String?,
      secondaryInsuranceBackImageOriginal:
          json['secondaryInsuranceBackImageOriginal'] as String?,
      secondaryInsuranceFrontImage:
          json['secondaryInsuranceFrontImage'] as String?,
      secondaryInsuranceFrontImageOriginal:
          json['secondaryInsuranceFrontImageOriginal'] as String?,
      tertiaryInsuranceFrontImage:
          json['tertiaryInsuranceFrontImage'] as String?,
      tertiaryInsuranceFrontImageOriginal:
          json['tertiaryInsuranceFrontImageOriginal'] as String?,
      tertiaryInsuranceBackImage: json['tertiaryInsuranceBackImage'] as String?,
      tertiaryInsuranceBackImageOriginal:
          json['tertiaryInsuranceBackImageOriginal'] as String?,
      quaternaryInsuranceFrontImage:
          json['quaternaryInsuranceFrontImage'] as String?,
      quaternaryInsuranceFrontImageOriginal:
          json['quaternaryInsuranceFrontImageOriginal'] as String?,
      quaternaryInsuranceBackImage:
          json['quaternaryInsuranceBackImage'] as String?,
      quaternaryInsuranceBackImageOriginal:
          json['quaternaryInsuranceBackImageOriginal'] as String?,
    );

Map<String, dynamic> _$InsuranceToJson(Insurance instance) => <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'patientId': instance.patientId,
      'primaryInsuranceBackImage': instance.primaryInsuranceBackImage,
      'primaryInsuranceBackImageOriginal':
          instance.primaryInsuranceBackImageOriginal,
      'primaryInsuranceFrontImage': instance.primaryInsuranceFrontImage,
      'primaryInsuranceFrontImageOriginal':
          instance.primaryInsuranceFrontImageOriginal,
      'provincialInsuranceBackImage': instance.provincialInsuranceBackImage,
      'provincialInsuranceBackImageOriginal':
          instance.provincialInsuranceBackImageOriginal,
      'provincialInsuranceFrontImage': instance.provincialInsuranceFrontImage,
      'provincialInsuranceFrontImageOriginal':
          instance.provincialInsuranceFrontImageOriginal,
      'secondaryInsuranceBackImage': instance.secondaryInsuranceBackImage,
      'secondaryInsuranceBackImageOriginal':
          instance.secondaryInsuranceBackImageOriginal,
      'secondaryInsuranceFrontImage': instance.secondaryInsuranceFrontImage,
      'secondaryInsuranceFrontImageOriginal':
          instance.secondaryInsuranceFrontImageOriginal,
      'tertiaryInsuranceFrontImage': instance.tertiaryInsuranceFrontImage,
      'tertiaryInsuranceFrontImageOriginal':
          instance.tertiaryInsuranceFrontImageOriginal,
      'tertiaryInsuranceBackImage': instance.tertiaryInsuranceBackImage,
      'tertiaryInsuranceBackImageOriginal':
          instance.tertiaryInsuranceBackImageOriginal,
      'quaternaryInsuranceFrontImage': instance.quaternaryInsuranceFrontImage,
      'quaternaryInsuranceFrontImageOriginal':
          instance.quaternaryInsuranceFrontImageOriginal,
      'quaternaryInsuranceBackImage': instance.quaternaryInsuranceBackImage,
      'quaternaryInsuranceBackImageOriginal':
          instance.quaternaryInsuranceBackImageOriginal,
    };
