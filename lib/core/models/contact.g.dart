// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contact _$ContactFromJson(Map<String, dynamic> json) => Contact(
      id: json['id'] as int?,
      userId: json['userId'] as int?,
      day: json['day'] as String?,
      disabled: json['disabled'] as bool?,
      timeHigh: json['timeHigh'] as String?,
      timeLow: json['timeLow'] as String?,
      prescriptionPreference: json['prescriptionPreference'] as String?,
      deliveryBy: json['deliveryBy'] as String?,
      userFlow: json['userFlow'] as String?,
      entityId: json['entityId'] as int?,
    );

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'day': instance.day,
      'timeHigh': instance.timeHigh,
      'timeLow': instance.timeLow,
      'userId': instance.userId,
      'id': instance.id,
      'disabled': instance.disabled,
      'prescriptionPreference': instance.prescriptionPreference,
      'deliveryBy': instance.deliveryBy,
      'userFlow': instance.userFlow,
      'entityId': instance.entityId,
    };
