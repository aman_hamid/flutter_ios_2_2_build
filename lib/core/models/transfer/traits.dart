import 'package:json_annotation/json_annotation.dart';
part 'traits.g.dart';

@JsonSerializable()
class Traits {
  @JsonKey(name: 'ip_address')
  String? ip;

  Traits({this.ip});

  factory Traits.fromJson(Map<String, dynamic> json) => _$TraitsFromJson(json);
}
