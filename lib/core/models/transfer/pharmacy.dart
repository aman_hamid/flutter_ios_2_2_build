import 'package:json_annotation/json_annotation.dart';
part 'pharmacy.g.dart';

@JsonSerializable()
class Pharmacy {
  String? pharmacyName;
  String? pharmacyAddress;
  String? pharmacyPlaceId;
  String? postalCode;
  int? searchIndex;

  Pharmacy({required this.pharmacyName, required this.pharmacyPlaceId, required this.pharmacyAddress, this.postalCode, this.searchIndex});

  factory Pharmacy.fromJson(Map<String, dynamic> json) => _$PharmacyFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> jsonMap = Map();
    jsonMap['pharmacyName'] = this.pharmacyName.toString();
    jsonMap['pharmacyAddress'] = this.pharmacyAddress.toString();
    jsonMap['pharmacyPlaceId'] = this.pharmacyPlaceId.toString();
    jsonMap['postalCode'] = this.postalCode ?? "";
    jsonMap['searchIndex'] = this.searchIndex ?? null;
    return jsonMap;
  }
}
