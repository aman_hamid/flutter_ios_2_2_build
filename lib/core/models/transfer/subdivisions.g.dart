// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subdivisions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Subdivisions _$SubdivisionsFromJson(Map<String, dynamic> json) => Subdivisions(
      json['names'] == null
          ? null
          : SubdivisionsName.fromJson(json['names'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubdivisionsToJson(Subdivisions instance) =>
    <String, dynamic>{
      'names': instance.names,
    };
