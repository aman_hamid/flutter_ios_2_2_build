import 'package:json_annotation/json_annotation.dart';
part 'city_name.g.dart';

@JsonSerializable()
class CityName {
  String? en;

  CityName({this.en});

  factory CityName.fromJson(Map<String, dynamic> json) => _$CityNameFromJson(json);
}
