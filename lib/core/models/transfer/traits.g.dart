// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'traits.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Traits _$TraitsFromJson(Map<String, dynamic> json) => Traits(
      ip: json['ip_address'] as String?,
    );

Map<String, dynamic> _$TraitsToJson(Traits instance) => <String, dynamic>{
      'ip_address': instance.ip,
    };
