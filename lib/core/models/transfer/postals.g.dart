// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'postals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Postal _$PostalFromJson(Map<String, dynamic> json) => Postal(
      code: json['code'] as String?,
    );

Map<String, dynamic> _$PostalToJson(Postal instance) => <String, dynamic>{
      'code': instance.code,
    };
