// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_name.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CityName _$CityNameFromJson(Map<String, dynamic> json) => CityName(
      en: json['en'] as String?,
    );

Map<String, dynamic> _$CityNameToJson(CityName instance) => <String, dynamic>{
      'en': instance.en,
    };
