// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subdivisions_name.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubdivisionsName _$SubdivisionsNameFromJson(Map<String, dynamic> json) =>
    SubdivisionsName(
      json['en'] as String,
    );

Map<String, dynamic> _$SubdivisionsNameToJson(SubdivisionsName instance) =>
    <String, dynamic>{
      'en': instance.en,
    };
