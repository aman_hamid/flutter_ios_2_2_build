import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer/city_name.dart';
part 'cities.g.dart';

@JsonSerializable()
class City {
  CityName? names;

  City({this.names});

  factory City.fromJson(Map<String, dynamic> json) => _$CityFromJson(json);
}
