class TransferSearchSelectModel {
  String? selectedPlaceId;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  String? pharmacyPostalCode;
  String? pharmacyProvince;
  String? comments;
  String? medicineName;
  String? prescriptionState;
  int? quantity;
  bool? transferAll;
  bool? isSignup;
  int? id;
  int? omsPrescriptionId;
  bool? isPrescriptionEdited;

  TransferSearchSelectModel({
    this.selectedPlaceId,
    this.pharmacyName,
    this.pharmacyAddress,
    this.pharmacyPhone,
    this.pharmacyPostalCode,
    this.pharmacyProvince,
    this.comments,
    this.medicineName,
    this.prescriptionState,
    this.quantity,
    this.transferAll,
    this.isSignup,
    this.id,
    this.omsPrescriptionId,
    this.isPrescriptionEdited,
  });
}
