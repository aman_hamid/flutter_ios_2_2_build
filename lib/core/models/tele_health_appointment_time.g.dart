// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tele_health_appointment_time.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentTime _$AppointmentTimeFromJson(Map<String, dynamic> json) =>
    AppointmentTime(
      actualAppointmentTime:
          PPDateUtils.fromStr(json['actualAppointmentTime'] as String?),
      appointmentDate: json['appointmentDate'] as String?,
      daysFromCurrentDate: json['daysFromCurrentDate'] as int?,
      available: json['available'] as bool?,
      clinicName: json['clinicName'] as String?,
      displayDate: json['displayDate'] as String?,
      doctorName: json['doctorName'] as String?,
      index: json['index'] as int?,
      doctorTimeSlots: (json['doctorTimeSlots'] as List<dynamic>?)
          ?.map(
              (e) => AppointmentDoctorSlots.fromJson(e as Map<String, dynamic>))
          .toList(),
      timeSlot: json['timeSlot'] as String?,
      doctorId: json['doctorId'] as int?,
      timeSlots: json['timeSlots'] as Map<String, dynamic>?,
      status: json['status'] as String?,
      slotDate: json['slotDate'] as String?,
    );

Map<String, dynamic> _$AppointmentTimeToJson(AppointmentTime instance) =>
    <String, dynamic>{
      'actualAppointmentTime':
          PPDateUtils.toStr(instance.actualAppointmentTime),
      'appointmentDate': instance.appointmentDate,
      'daysFromCurrentDate': instance.daysFromCurrentDate,
      'available': instance.available,
      'clinicName': instance.clinicName,
      'displayDate': instance.displayDate,
      'doctorName': instance.doctorName,
      'index': instance.index,
      'doctorTimeSlots': instance.doctorTimeSlots,
      'status': instance.status,
      'timeSlot': instance.timeSlot,
      'slotDate': instance.slotDate,
      'doctorId': instance.doctorId,
      'timeSlots': instance.timeSlots,
    };
