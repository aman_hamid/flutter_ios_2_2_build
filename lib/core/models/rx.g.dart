// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rx.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rx _$RxFromJson(Map<String, dynamic> json) => Rx(
      createdDate: json['createdDate'] as String?,
      days: json['days'] as int?,
      din: json['din'] as String?,
      doctorFirstName: json['doctorFirstName'] as String?,
      doctorLastName: json['doctorLastName'] as String?,
      drug: json['drug'] as String?,
      filledQty: json['filledQty'] as num?,
      medicationId: json['medicationId'] as int?,
      phnNumber: json['phnNumber'] as String?,
      preApprovedRefillCount: json['preApprovedRefillCount'] as int?,
      prescriptionId: json['prescriptionId'] as int?,
      qty: json['qty'] as num?,
      refillAvailable: json['refillAvailable'] as bool?,
      remainingQty: json['remainingQty'] as num?,
      rxId: json['rxId'] as int?,
      rxStop: json['rxStop'] as String?,
      sig: json['sig'] as String?,
      validTill: json['validTill'] as String?,
      copay: json['copay'] as num?,
      cost: json['cost'] as num?,
      insurance: json['insurance'] as num?,
      pharmaCare: json['pharmaCare'] as num?,
      quantity: json['quantity'] as num?,
      refillId: json['refillId'] as int?,
    );

Map<String, dynamic> _$RxToJson(Rx instance) => <String, dynamic>{
      'medicationId': instance.medicationId,
      'rxId': instance.rxId,
      'prescriptionId': instance.prescriptionId,
      'createdDate': instance.createdDate,
      'rxStop': instance.rxStop,
      'validTill': instance.validTill,
      'drug': instance.drug,
      'din': instance.din,
      'doctorFirstName': instance.doctorFirstName,
      'doctorLastName': instance.doctorLastName,
      'phnNumber': instance.phnNumber,
      'refillAvailable': instance.refillAvailable,
      'remainingQty': instance.remainingQty,
      'filledQty': instance.filledQty,
      'qty': instance.qty,
      'sig': instance.sig,
      'preApprovedRefillCount': instance.preApprovedRefillCount,
      'days': instance.days,
      'refillId': instance.refillId,
      'quantity': instance.quantity,
      'copay': instance.copay,
      'pharmaCare': instance.pharmaCare,
      'insurance': instance.insurance,
      'cost': instance.cost,
    };
