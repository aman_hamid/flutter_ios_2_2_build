import 'package:json_annotation/json_annotation.dart';

part 'medicine.g.dart';

@JsonSerializable()
class Medicine {
  final String? drug;
  final String? din;
  final String? name;
  final String? note;
  final int? icon;
  final String? drugType;
  final String? um;
  final int? dosage;
  final String? sig;
  final String? sigCode;
  final num? quantity;
  final num? cost;
  final String? trade;
  final String? manufacturer;
  final String? drugCategory;
  final String? drugShelf;
  final num? markUp;
  final num? flatMarkup;
  final String? description;
  final num? defaultQuantity;
  final num? packSize;
  final num? costPrice;
  final num? unitPrice;
  final num? price;
  final num? dispensingFee;
  final num? fee;
  final String? benefits;
  final String? typeDesc;
  final String? dgPrice;
  final String? details;
  final String? id;
  final int? type;
  final int? variantId;
  final String? highlight;
  final String? userSigCode;
  final String? drugName;
  final String? recommendedDailyDosage;
  final String? benefitsHeadline;
  final String? naturalSources;
  final String? supportingStudies;
  final String? itemName;
  final String? benefitsHeadlineIconUrl;
  final num? variantListingPrice;

  bool? isInShoppingCart = false;
  int? monthsSupply;
  String? modifyuserSigCode;
  num? totalPrice;
  num? modifyQuantity;
  num? listPrice;
  num? drugTotalPrice;

  Medicine(
      {this.drug,
      this.din,
      this.name,
      this.note,
      this.icon,
      this.drugType,
      this.um,
      this.dosage,
      this.sig,
      this.sigCode,
      this.quantity,
      this.cost,
      this.trade,
      this.manufacturer,
      this.drugCategory,
      this.drugShelf,
      this.markUp,
      this.flatMarkup,
      this.description,
      this.defaultQuantity,
      this.packSize,
      this.costPrice,
      this.unitPrice,
      this.price,
      this.dispensingFee,
      this.fee,
      this.benefits,
      this.typeDesc,
      this.dgPrice,
      this.details,
      this.id,
      this.type,
      this.variantId,
      this.highlight,
      this.userSigCode,
      this.drugName,
      this.recommendedDailyDosage,
      this.benefitsHeadline,
      this.naturalSources,
      this.supportingStudies,
      this.isInShoppingCart,
      this.monthsSupply,
      this.totalPrice,
      this.listPrice,
      this.drugTotalPrice,
      this.itemName,
      this.benefitsHeadlineIconUrl,
      this.variantListingPrice});

  getDrugCost({num quantity = 0}) {
    if (quantity == 0) return 0;
    return (quantity * price! + (flatMarkup != null ? flatMarkup! : 0));
  }

  getTotalCost({bool? coverDispensingFee, num? quantity, int? insuranceCover}) {
    if (quantity == 0) return 0;

    num drugCost = this.getDrugCost(quantity: quantity!);

    num totalCost = (coverDispensingFee == true)
        ? ((drugCost + dispensingFee!) - ((drugCost + dispensingFee!) * insuranceCover! / 100))
        : (((drugCost) - ((drugCost) * insuranceCover! / 100)) + dispensingFee!);

    return totalCost;
  }

  String getDrugTypeSource() {
    switch (type) {
      case 1:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/bottle.png';
        break;
      case 2:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/box.png';
        break;
      case 3:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/capsule.png';
        break;
      case 4:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/cream.png';
        break;
      case 5:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/inhaler.png';
        break;
      case 6:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/ring.png';
        break;
      case 7:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/spray.png';
        break;
      case 8:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/syringe.png';
        break;
      case 9:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/tablet.png';
        break;
      case 10:
        return 'https://static.pocketpills.com/webapp/img/new-ui/icon/medicines/vial.png';
        break;
      default:
        return 'https://static.pocketpills.com/webapp/img/box.png';
        break;
    }
  }

  factory Medicine.fromJson(Map<String, dynamic> json) => _$MedicineFromJson(json);

  Map<String, dynamic> toJson() => _$MedicineToJson(this);
}
