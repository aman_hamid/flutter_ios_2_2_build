// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupDto _$SignupDtoFromJson(Map<String, dynamic> json) => SignupDto(
      password: json['password'] as String?,
      phone: json['phone'] as int?,
      email: json['email'] as String?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      gender: json['gender'] as String?,
      hasPassword: json['hasPassword'] as bool?,
      maskedEmail: json['maskedEmail'] as bool?,
      birthDate: json['birthDate'] as String?,
      hasDailyMedication: json['hasDailyMedication'] as bool?,
      isCaregiver: json['isCaregiver'] as bool?,
      province: json['province'] as String?,
      isMailVerified: json['isMailVerified'] as bool?,
      locale: json['locale'] as String?,
      verified: json['verified'] as bool?,
      signUpType: json['signUpType'] as String?,
      prescriptionFlow: json['prescriptionFlow'] as String?,
      signupFlow: json['signupFlow'] as String?,
      provider: json['provider'] as String?,
      hasHealthCard: json['hasHealthCard'] as bool?,
      healthCardExpiryDate: json['healthCardExpiryDate'] as String?,
      patientAddress: json['patientAddress'] == null
          ? null
          : Address.fromJson(json['patientAddress'] as Map<String, dynamic>),
      phn: json['phn'] as String?,
    );

Map<String, dynamic> _$SignupDtoToJson(SignupDto instance) => <String, dynamic>{
      'phone': instance.phone,
      'email': instance.email,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'gender': instance.gender,
      'password': instance.password,
      'birthDate': instance.birthDate,
      'hasDailyMedication': instance.hasDailyMedication,
      'hasHealthCard': instance.hasHealthCard,
      'hasPassword': instance.hasPassword,
      'maskedEmail': instance.maskedEmail,
      'isCaregiver': instance.isCaregiver,
      'province': instance.province,
      'provider': instance.provider,
      'isMailVerified': instance.isMailVerified,
      'locale': instance.locale,
      'verified': instance.verified,
      'signUpType': instance.signUpType,
      'prescriptionFlow': instance.prescriptionFlow,
      'signupFlow': instance.signupFlow,
      'phn': instance.phn,
      'healthCardExpiryDate': instance.healthCardExpiryDate,
      'patientAddress': instance.patientAddress,
    };
