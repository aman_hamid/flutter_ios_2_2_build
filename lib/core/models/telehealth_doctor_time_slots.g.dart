// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_doctor_time_slots.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentDoctorSlots _$AppointmentDoctorSlotsFromJson(
        Map<String, dynamic> json) =>
    AppointmentDoctorSlots(
      actualApptTime: json['actualApptTime'] as String?,
      doctorId: json['doctorId'] as int?,
      doctorName: json['doctorName'] as String?,
      startTime: json['startTime'] as String?,
      teleHealthDoctorDayPlanId: json['teleHealthDoctorDayPlanId'] as int?,
      telehealthAppointmentSlotId: json['telehealthAppointmentSlotId'] as int?,
    );

Map<String, dynamic> _$AppointmentDoctorSlotsToJson(
        AppointmentDoctorSlots instance) =>
    <String, dynamic>{
      'actualApptTime': instance.actualApptTime,
      'doctorId': instance.doctorId,
      'doctorName': instance.doctorName,
      'startTime': instance.startTime,
      'teleHealthDoctorDayPlanId': instance.teleHealthDoctorDayPlanId,
      'telehealthAppointmentSlotId': instance.telehealthAppointmentSlotId,
    };
