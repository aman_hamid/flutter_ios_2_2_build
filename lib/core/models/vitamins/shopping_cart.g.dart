// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopping_cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoppingCart _$ShoppingCartFromJson(Map<String, dynamic> json) => ShoppingCart(
      json['id'] as int?,
      json['disabled'] as bool?,
      json['version'] as int?,
      json['patientId'] as int?,
      (json['shoppingCartItems'] as List<dynamic>?)
          ?.map((e) => ShoppingCartItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['addressId'] as int?,
      json['ccId'] as int?,
      json['listPrice'] as num?,
      json['totalPrice'] as num?,
    );

Map<String, dynamic> _$ShoppingCartToJson(ShoppingCart instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'version': instance.version,
      'patientId': instance.patientId,
      'shoppingCartItems': instance.shoppingCartItems,
      'addressId': instance.addressId,
      'ccId': instance.ccId,
      'listPrice': instance.listPrice,
      'totalPrice': instance.totalPrice,
    };
