import 'package:json_annotation/json_annotation.dart';

part 'item_group_filter_list.g.dart';

@JsonSerializable()
class ItemGroupFilterList {
  int? id;
  String? itemGroupName;
  String? itemGroupIconUrl;
  bool? isInVitaminFilter;

  ItemGroupFilterList(this.id, this.itemGroupName, this.itemGroupIconUrl, this.isInVitaminFilter);

  factory ItemGroupFilterList.fromJson(Map<String, dynamic> json) => _$ItemGroupFilterListFromJson(json);
}
