import 'package:json_annotation/json_annotation.dart';

part 'transfer_prescription.g.dart';

@JsonSerializable()
class TransferPrescription {
  int? id;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  String? prescriptionState;
  String? pharmacyGooglePlaceId;
  String? appointmentDisplayDate;
  String? type;
  int? omsPrescriptionId;
  bool? isPrescriptionEdited;
  int? appointmentTime;
  double? revenue;

  TransferPrescription(
      {this.id, this.prescriptionState, this.pharmacyName, this.pharmacyAddress, this.pharmacyPhone, this.omsPrescriptionId, this.type, this.appointmentTime, this.revenue,this.pharmacyGooglePlaceId,this.isPrescriptionEdited,this.appointmentDisplayDate});

  factory TransferPrescription.fromJson(Map<String, dynamic> json) => _$TransferPrescriptionFromJson(json);

  Map<String, dynamic> toJson() => _$TransferPrescriptionToJson(this);
}
