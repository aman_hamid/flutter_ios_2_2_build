import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_doctor_time_slots.g.dart';

@JsonSerializable()
class AppointmentDoctorSlots {
  String? actualApptTime;
  int? doctorId;
  String? doctorName;
  String? startTime;
  int? teleHealthDoctorDayPlanId;
  int? telehealthAppointmentSlotId;


  AppointmentDoctorSlots({
    this.actualApptTime,
    this.doctorId,
    this.doctorName,
    this.startTime,
    this.teleHealthDoctorDayPlanId,
    this.telehealthAppointmentSlotId,
  });

  factory AppointmentDoctorSlots.fromJson(Map<String, dynamic> json) => _$AppointmentDoctorSlotsFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentDoctorSlotsToJson(this);
}
