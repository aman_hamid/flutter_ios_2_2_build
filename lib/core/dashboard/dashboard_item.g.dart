// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DashboardItem _$DashboardItemFromJson(Map<String, dynamic> json) =>
    DashboardItem(
      title: json['title'] as String?,
      description: json['description'] as String?,
      action: json['action'] as String?,
      cardType: json['cardType'] as String?,
      icon: json['icon'] as String?,
      tagName: json['tagName'] as String?,
      image: json['image'] as String?,
      itemId: json['itemId'] as String?,
      id: json['id'] as int?,
      trackActionComplete: json['trackActionComplete'] as bool?,
      colorSchema: json['colorSchema'] == null
          ? null
          : ColorSchema.fromJson(json['colorSchema'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DashboardItemToJson(DashboardItem instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'action': instance.action,
      'cardType': instance.cardType,
      'icon': instance.icon,
      'tagName': instance.tagName,
      'image': instance.image,
      'itemId': instance.itemId,
      'id': instance.id,
      'trackActionComplete': instance.trackActionComplete,
      'colorSchema': instance.colorSchema,
    };
