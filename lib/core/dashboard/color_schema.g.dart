// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'color_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ColorSchema _$ColorSchemaFromJson(Map<String, dynamic> json) => ColorSchema(
      name: json['name'] as String?,
    );

Map<String, dynamic> _$ColorSchemaToJson(ColorSchema instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
