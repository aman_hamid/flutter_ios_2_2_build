// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'header.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Header _$HeaderFromJson(Map<String, dynamic> json) => Header(
      heading: json['heading'] as String?,
      buttonAction: json['buttonAction'] as String?,
      buttonText: json['buttonText'] as String?,
      headingType: json['headingType'] as String?,
    );

Map<String, dynamic> _$HeaderToJson(Header instance) => <String, dynamic>{
      'heading': instance.heading,
      'buttonAction': instance.buttonAction,
      'buttonText': instance.buttonText,
      'headingType': instance.headingType,
    };
