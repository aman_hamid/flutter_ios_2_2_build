// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_item_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DashboardItemList _$DashboardItemListFromJson(Map<String, dynamic> json) =>
    DashboardItemList(
      layoutType: json['layoutType'] as String?,
      header: json['header'] == null
          ? null
          : Header.fromJson(json['header'] as Map<String, dynamic>),
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => DashboardItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DashboardItemListToJson(DashboardItemList instance) =>
    <String, dynamic>{
      'layoutType': instance.layoutType,
      'header': instance.header,
      'data': instance.data,
    };
