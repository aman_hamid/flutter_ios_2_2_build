import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class LoginService {
  final HttpApi _api = locator<HttpApi>();
  static final DataStoreService dataStore = locator<DataStoreService>();
  static final Analytics analyticsEvents = locator<Analytics>();

  final Base64Codec base64 = Base64Codec();

  Future<Response> login(String phoneNumber, String password) async {
    DeviceBody loginRequest = DeviceBody.fromDeviceBody();
    var stringToBeEncoded = phoneNumber + ":" + password;
    var encodedString = "Basic " + base64.encode(utf8.encode(stringToBeEncoded));
    await dataStore.saveAuthorizationToken(encodedString);
    await loginRequest.init();
    var response = await _api.postLogin(encodedString, RequestWrapper(body: loginRequest));
    await loginUser(response);
    return response;
  }

  //Response should have a valid LoginResponse to login user
  Future<bool> loginUser(Response response) async {
    if (response != null && response.data['details'] != null) {
      BaseResponse<LoginResponse> loginResponse = BaseResponse<LoginResponse>.fromJson(response.data);
      await dataStore.writeInt(DataStoreService.USERID, loginResponse.response!.userId!);

      if (loginResponse.response!.userPatientList!.length != null && loginResponse.response!.userPatientList!.length > 0) {
        await dataStore.writeInt(DataStoreService.PATIENTID, loginResponse.response!.userPatientList![0].patientId!);
        try {
          await dataStore.writeString(DataStoreService.FIRSTNAME, loginResponse.response!.userPatientList![0].patient!.firstName!);
          await dataStore.writeString(DataStoreService.EMAIL, loginResponse.response!.userPatientList![0].patient!.email!);
          await dataStore.writeString(DataStoreService.LASTNAME, loginResponse.response!.userPatientList![0].patient!.lastName!);
          await dataStore.writeString(DataStoreService.LANGUAGE, ViewConstants.languageId);
          await dataStore.writeString(DataStoreService.PROVINCE, loginResponse.response!.userPatientList![0].patient!.province!);
          await dataStore.writeString(DataStoreService.DATE_OF_BIRTH, loginResponse.response!.userPatientList![0].patient!.birthDate!);
          await dataStore.writeString(DataStoreService.ZIP, loginResponse.response!.userPatientList![0].patient!.countryCode!.toString());
          await dataStore.writeString(DataStoreService.GENDER, loginResponse.response!.userPatientList![0].patient!.gender!);
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
      try {
        await dataStore.saveAuthorizationToken(response.headers['Authorization']![0]);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
      try {
        await getAuthCookie(response);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
      await dataStore.markSelectedPatientAsCaregiver(true);
      await dataStore.writeBoolean(DataStoreService.IS_LOGGED_IN, true);
      analyticsEvents.setUserIdentifiers();
      return true;
    }
    return false;
  }

  Future<bool> registerUser(Response response) async {
    if (response != null) {
      BaseResponse<SignupResponse?> res = BaseResponse<SignupResponse?>.fromJson(response.data);
      if (res.response!.userId != null) await dataStore.writeInt(DataStoreService.USERID, res.response!.userId!);
      if (res.response!.patientId != null) await dataStore.writeInt(DataStoreService.PATIENTID, res.response!.patientId!);
      SignupDto dto = res.response!.signupDto!;
      await dataStore.writeString(DataStoreService.FIRSTNAME, dto.firstName ?? "");
      await dataStore.writeString(DataStoreService.EMAIL, dto.email ?? "");
      await dataStore.writeString(DataStoreService.LASTNAME, dto.lastName ?? "");
      await dataStore.writeString(DataStoreService.LANGUAGE, getCurrentLocale(dto));
      await dataStore.writeString(DataStoreService.PROVINCE, dto.province ?? "");
      await dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, dto.province ?? "");
      await dataStore.writeString(DataStoreService.PHN, dto.phn ?? "");
      await dataStore.writeString(DataStoreService.PHNExpiry, dto.healthCardExpiryDate ?? "");
      await dataStore.writeBoolean(DataStoreService.HAS_HEALTHCARD, dto.hasHealthCard ?? false);
      await dataStore.writeString(DataStoreService.DATE_OF_BIRTH, dto.birthDate ?? "");
      await dataStore.writeString(DataStoreService.GENDER, dto.gender ?? "");
      await dataStore.writeString(DataStoreService.PHONE, dto.phone == null ? "" : dto.phone.toString());
      await dataStore.writeBoolean(DataStoreService.HAS_PASSWORD, dto.hasPassword ?? false);
      await dataStore.writeBoolean(DataStoreService.VERIFIED, dto.verified ?? false);
      await dataStore.writeBoolean(DataStoreService.MASKED_EMAIL, dto.maskedEmail ?? false);
      await dataStore.writeString(DataStoreService.SIGNUP_TYPE, dto.signUpType ?? "");
      await dataStore.writeString(DataStoreService.PROVIDER, dto.provider??"");

      if(dto.province!=null && dto.province!.isNotEmpty){
        await dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, dto.province!.replaceAll("_", " ").capitalizeFirstOfEach());
      }

      try {
        await dataStore.saveAuthorizationToken(response.headers['Authorization']![0]);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }

      try {
        await getAuthCookie(response);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
      await dataStore.writeBoolean(DataStoreService.IS_LOGGED_IN, true);

      if (res.response!.prescription != null) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);

        if ((res.response!.prescription!.pharmacyAddress == null ||
                res.response!.prescription!.pharmacyName == null ||
                res.response!.prescription!.pharmacyPhone == null) &&
            res.response!.prescription!.prescriptionState == "PENDING") {
          await dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
          await dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, false);
          GlobalVariable().prescriptionId = res.response!.prescription!.omsPrescriptionId;
          GlobalVariable().userFlow = PrescriptionTypeConstant.SIGNUP;
        } else {
          await dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);
          await dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, false);
        }
      } else {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, false);
      }

      if ((dto.signUpType == null && dto.phone == null) || (dto.signUpType == null && dto.phone != null && dto.phone!.toString().isEmpty)) {
        await dataStore.writeBoolean(DataStoreService.PHONE_NUMBER_EMPTY, true);
      } else if ((dto.signUpType == "PHONE_BASED" && dto.phone == null) ||
          (dto.signUpType == "PHONE_BASED" && dto.phone != null && dto.phone!.toString().isEmpty)) {
        await dataStore.writeBoolean(DataStoreService.PHONE_NUMBER_EMPTY, true);
      } else if ((dto.signUpType == "PHONE_BASED" && dto.phone == null && dto.maskedEmail == true) ||
          (dto.signUpType == "PHONE_BASED" && dto.phone != null && dto.phone!.toString().isEmpty && dto.maskedEmail == true)) {
        await dataStore.writeBoolean(DataStoreService.PHONE_NUMBER_EMPTY, true);
      } else {
        await dataStore.writeBoolean(DataStoreService.PHONE_NUMBER_EMPTY, false);
      }

      if (["", null].contains(dto.firstName) || ["", null].contains(dto.lastName) || ["", null].contains(dto.birthDate)) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ABOUT_YOU, false);
      } else {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ABOUT_YOU, true);
      }

      if(dto.signupFlow != null && res.response!.prescription == null && dto.prescriptionFlow == null){
        await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING,true);
      }
      if((res.response!.prescription == null && dto.signupFlow == "TELEHEALTH") || (res.response!.prescription == null && dto.prescriptionFlow == "TELEHEALTH")){
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH,true);
      }
      if((res.response!.prescription == null && dto.signupFlow == "TRANSFER") || (res.response!.prescription == null && dto.prescriptionFlow == "TRANSFER")){
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER,true);
      }
      if((res.response!.prescription == null && dto.signupFlow == "UPLOAD") || (res.response!.prescription == null && dto.prescriptionFlow == "UPLOAD")){
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION,true);
      }

      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "PENDING")) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, false);
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, false);
      }

      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TRANSFER" && res.response!.prescription!.prescriptionState == "PENDING")) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, false);
        await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
        dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      }
      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED" &&
          (dto.phn != null && dto.hasHealthCard != null && dto.hasHealthCard == false && dto.patientAddress ==null))) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, true);
      }
      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED" &&
          (dto.phn == null && dto.hasHealthCard != null && dto.hasHealthCard == true && dto.patientAddress ==null))) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, true);
      }
      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED" &&
          (dto.phn != null && dto.hasHealthCard != null && dto.hasHealthCard == true && dto.patientAddress ==null))) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, true);
      }

      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED" &&
          (dto.phn != null && dto.hasHealthCard != null && dto.hasHealthCard == false && dto.patientAddress !=null))) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, false);
      }

      if ((res.response!.prescription != null &&
          res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED" &&
          (dto.phn != null && dto.hasHealthCard != null && dto.hasHealthCard == true && dto.patientAddress !=null))) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, false);
      }

      if (["", null].contains(dto.province) || ["", null].contains(dto.gender) || ["", null].contains(dto.email) || ["", null].contains(dto.phone)) {
        if (res.response!.prescription != null &&
            res.response!.prescription!.prescriptionState == "FILED" &&
            res.response!.prescription!.type.toString().toLowerCase() == "transfer") {
          await dataStore.writeString(DataStoreService.PRESCRIPTION_TYPE, "transfer");
          await dataStore.writeString(DataStoreService.PRESCRIPTION_FOR_TRANSFER, jsonEncode(res.response!.prescription!.toJson()));
          await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, false);
        }
        if (res.response!.prescription != null &&
            res.response!.prescription!.prescriptionState == "FILED" &&
            res.response!.prescription!.type.toString().toLowerCase() == "upload") {
          await dataStore.writeString(DataStoreService.PRESCRIPTION_TYPE, "prescription");
          await dataStore.writeString(DataStoreService.PRESCRIPTION_FOR_UPLOAD, jsonEncode(res.response!.documentsList!));
          await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, false);
        }
        if(dto.signupFlow == "REGULAR" && dto.prescriptionFlow == "NONE"){
          await dataStore.writeString(DataStoreService.PRESCRIPTION_TYPE, "add others");
          await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, false);
        }

        if (res.response!.prescription != null && res.response!.prescription!.type == "TELEHEALTH" && res.response!.prescription!.prescriptionState == "FILED") {
          await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, true);
        }
      } else {
        await dataStore.writeString(DataStoreService.PROVINCE, dto.province!);
        await dataStore.writeBoolean(DataStoreService.HAS_DAILY_MEDICATION, dto.hasDailyMedication ?? false);
        await dataStore.writeBoolean(DataStoreService.IS_CARE_GIVER, dto.isCaregiver ?? false);
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, true);
        if (dto.locale != null && dto.locale!.contains('QC')) {
          await dataStore.writeBoolean(DataStoreService.QUEBEC, true);
          PPApplication.quebecCheck = true;
        } else {
          await dataStore.writeBoolean(DataStoreService.QUEBEC, false);
          PPApplication.quebecCheck = false;
        }
      }

      if (dto.hasPassword == true && dto.verified == true) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_OTP_PASSWORD, true);
      } else if (dto.hasPassword == true && dto.isMailVerified == true) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_OTP_PASSWORD, true);
      } else if (dto.hasPassword == true && dto.provider != null) {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_OTP_PASSWORD, true);
      } else {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_OTP_PASSWORD, false);
      }

      analyticsEvents.setUserIdentifiers();
      return true;
    } else {
      return false;
    }
  }

  Future<void> getAuthCookie(Response response) async {
    if (response == null || response.headers == null || response.headers['set-cookie'] == null) return null;
    for (int i = 0; i < response.headers['set-cookie']!.length; i++) {
      if (response.headers['set-cookie']![i].contains("cookieAuthorization")) {
        return await dataStore.saveAuthorizationCookie(response.headers['set-cookie']![i].split(';')[0].split('=')[1]);
      }
    }
  }

  bool isUserLogin() {
    return dataStore.readBoolean(DataStoreService.IS_LOGGED_IN)!;
  }

  int? checkOutSignUpFlow() {
    bool signupCompleted = dataStore.readBoolean(DataStoreService.SIGNUP_COMPLETED)!;
    bool aboutYouDetails = dataStore.readBoolean(DataStoreService.COMPLETE_ABOUT_YOU)!;
    bool transferDetails = dataStore.readBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER)!;
    bool uploadPrescription = dataStore.readBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION)!;
    bool telehealthPreference = dataStore.readBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH)!;
    bool telehalthCard = dataStore.readBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD)!;
    bool almostDone = dataStore.readBoolean(DataStoreService.COMPLETE_ALMOST_DONE)!;
    bool otpPassword = dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD)!;
    bool phoneAdd = dataStore.readBoolean(DataStoreService.PHONE_NUMBER_EMPTY)!;
    bool customizedSignup = dataStore.readBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING)!;
    if (phoneAdd == true) {
      return SignupStepperStateEnums.SIGN_UP_PHONE_ADD;
    }else if (signupCompleted == true) {
      return SignupStepperStateEnums.SIGN_UP_COMPLETED;
    } else if (aboutYouDetails == false) {
      return SignupStepperStateEnums.SIGN_UP_ABOUT_YOU;
    } else if (customizedSignup == true) {
      return SignupStepperStateEnums.CUSTOMISED_SIGN_UP;
    } else if (transferDetails == true) {
      return SignupStepperStateEnums.SIGN_UP_TRANSFER;
    } else if (uploadPrescription == true) {
      return SignupStepperStateEnums.SIGN_UP_UPLOAD_PRESCRIPTION;
    } else if (telehealthPreference == true) {
      return SignupStepperStateEnums.SIGN_UP_TELEHEALTH;
    } else if (telehalthCard == true) {
      return SignupStepperStateEnums.SIGN_UP_TELEHEALTH_CARD;
    } else if (almostDone == false) {
      return SignupStepperStateEnums.SIGN_UP_ALMOST_DONE;
    } else if (otpPassword == false) {
      return SignupStepperStateEnums.SIGN_UP_OTP_PASSWORD;
    }
  }

  String getCurrentLocale(SignupDto dto) {
    print('AMANRES:${dto.locale}');
    if (LocalizationUtils.isProvinceQuebec()) {
      return (dto.locale == null || (dto.locale != null && dto.locale!.contains('en'))) ? ViewConstants.languageId : ViewConstants.languageIdFr;
    } else {
      return (dto.locale == null || (dto.locale != null && dto.locale!.contains('en'))) ? ViewConstants.languageId : ViewConstants.languageIdFr;
    }
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  }

  String capitalizeFirstOfEach() {
   return "${this.split(" ").map((str) => str.capitalize()).join(" ")}";
  }
}
