import 'package:intl/intl.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/home_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import '../../locator.dart';

class PatientUtils {
  static final DataStoreService dataStore = locator<DataStoreService>();

  static bool isPatientMinor(Patient patient) {
    if (patient.birthDate == null) {
      print("Cannot compute if a patient is minor without dob");
      return false;
    }

    var dob = new DateFormat("MM/dd/yyyy", "en_US").parse(patient.birthDate!);
    var diffDays = DateTime.now().difference(dob).inDays;
    var age = diffDays / 365;
    if (age < 14) return true;
    return false;
  }

  static String getYourOrNameTitle(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.your");
    Patient patient = userPatient.patient!;
    return patient.firstName! + "'s";
  }

  static String getYouOrNameTitle(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.you");
    Patient patient = userPatient.patient!;
    return patient.firstName!;
  }

  static String getYourOrName(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.your");
    Patient patient = userPatient.patient!;
    if (userPatient.primary == true)
      return LocalizationUtils.getSingleValueString("common", "common.all.your");
    else
      return patient.firstName! + "'s";
  }

  static String getPatientName(UserPatient? userPatient) {
    if (userPatient == null)
      return dataStore.readString(DataStoreService.FIRSTNAME)!;
    else {
      Patient patient = userPatient.patient!;
      return patient.firstName!;
    }
  }

  static String getYouOrName(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.you");
    Patient patient = userPatient.patient!;
    if (userPatient.primary == true)
      return LocalizationUtils.getSingleValueString("common", "common.all.you");
    else
      return patient.firstName!;
  }

  static String getYourOrNameWithoutApostrophe(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.your");
    Patient patient = userPatient.patient!;
    if (userPatient.primary == true)
      return LocalizationUtils.getSingleValueString("common", "common.all.your");
    else
      return patient.firstName!;
  }

  static String getPronounForGender(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.your");
    Patient patient = userPatient.patient!;
    String pronoun = patient.firstName! + "'s";
    if (userPatient.primary == true) {
      pronoun = LocalizationUtils.getSingleValueString("common", "common.all.your");
    } else {
      switch (patient.gender) {
        case "MALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.hisher.male");
          break;
        case "FEMALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.himher.female");
          break;
        case "OTHER":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.hisher.other");
          break;
      }
    }

    return pronoun;
  }

  static String getSecondPossessivePronounFromGender(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.my");
    Patient patient = userPatient.patient!;
    String pronoun = "";
    if (userPatient.primary == true) {
      pronoun = LocalizationUtils.getSingleValueString("common", "common.all.my");
    } else {
      switch (patient.gender) {
        case "MALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.hisher.male");
          break;
        case "FEMALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.himher.female");
          break;
        case "OTHER":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.hisher.other");
          break;
      }
    }
    return pronoun;
  }

  static String getFirstPossessivePronounFromGender(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.i-text");
    Patient patient = userPatient.patient!;
    String pronoun = "";
    if (userPatient.primary == true) {
      pronoun = LocalizationUtils.getSingleValueString("common", "common.all.i-text");
    } else {
      switch (patient.gender) {
        case "MALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.heshe.male");
          break;
        case "FEMALE":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.heshe.female");
          break;
        case "OTHER":
          pronoun = LocalizationUtils.getSingleValueString("common", "common.heshe.other");
          break;
      }
    }
    return pronoun;
  }

  static String getThey(UserPatient? userPatient) {
    if (userPatient == null) return LocalizationUtils.getSingleValueString("common", "common.all.you");
    String result = LocalizationUtils.getSingleValueString("common", "common.heshe.other");
    Patient patient = userPatient.patient!;
    if (userPatient.primary == true) {
      return LocalizationUtils.getSingleValueString("common", "common.all.you");
    } else
      switch (patient.gender) {
        case "MALE":
          result = LocalizationUtils.getSingleValueString("common", "common.heshe.male");
          break;
        case "FEMALE":
          result = LocalizationUtils.getSingleValueString("common", "common.heshe.female");
          break;
        case "OTHER":
          result = LocalizationUtils.getSingleValueString("common", "common.heshe.other");
          break;
      }
    return result;
  }

  /*static String getDoThey(UserPatient userPatient) {
    if (userPatient == null) return "Do you";
    String result = "Do they";
    Patient patient = userPatient.patient;
    if (userPatient.primary == true) {
      return "Do you";
    } else
      switch (patient.gender) {
        case "MALE":
          result = "Does he";
          break;
        case "FEMALE":
          result = "Does she";
          break;
        case "OTHER":
          result = "Do they";
          break;
      }
    return result;
  }*/

  /* static String getTheyAre(UserPatient userPatient) {
    if (userPatient == null) return "you are";
    String result = "they are";
    Patient patient = userPatient.patient;
    if (userPatient.primary == true) {
      return "you are";
    } else
      switch (patient.gender) {
        case "MALE":
          result = "he is";
          break;
        case "FEMALE":
          result = "she is";
          break;
        case "OTHER":
          result = "they are";
          break;
      }
    return result;
  }*/

  /*static String getTheyHave(UserPatient userPatient) {
    if (userPatient == null) return "you have";
    String result = "they have";
    Patient patient = userPatient.patient;
    if (userPatient.primary == true) {
      return "you have";
    } else
      switch (patient.gender) {
        case "MALE":
          result = "he has";
          break;
        case "FEMALE":
          result = "she has";
          break;
        case "OTHER":
          result = "they have";
          break;
      }
    return result;
  }*/

  static UserPatient getUserPatientFromList(List<UserPatient>? list, int? patientId) {
    UserPatient? result;
    for (var temp in list!) {
      if (temp.patientId == patientId) {
        result = temp;
        break;
      }
    }
    return result!;
  }

  static String getIncompleteFieldName(HomeResponse homeResponse) {
    if (homeResponse.hasInsurance == false) {
      return "Insurance";
    } else if (homeResponse.hasAddress == false) {
      return "Address";
    } else if (homeResponse.hasCreditCard == false) {
      return "Payment";
    } else if (homeResponse.hasHealthInformation == false) {
      return "Health";
    } else {
      return "";
    }
  }

  static String getIncompleteFieldNameCombination(HomeResponse homeResponse) {
    if (homeResponse.hasInsurance == false && homeResponse.hasAddress == false && homeResponse.hasCreditCard == false) {
      return "insurance, address and payment";
    } else if (homeResponse.hasInsurance == false && homeResponse.hasAddress == false) {
      return "insurance and address";
    } else if (homeResponse.hasAddress == false && homeResponse.hasCreditCard == false) {
      return "address and payment";
    } else if (homeResponse.hasInsurance == false && homeResponse.hasCreditCard == false) {
      return "insurance and payment";
    } else if (homeResponse.hasCreditCard == false) {
      return "payment";
    } else if (homeResponse.hasAddress == false) {
      return "address";
    } else if (homeResponse.hasInsurance == false) {
      return "insurance";
    } else if (homeResponse.hasHealthInformation == false) {
      return "health";
    } else {
      return "insurance, address and payment";
    }
  }

  static String getForGender(UserPatient? userPatient) {
    if (userPatient == null) return "your";
    if (userPatient.patient == null) return "your";
    Patient patient = userPatient.patient!;
    String pronoun = patient.firstName! + "'s";
    if (userPatient.primary == true) {
      pronoun = "your";
    } else {
      switch (patient.gender) {
        case "MALE":
          pronoun = "MALE";
          break;
        case "FEMALE":
          pronoun = "FEMALE";
          break;
        case "OTHER":
          pronoun = "OTHER";
          break;
      }
    }

    return pronoun;
  }
}
