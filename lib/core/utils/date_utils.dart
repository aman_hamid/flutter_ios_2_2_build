import 'package:intl/intl.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PPDateUtils {
  static DateTime? fromStr(String? dateStr) {
    if (dateStr == null) return null;
    return DateTime?.parse(dateStr);
  }

  static String? toStr(DateTime? date) {
    if (date == null) return null;
    return date.toIso8601String();
  }

  static String displayDate(DateTime? date) {
    if (date == null) return "";
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      return new DateFormat("LLL d, y", "en").format(date);
    } else {
      return new DateFormat("LLL d, y", "fr").format(date);
    }
  }

  static DateTime fromMMDDYYYYString(String dateStr) {
    try {
      List<String> match = dateStr.split("/");
      String newFormat = match[2] + match[0] + match[1];
      return DateTime.parse(newFormat);
    } catch (e) {
      print(e);
      return DateTime.now();
    }
  }

  static isAbove14(String mmddyyyy) {
    DateTime birthDate = PPDateUtils.fromMMDDYYYYString(mmddyyyy);
    Duration difference = DateTime.now().difference(birthDate);
    if (difference.inDays >= ((14 * 365) + 4)) return true;
    return false;
  }
}
