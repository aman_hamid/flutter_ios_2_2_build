import 'package:flutter/material.dart';

const Color brandColor = Color(0xff4e2a84);
const Color secondaryBrandColor = Color(0xff4e2a84);
const Color headerBgColor = Color(0xfff5f5f5);
const Color headerColor = Color(0xfffAfAfA);
const Color accentColor = Color(0xff9e2ddf);
const Color primaryColor = Color(0xff424242);
const Color blackColor = Color(0xff000000);
const Color timerColor = Color(0xff333333);
const Color secondaryColor = Color(0xff666666);
const Color tertiaryColor = Color(0xff999999);
const Color pinkColor = Color(0xffFF6485);
const Color quartiaryColor = Color(0xfff7f7f7);
const Color linkColor = Color(0xff4e2a84);
const Color googleColor = Color(0xff4384F4);
const Color errorColor = Color(0xffEB4646);
const Color maroonColor = Color(0xff7C0F27);
const Color lightBrownColor = Color(0xff54c7da);
const Color darkBrownColor = Color(0xff54c7da);
const Color warningColor = Color(0xffFFA700);
const Color yellowColor = Color(0xffffe234);
const Color successColor = Color(0xff2E7D32);
const Color borderColor = Color(0xffe0e0e0);
const Color greyColor = Color(0xffcccccc);
const Color whiteColor = Color(0xffffffff);
const Color lightBlueColor = Color(0xffE5E3FF);
const Color lightBlue2Color = Color(0xffF4F5F9);
const Color bghighlight = Color(0xffF0ECFF);
const Color bghighlight2 = Color(0xFFE5E3FF);
const Color lightGray = Color(0xFFE5E3FF);
const Color thinGrey = Color(0xFF747474);
const Color lightPurple = Color(0xFF8c60ff);
const Color lightPurple2 = Color(0xffa794ea);

const Color darkBlue = Color(0xff4e2a84);
const Color lightBlue = Color(0xff5061A0);

const Color lightPinkColor = Color(0xffFFC2CA);
const Color violetColorDark = Color(0xffAAA4FF);
const Color darkBlueColor = Color(0xff504774);
const Color peachColor = Color(0xffFFD9CA);
const Color violetColor = Color(0xffDFE4F8);
const Color darkPrimaryColor = Color(0xff333333);
const Color whiteOpacity = Color(0xc0ffffff);
const Color darkBlueColor2 = Color(0xff4e2a84);
const Color bottomSuccessColor = Color(0xA3666666);
const Color bottomSuccessPrimaryColor = Color(0x3333333);
const Color bgDateColor = Color(0xFFE8E8E8);

const MaterialColor primaryColorMain = const MaterialColor(
  0xff4e2a84,
  const <int, Color>{
    50: Color.fromRGBO(78, 42, 132, .1),
    100: Color.fromRGBO(78, 42, 132, .2),
    200: Color.fromRGBO(78, 42, 132, .3),
    300: Color.fromRGBO(78, 42, 132, .4),
    400: Color.fromRGBO(78, 42, 132, .5),
    500: Color.fromRGBO(78, 42, 132, .6),
    600: Color.fromRGBO(78, 42, 132, .7),
    700: Color.fromRGBO(78, 42, 132, .8),
    800: Color.fromRGBO(78, 42, 132, .9),
    900: Color.fromRGBO(78, 42, 132, 1),
  },
);
