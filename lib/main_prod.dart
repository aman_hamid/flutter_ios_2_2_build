import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'app_config.dart';
import 'locator.dart';
import 'main.dart';

void main() async {
  ErrorWidget.builder = (FlutterErrorDetails details) => ErrorScreen();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: brandColor, // Color for Android
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
      ));
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await setupSharedPreferences();
  await initPlatformState();
  setupLocator();
  var configuredApp = new AppConfig(
    appName: 'PocketPills',
    flavorName: 'production',
    apiBaseUrl: 'https://api.pocketpills.com',
    webviewBaseUrl: 'https://app.pocketpills.com',
    mixPanelToken: 'bfea0392c70a9ec1e3f7d8b6fe5a9f57',
    oneSignalToken: '4835d56d-d0bf-4a44-abae-5a404bd452b0',
    child: new MyApp(),
  );
  setupAppConfig(configuredApp);
  PPApplication.initApplication();
  runZoned<Future<void>>(() async {
    runApp(configuredApp);
  }, onError: FirebaseCrashlytics.instance.recordError);
}
