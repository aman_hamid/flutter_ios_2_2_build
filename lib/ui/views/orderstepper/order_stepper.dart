
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/order_data_model.dart';
import 'package:pocketpills/core/viewmodels/stepper_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/custom_stepper.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/order_stepper_success_screen.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_address_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_view.dart';
import 'package:pocketpills/ui/views/profile/profile_insurance_view.dart';
import 'package:pocketpills/ui/views/profile/profile_payment_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';

class OrderStepper extends StatefulWidget {
  static const String routeName = 'orderstepper';
  final int? startStep;
  final BaseStepperSource? source;
  final String? from;

  OrderStepper({Key? key, this.source = BaseStepperSource.UNKNOWN_SCREEN, this.startStep, this.from}) : super(key: key);

  @override
  _OrderStepperState createState() => _OrderStepperState();
}

class _OrderStepperState extends State<OrderStepper> {
  final int ONE_STATE = 1;
  final int FOUR_STATE = 4;
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getOrderStepper();
  }

  Widget getOrderStepper() {
    return MultiProvider(
        providers: [ChangeNotifierProvider<StepperModel>(create: (_) => StepperModel(source: widget.source, currentStep: widget.startStep))],
        child: Consumer<StepperModel>(
          builder: (BuildContext context, StepperModel stepperModel, Widget? child) {
            return FutureBuilder(
                future: myFutureMethodOverall(stepperModel),
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData != null && snapshot.data != null) {
                    return getMainView(stepperModel);
                  } else if (snapshot.hasError) {
                    return ErrorScreen();
                  } else {
                    return LoadingScreen();
                  }
                });
          },
        ));
  }

  Future myFutureMethodOverall(StepperModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["stepper", "myorder"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(StepperModel stepperModel) {
    return Scaffold(
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("myorder", "myorder.signup.complete-profile"),
        appBar: AppBar(),
        backButtonIcon: Icon(
          Icons.home,
          color: whiteColor,
        ),
        leadingBackButton: () {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, true);
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        },
      ),
      body: widget.source == BaseStepperSource.NEW_USER
          ? CustomStepper(
              controlsBuilder: (BuildContext context, {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
                return PPContainer.emptyContainer();
              },
              type: CustomStepperType.horizontal,
              currentStep: stepperModel.getCurrentStep(),
              onStepTapped: (int step) {
                stepperModel.setCurrentStep(step: step);
                dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, step);
              },
              onStepContinue: stepperModel.getCurrentStep() < 2 ? () => stepperModel.incrCurrentStep() : null,
              onStepCancel: stepperModel.getCurrentStep() > 0 ? () => stepperModel.decrCurrentStep() : null,
              steps: <CustomStep>[
                CustomStep(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Text(
                      LocalizationUtils.getSingleValueString("stepper", "stepper.labels.health"),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro"),
                    ),
                  ),
                  content: SizedBox(
                    height: MediaQuery.of(context).size.height - 200,
                    child: getHealthView(stepperModel),
                  ),
                  isActive: stepperModel.getCurrentStep() >= 0,
                  state: stepperModel.getCurrentStep() >= 0 ? CustomStepState.complete : CustomStepState.disabled,
                ),
                CustomStep(
                  title: Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text(LocalizationUtils.getSingleValueString("stepper", "stepper.labels.address"),
                        textAlign: TextAlign.center, style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro")),
                  ),
                  content: SizedBox(height: MediaQuery.of(context).size.height - 128, child: getAddressView(stepperModel)),
                  isActive: stepperModel.getCurrentStep() >= 0,
                  state: stepperModel.getCurrentStep() >= 2 ? CustomStepState.complete : CustomStepState.disabled,
                )
              ],
            )
          : CustomStepper(
              controlsBuilder: (BuildContext context, {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
                return PPContainer.emptyContainer();
              },
              type: CustomStepperType.horizontal,
              currentStep: stepperModel.getCurrentStep(),
              onStepTapped: (int step) {
                stepperModel.setCurrentStep(step: step);
              },
              onStepContinue: stepperModel.getCurrentStep() < 4 ? () => stepperModel.incrCurrentStep() : null,
              onStepCancel: stepperModel.getCurrentStep() > 0 ? () => stepperModel.decrCurrentStep() : null,
              steps: <CustomStep>[
                CustomStep(
                  title: new Text(LocalizationUtils.getSingleValueString("stepper", "stepper.labels.healthcard"),
                      textAlign: TextAlign.center, style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro")),
                  content: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: ProfileHealthCardView(
                      onBack: onBack(context, stepperModel),
                      onSuccess: onSuccess(context, stepperModel),
                      source: BaseStepperSource.HEALTH_CARD_ORDER_STEPPER_SCREEN,
                      position: stepperModel.getCurrentStep(),
                    ),
                  ),
                  isActive: stepperModel.getCurrentStep() >= 0,
                  state: stepperModel.getCurrentStep() == 0 ? CustomStepState.editing : (stepperModel.getCurrentStep() >= 0 ? CustomStepState.complete : CustomStepState.disabled),
                ),
                CustomStep(
                  title: new Text(LocalizationUtils.getSingleValueString("stepper", "stepper.labels.insurance"),
                      textAlign: TextAlign.center, style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro")),
                  content: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: ProfileInsuranceView(
                      onBack: onBack(context, stepperModel),
                      onSuccess: onSuccess(context, stepperModel),
                      source: BaseStepperSource.INSURANCE_ORDER_STEPPER_SCREEN,
                      position: stepperModel.getCurrentStep(),
                    ),
                  ),
                  isActive: stepperModel.getCurrentStep() >= 1,
                  state: stepperModel.getCurrentStep() == 1 ? CustomStepState.editing : (stepperModel.getCurrentStep() >= 1 ? CustomStepState.complete : CustomStepState.disabled),
                ),
                CustomStep(
                  title: Text(LocalizationUtils.getSingleValueString("stepper", "stepper.labels.address"),
                      textAlign: TextAlign.center, style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro")),
                  content: SizedBox(height: MediaQuery.of(context).size.height, child: getAddressView(stepperModel)),
                  isActive: stepperModel.getCurrentStep() >= 2,
                  state: stepperModel.getCurrentStep() == 2 ? CustomStepState.editing : (stepperModel.getCurrentStep() >= 2 ? CustomStepState.complete : CustomStepState.disabled),
                ),
                CustomStep(
                  title: Text(LocalizationUtils.getSingleValueString("stepper", "stepper.labels.payment"),
                      textAlign: TextAlign.center, style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro")),
                  content: SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: ProfilePaymentView(
                        onBack: onBack(context, stepperModel),
                        onSuccess: onSuccess(context, stepperModel),
                        source: BaseStepperSource.ORDER_STEPPER,
                        position: stepperModel.getCurrentStep(),
                      )),
                  isActive: stepperModel.getCurrentStep() >= 3,
                  state: stepperModel.getCurrentStep() == 3 ? CustomStepState.editing : (stepperModel.getCurrentStep() >= 3 ? CustomStepState.complete : CustomStepState.disabled),
                ),
                CustomStep(
                  title: Text(
                    LocalizationUtils.getSingleValueString("stepper", "stepper.labels.health"),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro"),
                  ),
                  content: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: getHealthView(stepperModel),
                  ),
                  isActive: stepperModel.getCurrentStep() >= 4,
                  state: stepperModel.getCurrentStep() >= 4 ? CustomStepState.complete : CustomStepState.disabled,
                )
              ],
            ),
    );
  }

  Widget getHealthView(StepperModel stepperModel) {
    return ProfileHealthView(onBack: onBack(context, stepperModel), onSuccess: onSuccess(context, stepperModel), showStepperText: true, position: stepperModel.getCurrentStep());
  }

  Widget getAddressView(StepperModel stepperModel) {
    return ProfileAddressView(
      onBack: onBack(context, stepperModel),
      onSuccess: onSuccess(context, stepperModel),
      source: BaseStepperSource.ORDER_STEPPER,
      position: stepperModel.getCurrentStep(),
    );
  }

  onBack(BuildContext context, StepperModel stepperModel) {
    //SystemChannels.textInput.invokeMethod('TextInput.hide');
    return () async {
      stepperModel.decrCurrentStep();
    };
  }

  onSuccess(BuildContext context, StepperModel stepperModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      return () async {
        onSuccessSteps(context, stepperModel, ONE_STATE);
      };
    } else {
      return () async {
        onSuccessSteps(context, stepperModel, FOUR_STATE);
      };
    }
  }

  onSuccessSteps(BuildContext context, StepperModel stepperModel, int steps) {
    Provider.of<HomeModel>(context, listen: false).clearData();
    if (stepperModel.getCurrentStep() < steps) {
      stepperModel.incrCurrentStep();
    } else {
      stepperModel.resetWithoutNotify();
      Navigator.pushReplacementNamed(context, OrderStepperSuccessScreen.routeName, arguments: TelehealthArguments(from: widget.from));
    }
  }
}
