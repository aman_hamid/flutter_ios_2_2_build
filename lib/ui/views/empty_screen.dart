import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/container/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

import 'dashboard/dashboard_view.dart';

class EmptyScreen extends StatelessWidget {
  final String title;
  final String description;
  final bool showTransfer;
  final String imageLink;
  final DataStoreService dataStore = locator<DataStoreService>();
  final bool isQuebec;

  EmptyScreen({this.imageLink = "graphics/empty_prescriptions.png", this.title = "Title", this.description = "Description", this.showTransfer = false, this.isQuebec = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                imageLink,
                width: MediaQuery.of(context).size.width / 2,
              ),
            ],
          ),
          PPUIHelper.verticalSpaceMedium(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: PPTexts.getMainViewHeading(title, textAlign: TextAlign.center, mainAxisAlignment: MainAxisAlignment.center),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: PPTexts.getSecondaryHeading(description, isBold: false, mainAxisAlignment: MainAxisAlignment.center, textAlign: TextAlign.center),
          ),
          PPUIHelper.verticalSpaceLarge(),
          isQuebec == false
              ? Container()
              : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                  child: PPContainer(),
                ),
          PPUIHelper.verticalSpaceLarge(),
          showTransfer == false
              ? Container()
              : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: isQuebec == false
                        ? <Widget>[
                            PrimaryButton(
                              key: Key("uploadPrescription"),
                              isExpanded: true,
                              isSmall: true,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.upload-prescription"),
                              onPressed: () {
                                Navigator.pushNamed(context, UploadPrescription.routeName);
                              },
                            ),
                            SizedBox(
                              width: PPUIHelper.HorizontalSpaceSmall,
                            ),
                            PrimaryButton(
                              key: Key("transferRefill"),
                              isExpanded: true,
                              isSmall: true,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.transfer-refill"),
                              onPressed: () async {
                                Navigator.pushNamed(context, TransferWidget.routeName,
                                    arguments: TransferArguments(
                                      source: BaseStepperSource.MAIN_SCREEN,
                                    ));
                              },
                            )
                          ]
                        : <Widget>[
                            SecondaryButton(
                              key: Key("buttonCancel"),
                              isExpanded: true,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.cancel"),
                              onPressed: () {
                                Provider.of<DashboardModel>(context).dashboardIndex = 0;
                                Navigator.popAndPushNamed(context, DashboardWidget.routeName);
                              },
                            ),
                            SizedBox(
                              width: PPUIHelper.HorizontalSpaceSmall,
                            ),
                            SecondaryButton(
                              key: Key("buttonTransfer"),
                              isExpanded: true,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.transfer"),
                              onPressed: () async {
                                Navigator.pushNamed(
                                  context,
                                  TransferWidget.routeName,
                                  arguments: TransferArguments(
                                    source: BaseStepperSource.MAIN_SCREEN,
                                  ),
                                );
                              },
                            )
                          ],
                  ),
                ),
        ],
      ),
    );
  }
}
