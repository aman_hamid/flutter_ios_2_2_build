import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/login/reset_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SetPasswordWidget extends StatefulWidget {
  static const routeName = 'setpassword';

  @override
  State<StatefulWidget> createState() {
    return SetPasswordState();
  }
}

class SetPasswordState extends BaseState<SetPasswordWidget> {
  final TextEditingController _newpasswordController = TextEditingController();
  final TextEditingController _confirmpasswordController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _monthController = TextEditingController();
  final TextEditingController _yearController = TextEditingController();
  final FocusNode _dateFnode = FocusNode();
  final FocusNode _yearFnode = FocusNode();
  final FocusNode _setFnode = FocusNode();

  String? _mmValue;
  String? _ddValue;
  String? _yyyyValue;
  bool autovalidate = false;

  final _formKey = GlobalKey<FormState>();

  bool showDateError = false;

  @override
  void initState() {
    super.initState();
    _monthController.addListener(monthControllerListener);
    _dateController.addListener(dateControllerListener);
    _yearController.addListener(yearControllerListener);
  }

  void monthControllerListener() {
    if (_monthController.text.length == 2 && _mmValue != _monthController.text) {
      _mmValue = _monthController.text;
      String? validationError = validateMonth(_mmValue!, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_dateFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _mmValue = _monthController.text;
  }

  void dateControllerListener() {
    if (_dateController.text.length == 2 && _ddValue != _dateController.text) {
      _ddValue = _dateController.text;
      String? validationError = validateDate(_ddValue!, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_yearFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _ddValue = _dateController.text;
  }

  void yearControllerListener() {
    if (_yearController.text.length == 4 && _yyyyValue != _yearController.text) {
      _yyyyValue = _yearController.text;
      String? validationError = validateYear(_yyyyValue!, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_setFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _yyyyValue = _yearController.text;
  }

  bool evaluateDate() {
    if (validateMonth(_mmValue!, showErr: true) == null && validateDate(_ddValue!, showErr: true) == null && validateYear(_yyyyValue!, showErr: true) == null)
      return true;
    else
      return false;
  }

  /*
  Widget getDOB() {
    final double gap = 15.0;
    return Column(
      children: <Widget>[
        PPTexts.getFormLabel("Date of Birth (MM DD YYYY)"),
        PPUIHelper.verticalSpaceSmall(),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: PPFormFields.getNumericFormField(
                autovalidate: autovalidate,
                keyboardType: TextInputType.phone,
                maxLength: 2,
                validator: (value) => validateMonth(value,showErr: false),
                decoration: PPInputDecor.getDecoration(collapsed: true,
                    labelText: 'MM', hintText: 'MM'),
                controller: _monthController,
              ),
            ),
            SizedBox(width: gap),
            Expanded(
              flex: 1,
              child: PPFormFields.getNumericFormField(
                autovalidate: autovalidate,
                keyboardType: TextInputType.phone,
                maxLength: 2,
                validator: (value) => validateDate(value,showErr: false),
                decoration: PPInputDecor.getDecoration(collapsed: true,
                    labelText: 'DD', hintText: 'DD'),
                controller: _dateController,
                focusNode: _dateFnode,
              ),
            ),
            SizedBox(width: gap),
            Expanded(
              flex: 2,
              child: PPFormFields.getNumericFormField(
                autovalidate: autovalidate,
                keyboardType: TextInputType.phone,
                maxLength: 4,
                validator: (value) => validateYear(value,showErr: false),
                decoration: PPInputDecor.getDecoration(collapsed: true,
                    labelText: 'YYYY', hintText: 'YYYY'),
                controller: _yearController,
                focusNode: _yearFnode,
              ),
            ),
          ],
        )
      ],
    );
  }
*/
  setDateErrorState(bool showError) {
    setState(() {
      showDateError = showError;
    });
  }

  String? validateMonth(String value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < 1 || n > 12) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  String? validateDate(String value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < 1 || n > 31) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  String? validateYear(String value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < 1900 || n > 2030) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ResetModel(),
      child: Consumer<ResetModel>(builder: (BuildContext context, ResetModel model, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(ResetModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["forgot", "login"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(ResetModel model) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
          child: Builder(
            builder: (BuildContext context) {
              return GestureDetector(
                onTap: () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                },
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: LARGE_XX,
                              ),
                              Image.asset(
                                getLogoImage(),
                                height: LARGE_XX,
                              ),
                              SizedBox(
                                height: LARGE_XX,
                              ),
                              Text(
                                LocalizationUtils.getSingleValueString("forgot", "forgot.all.set-password"),
                                style: REGULAR_XXX_PRIMARY_BOLD,
                              ),
                              SizedBox(
                                height: SMALL_X,
                              ),
                              Text(
                                LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-description"),
                                style: MEDIUM_XXX_SECONDARY,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: LARGE,
                              ),
                            ],
                          ),
                        ),
                        PPFormFields.getPasswordFormField(
                          key: Key("set_password"),
                          minLength: 8,
                          labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-label"),
                          focusNode: _setFnode,
                          controller: _newpasswordController,
                          hintText: LocalizationUtils.getSingleValueString("login", "login.fields.pass-hint"),
                          onErrorStr: LocalizationUtils.getSingleValueString("forgot", "forgot.all.enter-password-set"),
                          validator: (value) {
                            if (value!.isEmpty) return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-error");
                            if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
                            if (value != _confirmpasswordController.text)
                              return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-error-match");
                            return null;
                          },
                        ),
                        PPUIHelper.verticalSpaceLarge(),
                        PPFormFields.getPasswordFormField(
                            key: Key("confirm_password"),
                            minLength: 8,
                            labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-label"),
                            controller: _confirmpasswordController,
                            hintText: LocalizationUtils.getSingleValueString("login", "login.fields.pass-hint"),
                            onErrorStr: LocalizationUtils.getSingleValueString("forgot", "forgot.all.enter-password-set"),
                            validator: (value) {
                              if (value!.isEmpty) return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-error");
                              if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
                              if (value != _newpasswordController.text)
                                return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-error-match");
                              return null;
                            }),
                        PPUIHelper.verticalSpaceLarge(),
                        model.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : Row(
                                children: <Widget>[
                                  SecondaryButton(
                                    text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-back"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                  PrimaryButton(
                                    text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.set-password"),
                                    onPressed: () {
                                      onSetClick(context, model);
                                    },
                                  )
                                ],
                              ),
                        PPUIHelper.verticalSpaceLarge(),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  onSetClick(BuildContext context, ResetModel model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (_formKey.currentState!.validate()) {
      bool success = await model.setPassword(_newpasswordController.text);
      if (success == true) {
        Provider.of<SignUpModel>(context).handleSetPasswordSuccess(context);
        bool succesGetUserInfo = await Provider.of<SignUpModel>(context, listen: false).getUserInfo();
        if (succesGetUserInfo == true) {
          Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
        }
      } else
        onFail(context, errMessage: model.errorMessage);
    } else {
      setState(() {
        autovalidate = true;
      });
    }
  }
}
