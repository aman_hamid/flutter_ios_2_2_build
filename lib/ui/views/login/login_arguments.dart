import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class LoginArguments {
  final String? hint;
  final String? snackbarMessage;
  final String? phone;
  final String? loginType;
  final BaseStepperSource? source;
  LoginArguments({this.hint, this.snackbarMessage, this.phone, this.loginType, this.source});
}
