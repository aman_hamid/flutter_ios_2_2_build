import 'dart:async';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/forgotpassword_view.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/signup/re_signup_verification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_verification_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_verification_otp_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/start/apple_phone_bottom_sheet.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/custom_auth_button.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/route/CustomRoute.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class LoginWidgetIdentifier extends StatefulWidget {
  static const routeName = 'loginIdentifier';
  final String? hint;
  final String? snackbarMessage;
  final String? phone;
  final String? loginType;
  final BaseStepperSource? source;

  LoginWidgetIdentifier({Key? key, this.hint, this.snackbarMessage, this.phone, this.loginType, this.source = BaseStepperSource.UNKNOWN_SCREEN})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends BaseState<LoginWidgetIdentifier> with SingleTickerProviderStateMixin {
  final DataStoreService dataStore = locator<DataStoreService>();

  String phState = '';
  String prevMobileNumber = "";
  String prevEmailAddress = "";

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final FocusNode _phoneNumberFnode = FocusNode();
  final FocusNode _passwordFnode = FocusNode();
  final FocusNode _emailFnode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  bool phAutoValidate = false;
  bool emAutoValidate = false;
  bool showSnackBarMessage = false;
  bool phoneNumberField = false, passwordField = false;
  bool emailAddressField = false;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.open_login);
    _phoneNumberController.addListener(phoneNumberListener);
    _emailController.addListener(emailAddrssListener);
    _passwordController.addListener(passwordFieldListener);

    if (widget.loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" || widget.loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS") {
      _phoneNumberController.text = widget.phone!;
    } else if (widget.loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS") {
      _phoneNumberController.text = widget.phone!;
    } else {
      _emailController.text = widget.phone!;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.phone != null && widget.phone != "") {
      FocusScope.of(context).requestFocus(_passwordFnode);
    } else {
      if (widget.loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" || widget.loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS") {
        FocusScope.of(context).requestFocus(_phoneNumberFnode);
      } else if (widget.loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS") {
        FocusScope.of(context).requestFocus(_emailFnode);
      } else {
        FocusScope.of(context).requestFocus(_phoneNumberFnode);
      }
    }
  }

  phoneNumberListener() {
    if (phoneNumberField == false && _phoneNumberController.text.length == 10) {
      phoneNumberField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_entered);
    }
  }

  emailAddrssListener() {
    if (emailAddressField == false && _emailController.text.isNotEmpty) {
      emailAddressField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.email_entered);
    }
  }

  checkPhoneNumber(BuildContext context) async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevMobileNumber);
      if (checkPhone!.redirect != "PHONE_LOGIN") {
        setState(() {
          phState = checkPhone.redirect!;
          phAutoValidate = true;
        });
      } else {
        phState = "";
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phState = "";
      });
    else {
      phState = "";
    }
    prevMobileNumber = _phoneNumberController.text;
  }

  checkEmailAddress(BuildContext context) async {
    if (StringUtils.isEmail(_emailController.text)) {
      prevEmailAddress = _emailController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevEmailAddress);
      if (checkPhone!.redirect != "EMAIL_LOGIN") {
        setState(() {
          phState = checkPhone.redirect!;
          emAutoValidate = true;
        });
      } else {
        phState = "";
      }
    } else if (!StringUtils.isEmail(_emailController.text))
      setState(() {
        phState = "";
      });
    else {
      phState = "";
    }
    prevEmailAddress = _emailController.text;
  }

  passwordFieldListener() {
    if (passwordField == false && _passwordController.text.isNotEmpty) {
      passwordField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.password_entered);
    }
  }

  @override
  void dispose() {
    _phoneNumberController.removeListener(phoneNumberListener);
    _emailController.removeListener(emailAddrssListener);
    _passwordController.removeListener(passwordFieldListener);
    _passwordFnode.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel()), ChangeNotifierProvider<SignUpModel>(create: (_) => SignUpModel())],
      child: Consumer2<LoginModel, SignUpModel>(builder: (BuildContext context, LoginModel loginModel, SignUpModel signUpModel, Widget? child) {
        return FutureBuilder(
            future: loginModel.getLocalization(["login", "forgot", "signup"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(loginModel, signUpModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget getMainView(LoginModel loginModel, SignUpModel signUpModel) {
    return BaseScaffold(
        body: GestureDetector(
      onTap: () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Builder(
        builder: (BuildContext context) {
          return SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Builder(
                builder: (BuildContext context) {
                  if (showSnackBarMessage == false && widget.snackbarMessage != null) {
                    Future.delayed(const Duration(milliseconds: 100), () {
                      setState(() {
                        showSnackBarMessage = true;
                      });
                      this.showOnSnackBar(context, successMessage: widget.snackbarMessage);
                    });
                  }
                  return SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: LARGE_XX,
                                ),
                                Image.asset(
                                  getLogoImage(),
                                  height: LARGE_XX,
                                ),
                                SizedBox(
                                  height: LARGE_XX,
                                ),
                                Text(
                                  LocalizationUtils.getSingleValueString("login", "login.all.title"),
                                  style: REGULAR_XXX_PRIMARY_BOLD,
                                ),
                                SizedBox(
                                  height: SMALL_X,
                                ),
                                Text(
                                  LocalizationUtils.getSingleValueString("login", "login.all.description"),
                                  style: MEDIUM_XXX_SECONDARY,
                                ),
                                SizedBox(
                                  height: MEDIUM_X,
                                ),
                              ],
                            ),
                          ),
                          Text(
                            getDisplayText(widget.loginType!),
                            style: MEDIUM_X_SECONDARY,
                          ),
                          SizedBox(
                            height: MEDIUM_XX,
                          ),
                          getUIForgot(widget.loginType, loginModel, signUpModel),
                          PPUIHelper.verticalSpaceMedium(),
                          checkAuthTrue() ? Container() : getPasswordField(context, loginModel, signUpModel),
                          PPUIHelper.verticalSpaceLarge(),
                          checkAuthTrue()
                              ? Container()
                              : (loginModel.state == ViewState.Busy || signUpModel.state == ViewState.Busy)
                                  ? ViewConstants.progressIndicator
                                  : getLoginButton(context, loginModel, signUpModel),
                          PPUIHelper.verticalSpaceLarge(),
                          checkAuthTrue()
                              ? Container()
                              : Center(
                                  child: TransparentButton(
                                      text: LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
                                      onPressed: () {
                                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_forget_password);
                                        Navigator.pushReplacement(
                                            context,
                                            CustomRoute(
                                                builder: (BuildContext context) => widget.loginType!.contains("EMAIL_LOGIN")
                                                    ? ForgotPasswdWidget(phoneNumber: _emailController.text, loginType: PhoneNumberStateConstant.EMAIL_LOGIN)
                                                    : ForgotPasswdWidget(
                                                        phoneNumber: _phoneNumberController.text, loginType: PhoneNumberStateConstant.PHONE_LOGIN)));
                                      })),
                          PPUIHelper.verticalSpaceXLarge(),
                          Center(
                            child: TransparentButton(
                                text: widget.loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS" ||
                                        widget.loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" ||
                                        widget.loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS"
                                    ? LocalizationUtils.getSingleValueString("login", "login.fields.signup-instead-emails")
                                    : LocalizationUtils.getSingleValueString("login", "login.fields.signup-instead-phones"),
                                onPressed: () {
                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
                                  reRegisterSelected(context, signUpModel, loginModel);
                                }),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          );
        },
      ),
    ));
  }

  TextFormField getPhoneField() {
    return PPFormFields.getNumericFormField(
        focusNode: _phoneNumberFnode,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        minLength: 10,
        labelText: LocalizationUtils.getSingleValueString("login", "login.fields.phone-label"),
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        controller: _phoneNumberController,
        onTextChanged: (value) {
          checkPhoneNumber(context);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFnode);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error2");
          if (phState != "") return getPhoneValidation()[phState];
          return null;
        });
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        focusNode: _emailFnode,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.emailAddress,
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
        controller: _emailController,
        onTextChanged: (value) {
          checkEmailAddress(context);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFnode);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (!StringUtils.isEmail(value)) return LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email");
          if (phState != "") return getEmailValidation()[phState];
          return null;
        });
  }

  Widget getPasswordField(context, loginModel, signUpModel) {
    return PPFormFields.getPasswordFormField(
        key: Key("login_password"),
        focusNode: _passwordFnode,
        controller: _passwordController,
        textInputAction: TextInputAction.done,
        hintText: "",
        //LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        helperText: LocalizationUtils.getSingleValueString("login", "login.fields.password-info"),
        minLength: 8,
        onErrorStr: LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        labelText: LocalizationUtils.getSingleValueString("login", "login.fields.password-label"),
        onFieldSubmitted: (value) {
          onLoginPressed(context, loginModel, signUpModel);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("login", "login.fields.password-info");
          if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
          return null;
        });
  }

  getAuthButton(LoginModel loginModel, SignUpModel signUpModel, String? loginType) {
    return (loginModel.state == ViewState.Busy || signUpModel.state == ViewState.Busy)
        ? ViewConstants.progressIndicator
        : loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS"
            ? AuthButton(
                backgroundColor: googleColor,
                text: LocalizationUtils.getSingleValueString("login", "login.button.google"),
                onPressed: () {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.login_google);
                  checkAuthSignIn(context, loginModel, "google");
                },
                height: 49,
                image: Image.asset("graphics/icon_google.png"),
              )
            : Platform.isIOS
                ? AuthButton(
                    backgroundColor: blackColor,
                    text: LocalizationUtils.getSingleValueString("login", "login.button.apple"),
                    onPressed: () {
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.login_apple);
                      checkAuthSignIn(context, loginModel, "apple");
                    },
                    height: 49,
                    image: Image.asset("graphics/icon_apple.png"),
                  )
                : Container();
  }

  getLoginButton(context, loginModel, signUpModel) {
    return PrimaryButton(
        fullWidth: true,
        isSmall: true,
        text: LocalizationUtils.getSingleValueString("login", "login.all.login-securely"),
        iconData: Icons.arrow_forward,
        onPressed: () {
          onLoginPressed(context, loginModel, signUpModel);
        });
  }

  void onLoginPressed(BuildContext context, LoginModel loginModel, SignUpModel signUpModel) async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_login);
    if (_formKey.currentState!.validate()) {
      var success = widget.loginType!.contains("EMAIL_LOGIN")
          ? await loginModel.login(_emailController.text, _passwordController.text)
          : await loginModel.login(_phoneNumberController.text, _passwordController.text);
      if (success == true) {
        signUpModel.handleLoginSuccess(context);
        bool successGetUserInfo = await signUpModel.getUserInfo(listenable: true);
        if (successGetUserInfo == true) {
          signUpModel.getSignUpFlow2(context);
        } else {
          onFail(context, errMessage: signUpModel.errorMessage);
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login);
        onFail(context, errMessage: loginModel.errorMessage);
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login_validation);
      if (phState != "")
        onFail(context, content: onPhoneVerificationError());
      else
        onFail(context);
    }
  }

  reRegisterSelected(BuildContext context, SignUpModel signUpModel, LoginModel loginModel) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }
    String userIdentifier = widget.loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS" ||
            widget.loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" ||
            widget.loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS"
        ? _phoneNumberController.text
        : _emailController.text;
    signUpModel.cleanReMemoizer();
    bool success = await signUpModel.getReSignup(userIdentifier, listenable: false);
    if (success == true) {
      Navigator.popAndPushNamed(context, ReSignUpVerificationWidget.routeName);
    } else
      onFail(context, errMessage: loginModel.errorMessage);
  }

  Widget onPhoneVerificationError() {
    Widget textWidget = Text(LocalizationUtils.getSingleValueString("login", "login.fields.cellphone-error") + "  ");
    Widget linkWidget = InkWell(
      child: Text(
        LocalizationUtils.getSingleValueString("login", "login.fields.signup"),
        textAlign: TextAlign.left,
        style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
      ),
      onTap: () {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
        Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
      },
    );
    if (phState == "EMAIL_OTP_FORGOT" || phState == "PHONE_OTP_FORGOT") {
      linkWidget = InkWell(
        child: Text(
          LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
          textAlign: TextAlign.left,
          style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
        ),
        onTap: () {
          // Can't pass the route name in pushReplacement
          Navigator.pushReplacement(
              context,
              CustomRoute(
                  builder: (BuildContext context) => phState.contains("EMAIL")
                      ? ForgotPasswdWidget(phoneNumber: _emailController.text, loginType: PhoneNumberStateConstant.EMAIL_LOGIN)
                      : ForgotPasswdWidget(phoneNumber: _phoneNumberController.text, loginType: PhoneNumberStateConstant.PHONE_LOGIN)));
        },
      );
      textWidget = Text(LocalizationUtils.getSingleValueString("forgot", "forgot.all.password-not-set") + "  ");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[textWidget, linkWidget],
    );
  }

  String getDisplayText(String loginType) {
    if (widget.hint != null) {
      if (loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS") {
        return LocalizationUtils.getSingleValueString("login", "login.all.hint-externalauth").isEmpty
            ? "Please login using registered Google account {{hint}}".replaceAll("{{hint}}", widget.hint!)
            : LocalizationUtils.getSingleValueString("login", "login.all.hint-externalauth")
                .replaceAll("{{extAuth}}", "Google")
                .replaceAll("{{hint}}", widget.hint!);
      } else if (loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS") {
        return LocalizationUtils.getSingleValueString("login", "login.all.hint-externalauth").isEmpty
            ? "Please login using registered Apple account {{hint}}".replaceAll("{{hint}}", widget.hint!)
            : LocalizationUtils.getSingleValueString("login", "login.all.hint-externalauth")
                .replaceAll("{{extAuth}}", "Apple")
                .replaceAll("{{hint}}", widget.hint!);
      } else if (loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS") {
        return LocalizationUtils.getSingleValueString("login", "login.all.hint-email").replaceAll("{{hint}}", widget.hint!);
      } else {
        return LocalizationUtils.getSingleValueString("login", "login.all.hint-phone").replaceAll("{{hint}}", widget.hint!);
      }
    } else {
      return "";
    }
  }

  checkAuthSignIn(BuildContext context, LoginModel loginModel, String authKey) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    AuthVerification? checkPhone;
    if (authKey == "google") {
      checkPhone = await loginModel.useGoogleAuthentication();
    } else {
      checkPhone = await loginModel.useAppleAuthentication();
    }
    if (checkPhone != null) {
      updateUserDetails(context, loginModel, checkPhone, authKey);
    } else {
      onFail(context, errMessage: ApplicationConstant.apiError);
    }
  }

  updateUserDetails(BuildContext context, LoginModel loginModel, AuthVerification checkPhone, String authKey) async {
    if (checkPhone.redirect == PhoneNumberStateConstant.MASKED_EMAIL_ASK_PHONE) {
      Map<String, dynamic> map = new Map();
      map["maskEmail"] = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.apple_success, map);
      showCallAndChatBottomSheet(context, checkPhone.userIdentifier);
    } else {
      if (authKey == "google") {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.google_success);
      } else {
        Map<String, dynamic> map = new Map();
        if (dataStore.readBoolean(DataStoreService.MASKED_EMAIL) != null && dataStore.readBoolean(DataStoreService.MASKED_EMAIL)!) {
          map["maskEmail"] = true;
        } else {
          map["maskEmail"] = false;
        }
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.apple_success, map);
      }

      bool successGetUserInfo = await loginModel.getUserInfo(listenable: true);
      if (successGetUserInfo) {
        if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
          Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
        } else if (checkPhone.redirect == PhoneNumberStateConstant.EXTERNALLY_AUTHENTICATED) {
          Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
        } else if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_UNVERIFIED_PHONE_LOGIN_EXISTS) {
          locator<NavigationService>().pushNamed(LoginWidgetIdentifier.routeName,
              argument: LoginArguments(hint: checkPhone.hint, phone: checkPhone.userIdentifier, loginType: checkPhone.redirect),
              viewPushEvent: AnalyticsEventConstant.landing_login,
              viewPopEvent: AnalyticsEventConstant.carousel1);
        } else {
          onFail(context, errMessage: loginModel.errorMessage);
        }
      } else {
        onFail(context, errMessage: loginModel.errorMessage);
      }
    }
  }

  getUIForgot(String? loginType, LoginModel loginModel, SignUpModel signUpModel) {
    if (loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" || loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS") {
      return getAuthButton(loginModel, signUpModel, loginType);
    } else if (loginType == "PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS") {
      return getEmailField();
    } else {
      return getPhoneField();
    }
  }

  bool checkAuthTrue() {
    if (widget.loginType == "PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS" || widget.loginType == "PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS") {
      return true;
    } else {
      return false;
    }
  }

  showCallAndChatBottomSheet(BuildContext context, String? userIdentifier) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return ApplePhoneSheet(context: context, userIdentifier: userIdentifier);
        });
  }
}
