import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/forgot_password_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';

import 'login_verification_new.dart';

class ForgotPasswdWidget extends StatefulWidget {
  static const routeName = 'forgotpassword';

  final String? phoneNumber;
  final String? loginType;

  ForgotPasswdWidget({this.phoneNumber, this.loginType});

  @override
  State<StatefulWidget> createState() {
    return ForgotPasswdState();
  }
}

class ForgotPasswdState extends BaseState<ForgotPasswdWidget> {
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _emailAdressController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  FocusNode? _phoneNumberFocusNode;
  FocusNode? _emailAddressFocusNode;

  @override
  void initState() {
    _phoneNumberFocusNode = FocusNode();
    _emailAddressFocusNode = FocusNode();

    if (widget.phoneNumber != null && widget.phoneNumber != "") {
      widget.loginType == PhoneNumberStateConstant.PHONE_LOGIN
          ? _phoneNumberController.text = widget.phoneNumber!
          : _emailAdressController.text = widget.phoneNumber!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ForgotPasswordModel>(
          create: (_) => ForgotPasswordModel(),
        )
      ],
      child: Consumer<ForgotPasswordModel>(builder: (BuildContext context, ForgotPasswordModel model, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(ForgotPasswordModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["forgot", "login", "signup"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(ForgotPasswordModel model) {
    return BaseScaffold(
      body: SafeArea(
        key: Key('ForgotPassword'),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: LARGE_XX,
                          ),
                          Image.asset(
                            getLogoImage(),
                            height: LARGE_XX,
                          ),
                          SizedBox(
                            height: LARGE_XX,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.title"),
                            style: REGULAR_XXX_PRIMARY_BOLD,
                          ),
                          SizedBox(
                            height: SMALL_X,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.description"),
                            style: MEDIUM_XXX_SECONDARY,
                          ),
                          SizedBox(
                            height: LARGE,
                          ),
                        ],
                      ),
                      widget.loginType == PhoneNumberStateConstant.PHONE_LOGIN ? getUsernameField() : getEmailField(),
                      PPUIHelper.verticalSpaceLarge(),
                      model.state == ViewState.Busy
                          ? ViewConstants.progressIndicator
                          : Row(
                              children: <Widget>[
                                SecondaryButton(
                                  key: Key("forgot back"),
                                  text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-back"),
                                  onPressed: () {
                                    Navigator.of(context).pushNamedAndRemoveUntil(StartView.routeName, (Route<dynamic> route) => false);
                                  },
                                ),
                                SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                PrimaryButton(
                                  key: Key("forgot next"),
                                  text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                                  onPressed: () {
                                    onNextPressed(context, model);
                                  },
                                ),
                              ],
                            ),
                      PPUIHelper.verticalSpaceLarge(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  onNextPressed(BuildContext context, ForgotPasswordModel model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (_formKey.currentState!.validate()) {
      var success = (widget.loginType == PhoneNumberStateConstant.PHONE_LOGIN)
          ? await model.getOtpByIdentifier(_phoneNumberController.text)
          : await model.getOtpByIdentifier(_emailAdressController.text);
      if (success) {
        Navigator.pushNamed(
          context,
          LoginVerificationWidgetNew.routeName,
          arguments: VerificationArguments(
              userIdentifier: widget.loginType == PhoneNumberStateConstant.PHONE_LOGIN ? _phoneNumberController.text : _emailAdressController.text),
        );
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }

  TextFormField getUsernameField() {
    return PPFormFields.getNumericFormField(
        key: Key("phoneField"),
        focusNode: _phoneNumberFocusNode!,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        minLength: 10,
        labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.phone-label"),
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        controller: _phoneNumberController,
        onFieldSubmitted: (value) {},
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error2");
          return null;
        });
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        focusNode: _emailAddressFocusNode!,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.emailAddress,
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
        controller: _emailAdressController,
        onFieldSubmitted: (value) {},
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (!StringUtils.isEmail(value)) return LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email");
          return null;
        });
  }
}
