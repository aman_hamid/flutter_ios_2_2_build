import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/login/reset_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ResetPasswordWidget extends StatefulWidget {
  static const routeName = 'resetpassword';
  String? userIdentifier;

  ResetPasswordWidget({this.userIdentifier});

  @override
  State<StatefulWidget> createState() {
    return ResetPasswordState();
  }
}

class ResetPasswordState extends BaseState<ResetPasswordWidget> {
  final TextEditingController _newpasswordController = TextEditingController();
  final TextEditingController _confirmpasswordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ResetModel(),
      child: Consumer<ResetModel>(builder: (BuildContext context, ResetModel model, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(ResetModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["forgot", "login"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(ResetModel model) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: LARGE_XX,
                            ),
                            Image.asset(
                              getLogoImage(),
                              height: LARGE_XX,
                            ),
                            SizedBox(
                              height: LARGE_XX,
                            ),
                            Text(
                              LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-title"),
                              style: REGULAR_XXX_PRIMARY_BOLD,
                            ),
                            SizedBox(
                              height: SMALL,
                            ),
                            Text(
                              LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-description"),
                              style: MEDIUM_XXX_SECONDARY,
                            ),
                            SizedBox(
                              height: LARGE,
                            ),
                          ],
                        ),
                      ),
                      PPFormFields.getPasswordFormField(
                          key: Key("reset_password"),
                          minLength: 8,
                          labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-label"),
                          controller: _newpasswordController,
                          hintText: LocalizationUtils.getSingleValueString("login", "login.fields.pass-hint"),
                          helperText: LocalizationUtils.getSingleValueString("login", "login.fields.password-info"),
                          onErrorStr: LocalizationUtils.getSingleValueString("forgot", "forgot.all.enter-new-password"),
                          validator: (value) {
                            if (value!.isEmpty) return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-error");
                            if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
                            if (value != _confirmpasswordController.text)
                              return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-error-match");
                            return null;
                          }),
                      PPUIHelper.verticalSpaceLarge(),
                      PPFormFields.getPasswordFormField(
                          key: Key("confirm_password"),
                          minLength: 8,
                          labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-label"),
                          controller: _confirmpasswordController,
                          hintText: LocalizationUtils.getSingleValueString("login", "login.fields.pass-hint"),
                          helperText: LocalizationUtils.getSingleValueString("login", "login.fields.password-info"),
                          onErrorStr: LocalizationUtils.getSingleValueString("forgot", "forgot.all.enter-new-password"),
                          validator: (value) {
                            if (value!.isEmpty) return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-newpassword-error");
                            if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
                            if (value != _newpasswordController.text)
                              return LocalizationUtils.getSingleValueString("forgot", "forgot.all.reset-confirmpassword-error-match");
                            return null;
                          }),
                      PPUIHelper.verticalSpaceLarge(),
                      model.state == ViewState.Busy
                          ? ViewConstants.progressIndicator
                          : Row(
                              children: <Widget>[
                                SecondaryButton(
                                  text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-back"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                PrimaryButton(
                                  text: LocalizationUtils.getSingleValueString("common", "common.button.reset"),
                                  onPressed: () {
                                    onResetClick(context, model);
                                  },
                                )
                              ],
                            ),
                      PPUIHelper.verticalSpaceLarge(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  onResetClick(BuildContext context, model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_reset_password);
    if (_formKey.currentState!.validate()) {
      bool success = await model.resetPassword(_newpasswordController.text);
      if (success == true) {
        Provider.of<SignUpModel>(context, listen: false).handleResetPasswordSuccess(context);
        bool succesGetUserInfo = await Provider.of<SignUpModel>(context, listen: false).getUserInfo();
        if (succesGetUserInfo == true) {
          Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
        }
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }
}
