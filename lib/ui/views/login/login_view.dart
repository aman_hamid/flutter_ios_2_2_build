import 'dart:async';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/forgotpassword_view.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view_identifier.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/route/CustomRoute.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class LoginWidget extends StatefulWidget {
  static const routeName = 'login';
  final String? snackbarMessage;
  final String? phone;
  final String? loginType;
  final BaseStepperSource? source;

  LoginWidget({Key? key, this.snackbarMessage, this.phone, this.loginType, this.source = BaseStepperSource.UNKNOWN_SCREEN}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends BaseState<LoginWidget> with SingleTickerProviderStateMixin {
  final DataStoreService dataStore = locator<DataStoreService>();

  static const passwordValidationError = 'Enter your password to login';

  //final String loginHeading = "Welcome Back!";
  //final String loginDescription = "Login to your PocketPills account";

  String phState = "";
  String prevMobileNumber = "";
  String prevEmailAddress = "";

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final FocusNode _phoneNumberFnode = FocusNode();
  final FocusNode _passwordFnode = FocusNode();
  final FocusNode _emailFnode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  bool phAutoValidate = false;
  bool emAutoValidate = false;
  bool showSnackBarMessage = false;
  bool phoneNumberField = false, passwordField = false;
  bool emailAddressField = false;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.open_login);
    _phoneNumberController.addListener(phoneNumberListener);
    _emailController.addListener(emailAddrssListener);
    _passwordController.addListener(passwordFieldListener);
    widget.loginType == "PHONE_LOGIN" ? _phoneNumberController.text = widget.phone! : _emailController.text = widget.phone!;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.phone != null && widget.phone != "") {
      FocusScope.of(context).requestFocus(_passwordFnode);
    } else {
      widget.loginType == "PHONE_LOGIN" ? FocusScope.of(context).requestFocus(_phoneNumberFnode) : FocusScope.of(context).requestFocus(_emailFnode);
    }
  }

  phoneNumberListener() {
    if (phoneNumberField == false && _phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      phoneNumberField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_entered);
    }
  }

  emailAddrssListener() {
    if (emailAddressField == false && _emailController.text.isNotEmpty) {
      emailAddressField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.email_entered);
    }
  }

  checkPhoneNumber(BuildContext context) async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevMobileNumber);
      if (checkPhone!.redirect != "PHONE_LOGIN") {
        setState(() {
          phState = checkPhone.redirect ?? "";
          phAutoValidate = true;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;
  }

  checkEmailAddress(BuildContext context) async {
    if (StringUtils.isEmail(_emailController.text)) {
      prevEmailAddress = _emailController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevEmailAddress);
      if (checkPhone!.redirect != "EMAIL_LOGIN") {
        setState(() {
          phState = checkPhone.redirect ?? "";
          emAutoValidate = true;
        });
      }
    } else if (!StringUtils.isEmail(_emailController.text))
      setState(() {
        phState = "";
      });
    prevEmailAddress = _emailController.text;
  }

  passwordFieldListener() {
    if (passwordField == false && _passwordController.text.isNotEmpty) {
      passwordField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.password_entered);
    }
  }

  @override
  void dispose() {
    _phoneNumberController.removeListener(phoneNumberListener);
    _emailController.removeListener(emailAddrssListener);
    _passwordController.removeListener(passwordFieldListener);
    _passwordFnode.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel()), ChangeNotifierProvider<SignUpModel>(create: (_) => SignUpModel())],
      child: Consumer2<LoginModel, SignUpModel>(builder: (BuildContext context, LoginModel loginModel, SignUpModel signUpModel, Widget? child) {
        return FutureBuilder(
            future: loginModel.getLocalization(["login", "forgot", "signup", "landing"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return Scaffold(
                    key: Key('LoginPage'),
                    body: GestureDetector(
                      onTap: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                      },
                      child: Builder(
                        builder: (BuildContext context) {
                          return SafeArea(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                              child: Builder(
                                builder: (BuildContext context) {
                                  if (showSnackBarMessage == false && widget.snackbarMessage != null) {
                                    Future.delayed(const Duration(milliseconds: 100), () {
                                      setState(() {
                                        showSnackBarMessage = true;
                                      });
                                      this.showOnSnackBar(context, successMessage: widget.snackbarMessage);
                                    });
                                  }
                                  return SingleChildScrollView(
                                    child: Form(
                                      key: _formKey,
                                      child: Column(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              SizedBox(
                                                height: LARGE_XX,
                                              ),
                                              LocalizationUtils.isProvinceQuebec() == false
                                                  ? Image.asset(
                                                      getLogoImage(),
                                                      height: LARGE_XX,
                                                    )
                                                  : Image.network(
                                                      "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                                                      height: LARGE_XX,
                                                      width: MediaQuery.of(context).size.width * 0.75,
                                                      fit: BoxFit.fill,
                                                    ),
                                              SizedBox(
                                                height: LARGE,
                                              ),
                                              Text(
                                                LocalizationUtils.getSingleValueString("login", "login.all.title"),
                                                style: REGULAR_XXX_PRIMARY_BOLD,
                                              ),
                                              SizedBox(
                                                height: SMALL_X,
                                              ),
                                              Text(
                                                LocalizationUtils.getSingleValueString("login", "login.all.description"),
                                                style: MEDIUM_XXX_SECONDARY,
                                              ),
                                              SizedBox(
                                                height: LARGE_X,
                                              ),
                                            ],
                                          ),
                                          getFieldsUI(),
                                          widget.loginType == "EMAIL_LOGIN" ? getEmailField() : getPhoneField(),
                                          PPUIHelper.verticalSpaceMedium(),
                                          Container(
                                            alignment: Alignment.topLeft,
                                            child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("login", "login.fields.password-label"),
                                                fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          getPasswordField(context, loginModel, signUpModel),
                                          PPUIHelper.verticalSpaceXMedium(),
                                          Container(
                                              alignment: Alignment.topRight,
                                              child: TransparentButton(
                                                  key: Key("forgotPasswordButton"),
                                                  text: LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
                                                  onPressed: () {
                                                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_forget_password);
                                                    Navigator.pushReplacement(
                                                        context,
                                                        CustomRoute(
                                                            builder: (BuildContext context) => widget.loginType == PhoneNumberStateConstant.EMAIL_LOGIN
                                                                ? ForgotPasswdWidget(
                                                                    phoneNumber: _emailController.text, loginType: PhoneNumberStateConstant.EMAIL_LOGIN)
                                                                : ForgotPasswdWidget(
                                                                    phoneNumber: _phoneNumberController.text,
                                                                    loginType: PhoneNumberStateConstant.PHONE_LOGIN)));
                                                  })),
                                          PPUIHelper.verticalSpaceXMedium(),
                                          (loginModel.state == ViewState.Busy || signUpModel.state == ViewState.Busy)
                                              ? ViewConstants.progressIndicator
                                              : getLoginButton(context, loginModel, signUpModel),
                                          PPUIHelper.verticalSpaceLarge(),
                                          Center(
                                            child: TransparentButton(
                                                text: LocalizationUtils.getSingleValueString("login", "login.fields.new-member"),
                                                onPressed: () {
                                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
                                                  Navigator.of(context).pushNamedAndRemoveUntil(StartView.routeName, (Route<dynamic> route) => false);
                                                }),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          );
                        },
                      ),
                    ));
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  TextFormField getPhoneField() {
    return PPFormFields.getNumericFormField(
        focusNode: _phoneNumberFnode,
        key: Key("phoneFieldLogin"),
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        minLength: 10,
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
        controller: _phoneNumberController,
        decoration: PPInputDecor.getDecoration(
            collapsed: false,
            labelText: " ",
            hintText: LocalizationUtils.getSingleValueString("landing", "landing.fields.phone"),
            floatingLabelBehavior: FloatingLabelBehavior.never,
          prefixIcon: Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                '+1 |',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "FSJoeyPro Medium",
                ),
              )),
        ),
        onTextChanged: (value) {
          checkPhoneNumber(context);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFnode);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phoneerror");
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error2");
          if (phState.isNotEmpty) return getPhoneValidation()[phState];
          return null;
        });
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        focusNode: _emailFnode,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.emailAddress,
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email"),
            floatingLabelBehavior: FloatingLabelBehavior.never),
        controller: _emailController,
        onTextChanged: (value) {
          checkEmailAddress(context);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFnode);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (!StringUtils.isEmail(value)) return LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email");
          if (phState.isNotEmpty) return getEmailValidation()[phState];
          return null;
        });
  }

  Widget getPasswordField(context, loginModel, signUpModel) {
    return PPFormFields.getPasswordFormField(
        key: Key("loginPasswordField"),
        focusNode: _passwordFnode,
        controller: _passwordController,
        textInputAction: TextInputAction.done,
        hintText: "",
        //LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        helperText: "",
        minLength: 8,
        onErrorStr: LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        labelText: "",
        onFieldSubmitted: (value) {
          onLoginPressed(context, loginModel, signUpModel);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
          if (value.length < 8) return LocalizationUtils.getSingleValueString("signup", "signup.fields.password-error");
          return null;
        });
  }

  getLoginButton(context, loginModel, signUpModel) {
    return PrimaryButton(
        fullWidth: true,
        key: Key("loginProceed"),
        text: LocalizationUtils.getSingleValueString("login", "login.all.login-securely"),
        iconData: Icons.lock_outlined,
        isLeft: true,
        onPressed: () {
          onLoginPressed(context, loginModel, signUpModel);
        });
  }

  void onLoginPressed(BuildContext context, LoginModel loginModel, SignUpModel signUpModel) async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_login);
    if (_formKey.currentState!.validate()) {
      var success = widget.loginType == "EMAIL_LOGIN"
          ? await loginModel.login(_emailController.text, _passwordController.text)
          : await loginModel.login(_phoneNumberController.text, _passwordController.text);
      if (success == true) {
        signUpModel.handleLoginSuccess(context);
        bool succesGetUserInfo = await signUpModel.getUserInfo(listenable: true);
        if (succesGetUserInfo == true) {
          Navigator.pop(context);
          signUpModel.getSignUpFlow2(context);
        } else {
          onFail(context, errMessage: signUpModel.errorMessage);
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login);
        onFail(context, errMessage: loginModel.errorMessage);
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login_validation);
      if (phState != "")
        onFail(context, content: onPhoneVerificationError());
      else
        onFail(context);
    }
  }

  Widget onPhoneVerificationError() {
    Widget textWidget = Text(LocalizationUtils.getSingleValueString("login", "login.fields.cellphone-error") + "  ");
    Widget linkWidget = InkWell(
      child: Text(
        LocalizationUtils.getSingleValueString("login", "login.fields.signup"),
        textAlign: TextAlign.left,
        style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
      ),
      onTap: () {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
        Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
      },
    );
    if (phState == "EMAIL_OTP_FORGOT" || phState == "PHONE_OTP_FORGOT") {
      linkWidget = InkWell(
        child: Text(
          LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
          textAlign: TextAlign.left,
          style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
        ),
        onTap: () {
          // Can't pass the route name in pushReplacement
          Navigator.pushReplacement(
              context,
              CustomRoute(
                  builder: (BuildContext context) => phState.contains("EMAIL")
                      ? ForgotPasswdWidget(phoneNumber: _emailController.text, loginType: PhoneNumberStateConstant.EMAIL_LOGIN)
                      : ForgotPasswdWidget(phoneNumber: _phoneNumberController.text, loginType: PhoneNumberStateConstant.PHONE_LOGIN)));
        },
      );
      textWidget = Text(LocalizationUtils.getSingleValueString("forgot", "forgot.all.password-not-set") + "  ");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[textWidget, linkWidget],
    );
  }

  checkAuthSignIn(BuildContext context, LoginModel loginModel, String authKey, SignUpModel signUpModel) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed);
    AuthVerification? checkPhone;
    if (authKey == "google") {
      checkPhone = await loginModel.useGoogleAuthentication();
    } else {
      checkPhone = await loginModel.useAppleAuthentication();
    }
    if (checkPhone != null) {
      updateUserDetails(context, loginModel, checkPhone, signUpModel);
    } else {
      onFail(context, errMessage: ApplicationConstant.apiError);
    }
  }

  updateUserDetails(BuildContext context, LoginModel loginModel, AuthVerification checkPhone, SignUpModel signUpModel) async {
    bool successGetUserInfo = await loginModel.getUserInfo(listenable: true);
    if (successGetUserInfo == true) {
      if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
        locator<NavigationService>().pushNamed(
          SignupWidget.routeName,
          argument: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU),
        );
      } else if (checkPhone.redirect == PhoneNumberStateConstant.EXTERNALLY_AUTHENTICATED) {
        signUpModel.getSignUpFlow2(context);
        /*bool getDashBoardStatus = await loginModel.getSignUpAuthRedirection();
        if (getDashBoardStatus == false) {
          locator<NavigationService>().pushNamed(
            SignupWidget.routeName,
            argument: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU),
          );
        } else {
          loginModel.handleLoginSuccess(context);
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        }*/
      } else if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_UNVERIFIED_PHONE_LOGIN_EXISTS) {
        locator<NavigationService>().pushNamed(LoginWidgetIdentifier.routeName,
            argument: LoginArguments(hint: checkPhone.hint!, phone: dataStore.readString(DataStoreService.PHONE), loginType: checkPhone.redirect!),
            viewPushEvent: AnalyticsEventConstant.landing_login,
            viewPopEvent: AnalyticsEventConstant.carousel1);
      } else {
        onFail(context, errMessage: loginModel.errorMessage);
      }
    } else {
      onFail(context, errMessage: loginModel.errorMessage);
    }
  }

  Widget getFieldsUI() {
    if (widget.loginType == "EMAIL_LOGIN") {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
              fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
          SizedBox(
            height: 5,
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("login", "login.fields.phone-label"),
              fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
          SizedBox(
            height: 5,
          ),
        ],
      );
    }
  }
}
