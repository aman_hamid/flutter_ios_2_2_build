import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class ResetArguments {
  final String? userIdentifier;

  ResetArguments({this.userIdentifier});
}
