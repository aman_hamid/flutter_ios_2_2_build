import 'dart:async';
import 'package:flutter/material.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/logos.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewHomeView extends StatefulWidget {
  static const routeName = 'webview';

  final String? sourceUrl;

  WebviewHomeView({this.sourceUrl});

  @override
  State<StatefulWidget> createState() {
    return WebViewState1();
  }
}

class WebViewState1 extends State<WebviewHomeView> {
  static final webviewUrl = locator<AppConfig>().webviewBaseUrl;
  final String initUrl = '$webviewUrl';
  final String logoutUrl = 'logout';

  final DataStoreService dataStore = locator<DataStoreService>();

  bool cookieSet = false;

  WebViewController? _webViewController;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            LocalizationUtils.getSingleValueString("common", "common.label.share-experience"),
          ),
        ),
        body: WillPopScope(
          onWillPop: () async {
            Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false);
            return true;
          },
          child: WebView(
            initialUrl: widget.sourceUrl ?? "",
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _webViewController = webViewController;
            },
            onProgress: (int progress) {
              print("WebView is loading (progress : $progress%)");
            },
            onPageStarted: (String url) {
              print('Page started loading: $url');
            },
            onPageFinished: (String url) {
              print('Page finished loading: $url');
            },
            gestureNavigationEnabled: true,
          ),
        ));
  }
}
