import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class FullImageContainer extends StatelessWidget {
  final ImageProvider? image;
  final Function()? onPressed;
  final Function()? onDelete;
  final bool? isFullWidth;
  final Function()? onEdit;
  final bool? healthCard;
  final bool? isView;
  final String? imageKey;

  FullImageContainer(
      {Key? key,
      @required this.image,
      @required this.onDelete,
      this.onEdit,
      this.onPressed,
      this.healthCard = false,
      this.isFullWidth = false,
      this.isView = false,
      this.imageKey = "0"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width);
    print(MediaQuery.of(context).size.height);
    return Container(
      height: 148.0,
      width: isFullWidth == true ? (MediaQuery.of(context).size.width - 36) : (MediaQuery.of(context).size.width - 50) / 2,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: image!,
            fit: isFullWidth == true ? BoxFit.contain : BoxFit.cover,
          ),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(4.0))),
      child: isView == true ? getViewImage(context) : isFullWidth == true ? getDottedBorder(context) : getWithoutDottedBorder(context),
    );
  }

  Widget getDottedBorder(BuildContext context) {
    return DottedBorder(
        dashPattern: [6, 4],
        strokeWidth: 1,
        color: tertiaryColor,
        child: Container(
          height: 50,
          decoration: BoxDecoration(color: Colors.black45),
          margin: EdgeInsets.only(top: 148),
          alignment: Alignment.bottomCenter,
          child: Align(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: (((MediaQuery.of(context).size.width - 60) / 2) - 2),
                  child: FlatButton(
                    key: Key(imageKey.toString()),
                    padding: EdgeInsets.all(0),
                    onPressed: onPressed,
                    child: Text(
                      LocalizationUtils.getSingleValueString("common", "common.button.view"),
                      style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro Bold"),
                    ),
                  )),
              SizedBox(
                width: 1,
                child: Container(
                  color: Colors.white,
                ),
              ),
              Container(
                width: (((MediaQuery.of(context).size.width - 60) / 2) - 2),
                child: FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: healthCard! ? onEdit : onDelete,
                  child: healthCard!
                      ? Text(
                          LocalizationUtils.getSingleValueString("common", "common.button.edit"),
                          style: TextStyle(color: Colors.white, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro Bold"),
                          key: Key(imageKey.toString()),
                        )
                      : Text(
                          LocalizationUtils.getSingleValueString("common", "common.button.delete"),
                          style: TextStyle(color: Colors.white, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro Bold"),
                          key: Key(imageKey.toString()),
                        ),
                ),
              )
            ],
          )),
        ));
  }

  Widget getWithoutDottedBorder(BuildContext context) {
    return Align(
        alignment: FractionalOffset.bottomCenter,
        child: Container(
          height: 50,
          decoration: BoxDecoration(color: Colors.black45),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: ((MediaQuery.of(context).size.width - 50) / 4) - 7,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: onPressed,
                    child: Text(
                      LocalizationUtils.getSingleValueString("common", "common.button.view"),
                      style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro Bold"),
                    ),
                  )),
              SizedBox(
                width: 1,
                child: Container(
                  color: Colors.white,
                ),
              ),
              healthCard!
                  ? Container(
                      width: ((MediaQuery.of(context).size.width - 50) / 4) - 1,
                      child: FlatButton(
                          key: Key("edit_image_" + imageKey.toString()),
                          padding: EdgeInsets.all(0),
                          onPressed: onEdit,
                          child: Text(
                            LocalizationUtils.getSingleValueString("common", "common.button.edit"),
                            style: TextStyle(color: Colors.white, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro Bold"),
                          )),
                    )
                  : Container(
                      key: Key("delete_image_" + imageKey.toString()),
                      width: ((MediaQuery.of(context).size.width - 50) / 4) - 1,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: onDelete,
                        child: Text(
                          LocalizationUtils.getSingleValueString('common', "common.button.delete"),
                          style: TextStyle(color: Colors.white, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro Bold"),
                        ),
                      ),
                    )
            ],
          ),
        ));
  }

  Widget getViewImage(BuildContext context) {
    return InkWell(child: Container(),onTap: onPressed,);
  }
}
