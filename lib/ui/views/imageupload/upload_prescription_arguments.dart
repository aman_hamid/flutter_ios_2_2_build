import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/models/user_patient.dart';

class UploadPrescriptionArguments {
  final BaseStepperSource? source;
  final String? medicineName;
  final double? quantity;
  final String? from;
  final UserPatient? userPatient;
  final SignUpTransferModel? model;

  UploadPrescriptionArguments({this.source, this.medicineName, this.quantity, this.from, this.model, this.userPatient});
}
