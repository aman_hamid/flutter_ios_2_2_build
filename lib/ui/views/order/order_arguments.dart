class OrderArguments {
  final int? orderId;

  OrderArguments({this.orderId});
}
