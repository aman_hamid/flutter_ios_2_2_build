import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/enums/telehealthAppointmentDetails.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_doctor_time_slots.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_reschedule_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/Triple.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

import '../../../locator.dart';

class RescheduleTimeSheet extends StatefulWidget {
  BuildContext? context;
  bool? prescriptionFilledByExternalPharmacy;
  String? prescriptionMedicalConditions;
  String? prescriptionComment;
  String? appointmentTime;
  String? telehealthRequestedMedications;
  int? prescriptionId;
  bool showGrid = false;
  AppointmentTime? selectedItem;
  String? slotDate;
  String? slotTime;


  RescheduleTimeSheet(
      {this.context,
      this.prescriptionFilledByExternalPharmacy,
      this.prescriptionMedicalConditions,
      this.prescriptionComment,
      this.appointmentTime,
      this.telehealthRequestedMedications,
      this.prescriptionId,
      this.showGrid = false,
      this.selectedItem,
      this.slotDate,
      this.slotTime});

  _RescheduleTimeSheetState createState() => _RescheduleTimeSheetState();
}

class _RescheduleTimeSheetState extends BaseState<RescheduleTimeSheet> with SingleTickerProviderStateMixin {
  AppointmentRescheduleModel? models;
  int provinceValue = -1;
  String? selectProvinceError;
  bool autovalidate = false;
  bool errorShow = false;
  ScrollController? _scrollController;
  List<bool> selectedIndexes = [];
  bool itemSelected = false;
  AppointmentTime? selectedItem;
  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();
  bool eventTime = false;

  //New Logic
  String? dropValue;
  String? timeDropValue;
  bool showGrid = false;
  bool selected = false;
  int selectedIndex = -1;
  //Map<String, String> availableTimings = Map();
  List<String> availableTimings = [];
  AppointmentDoctorSlots? _appointmentDoctorSlots;

  bool autoSelected = false;
  int dateIndex = 0;

  //String? dropDownValue;

  @override
  void initState() {
    super.initState();
    models = AppointmentRescheduleModel();
    _scrollController = new ScrollController(
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );

    dropValue = models!.selectedDate != null ? models!.selectedDate! : null;
    timeDropValue = models!.index != "-1" ? models!.index : null;
    selected = models!.selected;
    showGrid = widget.showGrid;
    selectedItem = widget.selectedItem;
  }

  @override
  void dispose() {
    //Provider.of<AppointmentDashboardModel>(context).showGrid = showGrid;
    models!.showGrid = showGrid;
    models!.selected = selected;
    super.dispose();
  }

  Widget build(BuildContext bc) {
    return FutureBuilder(
        future: myFutureMethodOverall(models!),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          Map<String, String> map = new Map();
          if (snapshot.hasData != null && snapshot.data != null) {
            if (eventTime == false) {
              eventTime = true;
              analyticsEvents.sendAnalyticsEvent("appointment_rescheduled", map);
            }
            return getMainView(models!, snapshot.data[0]);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(AppointmentRescheduleModel model) async {
    Future<List<AppointmentTime>?> future1 = model.getAppointmentDate();
    Future<Map<String, dynamic>?> future2 = model.getLocalization(["signup", "modal"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget getMainView(AppointmentRescheduleModel model, List<AppointmentTime> appointmentTime) {
    return SingleChildScrollView(
      child: Column(
        key: Key("Reschedule View"),
        children: [
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("modal", "modal.reschedule.title")),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          SizedBox(
            height: MEDIUM,
          ),
          getDateTimeDisplay(context, model),
          PPDivider(),
          Padding(
            padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
            child: models!.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : model.appointmentsList.length > 0
                    ? Row(
                        children: <Widget>[
                          SecondaryButton(
                            isExpanded: true,
                            text: LocalizationUtils.getSingleValueString("common", "common.button.close"),
                            onPressed: () {
                              model.showGrid = showGrid;
                              model.selected = selected;
                              Navigator.of(context).pop();
                            },
                          ),
                          SizedBox(
                            width: PPUIHelper.HorizontalSpaceSmall,
                          ),
                          PrimaryButton(
                            key: Key("dateSubmit"),
                            isExpanded: true,
                            text: LocalizationUtils.getSingleValueString("common", "common.button.reschedule"),
                            onPressed: () {
                              setState(() {
                                if (selectedItem != null && selectedIndex != -1) {
                                  errorShow = false;
                                  onClick(model);
                                } else if (selectedItem == null) {
                                  errorShow = true;
                                } else if (selectedIndex == -1) {
                                  errorShow = true;
                                }
                              });
                            },
                          )
                        ],
                      )
                    : Row(
                        children: [
                          PrimaryButton(
                              borderRadius: 30,
                              text: LocalizationUtils.getSingleValueString("common", "common.button.close").isEmpty
                                  ? "Close"
                                  : LocalizationUtils.getSingleValueString("common", "common.button.close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              }),
                        ],
                      ),
          ),
        ],
      ),
    );
  }

  onClick(AppointmentRescheduleModel model) async {
    AppointmentDetails.AppointmentSelectedItem = selectedItem;
    dataStore.writeString(DataStoreService.DOCTOR_NAME, _appointmentDoctorSlots!.doctorName!);
    dataStore.writeString(DataStoreService.APPOINTMENT_TIME, _appointmentDoctorSlots!.startTime!);
    dataStore.writeString(DataStoreService.APPOINTMENT_DATE, selectedItem!.displayDate!);
    TelehealthRequestUpdate telehealthRequest = TelehealthRequestUpdate(
        appointmentTime: _appointmentDoctorSlots!.actualApptTime,
        id: widget.prescriptionId!,
        doctorId: _appointmentDoctorSlots!.doctorId,
        teleHealthDoctorDayPlanId: _appointmentDoctorSlots!.teleHealthDoctorDayPlanId,
        telehealthAppointmentSlotId: _appointmentDoctorSlots!.telehealthAppointmentSlotId);

    bool success = await model.sendPreferences(telehealthRequest, widget.prescriptionId);
    if (success) {
      Map<String, String> map = new Map();
      map["daysDiffer"] = selectedItem!.appointmentDate.toString();
      map["slotSelected"] = selectedItem!.index.toString();
      analyticsEvents.sendAnalyticsEvent("appointment_rescheduled", map);
      Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
      Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
    } else {
      Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"),toastLength: Toast.LENGTH_LONG);
    }
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget getDosageTiles(SignUpTransferModel model) {
    List<Widget> tiles = [];
    for (int i = 0; i < model.provinceArrayList.length; i++) {
      TelehealthProvinceItem suggestedEmployer = model.provinceArrayList[i];
      tiles.add(RadioListTile(
        title: Text(
          suggestedEmployer.value,
          style: MEDIUM_XXX_PRIMARY_BOLD,
        ),
        value: i,
        groupValue: provinceValue,
        onChanged: (int? val) {
          setState(() {
            provinceValue = val!;
            selectProvinceError = null;
            autovalidate = false;
          });
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }

  Widget getDateTimeDisplay(BuildContext context, AppointmentRescheduleModel model) {
    return model.appointmentsList.length > 0
        ? Column(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
      getAvailableDaysDropDown(context, model),
      SizedBox(
        height: PPUIHelper.VerticalSpaceMedium,
      ),
      getAvailableTimesDropDown(context, model),
      SizedBox(
        height: PPUIHelper.VerticalSpaceMedium,
      ),
      selected ? doctorView(model) : Container(),
    ])
        : Padding(
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
      child: Container(
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(40, 0, 0, 0),
              blurRadius: 3,
              spreadRadius: 0,
              offset: Offset(
                0, // horizontal,
                1.0, // vertical,
              ),
            )
          ],
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(4.0),
          color: whiteColor,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Icon(Icons.event_busy, color: violetColorDark, size: 36),
              SizedBox(
                height: 5,
              ),
              Text(
                LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec").isEmpty
                    ? "Unfortunately, there are currently no slots available for tele-consultation"
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec"),
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18.0, height: 1.5, fontFamily: "FSJoeyPro", color: thinGrey),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getAvailableDaysDropDown(BuildContext context, AppointmentRescheduleModel model) {
    if (dropValue == null && autoSelected == false) selectDateAuto(model);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
          child: RichText(
            text: TextSpan(children: [
              TextSpan(
                text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label").isEmpty
                    ? "Available dates "
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label") + " ",
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
              ),
              TextSpan(
                text: '*',
                style: TextStyle(color: errorColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
              ),
            ]),
          ),
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
              height: 90.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: model.appointmentsList.length,
                itemBuilder: (BuildContext context, int i) => InkWell(
                  onTap: () {
                    setState(() {
                      selectedIndex = -1;
                      dateIndex = i;
                      model.selectedDate = model.appointmentsList[i].displayDate;
                      selectedItem = model.time[model.appointmentsList[i].displayDate];
                      model.currentItem = model.appointListToActualIndex[selectedItem!.index];
                      showGrid = true;
                      selected = false;
                      timeDropValue = null;
                      dropValue = model.appointmentsList[i].displayDate;
                      if (availableTimings != null) {
                        availableTimings.clear();
                      }
                      if (selectedItem!.doctorTimeSlots != null) {
                        for (int j = 0; j < selectedItem!.doctorTimeSlots!.length; j++) {
                          var dataItem = selectedItem!.doctorTimeSlots![j];
                          availableTimings.add(dataItem.startTime.toString());
                        }
                      }
                    });
                  },
                  child: Card(
                    elevation: 2,
                    color: dateIndex == i ? brandColor : Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: lightPurple, width: 2),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: 90,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item3,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 12, fontFamily: "FSJoeyPro", fontWeight: FontWeight.normal),
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item2,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 21.6, fontFamily: "FSJoeyPro Heavy", fontWeight: FontWeight.bold),
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 14.6, fontFamily: "FSJoeyPro", fontWeight: FontWeight.normal),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
        ),
      ],
    );

    /*return model.appointmentsList.length > 0
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: PPTexts.getFormLabelAsterisk(
                      LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label").isEmpty
                          ? "Available dates"
                          : LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label"),
                      fontSize: 14.0,
                      color: darkPrimaryColor,
                      lineHeight: 1.5,
                      fontFamily: "FSJoeyPro Medium",
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(width: 3.0, color: lightPurple),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: DropdownButton(
                      key: Key("availableDate"),
                      //borderRadius: BorderRadius.circular(20),
                      isExpanded: true,
                      underline: Container(),
                      icon: Icon(Icons.keyboard_arrow_down),
                      value: dropValue,
                      //model.appointmentsList[model.currentItem!].displayDate!,
                      items: model.appointmentsList.map<DropdownMenuItem<String>>((AppointmentTime e) {
                        return DropdownMenuItem<String>(value: e.displayDate!, child: Text(e.displayDate!, key: Key(e.index.toString())));
                      }).toList(),
                      onChanged: (String? val) {
                        setState(() {
                          selectedIndex = -1;
                          model.selectedDate = val;
                          selectedItem = model.time[val];
                          model.selectedItem = model.time[val];
                          model.currentItem = model.appointListToActualIndex[selectedItem!.index];
                          showGrid = true;
                          selected = false;
                          dropValue = val;
                          timeDropValue = null;

                          if (selectedItem!.doctorTimeSlots != null) {
                            model.setTimeListDoctorSlot(selectedItem!.doctorTimeSlots);
                          }
                        });
                      },
                      style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                      hint: Text(
                        LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-placeholder").isEmpty
                            ? 'Choose a date'
                            : LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-placeholder"),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SMALL_XX),
              SizedBox(
                height: SMALL_XX,
              ),
              getAvailableTimesDropDown(context, model),
              SizedBox(
                height: SMALL,
              ),
              errorShow == false
                  ? Container()
                  : Padding(
                      padding: EdgeInsets.only(bottom: 5),
                      child: PPTexts.getFormError(LocalizationUtils.getSingleValueString("signup", "signup.appointment.required"), color: errorColor),
                    ),
              SizedBox(height: LARGE),
              selected ? doctorView(model) : Container(),
            ],
          )
        : Padding(
            padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
            child: Container(
              decoration: new BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(40, 0, 0, 0),
                    blurRadius: 3,
                    spreadRadius: 0,
                    offset: Offset(
                      0, // horizontal,
                      1.0, // vertical,
                    ),
                  )
                ],
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(4.0),
                color: whiteColor,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Icon(Icons.event_busy, color: violetColorDark, size: 36),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec").isEmpty
                          ? "Unfortunately, there are currently no slots available for tele-consultation"
                          : LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec"),
                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18.0, height: 1.5, fontFamily: "FSJoeyPro", color: thinGrey),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          );*/
  }
  Widget getAvailableTimesDropDown(BuildContext context, AppointmentRescheduleModel model) {

    if (timeDropValue == null) selectTimeAuto(model);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 3),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label").isEmpty
                    ? 'Available time slots'
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label"),
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                children: [
                  TextSpan(
                    text: ' *',
                    style: TextStyle(color: errorColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            height: 45.0,
            child: ListView.builder(
              controller: _scrollController,
              scrollDirection: Axis.horizontal,
              itemCount: availableTimings.length,
              itemBuilder: (BuildContext context, int i) => InkWell(
                onTap: () {
                  setState(() {
                    timeDropValue = availableTimings[i];
                    int index = availableTimings.indexOf(availableTimings[i]);
                    _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![index];
                    selected = true;
                    selectedIndex = i;
                  });
                },
                child: Card(
                  elevation: 2,
                  color: selectedIndex == i ? brandColor : Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: lightPurple, width: 2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10,right: 10),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.check_circle_outlined,
                            color: selectedIndex == i ? Colors.white : greyColor,
                            size: 12.0,
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Text(
                            availableTimings[i],
                            textAlign: TextAlign.center,
                            style: TextStyle(color: selectedIndex == i ? Colors.white : brandColor, fontFamily: "FSJoeyPro", fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );

    /*return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Align(
            alignment: Alignment.centerLeft,
            child: PPTexts.getFormLabelAsterisk(
                LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label").isEmpty
                    ? 'Available time slots'
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label"),
                fontSize: 14.0,
                color: darkPrimaryColor,
                lineHeight: 1.5,
                fontFamily: "FSJoeyPro Medium",
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              border: Border.all(width: 3.0, color: lightPurple),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: DropdownButton(
                key: Key("availableTimeSlot"),
                //borderRadius: BorderRadius.circular(20),
                isExpanded: true,
                underline: Container(),
                icon: Icon(Icons.keyboard_arrow_down),
                value: timeDropValue,
                //model.appointmentsList[model.currentItem!].displayDate!,
                items: model.getTimeListDoctorSlot()
                    .map((String key, String value) {
                      return MapEntry(
                          key,
                          DropdownMenuItem<String>(
                            value: key,
                            child: Text(value,key: Key(key),),
                          ));
                    })
                    .values
                    .toList(),
                onChanged: (String? val) {
                  setState(() {
                    int index = int.parse(val!);
                    timeDropValue = val;
                    model.index = val;
                    _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![index];
                    models!.appointmentDoctorSlots = _appointmentDoctorSlots;
                    selected = true;
                    errorShow = false;
                    selectedIndex = index;
                  });
                },
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                hint: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-placeholder").isEmpty
                      ? 'Choose a time'
                      : LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-placeholder"),
                ),
              ),
            ),
          ),
        ),
      ],
    );*/
  }

  Widget doctorView(AppointmentRescheduleModel model) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.only(top: 10, bottom: 5),
        decoration: BoxDecoration(
          color: bgDateColor,
          border: Border.symmetric(horizontal: BorderSide(color: Colors.grey.shade300)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey, //(x,y)
              blurRadius: 2.0,
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceSmall),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: SMALL_XX,
              ),
              Text(
                _appointmentDoctorSlots!.doctorName.toString(),
                style: TextStyle(fontSize: 14, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
              ),
              SizedBox(
                height: SMALL,
              ),
              Text(
                _appointmentDoctorSlots!.startTime.toString() + " - " + selectedItem!.displayDate.toString(),
                style: TextStyle(fontSize: 16, fontFamily: "FSJoeyPro Heavy", color: brandColor),
              ),
              SizedBox(
                height: SMALL_XX,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void selectDateAuto(AppointmentRescheduleModel model) {
    Future.delayed(Duration(seconds: 0), () {
      setState(() {
        String val = model.appointmentsList[0].displayDate!;
        selectedIndex = -1;
        model.selectedDate = val;
        selectedItem = model.time[val];
        model.currentItem = model.appointListToActualIndex[selectedItem!.index];
        showGrid = true;
        selected = false;
        dropValue = val;
        if (availableTimings != null) {
          availableTimings.clear();
        }
        if (selectedItem!.doctorTimeSlots != null) {
          for (int j = 0; j < selectedItem!.doctorTimeSlots!.length; j++) {
            var dataItem = selectedItem!.doctorTimeSlots![j];
            availableTimings.add(dataItem.startTime.toString());
          }
        }
      });
    });
  }

  void selectTimeAuto(AppointmentRescheduleModel model) {
    Future.delayed(Duration(seconds: 0), () {
      setState(() {
        String val = availableTimings[0];
        timeDropValue = val;
        int index = availableTimings.indexOf(val);
        _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![index];
        selected = true;
        selectedIndex = index;
        autoSelected = true;
      });
    });
  }

  Triple getDisplayDate(String displayDate) {
    var collection = displayDate.split(" ");
    String month = collection[0];
    String date = collection[1].replaceAll(",", "");
    String day = collection[2];
    return Triple(item1: month, item2: date, item3: day);
  }
}
