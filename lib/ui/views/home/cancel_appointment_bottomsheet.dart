import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/telehealth/cancel_appointment_request.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_cancel_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/referral/referral_contact_sync_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class CancelAppointmentWidget extends StatefulWidget {
  BuildContext? context;
  int? prescriptionId;

  CancelAppointmentWidget({this.context, this.prescriptionId});

  @override
  _CancelAppointmentWidgetState createState() => _CancelAppointmentWidgetState();
}

class _CancelAppointmentWidgetState extends BaseState<CancelAppointmentWidget> with SingleTickerProviderStateMixin {
  AppointmentCancelModel? appointmentModel;
  String? _currentSelection;
  String? _currentCancelSelection;
  String? _currentSendingValue;
  bool buttonEnabled = false;
  TextEditingController _reasonController = TextEditingController();

  @override
  void initState() {
    appointmentModel = AppointmentCancelModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: myFutureMethodOverall(appointmentModel!),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          if (_currentSelection == null) {
            _currentSelection = getNoString();
          }
          return getCancelView(context, appointmentModel!);
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      },
    );
  }

  Future<dynamic> myFutureMethodOverall(AppointmentCancelModel model) {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["modal", "common"]);
    return Future.wait([future1]);
  }

  Widget getCancelView(BuildContext context, AppointmentCancelModel model) {
    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        child: SingleChildScrollView(
          reverse: true,
          child: Wrap(key: Key("Cancel View"), children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
              ),
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: PPTexts.getSecondaryHeading(
                        LocalizationUtils.getSingleValueString("modal", "modal.cancel.title"),
                        fontSize: PPUIHelper.FontSizeXXLarge,
                      )),
                  Divider(
                    color: secondaryColor,
                    height: 1,
                  ),
                  SizedBox(
                    height: PPUIHelper.VerticalSpaceMedium,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: PPTexts.getSecondaryHeading(
                      LocalizationUtils.getSingleValueString("modal", "modal.cancel.cancel-question"),
                      fontSize: 16.8,
                      color: darkPrimaryColor,
                      fontFamily: "FSJoeyPro Bold",
                      isBold: true,
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(10), child: getRadioChoice()),
                  _currentSelection == getYesString()
                      ? Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: PPTexts.getSecondaryHeading(
                            LocalizationUtils.getSingleValueString("modal", "modal.cancel.cancel-reason"),
                            fontSize: 16.8,
                            color: darkPrimaryColor,
                            fontFamily: "FSJoeyPro Bold",
                            isBold: true,
                          ),
                        )
                      : Container(),
                  _currentSelection == getYesString() ? getRadioReasonChoice() : Container(),
                  PPUIHelper.verticalSpaceSmall(),
                  _currentCancelSelection == getOtherReason() && _currentSelection == getYesString() ? getTextArea() : Container(),
                  PPUIHelper.verticalSpaceMedium(),
                  PPDivider(),
                  PPUIHelper.verticalSpaceMedium(),
                  SizedBox(
                    height: 70,
                    child: model.state == ViewState.Busy
                        ? ViewConstants.progressIndicator
                        : Row(
                            children: [
                              Expanded(
                                key: Key('close'),
                                child: getButtonCancel(
                                    text: LocalizationUtils.getSingleValueString("modal", "modal.all.close"),
                                    color: Colors.white,
                                    textColor: brandColor,
                                    callBack: () {
                                      Navigator.pop(context);
                                    }),
                              ),
                              Expanded(
                                key: Key('proceed'),
                                child: getButtonProceed(
                                    text: LocalizationUtils.getSingleValueString("modal", "modal.cancel.proceed"),
                                    textColor: Colors.white,
                                    color: brandColor,
                                    callBack: () {
                                      if (_currentSelection == getNoString()) {
                                        Navigator.pop(context);
                                      } else {
                                        cancelAppointment(widget.prescriptionId, model);
                                      }
                                    }),
                              ),
                            ],
                          ),
                  ),
                ],
              ),
            )
          ] /*
            ),*/
              ),
        ),
      ),
    );
  }

  void cancelAppointment(int? prescriptionId, AppointmentCancelModel model) async {
    setState(() {
      model.setState(ViewState.Busy);
    });
    CancelAppointmentRequest cancelRequest = CancelAppointmentRequest(
        omsPrescriptionId: prescriptionId,
        cancellationSmsFlag: false,
        reasonForCancellation: _currentCancelSelection == getOtherReason()
            ? "CANCELLATION REASON: " + _currentSendingValue! + " " + _reasonController.text
            : "CANCELLATION REASON: " + _currentSendingValue!,
        sendCancellationFax: false,
        status: "CANCELLED",
        createNewTelehealthPrescription: false);
    bool success = await model.cancelAppointment(cancelRequest, prescriptionId!);
    if (success) {
      Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (route) => false);
    } else {
      Fluttertoast.showToast(msg: model.errorMessage,toastLength: Toast.LENGTH_LONG);
    }
    setState(() {
      model.setState(ViewState.Idle);
    });
  }

  String getYesString() {
    return LocalizationUtils.getSingleValueString("common", "common.labels.yes");
  }

  String getNoString() {
    return LocalizationUtils.getSingleValueString("common", "common.labels.no");
  }

  String getNoLonger() {
    return LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-nolonger").isEmpty
        ? "I no longer need the appointment"
        : LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-nolonger");
  }

  String getConsultElse() {
    return LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-consltelse").isEmpty
        ? "I’m getting a consultation somewhere else"
        : LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-consltelse");
  }

  String getNotAvail() {
    return LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-ntavail").isEmpty
        ? "This time slot doesn’t work for me"
        : LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-ntavail");
  }

  String getOtherReason() {
    return LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-other").isEmpty
        ? "Other"
        : LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-other");
  }

  Widget getRadioChoice() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: getRadioOption(getYesString(), "yes"),
        ),
        Expanded(
          flex: 1,
          child: getRadioOption(getNoString(), "no"),
        ),
        Expanded(
            flex: 2,
            child: SizedBox(
              width: 10,
            ))
      ],
    );
  }

  Widget getRadioReasonChoice() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        getCancelReasons(getNoLonger(), "I no longer need the appointment"),
        getCancelReasons(getConsultElse(), "I’m getting a consultation somewhere else"),
        getCancelReasons(getNotAvail(), "This time slot doesn’t work for me"),
        getCancelReasons(getOtherReason(), "Other"),
      ],
    );
  }

  Widget getRadioOption(String value, String key) {
    return InkWell(
      onTap: () {
        setState(() {
          _currentSelection = value;
          if (_currentSelection == getNoString()) {
            buttonEnabled = false;
          }
          if (_currentSelection == getYesString() && _currentCancelSelection != null) {
            buttonEnabled = true;
          }
        });
      },
      child: Row(
        children: [
          Expanded(
            child: Radio(
              key: Key(key),
              value: value,
              groupValue: _currentSelection,
              onChanged: (String? value) {
                setState(() {
                  _currentSelection = value;
                  if (_currentSelection == getNoString()) {
                    buttonEnabled = false;
                  }
                  if (_currentSelection == getYesString() && _currentCancelSelection != null) {
                    buttonEnabled = true;
                  }
                });
              },
              activeColor: brandColor,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              value,
              style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro"),
            ),
          ),
          Expanded(flex: 1, child: SizedBox(width: 5))
        ],
      ),
    );
  }

  Widget getTextArea() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Column(
        children: [
          PPTexts.getSecondaryHeading(
            LocalizationUtils.getSingleValueString("modal", "modal.cancel.cancel-reason-title"),
            fontSize: PPUIHelper.FontSizeMedium,
            isBold: true,
          ),
          SizedBox(
            height: 4,
          ),
          TextField(
            textAlign: TextAlign.justify,
            decoration: InputDecoration(
              hintText: LocalizationUtils.getSingleValueString("modal", "modal.cancel.reason-placeholder"),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: lightPurple,
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: brandColor,
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            controller: _reasonController,
            textInputAction: TextInputAction.newline,
            maxLines: 4,
            expands: false,
          ),
        ],
      ),
    );
  }

  Widget getButtonCancel({String text = "", Color? color = brandColor, Color? textColor = brandColor, Function()? callBack}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          fixedSize: (Size.fromWidth(MediaQuery.of(context).size.width / 2)),
          primary: color,
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
          side: BorderSide(color: brandColor, width: 3),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        onPressed: callBack,
        child: Text(
          text.toUpperCase(),
          style: TextStyle(fontWeight: FontWeight.bold, color: textColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro"),
        ),
      ),
    );
  }

  Widget getButtonProceed({String text = "", Color? color = brandColor, Color? textColor = brandColor, Function()? callBack}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          fixedSize: (Size.fromWidth(MediaQuery.of(context).size.width / 2)),
          primary: color,
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
          side: BorderSide(color: brandColor, width: 3),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        onPressed: _currentSelection == getNoString()
            ? callBack
            : _currentSelection == getYesString() && buttonEnabled
                ? callBack
                : null,
        child: Text(
          text.toUpperCase(),
          style: TextStyle(fontWeight: FontWeight.bold, color: textColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro"),
        ),
      ),
    );
  }

  Widget getCancelReasons(String value, String key) {
    return InkWell(
      onTap: () {
          setState(() {
            buttonEnabled = true;
            _currentCancelSelection = value;
            _currentSendingValue = key;
          });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Radio(
            key: Key(key),
            value: value,
            groupValue: _currentCancelSelection,
            onChanged: (String? value) {
              setState(() {
                buttonEnabled = true;
                _currentCancelSelection = value;
                _currentSendingValue = key;
              });
            },
            activeColor: brandColor,
          ),
          Text(
            value,
            style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro"),
          ),
          SizedBox(width: 5)
        ],
      ),
    );
  }
}
