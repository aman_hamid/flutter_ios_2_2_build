import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';

class DashboardLongActionCardView extends StatelessWidget {
  final DashboardItem? dashboardItem;
  final HomeModel? homeModel;

  DashboardLongActionCardView({@required this.dashboardItem, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    return PPCard(
      onTap: () {
        homeModel!.handleDashboardRoute(dashboardItem!.getActionType()!, context, dashboardItem: dashboardItem);
      },
      cardColor: lightBrownColor,
      margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM),
      padding: EdgeInsets.all(MEDIUM_X),
      roundedRectangleBorder: RoundedRectangleBorder(
        side: BorderSide(color: darkBrownColor),
        borderRadius: BorderRadius.circular(SMALL_X),
      ),
      child: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  dashboardItem!.title!,
                  style: MEDIUM_XX_PRIMARY_FAX,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                  textAlign: TextAlign.center,
                  key:Key(dashboardItem!.getActionType()!.toString()),
                ),
                SizedBox(
                  height: SMALL_X,
                ),
                Text(
                  dashboardItem!.description!,
                  style: MEDIUM_XXX_PRIMARY_BOLD_FAX,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
