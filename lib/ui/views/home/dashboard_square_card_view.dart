import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/dashboard/dashboard_item_list.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';

class DashboardSquareCardView extends StatelessWidget {
  final DashboardItemList? dashboardItemList;
  final HomeModel? homeModel;

  DashboardSquareCardView({required this.dashboardItemList, required this.homeModel});

  @override
  Widget build(BuildContext context) {
    if (dashboardItemList?.data == null) {
      return PPContainer.emptyContainer();
    }
    List<Widget> column = [];
    int dataLength = dashboardItemList!.data!.length;
    for (int i = 0; i < dataLength; i++) {
      List<Widget> rowGridView = [];
      rowGridView.add(getRowWidget(dashboardItemList!.data![i], context));
      if (dataLength > i + 1) {
        rowGridView.add(getRowWidget(dashboardItemList!.data![i + 1], context));
      } else {
        rowGridView.add(getRowWidget(null, context));
      }
      i++;
      Widget widget = IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: rowGridView,
        ),
      );
      column.add(widget);
    }

    return Padding(
      padding: const EdgeInsets.only(
        left: SMALL_XXX,
        right: SMALL_XXX,
        top: SMALL_XXX,
      ),
      child: Column(
        children: column,
      ),
    );
  }

  Widget getRowWidget(DashboardItem? dashboardItem, BuildContext context) {
    if (dashboardItem != null) {
      return Container(
        width: (MediaQuery.of(context).size.width - 16) / 2,
        child: PPCard(
          key: Key("square "+dashboardItem.action!),
            onTap: () {
              homeModel!.handleDashboardRoute(dashboardItem.getActionType()!, context, dashboardItem: dashboardItem);
            },
            margin: EdgeInsets.all(SMALL_XXX),
            padding: EdgeInsets.only(left: MEDIUM_XX, right: MEDIUM_X, top: MEDIUM_X, bottom: MEDIUM_X),
            child: Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.network(
                  dashboardItem.icon!,
                  height: REGULAR_XXX,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  height: SMALL_XXX,
                ),
                Text(
                  dashboardItem.title!,
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  key:Key("homeSquareCardTextItem"),
                ),
                SizedBox(
                  height: SMALL_X,
                ),
                Text(
                  dashboardItem.description!,
                  style: MEDIUM_XX_SECONDARY,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ],
            ))),
      );
    } else {
      return Expanded(child: PPContainer.emptyContainer());
    }
  }

/*
        new GridView.builder(
          shrinkWrap: true,
          primary: false,
          scrollDirection: Axis.vertical,
          itemCount: dashboardItemList.data.length,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 8 / 7),
          itemBuilder: (BuildContext context, int index) {
            DashboardItem dashboardItem = dashboardItemList.data[index];
            return
            PPCard(
                onTap: () {
                  homeModel.handleDashboardRoute(dashboardItem.getActionType(), context, dashboardItem: dashboardItem);
                },
                margin: EdgeInsets.all(SMALL_XXX),
                padding: EdgeInsets.only(left: MEDIUM_X, right: MEDIUM_X, top: MEDIUM_X, bottom: SMALL),
                child: Container(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.network(
                      dashboardItem.icon,
                      height: REGULAR_XXX,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                    Text(
                      dashboardItem.title,
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: SMALL_X,
                    ),
                    Text(
                      dashboardItem.description,
                      style: MEDIUM_XX_SECONDARY,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ],
                )));
          })
   */
}
