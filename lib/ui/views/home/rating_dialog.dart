import 'dart:io';

import 'package:app_review/app_review.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/dashboard/rating_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/star_rating.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/webview/webview_arguments.dart';
import 'package:pocketpills/ui/views/webview/webview_home_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class RatingAlertDialog extends BaseStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider<RatingModel>(create: (_) => RatingModel())],
        child: Consumer<RatingModel>(builder: (BuildContext context, RatingModel ratingModel, Widget? child) {
          return AlertDialog(
            title: Text(
              LocalizationUtils.getSingleValueString("modal", "modal.rating.title"),
              style: REGULAR_XXX_PRIMARY_BOLD,
            ),
            content: Container(
              child: StarRating(
                  rating: ratingModel.rating,
                  color: yellowColor,
                  onRatingChanged: (rating) {
                    ratingModel.rating = rating;
                  }),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(LocalizationUtils.getSingleValueString("modal", "modal.rating.button-not-now")),
                onPressed: () {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_rate_us_not_now);
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text(LocalizationUtils.getSingleValueString("modal", "modal.rating.button-yes")),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (ratingModel.rating >= 4) {
                    onClickAppReview();
                  } else {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_rate_us_yes);
                    Navigator.pushNamed(context, WebviewHomeView.routeName, arguments: WebviewArguments(sourceUrl: ApplicationConstant.REVIEW_GOOGLE_FORM_URL));
                  }
                },
              ),
            ],
          );
        }));
  }

  onClickAppReview() {
    if (Platform.isIOS) {
      AppReview.writeReview.then((onValue) {
        //nothing
      });
    } else {
      _launchURL(ApplicationConstant.PLAY_STORE_URL);
    }
  }

  _launchURL(String orderTrackurl) async {
    if (orderTrackurl != null) {
      if (await canLaunch(orderTrackurl)) {
        await launch(orderTrackurl);
      } else {
        throw 'Could not launch $orderTrackurl';
      }
    }
  }
}
