import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/heading_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/home/cancel_appointment_bottomsheet.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/home/reschedule_time_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';
import 'package:pocketpills/core/response/telehealth/past_appointment_response.dart';

class DashboardAppointmentDetailsView extends StatefulWidget {
  static const routeName = 'dashboardAppointmentDetailsView';
  final int? prescriptionId;

  DashboardAppointmentDetailsView({this.prescriptionId});

  @override
  State<StatefulWidget> createState() {
    return DashboardAppointmentDetailsViewState();
  }
}

class DashboardAppointmentDetailsViewState extends BaseState<DashboardAppointmentDetailsView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late ScrollController _scrollController;
  List<bool> selectedIndexes = [];
  bool itemSelected = false;
  late AppointmentTime selectedItem;

  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController(
      // NEW
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider<AppointmentDashboardModel>(create: (_) => AppointmentDashboardModel())],
        child: Consumer<AppointmentDashboardModel>(
          builder: (BuildContext context, AppointmentDashboardModel appointmentDashboardModel, Widget? child) {
            return FutureBuilder(
                future: appointmentDashboardModel.getLocalization(["consultation", "common", "modal", "medications"]),
                builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
                  if (snapshot.hasData != null && snapshot.data != null) {
                    print("from details ${dataStore.readString(DataStoreService.PROVINCE)}");
                    analyticsEvents.sendAnalyticsEvent("appointment_details_open");
                    return getMainView(appointmentDashboardModel);
                  } else if (snapshot.hasError) {
                    return ErrorScreen();
                  } else {
                    return LoadingScreen();
                  }
                });
          },
        ));
  }

  Widget getMainView(AppointmentDashboardModel appointmentDashboardModel) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("consultation", "consultation.appointment.appointment-title"),
        appBar: AppBar(),
      ),
      body: FutureBuilder(
        future: myFutureMethodOverall(appointmentDashboardModel),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (appointmentDashboardModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
            return NoInternetScreen(
              onClickRetry: appointmentDashboardModel.clearAsyncMemoizer,
            );
          }

          if (snapshot.hasData && appointmentDashboardModel.connectivityResult != ConnectivityResult.none) {
            return showAppointMentDetailView(snapshot.data[0], snapshot.data[2], appointmentDashboardModel);
          } else if (snapshot.hasError && appointmentDashboardModel.connectivityResult != ConnectivityResult.none) {
            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
            return ErrorScreen();
          }

          if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
            return LoadingScreen();
          }
          return PPContainer.emptyContainer();
        },
      ),
    );
  }

  Future myFutureMethodOverall(AppointmentDashboardModel model) async {
    Future<AppointmentFirstResponse?> future1 = model.fetchPrescriptionById(widget.prescriptionId!);
    Future<List<TelehealthProvinceItem>?> future2 = model.getProvinceList();
    Future<List<PastAppointment>?> future3 = model.getPastTelehealthAppointment();
    return await Future.wait([future1, future2, future3]);
  }

  Widget showAppointMentDetailView(AppointmentFirstResponse? appointment, List<PastAppointment>? past, AppointmentDashboardModel model) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Padding(
        padding: EdgeInsets.only(left: 10, right: 10, top: 30),
        child: Column(
          key: Key("Appointment Detail View"),
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 2,
                    color: Colors.grey,
                  )
                ],
              ),
              child: getAppointmentDetailCard(appointment, model),
            ),
            PPUIHelper.verticalSpaceMedium(),
            model.requestedMedication != null && model.requestedMedication!.isNotEmpty
                ? Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 2,
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                color: Color(0xFFF7F7F7),
                                child: Text(
                                  LocalizationUtils.getSingleValueString("consultation", "consultation.appointment.current-medications"),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600, color: primaryColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
                                ),
                              ),
                              showRequestedMedication(appointment, model),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                : Container(),
            PPUIHelper.verticalSpaceMedium(),
            past != null && past.isNotEmpty ? getPastAppointmentList(past) : Container(),
            PPUIHelper.verticalSpaceXLarge(),
          ],
        ),
      ),
    );
  }

  Widget getAppointmentDetailCard(AppointmentFirstResponse? appointment, AppointmentDashboardModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          color: quartiaryColor,
          child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
              child: Text("Current Appointments",
                  style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeXLarge, fontFamily: "FSJoeyPro"))),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    getTextField(text: getSelectedLanguage() == ViewConstants.languageIdEn ? appointment!.timeSlotEn : appointment!.timeSlotFr),
                    PPUIHelper.verticalSpaceSmall(),
                    getTextField(
                        text: getSelectedLanguage() == ViewConstants.languageIdEn ? appointment.slotDateEn : appointment.slotDateFr,
                        fontWeight: FontWeight.bold,
                        color: primaryColor,
                        fontSize: PPUIHelper.FontSizeXLarge,
                        fontFamily: "FSJoeyPro Bold"),
                    /*PPUIHelper.verticalSpaceSmall(),
                    getTextField(text: 'Clinic: ' + appointment.teleHealthDoctor!.telehealthPharmacyName!),*/
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    appointment.prescriptionDTO!.doctor != null
                        ? getTextField(
                            text: appointment.prescriptionDTO!.doctor!.doctorNameWithDr!,
                          )
                        : SizedBox(height: 0),
                    PPUIHelper.verticalSpaceSmall(),
                    appointment.prescriptionMedicalConditions == null
                        ? Container()
                        : getTextField(
                            text: LocalizationUtils.getSingleValueString("medications", "medications.label.reason") +
                                ": " +
                                appointment.prescriptionMedicalConditions.toString()),
                    /*,
                    getTextField(text: 'Clinic: ' + appointment.teleHealthDoctor!.telehealthPharmacyName!),*/
                  ],
                ),
              ),
            ],
          ),
        ),
        appointment.prescriptionComment != null && appointment.prescriptionComment!.isNotEmpty
            ? showDescriptionContainer(appointment)
            : Container(),
        Container(
          height: 45,
          color: quartiaryColor,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextButton(
                    onPressed: () {
                      showCancelBottomSheet(context, model, widget.prescriptionId);
                    },
                    child: Text(
                      LocalizationUtils.getSingleValueString("common", "common.button.cancel").toUpperCase(),
                      style: TextStyle(fontWeight: FontWeight.w600, color: brandColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
                    )),
                SizedBox(
                  width: 8,
                ),
                getRescheduleButton(model)
                    ? TextButton(
                        onPressed: () {
                          showRescheduleBottomSheet(
                              context,
                              model,
                              appointment.prescriptionDTO!.prescriptionFilledByExternalPharmacy,
                              appointment.prescriptionDTO!.prescriptionMedicalConditions != null
                                  ? appointment.prescriptionDTO!.prescriptionMedicalConditions
                                  : appointment.prescriptionMedicalConditions!,
                              appointment.prescriptionDTO!.prescriptionComment != null
                                  ? appointment.prescriptionDTO!.prescriptionComment
                                  : appointment.prescriptionComment,
                              appointment.prescriptionDTO!.appointmentTime.toString() != null
                                  ? appointment.prescriptionDTO!.appointmentTime.toString()
                                  : appointment.timeSlot,
                              appointment.prescriptionDTO!.telehealthRequestedMedications,
                              widget.prescriptionId,
                              model.showGrid,
                              model.time[model.selectedDate],
                              appointment.slotDate,
                              appointment.timeSlot);
                        },
                        child: Text(
                          LocalizationUtils.getSingleValueString("common", "common.button.reschedule"),
                          style: TextStyle(fontWeight: FontWeight.w600, color: brandColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget getTextField(
      {required String? text,
      FontWeight? fontWeight = FontWeight.w400,
      Color? color = secondaryColor,
      double? fontSize = PPUIHelper.FontSizeMedium,
      String? fontFamily = "FSJoeyPro"}) {
    return Text(text!, style: TextStyle(fontWeight: fontWeight, color: color, fontSize: fontSize, fontFamily: fontFamily));
  }

  Widget showDescriptionContainer(AppointmentFirstResponse? appointment) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(height: 1, color: tertiaryColor.withOpacity(0.3)),
        Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 10, right: 10, top: 15,),
          child: Text(
            LocalizationUtils.getSingleValueString("consultation", "consultation.appointment.description"),
            style: TextStyle(fontWeight: FontWeight.w600, color: primaryColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10,right: 10, top: 15,bottom: 15),
          color: Colors.white,
          child: Text(
            appointment!.prescriptionComment!,
            style: TextStyle(fontWeight: FontWeight.normal, color: secondaryColor, fontSize: PPUIHelper.FontSizeXLarge, fontFamily: "FSJoeyPro"),
          ),
        ),
      ],
    );
  }

  Widget showRequestedMedication(AppointmentFirstResponse? appointment, AppointmentDashboardModel model) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.requestedMedication!.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    model.requestedMedication![index],
                    style: TextStyle(fontWeight: FontWeight.normal, color: secondaryColor, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro"),
                  ),
                ),
              ),
              PPDivider(),
            ],
          );
        });
  }

  Widget getPastAppointmentList(List<PastAppointment?> past) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 2),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            color: quartiaryColor,
            child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
                child: Text("Past Appointments",
                    style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeXLarge, fontFamily: "FSJoeyPro"))),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  border: index == 0
                      ? const Border() // This will create no border for the first item
                      : Border(top: BorderSide(width: 1, color: tertiaryColor.withOpacity(0.3))), // This will create top borders for the rest
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
                  child: getPastItemCards(past[index]!),
                ),
              );
            },
            itemCount: past.length,
          ),
        ],
      ),
    );
  }

  Widget getPastItemCards(PastAppointment past) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          flex: 2,
          child: Container(
            child: Column(
              children: [
                Align(
                    alignment: Alignment.centerLeft,
                    child: getTextField(
                        text: past.slotDate,
                        fontWeight: FontWeight.bold,
                        color: primaryColor,
                        fontSize: PPUIHelper.FontSizeXLarge,
                        fontFamily: "FSJoeyPro Bold")),
                PPUIHelper.verticalSpaceSmall(),
                Align(
                  alignment: Alignment.centerLeft,
                  child: getTextField(text: past.timeSlot),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 30,
          color: lightBlueColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: getTextField(text: past.status!, color: thinGrey, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
          ),
        ),
      ],
    );
  }

  static Widget getPrescriptionCardHeading(TelehealthAppointmentResponse appointment) {
    String reason = ViewConstants.getPrescriptionStatus(appointment.prescriptionRequestReason);
    if (reason.isNotEmpty && reason.length > 1) {
      reason = '${reason[0].toUpperCase()}${reason.substring(1).toLowerCase()}';
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Expanded(child: PPTexts.getHeading(reason))],
      );
    } else {
      return Container();
    }
  }

  showRescheduleBottomSheet(
      BuildContext context,
      AppointmentDashboardModel models,
      bool? prescriptionFilledByExternalPharmacy,
      String? prescriptionMedicalConditions,
      String? prescriptionComment,
      String? appointmentTime,
      String? telehealthRequestedMedications,
      int? prescriptionId,
      bool showGrid,
      AppointmentTime? appointment,
      String? slotDate,
      String? slotTime) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return RescheduleTimeSheet(
            context: bottomSheetContext,
            prescriptionFilledByExternalPharmacy: prescriptionFilledByExternalPharmacy,
            prescriptionMedicalConditions: prescriptionMedicalConditions,
            prescriptionComment: prescriptionComment,
            appointmentTime: appointmentTime,
            telehealthRequestedMedications: telehealthRequestedMedications,
            prescriptionId: prescriptionId,
            showGrid: showGrid,
            selectedItem: appointment,
            slotDate: slotDate,
            slotTime: slotTime,
          );
        });
  }

  showCancelBottomSheet(BuildContext context, AppointmentDashboardModel models, int? prescriptionId) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8)),
        ),
        builder: (BuildContext bottomSheetContext) {
          return CancelAppointmentWidget(
            context: bottomSheetContext,
            prescriptionId: prescriptionId,
          );
        }).then((value) => () {
          if (value != null && value.isNotEmpty) {
            Fluttertoast.showToast(msg: value, toastLength: Toast.LENGTH_LONG);
          }
        });
  }

  bool getRescheduleButton(AppointmentDashboardModel model) {
    String userProvince = dataStore.readString(DataStoreService.PROVINCE).toString();
    bool enable = true;
    for (int i = 0; i < model.provinceArrayList.length; i++) {
      if (model.provinceArrayList[i].label.toUpperCase() == userProvince.toUpperCase() && model.provinceArrayList[i].appointmentSlotsEnabled == false) {
        enable = false;
        break;
      }
    }
    return enable;
  }
}
