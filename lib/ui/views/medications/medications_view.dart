import 'dart:io';

import 'package:basic_utils/basic_utils.dart' as basic_utils;
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/medication.dart';
import 'package:pocketpills/core/response/medications_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/medications/medications_refill_action_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class MedicationsWidget extends StatefulWidget {
  static const routeName = 'medications';
  final int? bottomNavPos;

  MedicationsWidget(this.bottomNavPos);

  @override
  State<StatefulWidget> createState() {
    return MedicationsState();
  }
}

class MedicationsState extends BaseState<MedicationsWidget> {
  bool bottomSheetShown = false;
  PersistentBottomSheetController? persistentBottomSheetController;
  Map<String, String>? medicationTypeStates;
  Map<String, String>? medicationTypeStatesDescription;

  bool initialize = false;

  @override
  void initState() {
    if (!Provider.of<DashboardModel>(context, listen: false).tabPosition!.contains(1)) {
      Provider.of<DashboardModel>(context, listen: false).tabPosition!.add(1);
      Provider.of<HomeModel>(context, listen: false).clearData();
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.medications);
    super.initState();
  }

  _onBackPressed(BuildContext context) {
    List<int>? arrayValue = Provider.of<DashboardModel>(context, listen: false).tabPosition;
    if (arrayValue != null && arrayValue.length != 0) {
      Provider.of<DashboardModel>(context, listen: false).tabPosition!.removeAt(arrayValue.length - 1);
      Provider.of<DashboardModel>(context, listen: false).dashboardIndex = Provider.of<DashboardModel>(context, listen: false).tabPosition!.last;
      Provider.of<HomeModel>(context, listen: false).clearData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Consumer<MedicationsModel>(
        builder: (BuildContext context, MedicationsModel medicationsModel, Widget? child) {
          return FutureBuilder(
              future: myFutureMethodOverall(medicationsModel),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                dataStore.writeInt(DataStoreService.BOTTOM_NAV_POSITION, 1);
                if (snapshot.hasData != null && snapshot.data != null) {
                  if (!initialize) {
                    initializeContent();
                    initialize = true;

                    medicationsModel.unmarkAllMedications();
                  }
                  return getMainView(medicationsModel, snapshot);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Future myFutureMethodOverall(MedicationsModel medicationsModel) async {
    Future<MedicationsResponse?> future1 = medicationsModel.fetchMedications();
    Future<Map<String, dynamic>?> future2 = medicationsModel.getLocalization(["medications", "refill"]);
    return await Future.wait([future2, future1]);
  }

  Widget getMainView(MedicationsModel medicationsModel, AsyncSnapshot<dynamic> snapshot) {
    return Scaffold(
      key: Key("medicationView"),
      floatingActionButton: medicationsModel.medicationsResponse != null &&
              medicationsModel.medicationsResponse!.medicines != null &&
              medicationsModel.medicationsResponse!.medicines!.length > 0 &&
              medicationsModel.selectedMedicinesCount! > 0
          ? FloatingActionButton.extended(
              key: Key("medContinue"),
              backgroundColor: brandColor,
              elevation: 10.0,
              isExtended: true,
              //icon: Icon(Icons.arrow_forward),
              label: Text(
                LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase() +
                    "(" +
                    medicationsModel.selectedMedicinesCount.toString() +
                    ")",
                style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro"),
              ),
              onPressed: medicationsModel.selectedMedicinesCount == 0
                  ? null
                  : () async {
                      Navigator.pushNamed(context, MedicationsRefillAction.routeName);
                    })
          : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Builder(
        builder: (BuildContext context) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMedicationsChildren(medicationsModel, medicationsModel.medicationsResponse);
          } else if (snapshot.hasError) {
            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
            return ErrorScreen();
          }
          return LoadingScreen();
        },
      ),
    );
  }

  Widget getMedicationsChildren(MedicationsModel medicationsModel, MedicationsResponse? medicationsResponse) {
    List<Widget> medicationsChildren = [];
    if (medicationsModel.getMarkedMedications() != null) {}
    if (null != medicationsResponse && null != medicationsResponse.medicines) {
      if (medicationsResponse.medicines!.length == 0) {
        return EmptyScreen(
            imageLink: "graphics/empty_medications.png",
            title: LocalizationUtils.getSingleValueString("medications", "medications.all.looks-empty"),
            description: LocalizationUtils.getSingleValueString("medications", "medications.all.empty-description")
                .replaceAll("{{name}}", PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
            showTransfer: true);
      }
      medicationsResponse.medicines!.forEach((key, val) => generateSectionFor(medicationsModel, key, val, medicationsChildren));
      medicationsChildren.add(PPUIHelper.verticalSpace(100));
    } else {
      return EmptyScreen(
          imageLink: "graphics/empty_medications.png",
          title: LocalizationUtils.getSingleValueString("medications", "medications.all.looks-empty"),
          description: LocalizationUtils.getSingleValueString("medications", "medications.all.empty-description")
              .replaceAll("{{name}}", PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
          showTransfer: true);
    }
    return SingleChildScrollView(
      child: Column(children: medicationsChildren),
    );
  }

  generateSectionFor(MedicationsModel model, String medicationType, List<Medication> medicationsList, List<Widget> medicationsChildren) {
    medicationsChildren.add(Column(
      children: <Widget>[
        PPTexts.getSectionHeader(
            child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              PPUIHelper.verticalSpaceSmall(),
              Text(
                medicationTypeStates![medicationType] ?? "",
                style: MEDIUM_XXX_PRIMARY_BOLD,
              ),
              Text(
                medicationTypeStatesDescription![medicationType] ?? "",
                style: MEDIUM_XX_SECONDARY,
              ),
              PPUIHelper.verticalSpaceSmall()
            ],
          ),
        )),
        PPDivider()
      ],
    ));
    medicationsList.asMap().forEach((index,medication) => addCheckboxForMedication(model, medicationsChildren, medicationType, medication,index));
  }

  void addCheckboxForMedication(MedicationsModel medicationsModel, List<Widget> medicationsChildren, String medicationType, Medication medication, int index) {
    return medicationsChildren.add(
      Container(
        child: Column(
          children: <Widget>[
            IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      key: Key("viewDetails"+index.toString()),
                      onTap: _showDetailsBottomSheet(medicationType, medication),
                      child: Padding(
                        padding: const EdgeInsets.only(left: MEDIUM_XXX, top: MEDIUM_XXX, bottom: MEDIUM_XXX),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Text(
                              medication.isSmartPack == 1
                                  ? LocalizationUtils.getSingleValueString("medications", "medications.label.pocketpacks")
                                  : (medication.drugType != null && medication.drugType != "")
                                      ? medication.drugType.toString().toUpperCase()
                                      : "",
                              style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                            ),
                            SizedBox(
                              height: SMALL,
                            ),
                            Text(
                              StringUtils.capitalize(medication.drug.toString()),
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                            SizedBox(
                              height: SMALL_XX,
                            ),
                            Text(
                              LocalizationUtils.getSingleValueString("medications", "medications.label.view-details"),
                              style: MEDIUM_X_LINK,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    key: Key("medicine"+index.toString()),
                    onTap: () async {
                      bool success = medicationsModel.isSelected(medication.id!)!;
                      if (success == true) {
                        await onCheckBoxClick(medicationsModel, false, medicationType, medication);
                      } else {
                        await onCheckBoxClick(medicationsModel, true, medicationType, medication);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: REGULAR_XXX),
                      child: Checkbox(
                        value: medicationsModel.isSelected(medication.id!),
                        onChanged: (bool? value) async => await onCheckBoxClick(medicationsModel, value!, medicationType, medication),
                        activeColor: brandColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            PPDivider()
          ],
        ),
      ),
    );
  }

  _showDetailsBottomSheet(String medicationType, Medication medication) {
    String sigText = "";
    if (medication.sig != null && medication.sig != "") {
      sigText = "\"" + medication.sig!.toString() + "\" ";
    }
    return () async {
      showModalBottomSheet<Null>(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: MEDIUM_XXX),
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          StringUtils.capitalize(medication.drug),
                          style: MEDIUM_XXX_PRIMARY_BOLD,
                        ),
                        Text(
                          medicationTypeStates![medicationType] ?? "",
                          style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                        ),
                        SizedBox(
                          height: SMALL_XX,
                        ),
                        PPTexts.getDescription(sigText),
                        PPTexts.getDescription("-" + medication.doctor!.toString(), isBold: true),
                        PPUIHelper.verticalSpaceMedium(),
                      ],
                    ),
                  ),
                  PPDivider(),
                  Padding(
                    padding: const EdgeInsets.all(MEDIUM_XXX),
                    child: getQuantityUpdate(medication),
                  ),
                  PPDivider(),
                  PPUIHelper.verticalSpaceMedium(),
                  Padding(
                    padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SecondaryButton(
                          text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                          key: Key("bottomSheetClose"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    };
  }

  Widget getQuantityUpdate(Medication medication) {
    if (medication.dgType == "O") {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-filled").toUpperCase(),
                    style: MEDIUM_X_PRIMARY,
                  ),
                  Text(
                    medication.quantityFilled == null ? "" : Utils.removeDecimalZeroFormat(medication.quantityFilled!),
                    style: MEDIUM_X_PRIMARY_BOLD,
                  ),
                ],
              )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-left").toUpperCase(),
                style: MEDIUM_X_PRIMARY,
              ),
              Text(
                LocalizationUtils.getSingleValueString("medications", "medications.label.unlimited"),
                style: MEDIUM_X_PRIMARY_BOLD,
              ),
            ],
          )
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                LocalizationUtils.getSingleValueString("medications", "medications.label.valid-until").toUpperCase(),
                style: MEDIUM_X_PRIMARY,
              ),
              Text(
                medication.validUntil == null ? "" : medication.validUntil!.toString().split(" ")[0],
                style: MEDIUM_X_PRIMARY_BOLD,
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-filled").toUpperCase(),
                style: MEDIUM_X_PRIMARY,
              ),
              Text(
                medication.quantityFilled == null ? "" : Utils.removeDecimalZeroFormat(medication.quantityFilled!),
                style: MEDIUM_X_PRIMARY_BOLD,
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-left").toUpperCase(),
                style: MEDIUM_X_PRIMARY,
              ),
              Text(
                medication.quantityLeft == null ? "" : Utils.removeDecimalZeroFormat(medication.quantityLeft!),
                style: MEDIUM_X_PRIMARY_BOLD,
              ),
            ],
          )
        ],
      );
    }
  }

  onCheckBoxClick(MedicationsModel medicationsModel, bool value, String medicationType, Medication medication) async {
    if (medicationType == "RENEWAL_DUE" && value == true && medicationsModel.showRenewalPopup == true) {
      showModalBottomSheet<Null>(
          context: context,
          isScrollControlled: true,
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PPTexts.getHeading(medicationTypeStates![medicationType] ?? ""),
                    PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("medications", "medications.all.secondary-heading"), isBold: false),
                    PPUIHelper.verticalSpaceMedium(),
                    PPDivider(),
                    PPUIHelper.verticalSpaceMedium(),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SecondaryButton(
                          isExpanded: true,
                          text: LocalizationUtils.getSingleValueString("common", "common.button.close"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(
                          width: PPUIHelper.HorizontalSpaceSmall,
                        ),
                        PrimaryButton(
                          isExpanded: true,
                          text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                          onPressed: () async {
                            medicationsModel.setShowRenewalPopup(false);
                            Navigator.pop(context);
                            await paintBottomBar(medicationsModel, value, medication);
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
    } else if (medicationType == "AUTO_REFILL" && value == true && medicationsModel.showRenewalPopup == true) {
      showModalBottomSheet<Null>(
          context: context,
          isScrollControlled: true,
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PPTexts.getHeading(medicationTypeStates![medicationType] ?? ""),
                    PPUIHelper.verticalSpaceSmall(),
                    PPTexts.getDescription(
                        LocalizationUtils.getSingleValueString("medications", "medications.all.refill-secondary-heading").replaceAll('\\', '')),
                    PPUIHelper.verticalSpaceMedium(),
                    Padding(
                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: PPFormFields.getMultiLineTextField(
                        controller: medicationsModel.reasonController,
                        decoration: PPInputDecor.getDecoration(
                          hintText: LocalizationUtils.getSingleValueString("medications", "medications.label.give-reason"),
                          labelText: LocalizationUtils.getSingleValueString("medications", "medications.label.reason"),
                        ),
                      ),
                    ),
                    Divider(),
                    PPUIHelper.verticalSpaceSmall(),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SecondaryButton(
                          isExpanded: true,
                          text: LocalizationUtils.getSingleValueString("common", "common.button.close"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(
                          width: PPUIHelper.HorizontalSpaceSmall,
                        ),
                        PrimaryButton(
                          isExpanded: true,
                          text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                          onPressed: () async {
                            medicationsModel.setShowRefillPopup(false);
                            Navigator.pop(context);
                            await paintBottomBar(medicationsModel, value, medication);
                          },
                        )
                      ],
                    ),
                    PPUIHelper.verticalSpaceMedium()
                  ],
                ),
              ),
            );
          });
    } else
      await paintBottomBar(medicationsModel, value, medication);
  }

  Future paintBottomBar(MedicationsModel medicationsModel, bool value, Medication medication) async {
    setState(() {
      if (value == true) {
        medicationsModel.markMedicationSelected(medication.id!);
      } else {
        medicationsModel.unmarkMedicationSelected(medication.id!);
      }
    });
  }

  clearAll(MedicationsModel medicationsModel) {
    medicationsModel.unmarkAllMedications();
    if (bottomSheetShown == true) {
      persistentBottomSheetController!.close();
      bottomSheetShown = false;
      //dashboardModel.displayBottomBar();
    }
    setState(() {});
  }

  continueToNavBar() {
    Navigator.pushNamed(context, MedicationsRefillAction.routeName);
  }

  void initializeContent() {
    medicationTypeStates = {
      "NOT_TRANSFERRED": LocalizationUtils.getSingleValueString("medications", "medications.all.heading-NOT_TRANSFERRED"),
      "REFILL_VALID": LocalizationUtils.getSingleValueString("medications", "medications.all.heading-REFILL_VALID"),
      "AUTO_REFILL": LocalizationUtils.getSingleValueString("medications", "medications.all.heading-AUTO_REFILL")
    };

    medicationTypeStatesDescription = {
      "NOT_TRANSFERRED": LocalizationUtils.getSingleValueString("medications", "medications.all.subheading-NOT_TRANSFERRED"),
      "REFILL_VALID": LocalizationUtils.getSingleValueString("medications", "medications.all.subheading-REFILL_VALID"),
      "AUTO_REFILL": LocalizationUtils.getSingleValueString("medications", "medications.all.subheading-AUTO_REFILL")
    };
  }
}
