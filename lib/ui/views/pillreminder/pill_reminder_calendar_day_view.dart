import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_day_wise_text_status.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_status_update_button.dart';
import 'package:pocketpills/utils/screen_util.dart';
import 'package:pocketpills/utils/string_utils.dart';

class PillReminderCalendarDayView extends StatefulWidget {
  PillReminderModel? pillReminderModel;
  DayWiseMedications? dayWiseMedications;

  PillReminderCalendarDayView({this.pillReminderModel, this.dayWiseMedications});

  @override
  _PillReminderCalendarDayViewState createState() => _PillReminderCalendarDayViewState();
}

class _PillReminderCalendarDayViewState extends State<PillReminderCalendarDayView> {
  @override
  Widget build(BuildContext context) {
    return getMainCalendarDayPackView(context, widget.dayWiseMedications!, widget.pillReminderModel!);
  }

  Widget getMainCalendarDayPackView(BuildContext context, DayWiseMedications dayWiseMedications, PillReminderModel pillReminderModel) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.black.withOpacity(0.7),
          body: InkWell(
            onTap: () {
              pillReminderModel.showDayView = false;
            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SingleChildScrollView(
                      child: Container(
                          child: Align(
                    alignment: Alignment.bottomCenter,
                    // Center is a layout widget. It takes a single child and positions it
                    // in the middle of the parent.
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(SMALL_XXX)),
                        color: whiteColor,
                      ),
                      child: FractionallySizedBox(
                        widthFactor: 0.9,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(SMALL_XXX), topRight: Radius.circular(SMALL_XXX)),
                                color: whiteColor,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(top: MEDIUM_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      dayWiseMedications.title!,
                                      style: REGULAR_XXX_PRIMARY_BOLD,
                                    ),
                                    Text(
                                      StringUtils.getFormattedDateEEEEddMMMM(dayWiseMedications.date!),
                                      style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            IntrinsicHeight(
                              child: Container(
                                color: Colors.white,
                                padding: EdgeInsets.only(top: SMALL_XXX),
                                child: FractionallySizedBox(
                                    widthFactor: 1.12,
                                    heightFactor: 1,
                                    child: Container(width: double.infinity, child: getPillReminderTimes(dayWiseMedications.pocketPackDetails!, pillReminderModel))),
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              height: MEDIUM_XXX,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(SMALL_XXX), bottomRight: Radius.circular(SMALL_XXX)),
                                color: whiteColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ))),
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: LARGE,
                        ),
                        InkWell(
                          onTap: () {
                            pillReminderModel.showDayView = false;
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(LARGE_XX)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: SMALL_XXX,
                                ),
                              ],
                              color: whiteColor,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(MEDIUM_XXX),
                              child: Icon(
                                Icons.close,
                                size: 32,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        Text(
                          "CLOSE",
                          style: MEDIUM_XXX_WHITE_BOLD,
                        ),
                        SizedBox(
                          height: 32,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }

  Widget getPillReminderTimes(List<PocketPackDetails> pocketPackDetails, PillReminderModel pillReminderDayModel) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.fromLTRB(LARGE_X, 0.0, LARGE_X, MEDIUM),
          child: IntrinsicHeight(
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: pocketPackDetails.length == 0
                    ? <Widget>[PPContainer.emptyContainer()]
                    : pocketPackDetails
                        .map(
                          (pocketPackDetail) => Container(
                            constraints: BoxConstraints(maxWidth: pocketPackDetails.length == 1 ? ScreenUtil.screenWidthDp - 64 : ScreenUtil.screenWidthDp * .75),
                            child: PPCard(
                                onTap: () async {},
                                cardColor: whiteColor,
                                margin: EdgeInsets.only(left: SMALL_XXX, right: SMALL_XXX, top: SMALL_X),
                                padding: EdgeInsets.all(0.0),
                                child: Container(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(topRight: Radius.circular(SMALL_X), topLeft: Radius.circular(SMALL_X)),
                                        color: headerBgColor,
                                      ),
                                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_X),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                pocketPackDetail.partOfDay ?? "",
                                                style: MEDIUM_XXX_PRIMARY_BOLD,
                                              ),
                                              Text(
                                                StringUtils.getFormattedTimeHHMMNN(pocketPackDetail.time!),
                                                style: MEDIUM_X_SECONDARY_BOLD_MEDIUM,
                                              )
                                            ],
                                          ),
                                          PillReminderDayWiseTextStatus(pocketPackDetail),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: MEDIUM_X, horizontal: MEDIUM_XXX),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: pocketPackDetail.medicationDetails!.map((v) {
                                              return Padding(
                                                padding: const EdgeInsets.symmetric(vertical: SMALL_X),
                                                child: Text(
                                                  Utils.removeDecimalZeroFormat(v.quantity!) + " x " + v.medication!,
                                                  style: MEDIUM_XX_PRIMARY,
                                                ),
                                              );
                                            }).toList()),
                                      ),
                                    ),
                                    PillReminderStatusUpdateButton(
                                      updateMissedStatus: () {
                                        updateMissedStatus(pillReminderDayModel, pocketPackDetail);
                                      },
                                      updateTakenStatus: () {
                                        updateTakenStatus(pillReminderDayModel, pocketPackDetail);
                                      },
                                      pocketPackDetails: pocketPackDetail,
                                    )
                                  ],
                                ))),
                          ),
                        )
                        .toList()),
          ),
        ),
      ),
    );
  }

  void updateMissedStatus(PillReminderModel pillReminderDayModel, PocketPackDetails pocketPackDetail) async {
    await pillReminderDayModel.updatePillReminderStatus(PillStatusType.MISSED, pocketPackDetail.pocketPackIds!);
  }

  void updateTakenStatus(PillReminderModel pillReminderDayModel, PocketPackDetails pocketPackDetail) async {
    await pillReminderDayModel.updatePillReminderStatus(PillStatusType.TAKEN, pocketPackDetail.pocketPackIds!);
  }
}
