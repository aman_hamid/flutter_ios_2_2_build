import 'package:pocketpills/core/models/medicine.dart';

class CopayRequestArguments {
  Medicine? medicine;
  double? quantity;
  CopayRequestArguments({this.medicine, this.quantity});
}
