import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/consent_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class WaitConsentWidget extends BaseStatelessWidget {
  static const routeName = 'wait_consent';
  final UserPatient? userPatient;
  final String? from;

  WaitConsentWidget({Key? key, this.userPatient, this.from}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: from == "dashboard"
          ? () async {
              Navigator.pop(context);
              return true;
            }
          : () async {
              Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
              Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
              return true;
            },
      child: ChangeNotifierProvider(
        create: (_) => ConsentModel(),
        child: Consumer<ConsentModel>(builder: (BuildContext context, ConsentModel model, Widget? child) {
          return FutureBuilder(
              future: myFutureMethodOverall(model, context),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(model, context);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(ConsentModel model, BuildContext context) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["common", "signup", "modal"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(ConsentModel model, BuildContext context) {
    return Scaffold(
        appBar: InnerAppBar(
          titleText: LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.member-consent"),
          leadingBackButton: () {
            if (from == "dashboard") {
              Navigator.pop(context);
            } else {
              Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
              Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
            }
          },
          appBar: AppBar(),
        ),
        body: Builder(builder: (BuildContext context) {
          return SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    PPUIHelper.verticalSpaceMedium(),
                    PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.waiting-consent")),
                    PPUIHelper.verticalSpaceSmall(),
                    PPTexts.getDescription(getDescriptionText(PatientUtils.getForGender(userPatient), userPatient)),
                    PPUIHelper.verticalSpaceLarge(),
                    PrimaryButton(
                      fullWidth: true,
                      isExpanded: true,
                      text: LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.resend-sms"),
                      onPressed: onClick(context, model),
                    )
                  ],
                ),
              ),
            ),
          );
        }));
  }

  onClick(context, ConsentModel model) {
    return () async {
      var success = await model.resendConsent(userPatient!);
      if (success == true) {
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false,
            arguments: DashboardArguments(snackBarMessage: LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.sms-info")));
      } else
        onFail(context, errMessage: model.errorMessage);
    };
  }
}

String getDescriptionText(String gender, UserPatient? userPatient) {
  String phoneNumber = userPatient!.patient == null
      ? ""
      : userPatient.patient!.phone == null
          ? ""
          : userPatient.patient!.phone.toString();
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("modal", "modal.consent.msg-major-MALE")
          .replaceAll("{{name}}", userPatient.patient?.firstName ?? "")
          .replaceAll("{{phone}}", phoneNumber);
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("modal", "modal.consent.msg-major-FEMALE")
          .replaceAll("{{name}}", userPatient.patient?.firstName ?? "")
          .replaceAll("{{phone}}", phoneNumber);
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("modal", "modal.consent.msg-major-other")
          .replaceAll("{{name}}", userPatient.patient?.firstName ?? "")
          .replaceAll("{{phone}}", phoneNumber);
      break;
    default:
      return LocalizationUtils.getSingleValueString("modal", "modal.consent.msg-major")
          .replaceAll("{{name}}", userPatient.patient?.firstName ?? "")
          .replaceAll("{{phone}}", phoneNumber);
      break;
  }
}
