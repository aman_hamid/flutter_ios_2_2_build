import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/activate_patient_response.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/viewmodels/consent_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/addmember/about_patient.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ActivatePatientWidget extends StatefulWidget {
  static const routeName = 'activatepatient';
  final UserPatient? userPatient;

  ActivatePatientWidget({Key? key, this.userPatient}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ActivatePatientState();
  }
}

class ActivatePatientState extends BaseState<ActivatePatientWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _cellController = TextEditingController();
  bool isAbove19 = false;

  @override
  void initState() {
    super.initState();
    isAbove19 = PPDateUtils.isAbove14(widget.userPatient!.patient!.birthDate!);
    ConsentModel model = ConsentModel();
    getLanguageData(model);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ConsentModel(),
      child: Consumer<ConsentModel>(builder: (BuildContext context, ConsentModel consentModel, Widget? child) {
        String heading = LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.activate-member");
        String description =
            LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.activate-description1").replaceAll("{{name}}", widget.userPatient!.patient!.firstName!);
        if (isAbove19 == true) {
          description =
              LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.activate-description2").replaceAll("{{name}}", widget.userPatient!.patient!.firstName!);
        }
        return Scaffold(
          appBar: InnerAppBar(
            titleText: LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.member-consent"),
            appBar: AppBar(),
          ),
          bottomNavigationBar: Builder(
            builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
              child: consentModel.state == ViewState.Busy
                  ? ViewConstants.progressIndicator
                  : PrimaryButton(
                      fullWidth: true,
                      text: LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.activate"),
                      onPressed: onClick(context, consentModel),
                    ),
            ),
          ),
          body: Builder(
            builder: (BuildContext context) => SafeArea(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                child: Builder(
                  builder: (BuildContext context) {
                    return SingleChildScrollView(
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            PPUIHelper.verticalSpaceMedium(),
                            PPTexts.getMainViewHeading(heading),
                            PPUIHelper.verticalSpaceSmall(),
                            PPTexts.getDescription(description),
                            PPUIHelper.verticalSpaceMedium(),
                            isAbove19 == false
                                ? Container()
                                : PPFormFields.getNumericFormField(
                                    keyboardType: TextInputType.phone, textInputAction: TextInputAction.done, maxLength: 10, minLength: 10, controller: _cellController),
                            PPUIHelper.verticalSpaceMedium(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  onClick(context, ConsentModel consentModel) {
    return () async {
      if (_formKey.currentState!.validate()) {
        ActivatePatientResponse? res = await consentModel.activatePatient(widget.userPatient!, _cellController.text);
        if (res != null) {
          widget.userPatient!.patient = res.patient;
          Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
          await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
          await Provider.of<DashboardModel>(context, listen: false).setSelectedPatientId(res.patient.id!);
          if (res.patient.hasViewedAboutYouPatient == null || res.patient.hasViewedAboutYouPatient == false) {
            Navigator.pushReplacementNamed(context, AboutPatientWidget.routeName,
                arguments: TransferArguments(
                    userPatient: widget.userPatient!,
                    snackBarMessage:
                        LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.message").replaceAll("{{name}}", widget.userPatient!.patient!.firstName!)));
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false,
                arguments: DashboardArguments(
                    snackBarMessage:
                        LocalizationUtils.getSingleValueString("signup", "signup.activate-patient.message").replaceAll("{{name}}", widget.userPatient!.patient!.firstName!)));
          }
        } else
          onFail(context, errMessage: consentModel.errorMessage);
      }
    };
  }
}

void getLanguageData(ConsentModel model) {
  FutureBuilder(
      future: myFutureMethodOverall(model),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null) {
          return snapshot.data;
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      });
}

Future myFutureMethodOverall(ConsentModel model) async {
  Future<Map<String, dynamic>?> future1 = model.getLocalization(["common", "signup"]);
  return await Future.wait([future1]);
}
