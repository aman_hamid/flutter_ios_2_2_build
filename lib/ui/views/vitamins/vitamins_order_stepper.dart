import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_stepper_model.dart';
import 'package:pocketpills/ui/custom_stepper.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_address_view.dart';
import 'package:pocketpills/ui/views/profile/profile_payment_view.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_order_success_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_review_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class VitaminsOrderStepper extends StatefulWidget {
  static const String routeName = 'VitaminsOrderstepper';

  final int? startStep;
  final BaseStepperSource? source;

  VitaminsOrderStepper({Key? key, this.source = BaseStepperSource.UNKNOWN_SCREEN, this.startStep = 0}) : super(key: key);

  @override
  _VitaminsOrderStepperState createState() => _VitaminsOrderStepperState();
}

class _VitaminsOrderStepperState extends State<VitaminsOrderStepper> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getOrderStepper();
  }

  Widget getOrderStepper() {
    return Consumer<VitaminsStepperModel>(
      builder: (BuildContext context, VitaminsStepperModel stepperModel, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(stepperModel),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(stepperModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Future myFutureMethodOverall(VitaminsStepperModel stepperModel) async {
    Future<Map<String, dynamic>?> future1 = stepperModel.getLocalization(["stepper", "vitamins"]);

    return await Future.wait([future1]);
  }

  Widget getMainView(VitaminsStepperModel stepperModel) {
    return Scaffold(

      appBar: InnerAppBar(
        titleText: widget.source == BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN
            ? LocalizationUtils.getSingleValueString("vitamins", "vitamins.all.details-update")
            : LocalizationUtils.getSingleValueString("vitamins", "vitamins.all.compete-order"),
        appBar: AppBar(),
      ),
      body: CustomStepper(
        controlsBuilder: (BuildContext context, {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
          return PPContainer.emptyContainer();
        },
        type: CustomStepperType.horizontal,
        currentStep: stepperModel.getCurrentStep(),
        onStepTapped: (int step) {
          stepperModel.setCurrentStep(step: step);
        },
        onStepContinue: stepperModel.getCurrentStep() < 2 ? () => stepperModel.incrCurrentStep() : null,
        onStepCancel: stepperModel.getCurrentStep() > 0 ? () => stepperModel.decrCurrentStep() : null,
        steps: <CustomStep>[
          CustomStep(
            title: Padding(
              padding: const EdgeInsets.only(left:15.0),
              child: Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.review"),
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 14,fontFamily: "FSJoeyPro Medium"),
                key: Key("vitaminOrderReview"),
              ),
            ),
            content: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: VitaminsReviewWidget(
                onBack: onBack(context, stepperModel),
                onSuccess: onSuccess(context, stepperModel, widget.source!),
                source: widget.source,
              ),
            ),
            isActive: stepperModel.currentStep >= 0,
            state: stepperModel.currentStep == 0 ? CustomStepState.editing : CustomStepState.complete,
          ),
          CustomStep(
            title: new Text(
              LocalizationUtils.getSingleValueString("stepper", "stepper.labels.address"),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14,fontFamily: "FSJoeyPro Medium"),
              key: Key("vitaminAddress"),
            ),
            content: SizedBox(
                //widget.source
                height: MediaQuery.of(context).size.height,
                child: ProfileAddressView(
                    source: widget.source!,
                    onBack: onBack(context, stepperModel),
                    onSuccess: onSuccess(context, stepperModel, widget.source!),
                    position: stepperModel.getCurrentStep())),
            isActive: stepperModel.currentStep >= 1,
            state: stepperModel.currentStep == 1 ? CustomStepState.editing : (stepperModel.currentStep >= 1 ? CustomStepState.complete : CustomStepState.disabled),
          ),
          CustomStep(
            title: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: new Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.payment"),
                textAlign: TextAlign.end,
                style: TextStyle(fontSize: 14,fontFamily: "FSJoeyPro Medium"),
              ),
            ),
            content: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: ProfilePaymentView(
                  source: widget.source!,
                  onBack: onBack(context, stepperModel),
                  onSuccess: onSuccess(context, stepperModel, widget.source!),
                  position: stepperModel.getCurrentStep(),
                )),
            isActive: stepperModel.currentStep >= 2,
            state: stepperModel.currentStep == 2 ? CustomStepState.editing : (stepperModel.currentStep >= 2 ? CustomStepState.complete : CustomStepState.disabled),
          ),
        ],
      ),
    );
  }
}

onBack(BuildContext context, VitaminsStepperModel stepperModel) {
  return () async {
    stepperModel.decrCurrentStep();
  };
}

onSuccess(BuildContext context, VitaminsStepperModel stepperModel, BaseStepperSource source) {
  return () async {
    if (stepperModel.currentStep < 2)
      stepperModel.incrCurrentStep();
    else {
      Navigator.pushNamed(context, VitaminsOrderSuccessScreen.routeName, arguments: BaseStepperArguments(source: source));
      stepperModel.resetWithoutNotify();
    }
  };
}
