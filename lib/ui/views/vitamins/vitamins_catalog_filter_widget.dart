import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/models/vitamins/item_group_filter_list.dart';
import 'package:pocketpills/core/response/vitamins/coupon_code.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_filter_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class VitaminsCatalogFilterWidget extends StatefulWidget {
  static const routeName = 'vitaminsCatalogFilter';

  String? itemId;
  BaseStepperSource? source;

  VitaminsCatalogFilterWidget({this.itemId = "", this.source = BaseStepperSource.UNKNOWN_SCREEN});

  @override
  _VitaminsCatalogFilterWidgetState createState() => _VitaminsCatalogFilterWidgetState();
}

class _VitaminsCatalogFilterWidgetState extends BaseState<VitaminsCatalogFilterWidget> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<VitaminsCatalogFilterModel>.value(
      value: VitaminsCatalogFilterModel(itemId: widget.itemId),
      child: Consumer<VitaminsCatalogFilterModel>(
        builder: (BuildContext context, VitaminsCatalogFilterModel vitaminsCatalogFilterModel, Widget? child) {
          return FutureBuilder(
            future: myFutureMethodOverall(vitaminsCatalogFilterModel),
            // ignore: missing_return
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (vitaminsCatalogFilterModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return NoInternetScreen(
                  onClickRetry: vitaminsCatalogFilterModel.clearAsyncMemorizer,
                );
              }

              if (snapshot.hasData && snapshot.data != null && vitaminsCatalogFilterModel.connectivityResult != ConnectivityResult.none) {
                return _onVitaminsCatalogLoad(context, vitaminsCatalogFilterModel, snapshot.data[0]!);
              } else if (snapshot.hasError && vitaminsCatalogFilterModel.connectivityResult != ConnectivityResult.none) {
                FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                return ErrorScreen();
              }

              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                return LoadingScreen();
              }

              return Container();
            },
          );
        },
      ),
    );
  }

  Future myFutureMethodOverall(VitaminsCatalogFilterModel model) async {
    Future<List<ItemGroupFilterList>?> future1 = model.fetchVitaminsFilter();
    Future<Map<String, dynamic>?> future2 = model.getLocalization(["vitamins"]);
    return await Future.wait([future1, future2]);
  }

  Widget _onVitaminsCatalogLoad(BuildContext context, VitaminsCatalogFilterModel vitaminsCatalogFilterModel, List<ItemGroupFilterList> medicines) {
    return BaseScaffold(
      appBar: AppBar(
        backgroundColor: brandColor,
        elevation: 1,
        brightness: Platform.isIOS == true ? Brightness.light : null,
        iconTheme: IconThemeData(color: Colors.white60 //change your color here
            ),
        centerTitle: false,
        title: Text(LocalizationUtils.getSingleValueString("vitamins", "vitamins.title.build-pack"),style: TextStyle(fontFamily: "FSJoeyPro Bold"),),
        actions: AppBarIcons.getAppBarIcons(context, aboutUs: false),
        leading: InkWell(
            onTap: () {
              widget.source == BaseStepperSource.NEW_USER
                  ? Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false)
                  : Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: whiteColor)),
      ),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
          child: vitaminsCatalogFilterModel.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(right: SMALL_XXX),
                        child: RaisedButton(
                          key: Key("vitaminFilterClick"),
                          color: Colors.white,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: tertiaryColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          child: Text(
                            LocalizationUtils.getSingleValueString("vitamins", "vitamins.categories.show-all"),
                            style: MEDIUM_XX_PRIMARY_BOLD,
                            textAlign: TextAlign.center,
                          ),
                          onPressed: () {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_show_all_vitamins);
                            Provider.of<VitaminsCatalogModel>(context, listen: false).clearVitaminList();
                            vitaminsCatalogFilterModel.onClickContinue(true);
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: SMALL_XXX),
                        child: RaisedButton(
                          color: brandColor,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: brandColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          child: Text(
                            LocalizationUtils.getSingleValueString("vitamins", "vitamins.categories.continue").toUpperCase(),
                            style: MEDIUM_XX_WHITE_BOLD,
                          ),
                          onPressed: () {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_show_filter_vitamins);
                            Provider.of<VitaminsCatalogModel>(context, listen: false).clearVitaminList();
                            vitaminsCatalogFilterModel.onClickContinue(false);
                          },
                        ),
                      ),
                    )
                  ],
                ),
        ),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              return Stack(children: <Widget>[
                Container(
                  color: whiteColor,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Text(
                          LocalizationUtils.getSingleValueString("vitamins", "vitamins.categories.title"),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: LARGE_X,
                              letterSpacing: Platform.isIOS ? 1.6 : 0, fontWeight: FontWeight.w800, height: 1.4, color: primaryColor,fontFamily: "FSJoeyPro Medium"),
                        ),
                        Expanded(child: renderVitaminsCatalog(context, vitaminsCatalogFilterModel, medicines)),
                      ],
                    ),
                  ),
                ),
                vitaminsCatalogFilterModel.state != ViewState.Busy
                    ? Container()
                    : Container(color: Colors.white30, child: Center(child: SizedBox(height: LARGE_X, width: LARGE_X, child: CircularProgressIndicator())))
              ]);
            },
          ),
        ),
      ),
    );
  }

  Widget renderVitaminsCatalog(BuildContext context, VitaminsCatalogFilterModel vitaminsCatalogModel, List<ItemGroupFilterList> itemGroupFilterList) {
    return GridView.count(
        key: Key("vitaminList"),
        crossAxisCount: 3,
        padding: EdgeInsets.all(0.0),
        childAspectRatio: 8.0 / 9.0,
        children: itemGroupFilterList
            .map((value) => Container(
                  decoration: new BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromARGB(40, 0, 0, 0),
                        blurRadius: 3,
                        spreadRadius: 0,
                        offset: Offset(
                          0, // horizontal,
                          1.0, // vertical,
                        ),
                      )
                    ],
                    border: (value.isInVitaminFilter != null && value.isInVitaminFilter == true)
                        ? Border.all(color: Color(0xFFE3DBFF), width: 1)
                        : Border.all(color: whiteColor, width: 1),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(4.0),
                    color: (value.isInVitaminFilter != null && value.isInVitaminFilter == true) ? bghighlight : whiteColor,
                  ),
                  margin: EdgeInsets.all(SMALL_XXX),
                  child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(),
                    margin: EdgeInsets.all(1.0),
                    elevation: 0,
                    child: InkWell(
                      key: Key("vitaminItemClick"+value.id.toString()),
                      borderRadius: BorderRadius.circular(4.0),
                      child: Container(
                        padding: EdgeInsets.all(0.0),
                        child: Container(
                          color: (value.isInVitaminFilter != null && value.isInVitaminFilter == true) ? bghighlight : whiteColor,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: MEDIUM,
                                  right: MEDIUM,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    CachedNetworkImage(
                                      placeholder: (context, url) => Container(
                                        height: LARGE_X,
                                      ),
                                      imageUrl: value.itemGroupIconUrl!,
                                      height: LARGE_X,
                                    ),
                                    SizedBox(
                                      height: MEDIUM_XXX,
                                    ),
                                    Text(
                                      value.itemGroupName != null ? value.itemGroupName! : "",
                                      style: MEDIUM_XX_SECONDARY,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      maxLines: 3,
                                    ),
                                  ],
                                ),
                              ), //MEDIUM_XX_SECONDARY
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        (value.isInVitaminFilter != null && value.isInVitaminFilter == true)
                            ? vitaminsCatalogModel.removeMedicineFromShoppingCart(value)
                            : vitaminsCatalogModel.addMedicineToShoppingCart(value);
                      },
                    ),
                  ),
                ))
            .toList());
  }

  Widget getCardHeadLine(Medicine medicine) {
    if (medicine.benefitsHeadline != null) {
      return Container(
        width: MediaQuery.of(context).size.width * 0.3,
        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(MEDIUM_X)), color: headerBgColor),
        height: REGULAR_XXX,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: MEDIUM_XXX,
            ),
            Text(
              medicine.benefitsHeadline != null ? medicine.benefitsHeadline! : "",
              style: MEDIUM_X_SECONDARY_BOLD,
            )
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  void autoApplycouponCode(VitaminsCatalogModel vitaminsCatalogModel) {
    CouponCode? couponCode = vitaminsCatalogModel.addMedicineResponse.couponCode;
    if (couponCode == null) {
      var list = vitaminsCatalogModel.getCouponCodeList();
      list.then((value) => {
            if (value![0].title != null && value[0].title != "") {autoApplyCouponCode(context, value[0].title ?? "")}
          });
    }
  }

  autoApplyCouponCode(BuildContext context, String title) async {
    bool success = await Provider.of<VitaminsCatalogModel>(context).updateShoppingCartCoupon(title);
    if (success) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_apply_coupon);
    }
  }
}
