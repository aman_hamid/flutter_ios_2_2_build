import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/response/vitamins/coupon_code.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_bottom_sheet.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_coupon_view.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_details_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_order_stepper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class VitaminsCatalogWidget extends StatefulWidget {
  static const routeName = 'vitaminsCatalog';

  final BaseStepperSource? source;
  final String? filterArgu;

  VitaminsCatalogWidget({this.source = BaseStepperSource.UNKNOWN_SCREEN, this.filterArgu = ""});

  @override
  _VitaminsCatalogWidgetState createState() => _VitaminsCatalogWidgetState();
}

class _VitaminsCatalogWidgetState extends BaseState<VitaminsCatalogWidget> {
  final TextEditingController _vitaminNotFoundCommentController = TextEditingController();

  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController(
      // NEW
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(
      builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget? child) {
        return FutureBuilder(
          future: vitaminsCatalogModel.fetchVitaminsCatalog(widget.filterArgu!),
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot<List<Medicine>?> snapshot) {
            if (vitaminsCatalogModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
              return NoInternetScreen(
                onClickRetry: vitaminsCatalogModel.clearAsyncMemorizer,
              );
            }

            if (snapshot.hasData && snapshot.data != null && vitaminsCatalogModel.connectivityResult != ConnectivityResult.none) {
              return _onVitaminsCatalogLoad(context, vitaminsCatalogModel, snapshot.data!);
            } else if (snapshot.hasError && vitaminsCatalogModel.connectivityResult != ConnectivityResult.none) {
              FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
              return ErrorScreen();
            }

            if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
              return LoadingScreen();
            }

            return Container();
          },
        );
      },
    );
  }

  Widget _onVitaminsCatalogLoad(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, List<Medicine> medicines) {
    return BaseScaffold(
      appBar: AppBar(
        backgroundColor: brandColor,
        elevation: 1,
        brightness: Platform.isIOS == true ? Brightness.light : null,
        iconTheme: IconThemeData(color: Colors.white60 //change your color here
            ),
        centerTitle: false,
        title: Text(widget.source == BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN
            ? LocalizationUtils.getSingleValueString("vitamins", "vitamins.title.modify-pack")
            : LocalizationUtils.getSingleValueString("vitamins", "vitamins.title.build-pack"),style:TextStyle( fontFamily: "FSJoeyPro Bold"),),
        actions: AppBarIcons.getAppBarIcons(context, aboutUs: false),
        leading: InkWell(
            onTap: () {
              widget.source == BaseStepperSource.NEW_USER
                  ? Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false)
                  : Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: whiteColor)),
      ),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => VitaminsBottomSheet(
          onBack: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_catalog_back);
            widget.source == BaseStepperSource.NEW_USER
                ? Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false)
                : Navigator.pop(context);
          },
          onContiune: () async {
            bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
            if (connectivityResult == false) {
              onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
              return;
            }

            List<Medicine>? result = await vitaminsCatalogModel.getVitaminsData("");
            if (result!.length > 0) {
              if (vitaminsCatalogModel.addMedicineResponse.shoppingCartItems != null && vitaminsCatalogModel.addMedicineResponse.shoppingCartItems!.length > 0) {
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_review);
                autoApplycouponCode(vitaminsCatalogModel);
                //previous vitamins_SCREEN
                if (_vitaminNotFoundCommentController.text != null && _vitaminNotFoundCommentController.text != "") {
                  bool success = await Provider.of<VitaminsCatalogModel>(context).updateShoppingCartComment(_vitaminNotFoundCommentController.text);
                  if (success == true) {
                    Navigator.pushNamed(context, VitaminsOrderStepper.routeName, arguments: BaseStepperArguments(source: widget.source));
                  } else {
                    onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
                  }
                } else {
                  Navigator.pushNamed(context, VitaminsOrderStepper.routeName, arguments: BaseStepperArguments(source: widget.source));
                }
              } else {
                onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.error-select"));
              }
            } else {
              onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
            }
          },
          stepNumber: 2,
        ),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              return Stack(children: <Widget>[
                Container(
                  color: whiteColor,
                  child: Padding(
                    padding: const EdgeInsets.all(SMALL_XXX),
                    child: ListView(
                      children: <Widget>[
                        VitaminsCouponView(
                          source: BaseStepperSource.UNKNOWN_SCREEN,
                          padding: EdgeInsets.all(SMALL_XXX),
                        ),
                        renderVitaminsCatalog(context, vitaminsCatalogModel, medicines),
                        Padding(
                          padding: const EdgeInsets.all(SMALL_XXX),
                          child: vitaminNotFoundCard(),
                        ),
                      ],
                    ),
                  ),
                ),
                vitaminsCatalogModel.state != ViewState.Busy
                    ? Container()
                    : Container(color: Colors.white30, child: Center(child: SizedBox(height: LARGE_X, width: LARGE_X, child: CircularProgressIndicator())))
              ]);
            },
          ),
        ),
      ),
    );
  }

  Widget renderVitaminsCatalog(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, List<Medicine> medicines) {
    return new GridView.builder(
        key: Key("vitaminCatalog"),
        shrinkWrap: true,
        primary: false,
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        itemCount: vitaminsCatalogModel.vitaminsCatalogList.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 8 / 9.5),
        itemBuilder: (BuildContext context, int index) {
          int key = vitaminsCatalogModel.vitaminsCatalogList.keys.elementAt(index);
          Medicine? value = vitaminsCatalogModel.vitaminsCatalogList[key];
          return Container(
            decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(40, 0, 0, 0),
                  blurRadius: 3,
                  spreadRadius: 0,
                  offset: Offset(
                    0, // horizontal,
                    1.0, // vertical,
                  ),
                )
              ],
              border:
                  (value!.isInShoppingCart != null && value.isInShoppingCart == true) ? Border.all(color: Color(0xFFE3DBFF), width: 1) : Border.all(color: whiteColor, width: 1),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(4.0),
              color: (value.isInShoppingCart != null && value.isInShoppingCart == true) ? bghighlight : whiteColor,
            ),
            margin: EdgeInsets.all(SMALL_XXX),
            child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(),
              margin: EdgeInsets.all(1.0),
              elevation: 0,
              child: InkWell(
                key: Key("vitaminDetails"+index.toString()),
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    color: (value.isInShoppingCart != null && value.isInShoppingCart == true) ? bghighlight : whiteColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(left: MEDIUM_X, right: MEDIUM_X, top: MEDIUM),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  CachedNetworkImage(
                                    placeholder: (context, url) => Container(
                                      height: REGULAR_XX,
                                    ),
                                    imageUrl: value.benefitsHeadlineIconUrl!,
                                    height: REGULAR_XX,
                                  ),
                                  SizedBox(
                                    height: SMALL_X,
                                  ),
                                  Text(
                                    value.benefitsHeadline != null ? value.benefitsHeadline! : "",
                                    style: MEDIUM_XX_SECONDARY,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  SizedBox(
                                    height: SMALL,
                                  ),
                                  Text(
                                    value.itemName != null ? value.itemName! : "",
                                    style: MEDIUM_XXX_PRIMARY_BOLD,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                  SizedBox(
                                    height: MEDIUM,
                                  ),
                                  Text(
                                    LocalizationUtils.getSingleValueString("vitamins", "vitamins.all.view-details"),
                                    style: MEDIUM_X_LINK,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Divider(
                                  color: borderColor,
                                  height: 1,
                                  thickness: 1,
                                ),
                                InkWell(
                                  key: Key("vitaminAddRemove"+index.toString()),
                                  onTap: (value.isInShoppingCart != null && value.isInShoppingCart == true)
                                      ? () {
                                          removeVitaminsToCart(context, vitaminsCatalogModel, value);
                                        }
                                      : () {
                                          addVitaminsToCart(context, vitaminsCatalogModel, value);
                                        },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: MEDIUM_X),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "\$" + value.variantListingPrice!.toStringAsFixed(0).toString(),
                                                style: MEDIUM_XXX_PRIMARY_BOLD,
                                              ),
                                              Text(
                                                LocalizationUtils.getSingleValueString("vitamins", "vitamins.all.per-month"),
                                                style: MEDIUM_SECONDARY,
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      (value.isInShoppingCart != null && value.isInShoppingCart == true)
                                          ? FlatButton(
                                              onPressed: () {
                                                removeVitaminsToCart(context, vitaminsCatalogModel, value);
                                              },
                                              child: Text(
                                                LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.remove").toUpperCase(),
                                                style: MEDIUM_XX_RED_BOLD,
                                              ),
                                            )
                                          : FlatButton(
                                              onPressed: () {
                                                addVitaminsToCart(context, vitaminsCatalogModel, value);
                                              },
                                              child: Text(
                                                LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.add").toUpperCase(),
                                                style: MEDIUM_XX_PURPLE_BOLD,
                                              ),
                                            )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_item_details);
                  vitaminsCatalogModel.setSelectedMedicine(value.variantId);
                  Navigator.pushNamed(context, VitaminsCatalogDetailsWidget.routeName);
                },
              ),
            ),
          );
        });
  }

  Widget vitaminNotFoundCard() {
    return GestureDetector(
      onTap: () {
        showVitaminsNotFoundBottomSheet();
      },
      child: Container(
        decoration: BoxDecoration(
          color: quartiaryColor,
          borderRadius: BorderRadius.circular(SMALL_XX),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, top: MEDIUM, bottom: MEDIUM),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.cant-find"),
                      style: MEDIUM_XX_PRIMARY_BOLD,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: SMALL_XX,
                    ),
                    Text(
                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.let-us-know"),
                      style: MEDIUM_X_SECONDARY,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: MEDIUM_XXX),
              child: Image.asset(
                'graphics/vitamin_not_found.png',
                height: 74,
                width: 74,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getCardHeadLine(Medicine medicine) {
    if (medicine.benefitsHeadline != null) {
      return Container(
        width: MediaQuery.of(context).size.width * 0.3,
        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(MEDIUM_X)), color: headerBgColor),
        height: REGULAR_XXX,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: MEDIUM_XXX,
            ),
            Text(
              medicine.benefitsHeadline != null ? medicine.benefitsHeadline! : "",
              style: MEDIUM_X_SECONDARY_BOLD,
            )
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  void addVitaminsToCart(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine value) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_add_to_cart);
    bool result = await vitaminsCatalogModel.addMedicineToShoppingCart(value);
    if (result == false) onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
  }

  void removeVitaminsToCart(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine value) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_remove_from_cart);
    bool result = await vitaminsCatalogModel.removeMedicineFromShoppingCart(value);
    if (result == false) onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
  }

  void autoApplycouponCode(VitaminsCatalogModel vitaminsCatalogModel) {
    CouponCode? couponCode = vitaminsCatalogModel.addMedicineResponse.couponCode;
    if (couponCode == null) {
      var list = vitaminsCatalogModel.getCouponCodeList();
      list.then((value) => {
            if (value!.length > 0 && value[0].title != null && value[0].title != "") {autoApplyCouponCode(context, value[0].title ?? "")}
          });
    }
  }

  void autoApplyCouponCode(BuildContext context, String title) async {
    bool success = await Provider.of<VitaminsCatalogModel>(context).updateShoppingCartCoupon(title);
    if (success) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_apply_coupon);
    }
  }

  showVitaminsNotFoundBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.38,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(MEDIUM_X),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.modal-title"),
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          ),
                          SizedBox(
                            height: SMALL,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.modal-description"),
                            style: MEDIUM_XX_SECONDARY,
                          ),
                          getTextField()
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    color: secondaryColor,
                    height: 1,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
                      child: Row(
                        children: <Widget>[
                          SecondaryButton(
                            text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.close"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                          PrimaryButton(
                            text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.notify-me"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget getTextField() {
    return Column(
      children: <Widget>[
        SizedBox(height: MEDIUM_XXX),
        PPFormFields.getTextField(
          controller: _vitaminNotFoundCommentController,
          textInputAction: TextInputAction.done,
          decoration: PPInputDecor.getDecoration(
              labelText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.enter-vitamins"),
              hintText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.enter-vitamins")),
        ),
      ],
    );
  }
}
