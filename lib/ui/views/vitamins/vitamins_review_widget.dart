import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/response/vitamins/coupon.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/ui/app_expansion_tile_State.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_bottom_sheet.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/freevitamins/time_options.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

import '../../../locator.dart';

class VitaminsReviewWidget extends StatefulWidget {
  static const routeName = 'vitaminsReviewWidget';

  final Function? onSuccess;
  final Function? onBack;
  final BaseStepperSource? source;

  VitaminsReviewWidget({
    Key? key,
    this.source,
    this.onBack,
    this.onSuccess,
  }) : super(key: key);

  @override
  _VitaminsReviewWidgetState createState() => _VitaminsReviewWidgetState();
}

class _VitaminsReviewWidgetState extends BaseState<VitaminsReviewWidget> {
  final GlobalKey<_VitaminsReviewWidgetState> expansionTile = new GlobalKey();
  final GlobalKey<_VitaminsReviewWidgetState> _scaffold = new GlobalKey();

  final TextEditingController _invitationCodeController = TextEditingController();
  DataStoreService dataStore = locator<DataStoreService>();
  bool isShowCouponCode = true;
  bool enableOffer = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (Provider.of<VitaminsCatalogModel>(context).getSelectedMedicineList().length == 0) {
      Navigator.pop(context);
    }
    String? couponCode = Provider.of<VitaminsCatalogModel>(context).addMedicineResponse.couponCode?.userSuppliedCouponCode.toString();
    couponCode != null ? _invitationCodeController.text = couponCode : _invitationCodeController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(
      builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget? child) {
        return _showSelectedVitamins(context, vitaminsCatalogModel);
      },
    );
  }

  Widget _showSelectedVitamins(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel) {
    Map<int, Medicine> selectedMedicineList = vitaminsCatalogModel.getSelectedMedicineList();
    return Scaffold(
      key: _scaffold,
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => VitaminsBottomSheet(
          onBack: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_review_back);
            Navigator.pop(context);
          },
          onContiune: () {
            updateShoppingCart(vitaminsCatalogModel);
          },
          stepNumber: 0,
        ),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          reverse: false,
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX, bottom: MEDIUM_XXX),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                      color: quartiaryColor,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: SMALL_XXX,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(MEDIUM_XXX),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.title"),
                                style: MEDIUM_XXX_PRIMARY_BOLD,
                              ),
                              widget.source == BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN
                                  ? Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.autorenew,
                                          color: secondaryColor,
                                          size: 18,
                                        ),
                                        Text(
                                          StringUtils.getSubscriptionDate(Provider.of<VitaminsSubscriptionModel>(context).vitaminsSubscriptionResponse!.nextFillDate!) != null
                                              ? StringUtils.getSubscriptionDate(Provider.of<VitaminsSubscriptionModel>(context).vitaminsSubscriptionResponse!.nextFillDate!)!
                                              : "",
                                          style: MEDIUM_XX_SECONDARY,
                                        )
                                      ],
                                    )
                                  : SizedBox(
                                      height: 0.0,
                                    )
                            ],
                          ),
                        ),
                        ListView.builder(
                          key: Key("vitaminListReview"),
                          primary: false,
                          shrinkWrap: true,
                          itemCount: selectedMedicineList.length,
                          itemBuilder: (BuildContext ctxt, int index) {
                            int key = selectedMedicineList.keys.elementAt(index);
                            return Container(
                              constraints: BoxConstraints(maxWidth: double.infinity),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(SMALL_X), bottomRight: Radius.circular(SMALL_X)),
                                  color: whiteColor,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    PPDivider(),
                                    Padding(
                                      padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    selectedMedicineList[key]!.drugName!,
                                                    style: MEDIUM_XXX_PRIMARY_BOLD,
                                                  ),
                                                  SizedBox(
                                                    height: SMALL,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: getDosageTimes(context, vitaminsCatalogModel, selectedMedicineList[key]!),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "\$" + selectedMedicineList[key]!.drugTotalPrice!.toStringAsFixed(0).toString(),
                                                style: MEDIUM_XXX_PRIMARY_BOLD,
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        TransparentButton(
                                          text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.remove"),
                                          key: Key("vitaminRemoveReview"+index.toString()),
                                          onPressed: () {
                                            removeShoppingCartItem(context, vitaminsCatalogModel, selectedMedicineList[key]!);
                                          },
                                        ),
                                        SizedBox(
                                          height: MEDIUM_XXX,
                                        ),
                                        TransparentButton(
                                          key: Key("vitaminEditDoseReview"+index.toString()),
                                          text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.edit-dosage"),
                                          onPressed: _showBottomSheet(context, vitaminsCatalogModel, selectedMedicineList[key]!),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                (vitaminsCatalogModel.coupons != null && vitaminsCatalogModel.coupons!.length > 0 && !LocalizationUtils.isProvinceQuebec())
                    ? Padding(
                        padding: const EdgeInsets.only(left: MEDIUM_XXX, top: MEDIUM_X, right: MEDIUM_XXX, bottom: REGULAR_X),
                        child: Container(
                          decoration: BoxDecoration(color: quartiaryColor, borderRadius: BorderRadius.circular(SMALL_X)),
                          child: Column(
                            children: <Widget>[
                              AppExpansionTile(
                                key: expansionTile,
                                suffixIcon: true,
                                isListTile: false,
                                initiallyExpanded: true,
                                title: Padding(
                                  padding: const EdgeInsets.all(MEDIUM_X),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-code"),
                                      textAlign: TextAlign.start,
                                      style: TextStyle(fontWeight: FontWeight.w600, fontFamily: "FSJoeyPro Bold"),
                                    ),
                                  ),
                                ),
                                children: <Widget>[
                                  getCouponCodeView(vitaminsCatalogModel),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 0.0,
                      )
              ],
            ),
          ),
        ),
        vitaminsCatalogModel.state != ViewState.Busy
            ? Container()
            : Container(color: Colors.white30, child: Center(child: SizedBox(height: LARGE_X, width: LARGE_X, child: CircularProgressIndicator())))
      ]),
    );
  }

  Widget getCouponCodeView(VitaminsCatalogModel vitaminsCatalogModel) {
    return Column(
      children: <Widget>[
        PPDivider(),
        PPUIHelper.verticalSpaceMedium(),
        Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: Stack(children: <Widget>[
              PPFormFields.getTextField(
                  controller: _invitationCodeController,
                  decoration: PPInputDecor.getDecoration(
                    labelText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.code-optional"),
                    hintText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-hint"),
                  ),
                  validator: (value) {
                    return null;
                  }),
              Align(alignment: Alignment.centerRight, child: getRemoveCouponcodeButton(vitaminsCatalogModel))
            ])),
        FutureBuilder(
            future: vitaminsCatalogModel.getCouponCodeList(),
            builder: (BuildContext context, AsyncSnapshot<List<Coupon>?> snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                CallFirstItemCoupon(context,vitaminsCatalogModel,snapshot.data!);
                return _onCouponCodeListLoad(context, vitaminsCatalogModel, snapshot.data!);
              } else if (snapshot.hasError && snapshot.error != null) {
                return PPContainer.emptyContainer();
              }
              return PPContainer.emptyContainer();
            }),
      ],
    );
  }

  Widget getRemoveCouponcodeButton(VitaminsCatalogModel vitaminsCatalogModel) {
    if (vitaminsCatalogModel.addMedicineResponse.couponCode != null) {
      return FlatButton(
          onPressed: () {
            removeCouponCode(context, vitaminsCatalogModel);
          },
          child: Text(
            LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.remove").toUpperCase(),
            style: MEDIUM_XX_LINK,
          ));
    } else {
      return FlatButton(
          onPressed: () async {
            if (_invitationCodeController.text != null && _invitationCodeController.text != "") {
              bool success = await vitaminsCatalogModel.updateShoppingCartCoupon(_invitationCodeController.text);
              if (success) {
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_apply_coupon);
                onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-applied"));
              } else {
                onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
              }
            } else {
              onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamiins", "vitamins.field.coupon-error"));
            }
          },
          child: Text(
            LocalizationUtils.getSingleValueString("common", "common.button.apply").toUpperCase(),
            style: MEDIUM_XX_LINK,
          ));
    }
  }

  void updateShoppingCart(VitaminsCatalogModel vitaminsCatalogModel) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }
    if (_invitationCodeController.text != null && _invitationCodeController.text != "") {
      bool success = await vitaminsCatalogModel.updateShoppingCartCoupon(_invitationCodeController.text);
      if (success) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_address);
        if (widget.onSuccess != null) widget.onSuccess!();
      } else {
        onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_address);
      if (widget.onSuccess != null) widget.onSuccess!();
    }
  }

  Widget _onCouponCodeListLoad(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, List<Coupon> data) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return Container(
            constraints: BoxConstraints(maxWidth: double.infinity),
            child: Padding(
              padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_XXX, top: SMALL),
              child: DottedBorder(
                  dashPattern: [6, 4],
                  strokeWidth: 1,
                  color: pinkColor,
                  child: Container(
                    color: whiteColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: MEDIUM_X, top: SMALL_XXX, bottom: SMALL_XX),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  data[index].title ?? "",
                                  style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: SMALL,
                                ),
                                Text(
                                  data[index].description ?? "",
                                  style: MEDIUM_X_SECONDARY,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )
                              ],
                            ),
                          ),
                        ),
                        FlatButton(
                            onPressed: () async {
                              if (data[index].title != null && data[index].title != "") {
                                bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
                                if (connectivityResult == false) {
                                  onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
                                  return;
                                }
                                bool success = await vitaminsCatalogModel.updateShoppingCartCoupon(data[index].title ?? "");
                                if (success) {
                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_apply_coupon);
                                  onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-applied"));
                                } else {
                                  onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
                                }
                              } else {
                                onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-error"));
                              }
                            },
                            child: vitaminsCatalogModel.addMedicineResponse.couponCode?.userSuppliedCouponCode.toString() == data[index].title
                                ? Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: REGULAR,
                                        ),
                                      ],
                                      color: successColor,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Icon(
                                        Icons.check,
                                        color: whiteColor,
                                        size: 16,
                                      ),
                                    ),
                                  )
                                : Text(
                                    LocalizationUtils.getSingleValueString("common", "common.button.apply").toUpperCase(),
                                    style: MEDIUM_XX_LINK,
                                  ))
                      ],
                    ),
                  )),
            ),
          );
        });
  }

  _showBottomSheet(BuildContext context, VitaminsCatalogModel vitaminsModel, Medicine medicine) {
    return () async {
      showModalBottomSheet<Null>(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return TimeOptions(
            medicine: medicine,
            showSnackBar: (value) {
              onFail(_scaffold.currentContext, errMessage: value);
            },
          );
        },
      );
    };
  }

  void removeCouponCode(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel) async {
    enableOffer = false;
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }

    bool success = await vitaminsCatalogModel.updateShoppingCartRemoveCouponcode(true);
    if (success) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_remove_coupon);
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-removed"));
      setState(() {
        _invitationCodeController.text = "";
      });
    } else {
      onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
    }
  }

  void removeShoppingCartItem(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine selectedMedicineList) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_remove_from_cart);
    bool result = await vitaminsCatalogModel.removeMedicineFromShoppingCart(selectedMedicineList);
    if (result == false) onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
  }

  void CallFirstItemCoupon(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, List<Coupon> data) {
    if(!LocalizationUtils.isProvinceQuebec() && enableOffer){
      Future.delayed(const Duration(milliseconds: 800), () async {
        if(vitaminsCatalogModel.addMedicineResponse.couponCode?.userSuppliedCouponCode== null || vitaminsCatalogModel.addMedicineResponse.couponCode!.userSuppliedCouponCode.toString().isEmpty){
          int index = 0;
          if(data != null && data.length >0){
            if (data[index].title != null && data[index].title != "") {
              bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
              if (connectivityResult == false) {
                onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
                return;
              }
              bool success = await vitaminsCatalogModel.updateShoppingCartCoupon(data[index].title ?? "");
              if (success) {
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_apply_coupon);
                onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-applied"));
              } else {
                onFail(context, errMessage: vitaminsCatalogModel.errorMessage);
              }
            } else {
              onFail(context, errMessage: LocalizationUtils.getSingleValueString("vitamins", "vitamins.field.coupon-error"));
            }
          }
        }
      });
    }
  }
}

List<Widget> getDosageTimes(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine medicine) {
  List<Widget> ret = [];
  List<String?>? strArr = vitaminsCatalogModel.dosageToTimes(medicine);
  if (medicine.userSigCode == medicine.modifyuserSigCode) {
    String? text;
    switch (medicine.dosage) {
      case 1:
        text = LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.once-day");
        break;
      case 2:
        text = LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.twice-day");
        break;
      case 3:
        text = LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.three-day");
        break;
      case 4:
        text = LocalizationUtils.getSingleValueString("vitamins", "vitamins.cartreview.four-day");
        break;
    }
    ret.add(Text(
      text!,
      style: MEDIUM_XX_SECONDARY,
    ));
  } else {
    for (int i = 0; i < strArr!.length; i++) {
      ret.add(SizedBox(
        height: SMALL_XX,
      ));
      String dash = (i < strArr.length - 1) ? ", " : "";

      ret.add(Text(
        strArr.elementAt(i)! + dash,
        style: MEDIUM_XX_SECONDARY,
      ));
    }
  }
  return ret;
}
