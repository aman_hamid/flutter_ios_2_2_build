import 'package:flutter/material.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';

class PreferenceBottomSheet extends StatefulWidget {
  BuildContext context;
  TelehealthRequest telehealthRequest;
  TelehealthSignUpPreferenceRequest telehealthSignUpRequest;
  PreferenceModel model;
  SignUpTransferModel modelSighUp;
  String from;
  UserPatient? userPatient;
  BaseStepperSource? source;
  PreferenceBottomSheet(this.context, this.telehealthRequest, this.model, this.telehealthSignUpRequest, this.from, this.modelSighUp, this.source, this.userPatient);

  _PreferenceBottomSheetState createState() => _PreferenceBottomSheetState();
}

class _PreferenceBottomSheetState extends BaseState<PreferenceBottomSheet> with SingleTickerProviderStateMixin {
  _PreferenceBottomSheetState();
  PreferenceModel? modelContent;

  @override
  void initState() {
    super.initState();
    modelContent = PreferenceModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(modelContent!),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView(modelContent!);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(PreferenceModel modelSighUp) async {
    Future<Map<String, dynamic>?> future1 = modelSighUp.getLocalization(["modal", "signup"]);
    if (widget.from != "Telehealth") {
      Future<Insurance> future3 = modelSighUp.fetchInsuranceData(Provider.of<DashboardModel>(context, listen: false).selectedPatientId);
      return await Future.wait([future1, future3]);
    } else {
      return await Future.wait([future1]);
    }
  }

  Widget getMainView(PreferenceModel modelContent) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 60.0,
            alignment: Alignment.centerLeft,
            decoration: new BoxDecoration(
                color: lightBlueColor, //new Color.fromRGBO(255, 0, 0, 0.0),
                borderRadius: new BorderRadius.only(topLeft: const Radius.circular(10.0), topRight: const Radius.circular(10.0))),
            child: Padding(
              padding: EdgeInsets.all(MEDIUM_X),
              child: Text(
                LocalizationUtils.getSingleValueString("modal", "modal.consultation.title"),
                style: TextStyle(fontSize: 16.0, color: blackColor, fontWeight: FontWeight.w500,fontFamily: "FSJoeyPro Medium"),
              ),
            ),
          ),
          bottomSheetItems(Icons.medical_services_outlined, LocalizationUtils.getSingleValueString("modal", "modal.consultation.reach-doctor")),
          lineDivider,
          bottomSheetItems(Icons.sms_outlined, LocalizationUtils.getSingleValueString("signup", "signup.consultation-modal.sms")),
          lineDivider,
          bottomSheetItems(Icons.local_shipping_outlined, LocalizationUtils.getSingleValueString("signup", "signup.consultation-modal.delivery")),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: FlatButton(
              key: Key("continuePharmacy"),
              color: Colors.transparent,
              onPressed: () {
                widget.telehealthRequest.prescriptionFilledByExternalPharmacy = true;
                uploadDetails(context, modelContent);
              },
              child: Text(
                LocalizationUtils.getSingleValueString("signup", "signup.consultation-modal.continue-current-pharmacy"),
                style: TextStyle(
                  color: linkColor,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: "FSJoeyPro Medium"
                ),
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: RaisedButton(
              key: Key("continuePPPharmacy"),
              child: Text(
                LocalizationUtils.getSingleValueString("signup", "signup.consultation-modal.continue-pp-pharmacy"),
                style: TextStyle(color: Colors.white,fontSize:16.0,fontFamily: "FSJoeyPro Medium"),
              ),
              color: brandColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
              onPressed: () {
                uploadDetails(context, modelContent);
              },
            ),
          ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget bottomSheetItems(IconData icons, String text) {
    return Container(
      height: 60.0,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.all(MEDIUM_X),
        child: Row(
          children: [
            Icon(
              icons,
              color: darkBlueColor2,
            ),
            SizedBox(width: 15.0),
            Text(
              text,
              style: TextStyle(fontSize: 15.4, color: darkBlueColor2, fontWeight: FontWeight.w500,fontFamily: "FSJoeyPro Medium"),
            ),
          ],
        ),
      ),
    );
  }

  void uploadDetails(BuildContext context, PreferenceModel model) async {
    bool connectivityResult = await model.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (widget.from == "Telehealth") {
      bool success = await model.sendPreferencesSignUp(widget.telehealthSignUpRequest);
      if (success) {
        if (!widget.telehealthSignUpRequest.prescriptionFilledByExternalPharmacy!) {
          dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
          goToNextScreen(widget.modelSighUp);
        } else {
          dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
          if (widget.source == BaseStepperSource.NEW_USER) {
            Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
                arguments: TelehealthArguments(modelSignUp: widget.modelSighUp, source: widget.source, userPatient: widget.userPatient));
          } else {
            goToNextScreen(widget.modelSighUp);
          }
        }
      } else
        model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    } else {
      bool success = await model.sendPreferences(widget.telehealthRequest);
      if (success) {
        if (!widget.telehealthRequest.prescriptionFilledByExternalPharmacy!) {
          Navigator.pushReplacementNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(source: BaseStepperSource.TELEHEALTH_SCREEN, startStep: 0));
        } else {
          if (model.curInsurance!.provincialInsuranceFrontImage != null && model.curInsurance!.provincialInsuranceBackImage != null) {
            print("uooo");
            dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
            dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
            Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
            Provider.of<HomeModel>(context, listen: false).clearData();
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          } else {
            print("here");
            dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
            Navigator.pushReplacementNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(source: BaseStepperSource.TELEHEALTH_SCREEN, startStep: 0));
          }
        }
      } else
        model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    Chips.chips = [];
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }
}
