import 'dart:io';

import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion.dart';
import 'package:pocketpills/core/response/telehealth/medical_condition_validate.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/chips/chiplist_sample.dart';
import 'package:pocketpills/ui/shared/chips/pp_inputchiplist.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/views/signup/customised_signup.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/telehealth_success_bottom_sheet.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/Debouncer.dart';
import 'package:pocketpills/ui/views/telehealth/pp_inputchiplist_new.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';

//import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/signup/telehealth_signup_bottom_sheet.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';

import 'chip_file.dart';

class TelehealthPreference extends StatefulWidget {
  static const routeName = 'telehealth_preference';
  static ChipsInput? chipInput;
  String? from;
  SignUpTransferModel? modelSignUp;
  BaseStepperSource? source;
  UserPatient? userPatient;
  int? bookingId;

  TelehealthPreference({this.from = "telehealth", this.modelSignUp, this.source, this.userPatient, this.bookingId});

  @override
  _TelehealthPreferenceState createState() => _TelehealthPreferenceState();
}

class _TelehealthPreferenceState extends BaseState<TelehealthPreference> {
  InnerAppBar? appBar;
  int? preferencePosition;
  int? categoryPosition;
  bool defaultValue = true;
  String? dropDownPreferenceError;
  String? dropDownCategoryError;
  String? dropDownCategoryValue;
  String? dropDownPreferenceValue;
  String? inputChipError;
  PPInputChipList? medicationList;
  PPInputChipListNew? medicationListNew;
  bool _isKeyboardVisible = false;
  String valueChanged = "";
  String? special;
  String displayErrorMessage = "";
  bool valuePresentArray = false;
  bool autoFocus = false;
  bool saveDetails = false;
  bool popularSearchClicked = false;

  bool eventTime = false;
  bool eventDescription = false;
  bool disableContinueBtn = false;
  ChoiceChips? chipItem;

  List<MedicalConditionSuggestion> medicalConditionSuggestion = [];
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _enterMedicalController = TextEditingController();

  GlobalKey<AutoCompleteTextFieldState<MedicalConditionSuggestion>> key = new GlobalKey();
  late AutoCompleteTextField searchTextField;
  TextEditingController controller = new TextEditingController();
  final FocusNode autoSearchFnode = FocusNode();
  final FocusNode descriptionFnode = FocusNode();

  Key? medicationPreferenceKey;
  List<String>? currentMedications;

  bool autovalidate = false;
  late PreferenceModel preferenceModel;
  bool restrictedMedicalConditionErrMsg = false;
  bool restrictedMedicationErrMsg = false;
  MedicalConditionSuggestion? submittedItem;
  final _debouncer = Debouncer(delay: new Duration(milliseconds: 100));

  medicalConditionControllerListener() {
    if (controller.text.isEmpty) {
      popularSearchClicked = false;
      _descriptionController.text = "";
    }
  }

  descriptionControllerListener() {
    if (_descriptionController.text.isEmpty) {
      restrictedMedicalConditionErrMsg = false;
      disableContinueBtn = false;
    }
  }

  getSearchResponse(String keyword) async {
    medicalConditionSuggestion = (await preferenceModel.searchMedicalConditions(keyword))!;
    if (autoSearchFnode.hasFocus) {
      searchTextField.updateSuggestions(medicalConditionSuggestion);

      MedicalConditionSuggestion? finalCondition;
      keyword = keyword.trim();

      List<String> searchKeyword = keyword.split(' ');
      if (searchKeyword.length > 1) {
        searchKeyword.add(keyword);
      }
      bool res = false;
      for (var items in searchKeyword) {
        List<MedicalConditionSuggestion> searchConditions = (await preferenceModel.searchMedicalConditionsAndMedication(items))!;
        bool response = searchConditions.any((element) {
          if (element.restrictedToPrescribe == true && element.symptom != null && element.symptom!.toLowerCase() == items.toLowerCase()) {
            finalCondition = element;
            res = true;
            return true;
          } else if (element.symptom == null) {
            return false;
          }
          return false;
        });
      }

      if (res == true && finalCondition!.restrictedToPrescribe == true) {
        setState(() {
          restrictedMedicationErrMsg = true;
          disableContinueBtn = true;
        });
      } else {
        setState(() {
          restrictedMedicalConditionErrMsg = false;
          restrictedMedicationErrMsg = false;
          disableContinueBtn = false;
        });
      }
    }
  }

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_preference);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_1);
    //_loadData();
    super.initState();
    medicationPreferenceKey = UniqueKey();
    controller.addListener(medicalConditionControllerListener);
    _descriptionController.addListener(descriptionControllerListener);
    if(widget.from == "telehealth"){
      controller.text = TELEHEALTH_CONDITION;
      TELEHEALTH_CONDITION.isNotEmpty ? popularSearchClicked = true : popularSearchClicked = false;
      _descriptionController.text = TELEHEALTH_COMMENT;
    }
    if (widget.modelSignUp == null) {
      widget.modelSignUp = SignUpTransferModel();
    }
    Map<String, String> map = new Map();
    map["patientId"] = dataStore.getPatientId().toString();
    map["userId"] = dataStore.getUserId().toString();
    widget.from == "telehealth" ? analyticsEvents.sendAnalyticsEvent("au_consultation", map) : analyticsEvents.sendAnalyticsEvent("consultation", map);
    if (kDebugMode && isIntegration) {
      WidgetsBinding.instance!.addPostFrameCallback((_) => controller.text = "Acne");
      print("Im here");
    }
  }

  @override
  void dispose() {
    controller.removeListener(medicalConditionControllerListener);
    _descriptionController.removeListener(descriptionControllerListener);
    _debouncer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("from telehelath ${dataStore.readString(DataStoreService.PROVINCE)}");
    return WillPopScope(
      onWillPop: widget.from == "telehealth"
          ? widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, true);
                  Navigator.canPop(context)
                      ? Navigator.pop(context)
                      : Navigator.pushNamedAndRemoveUntil(context, CustomisedSignUpWidget.routeName, (Route<dynamic> route) => false,
                          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
                  //(Navigator.canPop(context) && saveDetails == false) ? Navigator.pop(context) : _onBackPressed(context);
                  return true;
                }
              : () => _onBackPressed(context)
          : () async {
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
              return true;
            },
      child: ChangeNotifierProvider(
        create: (_) => PreferenceModel(),
        child: Consumer<PreferenceModel>(builder: (BuildContext context, PreferenceModel model, Widget? child) {
          String name = StringUtils.capitalize(PatientUtils.getYouOrNameTitle(Provider.of<DashboardModel>(context, listen: false).selectedPatient));
          name = '${name[0].toUpperCase()}${name.substring(1)}';
          preferenceModel = model;
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  Future.delayed(const Duration(seconds: 1), () {
                    print("from telehelath1 ${dataStore.readString(DataStoreService.PROVINCE)}");
                  });
                  return getMainView(model, name);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else if (model.state == ViewState.Busy) {
                  return LoadingScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(PreferenceModel verificationModel) async {
    Future<Map<String, dynamic>?> future1 = verificationModel.getLocalization(["signup", "consultation", "transfer", "dasboard"]);
    Future<List<TelehealthProvinceItem>?> future2 = verificationModel.fetchPrescriptProvinceList();
    if(widget.from == "telehealth"){
      return await Future.wait([future1]);
    }else {
      return await Future.wait([future1,future2]);
    }

  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  Widget getMainView(PreferenceModel model, String name) {
    return GestureDetector(
      onTap: () {
        final bottomInset = WidgetsBinding.instance!.window.viewInsets.bottom;
        final newValue = bottomInset > 0.0;
        if (newValue != _isKeyboardVisible) {
          setState(() {
            _isKeyboardVisible = newValue;
          });
        }
        if (_isKeyboardVisible) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        } else {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      },
      child: Scaffold(
        key: Key("telehealthPreferenceView"),
        appBar: innerAppbar(widget.from!, name) as PreferredSizeWidget,
        body: Builder(
          builder: (BuildContext context) {
            final list = [
              SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
              Center(
                child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                child: Text.rich(
                  TextSpan(
                    children: widget.source == BaseStepperSource.NEW_USER
                        ? <TextSpan>[
                            TextSpan(
                              text: LocalizationUtils.getSingleValueString("signup", "signup.consultation.title").isEmpty
                                  ? "What can the doctor help you with?\n"
                                  : formatHtmlString(LocalizationUtils.getSingleValueString("signup", "signup.consultation.title")),
                              style:
                                  TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                            ),
                            TextSpan(
                              text: LocalizationUtils.getSingleValueString("signup", "signup.consultation.title").isEmpty
                                  ? "Note : If this is an emergency, please dial 911 for immediate assistance."
                                  : special,
                              style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Bold"),
                            ),
                          ]
                        : <TextSpan>[
                            TextSpan(
                              text: LocalizationUtils.getSingleValueString("signup", "signup.telehealth.description-pharmacist").isEmpty
                                  ? "What can the doctor help you with?\n"
                                  : formatHtmlString(LocalizationUtils.getSingleValueString("signup", "signup.telehealth.description-pharmacist")),
                              style:
                                  TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                            ),
                            TextSpan(
                              text: LocalizationUtils.getSingleValueString("signup", "signup.telehealth.description-pharmacist").isEmpty
                                  ? "Note : If this is an emergency, please dial 911 for immediate assistance."
                                  : special,
                              style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Bold"),
                            ),
                          ],
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              widget.from == "telehealth" ? SizedBox(height: PPUIHelper.VerticalSpaceLarge) : Container(),
              widget.from == "telehealth"
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocalizationUtils.getSingleValueString('signup', 'signup.common.province-label') +
                                    ': ' + getProvinceNameByLanguage(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString()),

                                style: TextStyle(color: brandColor, fontWeight: FontWeight.normal, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            flex: 0,
                            child: TextButton(
                              onPressed: () {
                                if (widget.modelSignUp == null) {
                                  widget.modelSignUp = SignUpTransferModel();
                                }
                                showProvinceBottomSheet(context, widget.modelSignUp!, BaseStepperSource.TELEHEALTH_SCREEN);
                              },
                              child: Text(
                                LocalizationUtils.getSingleValueString("signup", "signup.transfer.edit").isEmpty
                                    ? "Change"
                                    : LocalizationUtils.getSingleValueString("signup", "signup.transfer.edit"),
                                style: TextStyle(color: brandColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Bold"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              SizedBox(height: PPUIHelper.VerticalSpaceLarge),
              widget.from != "telehealth"
                  ? Padding(
                      padding: EdgeInsets.fromLTRB(16, 5, 16, 5),
                      child: PPTexts.getFormLabelAsterisk(
                        LocalizationUtils.getSingleValueString("dashboard", "dashboard.symptomsfield.title").isEmpty
                            ? "Search & choose your symptoms"
                            : LocalizationUtils.getSingleValueString("dashboard", "dashboard.symptomsfield.title"),
                        fontSize: 16.0,
                        color: brandColor,
                        lineHeight: 1.2,
                        isAsteriskEnable: false,
                        fontFamily: "FSJoeyPro Heavy",
                      ),
                    )
                  : Container(),
              getMedicalNeed(),
              dropDownPreferenceError == null
                  ? SizedBox(height: 2)
                  : Padding(
                      padding: EdgeInsets.fromLTRB(4, 0, 0, 0),
                      child: PPTexts.getFormError(dropDownPreferenceError!, color: errorColor),
                    ),
              (popularSearchClicked == false) ? getPopularSearchView(preferenceModel) : Container(),
              getPositionalViewPreference(controller.text, _descriptionController.text)!,
              (restrictedMedicationErrMsg || restrictedMedicalConditionErrMsg) ? SizedBox(height: PPUIHelper.VerticalSpaceXSmall) : Container(),
              restrictedMedicalConditionErrMsg || restrictedMedicationErrMsg
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                      child: restrictedMedicalConditionErrMsg
                          ? Text(
                              displayErrorMessage.isEmpty
                                  ? LocalizationUtils.getSingleValueString("consultation", "consultation.form.medical-conditions-warning")
                                  : displayErrorMessage,
                              style: TextStyle(color: errorColor, fontWeight: FontWeight.normal, fontSize: 14.0, height: 1.2, fontFamily: "FSJoeyPro Medium"),
                              textAlign: TextAlign.start,
                            )
                          : Text(
                              'Our doctor do not prescribe any narcotics or controlled substances like Adderall, Morphine, etc. You can request an appointment for any other conditions.',
                              //LocalizationUtils.getSingleValueString("consultation", "consultation.form.denied-warning"),
                              style: TextStyle(color: errorColor, fontWeight: FontWeight.normal, fontSize: 14.0, height: 1.2, fontFamily: "FSJoeyPro Medium"),
                              textAlign: TextAlign.start,
                            ),
                    )
                  : Container(),
              SizedBox(
                height: PPUIHelper.VerticalSpaceMedium,
              )
            ];
            return SafeArea(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: list,
                  ),
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
            child: model.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : disableContinueBtn
                    ? widget.from == "telehealth"
                        ? Row(
                            children: <Widget>[
                              PrimaryButton(
                                  key: Key("continueSignup"),
                                  isAllCaps: false,
                                  text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-cta").isEmpty
                                      ? "Continue Signup"
                                      : LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-cta"),
                                  onPressed: () {
                                    skipAppointment(model);
                                  }),
                            ],
                          )
                        : Row(
                            children: <Widget>[
                              PrimaryButton(
                                  isAllCaps: false,
                                  key: Key("continueDashboard"),
                                  text: LocalizationUtils.getSingleValueString("dashboard", "dashboard.appointment.no-slot-cta").isEmpty
                                      ? "Continue to Dashboard"
                                      : LocalizationUtils.getSingleValueString("dashboard", "dashboard.appointment.no-slot-cta"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                            ],
                          )
                    : Row(
                        children: <Widget>[
                          PrimaryButton(
                            key: Key("continueButton"),
                            text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                            onPressed: (disableContinueBtn || preferenceModel.chipRestrictedMedicaiton)
                                ? () {}
                                : () async {
                                    if (medicationListNew != null) {
                                      if (TelehealthPreference.chipInput != null && TelehealthPreference.chipInput!.state.effectiveFocuseNode.hasFocus) {
                                        await TelehealthPreference.chipInput!.state.removeFocus();
                                      }
                                    }
                                    onSaveDetails(context, model);
                                  },
                          ),
                        ],
                      ),
          ),
        ),
      ),
    );
  }

  Widget? getPositionalViewPreference(String value, String description) {
    //renewMedicalConditions = renewMedicalConditions.map((values) => values.toLowerCase()).toList();
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      if (value == "")
        return Container();
      /*else if (renewMedicalConditions.contains(value.toLowerCase())) {
        return commonView();
      }*/
      else if (restrictedMedicalConditionErrMsg && description.isEmpty)
        return Container();
      else
        return descriptionView();
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr) {
      if (value == "")
        return Container();
      /*else if (renewMedicalConditions.contains(value.toLowerCase())) {
        return commonView();
      }*/
      else if (restrictedMedicalConditionErrMsg && description.isEmpty)
        return Container();
      else
        return descriptionView();
    }
  }

  Widget getMedicalNeed() {
    print("AutoCompleteText");
    print(key);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Stack(
        children: [
          Column(children: <Widget>[
            Column(children: <Widget>[
              Container(
                key: Key("autoText"),
                child: searchTextField = AutoCompleteTextField<MedicalConditionSuggestion>(
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Bold"),
                  keyboardType: TextInputType.visiblePassword,
                  controller: controller,
                  suggestions: medicalConditionSuggestion,
                  submitOnSuggestionTap: true,
                  focusNode: autoSearchFnode,
                  key: key,
                  itemSorter: (a, b) => 0,
                  itemFilter: (suggestion, input) => true,
                  itemBuilder: (context, item) {
                    return Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                              child: getAutoCompleteItem(item),
                            ),
                            key: Key(item.symptom!),
                          ),
                        ),
                      ],
                    );
                  },
                  textChanged: (value) {
                    if (eventTime == false) {
                      eventTime = true;
                      analyticsEvents.sendAnalyticsEvent("consultation_condition_search");
                    }

                    setState(() {
                      valueChanged = value;
                      dropDownPreferenceError = null;
                      restrictedMedicationErrMsg = false;
                      restrictedMedicalConditionErrMsg = false;
                      if (value.isEmpty) {
                        popularSearchClicked = false;
                        _descriptionController.text = "";
                      }
                    });

                    _debouncer.debounce(() {
                      if (value.isNotEmpty) {
                        getSearchResponse(controller.text);
                      }
                    });
                  },
                  itemSubmitted: (MedicalConditionSuggestion item) async {
                    Chips.chips = [];
                    analyticsEvents.sendAnalyticsEvent("consultation_condition_select");
                    String medicalConditionSel = await preferenceModel.setValue(item.symptom!);
                    medicalConditionSel = medicalConditionSel.trim();

                    if (item.restrictedToPrescribe == true) {
                      popularSearchClicked = false;
                    } else {
                      popularSearchClicked = true;
                    }

                    /*bool res = (await preferenceModel.searchMedicalConditions(medicalConditionSel))!.any((element) {
                      if (element.restrictedToPrescribe == true && element.symptom!.toLowerCase() == medicalConditionSel.toLowerCase()) {
                        submittedItem = element;
                        popularSearchClicked = false;
                        return true;
                      } else if (element.restrictedToPrescribe == false && element.symptom!.toLowerCase() == medicalConditionSel.toLowerCase()) {
                        submittedItem = element;
                        popularSearchClicked = true;
                        return true;
                      }
                      return false;
                    });*/

                    submittedItem = item;
                    setState(() {
                      searchTextField.controller!.text = medicalConditionSel;
                      if (submittedItem!.restrictedToPrescribe == true) {
                        restrictedMedicalConditionErrMsg = true;
                        disableContinueBtn = true;
                      } else {
                        restrictedMedicationErrMsg = false;
                        restrictedMedicalConditionErrMsg = false;
                        disableContinueBtn = false;
                      }
                    });
                  },
                  decoration: PPInputDecor.getDecoration(
                    labelText: LocalizationUtils.getSingleValueString("signup", "signup.condition.search"),
                    hintText: LocalizationUtils.getSingleValueString("signup", "signup.condition.search"),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                  ),
                ),
              ),
            ]),
          ]),
          if (valueChanged != "")
            Positioned(
                right: 10,
                top: 10,
                child: InkWell(
                    onTap: () {
                      setState(() {
                        searchTextField.clear();
                        valueChanged = "";
                        restrictedMedicalConditionErrMsg = false;
                        restrictedMedicationErrMsg = false;
                        disableContinueBtn = false;
                        FocusScope.of(context).requestFocus(FocusNode());
                      });
                    },
                    child: Icon(Icons.close))),
        ],
      ),
    );
  }

  /* Widget getPositionalViewCategory(int position) {
    if (position == null)
      return Container();
    else if (position == getcategoryMap().length - 1)
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          medicalConditionView(),
          commonView(),
        ],
      );
    else
      return commonView();
  }*/

  Widget getAutoCompleteItem(MedicalConditionSuggestion item) {
    return Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 4), child: PPTexts.getTertiaryHeading(item.symptom!, isBold: true));
  }

  Widget medicalConditionView() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          RichText(
            text: TextSpan(
              text: LocalizationUtils.getSingleValueString("consultation", "consultation.form.symptoms"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                color: blackColor,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: '*',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                      color: Colors.red,
                    )),
              ],
            ),
          ),
          PPUIHelper.verticalSpaceSmall(),
          PPFormFields.getTextField(
            autovalidate: autovalidate,
            controller: _enterMedicalController,
            decoration: PPInputDecor.getDecoration(),
          ),
        ],
      ),
    );
  }


  Widget descriptionView() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          Text(
            LocalizationUtils.getSingleValueString("consultation", "consultation.description.title"),
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0, color: blackColor, fontFamily: "FSJoeyPro Bold"),
          ),
          PPUIHelper.verticalSpaceSmall(),
          PPFormFields.getMultiLineTextField(
              autovalidate: false,
              controller: _descriptionController,
              focusNode: descriptionFnode,
              decoration: PPInputDecor.getDecoration(),
              onTextChanged: (value) {
                print(value);
                if (eventDescription == false) {
                  eventDescription = true;
                  widget.from == "telehealth"
                      ? analyticsEvents.sendAnalyticsEvent("au_description_entered")
                      : analyticsEvents.sendAnalyticsEvent("description_entered");
                }
                setState(() {
                  restrictedMedicalConditionErrMsg = false;
                  disableContinueBtn = false;
                });
              }),
          Text(
            LocalizationUtils.getSingleValueString("consultation", "consultation.medicationfield.notes"),
            style: TextStyle(fontSize: 12.0, color: thinGrey, height: 1.2, fontFamily: "FSJoeyPro"),
          ),
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
        ],
      ),
    );
  }

  Widget getChipAddView() {
    medicationList = PPInputChipList(
      key: medicationPreferenceKey,
      chips: currentMedications == null ? [] : currentMedications,
      totalList: [],
      labelText: "(e.g. Lipitor, etc)",
    );
    return medicationList!;
  }

  Widget getChipAddViewNew(PreferenceModel model) {
    medicationListNew = PPInputChipListNew(
      key: medicationPreferenceKey!,
      chips: currentMedications == null ? [] : currentMedications,
      totalList: [],
      labelText: "(e.g. Lipitor, etc)",
      model: model,
    );
    return medicationListNew!;
  }

  void onSaveDetails(BuildContext context, PreferenceModel model) async {
    if (controller.text.isEmpty) {
      if (mounted)
        setState(() {
          dropDownPreferenceError = LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
          autovalidate = false;
        });
    } else {
      MedicalConditionValidate? conditionValidate = await model.validateMedicalConditionsAndComment(controller.text, _descriptionController.text);
      if (conditionValidate == null) {
        model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      } else {
        if (conditionValidate.restricted!) {
          setState(() {
            restrictedMedicalConditionErrMsg = true;
            disableContinueBtn = true;
            displayErrorMessage = conditionValidate.message ?? "";
          });
        } else {
          displayErrorMessage = "";
          if (widget.from == "telehealth") {
            bool connectivityResult = await model.isInternetConnected();
            if (connectivityResult == false) {
              onFail(context, errMessage: model.noInternetConnection);
              return;
            }
            saveDetails = true;
            model.prescriptionMedicalCondition = controller.text;
            model.prescriptionComment = _descriptionController.text.isEmpty ? null : _descriptionController.text;
            model.telehealthRequestedMedications = Chips.chips == null ? null : Chips.chips.join(",");


            if (dataStore.readBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED) == false) {
              TelehealthSignUpPreferenceRequest telehealthRequest = TelehealthSignUpPreferenceRequest(
                id: PPApplication.prescriptionID != -1 ? PPApplication.prescriptionID : null,
                type: "TELEHEALTH",
                appointmentTime: null,
                prescriptionFilledByExternalPharmacy: false,
                prescriptionState: "FILED",
                isPrescriptionEdited: PPApplication.prescriptionID != -1 ? true : false,
                doctorId: null,
                teleHealthDoctorDayPlanId: null,
                telehealthAppointmentSlotId: null,
                prescriptionMedicalConditions: controller.text,
                prescriptionComment: _descriptionController.text.isEmpty ? "" : _descriptionController.text,
              );

              bool success = await model.sendPreferencesSignUp(telehealthRequest);
              if (success) {

                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_allowdoctor_entered);
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_consultation_submit);
                Map<String, String> map = new Map();
                map["patientId"] = dataStore.getPatientId().toString();
                map["userId"] = dataStore.getUserId().toString();
                ////////

                Map<String, dynamic> leadMap = new Map();
                leadMap['lead_created_by'] = 'mobileApp';
                leadMap['lead_new'] = model.totalPrescriptionsCount == null ? 1 : model.totalPrescriptionsCount! > 1 ? 0 : 1;
                leadMap['lead_type'] = 'telehealth';
                leadMap['lead_origin'] = widget.source != null && widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
                leadMap['telehealth_condition_selected'] = controller.text;

                if (model.revenue) {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
                }
                ///////
                analyticsEvents.sendAnalyticsEvent("consultation_verification", map);

                PPApplication.prescriptionID = -1;
                TELEHEALTH_CONDITION = "";
                TELEHEALTH_COMMENT = "";
                dataStore.writeBoolean(DataStoreService.SIGNUP_ADD_OTHERS, false);
                dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
                await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
                Navigator.pushNamedAndRemoveUntil(context, HealthCardUploadViewSignUp.routeName, (Route<dynamic> route) => false,
                        arguments: TelehealthArguments(
                            modelSignUp: widget.modelSignUp,
                            source: widget.source,
                            userPatient: widget.userPatient,
                            appointmentDateTime: dataStore.readString(DataStoreService.TELEHEALTH_APPOINTMENT_TIME).toString()))
                    .then((value) => model.currentItem = 0);
              } else {
                analyticsEvents.sendAnalyticsEvent("consultation_verification_failed");
                model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
              }
            } else {
              TelehealthSignUpPreferenceRequest telehealthRequest = TelehealthSignUpPreferenceRequest(
                id: PPApplication.prescriptionID != -1 ? PPApplication.prescriptionID : null,
                type: "TELEHEALTH",
                prescriptionFilledByExternalPharmacy: false,
                prescriptionState: "INCOMPLETE",
                isPrescriptionEdited: PPApplication.prescriptionID != -1 ? true : false,
                prescriptionMedicalConditions: controller.text,
                prescriptionComment: _descriptionController.text.isEmpty ? "" : _descriptionController.text,
              );

              bool success = await model.sendPreferencesSignUp(telehealthRequest);

              if (success) {
                print("AppointmentID + ${PPApplication.prescriptionID}");
                TELEHEALTH_CONDITION = controller.text;
                TELEHEALTH_COMMENT = _descriptionController.text;
                Navigator.pushNamed(context, AppointmentDateWidget.routeName,
                        arguments: TelehealthArguments(
                            from: 'telehealth',
                            modelSignUp: widget.modelSignUp,
                            source: widget.source,
                            userPatient: widget.userPatient,
                            prescriptionMedicalConditions: controller.text,
                            prescriptionComment: _descriptionController.text.isEmpty ? null : _descriptionController.text,
                            defaultValue: defaultValue))
                    .then((value) => {
                          setState(() {
                            dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE);
                          }),
                        });
                ;
              } else {
                Fluttertoast.showToast(msg: model.errorMessage, toastLength: Toast.LENGTH_LONG);
              }
            }
          } else {
            TelehealthRequest telehealthRequest = TelehealthRequest(
                prescriptionType: "TELEHEALTH",
                prescriptionFilledByExternalPharmacy: false,
                prescriptionState: "FILED",
                isPrescriptionEdited: true,
                prescriptionMedicalConditions: controller.text,
                prescriptionComment: _descriptionController.text.isEmpty ? "" : _descriptionController.text,
                appointmentTime: null,
                doctorId: null,
                telehealthAppointmentSlotId: null,
                teleHealthDoctorDayPlanId: null);
            if (dataStore.readBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED) == false) {
              bool success = await model.sendPreferences(telehealthRequest);
              if (success) {
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_allowdoctor_entered);
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_consultation_submit);
                Map<String, String> map = new Map();
                map["patientId"] = dataStore.getPatientId().toString();
                map["userId"] = dataStore.getUserId().toString();
                ////////

                Map<String, dynamic> leadMap = new Map();
                leadMap['lead_created_by'] = 'mobileApp';
                leadMap['lead_new'] = model.totalPrescriptionsCount! > 1 ? 0 : 1;
                leadMap['lead_type'] = 'telehealth';
                leadMap['lead_origin'] = widget.source != null && widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
                leadMap['telehealth_condition_selected'] = controller.text;

                if (model.revenue) {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
                }
                ///////
                analyticsEvents.sendAnalyticsEvent("consultation_verification", map);

                int stepperValue = dataStore.readInteger(DataStoreService.STEPPER_CURRENT_STEP) ?? 0;

                Chips.chips = [];
                if (stepperValue > 0) {
                  showBottomSheet(context, "APPOINTMENT_DASHBOARD");
                  dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD, true);
                  Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
                  Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false).then((value) => model.currentItem = 0);
                } else {
                  dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD, true);
                  Navigator.pushReplacementNamed(context, OrderStepper.routeName,
                          arguments: BaseStepperArguments(source: BaseStepperSource.TELEHEALTH_SCREEN, startStep: 0, from: widget.from))
                      .then((value) => model.currentItem = 0);
                }
              } else {
                analyticsEvents.sendAnalyticsEvent("consultation_verification_failed");
                model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
              }
            } else {
              saveDetails = true;
              Navigator.pushNamed(context, AppointmentDateWidget.routeName,
                  arguments: TelehealthArguments(
                      from: 'dashboard',
                      modelSignUp: widget.modelSignUp,
                      source: widget.source,
                      userPatient: widget.userPatient,
                      prescriptionMedicalConditions: controller.text,
                      telehealthRequestedMedications: Chips.chips == null ? null : Chips.chips.join(","),
                      prescriptionComment: _descriptionController.text.isEmpty ? null : _descriptionController.text,
                      defaultValue: defaultValue));
            }
          }
        }
      }
    }
  }

  showBottomSheet(BuildContext context, String s) {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          //return WillPopScope(onWillPop: (context), child: SuccessSheet(context, s));
          return WillPopScope(
              onWillPop: () async {
                return false;
              },
              child: SuccessSheet(context, s));
        });
  }

  showProvinceBottomSheet(BuildContext context, SignUpTransferModel model, BaseStepperSource source) async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return DraggableScrollableSheet(
            initialChildSize: 0.75,
            minChildSize: 0.75,
            maxChildSize: 0.75,
            expand: false,
            builder: (_, controller) => SelectProvinsSheet(context: context, model: model, source: source, userPatient: widget.userPatient),
          );
        }).then((value) => {
          setState(() {
            dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE);
          }),
        });
  }

  Widget innerAppbar(String from, String name) {
    if (from == "telehealth") {
      return AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
        leading: IconButton(
          key: Key("back"),
          icon: Icon(Icons.arrow_back_rounded),
          color: brandColor,
          onPressed: () {
            Navigator.canPop(context)
                ? Navigator.pop(context)
                : Navigator.pushNamedAndRemoveUntil(context, CustomisedSignUpWidget.routeName, (Route<dynamic> route) => false,
                    arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
          },
        ),
      );
    } else {
      return InnerAppBar(
          titleText: name,
          appBar: AppBar(),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          });
    }
  }

  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model, String openFrom) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return SkipSignupSheet(context: context, model: model, source: widget.source!, userPatient: widget.userPatient!, from: openFrom);
        });
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    Chips.chips = [];
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context).pushNamedAndRemoveUntil(SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  String formatHtmlString(String string) {
    String formatted = string
        .replaceAll("<br>", "\n") // Line Breaks
        .trim(); // Whitespace
    int firstIndex = formatted.indexOf("?");
    special = "\n" + formatted.substring(firstIndex + 1).replaceAll("\n", "");
    return formatted.substring(0, firstIndex + 1);
  }

  Widget getPopularSearchView(PreferenceModel model) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: MEDIUM_XX,
          ),
          Text(
            LocalizationUtils.getSingleValueString("transfer", "transfer.search.popular").toUpperCase(),
            style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
          ),
          SizedBox(
            height: SMALL,
          ),
          getPopularSearch(model),
        ],
      ),
    );
  }

  Widget getPopularSearch(PreferenceModel model) {
    chipItem = ChoiceChips(
        chipName: model.popularSearches,
        fontSize: 12,
        selectedItem: (selected) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.Transfer_search_popular_selected);
          setState(() {
            restrictedMedicalConditionErrMsg = false;
            dropDownPreferenceError = null;
            disableContinueBtn = false;
            searchTextField.controller!.text = selected;
            valueChanged = selected;
            popularSearchClicked = true;
          });
        });
    return chipItem!;
  }

  Future<void> skipAppointment(PreferenceModel model) async {
    //dropValue = null;
    TelehealthSignUpPreferenceRequest telehealthRequest = TelehealthSignUpPreferenceRequest(
      type: "TELEHEALTH",
      prescriptionState: "PENDING",
    );
    bool connectivityResult = await model.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }

    bool success = await model.signupOthersFlow("NONE","REGULAR");
    if (success) {
      dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else {
      model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }

    /*bool success = await model.sendPreferencesSignUp(telehealthRequest);
    if (success) {
      saveDetails = true;
      model.prescriptionMedicalCondition = controller.text;
      model.prescriptionComment = _descriptionController.text.isEmpty ? null : _descriptionController.text;
      Navigator.pushNamed(context, AppointmentDateWidget.routeName,
              arguments: TelehealthArguments(
                  from: 'telehealth',
                  modelSignUp: widget.modelSignUp,
                  source: BaseStepperSource.NEW_USER,
                  userPatient: widget.userPatient,
                  prescriptionMedicalConditions: "",
                  prescriptionComment: "",
                  defaultValue: defaultValue))
          .then((value) => {
                setState(() {
                  dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE);
                }),
              });
    } else {
      model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }*/
  }
}
