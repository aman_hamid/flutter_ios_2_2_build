import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/chips/chips_input_local.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:provider/provider.dart';
import 'dart:developer' as developer;

import 'chip_file.dart';

class PPInputChipListNew extends StatefulWidget {
  final List<String>? chips;
  final List<String>? totalList;
  final Function? suggestions;
  final String? labelText;
  PreferenceModel? model;

  PPInputChipListNew({Key? key, this.chips, this.totalList, this.suggestions, this.labelText, this.model}) : super(key: key);
  final PPInputChipListNewState state = PPInputChipListNewState();

  List<String> getCurrentSelection() {
    return state.currentList;
  }

  @override
  PPInputChipListNewState createState() {
    return state;
  }
}

class PPInputChipListNewState extends State<PPInputChipListNew> {
  late List<String> currentList;
  List<String> restrictedList = [];

  ChipsInput? chipsInputLocal;
  Key key = UniqueKey();
  GlobalKey<ChipsInputState> _chipKey = GlobalKey();
  bool res = false;

  @override
  void initState() {
    super.initState();
    currentList = widget.chips!;
  }

  @override
  Widget build(BuildContext context) {
    if (chipsInputLocal == null) {
      chipsInputLocal = ChipsInput<String>(
        actionLabel: null,
        key: key,
        width: MediaQuery.of(context).size.width,
        separator: ',',
        decoration: PPInputDecor.getDecoration(collapsed: true, labelText: "(e.g. Lipitor, etc)"),
        initialTags: [],
        autofocus: false,
        inputAction: TextInputAction.done,
//        onChanged: (List<String> data) {
//          currentList = data;
//          Chips.chips = data;
//        },
        chipTextValidator: (String value) {
          value.contains('!');
          return -1;
        },
        chipBuilder: (context, state, chipText) {
          currentList.add(chipText);
          return FutureBuilder(
              future: checkRestrictedMedication(chipText, widget.model!),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  if (restrictedList.isNotEmpty) {
                    widget.model!.chipRestrictedMedicaiton = true;
                  }
                  return getChips(snapshot.data, chipText, state);
                } else {
                  return Container();
                }
              });
        },
      );
      TelehealthPreference.chipInput = chipsInputLocal;
    }
    if (TelehealthPreference.chipInput == null) {
      TelehealthPreference.chipInput = chipsInputLocal;
    }
    return chipsInputLocal!;
  }

  Widget getChips(bool val, String chipText, dynamic state) {
    return InputChip(
      //key: ObjectKey(chipText),
      deleteIconColor: secondaryColor,
      backgroundColor: !val ? headerBgColor : errorColor,
      autofocus: true,
      label: Text(
        chipText,
        style: TextStyle(fontFamily: "FSJoeyPro Bold"),
      ),
      labelStyle: TextStyle(color: primaryColor, fontSize: PPUIHelper.FontSizeSmall, height: 1.4, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
      onDeleted: () {
        state.deleteChip(chipText);
        if (restrictedList.contains(chipText.trim())) {
          restrictedList.remove(chipText.trim());
          if (restrictedList.isEmpty) {
            widget.model!.chipRestrictedMedicaiton = false;
          }
        }
      },
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );
  }

  Future<bool> checkRestrictedMedication(String text, PreferenceModel model) async {
    text = text.trim();
    List<String> medications = text.split(' ');
    bool flag = false;
    for (var items in medications) {
      List<MedicalConditionSuggestion> medication = (await model.searchMedicalConditionsAndMedication(items))!;
      bool res = medication.any((element) {
        if (element.restrictedToPrescribe == true && element.symptom != null && element.symptom!.toLowerCase().contains(items.toLowerCase())) {
          if (restrictedList.contains(text) == false) {
            restrictedList.add(text);
          }
          flag = true;
          return true;
        } else if (element.symptom == null) {
          return false;
        }
        return false;
      });
    }
    return flag ? true : false;
  }
}
