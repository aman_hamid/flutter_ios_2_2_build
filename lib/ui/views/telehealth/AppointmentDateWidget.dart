import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'package:pocketpills/core/enums/telehealthAppointmentDetails.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_doctor_time_slots.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/appointment_time_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_restricted_condition_request.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/telehealth_success_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/utils/Triple.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import '../../../locator.dart';
import '../base_state.dart';
import '../error_screen.dart';

class AppointmentDateWidget extends StatefulWidget {
  static const routeName = 'appointmentDateList';
  String? from;
  SignUpTransferModel? modelSignUp;
  BaseStepperSource? source;
  UserPatient? userPatient;
  String? prescriptionMedicalConditions;
  String? prescriptionComment;
  bool defaultValue = true;

  AppointmentDateWidget(
      {this.from = "telehealth",
      this.modelSignUp,
      this.source,
      this.userPatient,
      this.prescriptionMedicalConditions,
      this.prescriptionComment,
      this.defaultValue = true});

  @override
  _AppointmentDateWidgetState createState() => _AppointmentDateWidgetState();
}

class _AppointmentDateWidgetState extends BaseState<AppointmentDateWidget> with WidgetsBindingObserver {
  late ScrollController _scrollController;

  //List<bool> selectedIndexes = [];

  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();
  bool eventTime = false;
  bool itemSelected = false;
  AppointmentTime? selectedItem;
  Map<String, AppointmentTime>? appointmentMap;
  String? dropValue;
  String? timeDropValue;

  //New Logic
  bool showGrid = false;
  bool selected = false;
  bool autoSelected = false;
  int selectedIndex = -1;
  int dateIndex = 0;
  List<String> availableTimings = [];

  //List<String> availableTimingsInMilis = [];
  AppointmentDoctorSlots? _appointmentDoctorSlots;

  @override
  void initState() {
    super.initState();
    if (widget.modelSignUp == null) {
      widget.modelSignUp = SignUpTransferModel();
    }
    _scrollController = new ScrollController(
      // NEW
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.from == "telehealth"
          ? widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  Navigator.canPop(context) ? Navigator.pop(context) :
                  Navigator.pushNamedAndRemoveUntil(context,TelehealthPreference.routeName,(Route<dynamic> route) => false,
                      arguments: TelehealthArguments(
                        modelSignUp: widget.modelSignUp,
                        source: BaseStepperSource.NEW_USER,
                        userPatient: widget.userPatient,
                        from: "telehealth",
                      ));
                  return true;
                }
              : () => _onBackPressed(context)
          : () async {
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
              return true;
            },
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<PreferenceModel>(create: (context) => PreferenceModel()),
          ChangeNotifierProvider<SignUpTransferModel>(create: (context) => SignUpTransferModel())
        ],
        child: Consumer2<PreferenceModel, SignUpTransferModel>(
            builder: (BuildContext context, PreferenceModel model, SignUpTransferModel transferModel, Widget? child) {
          String name = StringUtils.capitalize(PatientUtils.getYouOrNameTitle(Provider.of<DashboardModel>(context, listen: false).selectedPatient));
          name = '${name[0].toUpperCase()}${name.substring(1)}';
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                Map<String, dynamic> map = new Map();
                if (snapshot.hasData != null && snapshot.data != null) {
                  if (eventTime == false) {
                    eventTime = true;
                    if (model.appointmentsList.isNotEmpty) {
                      String firstSlot = model.appointmentsListWithoutFilter[0].doctorTimeSlots![0].actualApptTime!;
                      DateTime docDateTime = DateTime.parse(firstSlot);
                      var value = docDateTime.millisecondsSinceEpoch;
                      List<dynamic> slotsInfoList = [];
                      List<dynamic> daysDifferList = [];
                      Map<String, dynamic> timeSlotsCategories = {"0_24_hours": 0, "24_48_hours": 0, "48_72_hours": 0, "72_plus_hours": 0};
                      model.appointmentsList.forEach((element) {
                        var allTimeSlots = element.doctorTimeSlots ?? [];
                        String currentDateString = getCurrentDateString();
                        allTimeSlots.forEach((timeSlot) {
                          String timeSlotString = timeSlot.actualApptTime!;
                          int timeDifference = getHourDifference(currentDateString, timeSlotString);
                          var valuesCount = timeSlotsCategories.values.toList();
                          if (timeDifference < 24) {
                            int currentValue = valuesCount[0];
                            timeSlotsCategories["0_24_hours"] = currentValue + 1;
                          } else if (timeDifference >= 24 && timeDifference < 48) {
                            int currentValue = valuesCount[1];
                            timeSlotsCategories["24_48_hours"] = currentValue + 1;
                          } else if (timeDifference >= 48 && timeDifference < 72) {
                            int currentValue = valuesCount[2];
                            timeSlotsCategories["48_72_hours"] = currentValue + 1;
                          } else if (timeDifference >= 72) {
                            int currentValue = valuesCount[3];
                            timeSlotsCategories["72_plus_hours"] = currentValue + 1;
                          }
                          // addTimeDifferenceCategory(timeDifference);
                        });
                      });

                      model.appointmentsListWithoutFilter.forEach((element) {
                        slotsInfoList.add(element.available!);
                        daysDifferList.add(element.daysFromCurrentDate);
                      });
                      map["firstSlot"] = value;
                      map["slotsInfoList"] = jsonEncode(slotsInfoList);
                      map["daysDifferList"] = jsonEncode(daysDifferList);
                      map["timeSlotCategories"] = timeSlotsCategories;
                    }

                    widget.from == "telehealth"
                        ? analyticsEvents.sendAnalyticsEvent("au_appointment", map)
                        : analyticsEvents.sendAnalyticsEvent("appointment", map);
                  }
                  return appointmentTimeLoad(context, model, transferModel, name); //snapshot.data[1],
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(PreferenceModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "consultation", "common", "landing"]);
    Future<List<AppointmentTime>?> future2 = model.getAppointmentDate();
    return await Future.wait([future1, future2]);
  }

  _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  Widget appointmentTimeLoad(BuildContext context, PreferenceModel model, SignUpTransferModel transferModel, String name) {
    return BaseScaffold(
        key: Key("dateSelectionView"),
        appBar: innerAppbar(widget.from, name),
        bottomNavigationBar: getPrimaryButton(model),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
              Center(
                child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
              Padding(
                padding: EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                child: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.appointment.book").isEmpty
                      ? "Let’s pick a date that works for you"
                      : LocalizationUtils.getSingleValueString("signup", "signup.appointment.book"),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeXLarge, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                ),
              ),
              SizedBox(
                height: PPUIHelper.VerticalSpaceXMedium,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
                child: widget.from == 'telehealth'
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                LocalizationUtils.getSingleValueString('signup', 'signup.common.province-label') +
                                    ': ' +
                                    getProvinceNameByLanguage(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString()),
                                style: TextStyle(color: brandColor, fontWeight: FontWeight.normal, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: TextButton(
                              onPressed: () {
                                model.clearMemoizer();
                                showProvinceBottomSheet(context, transferModel, BaseStepperSource.APPOINTMENT_DATE_SCREEN, model);
                              },
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  LocalizationUtils.getSingleValueString("signup", "signup.transfer.edit").isEmpty
                                      ? "Change"
                                      : LocalizationUtils.getSingleValueString("signup", "signup.transfer.edit"),
                                  style: TextStyle(color: brandColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Bold"),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ),
              getDateTimeDisplay(context, model),
            ],
          ),
        ));
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget innerAppbar(String? from, String name) {
    if (from != null && from == "telehealth") {
      if (widget.source == BaseStepperSource.NEW_USER) {
        return AppBar(
          backgroundColor: Colors.white,
          title: isProvinceQuebec() == true
              ? Image.network(
                  "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                  width: MediaQuery.of(context).size.width * 0.45,
                  fit: BoxFit.cover,
                )
              : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
          centerTitle: true,
          leading: IconButton(
            key: Key("back"),
            icon: Icon(Icons.arrow_back_rounded),
            color: brandColor,
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(context,TelehealthPreference.routeName,(Route<dynamic> route) => false,
                  arguments: TelehealthArguments(
                    modelSignUp: widget.modelSignUp,
                    source: BaseStepperSource.NEW_USER,
                    userPatient: widget.userPatient,
                    from: "telehealth",
                  ));
            },
          ),
        );
      } else {
        return AppBar(
          backgroundColor: Colors.white,
          title: isProvinceQuebec() == true
              ? Image.network(
                  "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                  width: MediaQuery.of(context).size.width * 0.45,
                  fit: BoxFit.cover,
                )
              : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
          centerTitle: true,
        );
      }
    } else {
      return InnerAppBar(
          titleText: name,
          appBar: AppBar(),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          });
    }
  }

  Widget getPrimaryButton(PreferenceModel model) {
    return Builder(
      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBarAnother(
        height: selected ? 135 : 80,
        child: Column(
          children: [
            Container(
              color: quartiaryColor,
              child: doctorView(),
            ),
            selected
                ? SizedBox(
                    height: 4,
                  )
                : SizedBox(
                    height: 8,
                  ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: model.state == ViewState.Busy
                  ? ViewConstants.progressIndicator
                  : widget.from == "telehealth" && model.appointmentsList.length == 0
                      ? Row(
                          children: [
                            PrimaryButton(
                              borderRadius: 30,
                              text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-cta").isEmpty
                                  ? "Complete Simple Signup"
                                  : LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-cta"),
                              onPressed: () {
                                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_schedule_appointment_skipped);
                                skipAppointment(model);
                              },
                            ),
                          ],
                        )
                      : widget.from != "telehealth" && model.appointmentsList.length == 0
                          ? Row(
                              children: [
                                PrimaryButton(
                                    borderRadius: 30,
                                    text: LocalizationUtils.getSingleValueString("landing", "landing.banner.dashboard-button").isEmpty
                                        ? "Go to Dashboard"
                                        : LocalizationUtils.getSingleValueString("landing", "landing.banner.dashboard-button"),
                                    onPressed: () {
                                      Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                                    }),
                              ],
                            )
                          : Row(
                              children: <Widget>[
                                PrimaryButton(
                                  key: Key("dateSubmit"),
                                  borderRadius: 30,
                                  text: LocalizationUtils.getSingleValueString("common", "common.button.confirm"),
                                  onPressed: () {
                                    if (selectedItem != null) {
                                      Map<String, String> map = new Map();
                                      map["daysDiffer"] = selectedItem!.appointmentDate.toString();
                                      map["slotSelected"] = selectedItem!.index.toString();
                                      widget.from == "telehealth"
                                          ? analyticsEvents.sendAnalyticsEvent("au_appointment_submit", map)
                                          : analyticsEvents.sendAnalyticsEvent("appointment_submit", map);
                                      AppointmentDetails.AppointmentSelectedItem = selectedItem;
                                      dataStore.writeString(DataStoreService.DOCTOR_NAME, _appointmentDoctorSlots!.doctorName ?? "");
                                      dataStore.writeString(DataStoreService.APPOINTMENT_TIME, _appointmentDoctorSlots!.startTime ?? "");
                                      dataStore.writeString(DataStoreService.APPOINTMENT_DATE, selectedItem!.displayDate!);
                                      if (widget.from == "telehealth") {
                                        getTimeStatus(context, model);
                                      } else {
                                        dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
                                        if (selectedIndex == -1) {
                                          Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("signup", "signup.appointment.required"));
                                        } else {
                                          bookAppointment(model);
                                        }
                                      }
                                    } else if (selectedItem == null) {
                                      Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("signup", "signup.appointment.required"));
                                    }
                                  },
                                ),
                              ],
                            ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getAvailableDaysDropDown(BuildContext context, PreferenceModel model) {
    if (dropValue == null && autoSelected == false) selectDateAuto(model);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
          child: RichText(
            text: TextSpan(children: [
              TextSpan(
                text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label").isEmpty
                    ? "Available dates "
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.date-label") + " ",
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
              ),
              TextSpan(
                text: '*',
                style: TextStyle(color: errorColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
              ),
            ]),
          ),
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
              height: 90.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: model.appointmentsList.length,
                itemBuilder: (BuildContext context, int i) => InkWell(
                  onTap: () {
                    setState(() {
                      selectedIndex = -1;
                      dateIndex = i;
                      model.selectedDate = model.appointmentsList[i].displayDate;
                      selectedItem = model.time[model.appointmentsList[i].displayDate];
                      model.currentItem = model.appointListToActualIndex[selectedItem!.index];
                      showGrid = true;
                      selected = false;
                      timeDropValue = null;
                      dropValue = model.appointmentsList[i].displayDate;
                      if (availableTimings != null) {
                        availableTimings.clear();
                      }
                      if (selectedItem!.doctorTimeSlots != null) {
                        for (int j = 0; j < selectedItem!.doctorTimeSlots!.length; j++) {
                          var dataItem = selectedItem!.doctorTimeSlots![j];
                          availableTimings.add(dataItem.startTime.toString());
                        }
                      }
                    });
                  },
                  child: Card(
                    elevation: 2,
                    color: dateIndex == i ? brandColor : Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: lightPurple, width: 2),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: 90,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item3,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 12, fontFamily: "FSJoeyPro", fontWeight: FontWeight.normal),
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item2,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 21.6, fontFamily: "FSJoeyPro Heavy", fontWeight: FontWeight.bold),
                          ),
                          Text(
                            getDisplayDate(model.appointmentsList[i].displayDate!).item1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: dateIndex == i ? Colors.white : brandColor, fontSize: 14.6, fontFamily: "FSJoeyPro", fontWeight: FontWeight.normal),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
        ),
      ],
    );
  }

  Widget getAvailableTimesDropDown(BuildContext context, PreferenceModel model) {
    if (timeDropValue == null) selectTimeAuto(model);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 3),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label").isEmpty
                    ? 'Available time slots'
                    : LocalizationUtils.getSingleValueString("signup", "signup.appointment.time-slots-label"),
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                children: [
                  TextSpan(
                    text: ' *',
                    style: TextStyle(color: errorColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            height: 45.0,
            child: ListView.builder(
              controller: _scrollController,
              scrollDirection: Axis.horizontal,
              itemCount: availableTimings.length,
              itemBuilder: (BuildContext context, int i) => InkWell(
                onTap: () {
                  setState(() {
                    timeDropValue = availableTimings[i];
                    int index = availableTimings.indexOf(availableTimings[i]);
                    _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![index];
                    selected = true;
                    selectedIndex = i;
                  });
                },
                child: Card(
                  elevation: 2,
                  color: selectedIndex == i ? brandColor : Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: lightPurple, width: 2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0,right: 10),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.check_circle_outlined,
                            color: selectedIndex == i ? Colors.white : greyColor,
                            size: 12.0,
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Text(
                            availableTimings[i],
                            textAlign: TextAlign.center,
                            style: TextStyle(color: selectedIndex == i ? Colors.white : brandColor, fontFamily: "FSJoeyPro", fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  getDropDownMenuItems(snapshot) {
    Map<int, dynamic> mapWithIndex = snapshot.data.services.asMap;
    List<DropdownMenuItem> _items = [];
    mapWithIndex.forEach((index, item) {
      _items.add(
        DropdownMenuItem<int>(
          value: index,
          child: Text(item.service),
        ),
      );
    });
    return _items;
  }

  Widget showDateGrid(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 2,
        mainAxisSpacing: 10,
        childAspectRatio: 0.2 / 0.05,
      ),
      itemBuilder: (context, index) {
        return getDateDisplay(context, index);
      },
      physics: NeverScrollableScrollPhysics(),
      itemCount: availableTimings.length,
    );
  }

  Widget addText(String s, int index) {
    //print('AMANTIME:${appointment[selectedIndex].time}');
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(width: 15),
          Icon(
            Icons.check_circle_outline_rounded,
            size: 20,
            color: (index != selectedIndex ? Colors.grey.withOpacity(0.5) : Colors.white),
          ),
          SizedBox(width: 15),
          Text(
            s,
            style: TextStyle(
              color: index == selectedIndex ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }

  Widget getDateDisplay(BuildContext context, int i) {
    return InkWell(
      key: Key("time " + i.toString()),
      onTap: () {
        setState(() {
          _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![i];
          selected = true;
          selectedIndex = i;
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: selectedIndex == i ? brandColor : Colors.white,
            border: Border.all(color: selectedIndex == i ? brandColor : lightPurple, width: 3.0),
            borderRadius: BorderRadius.circular(8)),
        child: Wrap(
          alignment: WrapAlignment.center,
          spacing: 4,
          runSpacing: 4.0,
          children: <Widget>[
            //addText(appointment[i].date!),
            addText(availableTimings[i], i),
            //addText(appointment[i].doctor!),
          ],
        ),
      ),
    );
  }

  Widget doctorView() {
    print("update Hitted");
    return selected
        ? Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.fromLTRB(16.0, 5.0, 16.0, 5.0),
              key: Key("doctorsViewPopup"),
              decoration: BoxDecoration(
                color: bgDateColor,
                boxShadow: [
                  BoxShadow(
                    color: quartiaryColor,
                    offset: Offset(0.1, 0.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: SMALL_XX,
                  ),
                  Text(
                    _appointmentDoctorSlots!.doctorName.toString(),
                    style: TextStyle(fontSize: 14, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: SMALL,
                  ),
                  Text(
                    _appointmentDoctorSlots!.startTime.toString() + " - " + selectedItem!.displayDate.toString(),
                    style: TextStyle(fontSize: 16, fontFamily: "FSJoeyPro Heavy", color: brandColor),
                  ),
                  SizedBox(
                    height: SMALL_XX,
                  ),
                ],
              ),
            ),
          )
        : Container();
  }

  void getTimeStatus(BuildContext context, PreferenceModel model) async {
    if (PPApplication.prescriptionID != -1) {
      print("AppointmentID + ${PPApplication.prescriptionID}");
      model.appointmentId = PPApplication.prescriptionID;
      await bookAppointment(model);
    } else {
      AppointmentTimeRequest appointmentTime = AppointmentTimeRequest();
      appointmentTime.appointmentTime = _appointmentDoctorSlots!.actualApptTime.toString();
      bool success = await model.sendPreferencesTime(appointmentTime);
      if (success && model.appointmentId != null && model.appointmentId != -1 && selectedIndex != -1) {
        print("AppointmentID2 + ${model.appointmentId}");
        await bookAppointment(model);
      } else {
        Fluttertoast.showToast(msg: 'Please select an appointment slot', toastLength: Toast.LENGTH_LONG);
      }
    }
  }

  Future<void> skipAppointment(PreferenceModel model) async {
    //dropValue = null;
    TelehealthSignUpPreferenceRequest telehealthRequest = TelehealthSignUpPreferenceRequest(
      type: "TELEHEALTH",
      prescriptionState: "PENDING",
    );
    bool connectivityResult = await model.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }

    bool success = await model.signupOthersFlow("NONE","REGULAR");
    if (success) {
      dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "telehealth"));
    } else {
      model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }

  Future<void> bookAppointment(PreferenceModel model) async {
    //dropValue = null;
    if (widget.from == "telehealth") {
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
      dataStore.writeString(
          DataStoreService.TELEHEALTH_APPOINTMENT_TIME, selectedItem!.displayDate.toString() + ", " + _appointmentDoctorSlots!.startTime.toString());
      TelehealthSignUpPreferenceRequest? telehealthRequest;
      TelehealthSignUpRestrictedRequest? restrictedRequest;

      if (widget.prescriptionMedicalConditions == null || widget.prescriptionMedicalConditions!.isEmpty) {
        restrictedRequest = TelehealthSignUpRestrictedRequest(
            id: model.appointmentId,
            type: "TELEHEALTH",
            prescriptionState: "FILED",
            isPrescriptionEdited: true,
            doctorId: _appointmentDoctorSlots!.doctorId,
            teleHealthDoctorDayPlanId: _appointmentDoctorSlots!.teleHealthDoctorDayPlanId,
            telehealthAppointmentSlotId: _appointmentDoctorSlots!.telehealthAppointmentSlotId,
            appointmentTime: _appointmentDoctorSlots!.actualApptTime.toString());
      } else {
        telehealthRequest = TelehealthSignUpPreferenceRequest(
            id: model.appointmentId,
            appointmentTime: _appointmentDoctorSlots!.actualApptTime.toString(),
            type: "TELEHEALTH",
            prescriptionFilledByExternalPharmacy: !widget.defaultValue,
            prescriptionState: "FILED",
            isPrescriptionEdited: true,
            doctorId: _appointmentDoctorSlots!.doctorId,
            teleHealthDoctorDayPlanId: _appointmentDoctorSlots!.teleHealthDoctorDayPlanId,
            telehealthAppointmentSlotId: _appointmentDoctorSlots!.telehealthAppointmentSlotId);
      }
      bool connectivityResult = await model.isInternetConnected();
      if (connectivityResult == false) {
        onFail(context, errMessage: model.noInternetConnection);
        return;
      }

      bool success = widget.prescriptionMedicalConditions == null || widget.prescriptionMedicalConditions!.isEmpty
          ? await model.sendRestrictedConditionSignUp(restrictedRequest)
          : await model.sendPreferencesSignUp(telehealthRequest);
      if (success) {
        dataStore.writeBoolean(DataStoreService.SIGNUP_ADD_OTHERS, false);
        //Chips.chips.clear();
        Map<String, String> map = new Map();
        map["patientId"] = dataStore.getPatientId().toString();
        map["userId"] = dataStore.getUserId().toString();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_allowdoctor_entered);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_consultation_submit);
        ////

        int count = model.prescriptionList!.where((element) => element.prescriptionState!.contains('FILED')).length;
        Map<String, dynamic> leadMap = new Map();
        leadMap['lead_created_by'] = 'mobileApp';
        leadMap['lead_new'] = count > 1 ? 0 : 1;
        leadMap['lead_type'] = 'telehealth';
        leadMap['lead_origin'] = widget.source != null && widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
        leadMap['telehealth_condition_selected'] = widget.prescriptionMedicalConditions;

        if (model.revenue) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
        }

        ////
        widget.from == "telehealth"
            ? analyticsEvents.sendAnalyticsEvent("au_consultation_verification", map)
            : analyticsEvents.sendAnalyticsEvent("consultation_verification", map);
        dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
        PPApplication.prescriptionID = -1;
        TELEHEALTH_CONDITION = "";
        TELEHEALTH_COMMENT = "";
        Navigator.pushNamedAndRemoveUntil(context, HealthCardUploadViewSignUp.routeName, (Route<dynamic> route) => false,
                arguments: TelehealthArguments(
                    modelSignUp: widget.modelSignUp,
                    source: widget.source,
                    userPatient: widget.userPatient,
                    appointmentDateTime: dataStore.readString(DataStoreService.TELEHEALTH_APPOINTMENT_TIME).toString()))
            .then((value) => model.currentItem = 0);
      } else {
        analyticsEvents.sendAnalyticsEvent("consultation_verification_failed");
        model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }
    } else {
      AppointmentTimeRequest appointmentTime = AppointmentTimeRequest();
      appointmentTime.appointmentTime = _appointmentDoctorSlots!.actualApptTime.toString();
      TelehealthRequest telehealthRequest = TelehealthRequest(
          prescriptionType: "TELEHEALTH",
          prescriptionFilledByExternalPharmacy: !widget.defaultValue,
          prescriptionState: "FILED",
          prescriptionMedicalConditions: widget.prescriptionMedicalConditions,
          prescriptionComment: widget.prescriptionComment,
          appointmentTime: appointmentTime.appointmentTime,
          doctorId: _appointmentDoctorSlots!.doctorId,
          telehealthAppointmentSlotId: _appointmentDoctorSlots!.telehealthAppointmentSlotId,
          teleHealthDoctorDayPlanId: _appointmentDoctorSlots!.teleHealthDoctorDayPlanId);
      bool connectivityResult = await model.isInternetConnected();
      if (connectivityResult == false) {
        onFail(context, errMessage: model.noInternetConnection);
        return;
      }

      if (!widget.defaultValue) {
        Provider.of<PreferenceModel>(context, listen: false).showCallAndChatBottomSheet(
            context, TelehealthSignUpPreferenceRequest(), telehealthRequest, model, "", widget.modelSignUp!, widget.userPatient, widget.source);
      } else {
        bool success = await model.sendPreferences(telehealthRequest);
        if (success) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_allowdoctor_entered);
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_consultation_submit);
          Map<String, String> map = new Map();
          map["patientId"] = dataStore.getPatientId().toString();
          map["userId"] = dataStore.getUserId().toString();
          ////////

          Map<String, dynamic> leadMap = new Map();
          leadMap['lead_created_by'] = 'mobileApp';
          leadMap['lead_new'] = model.totalPrescriptionsCount! > 1 ? 0 : 1;
          leadMap['lead_type'] = 'telehealth';
          leadMap['lead_origin'] = widget.source != null && widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
          leadMap['telehealth_condition_selected'] = widget.prescriptionMedicalConditions;

          if (model.revenue) {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
          }
          ///////
          analyticsEvents.sendAnalyticsEvent("consultation_verification", map);

          int stepperValue = dataStore.readInteger(DataStoreService.STEPPER_CURRENT_STEP) ?? 0;

          Chips.chips = [];
          if (stepperValue > 0) {
            showBottomSheet(context, "APPOINTMENT_DASHBOARD");

            dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD, true);
            Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
            Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false).then((value) => model.currentItem = 0);
          } else {
            dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD, true);
            Navigator.pushReplacementNamed(context, OrderStepper.routeName,
                    arguments: BaseStepperArguments(source: BaseStepperSource.TELEHEALTH_SCREEN, startStep: 0, from: widget.from))
                .then((value) => model.currentItem = 0);
          }
        } else {
          analyticsEvents.sendAnalyticsEvent("consultation_verification_failed");
          model.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
        }
      }
    }
  }

  showProvinceBottomSheet(BuildContext context, SignUpTransferModel model, BaseStepperSource source, PreferenceModel preferenceModel) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return DraggableScrollableSheet(
            initialChildSize: 0.75,
            minChildSize: 0.75,
            maxChildSize: 0.75,
            expand: false,
            builder: (_, controller) => SelectProvinsSheet(
              context: context,
              model: model,
              source: source,
              userPatient: widget.userPatient,
              prescriptionMedicalConditions: widget.prescriptionMedicalConditions,
              prescriptionComment: widget.prescriptionComment,
            ),
          );
        });
  }

  showBottomSheet(BuildContext context, String s) {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          //return WillPopScope(onWillPop: (context), child: SuccessSheet(context, s));
          return WillPopScope(
              onWillPop: () async {
                return false;
              },
              child: SuccessSheet(context, s));
        });
  }

  Widget getDateTimeDisplay(BuildContext context, PreferenceModel model) {
    return model.appointmentsList.length > 0
        ? Column(crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
            getAvailableDaysDropDown(context, model),
            SizedBox(
              height: PPUIHelper.VerticalSpaceMedium,
            ),
            getAvailableTimesDropDown(context, model),
          ])
        : Padding(
            padding: EdgeInsets.symmetric(horizontal: PPUIHelper.VerticalSpaceXMedium),
            child: Container(
              decoration: new BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(40, 0, 0, 0),
                    blurRadius: 3,
                    spreadRadius: 0,
                    offset: Offset(
                      0, // horizontal,
                      1.0, // vertical,
                    ),
                  )
                ],
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(4.0),
                color: whiteColor,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Icon(Icons.event_busy, color: violetColorDark, size: 36),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec").isEmpty
                          ? "Unfortunately, there are currently no slots available for tele-consultation"
                          : LocalizationUtils.getSingleValueString("signup", "signup.appointment.no-slot-dec"),
                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18.0, height: 1.5, fontFamily: "FSJoeyPro", color: thinGrey),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  void selectDateAuto(PreferenceModel model) {
    Future.delayed(Duration(seconds: 0), () {
      setState(() {
        String val = model.appointmentsList[0].displayDate!;
        selectedIndex = -1;
        model.selectedDate = val;
        selectedItem = model.time[val];
        model.currentItem = model.appointListToActualIndex[selectedItem!.index];
        showGrid = true;
        selected = false;
        dropValue = val;
        if (availableTimings != null) {
          availableTimings.clear();
        }
        if (selectedItem!.doctorTimeSlots != null) {
          for (int j = 0; j < selectedItem!.doctorTimeSlots!.length; j++) {
            var dataItem = selectedItem!.doctorTimeSlots![j];
            availableTimings.add(dataItem.startTime.toString());
          }
        }
      });
    });
  }

  void selectTimeAuto(PreferenceModel model) {
    Future.delayed(Duration(seconds: 0), () {
      setState(() {
        String val = availableTimings[0];
        timeDropValue = val;
        int index = availableTimings.indexOf(val);
        _appointmentDoctorSlots = selectedItem!.doctorTimeSlots![index];
        selected = true;
        selectedIndex = index;
        autoSelected = true;
      });
    });
  }

  getHourDifference(String firstDateString, String secondDateString) {
    DateTime date1 = DateTime.parse(firstDateString);
    DateTime date2 = DateTime.parse(secondDateString);
    int difference = date2.difference(date1).inHours;
    return difference;
  }

  getCurrentDateString() {
    String currentDate = DateTime.now().toUtc().toString();
    return currentDate;
  }

  Triple getDisplayDate(String displayDate) {
    var collection = displayDate.split(" ");
    String month = collection[0];
    String date = collection[1].replaceAll(",", "");
    String day = collection[2];
    return Triple(item1: month, item2: date, item3: day);
  }
}
