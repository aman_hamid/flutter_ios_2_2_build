import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/ui/shared/appbars/dashboard_appbar.dart';
import 'package:pocketpills/ui/shared/appbars/main_navigation_bottom_bar.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/home/home_view.dart';
import 'package:pocketpills/ui/views/medications/medications_view.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/order/order_view.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_day_pack_view.dart';
import 'package:pocketpills/ui/views/prescriptions/prescriptions_view.dart';
import 'package:provider/provider.dart';

class DashboardWidget extends StatefulWidget {
  static const routeName = 'dashboardview';
  final String? snackBarMessage;

  DashboardWidget({Key? key, this.snackBarMessage}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends BaseState<DashboardWidget> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    dataStore.writeBoolean(DataStoreService.SIGNUP_COMPLETED, true);
    dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
    if (widget.snackBarMessage != null) {
      Timer? timer;
      timer = Timer.periodic(Duration(milliseconds: 500), (Timer t) {
        if (scaffoldKey != null && scaffoldKey.currentState != null) {
          timer!.cancel();
          scaffoldKey.currentState!.showSnackBar(new SnackBar(
              content: Text(
            widget.snackBarMessage!,
            style: TextStyle(fontFamily: "FSJoeyPro"),
          )));
        }
        //showOnSnackBar(scaffoldKey.currentState.context,successMessage: widget.snackBarMessage);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<DashboardModel>(create: (_) => DashboardModel())],
      child: Consumer<DashboardModel>(
        builder: (BuildContext context, DashboardModel dashboardModel, Widget? child) {
          return FutureBuilder(
            future: dashboardModel.getAndSetUserPatientList(),
            builder: (BuildContext context, AsyncSnapshot<List<UserPatient>?> snapshot) {
              if (dashboardModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return NoInternetScreen(
                  onClickRetry: dashboardModel.clearAsyncMemorizer,
                );
              }

              if (snapshot.hasData && snapshot.data != null && dashboardModel.connectivityResult != ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return afterFutureBuild(context, dashboardModel, snapshot.data!);
              } else if (snapshot.hasError && dashboardModel.connectivityResult != ConnectivityResult.none) {
                FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                return ErrorScreen();
              }

              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                return LoadingScreen();
              }
              return Container();
            },
          );
        },
      ),
    );
  }

  Widget afterFutureBuild(BuildContext context, DashboardModel dashboardModel, List<UserPatient> data) {
    print("from dashboard ${dataStore.readString(DataStoreService.PROVINCE)}");
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: SafeArea(
            child: Scaffold(
                key: scaffoldKey,
                appBar: DashboardAppBar(appBar: AppBar()),
                bottomNavigationBar: MainNavigationBottomBar(),
                body: Builder(builder: (BuildContext context) {
                  switch (dashboardModel.dashboardIndex) {
                    case 0:
                      return Builder(
                        builder: (context) => HomeWidget(),
                      );
                      break;
                    case 1:
                      return Builder(
                        builder: (context) => MedicationsWidget(dataStore.getBottomNavPos()),
                      );
                      break;
                    case 2:
                      return Builder(
                        builder: (context) => PrescriptionsWidget(dataStore.getBottomNavPos()),
                      );
                      break;
                    case 3:
                      return Builder(
                        builder: (context) => OrderWidget(dataStore.getBottomNavPos()),
                      );
                      break;
                    default:
                      return Builder(
                        builder: (context) => HomeWidget(),
                      );
                      break;
                  }
                })),
          ),
        ),
        (dashboardModel.applicationStart == true) ? PPContainer.emptyContainer() : PillReminderDayPackView()
      ],
    );
  }
}
