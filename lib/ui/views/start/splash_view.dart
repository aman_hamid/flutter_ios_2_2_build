import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/response/telehealth/localization_update_response.dart';
import 'package:pocketpills/core/response/version_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/update_dialog.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/home/medicine_detail_view.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/referral/referral_view.dart';
import 'package:pocketpills/ui/views/signup/customised_signup.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/views/start/phone_add_view_otp_password.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:uni_links/uni_links.dart';

class SplashView extends StatefulWidget {
  static const routeName = 'splash';

  String? deepLinkRouteName = null;
  int? carouselIndex;

  SplashView({this.deepLinkRouteName = null, this.carouselIndex = 0});

  @override
  State<StatefulWidget> createState() {
    return SplashViewState();
  }
}

class SplashViewState extends BaseState<SplashView> {
  final DataStoreService dataStore = locator<DataStoreService>();
  final SmsAutoFill _autoFill = SmsAutoFill();

  final TextEditingController _phoneNumberController = TextEditingController();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  bool phAutoValidate = false;
  bool _hintShown = false;

  @override
  initState() {
    super.initState();
    this.initDynamicLinks();
    //FirebaseCrashlytics.instance.crash();
    if (widget.deepLinkRouteName != null) {
      print("+++++++" + widget.deepLinkRouteName!);
    }
    dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, false);

    if (dataStore.readBoolean(DataStoreService.IS_LOGGED_IN)!) {
      analyticsEvents.setUserIdentifiers();
    }
    _phoneNumberFocusNode.addListener(() async {
      if (!_hintShown) {
        _hintShown = true;
        await _askPhoneHint();
      }
      await SmsAutoFill().listenForCode;
    });
    _phoneNumberController.addListener(shiftLoginFocus);
  }

  Future<void> _askPhoneHint() async {
    String? hint = await _autoFill.hint;
    _phoneNumberController.value = TextEditingValue(text: StringUtils.getFormattedPhoneNumber(hint));
  }

  @override
  void dispose() {
    _phoneNumberController.removeListener(shiftLoginFocus);
    PPApplication.logout = false;
    super.dispose();
  }

  shiftLoginFocus() async {
    if (_phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
      setState(() {
        phAutoValidate = false;
      });
    }
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();

    if (data != null) {
      FirebaseDynamicLinks.instance.onLink(onSuccess: (PendingDynamicLinkData? dynamicLink) async {
        final Uri? deepLink = dynamicLink!.link;

        if (deepLink != null) {
          print("deep link $deepLink" + deepLink.path + " " + deepLink.scheme);
          //Navigator.pushNamed(context, deepLink.path);
        }
      }, onError: (OnLinkErrorException e) async {
        print('onLinkError');
        print(e.message);
        FirebaseCrashlytics.instance.log(e.toString());
      });
    }
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel()), Provider<LoginService>(create: (_) => LoginService())],
      child: Consumer2<LoginModel, LoginService>(builder: (BuildContext context, LoginModel loginModel, LoginService loginService, Widget? child) {
        return FutureBuilder(
            future: loginModel.checkContentEngineUpdates(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null) {
                return getInitialLoad(loginModel, loginService);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget getInitialLoad(LoginModel loginModel, LoginService loginService) {
    return getMainView(loginModel, loginService);
  }

  Widget getMainView(LoginModel loginModel, LoginService loginService) {
    return FutureBuilder(
      future: myFutureMethodOverall(loginModel),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (loginModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
          return NoInternetScreen(
            onClickRetry: loginModel.cleanMemorizer(),
          );
        }

        if (snapshot.hasData && loginModel.connectivityResult != ConnectivityResult.none) {
          if (snapshot.data[1] != null && snapshot.data[1].shouldUpgrade != null && snapshot.data[1].shouldUpgrade == true) {
            Future.delayed(
              Duration(milliseconds: 10),
              () {
                return showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return UpdateDialog(
                      versionResponse: snapshot.data[1],
                    );
                  },
                );
              },
            );
          }
          return getStatedView(loginService);
        } else if (snapshot.hasError && loginModel.connectivityResult != ConnectivityResult.none) {
          FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
          return ErrorScreen();
        }

        if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }
        return LoadingScreen();
      },
    );
  }

  Future myFutureMethodOverall(LoginModel loginModel) async {
    Future<Map<String, dynamic>?> future1 = loginModel.getLocalization(["common", "modals", "modal", "copay"]);
    Future<VersionResponse?> future2 = loginModel.onVersionCallCheck();
    return await Future.wait([future1, future2]);
  }

  Widget getStatedView(LoginService loginService) {
    PPApplication.logout = false;
    if (BaseState.app_open == true) {
      print('AMANLAUNCH:Launch Application');
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.app_open);
      BaseState.app_open = false;
    }
    if (loginService.isUserLogin()) {
      return getSignUpFlow(loginService);
    } else {
      return StartView(
        carouselIndex: widget.carouselIndex,
      );
    }
  }

  StatefulWidget getDeepLinkFlowView(BuildContext context) {
    if (widget.deepLinkRouteName == null) {
      return DashboardWidget();
    } else {
      switch (widget.deepLinkRouteName) {
        case TransferWidget.routeName:
          return TransferWidget();
        case AddMemberSignupWidget.routeName:
          return AddMemberSignupWidget();
        case UploadPrescription.routeName:
          return UploadPrescription();
        case ReferralView.routeName:
          return ReferralView();
        case MedicineDetailWidget.routeName:
          return MedicineDetailWidget();
        case VitaminsWidget.routeName:
          Provider.of<VitaminsSubscriptionModel>(context).clearVitaminList();
          return VitaminsWidget();
        default:
          return DashboardWidget();
      }
    }
  }

  Widget getSignUpFlow(LoginService loginService) {
    return FutureBuilder(
        future: Provider.of<SignUpModel>(context, listen: false).getUserInfo(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data != null && snapshot.data == true) {
            switch (loginService.checkOutSignUpFlow()) {
              case SignupStepperStateEnums.SIGN_UP_ABOUT_YOU:
                return SignupWidget(
                  source: BaseStepperSource.NEW_USER,
                );

              case SignupStepperStateEnums.CUSTOMISED_SIGN_UP:
                return CustomisedSignUpWidget(
                  source: BaseStepperSource.NEW_USER,
                );

              case SignupStepperStateEnums.SIGN_UP_TRANSFER:
                return TransferWidget(
                  source: BaseStepperSource.NEW_USER,
                );

              case SignupStepperStateEnums.SIGN_UP_UPLOAD_PRESCRIPTION:
                print('AMANMOVING:UPLOAD');
                return UploadPrescription(
                  source: BaseStepperSource.NEW_USER,
                  from: 'customize-signup',
                );

                case SignupStepperStateEnums.SIGN_UP_TELEHEALTH:
                print('AMANMOVING:UPLOAD');
                return TelehealthPreference(
                  source: BaseStepperSource.NEW_USER,
                  from: 'telehealth',
                );

              case SignupStepperStateEnums.SIGN_UP_ALMOST_DONE:
                return SignUpAlmostDoneWidget(
                  source: BaseStepperSource.NEW_USER,
                  from: dataStore.readString(DataStoreService.PRESCRIPTION_TYPE) ?? "",
                );

              case SignupStepperStateEnums.SIGN_UP_OTP_PASSWORD:
                return SignUpOtpPasswordWidget(
                  source: BaseStepperSource.NEW_USER,
                );
              case SignupStepperStateEnums.SIGN_UP_TELEHEALTH_CARD:
                return HealthCardUploadViewSignUp(
                  source: BaseStepperSource.NEW_USER,
                );
              case SignupStepperStateEnums.SIGN_UP_PHONE_ADD:
                return SignUpPhoneAddWidget();
              default:
                return getDeepLinkFlowView(context);
            }
          } else if (snapshot.data == false) {
            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
            return ErrorScreen(
              onRetry: () {
                Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
              },
            );
          }
          return LoadingScreen();
        });
  }
}
