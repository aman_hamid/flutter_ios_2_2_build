import 'dart:developer';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/login_view_identifier.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_verification_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_verification_otp_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/start/apple_phone_bottom_sheet.dart';
import 'package:pocketpills/ui/views/start/pp_carousel.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/custom_auth_button.dart';
import 'package:pocketpills/utils/custom_dropdown_2.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:url_launcher/url_launcher.dart';

class StartView extends StatefulWidget {
  static const routeName = 'start';

  String? deepLinkRouteName = null;
  int? carouselIndex;
  bool? chambersFlow = false;

  StartView({this.deepLinkRouteName = null, this.carouselIndex = 0, this.chambersFlow = false});

  @override
  State<StatefulWidget> createState() {
    return StartViewState();
  }
}

class StartViewState extends BaseState<StartView> {
  final DataStoreService dataStore = locator<DataStoreService>();
  //final SmsAutoFill _autoFill = SmsAutoFill();

  final TextEditingController _phoneNumberController = TextEditingController();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  bool phAutoValidate = false;
  bool _hintShown = false;

  String? getStartedWithoutPhoneNumber;
  String? languageDropDownValue;

  int? selectedRadio;

  @override
  initState() {
    super.initState();
    this.initDynamicLinks();
    if (widget.deepLinkRouteName != null) {
      print("+++++++" + widget.deepLinkRouteName!);
    }
    getStartedWithoutPhoneNumber = dataStore.readString(DataStoreService.GET_STARTED_WITHOUT_PHONE_NUMBER);
    if (dataStore.readBoolean(DataStoreService.IS_LOGGED_IN)!) {
      analyticsEvents.setUserIdentifiers();
    }
    _phoneNumberFocusNode.addListener(() async {
      /*if (!_hintShown) {
        _hintShown = true;
        await _askPhoneHint();
      }*/
    });
    _phoneNumberController.addListener(shiftLoginFocus);
    selectedRadio = 0;
    //_autoFillOtpInit();
  }

  /*void _autoFillOtpInit() async {
    await _autoFill.listenForCode;
  }

  Future<void> _askPhoneHint() async {
    String? hint = await _autoFill.hint;
    _phoneNumberController.value = TextEditingValue(text: StringUtils.getFormattedPhoneNumber(hint));
  }*/

  @override
  void dispose() {
    _phoneNumberController.removeListener(shiftLoginFocus);
    super.dispose();
  }

  shiftLoginFocus() async {
    if (_phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
      setState(() {
        phAutoValidate = false;
      });
    }
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    if (data != null) {
      FirebaseDynamicLinks.instance.onLink(onSuccess: (PendingDynamicLinkData? dynamicLink) async {
        final Uri? deepLink = dynamicLink!.link;

        if (deepLink != null) {
          print("deep link $deepLink" + deepLink.path + " " + deepLink.scheme);
          // Navigator.pushNamed(context, deepLink.path);
        }
      }, onError: (OnLinkErrorException e) async {
        print('onLinkError');
        print(e.message);
        FirebaseCrashlytics.instance.log(e.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel())],
      child: Consumer<LoginModel>(builder: (BuildContext context, LoginModel loginModel, Widget? child) {
        return FutureBuilder(
            future: loginModel.getLocalization(["app-landing", "common", "landing"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                if (loginModel.fireLandingEvent == true) {
                  loginModel.fireLandingEvent = false;
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.carousel1);
                }
                return getMainFlow(loginModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  TextFormField getUsernameField(BuildContext context, LoginModel model) {
    return PPFormFields.getNumericFormField(
        key: Key("phoneField"),
        keyboardType: TextInputType.phone,
        textInputAction: TextInputAction.done,
        controller: _phoneNumberController,
        maxLength: 10,
        focusNode: _phoneNumberFocusNode,
        decoration: PPInputDecor.getDecoration(
          labelText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-phone"),
          hintText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-info"),
          prefixIcon: Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                '+1 |',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "FSJoeyPro Medium",
                ),
              )),
        ),
        onFieldSubmitted: (value) {
          onClickProceedSecurely(context, model);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (StringUtils.isNumericUsingRegularExpression(value.toString())) {
            String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
            if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1");
            if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2");
          } else {
            if (!StringUtils.isEmail(value.toString())) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-error");
          }
          return null;
        });
  }

  Widget getMainFlow(LoginModel loginModel) {
    return BaseScaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: whiteColor,
        elevation: 1,
        brightness: Platform.isIOS == true ? Brightness.light : null,
        iconTheme: IconThemeData(color: Colors.white //change your color here
            ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            PPApplication.quebecCheck
                ? Image.network(
                    "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                    width: MediaQuery.of(context).size.width * 0.45,
                    fit: BoxFit.cover,
                  )
                : Image.asset(getLogoImage(), width: MediaQuery.of(context).size.width * 0.45)
          ],
        ),
        leading: null,
        actions: [
          _getLanguagePreference(),
          SizedBox(
            width: 5.0,
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(child: SizedBox(height: MediaQuery.of(context).size.height * 0.45, child: PPCarousel(carouselIndex: widget.carouselIndex))),
            Align(
              alignment: Alignment.bottomCenter,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(left: MEDIUM, right: MEDIUM, top: MEDIUM_XXX),
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(top: MEDIUM_X, right: MEDIUM_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                      child: getStartedView(loginModel, context),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isProvinceQuebec(LoginModel model) {
    return model.quebecCheck ? true : false;
  }

  Widget _getLanguagePreference() {
    return CustomDropdownButtonHideUnderline(
      child: CustomDropdownButton(
        value: languageDropDownValue == null
            ? getSelectedLanguage() == "en"
                ? "en"
                : "fr"
            : languageDropDownValue,
        onChanged: (String? newValue) {
          setState(() {
            languageDropDownValue = newValue;
            changeLanguage(context, newValue!);
          });
        },
        selectedItemBuilder: (BuildContext context) {
          return ViewConstants.languageMap
              .map((String key, String value) {
                return MapEntry(
                  key,
                  CustomDropdownMenuItem<String>(
                    value: key,
                    child: Text(
                      key.toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "FSJoeyPro Bold",
                      ),
                    ),
                  ),
                );
              })
              .values
              .toList();
        },
        items: ViewConstants.languageMap
            .map((String key, String value) {
              return MapEntry(
                key,
                CustomDropdownMenuItem<String>(
                  value: key,
                  child: Container(
                    width: 80,
                    child: Text(
                      key.toUpperCase(),
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontFamily: "FSJoeyPro",
                      ),
                    ),
                  ),
                ),
              );
            })
            .values
            .toList(),
      ),
    );
  }

  Widget getStartedView(LoginModel loginModel, BuildContext context) {
    return getPhoneNumberView(loginModel, context);
  }

  Widget getPhoneNumberView(LoginModel loginModel, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: SMALL_X,
        ),
        PPTexts.getSecondaryHeading(
          LocalizationUtils.getSingleValueString("landing", "landing.fields-new.signup-label-mobile").replaceAll("<br>", "\n"),
          textAlign: TextAlign.center,
          fontSize: 15,
          color: brandColor,
          isBold: true,
        ),
        SizedBox(
          height: SMALL_XXX,
        ),
        AuthButton(
          backgroundColor: googleColor,
          text: LocalizationUtils.getSingleValueString("app-landing", "app-landing.button.google"),
          onPressed: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed_google);
            checkAuthSignIn(context, loginModel, "google");
          },
          height: 49,
          image: Image.asset("graphics/icon_google.png"),
        ),
        SizedBox(
          height: SMALL_XXX,
        ),
        Platform.isIOS
            ? AuthButton(
                backgroundColor: blackColor,
                text: LocalizationUtils.getSingleValueString("app-landing", "app-landing.button.apple"),
                onPressed: () {
                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed_apple);
                  checkAuthSignIn(context, loginModel, "apple");
                },
                height: 49,
                image: Image.asset("graphics/icon_apple.png"),
              )
            : Container(),
        SizedBox(
          height: SMALL_X,
        ),
        Builder(builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 1,
                    width: 100,
                    color: tertiaryColor,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(LocalizationUtils.getSingleValueString("common", "common.label.or"),
                      style: TextStyle(
                        fontFamily: "FSJoeyPro",
                        fontSize: 12,
                      )),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    height: 1,
                    width: 100,
                    color: tertiaryColor,
                  ),
                ],
              ),
              SizedBox(
                height: SMALL_XX,
              ),
              getUsernameField(context, loginModel),
              SizedBox(
                height: MEDIUM,
              ),
              loginModel.state == ViewState.Busy
                  ? ViewConstants.progressIndicator
                  : PrimaryButton(
                      key: Key("buttonProceed"),
                      fullWidth: true,
                      disabled: false,
                      butttonColor: lightBlueColor,
                      textColor: brandColor,
                      text: LocalizationUtils.getSingleValueString("landing", "landing.fields-new.proceed").isEmpty ? "Get started":LocalizationUtils.getSingleValueString("landing", "landing.fields-new.proceed"),
                      isAllCaps: false,
                      onPressed: () {
                        onClickProceedSecurely(context, loginModel);
                      },
                    ),
              SizedBox(
                height: SMALL_XX,
              ),
              getTnCAndPrivacyPolicyView(),
            ],
          );
        })
      ],
    );
  }

  showCallAndChatBottomSheet(BuildContext context, String? userIdentifier) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return ApplePhoneSheet(context: context, userIdentifier: userIdentifier);
        });
  }

  Widget getTnCAndPrivacyPolicyView() {
    TapGestureRecognizer _onTapTandC = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getTermsAndCondtions())) {
          await launch(getTermsAndCondtions());
        }
      };
    TapGestureRecognizer _onTapPrivacyPolicy = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getPrivacyPolicy())) {
          await launch(getPrivacyPolicy());
        }
      };

    List<String> list = LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.terms-conditions").split("-");
    String first = list[0];
    String second = list[1];
    String third = list[2];
    String fourth = list[3];

    return RichText(
      text: TextSpan(
        text: first + ' ',
        style: MEDIUM_SECONDARY_PRIVACY,
        children: <TextSpan>[
          TextSpan(
            text: second + ' ',
            style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE,
            recognizer: _onTapTandC,
          ),
          TextSpan(text: third + ' ', style: MEDIUM_SECONDARY_PRIVACY),
          TextSpan(text: fourth, style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE, recognizer: _onTapPrivacyPolicy),
        ],
      ),
      textAlign: TextAlign.center,
    );
  }

  onClickProceedSecurely(BuildContext context, LoginModel loginModel) {
    if (_phoneNumberController.text.isEmpty) {
      showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phoneerror"));
    } else {
      String formattedNumber =
          _phoneNumberController.text.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      if (StringUtils.isNumericUsingRegularExpression(formattedNumber)) {
        if (formattedNumber.length == 10) {
          checkUserIdentifier(context, loginModel, formattedNumber);
        } else if (formattedNumber.length < 10) {
          showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1"));
        } else {
          showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2"));
        }
      } else {
        if (StringUtils.isEmail(_phoneNumberController.text)) {
          checkUserIdentifier(context, loginModel, _phoneNumberController.text);
        } else {
          showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.email-error"));
        }
      }
    }
  }

  checkUserIdentifier(BuildContext context, LoginModel loginModel, String userIdentifier) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_landing_form_submit);
    Map<String, String> map = new Map();
    map["signupType"] = "phone";
    //loginModel.fireLandingEvent = true;
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed, map);
    phAutoValidate = false;
    if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true && null == dataStore.readString(DataStoreService.PHONE)) {
      dataStore.writeString(DataStoreService.PHONE, _phoneNumberController.text);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.identity_submitted);
      await HttpApiUtils().chambersActivate(loginModel.group!, loginModel.token!, _phoneNumberController.text);
      if (ChambersRedirect.routeName()!.contains('signup')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
      }
      if (ChambersRedirect.routeName()!.contains('transfer')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: TransferArguments(
              source: BaseStepperSource.NEW_USER,
              chambersFlow: true,
            ));
      }
      if (ChambersRedirect.routeName()!.contains('signUpAlmostDoneWidget')) {
        Navigator.of(context).pushNamedAndRemoveUntil(ChambersRedirect.routeName()!, (Route<dynamic> route) => false,
            arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
      }
      return;
    }

    PhoneVerification? checkPhone = await loginModel.checkUserIdentifier(userIdentifier);
    if (checkPhone != null) {
      checkLoginProcess(context, loginModel, checkPhone);
    } else {
      onFail(context, errMessage: ApplicationConstant.apiError);
    }
  }

  moveInSignUpFlow(LoginModel loginModel) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_signup);
    SignupPhoneNumberResponse? signupResponse = await loginModel.phoneNumberSignup(_phoneNumberController.text);
    if (signupResponse != null && signupResponse.userId != null) {
      locator<NavigationService>().pushNamed(
        SignupWidget.routeName,
        argument: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU, source: BaseStepperSource.NEW_USER),
      );
    } else
      onFail(context, errMessage: loginModel.errorMessage);
  }

  checkLoginProcess(BuildContext buildContext, LoginModel loginModel, PhoneVerification? checkPhone) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.identity_submitted);
    dataStore.writeString(DataStoreService.SIGNUP_TYPE, checkPhone!.signUpType!);
    if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.PHONE_LOGIN) {
      locator<NavigationService>().pushNamed(LoginWidget.routeName,
          argument: LoginArguments(phone: _phoneNumberController.text, loginType: checkPhone.redirect!),
          viewPushEvent: AnalyticsEventConstant.landing_login,
          viewPopEvent: AnalyticsEventConstant.carousel1);
    } else if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_UNVERIFIED_PHONE_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS ||
        checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS) {
      locator<NavigationService>().pushNamed(LoginWidgetIdentifier.routeName,
          argument: LoginArguments(hint: checkPhone.hint, phone: _phoneNumberController.text, loginType: checkPhone.redirect),
          viewPushEvent: AnalyticsEventConstant.landing_login,
          viewPopEvent: AnalyticsEventConstant.carousel1);
    } else if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
      moveInSignUpFlow(loginModel);
    } else if (checkPhone.redirect == PhoneNumberStateConstant.PHONE_OTP_FORGOT || checkPhone.redirect == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
      var success = await loginModel.getOtpByIdentifier(_phoneNumberController.text);
      if (success == true) {
        locator<NavigationService>().pushNamed(
          SignUpVerificationOtpPasswordWidget.routeName,
        );
      } else
        onFail(context, errMessage: loginModel.errorMessage);
    }
  }

  checkAuthSignIn(BuildContext context, LoginModel loginModel, String authKey) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    AuthVerification? checkPhone;

    if (authKey == "google") {
      checkPhone = await loginModel.useGoogleAuthentication();
    } else {
      checkPhone = await loginModel.useAppleAuthentication();
    }
    if (checkPhone != null) {
      if (authKey == "google") {
        Map<String, String> map = new Map();
        map["signupType"] = "google";
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed, map);
      } else {
        Map<String, String> map = new Map();
        map["signupType"] = "apple";
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed, map);
      }
      updateUserDetails(context, loginModel, checkPhone, authKey);
    } else {
      onFail(context, errMessage: ApplicationConstant.apiError);
    }
  }

  updateUserDetails(BuildContext context, LoginModel loginModel, AuthVerification checkPhone, String authKey) async {
    if (checkPhone.redirect == PhoneNumberStateConstant.MASKED_EMAIL_ASK_PHONE) {
      Map<String, dynamic> map = new Map();
      map["maskEmail"] = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.apple_success, map);
      showCallAndChatBottomSheet(context, checkPhone.userIdentifier);
    } else {

        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.identity_submitted);
        if (authKey == "google") {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.google_success);
        } else {
          Map<String, dynamic> map = new Map();
          if (dataStore.readBoolean(DataStoreService.MASKED_EMAIL) != null && dataStore.readBoolean(DataStoreService.MASKED_EMAIL)!) {
            map["maskEmail"] = true;
          } else {
            map["maskEmail"] = false;
          }
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.apple_success, map);
        }
        if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
          bool successGetUserInfo = await loginModel.getUserInfo(listenable: true);
          if (successGetUserInfo) {
            Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
          }else {
            onFail(context, errMessage: loginModel.errorMessage);
          }
        } else if (checkPhone.redirect == PhoneNumberStateConstant.EXTERNALLY_AUTHENTICATED) {
          bool successGetUserInfo = await loginModel.getUserInfo(listenable: true);
          if (successGetUserInfo) {
            Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
          }else {
            onFail(context, errMessage: loginModel.errorMessage);
          }
        } else if (checkPhone.redirect == PhoneNumberStateConstant.PHONE_OTP_FORGOT || checkPhone.redirect == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
          var success = await loginModel.getOtpByIdentifier(_phoneNumberController.text);
          if (success == true) {
            locator<NavigationService>().pushNamed(
              SignUpVerificationOtpPasswordWidget.routeName,
            );
          }
        } else if (checkPhone.redirect == PhoneNumberStateConstant.EMAIL_UNVERIFIED_PHONE_LOGIN_EXISTS ||
            checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_EMAIL_LOGIN_EXISTS ||
            checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_GOOGLE_LOGIN_EXISTS ||
            checkPhone.redirect == PhoneNumberStateConstant.PHONE_UNVERIFIED_APPLE_LOGIN_EXISTS) {
          locator<NavigationService>().pushNamed(LoginWidgetIdentifier.routeName,
              argument: LoginArguments(hint: checkPhone.hint, phone: checkPhone.userIdentifier, loginType: checkPhone.redirect),
              viewPushEvent: AnalyticsEventConstant.landing_login,
              viewPopEvent: AnalyticsEventConstant.carousel1);
        } else {
          onFail(context, errMessage: loginModel.errorMessage);
        }
    }
  }
}
