import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/string_utils.dart';

class PhoneBottomSheet extends StatefulWidget {
  BuildContext? context;

  PhoneBottomSheet({this.context});

  _PhoneBottomSheet createState() => _PhoneBottomSheet();
}

class _PhoneBottomSheet extends BaseState<PhoneBottomSheet> with SingleTickerProviderStateMixin {
  late BuildContext context;

  final TextEditingController _phoneNumberController = TextEditingController();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  _PhoneBottomSheet();

  late LoginModel model;

  @override
  void initState() {
    super.initState();
    model = LoginModel();
    context = widget.context!;
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView(context);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(LoginModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["app-landing", "common", "modal"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Wrap(
        children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  PPTexts.getHeading(
                    LocalizationUtils.getSingleValueString("modal", "modal.editdetails.title-phone"),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  Builder(builder: (context) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        getUsernameField(context, model),
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        model.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : PrimaryButton(
                                fullWidth: true,
                                //disabled: true,
                                butttonColor: brandColor,
                                textColor: whiteColor,
                                text: LocalizationUtils.getSingleValueString("modal", "modal.all.update"),
                                onPressed: () {
                                  onClickProceedSecurely(context, model);
                                },
                              ),
                        SizedBox(
                          height: SMALL_XX,
                        ),
                        SizedBox(
                          height: SMALL,
                        ),
                      ],
                    );
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField getUsernameField(BuildContext context, LoginModel model) {
    return PPFormFields.getNumericFormField(
        keyboardType: TextInputType.phone,
        textInputAction: TextInputAction.done,
        controller: _phoneNumberController,
        maxLength: 10,
        focusNode: _phoneNumberFocusNode,
        decoration: PPInputDecor.getDecoration(
          labelText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-phone"),
          hintText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-info"),
          prefixIcon: Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                '+1 |',
                style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Medium"),
              )),
        ),
        onFieldSubmitted: (value) {
          onClickProceedSecurely(context, model);
        },
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          if (StringUtils.isNumericUsingRegularExpression(value.toString())) {
            String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
            if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1");
            if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2");
          } else {
            if (!StringUtils.isEmail(value.toString())) return LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone-error");
          }
          return null;
        });
  }

  onClickProceedSecurely(BuildContext context, LoginModel loginModel) {
    if (_phoneNumberController.text.isEmpty) {
      Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phoneerror"), timeInSecForIosWeb: 3, fontSize: 14.0);
    } else {
      String formattedNumber = _phoneNumberController.text.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      if (StringUtils.isNumericUsingRegularExpression(formattedNumber)) {
        if (formattedNumber.length == 10) {
          checkUserIdentifier(context, loginModel, formattedNumber);
        } else if (formattedNumber.length < 10) {
          Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error1"), timeInSecForIosWeb: 3, fontSize: 14.0);
        } else {
          Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.cell-error2"), timeInSecForIosWeb: 3, fontSize: 14.0);
        }
      }
    }
  }

  checkUserIdentifier(BuildContext context, LoginModel loginModel, String phone) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    setState(() {
      model.setState(ViewState.Busy);
    });
    bool result = await model.updatePhoneYou(_phoneNumberController.text);
    if (result == true) {
      var success = await model.getOtpByIdentifier(_phoneNumberController.text);
      if (success) {
        var phoneNumber = _phoneNumberController.text;
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_number_updated);
        Navigator.pop(context, phoneNumber);
      }
    } else {
      Fluttertoast.showToast(msg: loginModel.errorMessage,toastLength: Toast.LENGTH_LONG);
    }
    setState(() {
      model.setState(ViewState.Idle);
    });
  }
}
