import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:the_apple_sign_in/apple_id_request.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart';

class Authentication {
  static GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: [
      'profile',
      'email', /*'https://www.googleapis.com/auth/user.birthday.read', 'https://www.googleapis.com/auth/user.phonenumbers.read'*/
    ],
  );

  static Future<List<dynamic>> signInWithGoogle() async {
    List googleDetails = [];
    GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
      String? idToken = googleSignInAuthentication.idToken;
      String? accessToken = googleSignInAuthentication.accessToken;
      googleDetails.add(idToken);
      googleDetails.add(accessToken);

      printLogs(idToken!);

      return googleDetails;
    } else {
      throw PlatformException(
        code: 'ERROR_ABORTED_BY_USER',
        message: 'Sign in aborted by user',
      );
    }
  }

  static void signOutGoogle() async {
    await googleSignIn.signOut();
    print("Token ac Out");
  }

  static Future<List<dynamic>> signInWithApple() async {
    List googleDetails = [];
    // 1. perform the sign-in request
    List<Scope> scopes = const [];
    final result = await TheAppleSignIn.performRequests([
      AppleIdRequest(
        requestedScopes: [Scope.email, Scope.fullName],
      )
    ]);
    print('apple status');
    // 2. check the result
    switch (result.status) {
      case AuthorizationStatus.authorized:
        final appleIdCredential = result.credential;

        String idToken = String.fromCharCodes(appleIdCredential!.identityToken!);
        String accessToken = String.fromCharCodes(appleIdCredential.authorizationCode!);

        String? firstName = appleIdCredential.fullName?.givenName;
        String? lastName = appleIdCredential.fullName?.familyName;

        String? email = appleIdCredential.email == null ? getEmail(idToken) : appleIdCredential.email;
        googleDetails.add(idToken);
        googleDetails.add(accessToken);
        googleDetails.add(firstName);
        googleDetails.add(lastName);
        googleDetails.add(email);
        return googleDetails;
      case AuthorizationStatus.error:
        print('apple status ERROR_AUTHORIZATION_DENIED');
        throw PlatformException(
          code: 'ERROR_AUTHORIZATION_DENIED',
          message: result.error.toString(),
        );

      case AuthorizationStatus.cancelled:
        throw PlatformException(
          code: 'ERROR_ABORTED_BY_USER',
          message: 'Sign in aborted by user',
        );
      default:
        throw UnimplementedError();
    }
  }
}

String getEmail(String idToken) {
  Map<String, dynamic> decodedToken = JwtDecoder.decode(idToken);
  return decodedToken["email"];
}

void printLogs(String googleSignInAuthentication) {
  String id = googleSignInAuthentication;
  print("Token $id");
}
