import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_insurance_model.dart';
import 'package:pocketpills/core/viewmodels/progress_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_insurance_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class HealthCardNumberSheet extends StatefulWidget {
  BuildContext? context;
  ProfileInsuranceModel? profileInsuranceModel;
  BaseStepperSource? source;
  Function? onSuccess;

  HealthCardNumberSheet({this.context, this.profileInsuranceModel, this.source, this.onSuccess});

  _HealthCardNumberSheet createState() => _HealthCardNumberSheet();
}

class _HealthCardNumberSheet extends BaseState<HealthCardNumberSheet> with SingleTickerProviderStateMixin {
  late BuildContext context;

  final TextEditingController _healthCardController = TextEditingController();
  final FocusNode _healthCardFocusNode = FocusNode();

  _HealthCardNumberSheet();

  late ProfileInsuranceModel model2;
  late ProgressModel model3;
  bool autoValidate =  false;

  @override
  void initState() {
    super.initState();
    model2 = widget.profileInsuranceModel!;
    model3 = ProgressModel();
    context = widget.context!;
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model2),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(ProfileInsuranceModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["insurance", "signup", "common"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Padding(
      key: Key("HealthCardNumberView"),
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Wrap(
        children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  PPTexts.getSecondaryHeading(
                    LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.description"),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: MEDIUM_X,
                  ),
                  Builder(builder: (contexts) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        getUsernameField(contexts, model2),
                        SizedBox(
                          height: MEDIUM_X,
                        ),
                        model3.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : PrimaryButton(
                                key: Key("continueHealthNumber"),
                                //disabled: true,
                                fullWidth: true,
                                butttonColor: lightBlueColor,
                                textColor: brandColor,
                                text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.submit"),
                                onPressed: () {
                                  setState(() {
                                    model3.setState(ViewState.Busy);
                                  });
                                  onClickHealthCardNumber(contexts, model2);
                                },
                              ),
                        SizedBox(
                          height: SMALL_XX,
                        ),
                      ],
                    );
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField getUsernameField(BuildContext contextm, ProfileInsuranceModel profileInsuranceModel) {
    return PPFormFields.getTextField(
        key: Key("healthNumber"),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        autovalidate: autoValidate,
        controller: _healthCardController,
        focusNode: _healthCardFocusNode,
        decoration: PPInputDecor.getDecoration(
          labelText: "",
          hintText: "",
        ),
        onFieldSubmitted: (value) {
          onClickHealthCardNumber(contextm, profileInsuranceModel);
        },
    validator: (value) {
    RegExp exp = getRegex();
    bool val = exp.hasMatch(value.toString());
    if (val == true) return null;
    return value.toString().isEmpty
    ? LocalizationUtils.getSingleValueString("common", "common.label.this-field-required")
        : LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.invalid-error");
    },);
  }

  RegExp getRegex() {
    if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "BRITISH_COLUMBIA"){
      return RegExp(r"^[0-9]{1,10}$");
    }else if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NORTHWEST_TERRITORIES"){
      return RegExp(r"^[nN][0-9]{1,7}$");
    }else if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "ONTARIO"){
      return RegExp(r"^[0-9]{1,10}[A-Za-z]{2}$");
    }else if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NEWFOUNDLAND_AND_LABRADOR"){
      return RegExp(r"^[0-9]{1,12}$");
    }else if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "PRINCE_EDWARD_ISLAND"){
      return RegExp(r"^[0-9]{1,8}$");
    }else if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NOVA_SCOTIA"){
      return RegExp(r"^[0-9]{1,10}$");
    }else {
      return RegExp(r"^[0-9]{1,9}$");
    }
  }

  void onClickHealthCardNumber(BuildContext context, ProfileInsuranceModel profileInsuranceModel) async {
    RegExp exp = getRegex();
    bool val = exp.hasMatch(_healthCardController.text.toString());
    if (val == false) {
      setState(() {
        autoValidate = true;
      });
    } else {
      var connectivityResult = await profileInsuranceModel.checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        onFail(context, errMessage: profileInsuranceModel.noInternetConnection);
        return;
      }
        var addCardNumber = await profileInsuranceModel.addHealthCardNumber(_healthCardController.text.toString(), "PROVINCIAL");
        if (addCardNumber != null && addCardNumber.item2 == true) {
          setState(() {
            model3.setState(ViewState.Idle);
          });
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_number_entered);
          profileInsuranceModel.healthRetry = false;
          profileInsuranceModel.healthRetryCount = 0;
          if (widget.source != null && widget.source == BaseStepperSource.HEALTH_CARD_ORDER_STEPPER_SCREEN) {
            widget.onSuccess!();
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_submit);
            Fluttertoast.showToast(msg: addCardNumber.item1, timeInSecForIosWeb: 3, fontSize: 14.0);
            Navigator.pop(context);
          } else if (widget.source != null && widget.source == BaseStepperSource.COPAY_REQUEST) {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_submit);
            Fluttertoast.showToast(msg: addCardNumber.item1, timeInSecForIosWeb: 3, fontSize: 14.0);
            Navigator.pop(context);
            Navigator.pushNamed(context, ProfileInsuranceView.routeName, arguments: SourceArguments(source: widget.source));
          } else {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_submit);
            Fluttertoast.showToast(msg: addCardNumber.item1, timeInSecForIosWeb: 3, fontSize: 14.0);
            Navigator.pop(context);
          }
        } else {
          setState(() {
            model3.setState(ViewState.Idle);
          });
          Fluttertoast.showToast(msg: profileInsuranceModel.errorMessage, timeInSecForIosWeb: 3, fontSize: 14.0);
        }
    }
  }

  String getProvinceStorage() {
    if (dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD) != null) {
      return dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD)!.toLowerCase();
    }
    if (dataStore.readString(DataStoreService.PROVINCE) != null) {
      return dataStore.readString(DataStoreService.PROVINCE)!.toLowerCase();
    } else {
      return '';
    }
  }
}
