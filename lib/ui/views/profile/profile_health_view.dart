
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/health_info.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_health_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_inputchiplist.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ProfileHealthView extends StatefulWidget {
  final Function? onSuccess;
  final bool? noPadding;
  final void Function()? onBack;
  final int? position;
  final bool? showStepperText;

  ProfileHealthView({Key? key, this.onBack, this.onSuccess, this.noPadding = false, this.showStepperText = false, this.position = 0}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProfileHealthState();
  }
}

class _ProfileHealthState extends BaseState<ProfileHealthView> {
  Key? counterSupplementsKey;
  Key? allergicMedsKey;
  bool allergy = false;
  bool showErrorAllergy = false;

  final FocusNode _allergyFnode = FocusNode();
  final FocusNode _vitaminsFnode = FocusNode();
  List<String> currentVitamins = [];
  List<String> currentAllergies = [];
  final TextEditingController _vitaminController = TextEditingController();
  final TextEditingController _allergyController = TextEditingController();

  @override
  void initState() {
    super.initState();
    counterSupplementsKey = UniqueKey();
    allergicMedsKey = UniqueKey();
    _allergyController.addListener(_printAllergy);
    _vitaminController.addListener(_printVitamin);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _allergyController.removeListener(_printAllergy);
    _vitaminController.removeListener(_printVitamin);
    _vitaminController.dispose();
    _allergyController.dispose();
    _allergyFnode.dispose();
    _vitaminsFnode.dispose();
    super.dispose();
  }

  _printAllergy() {
    print('Second text field: ${_allergyController.text}');
  }

  _printVitamin() {
    print('Second text field: ${_vitaminController.text}');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: widget.position! > 0
            ? () async {
                widget.onBack!;
                return true;
              }
            : () async {
                Navigator.pop(context);
                return true;
              },
        child: Consumer<ProfileHealthModel>(
          builder: (BuildContext context, ProfileHealthModel profileHealthModel, Widget? child) {
            return FutureBuilder(
              future: myFutureMethodOverall(profileHealthModel, context),
              // ignore: missing_return
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (profileHealthModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                  return NoInternetScreen(
                    onClickRetry: profileHealthModel.clearAsyncMemorizer,
                  );
                }

                if (snapshot.hasData == true && profileHealthModel.connectivityResult != ConnectivityResult.none) {
                  return _profileHealthBuild(context, profileHealthModel);
                } else if (snapshot.hasError && profileHealthModel.connectivityResult != ConnectivityResult.none) {
                  FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                  return ErrorScreen();
                }

                if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                  return LoadingScreen();
                }
                return Container();
              },
            );
          },
        ));
  }

  Future myFutureMethodOverall(ProfileHealthModel profileHealthModel, BuildContext context) async {
    Future<HealthInfo?> future1 = profileHealthModel.fetchHealthData(Provider.of<DashboardModel>(context).selectedPatientId); // will take 1 sec
    Future<Map<String, dynamic>?> future2 = profileHealthModel.getLocalization(["order-checkout", "common", "landing"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  void initialiseController(ProfileHealthModel profileHealthModel) {
    _vitaminController.text =
        (profileHealthModel.healthInfo!.vitamins != null && profileHealthModel.healthInfo!.vitamins!.length > 0) ? profileHealthModel.healthInfo!.vitamins!.join(",") : "";
    _allergyController.text =
        (profileHealthModel.healthInfo!.allergies != null && profileHealthModel.healthInfo!.allergies!.length > 0) ? profileHealthModel.healthInfo!.allergies!.join(",") : "";
  }

  //// ${PatientUtils.getYouOrName(Provider.of<DashboardModel>(context).selectedPatient)} like to receive ${PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context).selectedPatient)}  ${StringUtils.capitalize(PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context).selectedPatient))}?",
  Widget _profileHealthBuild(BuildContext context, ProfileHealthModel profileHealthModel) {
    /* PPInputChipList allergiesChipList;
    PPInputChipList vitaminsChipList;
    allergiesChipList = PPInputChipList(
        key: allergicMedsKey, chips: profileHealthModel.healthInfo!.allergies == null ? [] : profileHealthModel.healthInfo!.allergies, totalList: ViewConstants.ALLERGIES);
    vitaminsChipList =
        PPInputChipList(key: counterSupplementsKey, chips: profileHealthModel.healthInfo!.vitamins == null ? [] : profileHealthModel.healthInfo!.vitamins, totalList: []);*/
    initialiseController(profileHealthModel);
    _vitaminController.selection = TextSelection.fromPosition(TextPosition(offset: _vitaminController.text.length));
    _allergyController.selection = TextSelection.fromPosition(TextPosition(offset: _allergyController.text.length));
    return Scaffold(
        bottomNavigationBar: Builder(
            builder: (BuildContext context) => profileHealthModel.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : PPBottomBars.getButtonedBottomBar(
                    child: widget.onSuccess != null
                        ? getSaveButtons(profileHealthModel, profileHealthModel.healthInfo!.allergies, profileHealthModel.healthInfo!.vitamins)
                        : getBottomButtom(profileHealthModel, profileHealthModel.healthInfo!.allergies, profileHealthModel.healthInfo!.vitamins))),
        body: GestureDetector(
          onTap: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          child: Padding(
            padding: widget.noPadding == true ? EdgeInsets.all(0) : EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: MEDIUM),
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-health")),
                    PPUIHelper.verticalSpaceXSmall(),
                    Text(
                      getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
                      style: MEDIUM_XX_SECONDARY,
                    ),
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPRadioGroup(
                      radioOptions: [
                        LocalizationUtils.getSingleValueString("common", "common.labels.yes").toUpperCase(),
                        LocalizationUtils.getSingleValueString("common", "common.labels.no").toUpperCase()
                      ],
                      initialValue: profileHealthModel.radioOption > 0
                          ? LocalizationUtils.getSingleValueString("common", "common.labels.yes").toUpperCase()
                          : LocalizationUtils.getSingleValueString("common", "common.labels.no").toUpperCase(),
                      labelText: getLabelText(context),
                      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
                      onChange: (String value) {
                        if (value == LocalizationUtils.getSingleValueString("common", "common.labels.no").toUpperCase()) {
                          profileHealthModel.setRadioOption(0);
                        } else {
                          if (allergy == false) {
                            allergy = true;
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.health_allergies_yes);
                          }
                          profileHealthModel.setRadioOption(1);
                        }
                      },
                    ),
                    PPUIHelper.verticalSpaceSmall(),
                    profileHealthModel.radioOption > 0
                        ? Column(
                            children: <Widget>[
                              PPTexts.getFormLabel(getMedicationText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient))),
                              PPUIHelper.verticalSpaceMedium(),
                              PPFormFields.getTextField(
                                key: Key("allergies"),
                                  controller: _allergyController,
                                  //initialValue: profileHealthModel.healthInfo!.allergies!.join(","),
                                  autovalidate: showErrorAllergy,
                                  minLength: 1,
                                  textInputAction: TextInputAction.next,
                                  focusNode: _allergyFnode,
                                  onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                                  decoration: PPInputDecor.getDecoration(
                                      labelText: LocalizationUtils.getSingleValueString("landing", "landing.telehealth.allergies"),
                                      hintText: LocalizationUtils.getSingleValueString("landing", "landing.telehealth.allergies")),
                                  onTextChanged: (value) {
                                  }),
                              SizedBox(
                                height: REGULAR_X,
                              ),
                            ],
                          )
                        : Container(),
                    PPTexts.getFormLabel(getSupplementsText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient))),
                    PPUIHelper.verticalSpaceMedium(),
                    PPFormFields.getTextField(
                      key: Key("meds and vitamins"),
                        controller: _vitaminController,
//                        initialValue: (profileHealthModel.healthInfo!.vitamins != null && profileHealthModel.healthInfo!.vitamins!.length > 0)
//                            ? profileHealthModel.healthInfo!.vitamins!.join(",")
//                            : " ",
                        autovalidate: false,
                        minLength: 1,
                        textInputAction: TextInputAction.next,
                        focusNode: _vitaminsFnode,
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString("common", "common.navbar.meds"),
                            hintText: LocalizationUtils.getSingleValueString("common", "common.navbar.meds")),
                        onTextChanged: (value) {}),
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPUIHelper.verticalSpaceLarge(),
                    PPUIHelper.verticalSpaceLarge(),
                    PPUIHelper.verticalSpaceLarge(),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  void getAllergies(ProfileHealthModel profileHealthModel) {
    if (_allergyController.text != null && _allergyController.text.length > 0) {
      List<String> allergy = _allergyController.text.split(",");
      currentAllergies.clear();
      currentAllergies.addAll(allergy);
      profileHealthModel.healthInfo!.allergies = currentAllergies;
    }else {
      profileHealthModel.healthInfo!.allergies = [];
    }
  }

  void getVitamins(ProfileHealthModel profileHealthModel) {
    if (_vitaminController.text != null && _vitaminController.text.length > 0) {
      List<String> vitamins = _vitaminController.text.split(",");
      currentVitamins.clear();
      currentVitamins.addAll(vitamins);
      profileHealthModel.healthInfo!.vitamins = currentVitamins;
    }else {
      profileHealthModel.healthInfo!.vitamins = [];
    }
  }

  Widget getSaveButtons(ProfileHealthModel profileHealthModel, List<String>? allergiesChipList, List<String>? vitaminsChipList) {
    Widget withoutStepper = PrimaryButton(
      key: Key("healthViewContinue"),
      text: LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase(),
      fullWidth: false,
      onPressed: () {
        getAllergies(profileHealthModel);
        getVitamins(profileHealthModel);
        if(allergy && profileHealthModel.healthInfo!.allergies!.isEmpty){
          setState(() {
            showErrorAllergy = true;
          });
        }else {
          onClickSave(profileHealthModel, profileHealthModel.healthInfo!.allergies, profileHealthModel.healthInfo!.vitamins);
        }
      },
    );
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SecondaryButton(
          key: Key("healthViewBack"),
          text: LocalizationUtils.getSingleValueString("common", "common.button.back").toUpperCase(),
          onPressed: widget.onBack,
        ),
        SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
        withoutStepper
      ],
    );
  }

  Widget getBottomButtom(ProfileHealthModel profileHealthModel, List<String>? allergiesChipList, List<String>? vitaminsChipList) {
    return PrimaryButton(
      key: Key("save"),
      text: LocalizationUtils.getSingleValueString("common", "common.button.save").toUpperCase(),
      fullWidth: true,
      onPressed: () {
        getAllergies(profileHealthModel);
        getVitamins(profileHealthModel);
        if(allergy && profileHealthModel.healthInfo!.allergies!.isEmpty){
          setState(() {
            showErrorAllergy = true;
          });
        }else {
          setState(() {
            showErrorAllergy = false;
          });
          onClickSave(profileHealthModel, profileHealthModel.healthInfo!.allergies, profileHealthModel.healthInfo!.vitamins);
        }
      },
    );
  }

  void onClickSave(ProfileHealthModel profileHealthModel, List<String>? allergiesList, List<String>? vitaminsList) async {
    bool connectivityResult = await profileHealthModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profileHealthModel.noInternetConnection);
      return;
    }
    //String f = allergiesChipList.getCurrentSelection();
    bool? res = await profileHealthModel.setPatientHealth(profileHealthModel.radioOption > 0 ? allergiesList : [], vitaminsList);
    profileHealthModel.clearAsyncMemorizer();
    if (res == true) {
      if (widget.onSuccess == null) showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.details-updated"));
      if (widget.onSuccess != null) widget.onSuccess!();
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.health_proceed);
    } else
      onFail(context, errMessage: profileHealthModel.errorMessage);
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-health-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-health-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-health-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-health");
    }
  }

  String getMedicationText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.medications-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.medications-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.medications-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.medications");
    }
  }

  String getSupplementsText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.supplements-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.supplements-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.supplements-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.supplements");
    }
  }

  String getLabelText(BuildContext context) {
    if (Provider.of<DashboardModel>(context).selectedPatient == null || Provider.of<DashboardModel>(context).selectedPatient!.primary == true) {
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.allergies");
    } else {
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.labels.allergies-secondary")
          .replaceAll("{{name}}", PatientUtils.getThey(Provider.of<DashboardModel>(context).selectedPatient));
    }
  }
}
