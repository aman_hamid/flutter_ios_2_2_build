import 'dart:async';

import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/address.dart';
import 'package:pocketpills/core/response/address/address_complete.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_address_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class AddressDetailWidget extends StatefulWidget {
  static const routeName = 'addressdetail';
  final Address? address;
  Function? updateList;
  final Function? onSuccess;
  final void Function()? onBack;
  BaseStepperSource? source;
  BaseStepperSource? from;

  AddressDetailWidget({Key? key, this.address, this.updateList, this.onSuccess, this.onBack, this.source, this.from}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddressDetailState();
  }
}

class AddressDetailState extends BaseState<AddressDetailWidget> {
  bool autovalidate = false;

  String? dropDownError;
  String? dropDownValue;
  bool defaultValue = true;
  String? title;
  Address? address;
  String? stAddressError;
  final _formKey = GlobalKey<FormState>();
  AutoCompleteTextField? stAddressSearchTextField;
  List<PinCodeSuggestion> pinCodeSuggestion = [];

  GlobalKey<AutoCompleteTextFieldState<PinCodeSuggestion>> key = new GlobalKey();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _stController = TextEditingController();
  TextEditingController _st2Controller = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _postalCodeController = TextEditingController();

  final FocusNode _nameFnode = FocusNode();
  final FocusNode _stFnode = FocusNode();
  final FocusNode _st2Fnode = FocusNode();
  final FocusNode _cityFnode = FocusNode();
  final FocusNode _postalFnode = FocusNode();

  String? prevPostalCodeName;
  ProfileAddressModel? _profileAddressModel;

  @override
  void initState() {
    super.initState();
    title = "Add Address";
    if (widget.address != null) {
      address = widget.address;
      defaultValue = address!.isDefault!;
      (widget.source != null) ? _nameController = TextEditingController(text: "") : _nameController = TextEditingController(text: address!.nickname);
      _stController = TextEditingController(text: address?.streetAddress);
      _st2Controller = TextEditingController(text: address?.streetAddressLineTwo);
      _cityController = TextEditingController(text: address?.city);
      _postalCodeController = TextEditingController(text: address?.postalCode);
      dropDownValue = address?.province.toString();
      title = "Edit Address";
    }
    _postalCodeController.addListener(pharmacyControllerListener);
    _stController.addListener(streetControllerListener);

    if (kDebugMode && isIntegration && widget.address == null) {
      WidgetsBinding.instance!.addPostFrameCallback((_) => {
            Future.delayed(const Duration(milliseconds: 1000), () {
              _stFnode.requestFocus();
              _stController.text = "canad";
              Future.delayed(const Duration(milliseconds: 800), () async {
                submitSuggestion("CA|CP|B|19943206", _profileAddressModel);
              });
            })
          });
    }
  }

  @override
  void dispose() {
    _postalCodeController.removeListener(pharmacyControllerListener);
    _stController.removeListener(streetControllerListener);
    super.dispose();
  }

  pharmacyControllerListener() {
    //getSearchResponse(_postalCodeController.text);
  }

  streetControllerListener() {
    if (_postalCodeController.text.isNotEmpty) {
      getSearchResponse(_postalCodeController.text + " " + _stController.text);
    } else {
      getSearchResponse(_stController.text);
    }
  }

  getSearchResponse(String keyword) async {
    if (_profileAddressModel != null && keyword.isNotEmpty) {
      pinCodeSuggestion = (await _profileAddressModel!.searchPostalCode(keyword))!;
      try {
        stAddressSearchTextField!.updateSuggestions(pinCodeSuggestion);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }
  }

  Widget getAutoCompleteItem(PinCodeSuggestion item) {
    return Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 4), child: PPTexts.getTertiaryHeading(item.text ?? "", isBold: true));
  }

  Widget? getAutoCompleteField(BuildContext context, ProfileAddressModel profileAddressModel) {
    stAddressSearchTextField = AutoCompleteTextField<PinCodeSuggestion>(
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Bold"),
      keyboardType: TextInputType.visiblePassword,
      controller: _stController,
      suggestions: _stFnode.hasFocus == true ? pinCodeSuggestion : [],
      submitOnSuggestionTap: true,
      focusNode: _stFnode,
      onFocusChanged: (focus) {
        if (focus) {
        } else {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      },
      key: key,
      itemSorter: (a, b) => 0,
      itemFilter: (suggestion, input) => true,
      itemBuilder: (context, item) {
        return Row(
          children: <Widget>[
            Expanded(
              child: _stFnode.hasFocus
                  ? Container(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                        child: getAutoCompleteItem(item),
                      ),
                    )
                  : Container(),
            ),
          ],
        );
      },
      itemSubmitted: (PinCodeSuggestion item) => submitSuggestion(item.suggestionId ?? "", _profileAddressModel),
      decoration: PPInputDecor.getDecoration(
        labelText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
        hintText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
        floatingLabelBehavior: FloatingLabelBehavior.never,
      ),
    );

    return stAddressSearchTextField;
  }

  submitSuggestion(String suggestionId, ProfileAddressModel? profileAddressModel) async {
    AddressComplete? _addressComplete = await profileAddressModel!.getCompleteAddress(suggestionId);
    if (_addressComplete != null && (_stController.text.indexOf("#") == -1 || _st2Controller.text.indexOf("#") == -1)) {
      _stController.text = _addressComplete.streetAddress ?? "";
      _st2Controller.text = _addressComplete.streetAddressLineTwo ?? "";
      _cityController.text = _addressComplete.city ?? "";
      _postalCodeController.text = _addressComplete.postalCode!.replaceAll(" ", "");
      setState(() {
        (_addressComplete.province != null && _addressComplete.province != "null") ? dropDownValue = _addressComplete.province : null;
      });
    }
    /*else if (_stController.text.indexOf("#") != -1 || _st2Controller.text.indexOf("#") != -1) {
      showErrorOverlay();
    }*/
  }

/*  void showErrorOverlay({String msg = ""}) {
    OverlayEntry? overlayEntry;
    OverlayState? overlayState = Overlay.of(context);
    if (overlayState != null) {
      overlayEntry = OverlayEntry(
          builder: (_) {
            return Positioned(
              bottom: MediaQuery.of(context).size.height * 0.1 + 10,
              left: MediaQuery.of(context).size.width / 2 - 100,
              child: GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (currentFocus != _stFnode) {
                    currentFocus.requestFocus(_stFnode);
                  }
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.grey.shade200,
                        Colors.grey.shade400,
                      ],
                    ),
                    border: Border.all(color: Colors.black, width: 0),
                    borderRadius:
                        BorderRadius.only(topRight: Radius.circular(100), bottomLeft: Radius.circular(100), topLeft: Radius.circular(100), bottomRight: Radius.circular(100)),
                  ),
                  child: msg.isEmpty
                      ? Text(
                          "Remove # from address field",
                          style: TextStyle(color: Colors.black, fontSize: 12, fontFamily: "FSJoeyPro Medium", decoration: TextDecoration.none),
                        )
                      : Text(
                          msg,
                          style: TextStyle(color: Colors.black, fontSize: 12, fontFamily: "FSJoeyPro Medium", decoration: TextDecoration.none),
                        ),
                ),
              ),
            );
          },
          opaque: false);
      overlayState.insert(overlayEntry);
      Timer(Duration(seconds: 3), () {
        if (overlayEntry != null) {
          overlayEntry.remove();
        }
      });
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ProfileAddressModel(),
      child: Consumer<ProfileAddressModel>(
        builder: (BuildContext context, ProfileAddressModel profileAddressModel, Widget? child) {
          _profileAddressModel = profileAddressModel;
          return FutureBuilder(
              future: profileAddressModel.getLocalization(["address"]),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getAllView();
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Widget getAllView() {
    if (widget.source == BaseStepperSource.ORDER_STEPPER || widget.source == BaseStepperSource.EDIT_PROFILE) {
      return getMainView(_profileAddressModel!);
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: InnerAppBar(
        titleText: widget.address == null
            ? LocalizationUtils.getSingleValueString("address", "address.labels.add-address")
            : LocalizationUtils.getSingleValueString("address", "address.labels.edit-address"),
        appBar: AppBar(),
      ),
      body: GestureDetector(
        onTap: () {
          this.pinCodeSuggestion = [];
        },
        child: Builder(
          builder: (BuildContext context) => Container(
            child: Builder(
              builder: (BuildContext context) {
                return getMainView(_profileAddressModel!);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget getMainView(ProfileAddressModel profileAddressModel) {
    return GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: Stack(
          key: Key("Address add or edit view"),
          children: <Widget>[
            Form(
              key: _formKey,
              child: ListView(
                primary: false,
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: REGULAR, left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: SMALL_XXX),
                    child: Column(
                      children: <Widget>[
                        PPUIHelper.verticalSpaceMedium(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                                child: Text(
                              LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-add-address"),
                              style: REGULAR_XXX_PRIMARY_BOLD,
                              textAlign: TextAlign.start,
                            )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: MEDIUM_XXX, top: SMALL_X),
                          child: Text(
                            getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
                            style: MEDIUM_XX_SECONDARY,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.fields.name"),
                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPFormFields.getTextField(
                        key: Key("addressName"),
                        autovalidate: autovalidate,
                        minLength: 1,
                        controller: _nameController,
                        textInputAction: TextInputAction.next,
                        focusNode: _nameFnode,
                        onErrorStr: LocalizationUtils.getSingleValueString("address", "address.fields.required-error"),
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString("address", "address.fields.name-input"),
                            hintText: LocalizationUtils.getSingleValueString("address", "address.fields.name-input"),
                            helperText: "",
                            floatingLabelBehavior: FloatingLabelBehavior.never),
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _nameFnode, _postalFnode);
                        }),
                  ),
                  PPUIHelper.verticalSpaceXMedium(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPFormFields.getTextField(
                      key: Key("addressPin"),
                      autovalidate: autovalidate,
                      textCapitalization: TextCapitalization.characters,
                      minLength: 1,
                      textInputAction: TextInputAction.done,
                      controller: _postalCodeController,
                      focusNode: _postalFnode,
                      validator: (value) {
                        RegExp exp = new RegExp(r"^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$");
                        bool val = exp.hasMatch(value.toString().toUpperCase());
                        if (val == true) return null;
                        return value.toString().isEmpty
                            ? LocalizationUtils.getSingleValueString("address", "address.fields.required-error")
                            : LocalizationUtils.getSingleValueString("address", "address.fields.postalcode-error");
                      },
                      decoration: PPInputDecor.getDecoration(
                          labelText: LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                          hintText: LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                          helperText: "",
                          floatingLabelBehavior: FloatingLabelBehavior.never),
                      onFieldSubmitted: (value) {
                        _fieldFocusChange(context, _nameFnode, _stFnode);
                      },
                    ),
                  ),
                  PPUIHelper.verticalSpaceXMedium(),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.street"),
                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: getAutoCompleteField(context, profileAddressModel),
                  ),
                  stAddressError == null
                      ? SizedBox(height: 2)
                      : Padding(
                          padding: EdgeInsets.fromLTRB(4, 0, 0, 0),
                          child: PPTexts.getFormError(stAddressError!, color: errorColor),
                        ),
                  PPUIHelper.verticalSpaceXMedium(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPFormFields.getTextField(
                        autovalidate: autovalidate,
                        minLength: 1,
                        textInputAction: TextInputAction.next,
                        controller: _st2Controller,
                        focusNode: _st2Fnode,
                        validator: (value) {
                          if (value !=null && value.isNotEmpty && value.contains("#")) {
                            return LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
                                ? "Special character not allowed"
                                : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
                          } else {
                            return null;
                          }
                        },
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString("address", "address.label.line2"),
                            hintText: LocalizationUtils.getSingleValueString("address", "address.label.line2"),
                            helperText: "",
                            floatingLabelBehavior: FloatingLabelBehavior.never),
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _st2Fnode, _cityFnode);
                        }),
                  ),
                  PPUIHelper.verticalSpaceXMedium(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.city"),
                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPFormFields.getTextField(
                        autovalidate: autovalidate,
                        minLength: 1,
                        textInputAction: TextInputAction.next,
                        focusNode: _cityFnode,
                        controller: _cityController,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
                          } else if (value.isNotEmpty && value.contains("#")) {
                            return LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
                                ? "Special character not allowed"
                                : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
                          } else {
                            return null;
                          }
                        },
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString("address", "address.label.city"),
                            hintText: LocalizationUtils.getSingleValueString("address", "address.label.city"),
                            helperText: "",
                            floatingLabelBehavior: FloatingLabelBehavior.never),
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _cityFnode, _postalFnode);
                        }),
                  ),
                  PPUIHelper.verticalSpaceXMedium(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                    child: PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.province"),
                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  getDropDown(),
                  dropDownError == null
                      ? SizedBox(height: 20)
                      : Padding(padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium), child: PPTexts.getFormError(dropDownError!, color: errorColor)),
                  CheckboxListTile(
                    title: Text(
                      LocalizationUtils.getSingleValueString("address", "address.label.mark-default"),
                      style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                    ),
                    value: defaultValue,
                    activeColor: brandColor,
                    controlAffinity: ListTileControlAffinity.leading,
                    onChanged: (bool? value) => setState(() {
                      defaultValue = !defaultValue;
                    }),
                  ),
                  (checkOrderStepperTbcAddress())
                      ? PPContainer.emptyContainer()
                      : profileAddressModel.state == ViewState.Busy
                          ? ViewConstants.progressIndicator
                          : getButton(context, profileAddressModel),
                  (checkOrderStepperTbcAddress() && widget.from == null)
                      ? Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: PPUIHelper.HorizontalSpaceMedium),
                          child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                            SecondaryButton(
                              key: Key("addAddressBack"),
                              text: LocalizationUtils.getSingleValueString("common", "common.button.back").toUpperCase(),
                              onPressed: widget.onBack!,
                            ),
                            SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                            PrimaryButton(
                              key: Key("addAddressContinue"),
                              text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                              onPressed: () {
                                onAddressPressed(context, profileAddressModel);
                              },
                            )
                          ]),
                        )
                      : PPContainer.emptyContainer(),
                  (checkOrderStepperTbcAddress() && widget.from != null)
                      ? Container(
                          height: 80,
                          child: VitaminsBottomSheet(
                            onBack: () {
                              analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_address_back);
                              if (widget.onBack != null) widget.onBack!();
                            },
                            onContiune: () {
                              onAddressPressed(context, profileAddressModel);
                            },
                            stepNumber: 0,
                          ),
                        )
                      : PPContainer.emptyContainer(),
                  (checkOrderStepperTbcAddress() && widget.from != null && widget.onBack != null)
                      ? Container()
                      : SizedBox(
                          height: MediaQuery.of(context).size.height * 0.2,
                        ),
                ],
              ),
            ),
          ],
        ));
  }

  bool checkOrderStepperTbcAddress() => widget.source != null && widget.source == BaseStepperSource.ORDER_STEPPER;

  getButton(context, model) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: PrimaryButton(
        key: Key("saveAddress"),
        fullWidth: true,
        text: LocalizationUtils.getSingleValueString("address", "address.button.save"),
        onPressed: () {
          onAddressPressed(context, model);
        },
      ),
    );
  }

  bool isFormValid() {
    return dropDownValue != null;
  }

  void onAddressPressed(BuildContext context, ProfileAddressModel profileAddressModel) async {
    bool connectivityResult = await profileAddressModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profileAddressModel.noInternetConnection);
      return;
    }
    if (_stController.text.toString().isEmpty) {
      setState(() {
        stAddressError = LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
      });
      return;
    } else if (_stController.text.indexOf("#") != -1) {
      //showErrorOverlay();
      setState(() {
        stAddressError = LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
            ? "Special character not allowed"
            : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
      });
      return;
    } else {
      stAddressError = null;
    }

    if (_formKey.currentState!.validate() && isFormValid()) {
      bool success = false;
      if (address == null) {
        success = await profileAddressModel.addPatientAddress(
            name: _nameController.text,
            sal1: _stController.text,
            sal2: _st2Controller.text == "" ? null : _st2Controller.text,
            city: _cityController.text,
            province: dropDownValue,
            isDefault: defaultValue,
            postalCode: _postalCodeController.text.toUpperCase());
      } else {
        success = await profileAddressModel.editPatientAddress(
            addressId: address!.id,
            name: _nameController.text,
            sal1: _stController.text,
            sal2: _st2Controller.text == "" ? null : _st2Controller.text,
            city: _cityController.text,
            province: dropDownValue,
            isDefault: defaultValue,
            postalCode: _postalCodeController.text.toUpperCase());
      }
      if (success == true) {
        if (widget.source != null && (widget.source == BaseStepperSource.ORDER_STEPPER || widget.source == BaseStepperSource.EDIT_PROFILE)) {
          widget.updateList!();
          (widget.onSuccess != null) ? widget.onSuccess!() : null;
        } else {
          Navigator.pop(context, true);
        }
      } else
        onFail(context, errMessage: profileAddressModel.errorMessage);
    } else if (dropDownValue == null)
      setState(() {
        dropDownError = LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
        autovalidate = true;
      });
    else
      setState(() {
        autovalidate = true;
      });
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
        child: PPFormFields.getDropDown(
          labelText: LocalizationUtils.getSingleValueString("address", "address.field.select-province"),
          fullWidth: true,
          value: getDropDownValue(dropDownValue),
          items: getStateMap()
              .map((state) => DropdownMenuItem<String>(
                  value: state["value"],
                  child: Text(
                    state["value"]!,
                    style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                  )))
              .toList(),
          onChanged: (selectedItem) => setState(() {
            dropDownValue = getServerSendingValue(selectedItem!);
            setState(() {
              dropDownError = null;
            });
          }),
        ),
      );
    else
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
        child: PPFormFields.getDropDown(
          labelText: LocalizationUtils.getSingleValueString("address", "address.field.select-province"),
          fullWidth: true,
          value: getDropDownValue(dropDownValue),
          isError: true,
          items:
              getStateMap().map((state) => DropdownMenuItem<String>(value: state["value"], child: Text(state["value"]!, style: TextStyle(fontFamily: "FSJoeyPro Bold")))).toList(),
          onChanged: (selectedItem) => setState(() {
            dropDownValue = getServerSendingValue(selectedItem!);
            setState(() {
              dropDownError = null;
            });
          }),
        ),
      );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address");
    }
  }
}
