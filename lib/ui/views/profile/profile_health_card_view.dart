import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_insurance_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/permission_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/copay/copay_request_success_screen.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/empty_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/full_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_insurance_view.dart';
import 'package:pocketpills/ui/views/profile/health_card_number_bottom_sheet.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/permission_utils.dart';
import 'package:pocketpills/utils/rotate_compress_image.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../locator.dart';

class ProfileHealthCardView extends BaseStatelessWidget {
  static const routeName = 'profileHealthCardView';
  final Function? onSuccess;
  final Function? onBack;
  final bool? noPadding;
  final int? position;
  BaseStepperSource? source;
  final DataStoreService dataStore = locator<DataStoreService>();

  ProfileHealthCardView({Key? key, this.onBack, this.noPadding = false, this.onSuccess, this.source, this.position = 0}) : super(key: key);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool frontPrimary = false, backPrimary = false, frontSec = false, backSec = false, frontTert = false, backTert = false, frontQuat = false, backQuat = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: position! > 0
            ? () async {
                onBack!;
                return true;
              }
            : () async {
                Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false);
                return true;
              },
        child: MultiProvider(
          providers: [ChangeNotifierProvider<ProfileInsuranceModel>(create: (_) => ProfileInsuranceModel())],
          child: Consumer<ProfileInsuranceModel>(
            builder: (BuildContext context, ProfileInsuranceModel profileInsuranceModel, Widget? child) {
              return FutureBuilder(
                future: myFutureMethodOverall(profileInsuranceModel, context),
                // ignore: missing_return
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (profileInsuranceModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: profileInsuranceModel.clearAsyncMemorizer,
                    );
                  }

                  if (snapshot.hasData == true && profileInsuranceModel.connectivityResult != ConnectivityResult.none) {
                    print("from profileHealth card ${dataStore.readString(DataStoreService.PROVINCE)}");
                    return _profileInsuranceBuild(context, profileInsuranceModel, profileInsuranceModel.curInsurance!);
                  } else if (snapshot.hasError && profileInsuranceModel.connectivityResult != ConnectivityResult.none) {
                    FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }
                  return Container();
                },
              );
            },
          ),
        ));
  }

  Future myFutureMethodOverall(ProfileInsuranceModel profileInsuranceModel, BuildContext context) async {
    Future<Insurance?> future1 = profileInsuranceModel.fetchInsuranceData(Provider.of<DashboardModel>(context).selectedPatientId); // will take 1 sec
    Future<Map<String, dynamic>?> future2 = profileInsuranceModel.getLocalization(["insurance", "order-checkout"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget getUploadInsuranceView(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PPUIHelper.verticalSpaceMedium(),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
                child: Text(
              getHeaderText(PatientUtils.getForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
              style: REGULAR_XXX_PRIMARY_BOLD,
              textAlign: TextAlign.start,
            )),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: MEDIUM_XXX),
          child: Text(
            getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
            style: MEDIUM_XX_SECONDARY,
          ),
        ),
        getProvincialCard(context, profileInsuranceModel),
        Padding(
          padding: const EdgeInsets.only(right: MEDIUM_XXX, bottom: REGULAR_XXX),
          child: GestureDetector(
            onTap: () {
              analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_cancel_subscription);
              LocalizationUtils.isProvinceQuebec()
                  ? launch("tel://" + ViewConstants.PHARMACY_PHONE_QC.toString())
                  : launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
            },
            child: Text(
              LocalizationUtils.isProvinceQuebec()
                  ? LocalizationUtils.getSingleValueString("insurance", "insurance.help.note") + " 1-855-774-0104"
                  : LocalizationUtils.getSingleValueString("insurance", "insurance.help.note") + " 1-855-950-7225",
              style: MEDIUM_XX_SECONDARY,
            ),
          ),
        ),
        PPUIHelper.verticalSpaceMedium(),
      ],
    );
  }

  Widget getAddInsuranceButton(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    return profileInsuranceModel.allInsuranceAvailable == true
        ? Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                key: Key("add more insurance"),
                onTap: () {
                  if (profileInsuranceModel.secondaryInsurance == false) {
                    profileInsuranceModel.secondaryInsurance = true;
                  } else if (profileInsuranceModel.tertiaryInsurance == false) {
                    profileInsuranceModel.tertiaryInsurance = true;
                  } else if (profileInsuranceModel.quaternaryInsurance == false) {
                    profileInsuranceModel.quaternaryInsurance = true;
                    profileInsuranceModel.allInsuranceAvailable = false;
                  } else {
                    profileInsuranceModel.allInsuranceAvailable = false;
                  }
                },
                child: Container(
                    child: DottedBorder(
                        dashPattern: [6, 4],
                        borderType: BorderType.RRect,
                        strokeWidth: 1,
                        radius: Radius.circular(SMALL_XXX),
                        color: linkColor,
                        child: Container(
                            width: MediaQuery.of(context).size.width - 64,
                            child: Padding(
                              padding: const EdgeInsets.only(top: MEDIUM_XXX, bottom: MEDIUM_XXX),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.add_circle_outline,
                                    color: linkColor,
                                  ),
                                  Text(
                                    LocalizationUtils.getSingleValueString("insurance", "insurance.labels.new-insurance"),
                                    style: MEDIUM_XXX_LINK_BOLD_MEDIUM,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                            )))),
              )
            ],
          )
        : PPContainer.emptyContainer();
  }

  Widget _profileInsuranceBuild(BuildContext context, ProfileInsuranceModel profileInsuranceModel, Insurance insurance) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: (source != null && source == BaseStepperSource.COPAY_REQUEST)
            ? InnerAppBar(
                titleText: LocalizationUtils.getSingleValueString("insurance", "insurance.labels.copy-request"),
                appBar: AppBar(),
                leadingBackButton: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                })
            : null,
        bottomNavigationBar: getButtons(profileInsuranceModel),
        body: Builder(
          builder: (context) => Container(
              key: Key("profile_health_view"),
              child: Stack(children: <Widget>[
                SingleChildScrollView(
                  child: Padding(
                      padding: noPadding == true ? EdgeInsets.all(0) : EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: MEDIUM),
                      child: getUploadInsuranceView(context, profileInsuranceModel)),
                ),
                Center(
                  child: profileInsuranceModel.state != ViewState.Busy
                      ? Container()
                      : Container(height: double.infinity, width: double.infinity, color: Colors.white30, child: ViewConstants.progressIndicator),
                )
              ])),
        ));
  }

  Widget? getButtons(ProfileInsuranceModel profileInsuranceModel) {
    if (source != null && source == BaseStepperSource.HEALTH_CARD_ORDER_STEPPER_SCREEN) {
      return Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PrimaryButton(
                      key: Key("healthContinue"),
                      text: LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase(),
                      onPressed: () {
                        Insurance? ins = profileInsuranceModel.curInsurance;
                        if (ins == null || (ins.provincialInsuranceFrontImageOriginal == null && ins.provincialInsuranceBackImageOriginal == null)) {
                          showOnSnackBar(context,
                              successMessage: LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.card-mandatory"));
                        } else if (ins != null &&
                            (dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_") == "british_columbia" &&
                                ins.provincialInsuranceFrontImageOriginal != null &&
                                ins.provincialInsuranceBackImageOriginal == null)) {
                          showOnSnackBar(context,
                              successMessage: LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error").isEmpty
                                  ? "Your front and back health card images are mandatory for filling the prescription"
                                  : LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error"));
                        } else if (ins != null &&
                            (dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_") == "british_columbia" &&
                                ins.provincialInsuranceFrontImageOriginal == null &&
                                ins.provincialInsuranceBackImageOriginal != null)) {
                          showOnSnackBar(context,
                              successMessage: LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error").isEmpty
                                  ? "Your front and back health card images are mandatory for filling the prescription"
                                  : LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error"));
                        }
                        /*else if (ins != null &&
                            (dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_") == "manitoba" && (dataStore.readString(DataStoreService.PHN) == null || dataStore.readString(DataStoreService.PHN).toString().isEmpty) &&
                                (ins.provincialInsuranceFrontImageOriginal != null ||
                                ins.provincialInsuranceBackImageOriginal != null))) {
                          showHealthNumberBottomSheet(context, profileInsuranceModel);
                        }*/
                        else if (ins != null &&
                            (dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase().replaceAll(" ", "_") == "british_columbia" &&
                                ins.provincialInsuranceFrontImageOriginal != null &&
                                ins.provincialInsuranceBackImageOriginal != null)) {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_proceed);
                          onSuccess!();
                        } else if (ins != null && (ins.provincialInsuranceFrontImageOriginal != null || ins.provincialInsuranceBackImageOriginal != null)) {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_proceed);
                          onSuccess!();
                        }
                      },
                    )
                  ],
                ),
              ));
    } else if (source != null && source == BaseStepperSource.COPAY_REQUEST) {
      return Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                  child: Container(
                color: Colors.white,
                child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  PrimaryButton(
                    text: LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase(),
                    onPressed: () {
                      print(dataStore.readString(DataStoreService.PROVINCE).toString().toLowerCase() + "dis");
                      Navigator.pushNamed(context, ProfileInsuranceView.routeName, arguments: SourceArguments(source: source));
                    },
                  )
                ]),
              )));
    }
  }

  Widget getProvincialCard(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    List<Widget> children = [];
    Insurance ins = profileInsuranceModel.curInsurance!;
    children.add(PPUIHelper.verticalSpaceMedium());
    children.add(
      Container(
        height: 200,
        child: GridView.count(
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          crossAxisSpacing: PPUIHelper.HorizontalSpaceMedium,
          children: [
            GridTile(
                key: Key("first image"),
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthFrontImage"
                    ? ViewConstants.progressIndicator
                    : ins.provincialInsuranceFrontImage != null
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", ins.provincialInsuranceFrontImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", "PROVINCIAL")),
            GridTile(
                key: Key("second image"),
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthBackImage"
                    ? ViewConstants.progressIndicator
                    : ins.provincialInsuranceBackImage != null
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", ins.provincialInsuranceBackImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", "PROVINCIAL"))
          ],
        ),
      ),
    );
    return Container(
      child: Column(
        children: children,
      ),
    );
  }

  void revealBottonSheet(
    BuildContext contexts,
    ProfileInsuranceModel profileInsuranceModel,
    String source,
  ) {
    List<Widget> tiles = [];
    showModalBottomSheet<void>(
      context: contexts,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
            ),
            PPDivider(),
            ListTile(
              key: Key("open camera"),
              leading: new Icon(
                Icons.photo_camera,
                color: primaryColor,
              ),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                if (successCamera == true) {
                  Navigator.pop(contexts);
                  getImage(contexts, profileInsuranceModel, source, ImageSource.camera);
                } else {
                  PermissionDialog.show(contexts, "camera");
                }
              },
            ),
            ListTile(
              key: Key("open gallery"),
              leading: new Icon(Icons.photo_library, color: primaryColor),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                PermissionStatus status = await Permission.photos.status;
                bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
                if (successPhotos == true || status.isLimited) {
                  Navigator.pop(contexts);
                  getImage(contexts, profileInsuranceModel, source, ImageSource.gallery);
                } else {
                  Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
                }
              },
            ),
            tiles.length > 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      PPDivider(),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"),
                            style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                      ),
                    ],
                  )
                : SizedBox(
                    height: 0.0,
                  ),
            Column(
              children: tiles,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }

  getFullImageContainer(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, String? filePath) {
    return FullImageContainer(
      image: CachedNetworkImageProvider(
        filePath!,
      ),
      onPressed: () {
        fireEvent(source);
        Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: CachedNetworkImageProvider(filePath))));
        //Image.file(filePath);
      },
      onDelete: deleteImage(context, profileInsuranceModel, source),
      onEdit: () {
        if (profileInsuranceModel.healthRetryCount == 2) {
          showHealthNumberBottomSheet(context, profileInsuranceModel);
        } else {
          revealBottonSheet(context, profileInsuranceModel, source);
        }
      },
      healthCard: true,
      imageKey: source == "provincialInsuranceHealthFrontImage" ? 1.toString() : 2.toString(),
    );
  }

  deleteImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source) {
    return () async {
      profileInsuranceModel.deleteImage(source);
    };
  }

  Future<void> getImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, var imgSource) async {
    try {
      ImagePicker imagePicker = ImagePicker();
      File image;
      var _image = await imagePicker.getImage(source: imgSource);
      if (_image == null) return;
      image = File(_image.path);
      var finalImage = await RotateAndCompressImage().rotateAndCompressAndSaveImage(image);
      if (finalImage == null) return;

      bool connectivityResult = await profileInsuranceModel.isInternetConnected();
      if (connectivityResult == false) {
        onFail(context, errMessage: profileInsuranceModel.noInternetConnection);
        return;
      }

      var res = await profileInsuranceModel.addHealthImage(finalImage, source);
      if (res != null && res != "") {
        if (res.item2 == false) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_digitize_failed);
          if (profileInsuranceModel.healthRetry == false) {
            Fluttertoast.showToast(
              msg: res.item1,
              timeInSecForIosWeb: 3,
              fontSize: 14.0,
              gravity: ToastGravity.SNACKBAR,
            );
            profileInsuranceModel.healthRetry = true;
          } else if (profileInsuranceModel.healthRetry = true) {
            Fluttertoast.showToast(msg: res.item1, timeInSecForIosWeb: 3, fontSize: 14.0);
            showHealthNumberBottomSheet(context, profileInsuranceModel);
          }
        } else {
          try {
            _scaffoldKey.currentState!.showSnackBar(SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(
                res.item1,
                style: TextStyle(fontFamily: "FSJoeyPro Medium"),
              ),
              duration: Duration(seconds: 3),
            ));
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
        }
      }
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  void fireEvent(String source) {
    switch (source) {
      case "primaryInsuranceFrontImage":
        if (frontPrimary == false) {
          frontPrimary = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_fr_click);
        }
        break;
      case "primaryInsuranceBackImage":
        if (backPrimary == false) {
          backPrimary = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_bk_click);
        }
        break;
      case "secondaryInsuranceFrontImage":
        if (frontSec == false) {
          frontSec = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_fr_click);
        }
        break;
      case "secondaryInsuranceBackImage":
        if (backSec == false) {
          backSec = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_bk_click);
        }
        break;
      case "tertiaryInsuranceFrontImage":
        if (frontTert == false) {
          frontTert = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_fr_click);
        }
        break;
      case "tertiaryInsuranceBackImage":
        if (backTert == false) {
          backTert = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_bk_click);
        }
        break;
      case "quaternaryInsuranceFrontImage":
        if (frontQuat == false) {
          frontQuat = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_fr_click);
        }
        break;
      case "quaternaryInsuranceBackImage":
        if (backQuat == false) {
          backQuat = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.insurance_bk_click);
        }
        break;
    }
  }

  getEmptyContainer(BuildContext contextm, ProfileInsuranceModel profileInsuranceModel, String source, String sourceType) {
    return EmptyImageContainer(
      onTap: () async {
        if (profileInsuranceModel.healthRetryCount == 2) {
          showHealthNumberBottomSheet(contextm, profileInsuranceModel);
        } else {
          fireEvent(source);
          bool successStorage = isIntegration == true
              ? await PermissionUtils().requestPermission(Permission.photos)
              : await PermissionUtils().requestPermission(Permission.storage);
          if (successStorage == true) {
            List<Widget> tiles = [];
            if (sourceType != "PROVINCIAL") {
              bool result = await profileInsuranceModel.getCopyInsuranceCandidate();

              if (result == true) {
                if (profileInsuranceModel.copySecondaryInsuranceList != null && profileInsuranceModel.copySecondaryInsuranceList.length > 0) {
                  for (int i = 0; i < profileInsuranceModel.copySecondaryInsuranceList.length; i++) {
                    Patient? patient = profileInsuranceModel.copySecondaryInsuranceList[i];
                    tiles.add(
                      ListTile(
                        dense: true,
                        title: Row(
                          children: <Widget>[
                            Text(patient!.firstName! + " " + patient.lastName! + " ", style: MEDIUM_XXX_PRIMARY_BOLD),
                            PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.secondary"))
                          ],
                        ),
                        onTap: () async {
                          Navigator.pop(contextm);
                          await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "SECONDARY", sourceType);
                        },
                      ),
                    );
                  }
                }

                if (profileInsuranceModel.copyPrimaryInsuranceList != null && profileInsuranceModel.copyPrimaryInsuranceList.length > 0) {
                  for (int i = 0; i < profileInsuranceModel.copyPrimaryInsuranceList.length; i++) {
                    Patient? patient = profileInsuranceModel.copyPrimaryInsuranceList[i]!;
                    tiles.add(
                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              patient.firstName! + " " + patient.lastName! + " ",
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                            ),
                            PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.primary"))
                          ],
                        ),
                        onTap: () async {
                          Navigator.pop(contextm);
                          bool result = await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "PRIMARY", sourceType);
                          if (result == true) {
                            showOnSnackBar(contextm, successMessage: profileInsuranceModel.errorMessage);
                          } else {
                            onFail(contextm, errMessage: profileInsuranceModel.errorMessage);
                          }
                        },
                      ),
                    );
                  }
                }
              }
            }
            showModalBottomSheet<void>(
              context: contextm,
              isScrollControlled: true,
              builder: (BuildContext contexts) {
                return new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
                    ),
                    PPDivider(),
                    ListTile(
                      key: Key("new open camera"),
                      leading: new Icon(
                        Icons.photo_camera,
                        color: primaryColor,
                      ),
                      title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
                      onTap: () async {
                        bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                        if (successCamera == true) {
                          Navigator.pop(contextm);
                          getImage(contextm, profileInsuranceModel, source, ImageSource.camera);
                        } else {
                          PermissionDialog.show(contextm, "camera");
                        }
                      },
                    ),
                    ListTile(
                      key: Key("new open gallery"),
                      leading: new Icon(Icons.photo_library, color: primaryColor),
                      title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
                      onTap: () async {
                        PermissionStatus status = await Permission.photos.status;
                        bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
                        if (successPhotos == true || status.isLimited) {
                          Navigator.pop(contextm);
                          getImage(contextm, profileInsuranceModel, source, ImageSource.gallery);
                        } else {
                          Platform.isIOS ? PermissionDialog.show(contextm, "photos") : PermissionDialog.show(contextm, "storage");
                        }
                      },
                    ),
                    tiles.length > 0
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              PPDivider(),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"),
                                    style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                              ),
                            ],
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                    Column(
                      children: tiles,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              },
            );
          }
        }
      },
      iconText: source.contains("FrontImage")
          ? LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-health-front").replaceAll("<br>", "\n")
          : LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-health-back").replaceAll("<br>", "\n"),
    );
  }

  showHealthNumberBottomSheet(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    if (dataStore.readString(DataStoreService.PHN).toString().isNotEmpty) {
      profileInsuranceModel.healthRetry = false;
      profileInsuranceModel.healthRetryCount = 0;
    } else {
      showModalBottomSheet(
          context: context,
          isDismissible: true,
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SMALL_XXX),
          ),
          builder: (context) {
            return HealthCardNumberSheet(
              context: context,
              profileInsuranceModel: profileInsuranceModel,
              source: source!,
              onSuccess: onSuccess,
            );
          });
    }
  }

  String getHeaderText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-healthcard-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-healthcard-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-healthcard-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-healthcard");
    }
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard");
    }
  }
}
