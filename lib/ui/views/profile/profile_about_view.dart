import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/contact.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_about_model.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

import '../../../locator.dart';

class ProfileAboutView extends StatefulWidget {
  ProfileAboutView({Key? key}) : super(key: key);

  @override
  _ProfileAboutViewState createState() => _ProfileAboutViewState();
}

class _ProfileAboutViewState extends BaseState<ProfileAboutView> {
  final TextEditingController _invitationCodeController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  String? dropDownValue;
  String? languageDropDownValue;
  String? dropDownError;
  int? patient_id;
  PPRadioGroup<String>? genderRadioGroup;
  String selectedGender = "";

  bool email = false;
  bool autovalidate = false;
  static bool gender = false;

  @override
  void initState() {
    super.initState();
    _emailController.addListener(emailListener);
  }

  emailListener() {
    if (email == false) {
      email = true;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.removeListener(emailListener);
  }

  Widget getBottomButton(BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel) {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    patient_id = curPatient != null ? curPatient.patientId : null;
    if ((curPatient != null &&
        curPatient.patient!.insuranceDetailsSet != null &&
        curPatient.patient!.insuranceDetailsSet!.length > 0 &&
        curPatient.patient!.employerDisplayName != null)) {
      return SizedBox(
        height: 0.0,
      );
    } else {
      return PPBottomBars.getButtonedBottomBar(
          child: PrimaryButton(
        key: Key("updateButton"),
        text: LocalizationUtils.getSingleValueString("profile", "profile.button.update"),
        fullWidth: true,
        onPressed: onUpdateClick(context, profileAboutModel, userContactModel),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProfileAboutModel>(create: (_) => ProfileAboutModel()),
        ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
        ChangeNotifierProvider<HomeModel>(create: (_) => HomeModel()),
        ChangeNotifierProvider<DashboardModel>(create: (_) => DashboardModel()),
      ],
      child: Consumer3<ProfileAboutModel, UserContactModel, DashboardModel>(builder:
          (BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel, DashboardModel dashboardModel, Widget? child) {
        return FutureBuilder(
            future: profileAboutModel.getLocalization(["profile", "signup"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                initialize();
                return getMainView(profileAboutModel, userContactModel, context);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget showEmailField(BuildContext context) {
    UserPatient? selectedPatient = Provider.of<DashboardModel>(context).selectedPatient;
    if (_emailController.text.isEmpty) {
      _emailController.text = selectedPatient!.patient!.email == null ? "" : selectedPatient.patient!.email!;
    }

    return selectedPatient!.patient!.email != null
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                LocalizationUtils.getSingleValueString("profile", "profile.labels.email"),
                style: getProfileHeaderStyle(),
              ),
              PPUIHelper.verticalSpaceXSmall(),
              Text(
                Provider.of<DashboardModel>(context).selectedPatient?.patient!.email ?? "-",
                style: getProfileItemStyle(),
              ),
            ],
          )
        : PPFormFields.getTextField(
            autovalidate: autovalidate,
            enabled: selectedPatient.patient!.email != null ? false : true,
            keyboardType: TextInputType.emailAddress,
            controller: _emailController,
            textInputAction: TextInputAction.done,
            onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required") + "*",
            decoration: PPInputDecor.getDecoration(labelText: 'E-mail Address', hintText: 'Enter Your Email', labelColor: primaryColor).copyWith(
              floatingLabelBehavior: FloatingLabelBehavior.always,
              errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required") + "*",
            ),
            onFieldSubmitted: (value) {});
  }

  Widget getMainView(ProfileAboutModel profileAboutModel, UserContactModel userContactModel, BuildContext context) {
    return BaseScaffold(
        key: Key("profileView"),
        bottomNavigationBar: (Provider.of<DashboardModel>(context).selectedPatient?.patient!.email ==null || Provider.of<DashboardModel>(context).selectedPatient?.patient!.gender ==null) ? Builder(
            builder: (BuildContext context) =>
                profileAboutModel.state == ViewState.Busy ? ViewConstants.progressIndicator : getBottomButton(context, profileAboutModel, userContactModel)) : SizedBox(),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: PPUIHelper.HorizontalSpaceMedium, right: PPUIHelper.HorizontalSpaceMedium, top: MEDIUM),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  PPUIHelper.verticalSpaceLarge(),
                  Text(
                    LocalizationUtils.getSingleValueString("profile", "profile.labels.firstname"),
                    style: getProfileHeaderStyle(),
                  ),
                  PPUIHelper.verticalSpaceXSmall(),
                  Text(
                    Provider.of<DashboardModel>(context).selectedPatient?.patient!.firstName ?? "-",
                    style: getProfileItemStyle(),
                  ),
                  PPUIHelper.verticalSpaceMedium(),
                  Text(
                    LocalizationUtils.getSingleValueString("profile", "profile.labels.lastname"),
                    style: getProfileHeaderStyle(),
                  ),
                  PPUIHelper.verticalSpaceXSmall(),
                  Text(
                    Provider.of<DashboardModel>(context).selectedPatient?.patient!.lastName ?? "-",
                    style: getProfileItemStyle(),
                  ),
                  Provider.of<DashboardModel>(context).selectedPatient!.patient!.phone == null
                      ? Container()
                      : Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              PPUIHelper.verticalSpaceMedium(),
                              Text(
                                LocalizationUtils.getSingleValueString("profile", "profile.labels.phone"),
                                style: getProfileHeaderStyle(),
                              ),
                              PPUIHelper.verticalSpaceXSmall(),
                              Text(
                                Provider.of<DashboardModel>(context).selectedPatient!.patient!.phone!.toString(),
                                style: getProfileItemStyle(),
                              ),
                            ],
                          ),
                        ),
                  PPUIHelper.verticalSpaceMedium(),
                  Text(
                    LocalizationUtils.getSingleValueString("profile", "profile.labels.dob"),
                    style: getProfileHeaderStyle(),
                  ),
                  PPUIHelper.verticalSpaceXSmall(),
                  Text(
                    Provider.of<DashboardModel>(context).selectedPatient?.patient!.birthDate ?? "-",
                    style: getProfileItemStyle(),
                  ),
                  PPUIHelper.verticalSpaceMedium(),
                  getGenderField(context),
                  PPUIHelper.verticalSpaceMedium(),
                  showEmailField(context),
                  PPUIHelper.verticalSpaceMedium(),
                  showEmployerDetails(context, Provider.of<DashboardModel>(context).selectedPatient?.patient!),
                  PPUIHelper.verticalSpaceMedium(),
                  //getInvitationCode(context, dashboardModel, profileAboutModel),
                  /*_getLanguagePreference(profileAboutModel),*/
                  //PPUIHelper.verticalSpaceMedium(),
                  /*FutureBuilder(
                    future: userContactModel.fetchContactDetails(),
                    builder: (BuildContext context, AsyncSnapshot<Contact?> snapshot) {
                      return Container(
                        child: Builder(
                          builder: (BuildContext context) {
                            if (snapshot.hasData != null || snapshot.data == null) {
                              return _afterFutureResolved(context, userContactModel);
                            } else {
                              return PPContainer.emptyContainer();
                            }
                          },
                        ),
                      );
                    },
                  ),*/
                  //PPUIHelper.verticalSpaceMedium(),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _getLanguagePreference(ProfileAboutModel profileAboutModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PPUIHelper.verticalSpaceXSmall(),
        Text(
          LocalizationUtils.getSingleValueString("profile", "profile.labels.language"),
          style: TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 15.84, color: darkPrimaryColor),
        ),
        PPUIHelper.verticalSpaceSmall(),
        Container(
          width: double.infinity,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
              borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                key: Key("languageDropDown"),
                value: languageDropDownValue == null
                    ? profileAboutModel.getStoredLanguage() == "en"
                        ? "en"
                        : "fr"
                    : languageDropDownValue!,
                onChanged: (String? newValue) {
                  languageDropDownValue = newValue;
                  profileAboutModel.setLanguageDropDown(newValue!);
                },
                items: ViewConstants.languageMap
                    .map((String key, String value) {
                      return MapEntry(
                        key,
                        DropdownMenuItem<String>(
                          value: key,
                          child: Text(
                            value,
                            style: TextStyle(fontFamily: "FSJoeyPro Medium", fontSize: 18),
                            key: Key(key),
                          ),
                        ),
                      );
                    })
                    .values
                    .toList(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _afterFutureResolved(BuildContext context, UserContactModel userContactModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              LocalizationUtils.getSingleValueString("profile", "profile.timings.title"),
              style: TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 18, color: darkPrimaryColor),
            ),
            PPUIHelper.verticalSpaceMedium(),
            Text(
              LocalizationUtils.getSingleValueString("modals", "modals.timings.day"),
              style: TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 15.84, color: darkPrimaryColor),
            ),
            PPUIHelper.verticalSpaceSmall(),
            Container(
              width: double.infinity,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
                  borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    key: Key("callDayDropDown"),
                    value: userContactModel.day,
                    onChanged: (String? newValue) async {
                      userContactModel.setDay(newValue!);
                      bool result = await userContactModel.putContact();
                      if (result == true) {
                        onFail(context, errMessage: userContactModel.errorMessage);
                      }
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.profile_user_contact_day_change);
                    },
                    items: getDayMap()
                        .map((String key, String value) {
                          return MapEntry(
                              key,
                              DropdownMenuItem<String>(
                                value: key,
                                child: Text(
                                  value,
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium", fontSize: 18),
                                  key: Key(key),
                                ),
                              ));
                        })
                        .values
                        .toList(),
                  ),
                ),
              ),
            ),
            PPUIHelper.verticalSpaceMedium(),
            Text(
              LocalizationUtils.getSingleValueString("modals", "modals.timings.time"),
              style: TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 15.84, color: darkPrimaryColor),
            ),
            //PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.time")),
            PPUIHelper.verticalSpaceSmall(),
            Container(
              width: double.infinity,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
                  borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    key: Key("callTimeDropDown"),
                    value: userContactModel.time,
                    onChanged: (String? newValue) async {
                      userContactModel.setTime(newValue!);
                      bool result = await userContactModel.putContact();
                      if (result == true) {
                        onFail(context, errMessage: userContactModel.errorMessage);
                      }
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.profile_user_contact_time_change);
                    },
                    items: getTimeMap()
                        .map((String key, String value) {
                          return MapEntry(
                              key,
                              DropdownMenuItem<String>(
                                value: key,
                                child: Text(
                                  value,
                                  style: TextStyle(fontFamily: "FSJoeyPro Medium", fontSize: 18),
                                  key: Key(key),
                                ),
                              ));
                        })
                        .values
                        .toList(),
                  ),
                ),
              ),
            ),
          ],
        )),
        SizedBox(
          height: LARGE_X,
        )
      ],
    );
  }

  Widget getInvitationCode(BuildContext context, DashboardModel dashboardModel, ProfileAboutModel profileAboutModel) {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    if (curPatient!.primary != true) return Container();
    if (curPatient.patient!.insuranceDetailsSet != null &&
        curPatient.patient!.insuranceDetailsSet!.length > 0 &&
        curPatient.patient!.employerDisplayName != null)
      return Container(
          child: Column(
        children: <Widget>[
          PPFormFields.getTextField(
            enabled: false,
            controller: TextEditingController(text: curPatient.patient!.employerDisplayName),
            textCapitalization: TextCapitalization.characters,
            decoration: PPInputDecor.getDecoration(
              labelText: 'Employer Code',
            ),
          ),
        ],
      ));
    else
      return FutureBuilder(
        future: profileAboutModel.getSuggestedEmployers(),
        builder: (BuildContext context, AsyncSnapshot<EmployerSuggestionResponse?> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data != null && snapshot.data!.suggestionsValidCount! > 0) {
              return afterFutureResolved(context, profileAboutModel, snapshot.data!);
            } else {
              return Container(
                child: Column(
                  children: <Widget>[
                    PPFormFields.getTextField(
                        controller: _invitationCodeController,
                        decoration: PPInputDecor.getDecoration(labelText: 'Employer Code (Optional)', hintText: 'Enter employer code here'),
                        validator: (value) {
                          return null;
                        }),
                    PPUIHelper.verticalSpaceMedium(),
                  ],
                ),
              );
            }
          } else if (snapshot.hasError) {
            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
            return PPContainer.emptyContainer();
          }
          return PPContainer.emptyContainer();
        },
      );
  }

  onUpdateClick(BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel) {
    return () async {
      bool connectivityResult = await profileAboutModel.isInternetConnected();

      if (connectivityResult == false) {
        onFail(context, errMessage: profileAboutModel.noInternetConnection);
        return;
      }

      if ((_emailController.text != null && _emailController.text == '') && !StringUtils.isEmail(_emailController.text)) {
        onFail(context, errMessage: 'Invalid Email');
        return;
      }

      /*analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_employer_updated);
      if ((_invitationCodeController.text != null && _invitationCodeController.text.length > 0)) {
        var inviteRes = await profileAboutModel.updateInvitationCode(_invitationCodeController.text);
        showOnSnackBar(context, successMessage: inviteRes);
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
      }*/

      /*if (languageDropDownValue != null) {
        bool? success = await profileAboutModel.updateLanguage(languageDropDownValue!);
        if (success!) {
          profileAboutModel.setLanguage(context, languageDropDownValue!);
        }
      }*/

      String email = _emailController.text;
      if (email.isNotEmpty && await profileAboutModel.updateMemberEmail(patient_id!, email)) {
        await Provider.of<DashboardModel>(context, listen: false).setEmail(email);
        await Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
      }

      if (selectedGender.isNotEmpty) {
        bool status = await profileAboutModel.updateAboutYou(gender: selectedGender);
        if (status) {
          await Provider.of<DashboardModel>(context, listen: false).setGender(selectedGender);
          await Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
          await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
          selectedGender = "";
        }
      }
    };
  }

  Widget afterFutureResolved(BuildContext context, ProfileAboutModel profileAboutModel, EmployerSuggestionResponse data) {
    _emailController.text = profileAboutModel.email;
    return PPFormFields.getDropDown(
      labelText: "Select Employer",
      fullWidth: true,
      value: dropDownValue!,
      isError: false,
      items: data.suggestedEmployers!
          .map((state) => DropdownMenuItem<String>(
              value: state.employerName! +
                  " " +
                  ((state.invitationCodeDataSet != null && state.invitationCodeDataSet!.length > 0) ? state.invitationCodeDataSet![0].invitationCode! : ""),
              child: Text(state.employerDisplayName!)))
          .toList(),
      onChanged: (selectedItem) => setState(() {
        dropDownValue = selectedItem;
        if (selectedItem!.split(" ")[1] != null) {
          _invitationCodeController.text = selectedItem.split(" ")[1];
        } else {
          _invitationCodeController.text = selectedItem.split(" ")[0];
        }
        setState(() {
          dropDownError = null;
        });
      }),
    );
  }

  Widget getGenderField(BuildContext context) {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    if (dataStore.readInteger(DataStoreService.USERID) == curPatient!.userId && (curPatient.patient!.gender == null)) {
      return genderRadioGroup!;
    } else {
      UserPatient? selectedPatient = Provider.of<DashboardModel>(context).selectedPatient;

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            LocalizationUtils.getSingleValueString("profile", "profile.labels.gender"),
            style: getProfileHeaderStyle(),
          ),
          PPUIHelper.verticalSpaceXSmall(),
          Text(
            selectedPatient?.patient!.gender ?? "-",
            style: getProfileItemStyle(),
          ),
          /*PPFormFields.getTextField(
            enabled: false,
            controller: TextEditingController(text: selectedPatient!.patient!.gender ?? "-"),
            textCapitalization: TextCapitalization.words,
            decoration: PPInputDecor.getDecoration(
                labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.gender"),
                hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.gender"),
                labelColor: primaryColor),
          ),*/
        ],
      );
    }
  }

  initGender() {
    genderRadioGroup = PPRadioGroup(
      key: Key("genderRadioGroup"),
      type: "gender",
      radioOptions: [
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-label"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        selectedGender = value;
        if (gender == false) {
          gender = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_gender_entered);
        }
      },
    );
  }

  void initialize() {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    patient_id = curPatient != null ? curPatient.patientId : null;
    if (dataStore.readInteger(DataStoreService.USERID) == curPatient!.userId && dataStore.readString(DataStoreService.GENDER).toString().isEmpty) {
      initGender();
    }
  }

  TextStyle getProfileHeaderStyle() {
    return TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 16.8, color: darkPrimaryColor);
  }

  TextStyle getProfileItemStyle() {
    return TextStyle(fontFamily: "FSJoeyPro Heavy", fontSize: 22.8, color: darkPrimaryColor);
  }

  TextStyle getProfileEmployerStyle() {
    return TextStyle(fontFamily: "FSJoeyPro", fontSize: 18.9, color: bottomSuccessColor);
  }

  Widget showEmployerDetails(BuildContext context, Patient? patient) {
    if (patient != null && patient.insuranceDetailsSet!.length > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            LocalizationUtils.getSingleValueString("profile", "profile.labels.employer").isEmpty ? "Employer" : LocalizationUtils.getSingleValueString("profile", "profile.labels.employer"),
            style: getProfileHeaderStyle(),
          ),
          PPUIHelper.verticalSpaceXSmall(),
          Text(StringUtils.capitalize(patient.insuranceDetailsSet![0]["employerDisplayName"] ?? ""),
            style: getProfileEmployerStyle(),
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}

String getGender(String? gender) {
  if (gender!.toLowerCase() == "male") {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-male");
  } else if (gender.toLowerCase() == "female") {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-female");
  } else {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-other");
  }
}

getGenderEnglish(String? value) {
  if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase()) {
    return "MALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase()) {
    return "FEMALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()) {
    return "OTHER";
  }
}
