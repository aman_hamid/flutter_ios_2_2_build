import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/transfer/transfer_search_selection_model.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/success/base_success_screen.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class TransferPharmacySearchView extends StatefulWidget {
  static const routeName = 'transfer_pharmacy_search_view';

  final BaseStepperSource? source;
  final String? PharmacyNamePopular;
  final UserPatient? userPatient;
  final String? medicineName;
  final int? quantity;
  final SignUpTransferModel? model;

  TransferPharmacySearchView(
      {Key? key, this.source = BaseStepperSource.MAIN_SCREEN, this.PharmacyNamePopular, this.userPatient, this.medicineName, this.quantity, this.model})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TransferPharmacySearchViewState();
  }
}

class TransferPharmacySearchViewState extends BaseState<TransferPharmacySearchView> {
  final DataStoreService dataStore = locator<DataStoreService>();
  String postal_code = "";
  bool autovalidate = false;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _pharmacySearchNameController = TextEditingController();
  final TextEditingController _pharmacyNameController = TextEditingController();
  final TextEditingController _pharmacyPhoneController = TextEditingController();
  final TextEditingController _pharmacyAddressController = TextEditingController();
  final TextEditingController _commentsController = TextEditingController();

  bool newUserFlow = false;
  bool transferAll = true;
  bool pharmacyName = false, pharmacyAddress = false, pharmacyPhone = false, pharmacyComment = false, pharmacySearchName = false;
  bool addedValue = false;
  bool suggestionChanged = false;

  BuildContext? innerContext;
  String prevPharmacyName = "";
  String province = "";
  String selectedPlaceId = '';

  File? transferImageResource;

  late FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _pharmacyNameController.addListener(pharmacyNameListener);
    _pharmacySearchNameController.addListener(pharmacyControllerListener);
    _pharmacyPhoneController.addListener(pharmacyPhoneListener);
    _pharmacyAddressController.addListener(pharmacyAddressListener);
    _commentsController.addListener(pharmacyCommentListener);
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _pharmacyNameController.removeListener(pharmacyNameListener);
    _pharmacySearchNameController.removeListener(pharmacyControllerListener);
    _pharmacyPhoneController.removeListener(pharmacyPhoneListener);
    _pharmacyAddressController.removeListener(pharmacyAddressListener);
    _commentsController.removeListener(pharmacyCommentListener);
    super.dispose();
  }

  pharmacyControllerListener() {
    if (pharmacySearchName == false && widget.source == BaseStepperSource.NEW_USER && _pharmacySearchNameController.text.isNotEmpty) {
      pharmacySearchName = true;
      if (widget.PharmacyNamePopular == "") {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_name_entered);
      }
    }
  }

  pharmacyPhoneListener() {
    if (pharmacyPhone == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyPhone = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_phone_entered);
    }
  }

  pharmacyNameListener() {
    if (pharmacyName == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyName = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_name_entered);
    }
  }

  pharmacyCommentListener() {
    if (pharmacyComment == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyComment = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_comment_entered);
    }
  }

  pharmacyAddressListener() {
    if (pharmacyAddress == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyAddress = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_address_entered);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SignUpTransferModel(),
      child: Consumer<SignUpTransferModel>(builder: (BuildContext context, SignUpTransferModel model, Widget? child) {
        return FutureBuilder(
            future: model.getLocalization(["signup"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                if (!addedValue) {
                  addedValue = true;
                  if (!["", null].contains(widget.PharmacyNamePopular)) {
                    _pharmacySearchNameController.text = widget.PharmacyNamePopular!;
                    model.getPlaceSuggestions(_pharmacySearchNameController.text);
                  }
                }
                return WillPopScope(
                    onWillPop: () async {
                      backProcess(model);
                      return true;
                    },
                    child: getStartView(model));
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  void backProcess(SignUpTransferModel signUpTransferModel) {

    Navigator.pop(context);

    /*if (dataStore.readBoolean(DataStoreService.NEW_USER_SEARCH) == true) {
      if (dataStore.readInteger(DataStoreService.EXIT_COUNT) == 1) {
        dataStore.writeInt(DataStoreService.EXIT_COUNT, 2);
      } else {
        dataStore.writeInt(DataStoreService.EXIT_COUNT, 0);
      }

      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(
            source: BaseStepperSource.NEW_USER,
            postalCode: signUpTransferModel.postalCode,
            province: signUpTransferModel.province,
            from: "transfer",
            pharmacyName: signUpTransferModel.pharmacyName,
            modelFrom: signUpTransferModel,
            pharmacyAddress: signUpTransferModel.pharmacyAddress,
          ));
    } else {
      Navigator.pop(context);
    }*/
  }

  Widget getStartView(SignUpTransferModel model) {
    return BaseScaffold(
      key: Key("PharmacySearchView"),
      body: SafeArea(child: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: <Widget>[
              getMainView(model),
              model.state == ViewState.Busy
                  ? Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: whiteOpacity,
                      child: ViewConstants.progressIndicator,
                    )
                  : PPContainer.emptyContainer()
            ],
          );
        },
      )),
    );
  }

  Widget getMainView(SignUpTransferModel model) {
    bool success;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Material(
                elevation: 4,
                color: whiteColor,
                child: Padding(
                  padding: const EdgeInsets.only(top: REGULAR_XX, left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: SMALL),
                  child: getAutoCompleteField(model),
                )),
          ),
        ),
        Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: model.pharmacyPredictions.length,
              itemBuilder: (BuildContext context, int index) {
                Pharmacy pharmacy = model.pharmacyPredictions[index];
                return InkWell(
                  key: Key("Select" + index.toString()),
                  onTap: () async {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    model.pharmacySubmitted = true;
                    model.setState(ViewState.Busy);
                    PlaceDetails selectedPlace = await model.getPlaceDetails(pharmacy.pharmacyPlaceId??"");
                    List<AddressComponent> addressComponents = selectedPlace.addressComponents;
                    AddressComponent addressCompone;
                    addressComponents.forEach((value) => {
                          addressCompone = value,
                          if (ViewConstants.province.containsKey(addressCompone.longName))
                            {province = ViewConstants.province[addressCompone.longName.toString()]!},
                          addressCompone.types.forEach((element) {
                            if (element == "postal_code") {
                              postal_code = addressCompone.longName;
                            }
                          }),
                        });
                    dataStore.writeString(DataStoreService.PHARMACY_ZIP_EDD, postal_code);
                    dataStore.writeString(DataStoreService.PHARMACY_PROVINCE_EDD, province);
                    model.setState(ViewState.Idle);

                    if (selectedPlace != null) {
                      selectedPlaceId = selectedPlace.placeId;

                      _pharmacySearchNameController.text = selectedPlace.name;
                      model.pharmacyAddress = selectedPlace.formattedAddress ?? "";
                      model.postalCode = postal_code;
                      model.pharmacyName = selectedPlace.name;
                      model.pharmacyPhoneNumber = selectedPlace.formattedPhoneNumber ?? "";
                      model.province = province;
                      _pharmacyNameController.text = selectedPlace.name;
                      _pharmacyPhoneController.text = selectedPlace.formattedPhoneNumber ?? "";
                      _pharmacyAddressController.text = selectedPlace.formattedAddress ?? "";
                      /*if (!LocalizationUtils.isProvinceQuebec()) {*/
                        TransferSearchSelectModel selectedModel = TransferSearchSelectModel();
                        selectedModel.selectedPlaceId = selectedPlace.placeId;
                        selectedModel.pharmacyName = selectedPlace.name;
                        selectedModel.pharmacyAddress = selectedPlace.formattedAddress ?? "";
                        selectedModel.pharmacyPhone =
                            getFormatterPhoneNumber(selectedPlace.formattedPhoneNumber != null ? selectedPlace.formattedPhoneNumber.toString() : "");
                        selectedModel.pharmacyProvince = province;
                        selectedModel.pharmacyPostalCode = postal_code;
                        selectedModel.transferAll = transferAll;
                        selectedModel.comments = transferAll ? null : (_commentsController.text != null ? _commentsController.text : "");
                        selectedModel.isSignup = true;
                        selectedModel.medicineName = widget.medicineName;
                        selectedModel.quantity = widget.quantity;
                        selectedModel.prescriptionState = null;
                        if (model.omsPrescriptionId != null) {
                          selectedModel.isPrescriptionEdited = true;
                          selectedModel.id = model.id;
                          selectedModel.omsPrescriptionId = model.omsPrescriptionId;
                        }
                        Navigator.pop(context, selectedModel);
                      }
                      /*else {
                        success = widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
                            ? await model.uploadPrescriptionAdd(
                                widget.userPatient,
                                selectedPlaceId,
                                model.pharmacyName != null ? model.pharmacyName : "",
                                model.pharmacyAddress != null ? model.pharmacyAddress : "",
                                getFormatterPhoneNumber(model.pharmacyPhoneNumber != null ? model.pharmacyPhoneNumber : ""),
                                transferAll,
                                transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
                                true,
                                null,
                                model.province != null ? model.province : "",
                                widget.medicineName,
                                widget.quantity,
                                model.id,
                                model.omsPrescriptionId == null ? false : true,
                                model.omsPrescriptionId)
                            : await model.uploadPrescription(
                                userPatient: widget.userPatient,
                                placeId: selectedPlaceId,
                                pharmacyName: model.pharmacyName != null ? model.pharmacyName : "",
                                pharmacyAddress: model.pharmacyAddress != null ? model.pharmacyAddress : "",
                                pharmacyPhone: getFormatterPhoneNumber(model.pharmacyPhoneNumber != null ? model.pharmacyPhoneNumber : ""),
                                isTransferAll: transferAll,
                                comments: transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
                                isSignUpFlow: checkSignupFlow(),
                                prescriptionState: null,
                                province: model.province != null ? model.province : "",
                                isPrescriptionEdited: model.omsPrescriptionId == null ? false : true,
                                omsPrescriptionId: model.omsPrescriptionId,
                                id: model.id,
                                medicineName: widget.medicineName,
                                quantity: widget.quantity);

                        if (success == true) {
                          Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
                          Provider.of<PrescriptionModel>(context, listen: false).clearData();
                          dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);

                          int? count = widget.source == BaseStepperSource.NEW_USER
                              ? model.prescriptionList!.where((element) => element.prescriptionState!.contains('FILED')).length
                              : model.totalPrescriptionsCount;
                          Map<String, dynamic> leadMap = new Map();
                          leadMap['lead_created_by'] = 'mobileApp';
                          leadMap['lead_new'] = count != null && count > 1 ? 0 : 1;
                          leadMap['lead_type'] = 'transfer';
                          leadMap['lead_origin'] = widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
                          leadMap['transfer_pharmacy_name'] = model.pharmacyName != null ? model.pharmacyName : "";
                          leadMap['transfer_pharmacy_city'] = model.pharmacyAddress != null ? model.pharmacyAddress : "";
                          if (model.revenue) {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
                          }

                          goToNextScreen(model);
                          if (checkSignupFlow()) {
                            analyticsEvents.sendInitiatedCheckoutEvent();
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new_formfilled);
                          } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
                            analyticsEvents.sendInitiatedCheckoutEvent();
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_refills_formfilled);
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_congratulations);
                          }
                        } else {
                          onFail(context, errMessage: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
                        }
                      }
                      if (model.checkSignupFlow(widget.source!)) {
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search_select);
                      } else {
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_select);
                      }*/
                    //}
                  },
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(MEDIUM_X, SMALL_XXX, MEDIUM_X, SMALL_XXX),
                          child: getAutoCompleteItem(pharmacy),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: SMALL_X),
                          child: PPDivider(),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
        getButton(model)
      ],
    );
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) async {
    if (widget.source == BaseStepperSource.NEW_USER) {
      await dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
        if (ChambersRedirect.routeName()!.contains('signUpAlmostDoneWidget')) {
          Navigator.pushNamed(context, ChambersRedirect.routeName()!,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        }
      }
      dataStore.writeString(DataStoreService.PHARMACY_NAME, signUpTransferModel.pharmacyName);
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, signUpTransferModel.pharmacyAddress);
      dataStore.writeString(DataStoreService.POSTAL_CODE_TRANSFER, signUpTransferModel.postalCode);
      dataStore.writeString(DataStoreService.PROVINCE_TRANSFER, signUpTransferModel.province);
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, true);
      //Navigator.pop(context);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(
            source: BaseStepperSource.NEW_USER,
            postalCode: signUpTransferModel.postalCode,
            province: signUpTransferModel.province,
            from: "transfer",
            pharmacyName: signUpTransferModel.pharmacyName,
            modelFrom: signUpTransferModel,
            pharmacyAddress: signUpTransferModel.pharmacyAddress,
          ));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
      dataStore.writeString(DataStoreService.PHARMACY_NAME, signUpTransferModel.pharmacyName);
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, signUpTransferModel.pharmacyAddress);
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      dataStore.writeString(DataStoreService.PHARMACY_NAME, signUpTransferModel.pharmacyName);
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, signUpTransferModel.pharmacyAddress);
      dataStore.writeString(DataStoreService.POSTAL_CODE_TRANSFER, signUpTransferModel.postalCode);
      dataStore.writeString(DataStoreService.PROVINCE_TRANSFER, signUpTransferModel.province);
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.MAIN_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  Widget getButton(SignUpTransferModel model) {
    return Builder(
      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
        child: model.state == ViewState.Busy
            ? Container(
                width: double.infinity,
                height: double.infinity,
                color: whiteOpacity,
              )
            : Row(
                children: <Widget>[
                  /*dataStore.readBoolean(DataStoreService.NEW_USER_SEARCH) == false
                      ? SecondaryButton(
                          key: Key("back"),
                          isExpanded: false,
                          text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.back"),
                          onPressed: () {
                            SystemChannels.textInput.invokeMethod('TextInput.hide');
                            Navigator.pop(context);
                          },
                        )
                      : Container(),*/
                  SecondaryButton(
                    key: Key("back"),
                    isExpanded: false,
                    text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.back"),
                    onPressed: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                  SecondaryButton(
                    key: Key("not_find_pharmacy"),
                    isExpanded: true,
                    isTextUpperCase: false,
                    text: getButtonText(PatientUtils.getForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
                    textColor: linkColor,
                    onPressed: () {
                      _settingModalBottomSheet(context, model);
                    },
                  )
                ],
              ),
      ),
    );
  }

  bool checkSignupFlow() {
    return widget.source == BaseStepperSource.NEW_USER;
  }

  Widget getAutoCompleteItem(Pharmacy item) {
    if (item != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  item.pharmacyName??"",
                  style: widget.PharmacyNamePopular == ""
                      ? MEDIUM_XXX_PRIMARY_BOLD
                      : suggestionChanged
                          ? MEDIUM_XXX_PRIMARY_BOLD
                          : MEDIUM_XX_SECONDARY,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: SMALL,
                ),
                Text(
                  item.pharmacyAddress??"",
                  style: widget.PharmacyNamePopular == ""
                      ? MEDIUM_XX_SECONDARY
                      : suggestionChanged
                          ? MEDIUM_XX_SECONDARY
                          : MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          Text(
            LocalizationUtils.getSingleValueString("signup", "signup.transfer.select").toUpperCase(),
            style: MEDIUM_X_LINK_MEDIUM_BOLD,
          )
        ],
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getAutoCompleteField(SignUpTransferModel model) {
    return Stack(
      children: <Widget>[
        Focus(
          child: PPFormFields.getTextField(
              key: Key("SearchTextView"),
              keyboardType: TextInputType.emailAddress,
              focusNode: _focusNode,
              controller: _pharmacySearchNameController,
              autoFocus: true,
              textInputAction: TextInputAction.next,
              onTextChanged: (value) {
                suggestionChanged = true;
                model.getPlaceSuggestions(_pharmacySearchNameController.text);
              },
              decoration: PPInputDecor.getDecoration(
                  labelText: getPlaceHolderDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
                  hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-name"),
                  suffixIcon: Icon(Icons.search)),
              onFieldSubmitted: (value) {}),
          onFocusChange: (hasFocus) {
            if (hasFocus) {
              model.pharmacySubmitted = false;
            }
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(MEDIUM_X),
            child: (_pharmacySearchNameController.text != null && _pharmacySearchNameController.text != "")
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        _pharmacySearchNameController.text = "";
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(MEDIUM_XXX),
                          )),
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Icon(
                          Icons.clear,
                          size: 18,
                          color: whiteColor,
                        ),
                      ),
                    ),
                  )
                : Icon(
                    Icons.search,
                    size: 24,
                    color: secondaryColor,
                  ),
          ),
        )
      ],
    );
  }

  Widget getCommentsBox() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
      child: PPFormFields.getMultiLineTextField(
        controller: _commentsController,
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.medications"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.list-pahramacy-info")),
      ),
    );
  }

  void _settingModalBottomSheet(context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(bc);
              if (!currentFocus.hasPrimaryFocus &&
                  currentFocus.focusedChild != null) {
                FocusManager.instance.primaryFocus?.unfocus();
              }
            },
            child: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(bc).viewInsets.bottom),
                child: Container(
                  child: new Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: MEDIUM_XXX, bottom: MEDIUM_XXX, top: MEDIUM_XXX),
                            child: Text(
                              LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-title"),
                              style:
                                  TextStyle(color: primaryColor, fontSize: REGULAR, fontWeight: FontWeight.bold, height: 1.4, fontFamily: "FSJoeyPro Heavy"),
                            ),
                          ),
                          Divider(
                            color: secondaryColor,
                            height: 1,
                          ),
                          Padding(
                              padding: const EdgeInsets.all(MEDIUM_XXX),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-name"),
                                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    PPFormFields.getTextField(
                                        key: Key("pharmacy_name"),
                                        autoFocus: true,
                                        textCapitalization: TextCapitalization.words,
                                        controller: _pharmacyNameController,
                                        autovalidate: autovalidate,
                                        textInputAction: TextInputAction.next,
                                        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                                        decoration: PPInputDecor.getDecoration(labelText: "", hintText: ""),
                                        onFieldSubmitted: (value) {}),
                                    PPUIHelper.verticalSpaceMedium(),
                                    PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-phone"),
                                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    PPFormFields.getNumericFormField(
                                      key: Key("pharmacy_number"),
                                      textInputAction: TextInputAction.next,
                                      autovalidate: autovalidate,
                                      controller: _pharmacyPhoneController,
                                      errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                                      decoration: PPInputDecor.getDecoration(labelText: '', hintText: ''),
                                      maxLength: 10,
                                      minLength: 10,
                                    ),
                                    PPUIHelper.verticalSpaceMedium(),
                                    PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-address"),
                                        fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    PPFormFields.getMultiLineTextField(
                                      key: Key("pharmacy_address"),
                                      autoFocus: false,
                                      autovalidate: autovalidate,
                                      onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                                      controller: _pharmacyAddressController,
                                      decoration: PPInputDecor.getDecoration(labelText: '', hintText: ''),
                                    ),
                                  ],
                                ),
                              )),
                        ],
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          children: <Widget>[
                            PPDivider(),
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment(0, 0),
                                  child: Row(
                                    children: <Widget>[
                                      SecondaryButton(
                                        key: Key("pharmacy_close"),
                                        text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-close"),
                                        onPressed: () {
                                          Navigator.pop(bc);
                                        },
                                      ),
                                      SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                      PrimaryButton(
                                          key: Key("pharmacy_continue"),
                                          text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-continue"),
                                          onPressed: () async {
                                            autovalidate = true;
                                            model.postalCode = postal_code.isNotEmpty ? postal_code : dataStore.getUserPostalCode() ?? "";
                                            model.pharmacyAddress = _pharmacyAddressController.text;
                                            model.pharmacyName = _pharmacyNameController.text;
                                            model.pharmacyPhoneNumber = _pharmacyPhoneController.text;
                                            model.province = province;

                                            await dataStore.writeString(DataStoreService.PHARMACY_ZIP_EDD, model.postalCode);
                                            await dataStore.writeString(DataStoreService.PHARMACY_PROVINCE_EDD, model.province); //dataStore.getGeoIpProvince()!

                                            if (_formKey.currentState!.validate()) {
                                              if (_pharmacyNameController.text == '' || _pharmacyAddressController.text == '') {
                                                onFail(context, errMessage: 'Please fill in the required fields.');
                                                return;
                                              } else {
                                                /*if (!LocalizationUtils.isProvinceQuebec()) {*/
                                                  TransferSearchSelectModel selectedModel = TransferSearchSelectModel();
                                                  selectedModel.selectedPlaceId = selectedPlaceId;
                                                  selectedModel.pharmacyName = _pharmacyNameController.text;
                                                  selectedModel.pharmacyAddress = _pharmacyAddressController.text;
                                                  selectedModel.pharmacyPhone = getFormatterPhoneNumber(_pharmacyPhoneController.text);
                                                  selectedModel.pharmacyProvince = province;
                                                  selectedModel.pharmacyPostalCode = postal_code.isNotEmpty ? postal_code : dataStore.getUserPostalCode() ?? "";
                                                  selectedModel.transferAll = transferAll;
                                                  selectedModel.comments =
                                                      transferAll ? null : (_commentsController.text != null ? _commentsController.text : "");
                                                  selectedModel.isSignup = true;
                                                  selectedModel.medicineName = widget.medicineName;
                                                  selectedModel.quantity = widget.quantity;
                                                  selectedModel.prescriptionState = null;
                                                  if (model.omsPrescriptionId != null) {
                                                    selectedModel.isPrescriptionEdited = true;
                                                    selectedModel.id = model.id;
                                                    selectedModel.omsPrescriptionId = model.omsPrescriptionId;
                                                  }
                                                  Navigator.pop(context);
                                                  Navigator.pop(context, selectedModel);
                                                /*}
                                                else {
                                                  bool success;
                                                  success = widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
                                                      ? await model.uploadPrescriptionAdd(
                                                          widget.userPatient,
                                                          selectedPlaceId,
                                                          model.pharmacyName != null ? model.pharmacyName : "",
                                                          model.pharmacyAddress != null ? model.pharmacyAddress : "",
                                                          getFormatterPhoneNumber(model.pharmacyPhoneNumber != null ? model.pharmacyPhoneNumber : ""),
                                                          transferAll,
                                                          transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
                                                          true,
                                                          null,
                                                          model.province != null ? model.province : "",
                                                          //dataStore.getGeoIpProvince(),
                                                          widget.medicineName,
                                                          widget.quantity,
                                                          model.id,
                                                          model.omsPrescriptionId == null ? false : true,
                                                          model.omsPrescriptionId,
                                                        )
                                                      : await model.uploadPrescription(
                                                          userPatient: widget.userPatient,
                                                          placeId: selectedPlaceId,
                                                          pharmacyName: model.pharmacyName != null ? model.pharmacyName : "",
                                                          pharmacyAddress: model.pharmacyAddress != null ? model.pharmacyAddress : "",
                                                          pharmacyPhone:
                                                              getFormatterPhoneNumber(model.pharmacyPhoneNumber != null ? model.pharmacyPhoneNumber : ""),
                                                          isTransferAll: transferAll,
                                                          comments: transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
                                                          isSignUpFlow: checkSignupFlow(),
                                                          prescriptionState: null,
                                                          province: model.province != null ? model.province : "",
                                                          medicineName: widget.medicineName,
                                                          isPrescriptionEdited: model.omsPrescriptionId == null ? false : true,
                                                          omsPrescriptionId: model.omsPrescriptionId,
                                                          id: model.id,
                                                          quantity: widget.quantity);

                                                  if (success == true) {
                                                    Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
                                                    Provider.of<PrescriptionModel>(context, listen: false).clearData();
                                                    dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);
                                                    int? count = widget.source == BaseStepperSource.NEW_USER
                                                        ? model.prescriptionList!.where((element) => element.prescriptionState!.contains('FILED')).length
                                                        : model.totalPrescriptionsCount;
                                                    Map<String, dynamic> leadMap = new Map();
                                                    leadMap['lead_created_by'] = 'mobileApp';
                                                    leadMap['lead_new'] = count != null && count > 1 ? 0 : 1;
                                                    leadMap['lead_type'] = 'transfer';
                                                    leadMap['lead_origin'] = widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
                                                    leadMap['transfer_pharmacy_name'] = model.pharmacyName != null ? model.pharmacyName : "";
                                                    leadMap['transfer_pharmacy_city'] = model.pharmacyAddress != null ? model.pharmacyAddress : "";
                                                    if (model.revenue) {
                                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
                                                    }
                                                    if (checkSignupFlow()) {
                                                      analyticsEvents.sendInitiatedCheckoutEvent();
                                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new_formfilled);
                                                    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
                                                      analyticsEvents.sendInitiatedCheckoutEvent();
                                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_refills_formfilled);
                                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_congratulations);
                                                    }
                                                    goToNextScreen(model);
                                                  } else {
                                                    onFail(context, errMessage: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
                                                  }
                                                  *//*                                                Navigator.pop(bc);
                                                Navigator.pop(context, _pharmacyNameController.text + "*" + _pharmacyPhoneController.text + "*" + _pharmacyAddressController.text);*//*
                                                }*/
                                              }
                                            }
                                          })
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  int getFormatterPhoneNumber(String phoneNumber) {
    try {
      String formattedNumber = phoneNumber.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      return int.parse(formattedNumber);
    } catch (ec) {
      return -1;
    }
  }

  String getPlaceHolderDescriptionText(String? gender) {
    switch (gender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("signup", "signup.fields.searchpharmacy-MALE");
        break;
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("signup", "signup.fields.searchpharmacy-FEMALE");
        break;
      case "OTHER":
        return LocalizationUtils.getSingleValueString("signup", "signup.fields.searchpharmacy-other");
        break;
      default:
        return LocalizationUtils.getSingleValueString("signup", "signup.fields.searchpharmacy");
        break;
    }
  }

  String getButtonText(String? gender) {
    switch (gender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("signup", "signup.transfer.forgotbutton-MALE");
        break;
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("signup", "signup.transfer.forgotbutton-FEMALE");
        break;
      case "OTHER":
        return LocalizationUtils.getSingleValueString("signup", "signup.transfer.forgotbutton-OTHER");
        break;
      default:
        return LocalizationUtils.getSingleValueString("signup", "signup.transfer.forgotbutton");
        break;
    }
  }
}
