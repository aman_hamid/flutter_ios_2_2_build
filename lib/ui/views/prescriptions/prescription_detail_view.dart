import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/documents_type.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/response/prescription/prescription_medication.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/views/pdf_viewer.dart';
import 'package:pocketpills/ui/views/prescriptions/prescriptions_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class PrescriptionDetailWidget extends StatelessWidget {
  static const routeName = 'prescriptiondetail';
  Prescription? prescription;

  PrescriptionDetailWidget({this.prescription});

  @override
  Widget build(BuildContext context) {
    return Consumer<PrescriptionModel>(
      builder: (BuildContext context, PrescriptionModel prescriptionModel, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(prescriptionModel),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(prescriptionModel, prescription);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Future myFutureMethodOverall(PrescriptionModel prescriptionModel) async {
    Future<Map<String, dynamic>?> future1 = prescriptionModel.getLocalization(["modals", "medications", "prescriptions"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(PrescriptionModel prescriptionModel, Prescription? prescription) {
    return Scaffold(
      key: Key("prescriptionDetailsView"),
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.labels.title"),
        appBar: AppBar(),
      ),
      body: getPrescriptionDetailView(prescription),
    );
  }

  Widget getPrescriptionDetailView(Prescription? prescription) {
    if (prescription != null) {
      return Container(
        child: Builder(
          builder: (BuildContext context) {
            List<Widget> prescriptionDetailsChildren = [];
            prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
            prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: getPrescriptionCardHeading(prescription),
            ));
            prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: PPTexts.getSecondaryHeading(
                  LocalizationUtils.getSingleValueString("modals", "modals.prescription.created-label") +
                      ": " +
                      PPDateUtils.displayDate(prescription.createDateTime),
                  isBold: false),
            ));
            prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: PPTexts.getSecondaryHeading(
                  LocalizationUtils.getSingleValueString("common", "common.label.prescription-id") + ": " + prescription.id.toString(),
                  isBold: false),
            ));
            if (prescription.medications != null && prescription.medications!.length > 0) {
              prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              prescriptionDetailsChildren.add(getMedications(prescription));
            }
            if (prescription.documents != null && prescription.documents!.length > 0) {
              prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              prescriptionDetailsChildren.add(getDocuments(context, prescription));
            }
            if (prescription.exPharmacyName != null || prescription.exPharmacyAddress != null || prescription.exPharmacyPhone != null) {
              prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
              prescriptionDetailsChildren.add(getTransferDetails(prescription));
            }

            return SingleChildScrollView(
              child: Column(children: prescriptionDetailsChildren),
            );
          },
        ),
      );
    } else {
      return Container();
    }
  }

  Widget getMedications(Prescription prescription) {
    List<Widget> medicationsChildren = [];
    if (prescription.medications!.length > 0) {
      medicationsChildren.add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.prescription.medications")));
      medicationsChildren.add(PPUIHelper.verticalSpaceMedium());
      for (int i = 0; i < prescription.medications!.length; i++) {
        List<Widget> medicinesColumn = [];
        PrescriptionMedication med = prescription.medications!.elementAt(i);
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        medicinesColumn.add(PPTexts.getTertiaryHeading(med.medicationName != null ? StringUtils.capitalize(med.medicationName!) : "", isBold: true));
        if (med != null) {
          medicinesColumn.add(
              PPTexts.getDescription(med.doctorFirstName != null ? med.doctorFirstName! : "" + " " + (med.doctorLastName != null ? med.doctorLastName! : "")));
        }
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        medicinesColumn.add(
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    PPTexts.getDescription(LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-filled").toUpperCase()),
                    PPTexts.getTertiaryHeading(r'' + Utils.removeDecimalZeroFormat(med.filledQty!))
                  ],
                ),
              ),
              //SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    PPTexts.getDescription(LocalizationUtils.getSingleValueString("medications", "medications.label.quantity-left").toUpperCase()),
                    PPTexts.getTertiaryHeading(r'' + Utils.removeDecimalZeroFormat(med.remainingQty!))
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    PPTexts.getDescription(LocalizationUtils.getSingleValueString("medications", "medications.label.valid-until").toUpperCase()),
                    PPTexts.getTertiaryHeading(r'' + PPDateUtils.displayDate(med.validTill))
                  ],
                ),
              )
            ],
          ),
        );
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        if (i != prescription.medications!.length - 1) medicinesColumn.add(Divider());

        medicationsChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Container(
            child: Column(children: medicinesColumn),
          ),
        ));
      }
    }
    return Container(child: Column(children: medicationsChildren));
  }

  Widget getTransferDetails(Prescription? prescription) {
    List<Widget> transferringPharmacyDetailsChildren = [];

    transferringPharmacyDetailsChildren
        .add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.prescription.transfer-label")));
    transferringPharmacyDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
    if (prescription!.exPharmacyName != null)
      transferringPharmacyDetailsChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: PPTexts.getSecondaryHeading(
            prescription.exPharmacyName!,
            isBold: true,
          )));
    if (prescription.exPharmacyAddress != null && prescription.exPharmacyAddress!.isNotEmpty)
      transferringPharmacyDetailsChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: PPTexts.getSecondaryHeading(prescription.exPharmacyAddress.toString(), isBold: false)));
    if (prescription.exPharmacyPhone != null && prescription.exPharmacyPhone != 0)
      transferringPharmacyDetailsChildren.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: PPTexts.getSecondaryHeading(
              LocalizationUtils.getSingleValueString("modals", "modals.prescription.phone") + ": " + prescription.exPharmacyPhone.toString(),
              isBold: false)));

    return Container(child: Column(children: transferringPharmacyDetailsChildren));
  }

  Widget getDocuments(BuildContext context, Prescription prescription) {
    List<Widget> documentsChildren = [];
    List<Widget> docsChildren = [];
    for (int i = 0; prescription.documents != null && i < prescription.documents!.length; i++) {
      if (prescription.documents![i].documentType == DocumentsType.IMAGE) {
        docsChildren.add(new GestureDetector(
            key: Key("document" + i.toString()),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => ImageViewer(image: NetworkImage(prescription.documents![i].documentPath!)),
                ),
              );
            },
            child: Container(height: 100, child: Image.network(prescription.documents![i].documentPath!, height: 100))));
      } else if (prescription.documents![i].documentType == DocumentsType.PDF) {
        print(prescription.documents![i].documentPath);
        docsChildren.add(new GestureDetector(
            key: Key("document" + i.toString()),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => PDFViewer(pdfUrl: prescription.documents![i].documentPath, order: false),
                ),
              );
            },
            child: Container(height: 100, child: new Icon(Icons.picture_as_pdf, size: 100))));
      } else {
        docsChildren.add(new GestureDetector(
            key: Key("document" + i.toString()),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => ImageViewer(image: NetworkImage(prescription.documents![i].documentPath!)),
                ),
              );
            },
            child: Container(height: 100, child: Image.network(prescription.documents![i].documentPath!, height: 100))));
      }
    }
    documentsChildren.add(PPTexts.getSectionHeader(text: LocalizationUtils.getSingleValueString("modals", "modals.prescription.documents")));
    if (prescription.documents != null && prescription.documents!.length > 0) {
      documentsChildren.add(
//        Image.network(prescription.documents[0].documentPath,width: 100,height: 100,)
          Padding(
        padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
        child: Container(
          height: (MediaQuery.of(context).size.width / 3) * ((docsChildren.length / 3).ceil()),
          child: GridView.count(
            //mainAxisSpacing: PPUIHelper.HorizontalSpaceMedium,
            mainAxisSpacing: 0,
            crossAxisCount: 3,
            children: docsChildren,
          ),
        ),
      ));
    }
    return Container(child: Column(children: documentsChildren));
  }

  Widget getPrescriptionCardHeading(Prescription prescription) {
    String status = ViewConstants.getPrescriptionStatus(prescription.status);
    if (status.isNotEmpty && status.length > 1) {
      status = '${status[0].toUpperCase()}${status.substring(1).toLowerCase()}';
      String type = ViewConstants.getPrescriptionType(prescription.type!);
      //type = '${type[0].toUpperCase()}${type.substring(1).toLowerCase()}';
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Expanded(child: PPTexts.getHeading(type))],
      );
    } else {
      return Container();
    }
  }
}
