import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
export 'package:pocketpills/utils/analytics_event_constant.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> with WidgetsBindingObserver {
  String errMessage = "Please address the issues above to proceed.";
  String successMessage = "Action completed successfully.";
  Widget? content;
  bool showingError = false;
  static bool app_open = true;

  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    if (dataStore != null) super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      app_open = true;
      // went to Background
    }
    if (state == AppLifecycleState.resumed) {
      if (app_open == true) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.app_open);
        app_open = false;
      }
    }
  }

  @override
  void dispose() {
    analyticsEvents.flush();
    WidgetsBinding.instance!.removeObserver(this);
    app_open = PPApplication.logout == false ? true : false;
    super.dispose();
  }

  onFail(context, {errMessage, content}) {
    if (errMessage == null || errMessage == "") errMessage = LocalizationUtils.getSingleValueString("common", "common.label.please-address-issue");
    if (content == null)
      content = Text(errMessage,
          style: TextStyle(
            fontFamily: "FSJoeyPro",
          ));
    final snackBar = SnackBar(
      content: content,
      duration: const Duration(seconds: 3),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }

  showOnSnackBar(context, {successMessage}) {
    if (successMessage == null || successMessage == "") successMessage = LocalizationUtils.getSingleValueString("common", "common.label.action-completed-msg");
    final snackBar = SnackBar(
      content: Text(successMessage, style: TextStyle(fontFamily: "FSJoeyPro")),
      duration: const Duration(seconds: 3),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
