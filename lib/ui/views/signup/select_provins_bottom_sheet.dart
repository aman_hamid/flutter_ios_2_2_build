import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request_skip.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/viewmodels/signup/bottom_sheet_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SelectProvinsSheet extends StatefulWidget {
  BuildContext? context;
  SignUpTransferModel? model;
  BaseStepperSource? source;
  UserPatient? userPatient;
  String? prescriptionMedicalConditions;
  String? prescriptionComment;

  SelectProvinsSheet({
    this.context,
    this.model,
    this.source,
    this.userPatient,
    this.prescriptionMedicalConditions,
    this.prescriptionComment,
  });

  _SelectProvinsSheetState createState() => _SelectProvinsSheetState();
}

class _SelectProvinsSheetState extends BaseState<SelectProvinsSheet> with SingleTickerProviderStateMixin {
  late BottomSheetModel bottomSheetModel;
  int provinceValue = -1;
  String? selectProvinceError;
  bool autovalidate = false;
  bool province = false;

  @override
  void initState() {
    bottomSheetModel = BottomSheetModel();
    widget.model ?? SignUpTransferModel();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext bc) {
    return FutureBuilder(
        future: myFutureMethodOverall(bottomSheetModel),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            bottomSheetModel.getProvinceList();
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(BottomSheetModel model) async {
    Future<List<TelehealthProvinceItem>?> future1 = model.fetchPrescriptProvinceList();
    Future<Map<String, dynamic>?> future2 = model.getLocalization(["signup", "modal"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget getMainView() {
    return SafeArea(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("modal", "modal.selectprovince.title"), textAlign: TextAlign.center),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          SizedBox(
            height: SMALL,
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.59,
            child: Padding(
              padding: const EdgeInsets.only(right: 2.0),
              child: Scrollbar(
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  child: Wrap(
                    key: Key("countryListView"),
                    children: [
                      Column(
                        children: <Widget>[
                          SizedBox(
                            height: MEDIUM,
                          ),
                          getDosageTiles(bottomSheetModel),
                          selectProvinceError == null ? SizedBox(height: 2) : PPTexts.getFormError(selectProvinceError!, color: errorColor),
                          Padding(
                            padding: const EdgeInsets.only(left: MEDIUM_X),
                            child: SizedBox(
                              height: REGULAR_XXX,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: MEDIUM,
          ),
          bottomSheetModel.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Padding(
                  padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SecondaryButton(
                        key: Key("skipButton"),
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("modal", "modal.all.go-home"),
                        onPressed: () {
                          //onSkipClick(model);
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(
                        width: PPUIHelper.HorizontalSpaceSmall,
                      ),
                      PrimaryButton(
                        key: Key("continueButton"),
                        isExpanded: true,
                        text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-continue"),
                        onPressed: () {
                          if (province == false) {
                            province = true;
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_provincelist_entered);
                          }
                          if (provinceValue != -1) {
                            uploadDetails(context, bottomSheetModel.provinceArrayList[provinceValue]);
                            setState(() {
                              autovalidate = true;
                            });
                          } else {
                            setState(() {
                              selectProvinceError = LocalizationUtils.getSingleValueString("signup", "signup.fields.error-required");
                              autovalidate = false;
                            });
                          }
                        },
                      )
                    ],
                  ),
                ),
          SizedBox(
            height: MEDIUM_XXX,
          ),
        ],
      ),
    );
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget getDosageTiles(BottomSheetModel model) {
    List<Widget> tiles = [];
    for (int i = 0; i < model.provinceArrayList.length; i++) {
      TelehealthProvinceItem suggestedEmployer = model.provinceArrayList[i];
      tiles.add(RadioListTile(
        key: Key("country" + i.toString()),
        title: Text(
          getProvinceNameByLanguage(suggestedEmployer.value),
          style: MEDIUM_XXX_PRIMARY_BOLD,
        ),
        value: i,
        activeColor: brandColor,
        groupValue: provinceValue,
        onChanged: (int? val) {
          setState(() {
            provinceValue = val!;
            selectProvinceError = null;
            autovalidate = false;
          });
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }

  void uploadDetails(
    BuildContext context,
    TelehealthProvinceItem telehealthProvinceItem,
  ) async {
    bool connectivityResult = await bottomSheetModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: bottomSheetModel.noInternetConnection);
      return;
    }
    await dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, telehealthProvinceItem.value);
    PPApplication.province = telehealthProvinceItem.value;
    await dataStore.writeBoolean(DataStoreService.TELEHEALTH_APPT_TIME_ENABLED, telehealthProvinceItem.appointmentSlotsEnabled ?? false);
    await dataStore.writeBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED, telehealthProvinceItem.showHealthCardScreen ?? false);

    if (widget.source != null && widget.source == BaseStepperSource.HEALTH_CARD_SIGNUP_SCREEN) {
      String province = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE)!.replaceAll(' ', '_').toUpperCase();
      bool success = await bottomSheetModel.updateProvince(province);
      if (success) {
        Navigator.pop(context);
      }
      return;
    } else if (widget.source != null && widget.source == BaseStepperSource.APPOINTMENT_DATE_SCREEN) {
      String province = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE)!.replaceAll(' ', '_').toUpperCase();
      bool success = await bottomSheetModel.updateProvince(province);
      if (success) {
        if (telehealthProvinceItem.appointmentSlotsEnabled == false) {
          skipTimeSlotAppointment();
        } else {

          Navigator.pushNamedAndRemoveUntil(context, AppointmentDateWidget.routeName, (route) => false,
              arguments: TelehealthArguments(
                from: 'telehealth',
                userPatient: widget.userPatient,
                modelSignUp: widget.model,
                source: BaseStepperSource.NEW_USER,
                prescriptionMedicalConditions: widget.prescriptionMedicalConditions,
                prescriptionComment: widget.prescriptionComment,
              ));
        }
      } else {
        Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }
    } else if (widget.source != null && widget.source == BaseStepperSource.TELEHEALTH_SCREEN) {
      String province = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE)!.replaceAll(' ', '_').toUpperCase();
      bool success = await bottomSheetModel.updateProvince(province);
      if (success) {
        Navigator.pop(context);
      } else {
        Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }
    } else {
      bool success1 = await bottomSheetModel.updatePrescriptionFlow("TELEHEALTH");
      bool success = await bottomSheetModel.telehealthProvince(telehealthProvinceItem.value);
      if (success) {
        setState(() {
          bottomSheetModel.setState(ViewState.Idle);
        });
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_appointment_province_submit);
        Navigator.pop(context);
        Navigator.pushNamed(context, TelehealthPreference.routeName,
            arguments: TelehealthArguments(
              modelSignUp: widget.model,
              source: widget.source,
              userPatient: widget.userPatient,
              from: "telehealth",
            ));
        //showCallAndChatBottomSheet(context,model);
      } else
        Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }

  Future<void> skipTimeSlotAppointment() async {
    //dropValue = null;
    TelehealthSignUpPreferenceRequestSkip telehealthRequest = TelehealthSignUpPreferenceRequestSkip(
        prescriptionState: "FILED",
        type: "TELEHEALTH",
        doctorId: null,
        teleHealthDoctorDayPlanId: null,
        telehealthAppointmentSlotId: null,
        appointmentTime: null);
    bool connectivityResult = await bottomSheetModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: bottomSheetModel.noInternetConnection);
      return;
    }

    bool success = await bottomSheetModel.sendPreferencesSignUpSkip(telehealthRequest);
    if (success) {
      TELEHEALTH_CONDITION = "";
      TELEHEALTH_COMMENT = "";
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_customise_telehealth_select);
      Navigator.pushNamedAndRemoveUntil(context, HealthCardUploadViewSignUp.routeName, (Route<dynamic> route) => false,
          arguments: TelehealthArguments(
              modelSignUp: widget.model,
              source: BaseStepperSource.NEW_USER,
              userPatient: widget.userPatient,
              appointmentDateTime: dataStore.readString(DataStoreService.TELEHEALTH_APPOINTMENT_TIME).toString()));
    } else {
      Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }
}
