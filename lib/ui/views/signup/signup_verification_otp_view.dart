import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/timer_model_bottom_sheet.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/views/signup/telehealth_success_bottom_sheet.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';

class SignUpVerificationOtpPasswordWidget extends StatefulWidget {
  static const routeName = 'signupVerificationPhoneOtpDetailsView';

  SignUpVerificationOtpPasswordWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpOtpPasswordState();
  }
}

class SignUpOtpPasswordState extends BaseState<SignUpVerificationOtpPasswordWidget> {
  final _formKey = GlobalKey<FormState>();
  final DataStoreService dataStore = locator<DataStoreService>();

  final _aboutUsformKey = GlobalKey<FormState>();

  bool autovalidate = false;
  bool verified = false;

  static bool password = false, otp = false;
  String phoneNumber = "";
  String emailAddress = "";

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  final FocusNode _phoneNumberFocusNode = FocusNode();

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _otpController = TextEditingController();

  final FocusNode _passwordFnode = FocusNode();
  final FocusNode _otpFnode = FocusNode();
  final FocusNode _emailFnode = FocusNode();

  @override
  void initState() {
    super.initState();
    if (dataStore.readString(DataStoreService.PHONE) != null && dataStore.readString(DataStoreService.PHONE)!.isNotEmpty) {
      phoneNumber = dataStore.readString(DataStoreService.PHONE)!;
    }
    _passwordController.addListener(passwordListener);
    _otpController.addListener(otpListener);
  }

  @override
  void dispose() {
    _passwordController.removeListener(passwordListener);
    _otpController.removeListener(otpListener);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  otpListener() {
    if (otp == false) {
      otp = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_otp_entered);
    }
  }

  passwordListener() {
    if (password == false) {
      password = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_newpassword_entered);
    }
  }

  _displayDialog(BuildContext context, SignUpAboutYouModel model) async {
    return showDialog(
        context: context,
        builder: (bc) {
          return AlertDialog(
            title: Text(dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED"
                ? LocalizationUtils.getSingleValueString("modal", "modal.editdetails.title-email")
                : LocalizationUtils.getSingleValueString("modal", "modal.editdetails.title-phone")),
            content: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_XXX,
                  ),
                  PPTexts.getFormLabelAsterisk(dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED" ? LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label") : LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-phone"),
                      fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
                  PPUIHelper.verticalSpaceSmall(),
                  Form(
                      key: _formKey,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      child: dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED" ? getEmailField() : getPhoneField()),
                  SizedBox(
                    height: MEDIUM,
                  ),
                  Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.otp.dialog-description"),
                    style: MEDIUM_XX_SECONDARY,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                key: Key("updateField"),
                  child: new Text(LocalizationUtils.getSingleValueString("common", "common.button.update")),
                  onPressed: () async {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }

                    if (_phoneNumberController.text != null && _phoneNumberController.text != "") {
                      bool result = await model.updateAboutYou(phone: _phoneNumberController.text);
                      if (result == true) {
                        await model.resendOtpByIdentifier(_phoneNumberController.text, 'sms', true);
                        setState(() {
                          phoneNumber = _phoneNumberController.text;
                        });
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_number_updated);
                      } else {
                        onFail(context, errMessage: model.errorMessage);
                      }
                      Navigator.of(context).pop();
                    }

                    /*if (dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED") {
                      if (_emailController.text != null && _emailController.text != "") {
                        bool result = await model.updateAboutYou(email: _emailController.text);
                        if (result == true) {
                          await model.resendOtpByIdentifier(_emailController.text, 'sms', true);
                          setState(() {
                            emailAddress = _emailController.text;
                          });
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.email_updated);
                        } else {
                          onFail(context, errMessage: model.errorMessage);
                        }
                        Navigator.of(context).pop();
                      }
                    } else {
                      if (_phoneNumberController.text != null && _phoneNumberController.text != "") {
                        bool result = await model.updateAboutYou(phone: _phoneNumberController.text);
                        if (result == true) {
                          await model.resendOtpByIdentifier(_phoneNumberController.text, 'sms', true);
                          setState(() {
                            phoneNumber = _phoneNumberController.text;
                          });
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_number_updated);
                        } else {
                          onFail(context, errMessage: model.errorMessage);
                        }
                        Navigator.of(context).pop();
                      }*/
                    //}
                  })
            ],
          );
        });
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        autovalidate: false,
        keyboardType: TextInputType.emailAddress,
        focusNode: _emailFnode,
        controller: _emailController,
        textInputAction: TextInputAction.done,
        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
        decoration: PPInputDecor.getDecoration(
            labelText: "",
            hintText: "",floatingLabelBehavior: FloatingLabelBehavior.never),
        onFieldSubmitted: (value) {});
  }

  TextFormField getPhoneField() {
    return PPFormFields.getNumericFormField(
        autovalidate: false,
        maxLength: 10,
        keyboardType: TextInputType.phone,
        minLength: 10,
        textInputAction: TextInputAction.next,
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocusNode,
        errorText: '',
        decoration: PPInputDecor.getDecoration(
          labelText: "",
          hintText: "",
          prefixIcon: Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                '+1 |',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "FSJoeyPro Medium",
                ),
              )),
        ),
        floatingLabelBehavior: FloatingLabelBehavior.never,
        onFieldSubmitted: (value) {},
        validator: (value) {
          if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString('signup', "signup.fields.cell-error1");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString('signup', "signup.fields.cell-error2");
          return null;
        });
  }

  Widget getPasswordField(SignUpAboutYouModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: MEDIUM_XXX),
        PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.newpassword-label"),
            fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
        PPUIHelper.verticalSpaceSmall(),
        PPFormFields.getPasswordFormField(
            key: Key("signup_password"),
            focusNode: _passwordFnode,
            minLength: 8,
            labelText: "",
            hintText: "",
            helperText: LocalizationUtils.getSingleValueString("signup", "signup.fields.newpassword-error"),
            autovalidate: autovalidate,
            controller: _passwordController,
            textInputAction: TextInputAction.done,
            onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
            onFieldSubmitted: (value) {
              onNextClick(context, phoneNumber, model);
            },
            validator: (value) {
              if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
              String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
              if (formattedNumber.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
              return null;
            }),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<SignUpAboutYouModel>(create: (_) => SignUpAboutYouModel())],
      child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel signUpAboutYouModel, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(signUpAboutYouModel),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(signUpAboutYouModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(SignUpAboutYouModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "login", "forgot"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(SignUpAboutYouModel signUpAboutYouModel) {
    return BaseScaffold(
      key: Key("loginOTPView"),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
        leading: Container(),
      ),
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                children: <Widget>[
                  Expanded(child: SingleChildScrollView(child: getStartedView(context, signUpAboutYouModel))),
                  Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                            child: signUpAboutYouModel.state == ViewState.Busy
                                ? ViewConstants.progressIndicator
                                : PrimaryButton(
                                    key: Key("nextButton"),
                                    text: LocalizationUtils.getSingleValueString("signup", "signup.employer-consent.next"),
                                    onPressed: () {
                                      onNextClick(context, phoneNumber, signUpAboutYouModel);
                                    },
                                    fullWidth: true),
                          ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getStartedView(BuildContext context, SignUpAboutYouModel model) {
    return getAboutUsView(context, model);
  }

  Widget getAboutUsView(BuildContext context, SignUpAboutYouModel model) {
    return Form(
      key: _aboutUsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      pharmacistFlow(),
                    ],
                  )),
              Container(
                width: MediaQuery.of(context).size.width,
                color: lightGray,
                child: Padding(
                  padding: const EdgeInsets.only(left: MEDIUM_XXX),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start, //change here don't //worked
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        LocalizationUtils.getSingleValueString('signup', "signup.otp.verify-label") + " : ",
                        style: TextStyle(color: brandColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium"),
                      ),
                      Text(
                        dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED" ? emailAddress : phoneNumber,
                        style: TextStyle(color: brandColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium"),
                      ),
                      Spacer(),
                      InkWell(
                        key: Key("changeField"),
                        onTap: () {
                          dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED"
                              ? analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.edit_email_click)
                              : analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.edit_phone_click);
                          _displayDialog(context, model);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                            LocalizationUtils.getSingleValueString("signup", "signup.otp.phone-change"),
                            style: TextStyle(color: brandColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Heavy"),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: MEDIUM_XXX),
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                    PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.otp-label"),
                        fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
                    PPUIHelper.verticalSpaceSmall(),
                    Stack(children: <Widget>[
                      PPFormFields.getNumericFormField(
                        key: Key("otpView"),
                          focusNode: _otpFnode,
                          minLength: 4,
                          maxLength: 4,
                          autovalidate: autovalidate,
                          controller: _otpController,
                          textInputAction: TextInputAction.next,
                          errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                          decoration: PPInputDecor.getDecoration(
                              labelText: '',
                              hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.otp-label"),
                              helperText: "",
                              floatingLabelBehavior: FloatingLabelBehavior.never
                          ),
                          onFieldSubmitted: (value) {
                            _fieldFocusChange(context, _otpFnode, _passwordFnode);
                          },
                          validator: (value) {
                            if (value!.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
                            if (value.length < 4) return LocalizationUtils.getSingleValueString("signup", "signup.fields.otp-error");
                            return null;
                          }),
                      Align(
                        alignment: Alignment.centerRight,
                        child: FlatButton(
                          key: Key("resendCall"),
                          color: Colors.transparent,
                          child: Text(
                            LocalizationUtils.getSingleValueString("signup", "signup.otp.resend-call"),
                            style: MEDIUM_XX_LINK,
                          ),
                          onPressed: () async {
                            showCancelSubscriptionBottomSheet(context, model);
                          },
                        ),
                      )
                    ]),
                    SizedBox(
                      height: MEDIUM,
                    ),
                    getPasswordField(model),
                    SizedBox(height: MEDIUM),
                  ])),
              SizedBox(height: REGULAR_XXX),
            ],
          ),
        ],
      ),
    );
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.otp.description-not-guest-with-password"),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  Widget normalFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.otp.title")),
        SizedBox(height: SMALL_X),
        PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.otp.description-with-password"), isBold: false),
        SizedBox(height: MEDIUM_XXX),
      ],
    );
  }

  onNextClick(context, String phoneNumber, SignUpAboutYouModel signUpAboutYouModel) async {
    setState(() {
      autovalidate = true;
    });
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }

    if (_aboutUsformKey.currentState!.validate()) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      var result = await signUpAboutYouModel.otpVerifyIdentifier(phoneNumber, _otpController.text, false);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_otp_verification);
      dataStore.writeBoolean(DataStoreService.VERIFIED, true);
      if (result != null) {
        var success = await signUpAboutYouModel.setPassword(_passwordController.text);
        if (success) {
          bool? succesGetUserInfo = await signUpAboutYouModel.getUserInfo(listenable: true);
          if (succesGetUserInfo == true) {
            Navigator.pop(context);
            Provider.of<SignUpModel>(context, listen: false).getSignUpFlow2(context);
          } else {
            onFail(context, errMessage: signUpAboutYouModel.errorMessage);
          }
        } else {
          onFail(context, errMessage: signUpAboutYouModel.errorMessage);
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_otp_verification_failed);
        onFail(context, errMessage: signUpAboutYouModel.errorMessage);
      }
    }
  }

  showCancelSubscriptionBottomSheet(BuildContext context, SignUpAboutYouModel model) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return TimerModalBottomSheet(
            phoneNumber: phoneNumber,
            model: model,
            context: context,
          );
        });
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
