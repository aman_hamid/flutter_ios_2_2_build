import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/home/medicine_detail_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription_arguments.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/youtube_bottom_sheet.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:provider/provider.dart';

class SkipSignupSheet extends StatefulWidget {
  BuildContext? context;
  SignUpTransferModel? model;
  BaseStepperSource? source;
  UserPatient? userPatient;
  String? from;

  SkipSignupSheet({this.context, this.model, this.source, this.userPatient, this.from});

  _SkipSignupSheetState createState() => _SkipSignupSheetState();
}

class _SkipSignupSheetState extends BaseState<SkipSignupSheet> with SingleTickerProviderStateMixin {
  SignUpTransferModel? model;
  late BuildContext context;

  _SkipSignupSheetState();

  late SignUpTransferModel model2;

  bool doctorSelected = false, uploadSelected = false, medicationSelected = false;

  @override
  void initState() {
    super.initState();
    model = widget.model;
    model2 = SignUpTransferModel();
    context = widget.context!;
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dropoff_popup_open);
  }

  @override
  void dispose() {
    super.dispose();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dropoff_popup_close);
  }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model2),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(SignUpTransferModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["common", "signup", "dropoff"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Wrap(
      key: Key("bottomSheetView"),
      children: [
        Container(
          child: Column(
            children: [
              Container(
                height: 45,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  clipBehavior: Clip.none,
                  children: [
                    Container(
                      decoration: new BoxDecoration(
                          color: brandColor, //new Color.fromRGBO(255, 0, 0, 0.0),
                          borderRadius: new BorderRadius.only(topLeft: const Radius.circular(10.0), topRight: const Radius.circular(10.0))),
                    ),
                    Positioned(
                      top: -40,
                      child: CircleAvatar(
                          radius: 40,
                          backgroundColor: lightBlueColor,
                          child: CircleAvatar(
                            radius: 36,
                            backgroundColor: brandColor,
                            backgroundImage: CachedNetworkImageProvider(
                              "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg",
                            ),
                          )),
                    ),
                  ],
                ),
              ),
              Container(
                color: whiteColor,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      color: brandColor,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.all.transfer-title").isEmpty
                              ? "Its Cathy, your pharmacist!"
                              : LocalizationUtils.getSingleValueString("dropoff", "dropoff.all.transfer-title")
                                  .replaceAll("{{phamacistname}}", ViewConstants.pharmacistName),
                          style: TextStyle(fontSize: 21.0, color: whiteColor, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro"),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.all.transfer-description"),
                          style: TextStyle(fontSize: 18.0, color: whiteColor, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Bold"),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                      ]),
                    ),
                    /*(widget.from != "preference_sheet" && !LocalizationUtils.isProvinceQuebec())
                        ? InkWell(
                            key: Key("telehealthFlow"),
                            onTap: () {
                              if (doctorSelected == false) {
                                if (widget.from == 'customize-signup') {
                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_customise_telehealth_select);
                                }
                                doctorSelected = true;
                                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dropoff_popup_telehealth);
                              }
                              Navigator.pop(context);
                              model!.setProvincePop("clicked");
                              PPApplication.provinceArrayListMemo!.any((element) => (element.label.contains(PPApplication.province!)))
                                  ? goToNextScreen(context, model ?? model2)
                                  : showCallAndChatBottomSheet(context, model ?? model2);
                            },
                            child: bottomSheetItems(
                              Icons.record_voice_over_outlined,
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-telehealth-title"),
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-telehealth-description"),
                            ),
                          )
                        : Container(),
                    !LocalizationUtils.isProvinceQuebec() ? lineDivider : Container(),*/
                    (widget.from != "preference_sheet" && !LocalizationUtils.isProvinceQuebec())
                        ? InkWell(
                            key: Key("searchMedicine"),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, MedicineDetailWidget.routeName,
                                  arguments: UploadPrescriptionArguments(
                                    from: "telehealth",
                                    source: widget.source,
                                    model: model,
                                    userPatient: widget.userPatient,
                                  ));
                            },
                            child: bottomSheetItems(
                              Icons.search,
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-search-title"),
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-search-description"),
                            ),
                          )
                        : Container(),
                    !LocalizationUtils.isProvinceQuebec() ? lineDivider : Container(),
                    !LocalizationUtils.isProvinceQuebec()
                        ? InkWell(
                            key: Key("openVideo"),
                            onTap: () {
                              Navigator.pop(context);
                              openVideoPlayer();
                            },
                            child: bottomSheetItems(
                              Icons.play_circle_outline_outlined,
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-video-title"),
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-video-description"),
                            ),
                          )
                        : Container(),
                    widget.from == "preference_sheet"
                        ? InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              // Navigator.of(context).pushNamedAndRemoveUntil(TransferWidget.routeName);
                              Navigator.of(context).pushNamedAndRemoveUntil(TransferWidget.routeName, (Route<dynamic> route) => false,
                                  arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
                            },
                            child: bottomSheetItems(
                              Icons.post_add_outlined,
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-title"),
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-description"),
                            ),
                          )
                        : Container(),
                    !LocalizationUtils.isProvinceQuebec() ? lineDivider : Container(),
                    LocalizationUtils.isProvinceQuebec()
                        ? SizedBox(
                            height: 5,
                          )
                        : Container(),
                    InkWell(
                      key: Key("signup others"),
                      onTap: () {
                        Navigator.pop(context);
                        onSkipClick(model ?? model2);
                      },
                      child: bottomSheetItems(
                        Icons.group_add_outlined,
                        LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.add-member-title"),
                        LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.add-member-description"),
                      ),
                    ),
                    widget.from == "preference_sheet"
                        ? InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              // Navigator.of(context).pushNamedAndRemoveUntil(TransferWidget.routeName);
                              Navigator.of(context).pushNamedAndRemoveUntil(TransferWidget.routeName, (Route<dynamic> route) => false,
                                  arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
                            },
                            child: bottomSheetItems(
                              Icons.post_add_outlined,
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-title"),
                              LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-description"),
                            ),
                          )
                        : Container(),
                    SizedBox(height: 20.0),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                      child: RaisedButton(
                        key: Key("closePopUp"),
                        child: Text(
                          LocalizationUtils.getSingleValueString("common", "common.button.close"),
                          style: TextStyle(color: Colors.grey, fontFamily: "FSJoeyPro Bold"),
                        ),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: tertiaryColor),
                          borderRadius: BorderRadius.circular(SMALL_XX),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                          //goToNextScreen(model);
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  openVideoPlayer() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: false,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return YouTubeSheet(
            context: context,
          );
        });
  }

  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return SelectProvinsSheet(context: context, model: model, source: widget.source, userPatient: widget.userPatient);
        });
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget bottomSheetItems(IconData icons, String text, String subtext) {
    return Wrap(
      children: [
        Container(
          width: double.infinity,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: Row(
              children: [
                Icon(
                  icons,
                  color: darkBlueColor2,
                ),
                SizedBox(width: 15.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          text,
                          style: TextStyle(fontSize: 16.0, color: darkBlueColor2, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Bold"),
                        ),
                      ),
                      SizedBox(height: 1.0),
                      Wrap(
                        children: [
                          Container(
                            child: Text(
                              subtext,
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 14.4, color: darkBlueColor2, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro"),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  void goToNextScreen(BuildContext context, SignUpTransferModel signUpTransferModel) async {
    bool connectivityResult = await model!.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model!.noInternetConnection);
      return;
    }
    bool success = await sendProvince(signUpTransferModel);

    if (success) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_appointment_province_submit);
      await dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, PPApplication.province!);
      Navigator.pushNamed(context, TelehealthPreference.routeName,
          arguments: TelehealthArguments(
            modelSignUp: widget.model,
            source: widget.source,
            userPatient: widget.userPatient,
            from: "telehealth",
          ));

      //showCallAndChatBottomSheet(context,model);
    } else {
      model!.showSnackBar(context, LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }

  Future<bool> sendProvince(SignUpTransferModel model) async {
    return await model.telehealthProvince(PPApplication.province!);
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.NEW_USER || widget.source == BaseStepperSource.ADD_PATIENT) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_skipped);
      bool success = await model.signupOthersFlow("NONE","REGULAR");
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreenSkip(model);
      }
    } else
      Navigator.pop(context);
  }

  void goToNextScreenSkip(SignUpTransferModel model) {
    dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
    Navigator.of(context).pushNamed(SignUpAlmostDoneWidget.routeName,
        arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER,from: "add others"));
  }
}
