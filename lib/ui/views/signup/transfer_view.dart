import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/transfer/transfer_search_selection_model.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/chips/chiplist_sample.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/customised_signup.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:flutter/gestures.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:url_launcher/url_launcher.dart';

class TransferWidget extends StatefulWidget {
  static const routeName = 'transfer';
  final BaseStepperSource? source;
  final UserPatient? userPatient;
  final Function? onSuccess;
  final Function? onBack;
  final String? transferAbTest;
  final String? medicineName;
  final double? quantity;
  final bool? chambersFlow;
  final TransferSearchSelectModel? selectedModel;

  TransferWidget({Key? key,
    this.source = BaseStepperSource.MAIN_SCREEN,
    this.onBack,
    this.onSuccess,
    this.userPatient,
    this.transferAbTest,
    this.medicineName,
    this.quantity,
    this.selectedModel,
    this.chambersFlow = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TransferState();
  }
}

class TransferState extends BaseState<TransferWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  bool autovalidate = false;
  bool pharmacyShowed = false;
  bool showQuebec = false;
  bool defaultValue = true;
  Key? popularSearchKey;
  final double _horizontalSpacing = PPUIHelper.HorizontalSpaceMedium;

  final TextEditingController _commentsController = TextEditingController();

  bool newUserFlow = false;
  bool transferAll = true,
      pharmacySubmitted = false;
  bool pharmacyName = false,
      pharmacyAddress = false,
      pharmacyPhone = false,
      pharmacyComment = false;

  BuildContext? innerContext;

  String province = "";
  String postal_code = "";
  String selectedPlaceId = '';
  ChoiceChips? chipItem;

  @override
  void initState() {
    super.initState();
    dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_1);
    _commentsController.addListener(pharmacyCommentListener);
    popularSearchKey = UniqueKey();
  }

  @override
  void dispose() {
    _commentsController.removeListener(pharmacyCommentListener);
    super.dispose();
  }

  pharmacyCommentListener() {
    if (pharmacyComment == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyComment = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_comment_entered);
    }
  }

  UserPatient? getUserPatientTuple() {
    UserPatient? up = widget.userPatient != null ? widget.userPatient! : Provider
        .of<DashboardModel>(context, listen: false)
        .selectedPatient;
    return up;
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.source == BaseStepperSource.MAIN_SCREEN ||
          widget.source == BaseStepperSource.ADD_PATIENT ||
          widget.source == BaseStepperSource.CONSENT_FLOW ||
          widget.source == BaseStepperSource.COPAY_REQUEST
          ? () async {
        Navigator.pop(context);
        return true;
      }
          : widget.source == BaseStepperSource.NEW_USER
          ? () async {
        await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, true);
        Navigator.canPop(context)? Navigator.pop(context) : Navigator.pushNamedAndRemoveUntil(context,CustomisedSignUpWidget.routeName,(Route<dynamic> route) => false,arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        return true;
      }
          : () => _onBackPressed(context),
      child: ChangeNotifierProvider(
        create: (_) => SignUpTransferModel(source: widget.source, searchNearBy: true, isDialogDisplayed: false),
        child: Consumer<SignUpTransferModel>(builder: (BuildContext context, SignUpTransferModel model, Widget? child) {
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  if (widget.selectedModel != null && !pharmacyShowed) {
                    Future.delayed(const Duration(milliseconds: 500), () {
                      showTransferBottomSheet(context, model, widget.selectedModel!, "");
                    });
                  }
                  return getTopView(getName(), model, getTransferHeading(), getTransferDescription(), getTransferButtonDescription());
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(SignUpTransferModel signUpTransferModel) async {
    Future<Map<String, dynamic>?> future1 = signUpTransferModel.getLocalization(["signup", "transfer", "dropoff", "modal", "common", "modals", "copay"]);
    Future<List<String>?> future2 = signUpTransferModel.getPopularPharmacy();
    return await Future.wait([future1, future2]);
  }

  String getTransferHeading() {
    return widget.source != BaseStepperSource.COPAY_REQUEST
        ? getTitleText(PatientUtils.getForGender(Provider
        .of<DashboardModel>(context, listen: false)
        .selectedPatient))
        : LocalizationUtils.getSingleValueString("signup", "signup.transfer.title-employer");
  }

  String getTransferDescription() {
    return widget.source != BaseStepperSource.COPAY_REQUEST
        ? getDescriptionText(PatientUtils.getForGender(Provider
        .of<DashboardModel>(context, listen: false)
        .selectedPatient))
        : LocalizationUtils.getSingleValueString("signup", "signup.transfer.description-employer");
  }

  String getTransferButtonDescription() {
    return widget.source != BaseStepperSource.COPAY_REQUEST
        ? getPlaceHolderDescriptionText(PatientUtils.getForGender(Provider
        .of<DashboardModel>(context, listen: false)
        .selectedPatient))
        : LocalizationUtils.getSingleValueString("signup", "signup.transfer.search-placeholder");
  }

  String getName() {
    String? name = PatientUtils.getYouOrNameTitle(getUserPatientTuple());
    name = '${name[0].toUpperCase()}${name.substring(1)}';
    return name;
  }

  Widget? getAppbar(String name, SignUpTransferModel model, String transferHeading, String transferDescription) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      return AppBar(
          backgroundColor: Colors.white,
          title: isProvinceQuebec() == true
              ? Image.network(
            "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
            width: MediaQuery
                .of(context)
                .size
                .width * 0.45,
            fit: BoxFit.cover,
          )
              : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery
              .of(context)
              .size
              .width * 0.45),
          centerTitle: true,
          leading: IconButton(
            key: Key("back"),
            icon: Icon(Icons.arrow_back_rounded),color: brandColor,
            onPressed: () {
              Navigator.canPop(context)? Navigator.pop(context) : Navigator.pushNamedAndRemoveUntil(context,CustomisedSignUpWidget.routeName,(Route<dynamic> route) => false,arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
            },
          )
      );
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN ||
        widget.source == BaseStepperSource.ADD_PATIENT ||
        widget.source == BaseStepperSource.CONSENT_FLOW) {
      return InnerAppBar(
        titleText: name,
        appBar: AppBar(),
        leadingBackButton: () {
          if (widget.source == BaseStepperSource.ADD_PATIENT) {
            onSkipClick(model);
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          }
        },
      );
    }
    if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      return InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.title-copay-request"),
        appBar: AppBar(),
        leadingBackButton: () {
          Navigator.pop(context);
        },
      );
    }
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget getTopView(String name, SignUpTransferModel model, String transferHeading, String transferDescription, String transferButtonDescription) {
    return BaseScaffold(
      key: Key("TransferView"),
      appBar: getAppbar(name, model, transferHeading, transferDescription),
      body: Builder(
        builder: (BuildContext context) {
          innerContext = context;
          return Container(
            child: Builder(
              builder: (BuildContext context) {
                return getMainView(model, transferHeading, transferDescription, transferButtonDescription);
              },
            ),
          );
        },
      ),
    );
  }

  Widget getMainView(SignUpTransferModel model, String transferHeading, String transferDescription, String transferButtonDescription) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(MEDIUM_XXX),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //PPTexts.getMainViewHeading(transferHeading),
                      widget.source == BaseStepperSource.NEW_USER ? pharmacistFlow() : PPTexts.getMainViewHeading(transferHeading),
                      widget.source != BaseStepperSource.NEW_USER ? normalFlow(transferDescription) : Container(),
                      widget.source == BaseStepperSource.NEW_USER
                          ? SizedBox(
                        height: REGULAR_XXX,
                      )
                          : Container(),
                      dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW)! && widget.source == BaseStepperSource.NEW_USER
                          ? Align(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            FlatButton(
                              child: Text(
                                'NOT YOU?',
                                style: TextStyle(color: brandColor, fontFamily: "FSJoeyPro Bold"),
                              ),
                              onPressed: () async {
                                PPApplication.logout = true;
                                await Provider.of<SignUpModel>(context, listen: false).logout();
                                await Provider.of<SignUpModel>(context, listen: false).updateDeviceDetails();
                                dataStore.writeBoolean(DataStoreService.CHAMBERS_FLOW, false);
                                dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, true);
                                Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
                              },
                            ),
                            SizedBox(height: SMALL_X),
                          ],
                        ),
                      )
                          : Container(),

                      GestureDetector(
                          key: Key("searchClick"),
                          onTap: () {
                            onClickPharmacySearchScreen(model, "");
                          },
                          child: Hero(
                              tag: 'hero-rectangle',
                              transitionOnUserGestures: true,
                              child: Container(
                                padding: const EdgeInsets.all(MEDIUM_X),
                                decoration: BoxDecoration(
                                    border: Border.all(color: brandColor, width: 2),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(SMALL_XX),
                                    )),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      transferButtonDescription.toString(),
                                      style: MEDIUM_XXX_SECONDARY,
                                    ),
                                    Icon(
                                      Icons.search,
                                      color: secondaryColor,
                                    )
                                  ],
                                ),
                              ))),
                      SizedBox(
                        height: MEDIUM_XXX,
                      ),
                      Text(
                        LocalizationUtils.getSingleValueString("transfer", "transfer.search.popular").toUpperCase(),
                        style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
                      ),
                      SizedBox(
                        height: SMALL_XX,
                      ),
                      Container(child: getChipAddView(model)),
                      SizedBox(
                        height: SMALL_XX,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        (widget.source == BaseStepperSource.MAIN_SCREEN || widget.source == BaseStepperSource.COPAY_REQUEST)
            ? SizedBox(
          height: 0.0,
        )
            : model.state == ViewState.Busy
            ? SizedBox(
          height: 0.0,
        )
            : GestureDetector(
            onTap: () {
              widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
                  ? onSkipClick(model)
                  : () {}; //showCallAndChatBottomSheet(context, model);
            },
            child: Container(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: LARGE_XX),
                  child: widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
                      ? Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.transfer.skip"),
                    style: MEDIUM_XXX_LINK_BOLD_MEDIUM,
                  )
                      : PPContainer.emptyContainer(),
                ),
              ),
            ))
      ],
    );
  }

  Widget getChipAddView(SignUpTransferModel model) {
    chipItem = ChoiceChips(
        key: popularSearchKey!,
        chipName: model.popularPharmacyList.isEmpty ? model.popularPharmacies : model.popularPharmacyList,
        selectedItem: (selected) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.Transfer_search_popular_selected);
          setState(() {
            onClickPharmacySearchScreen(model, selected);
            print(selected + "test");
          });
        });
    return chipItem!;
  }

  Widget pharmacistFlow() {
    return Column(
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        !LocalizationUtils.isProvinceQuebec()
            ? Text(
          LocalizationUtils.getSingleValueString("signup", "signup.transfer.title-pharmacist"),
          style: TextStyle(color: darkBlue,
              fontWeight: FontWeight.w500,
              fontSize: 21.6,
              height: 1.5,
              fontFamily: "FSJoeyPro Heavy"),
          textAlign: TextAlign.center,
        )
            : Container(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpacexSmall),
          child: !LocalizationUtils.isProvinceQuebec()
              ? Text(
            LocalizationUtils.getSingleValueString("signup", "signup.transfer.description-pharmacist-new"),
            style: TextStyle(color: darkPrimaryColor,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
                height: 1.5,
                fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          )
              : Text(
            LocalizationUtils.getSingleValueString("signup", "signup.transfer.description-pharmacist"),
            style: TextStyle(color: darkPrimaryColor,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
                height: 1.5,
                fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget normalFlow(String transferDescription) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PPUIHelper.verticalSpaceXSmall(),
        Text(
          transferDescription,
          style: MEDIUM_XXX_SECONDARY,
        ),
        SizedBox(
          height: REGULAR_XXX,
        )
      ],
    );
  }

  bool checkPharmacySelectedDetails(SignUpTransferModel model) {
    return !["", null].contains(model.pharmacyName) && model.pharmacyName != '' && model.pharmacyAddress != null && model.pharmacyAddress != '';
  }

  void onClickPharmacySearchScreen(SignUpTransferModel model, String PharmacyNamePopular) async {
    final result = await Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
        arguments: TransferArguments(
            source: widget.source,
            model: model,
            PharmacyNamePopular: PharmacyNamePopular,
            userPatient: widget.userPatient,
            medicineName: widget.medicineName,
            quantity: widget.quantity));
    if (result != null) {
      if (pharmacyShowed) {
        Navigator.pop(context);
      }

      TransferSearchSelectModel selectedValue = result as TransferSearchSelectModel;

      showTransferBottomSheet(context, model, selectedValue, PharmacyNamePopular);
    }

    if (checkSignupFlow()) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search);
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search);
    }
  }

  Widget showNearbyPharmacyListing(SignUpTransferModel model) {
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          model.pharmacyPredictions.length > 0
              ? Padding(
            padding: const EdgeInsets.all(MEDIUM_X),
            child: Text(
              LocalizationUtils.getSingleValueString("signup", "signup.transfer.nearby-title"),
              style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
            ),
          )
              : PPContainer.emptyContainer(),
          Container(
            height: 80,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: model.pharmacyPredictions.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  Pharmacy pharmacy = model.pharmacyPredictions[index];
                  return Padding(
                    padding: const EdgeInsets.only(left: MEDIUM_X, right: 0.0),
                    child: InkWell(
                      onTap: () {
                        onClickSelectPharmacy(model, pharmacy);
                      },
                      child: Container(
                          decoration:
                          BoxDecoration(color: Color(0xffffffff), border: Border.all(color: borderColor), borderRadius: BorderRadius.circular(SMALL_XXX)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                width: MEDIUM_X,
                              ),
                              getAutoCompleteItem(pharmacy),
                              TransparentButton(
                                text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.nearby-select"),
                                onPressed: () {
                                  onClickSelectPharmacy(model, pharmacy);
                                },
                              )
                            ],
                          )),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  onClickSelectPharmacy(SignUpTransferModel model, Pharmacy pharmacy) async {
    pharmacySubmitted = true;
    PlaceDetails selectedPlace = await model.getPlaceDetails(pharmacy.pharmacyPlaceId??"");
    List<AddressComponent> addressComponents = selectedPlace.addressComponents;
    AddressComponent addressCompone;
    addressComponents.forEach((value) =>
    {
      addressCompone = value,
      if (ViewConstants.province.containsKey(addressCompone.longName)) {province = ViewConstants.province[addressCompone.longName.toString()]!},
    });
    if (selectedPlace != null) {
      selectedPlaceId = selectedPlace.placeId;
      model.pharmacyAddress = selectedPlace.formattedAddress!;
      model.pharmacyName = selectedPlace.name;
      model.pharmacyPhoneNumber = selectedPlace.formattedPhoneNumber!;
      model.province = province;
    }
/*    if (checkSignupFlow()) {
      //analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_nearby_selected);
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_nearby_selected);
    }*/
  }

  Widget getCopayRequestBottomButton(SignUpTransferModel model) {
    return Builder(
        builder: (BuildContext context) =>
            PPBottomBars.getButtonedBottomBar(
                child: Container(
                  color: Colors.white,
                  child: model.state == ViewState.Busy
                      ? ViewConstants.progressIndicator
                      : Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    SecondaryButton(
                      text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.back"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                    PrimaryButton(
                      butttonColor: brandColor,
                      text: LocalizationUtils.getSingleValueString("signup", "signup.upload.continue"),
                      onPressed: () {
                        onTransferClick(context, model);
                      },
                    )
                  ]),
                )));
  }

  Widget getTransferBottomButton(SignUpTransferModel model) {
    return Builder(
      builder: (BuildContext context) =>
          PPBottomBars.getButtonedBottomBar(
            child: model.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : Row(
              children: <Widget>[
                PrimaryButton(
                    borderRadius: LARGE_XX,
                    key: Key("buttonTransfer"),
                    text: (!checkPharmacySelectedDetails(model))
                        ? LocalizationUtils.getSingleValueString("signup", "signup.transfer.transfer")
                        : LocalizationUtils.getSingleValueString("common", "common.button.next"),
                    onPressed: () {
                      onTransferClick(context, model);
                    })
              ],
            ),
          ),
    );
  }

  Widget getButton(SignUpTransferModel model) {
    if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      return getCopayRequestBottomButton(model);
    } else {
      return getTransferBottomButton(model);
    }
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(SMALL_XXX), topRight: Radius.circular(SMALL_XXX)),
        ),
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: _horizontalSpacing, left: PPUIHelper.HorizontalSpaceMedium),
                      child: PPTexts.getHeading(LocalizationUtils.getSingleValueString("transfer", "transfer.all.sample-photo")),
                    ),
                    PPUIHelper.verticalSpaceMedium(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'graphics/transfer_refill/transfer_refill_1.png',
                          height: 240,
                          width: (MediaQuery
                              .of(context)
                              .size
                              .width - 50) / 2,
                        ),
                        PPUIHelper.horizontalSpaceSmall(),
                        Image.asset(
                          'graphics/transfer_refill/transfer_refill_2.png',
                          height: 240,
                          width: (MediaQuery
                              .of(context)
                              .size
                              .width - 50) / 2,
                        )
                      ],
                    ),
                  ],
                ),
                PPUIHelper.verticalSpaceMedium(),
                PPDivider(),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: _horizontalSpacing, vertical: _horizontalSpacing),
                    child: Container(
                        child: Container(
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(SMALL_XX), boxShadow: <BoxShadow>[
                              BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.11), offset: Offset(0, 2), blurRadius: 2.0),
                            ]),
                            child: OutlineButton(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(color: tertiaryColor),
                                  borderRadius: BorderRadius.circular(SMALL_XX),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment(0, 0),
                                  child: Text(
                                    LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-close"),
                                  ),
                                ))))),
              ],
            ),
          );
        });
  }

  Widget getAutoCompleteItem(Pharmacy item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          item.pharmacyName??"",
          style: TextStyle(fontSize: PPUIHelper.FontSizeMedium, color: primaryColor, fontWeight: FontWeight.w500),
        ),
        Container(
          width: MediaQuery
              .of(context)
              .size
              .width * 0.4,
          child: Text(
            item.pharmacyAddress??"",
            maxLines: 2,
            style: TextStyle(height: 1.4, fontSize: PPUIHelper.FontSizeSmall, color: secondaryColor, fontWeight: FontWeight.normal),
          ),
        ),
      ],
    );
  }

  Widget getCommentsBox() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: _horizontalSpacing),
        child: PPFormFields.getMultiLineTextField(
          controller: _commentsController,
          decoration: PPInputDecor.getDecoration(
              labelText: LocalizationUtils.getSingleValueString("modals", "modals.order.medications"),
              hintText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.list-pahramacy-info")),
        ));
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_skipped);
      bool success = widget.source == BaseStepperSource.NEW_USER
          ? await model.uploadPrescriptionSignUp()
          : await model.uploadPrescriptionSkip(
          widget.userPatient!,
          null,
          null,
          null,
          null,
          false,
          null,
          true,
          "PENDING",
          null,
          null,
          null,
          null,
          null,
          null);
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreen(model);
      }
    } else
      Navigator.pop(context);
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
      dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
        if (ChambersRedirect.routeName()!.contains('signUpAlmostDoneWidget')) {
          Navigator.pushNamed(context, ChambersRedirect.routeName()!,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.pushNamed(context, ChambersRedirect.routeName()!);
        }
      }

      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(
              source: BaseStepperSource.NEW_USER, postalCode: signUpTransferModel.postalCode, province: signUpTransferModel.province, from: "transfer"));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

/*  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
          return SkipSignupSheet(context: context, model: model, source: BaseStepperSource.NEW_USER, userPatient: widget.userPatient, openFrom: "transfer");
        });
  }*/

  onTransferClick(BuildContext context, SignUpTransferModel signUpTransferModel) async {
    bool success;

    var connectivityResult = await signUpTransferModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpTransferModel.noInternetConnection);
      return;
    }

    if (LocalizationUtils.isProvinceQuebec() && !defaultValue) {} else {
      TransferSearchSelectModel searchSelectModel = signUpTransferModel.transferQuebecModel!;
      success = widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
          ? await signUpTransferModel.uploadPrescriptionAdd(
          widget.userPatient,
          selectedPlaceId,
          searchSelectModel.pharmacyName ?? "",
          searchSelectModel.pharmacyAddress ?? "",
          searchSelectModel.pharmacyPhone,
          transferAll,
          transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
          true,
          null,
          searchSelectModel.pharmacyProvince ?? "",
          widget.medicineName,
          widget.quantity,
          searchSelectModel.id,
          searchSelectModel.isPrescriptionEdited,
          searchSelectModel.omsPrescriptionId)
          : await signUpTransferModel.uploadPrescription(
          userPatient: widget.userPatient,
          placeId: selectedPlaceId,
          pharmacyName: searchSelectModel.pharmacyName ?? "",
          pharmacyAddress: searchSelectModel.pharmacyAddress ?? "",
          pharmacyPhone: searchSelectModel.pharmacyPhone,
          isTransferAll: transferAll,
          comments: transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
          isSignUpFlow: checkSignupFlow(),
          prescriptionState: null,
          province: signUpTransferModel.province != null ? signUpTransferModel.province : "",
          medicineName: widget.medicineName,
          quantity: widget.quantity,
          id: searchSelectModel.id,
          isPrescriptionEdited: searchSelectModel.isPrescriptionEdited,
          omsPrescriptionId: searchSelectModel.omsPrescriptionId);

      if (success == true) {
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        Provider.of<PrescriptionModel>(context, listen: false).clearData();
        dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);

        goToNextTransferScreen(signUpTransferModel, searchSelectModel);
        if (checkSignupFlow()) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new_formfilled);
        } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_refills_formfilled);
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_congratulations);
        }
      } else {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }

      if (signUpTransferModel.checkSignupFlow(widget.source!)) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search_select);
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_select);
      }
    }

    /*if (dataStore.getPharmacyName() != null &&
        dataStore.getPharmacyName()!.isNotEmpty &&
        widget.source != null &&
        widget.source == BaseStepperSource.NEW_USER) {
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(
            source: BaseStepperSource.NEW_USER,
            from: "transfer",
            modelFrom: signUpTransferModel,
          ));
    } else if (signUpTransferModel.pharmacyName != '' &&
        signUpTransferModel.pharmacyAddress != '' &&
        signUpTransferModel.pharmacyName != null &&
        signUpTransferModel.pharmacyAddress != null) {
      if (!transferAll && _commentsController.text == '') {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("transfer", "transfer.all.list-medications"));
        return;
      }

      success = widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
          ? await signUpTransferModel.uploadPrescriptionAdd(
              widget.userPatient,
              selectedPlaceId,
              signUpTransferModel.pharmacyName != null ? signUpTransferModel.pharmacyName : "",
              signUpTransferModel.pharmacyAddress != null ? signUpTransferModel.pharmacyAddress : "",
              getFormatterPhoneNumber(signUpTransferModel.pharmacyPhoneNumber != null ? signUpTransferModel.pharmacyPhoneNumber : "1111111111"),
              transferAll,
              transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
              true,
              null,
              signUpTransferModel.province != null ? signUpTransferModel.province : "",
              widget.medicineName,
              widget.quantity)
          : await signUpTransferModel.uploadPrescription(
              userPatient: widget.userPatient,
              placeId: selectedPlaceId,
              pharmacyName: signUpTransferModel.pharmacyName != null ? signUpTransferModel.pharmacyName : "",
              pharmacyAddress: signUpTransferModel.pharmacyAddress != null ? signUpTransferModel.pharmacyAddress : "",
              pharmacyPhone: getFormatterPhoneNumber(signUpTransferModel.pharmacyPhoneNumber != null ? signUpTransferModel.pharmacyPhoneNumber : "1111111111"),
              isTransferAll: transferAll,
              comments: transferAll ? null : (_commentsController.text != null ? _commentsController.text : ""),
              isSignUpFlow: checkSignupFlow(),
              prescriptionState: null,
              province: signUpTransferModel.province != null ? signUpTransferModel.province : "",
              medicineName: widget.medicineName,
              quantity: widget.quantity);

      if (success == true) {
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        Provider.of<PrescriptionModel>(context, listen: false).clearData();
        dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);

        goToNextScreen(signUpTransferModel);
        if (checkSignupFlow()) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new_formfilled);
        } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_refills_formfilled);
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_congratulations);
        }
      } else {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
      }
    } else {
      setState(() {
        autovalidate = true;
      });
      Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
          arguments: TransferArguments(
              source: BaseStepperSource.MAIN_SCREEN,
              model: signUpTransferModel,
              userPatient: widget.userPatient,
              medicineName: widget.medicineName,
              quantity: widget.quantity));
      if (checkSignupFlow()) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search);
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search);
      }
    }*/
  }

  bool checkSignupFlow() {
    return widget.source == BaseStepperSource.NEW_USER;
  }

  int getFormatterPhoneNumber(String phoneNumber) {
    try {
      String formattedNumber = phoneNumber.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      return int.parse(formattedNumber);
    } catch (ec) {
      return -1;
    }
  }

  onBack() {
    return () async {
      if (widget.onBack != null) widget.onBack!();
    };
  }

  onSuccess() {
    return () async {
      if (widget.onSuccess != null) widget.onSuccess!();
    };
  }

  void showTransferBottomSheet(BuildContext context, SignUpTransferModel model, TransferSearchSelectModel selectedValue, String pharmacyNamePopular) {
    pharmacyShowed = true;
    bool showProgress = false;
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
              return WillPopScope(
                onWillPop: () async {
                  pharmacyShowed = false;
                  return true;
                },
                child: Container(
                  color: darkBlue.withOpacity(0.06),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              LocalizationUtils
                                  .getSingleValueString("signup", "signup.transfer.transfer-hint-new")
                                  .isEmpty
                                  ? "Transferring from"
                                  : LocalizationUtils.getSingleValueString("signup", "signup.transfer.transfer-hint-new"),
                              style: TextStyle(fontSize: 15.6, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Text(
                              selectedValue.pharmacyName ?? "",
                              style: TextStyle(fontSize: 22.8, fontFamily: "FSJoeyPro Heavy", color: darkBlue),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              selectedValue.pharmacyPhone == null ? "" : "${selectedValue.pharmacyPhone}",
                              style: TextStyle(fontSize: 16.8, fontFamily: "FSJoeyPro Bold", color: darkBlue),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              selectedValue.pharmacyAddress ?? "",
                              style: TextStyle(fontSize: 18, fontFamily: "FSJoeyPro", color: darkBlue),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            LocalizationUtils.isProvinceQuebec()
                                ? Container(
                              child: CheckboxListTile(
                                key: Key("checkOrUncheck"),
                                title: RichText(
                                  text: TextSpan(
                                    text: getTextForCheckbox("1") + ' ',
                                    style: TextStyle(color: darkPrimaryColor, fontSize: 15, height: 1.4, fontFamily: "FSJoeyPro"),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: getTextForCheckbox("2") + ' ',
                                        style: TextStyle(color: lightPurple, fontSize: 15, height: 1.4, fontFamily: "FSJoeyPro"),
                                        recognizer: getLaunchAction(),
                                      ),
                                      TextSpan(text: getTextForCheckbox("3") + ' ',
                                          style: TextStyle(color: darkPrimaryColor, fontSize: 15, height: 1.4, fontFamily: "FSJoeyPro")),
                                    ],
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                                value: defaultValue,
                                activeColor: brandColor,
                                contentPadding: EdgeInsets.zero,
                                controlAffinity: ListTileControlAffinity.leading,
                                onChanged: (bool? value) =>
                                    setState(
                                          () {
                                        defaultValue = !defaultValue;
                                      },
                                    ),
                              ),
                            )
                                : Container(),
                            LocalizationUtils.isProvinceQuebec() && !defaultValue
                                ? Text(
                              LocalizationUtils
                                  .getSingleValueString("signup", "signup.fields.error-tc")
                                  .isEmpty
                                  ? "Please accept the Terms and Conditions"
                                  : LocalizationUtils.getSingleValueString("signup", "signup.fields.error-tc"),
                              style: TextStyle(color: errorColor, fontSize: 13, fontFamily: "FSJoeyPro"),
                            )
                                : Container(),
                          ],
                        ),
                      ),
                      lineDivider,
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: showProgress
                            ? ViewConstants.progressIndicator
                            : Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 45,
                                child: OutlinedButton(
                                  key: Key("change pharmacy"),
                                  child: Text(
                                    LocalizationUtils.getSingleValueString("signup", "signup.transfer.change").toUpperCase(),
                                    style: TextStyle(color: darkBlue, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                  ),
                                  style: OutlinedButton.styleFrom(
                                    primary: Colors.white,
                                    backgroundColor: darkBlue.withOpacity(0.04),
                                    side: BorderSide(color: darkBlue, width: 2),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                  ),
                                  onPressed: () {
                                    onClickPharmacySearchScreen(model, "");
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Container(
                                height: 45,
                                child: OutlinedButton(
                                  key: Key("transfer pharmacy"),
                                  child: Text(
                                    LocalizationUtils.getSingleValueString("signup", "signup.transfer.transfer").toUpperCase(),
                                    style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                  ),
                                  style: OutlinedButton.styleFrom(
                                    primary: darkBlue,
                                    backgroundColor: darkBlue,
                                    side: BorderSide(color: darkBlue, width: 2),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                  ),
                                  onPressed: () async {
                                    if (defaultValue) {
                                      setState(() {
                                        showProgress = true;
                                      });
                                      bool isUploaded = await UploadDetails(model, selectedValue);
                                      if (isUploaded) {
                                        setState(() {
                                          showProgress = false;
                                        });
                                        goToNextTransferScreen(model, selectedValue);
                                      } else {
                                        setState(() {
                                          showProgress = false;
                                        });
                                      }
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  void goToNextTransferScreen(SignUpTransferModel signUpTransferModel, TransferSearchSelectModel selectedValue) async {
    if (widget.source == BaseStepperSource.NEW_USER) {
      await dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
        if (ChambersRedirect.routeName()!.contains('signUpAlmostDoneWidget')) {
          Navigator.pushNamed(context, ChambersRedirect.routeName()!,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        }
      }
      dataStore.writeString(DataStoreService.POSTAL_CODE_TRANSFER, selectedValue.pharmacyPostalCode ?? "");
      dataStore.writeString(DataStoreService.PROVINCE_TRANSFER, selectedValue.pharmacyProvince ?? "");
      dataStore.writeString(DataStoreService.PHARMACY_NAME, selectedValue.pharmacyName ?? "");
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, selectedValue.pharmacyAddress ?? "");
      dataStore.writeString(DataStoreService.PRESCRIPTION_TYPE, "transfer");
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, true);
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(
            source: BaseStepperSource.NEW_USER,
            postalCode: selectedValue.pharmacyPostalCode ?? "",
            province: selectedValue.pharmacyProvince,
            from: "transfer",
            pharmacyName: selectedValue.pharmacyName,
            modelFrom: signUpTransferModel,
            pharmacyAddress: selectedValue.pharmacyAddress,
          ));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
      dataStore.writeString(DataStoreService.PHARMACY_NAME, selectedValue.pharmacyName ?? "");
      dataStore.writeString(DataStoreService.POSTAL_CODE_TRANSFER, selectedValue.pharmacyPostalCode ?? "");
      dataStore.writeString(DataStoreService.PROVINCE_TRANSFER, selectedValue.pharmacyProvince ?? "");
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, selectedValue.pharmacyAddress ?? "");
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
      Navigator.pushNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      dataStore.writeString(DataStoreService.PHARMACY_NAME, selectedValue.pharmacyName ?? "");
      dataStore.writeString(DataStoreService.POSTAL_CODE_TRANSFER, selectedValue.pharmacyPostalCode ?? "");
      dataStore.writeString(DataStoreService.PROVINCE_TRANSFER, selectedValue.pharmacyProvince ?? "");
      dataStore.writeString(DataStoreService.PHARMACY_ADDRESS, selectedValue.pharmacyAddress ?? "");
      dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
      Navigator.of(context).pushNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.MAIN_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  String getTextForCheckbox(String type) {
    List<String> checkBoxData1 = LocalizationUtils.getSingleValueString("signup", "signup.transfer.termsandconditions").split(' <a');
    List<String> checkBoxData2 = checkBoxData1[1].split('\">');
    List<String> checkBoxData3 = checkBoxData2[1].split('\</a>');
    String data = checkBoxData1[0];
    String data1 = checkBoxData2[0];
    String data2 = checkBoxData3[0];
    String data3 = checkBoxData3[1];
    print("$data,$data1,$data2,$data3");
    data1.replaceAll("target=\"blank\'", " ");

    if (type == "1") {
      return data;
    } else if (type == "2") {
      return data2;
    } else {
      return data3;
    }
  }


  TapGestureRecognizer getLaunchAction() {
    List<String> checkBoxData1 = LocalizationUtils.getSingleValueString("signup", "signup.transfer.termsandconditions").split(' <a');
    List<String> checkBoxData2 = checkBoxData1[1].split('\">');
    List<String> checkBoxData3 = checkBoxData2[1].split('\</a>');
    String data = checkBoxData1[0];
    String data1 = checkBoxData2[0];
    String data2 = checkBoxData3[0];
    String data3 = checkBoxData3[1];
    print("$data,$data1,$data2,$data3");
    data1.replaceAll("target=\"blank\'", " ");

    TapGestureRecognizer _onTapTandC = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(data1 + "\"")) {
          await launch(data1 + "\"");
        }
      };

    return _onTapTandC;
  }

  Future<bool> UploadDetails(SignUpTransferModel model, TransferSearchSelectModel selectedValue) async {
    var success = widget.source == BaseStepperSource.ADD_PATIENT || widget.source == BaseStepperSource.CONSENT_FLOW
        ? await model.uploadPrescriptionAdd(
        widget.userPatient,
        selectedValue.selectedPlaceId,
        selectedValue.pharmacyName ?? "",
        selectedValue.pharmacyAddress ?? "",
        selectedValue.pharmacyPhone,
        true,
        selectedValue.comments,
        selectedValue.isSignup,
        selectedValue.prescriptionState,
        selectedValue.pharmacyProvince ?? "",
        widget.medicineName,
        widget.quantity,
        selectedValue.id,
        selectedValue.isPrescriptionEdited,
        selectedValue.omsPrescriptionId)
        : await model.uploadPrescription(
        userPatient: widget.userPatient,
        placeId: selectedValue.selectedPlaceId,
        pharmacyName: selectedValue.pharmacyName ?? "",
        pharmacyAddress: selectedValue.pharmacyAddress ?? "",
        pharmacyPhone: selectedValue.pharmacyPhone,
        isTransferAll: true,
        comments: selectedValue.comments,
        isSignUpFlow: checkSignupFlow(),
        prescriptionState: selectedValue.prescriptionState,
        province: selectedValue.pharmacyProvince ?? "",
        medicineName: widget.medicineName,
        quantity: widget.quantity,
        id: selectedValue.id,
        isPrescriptionEdited: selectedValue.isPrescriptionEdited,
        omsPrescriptionId: selectedValue.omsPrescriptionId);

    if (success == true) {
      Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
      Provider.of<PrescriptionModel>(context, listen: false).clearData();
      dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);

      int? count = widget.source == BaseStepperSource.NEW_USER
          ? model.prescriptionList!.where((element) => element.prescriptionState!.contains('FILED')).length
          : model.totalPrescriptionsCount;
      Map<String, dynamic> leadMap = new Map();
      leadMap['lead_created_by'] = 'mobileApp';
      leadMap['lead_new'] = count != null && count > 1 ? 0 : 1;
      leadMap['lead_type'] = 'transfer';
      leadMap['lead_origin'] = widget.source == BaseStepperSource.NEW_USER ? 'signup' : 'dashboard';
      leadMap['transfer_pharmacy_name'] = selectedValue.pharmacyName ?? "";
      leadMap['transfer_pharmacy_city'] = selectedValue.pharmacyAddress ?? "";
      if (model.revenue) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_event, leadMap);
      }
      if (checkSignupFlow()) {
        analyticsEvents.sendInitiatedCheckoutEvent();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new_formfilled);
      } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
        analyticsEvents.sendInitiatedCheckoutEvent();
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_refills_formfilled);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_congratulations);
      }
    } else {
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
    if (model.checkSignupFlow(widget.source!)) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search_select);
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_select);
    }
    return success;
  }
}

String getDescriptionText(String? gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.description-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.description-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.description-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.description");
      break;
  }
}

String getPlaceHolderDescriptionText(String? gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.search-placeholder-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.search-placeholder-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.search-placeholder-OTHER");
      break;
    default:
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.search-placeholder");
      break;
  }
}

String getTitleText(String? gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.title-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.title-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.title-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString("transfer", "transfer.all.title");
      break;
  }
}

String getQuebecTitle(String? gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.selected-title-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.selected-title-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.selected-title-OTHER");
      break;
    default:
      return LocalizationUtils.getSingleValueString("signup", "signup.transfer.selected-title");
      break;
  }
}
