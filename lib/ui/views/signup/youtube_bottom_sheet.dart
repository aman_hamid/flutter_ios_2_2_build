import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pocketpills/res/colors.dart';

import 'package:pocketpills/ui/views/base_state.dart';

import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';

import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YouTubeSheet extends StatefulWidget {
  BuildContext? context;

  YouTubeSheet({this.context});

  _YouTubeSheetState createState() => _YouTubeSheetState();
}

class _YouTubeSheetState extends BaseState<YouTubeSheet> {
  late YoutubePlayerController _controller;
  bool _isPlayerReady = false;
  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;

  _YouTubeSheetState();

  late SignUpTransferModel model2;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: "JyIeDivW1Jo",
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ),
    );
    _playerState = PlayerState.unknown;
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  Widget build(BuildContext buildContext) {
    return YoutubePlayerBuilder(
        key: Key("youTubePlayer"),
        onExitFullScreen: () {
          // The player forces portraitUp after exiting fullscreen. This overrides the behaviour.
          SystemChrome.setPreferredOrientations(DeviceOrientation.values);
        },
        player: YoutubePlayer(
          controller: _controller,
          showVideoProgressIndicator: true,
          progressIndicatorColor: primaryColorMain,
          onReady: () {
            _isPlayerReady = true;
          },
          onEnded: (data) {
            Navigator.pop(buildContext);
          },
        ),
        builder: (context, player) => Container(
              height: 300,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(child: player),
                ],
              ),
            ));
  }
}
