import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/outlined_button.dart';
import 'package:url_launcher/url_launcher.dart';

class TransferDetailsSignup extends StatelessWidget {
  static const routeName = 'transfer_details_signup';
  final BaseStepperSource? source;
  final String? from;

  const TransferDetailsSignup({Key? key, this.source, this.from}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _onBackPressed(context);
        return true;
      },
      child: BaseScaffold(
        appBar: source == BaseStepperSource.NEW_USER
            ? AppBar(
                backgroundColor: Colors.white,
                title: isProvinceQuebec() == true
                    ? Image.network(
                        "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                        width: MediaQuery.of(context).size.width * 0.45,
                        fit: BoxFit.cover,
                      )
                    : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
                centerTitle: true,
                leading: Container(),
              )
            : null,
        body: Container(
          color: lightBlueColor,
          child: SafeArea(
            child: Builder(
              builder: (BuildContext context) {
                return Column(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16, top: 8, bottom: 8),
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(4.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 10,
                                blurRadius: 5,
                                offset: Offset(0, 7), // changes position of shadow
                              ),
                            ],
                          ),
                          child: SingleChildScrollView(child: getStartedView(context))),
                    )),
                    Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 45,
                                child: OutlinedButtonCustom.icon(
                                  icon: Icon(
                                    Icons.phone_enabled_outlined,
                                    color: brandColor,
                                    size: 18,
                                  ),
                                  label: Text(
                                    LocalizationUtils.getSingleValueString("signup", "signup.details.help").toUpperCase(),
                                    style: TextStyle(color: darkBlue, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                  ),
                                  style: OutlinedButton.styleFrom(
                                    primary: Colors.white,
                                    backgroundColor: whiteColor,
                                    side: BorderSide(color: darkBlue, width: 2),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                  ),
                                  onPressed: () {
                                    LocalizationUtils.isProvinceQuebec()
                                        ? launch("tel://" + ViewConstants.PHARMACY_PHONE_QC.toString())
                                        : launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Container(
                                height: 45,
                                child: OutlinedButtonCustom.icon(
                                  isLeft: true,
                                  label: Text(
                                    LocalizationUtils.getSingleValueString("signup", "signup.details.continue").toUpperCase(),
                                    style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                  ),
                                  icon: Icon(
                                    Icons.arrow_forward,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                  style: OutlinedButton.styleFrom(
                                    primary: darkBlue,
                                    backgroundColor: darkBlue,
                                    side: BorderSide(color: darkBlue, width: 2),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamedAndRemoveUntil(context, SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false,
                                        arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "transfer"));
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget getStartedView(BuildContext context) {
    DataStoreService dataStore = DataStoreService();
    TransferPrescription transferPrescription =
        TransferPrescription.fromJson(jsonDecode(dataStore.readString(DataStoreService.PRESCRIPTION_FOR_TRANSFER).toString()));
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.details.title-transfer").isEmpty
                ? "Your prescription transfer request has been received"
                : LocalizationUtils.getSingleValueString("signup", "signup.details.title-transfer"),
            style: TextStyle(color: brandColor, fontFamily: "FSJoeyPro Heavy", fontSize: 21.6),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.details.desc-transfer").isEmpty
                ? "Your prescription transfer request has been received"
                : LocalizationUtils.getSingleValueString("signup", "signup.details.desc-transfer"),
            style: TextStyle(color: brandColor, fontFamily: "FSJoeyPro", fontSize: 18),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 1,
              child: ListTile(
                title: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.details.firstname"),
                  style: getProfileHeaderStyle(),
                ),
                subtitle: Text(
                  signUpResponse!.signupDto!.firstName ?? "",
                  style: getProfileItemStyle(),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: ListTile(
                title: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.details.lastname"),
                  style: getProfileHeaderStyle(),
                ),
                subtitle: Text(
                  signUpResponse!.signupDto!.lastName ?? "",
                  style: getProfileItemStyle(),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            signUpResponse!.signupDto!.phone == null
                ? Container()
                : Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text(
                        LocalizationUtils.getSingleValueString("signup", "signup.details.phone"),
                        style: getProfileHeaderStyle(),
                      ),
                      subtitle: Text(
                        signUpResponse!.signupDto!.phone!.toString(),
                        style: getProfileItemStyle(),
                      ),
                    ),
                  ),
            Expanded(
              flex: 1,
              child: ListTile(
                title: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.details.dob"),
                  style: getProfileHeaderStyle(),
                ),
                subtitle: Text(
                  signUpResponse!.signupDto!.birthDate ?? "",
                  style: getProfileItemStyle(),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        ListTile(
          title: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.details.service").isEmpty
                ? "Service selected"
                : LocalizationUtils.getSingleValueString("signup", "signup.details.service"),
            style: getProfileHeaderStyle(),
            textAlign: TextAlign.start,
          ),
          subtitle: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.details.service-transfer").isEmpty
                ? "Transfer prescription to Pocketpills"
                : LocalizationUtils.getSingleValueString("signup", "signup.details.service-transfer"),
            style: getProfileItemStyle(),
            textAlign: TextAlign.start,
          ),
        ),
        ListTile(
          title: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.details.transfer-from").isEmpty
                ? "Transferring From"
                : LocalizationUtils.getSingleValueString("signup", "signup.details.transfer-from"),
            style: getProfileHeaderStyle(),
            textAlign: TextAlign.start,
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: PPUIHelper.VerticalSpaceXSmall),
              Text(
                transferPrescription.pharmacyName ?? "",
                style: getProfileItemStyle(),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceXSmall),
              Text(
                transferPrescription.pharmacyPhone.toString(),
                style: getPharmacyStyle(),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceXSmall),
              Text(
                transferPrescription.pharmacyAddress ?? "",
                style: getPharmacyStyle(),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: PPUIHelper.VerticalSpaceMedium),
            ],
          ),
        )
      ],
    );
  }

  TextStyle getProfileHeaderStyle() {
    return TextStyle(fontFamily: "FSJoeyPro", fontSize: 15.6, color: darkPrimaryColor);
  }

  TextStyle getProfileItemStyle() {
    return TextStyle(fontFamily: "FSJoeyPro Bold", fontSize: 19.6, color: darkPrimaryColor);
  }

  TextStyle getPharmacyStyle() {
    return TextStyle(fontFamily: "FSJoeyPro", fontSize: 18, color: darkPrimaryColor);
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }
}
