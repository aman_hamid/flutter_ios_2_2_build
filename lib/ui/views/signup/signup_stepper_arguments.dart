import 'dart:io';

import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class SignupStepperArguments {
  final int? position;
  final String? from;
  final BaseStepperSource? source;
  final bool? chambersFlow;
  final String? postalCode;
  final String? province;
  final String? pharmacyName;
  final String? pharmacyAddress;
  final SignUpTransferModel? modelFrom;
  final String? medicineName;
  final int? quantity;
  final UserPatient? userPatient;
  final String? dialogMessage;
  final String? firstName;
  final List<File>? prescriptionPages;

  SignupStepperArguments(
      {this.position,
      this.source,
      this.from,
      this.chambersFlow,
      this.postalCode,
      this.province,
      this.pharmacyName,
      this.pharmacyAddress,
      this.modelFrom,
      this.medicineName,
      this.quantity,
      this.userPatient,
      this.dialogMessage,
      this.firstName,this.prescriptionPages});
}
