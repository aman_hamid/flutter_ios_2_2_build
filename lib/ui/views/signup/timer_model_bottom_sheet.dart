import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class TimerModalBottomSheet extends StatefulWidget {
  String? phoneNumber;
  SignUpAboutYouModel? model;
  BuildContext? context;

  TimerModalBottomSheet({this.phoneNumber, this.model, this.context});

  _TimerModalBottomSheetState createState() => _TimerModalBottomSheetState();
}

class _TimerModalBottomSheetState extends BaseState<TimerModalBottomSheet> with SingleTickerProviderStateMixin {
  bool isWidgetLive = false;
  Timer? _timer;
  int start = 0;
  bool stopTimer = false;

  @override
  void initState() {
    super.initState();
    isWidgetLive = true;
    start = widget.model!.getStartTime;
    startTimer();
  }

  @override
  void dispose() {
    _timer!.cancel();
    isWidgetLive = false;
    widget.model!.startTimer();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verification_resend_modal_open);
  }

  Widget build(BuildContext context) {
    return FutureBuilder(
        future: widget.model!.getLocalization(["forgot"]),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getMainView() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: SMALL_XXX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-resend"),
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    start > 0
                        ? Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry"),
                            style: MEDIUM_XX_SECONDARY,
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                    start > 0
                        ? Text(
                            start.toString() + " " + LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry-sec"),
                            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                  ],
                )
              ],
            ),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () async {
                    Navigator.pop(context);
                    bool result = await widget.model!.resendOtpByIdentifier(widget.phoneNumber!, 'sms', true);
                    if (result == true) {
                      start = 30;
                      widget.model!.setStartTime(30);
                      startTimer();
                      onFail(widget.context, errMessage: widget.model!.errorMessage);
                    }
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_resend_sms);
                  },
                  child: Padding(
                      padding: const EdgeInsets.all(MEDIUM_XXX),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.textsms),
                          SizedBox(
                            width: MEDIUM_X,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.resend-sms"),
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          )
                        ],
                      )),
                ),
                InkWell(
                  onTap: () async {
                    Navigator.pop(context);
                    bool result = await widget.model!.resendOtpByIdentifier(widget.phoneNumber!, 'call', true);
                    if (result == true) {
                      start = 30;
                      widget.model!.setStartTime(30);
                      startTimer();
                      onFail(widget.context, errMessage: widget.model!.errorMessage);
                    }
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_get_call);
                  },
                  child: Padding(
                      padding: const EdgeInsets.all(MEDIUM_XXX),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.call),
                          SizedBox(
                            width: MEDIUM_X,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.resend-call"),
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          )
                        ],
                      )),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: REGULAR_XXX),
              child: SecondaryButton(
                key: Key("closeBottomSheet"),
                isExpanded: false,
                text: LocalizationUtils.getSingleValueString("common", "common.navbar.close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(oneSec, (Timer timer) {
      if (start < 1 || stopTimer == true) {
        _timer!.cancel();
      } else {
        if (isWidgetLive) {
          setState(() {
            start--;
          });
        } else {
          start--;
        }
        widget.model!.setStartTime(start);
      }
    });
  }
}
