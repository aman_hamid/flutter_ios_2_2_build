import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_timer_countdown/flutter_timer_countdown.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/address.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/request/add_phn_address_request.dart';
import 'package:pocketpills/core/request/add_phn_patient_address_request.dart';
import 'package:pocketpills/core/response/address/address_complete.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_address_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_insurance_model.dart';
import 'package:pocketpills/core/viewmodels/progress_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/dialogs/permission_dialog.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/empty_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/full_image_container.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/views/signup/details/telehealth_details_signup.dart';
import 'package:pocketpills/ui/views/signup/health_card_number_bottom_sheet_signup.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_email_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/permission_utils.dart';
import 'package:pocketpills/utils/rotate_compress_image.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'dart:io';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class HealthCardUploadViewSignUp extends StatefulWidget {
  static const routeName = 'healthCardSignUp';
  BaseStepperSource? source;
  SignUpTransferModel? model;
  UserPatient? userPatient;
  String? appointmentDateTime;

  HealthCardUploadViewSignUp({this.source = BaseStepperSource.NEW_USER, this.model, this.userPatient, this.appointmentDateTime});

  @override
  State<StatefulWidget> createState() {
    return _HealthCardUploadViewSignUpState();
  }
}

class _HealthCardUploadViewSignUpState extends BaseState<HealthCardUploadViewSignUp> {
  bool front = false, back = false;
  File? frontImage, backImage;
  String? successMsg;
  final TextEditingController _healthCardController = TextEditingController();
  final FocusNode _healthCardFocusNode = FocusNode();
  bool clicked = false;

  /*bool timerCompleted = false;*/
  bool timerMessageShow = false;
  bool visibleError = false;
  bool showPHN = true;
  bool showDateError = false;
  bool showPhnDateError = false;
  bool autovalidate = false;
  String? gender;
  bool initialized = false;

  // TimerCountdown? timerCountDown;

  String? _mmPhnValue;
  String? _ddPhnValue;
  String? _yyyyPhnValue;
  String? dropDownError;
  String? genderError;
  String? dropDownValue;
  String? selectedGender;
  bool defaultValue = true;
  Address? address;
  String? stAddressError;
  String? prevPostalCodeName;
  ProfileAddressModel? _profileAddressModel;
  AutoCompleteTextField? stAddressSearchTextField;
  List<PinCodeSuggestion> pinCodeSuggestion = [];
  late PPRadioGroup<String>? genderRadioGroup;

  GlobalKey<AutoCompleteTextFieldState<PinCodeSuggestion>> key = new GlobalKey();
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _datePhnController = TextEditingController();
  final TextEditingController _monthPhnController = TextEditingController();
  final TextEditingController _yearPhnController = TextEditingController();
  final TextEditingController _phnController = TextEditingController();
  final TextEditingController _cellController = TextEditingController();
  TextEditingController _stController = TextEditingController();
  TextEditingController _st2Controller = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _postalCodeController = TextEditingController();

  final FocusNode _nameFnode = FocusNode();
  final FocusNode _stFnode = FocusNode();
  final FocusNode _st2Fnode = FocusNode();
  final FocusNode _cityFnode = FocusNode();
  final FocusNode _postalFnode = FocusNode();
  final FocusNode _phoneFnode = FocusNode();

  final FocusNode _datePhnFnode = FocusNode();
  final FocusNode _monthPhnFnode = FocusNode();
  final FocusNode _yearPhnFnode = FocusNode();
  final FocusNode _phnFnode = FocusNode();

  static bool phnMonth = false, phnDay = false, phnYear = false, phoneNumber = false;

  @override
  void initState() {
    super.initState();

    //timerMessageShow = getTimerStatus();

    if (widget.model == null) {
      widget.model = SignUpTransferModel();
    }
    _stController.addListener(streetControllerListener);
    PPApplication.prescriptionID = -1;
    TELEHEALTH_CONDITION = "";
    TELEHEALTH_COMMENT = "";
    dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, true);
    showPHN = dataStore.readBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED) == true ? true : false;
    selectedGender = GlobalVariable().gender.isEmpty ? null : GlobalVariable().gender;
    _stController.text = GlobalVariable().address1;
    _st2Controller.text = GlobalVariable().address2;
    _cityController.text = GlobalVariable().city;
    _postalCodeController.text = GlobalVariable().postalCode;
    if (GlobalVariable().healthCardNumber.isNotEmpty) {
      _phnController.text = GlobalVariable().healthCardNumber;
    }
    _datePhnController.text = GlobalVariable().healthCadExpiryDD;
    _monthPhnController.text = GlobalVariable().healthCadExpiryMM;
    _yearPhnController.text = GlobalVariable().healthCadExpiryYY;
    if (GlobalVariable().phone.isNotEmpty) {
      _cellController.text = GlobalVariable().phone;
    }
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  @override
  void dispose() {
    _stController.removeListener(streetControllerListener);
    _monthPhnController.removeListener(monthPhnControllerListener);
    _datePhnController.removeListener(datePhnControllerListener);
    _yearPhnController.removeListener(yearPhnControllerListener);
    _postalCodeController.removeListener(pharmacyControllerListener);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard);

    if (!["", null].contains(dataStore.readString(DataStoreService.PHN)) && _phnController.text.isEmpty) {
      _phnController.text = dataStore.readString(DataStoreService.PHN) ?? "";
    }
  }

  pharmacyControllerListener() {
    //getSearchResponse(_postalCodeController.text);
  }

  streetControllerListener() {
    if (_stController.text.toString().isEmpty && autovalidate == true) {
      setState(() {
        stAddressError = LocalizationUtils.getSingleValueString("common", "common.label.this-field-required");
      });
      return;
    }
    if (_stController.text.contains("#")) {
      setState(() {
        stAddressError = LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
            ? "Special character not allowed"
            : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
      });
      return;
    }
    if (_stController.text.isEmpty && autovalidate == false) {
      setState(() {
        stAddressError = null;
      });
      return;
    }
    if (!_stController.text.contains("#") && autovalidate == false) {
      setState(() {
        stAddressError = null;
      });
    }
    if (_postalCodeController.text.isNotEmpty) {
      getSearchResponse(_postalCodeController.text + " " + _stController.text);
    } else {
      getSearchResponse(_stController.text);
    }
  }

  getSearchResponse(String keyword) async {
    if (_profileAddressModel != null && keyword.isNotEmpty) {
      pinCodeSuggestion = (await _profileAddressModel!.searchPostalCode(keyword))!;
      try {
        stAddressSearchTextField!.updateSuggestions(pinCodeSuggestion);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }
  }

  Widget getAutoCompleteItem(PinCodeSuggestion item) {
    return Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 4), child: PPTexts.getTertiaryHeading(item.text ?? "", isBold: true));
  }

  Widget? getAutoCompleteField(BuildContext context, ProfileAddressModel profileAddressModel) {
    stAddressSearchTextField = AutoCompleteTextField<PinCodeSuggestion>(
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Bold"),
      keyboardType: TextInputType.visiblePassword,
      controller: _stController,
      suggestions: _stFnode.hasFocus == true ? pinCodeSuggestion : [],
      submitOnSuggestionTap: true,
      focusNode: _stFnode,
      onFocusChanged: (focus) {
        if (focus) {
        } else {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      },
      key: key,
      itemSorter: (a, b) => 0,
      itemFilter: (suggestion, input) {
        if (input.contains("#")) {
          return false;
        } else {
          return true;
        }
      },
      itemBuilder: (context, item) {
        return Row(
          children: <Widget>[
            Expanded(
              child: _stFnode.hasFocus
                  ? Container(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                        child: getAutoCompleteItem(item),
                      ),
                    )
                  : Container(),
            ),
          ],
        );
      },
      itemSubmitted: (PinCodeSuggestion item) => submitSuggestion(item.suggestionId ?? "", _profileAddressModel),
      decoration: PPInputDecor.getDecoration(
        labelText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
        hintText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
        floatingLabelBehavior: FloatingLabelBehavior.never,
      ),
    );

    return stAddressSearchTextField;
  }

  submitSuggestion(String suggestionId, ProfileAddressModel? profileAddressModel) async {
    AddressComplete? _addressComplete = await profileAddressModel!.getCompleteAddress(suggestionId);
    if (_addressComplete != null && (_stController.text.indexOf("#") == -1 || _st2Controller.text.indexOf("#") == -1)) {
      _stController.text = _addressComplete.streetAddress ?? "";
      _st2Controller.text = _addressComplete.streetAddressLineTwo ?? "";
      _cityController.text = _addressComplete.city ?? "";
      _postalCodeController.text = _addressComplete.postalCode!.replaceAll(" ", "");
    }
    /*else if (_stController.text.indexOf("#") != -1 || _st2Controller.text.indexOf("#") != -1) {
      showErrorOverlay();
    }*/
  }

  bool isFormValid() {
    bool ge = genderRadioGroup!.validate();
    return ge;
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void monthPhnControllerListener() {
    if (_monthPhnController.text.length == 2 && _mmPhnValue != _monthPhnController.text) {
      _mmPhnValue = _monthPhnController.text;
      String? validationError = validatePhnMonth(_mmPhnValue!, showErr: false);
      if (validationError != null) {
        setDatePhnErrorState(true);
      } else {
        if (phnMonth == false) {
          phnMonth = true;
          //analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_dob_month_entered);
        }
        FocusScope.of(context).requestFocus(_datePhnFnode);
      }
      if (evaluatePhnDate()) setDatePhnErrorState(false);
    }
    _mmPhnValue = _monthPhnController.text;
  }

  void datePhnControllerListener() {
    if (_datePhnController.text.length == 2 && _ddPhnValue != _datePhnController.text) {
      _ddPhnValue = _datePhnController.text;
      String? validationError = validatePhnDate(_ddPhnValue!, showErr: false);
      if (validationError != null) {
        setDatePhnErrorState(true);
      } else {
        if (phnDay == false) {
          phnDay = true;
          //analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_dob_date_entered);
        }
        FocusScope.of(context).requestFocus(_yearPhnFnode);
      }
      if (evaluatePhnDate()) setDatePhnErrorState(false);
    }
    _ddPhnValue = _datePhnController.text;
  }

  void yearPhnControllerListener() {
    if (_yearPhnController.text.length == 4 && _yyyyPhnValue != _yearPhnController.text) {
      _yyyyPhnValue = _yearPhnController.text;
      String? validationError = validatePhnYear(_yyyyPhnValue!, showErr: false);
      if (validationError != null) {
        setDatePhnErrorState(true);
      } else if (evaluatePhnDate()) {
        setDatePhnErrorState(false);
        if (phnYear == false) {
          phnYear = true;
          //analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_dob_year_entered);
        }
        FocusScope.of(context).requestFocus(_stFnode);
      }
    }
    _yyyyPhnValue = _yearPhnController.text;
  }

  bool evaluatePhnDate() {
    if (validatePhnMonth(_mmPhnValue, showErr: false) == null &&
        validatePhnDate(_ddPhnValue, showErr: false) == null &&
        validatePhnYear(_yyyyPhnValue, showErr: false) == null)
      return true;
    else
      return false;
  }

  String? validatePhnMonth(String? value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < 1 || n > 12) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  String? validatePhnDate(String? value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < 1 || n > 31) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  String? validatePhnYear(String? value, {bool? showErr}) {
    final date2 = DateTime.now();
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("common", "common.label.required") + "*" : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    } else if (n < date2.year - 1 || n > date2.year + 150) {
      return showErr! ? LocalizationUtils.getSingleValueString("forgot", "forgot.all.valid-value") : "";
    }
    return null;
  }

  isDatePhnValid() {
    if (_datePhnController.text.isNotEmpty && _monthPhnController.text.isNotEmpty && _yearPhnController.text.isNotEmpty) {
      final birthday = DateTime(int.parse(_yearPhnController.text), int.parse(_monthPhnController.text), int.parse(_datePhnController.text));
      final date2 = DateTime.now();
      final difference = date2.difference(birthday).inDays;
      if (difference <= 365) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  setDatePhnErrorState(bool showError) {
    setState(() {
      showPhnDateError = showError;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => validateBackCheck(),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<ProfileInsuranceModel>(
            create: (_) => ProfileInsuranceModel(),
          ),
          ChangeNotifierProvider<SignUpTransferModel>(
            create: (context) => SignUpTransferModel(),
          ),
          ChangeNotifierProvider<ProfileAddressModel>(
            create: (context) => ProfileAddressModel(),
          ),
        ],
        child: Consumer3<ProfileInsuranceModel, SignUpTransferModel, ProfileAddressModel>(
          builder: (BuildContext context, ProfileInsuranceModel profileInsuranceModel, SignUpTransferModel transferModel, ProfileAddressModel addressModel,
              Widget? child) {
            return FutureBuilder(
                future: myFutureMethodOverall(profileInsuranceModel, context, transferModel),
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData != null && snapshot.data != null) {
                    _profileAddressModel = addressModel;
                    return _profileInsuranceBuild(context, profileInsuranceModel, transferModel, addressModel);
                  } else if (snapshot.hasError) {
                    return ErrorScreen();
                  } else {
                    return LoadingScreen();
                  }
                });
          },
        ),
      ),
    );
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  validateBackCheck() {
    String name = signUpResponse!.signupDto!.firstName ?? "";
    String dob = signUpResponse!.signupDto!.birthDate ?? "";
    String prescription = signUpResponse!.prescription!.prescriptionState ?? "";
    if (name.isNotEmpty && dob.isNotEmpty && prescription.isNotEmpty && prescription == "FILED") {
      GlobalVariable().address1 = _stController.text.toString();
      GlobalVariable().address2 = _st2Controller.text.toString();
      GlobalVariable().city = _cityController.text.toString();
      GlobalVariable().healthCardNumber = _phnController.text.toString();
      GlobalVariable().healthCadExpiryDD = _datePhnController.text.toString();
      GlobalVariable().healthCadExpiryMM = _monthPhnController.text.toString();
      GlobalVariable().healthCadExpiryYY = _yearPhnController.text.toString();
      GlobalVariable().postalCode = _postalCodeController.text.toString();
      GlobalVariable().gender = selectedGender ?? "";
      GlobalVariable().phone = _cellController.text.toString();
      Navigator.pushNamedAndRemoveUntil(context, TelehealthDetailsSignup.routeName, (Route<dynamic> route) => false,
          arguments: TelehealthArguments(
              modelSignUp: widget.model, source: widget.source, userPatient: widget.userPatient, appointmentDateTime: widget.appointmentDateTime));
    } else {
      _onBackPressed(context);
    }
  }

  Widget _profileInsuranceBuild(
      BuildContext context, ProfileInsuranceModel profileInsuranceModel, SignUpTransferModel transferModel, ProfileAddressModel addressModel) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          resizeToAvoidBottomInset: true,
          key: Key("healthSignupView"),
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: isProvinceQuebec() == true
                ? Image.network(
                    "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                    width: MediaQuery.of(context).size.width * 0.45,
                    fit: BoxFit.cover,
                  )
                : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
            centerTitle: true,
            leading: IconButton(
              key: Key("back"),
              icon: Icon(Icons.arrow_back_rounded),
              color: brandColor,
              onPressed: () {
                validateBackCheck();
              },
            ),
          ),
          bottomNavigationBar: getButtons(profileInsuranceModel),
          body: SingleChildScrollView(
              child: Padding(
            padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: MEDIUM),
            child: getUploadInsuranceView(context, profileInsuranceModel, transferModel, addressModel),
          ))),
    );
  }

  Future myFutureMethodOverall(ProfileInsuranceModel profileInsuranceModel, BuildContext context, SignUpTransferModel transferModel) async {
    Future<Map<String, dynamic>?> future2 = profileInsuranceModel.getLocalization(["insurance", "order-checkout", "common", "signup", "healthcard", "address"]);
    Future<List<TelehealthProvinceItem>?> future1 = profileInsuranceModel.fetchPrescriptProvinceList(); // will take 3 secs
    Future<bool> future3 = profileInsuranceModel.getUserInfo(); // will take 3 secs
    return await Future.wait([future2, future1, future3]);
  }

  Widget getUploadInsuranceView(
      BuildContext context, ProfileInsuranceModel profileInsuranceModel, SignUpTransferModel transferModel, ProfileAddressModel addressModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PPUIHelper.verticalSpaceMedium(),
        pharmacistFlow(),
        PPUIHelper.verticalSpaceMedium(),
        getProvincialCard(context, profileInsuranceModel, addressModel),
      ],
    );
  }

  Widget getButtons(ProfileInsuranceModel profileInsuranceModel) {
    return dataStore.readBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED) == true
        ? PPBottomBars.getButtonedBottomBarAnother(
            height: 75, //timerMessageShow ? 75 : 110,
            child: profileInsuranceModel.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : Column(
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      showPHN
                          ? Padding(
                              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SecondaryButton(
                                    key: Key("enterPHN"),
                                    text: LocalizationUtils.getSingleValueString("signup", "signup.healthcard.manual").isEmpty
                                        ? "Enter PHN manually"
                                        : LocalizationUtils.getSingleValueString("signup", "signup.healthcard.manual"),
                                    onPressed: () {
                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_entermanually);
                                      setState(() {
                                        showPHN = false;
                                        /*if (timerCountDown != null) {
                                      timerMessageShow = true;
                                    }*/
                                      });
                                    },
                                    textColor: brandColor,
                                    isTextUpperCase: false,
                                    fontSizeManual: 14,
                                  ),
                                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                  PrimaryButton(
                                    key: Key("uploadSignupButton"),
                                    text: LocalizationUtils.getSingleValueString("signup", "signup.healthcard.continue"),
                                    onPressed: () async {
                                      if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "british columbia" &&
                                          profileInsuranceModel.fstHealth == true &&
                                          profileInsuranceModel.scndHealth == false) {
                                        showOnSnackBar(context,
                                            successMessage: LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error").isEmpty
                                                ? "Your front and back health card images are mandatory for filling the prescription"
                                                : LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error"));
                                      } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "british columbia" &&
                                          profileInsuranceModel.fstHealth == false &&
                                          profileInsuranceModel.scndHealth == true) {
                                        showOnSnackBar(context,
                                            successMessage: LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error").isEmpty
                                                ? "Your front and back health card images are mandatory for filling the prescription"
                                                : LocalizationUtils.getSingleValueString("signup", "signup.insurance.both-error"));
                                      } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "british columbia" &&
                                          profileInsuranceModel.fstHealth != false &&
                                          profileInsuranceModel.scndHealth != false) {
                                        CallNextStep(context, profileInsuranceModel);
                                      } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() != "british columbia" &&
                                          (profileInsuranceModel.fstHealth != false || profileInsuranceModel.scndHealth != false)) {
                                        CallNextStep(context, profileInsuranceModel);
                                      } else
                                        showOnSnackBar(context,
                                            successMessage: LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.card-mandatory"));
                                    },
                                  )
                                ],
                              ),
                            )
                          : Padding(
                              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SecondaryButton(
                                    key: Key("backPHN"),
                                    text: LocalizationUtils.getSingleValueString("signup", "signup.about.back").isEmpty
                                        ? "Back"
                                        : LocalizationUtils.getSingleValueString("signup", "signup.about.back"),
                                    onPressed: () {
                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_entermanually_back);
                                      GlobalVariable().address1 = _stController.text.toString();
                                      GlobalVariable().address2 = _st2Controller.text.toString();
                                      GlobalVariable().city = _cityController.text.toString();
                                      GlobalVariable().healthCardNumber = _phnController.text.toString();
                                      GlobalVariable().healthCadExpiryDD = _datePhnController.text.toString();
                                      GlobalVariable().healthCadExpiryMM = _monthPhnController.text.toString();
                                      GlobalVariable().healthCadExpiryYY = _yearPhnController.text.toString();
                                      GlobalVariable().postalCode = _postalCodeController.text.toString();
                                      GlobalVariable().gender = selectedGender ?? "";
                                      GlobalVariable().phone = _cellController.text.toString();
                                      setState(() {
                                        showPHN = true;
                                        visibleError = false;
                                        /*if (timerCountDown != null) {
                                            timerMessageShow = false;
                                          }*/
                                      });
                                    },
                                    textColor: brandColor,
                                  ),
                                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                  PrimaryButton(
                                    key: Key("phn"),
                                    text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                                    disabled: defaultValue ? false : true,
                                    onPressed: () {
                                      if (defaultValue) {
                                        onClickHealthCardNumber(context, profileInsuranceModel);
                                      }
                                    },
                                  )
                                ],
                              ),
                            ),
                    ],
                  ),
          )
        : PPBottomBars.getButtonedBottomBarAnother(
            height: 75, //timerMessageShow ? 75 : 110,
            child: profileInsuranceModel.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : Column(
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            PrimaryButton(
                              key: Key("phn"),
                              text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                              disabled: defaultValue ? false : true,
                              onPressed: () {
                                if (defaultValue) {
                                  onClickHealthCardNumber(context, profileInsuranceModel);
                                }
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
          );
  }

  Widget pharmacistFlow() {
    return Column(
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.healthcard.description"),
            //getDescriptionTextImage(PatientUtils.getForGender(Provider.of<DashboardModel>(context, listen: false).selectedPatient)),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget getProvincialCard(BuildContext context, ProfileInsuranceModel profileInsuranceModel, ProfileAddressModel addressModel) {
    if (!initialized) {
      initializeContent();
      initialized = true;
    }
    List<Widget> children = [];
    children.add(PPUIHelper.verticalSpaceMedium());
    children.add(
      Container(
        height: 190,
        child: GridView.count(
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          crossAxisSpacing: PPUIHelper.HorizontalSpaceMedium,
          children: [
            GridTile(
                key: Key("imageOneSignup"),
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthFrontImage"
                    ? ViewConstants.progressIndicator
                    : profileInsuranceModel.fstHealth != false
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", frontImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthFrontImage", "PROVINCIAL")),
            GridTile(
                key: Key("imageTwoSignup"),
                child: profileInsuranceModel.curImageUploading == "provincialInsuranceHealthBackImage"
                    ? ViewConstants.progressIndicator
                    : profileInsuranceModel.scndHealth != false
                        ? getFullImageContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", backImage!)
                        : getEmptyContainer(context, profileInsuranceModel, "provincialInsuranceHealthBackImage", "PROVINCIAL"))
          ],
        ),
      ),
    );
    children.add(PPUIHelper.verticalSpaceSmall());
    children.add(Padding(
      padding: const EdgeInsets.only(right: MEDIUM_XXX, bottom: REGULAR_XXX),
      child: TextButton(
        onPressed: () {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_cancel_subscription);
          LocalizationUtils.isProvinceQuebec()
              ? launch("tel://" + ViewConstants.PHARMACY_PHONE_QC.toString())
              : launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
        },
        child: Text(
          LocalizationUtils.isProvinceQuebec()
              ? LocalizationUtils.getSingleValueString("insurance", "insurance.help.note") + " 1-855-774-0104"
              : LocalizationUtils.getSingleValueString("insurance", "insurance.help.note") + " 1-855-950-7225",
          style: MEDIUM_XX_SECONDARY,
        ),
      ),
    ));
    children.add(PPUIHelper.verticalSpaceMedium());
    List<Widget> childrenPHN = [];
    childrenPHN.add(Form(key: _formKey, child: getStartedFlow(profileInsuranceModel, addressModel)));
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: showPHN ? children : childrenPHN,
      ),
    );
  }

  getFullImageContainer(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, File path) {
    return FullImageContainer(
      image: FileImage(path),
      onPressed: () {
        if (front == false && source.contains("FrontImage")) {
          front = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_click);
        } else if (back == false && source.contains("BackImage")) {
          back = true;
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_click);
        }
        Navigator.push(context, MaterialPageRoute(builder: (_) => ImageViewer(image: FileImage(path))));
        Image.file(path);
      },
      onDelete: deleteImage(context, profileInsuranceModel, source, 0),
      onEdit: () {
        if (profileInsuranceModel.healthRetryCount == 2) {
          setState(() {
            showPHN = false;
            /*if (timerCountDown != null) {
              timerMessageShow = true;
            }*/
          });
          //showHealthNumberBottomSheet(context, profileInsuranceModel);
        } else {
          revealBottomSheet(context, profileInsuranceModel, source);
        }
      },
      healthCard: true,
    );
  }

  deleteImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, int index) {
    return () async {
      /*prescriptionPages.removeAt(index);
      profileInsuranceModel.deleteImage(source);*/
    };
  }

  Future<void> getImage(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source, var imgSource) async {
    try {
      ImagePicker imagePicker = ImagePicker();
      File image;
      var _image = await imagePicker.pickImage(source: imgSource);
      if (_image == null) return;
      image = File(_image.path);
      var finalImage = await RotateAndCompressImage().rotateAndCompressAndSaveImage(image);
      if (finalImage == null) return;

      bool connectivityResult = await profileInsuranceModel.isInternetConnected();
      if (connectivityResult == false) {
        onFail(context, errMessage: profileInsuranceModel.noInternetConnection);
        return;
      }

      var res = await profileInsuranceModel.addHealthImage(finalImage, source);
      if (res != null && res != "") {
        successMsg = res.item1;
        if (res.item2 == false) {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_digitize_failed);
          if (profileInsuranceModel.healthRetry == false) {
            Fluttertoast.showToast(msg: res.item1, gravity: ToastGravity.BOTTOM, toastLength: Toast.LENGTH_LONG);
            profileInsuranceModel.healthRetry = true;
          } else if (profileInsuranceModel.healthRetry = true) {
            Fluttertoast.showToast(msg: res.item1, gravity: ToastGravity.TOP, toastLength: Toast.LENGTH_LONG);
            setState(() {
              showPHN = false;
              /*if (timerCountDown != null) {
                timerMessageShow = true;
              }*/
            });
            // showHealthNumberBottomSheet(context, profileInsuranceModel);
          }
        } else {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_digitize_success);
          try {
            setState(() {
              if (source == "provincialInsuranceHealthFrontImage") {
                frontImage = finalImage;
              } else {
                backImage = finalImage;
              }
            });
          } catch (ex) {
            FirebaseCrashlytics.instance.log(ex.toString());
          }
          showOnSnackBar(context, successMessage: res.item1);
        }
      }
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }
  }

  getEmptyContainer(BuildContext contexts, ProfileInsuranceModel profileInsuranceModel, String source, String sourceType) {
    return EmptyImageContainer(
      onTap: () async {
        if (profileInsuranceModel.healthRetryCount == 2) {
          //showHealthNumberBottomSheet(context, profileInsuranceModel);
          setState(() {
            showPHN = false;
            /*if (timerCountDown != null) {
              timerMessageShow = true;
            }*/
          });
        } else {
          if (front == false && source.contains("FrontImage")) {
            front = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_click);
          } else if (back == false && source.contains("BackImage")) {
            back = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_click);
          }
          bool successStorage = isIntegration == true
              ? await PermissionUtils().requestPermission(Permission.photos)
              : await PermissionUtils().requestPermission(Permission.storage);
          if (successStorage == true) {
            List<Widget> tiles = [];
            if (sourceType != "PROVINCIAL") {
              bool result = await profileInsuranceModel.getCopyInsuranceCandidate();

              if (result == true) {
                if (profileInsuranceModel.copySecondaryInsuranceList != null && profileInsuranceModel.copySecondaryInsuranceList.length > 0) {
                  for (int i = 0; i < profileInsuranceModel.copySecondaryInsuranceList.length; i++) {
                    Patient? patient = profileInsuranceModel.copySecondaryInsuranceList[i];
                    tiles.add(
                      ListTile(
                        dense: true,
                        title: Row(
                          children: <Widget>[
                            Text(patient!.firstName! + " " + patient.lastName! + " ", style: MEDIUM_XXX_PRIMARY_BOLD),
                            PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.secondary"))
                          ],
                        ),
                        onTap: () async {
                          Navigator.pop(context);
                          await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "SECONDARY", sourceType);
                        },
                      ),
                    );
                  }
                }

                if (profileInsuranceModel.copyPrimaryInsuranceList != null && profileInsuranceModel.copyPrimaryInsuranceList.length > 0) {
                  for (int i = 0; i < profileInsuranceModel.copyPrimaryInsuranceList.length; i++) {
                    Patient? patient = profileInsuranceModel.copyPrimaryInsuranceList[i];
                    tiles.add(
                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              patient!.firstName! + " " + patient.lastName! + " ",
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                            ),
                            PPChip(label: LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.primary"))
                          ],
                        ),
                        onTap: () async {
                          Navigator.pop(context);
                          bool result = await profileInsuranceModel.getCopyInsuranceCardFromCandidate(patient.id!, "PRIMARY", sourceType);
                          if (result == true) {
                            showOnSnackBar(context, successMessage: profileInsuranceModel.errorMessage);
                          } else {
                            // onFail(context,
                            //     errMessage: profileInsuranceModel.errorMessage);
                          }
                        },
                      ),
                    );
                  }
                }
              }
            }
            showModalBottomSheet<void>(
              context: contexts,
              builder: (BuildContext context) {
                print("First dialogue");
                return new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
                    ),
                    PPDivider(),
                    ListTile(
                      key: Key("openCameraSignup2"),
                      leading: new Icon(
                        Icons.photo_camera,
                        color: primaryColor,
                      ),
                      title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
                      onTap: () async {
                        bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                        if (successCamera == true) {
                          Navigator.pop(context);
                          getImage(contexts, profileInsuranceModel, source, ImageSource.camera);
                        } else {
                          PermissionDialog.show(context, "camera");
                        }
                      },
                    ),
                    ListTile(
                      key: Key("openGallerySignup2"),
                      leading: new Icon(Icons.photo_library, color: primaryColor),
                      title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
                      onTap: () async {
                        PermissionStatus status = await Permission.photos.status;
                        bool successPhotos = await PermissionUtils().requestPermission(Permission.photos);
                        if (successPhotos == true || status.isLimited) {
                          Navigator.pop(context);
                          getImage(contexts, profileInsuranceModel, source, ImageSource.gallery);
                        } else {
                          Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
                        }
                      },
                    ),
                    tiles.length > 0
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              PPDivider(),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"),
                                    style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                              ),
                            ],
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                    Column(
                      children: tiles,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              },
            );
          }
        }
      },
      iconText: source.contains("FrontImage")
          ? LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-front")
          : LocalizationUtils.getSingleValueString("insurance", "insurance.labels.upload-back"),
    );
  }

  void revealBottomSheet(BuildContext context, ProfileInsuranceModel profileInsuranceModel, String source) {
    print("Second dialogue");
    List<Widget> tiles = [];
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext contexts) {
        return new Column(
          key: Key("optionSheetSignup"),
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
            ),
            PPDivider(),
            ListTile(
              key: Key("openCameraSignup"),
              leading: new Icon(
                Icons.photo_camera,
                color: primaryColor,
              ),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                bool successCamera = await PermissionUtils().requestPermission(Permission.camera);
                if (successCamera == true) {
                  Navigator.pop(context);
                  getImage(context, profileInsuranceModel, source, ImageSource.camera);
                } else {
                  PermissionDialog.show(context, "camera");
                }
              },
            ),
            ListTile(
              key: Key("openGallerySignup"),
              leading: new Icon(Icons.photo_library, color: primaryColor),
              title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD),
              onTap: () async {
                PermissionStatus status = await Permission.photos.status;
                bool successPhotos = isIntegration == true
                    ? await PermissionUtils().requestPermission(Permission.photos)
                    : await PermissionUtils().requestPermission(Permission.storage);
                if (successPhotos == true || status.isLimited) {
                  Navigator.pop(context);
                  getImage(context, profileInsuranceModel, source, ImageSource.gallery);
                } else {
                  Platform.isIOS ? PermissionDialog.show(context, "photos") : PermissionDialog.show(context, "storage");
                }
              },
            ),
            tiles.length > 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      PPDivider(),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.label-choose"),
                            style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM),
                      ),
                    ],
                  )
                : SizedBox(
                    height: 0.0,
                  ),
            Column(
              children: tiles,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_proceed);
    if (frontImage != null) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_fr_uploaded);
    }
    if (backImage != null) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.healthcard_bk_uploaded);
    }
    if (widget.source == BaseStepperSource.NEW_USER) {
      if (dataStore.readBoolean(DataStoreService.HAS_HEALTHCARD) == true) {
        dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 1);
      } else {
        dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
      }
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      dataStore.writeBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD, false);

      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, true);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_ADD_OTHERS, false);
      dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) == false) {
        if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: false));
        }
      } else if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) == true && dataStore.readString(DataStoreService.EMAIL).toString().isEmpty) {
        if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpEmailWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpEmailWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: false));
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.signup);
        analyticsEvents.logCompleteRegistrationEvent();
        analyticsEvents.mixPanelIdentifier();
        bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED)!;
        if (transferSkipped == true) {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        } else {
          if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) != true) {
            dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, true);
          }
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          //Navigator.of(context).pushNamedAndRemoveUntil(SignUpSuccessWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        }
      }
      /*Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "telehealth", dialogMessage: successMsg));*/
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  showHealthNumberBottomSheet(BuildContext context, ProfileInsuranceModel profileInsuranceModel) {
    Future<bool?> future = showModalBottomSheet<bool>(
        context: context,
        isScrollControlled: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (context) {
          return HealthCardNumberSheetSignup(
            context: context,
            profileInsuranceModel: profileInsuranceModel,
            source: widget.source,
          );
        });
    future.then((bool? value) => {
          if (value != null && value == true)
            {
              if (widget.model == null) {widget.model = SignUpTransferModel()},
              analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_submit),
              goToNextScreen(widget.model!)
            }
        });
  }

  showProvinceBottomSheet(BuildContext context, SignUpTransferModel model, BaseStepperSource source) async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return DraggableScrollableSheet(
            initialChildSize: 0.75,
            minChildSize: 0.75,
            maxChildSize: 0.75,
            expand: false,
            builder: (_, controller) =>
                SelectProvinsSheet(context: context, model: model, source: BaseStepperSource.HEALTH_CARD_SIGNUP_SCREEN, userPatient: widget.userPatient),
          );
        });

    bool healthCardEnabled = dataStore.readBoolean(DataStoreService.TELEHEALTH_HEALTHCARD_ENABLED) == true ? true : false;
    print("HealthCardValue  $healthCardEnabled");
    if (!healthCardEnabled) {
      setState(() {
        showPHN = false;
      });
    } else if (healthCardEnabled) {
      setState(() {
        showPHN = true;
      });
    } else {
      setState(() {
        if (showPHN) {
          showPHN = true;
        } else {
          showPHN = false;
        }
      });
    }
  }

  Widget getTimerView() {
    return Container(
      height: 45,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                height: 45,
                child: Center(
                  child: Text(
                    LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.mobile-view").isEmpty
                        ? "The slot of " + dataStore.readString(DataStoreService.TELEHEALTH_APPOINTMENT_TIME).toString() + " is blocked for you for"
                        : LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.mobile-view")
                            .replaceAll("{{appointmentSlot}}", dataStore.readString(DataStoreService.TELEHEALTH_APPOINTMENT_TIME).toString()),
                    style: TextStyle(
                      fontSize: 14,
                      color: timerColor,
                      fontFamily: "FSJoeyPro Bold",
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 6,
            ),
            /*Row(
              children: [
                timerCountDown == null ? getTimerCounter() : timerCountDown,
                SizedBox(
                  width: 2,
                ),
                Text(
                  LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.min").isEmpty
                      ? "min"
                      : LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.min"),
                  style: TextStyle(fontFamily: "FSJoeyPro", fontSize: 14),
                ),
              ],
            ),*/
          ],
        ),
      ),
    );
  }

  /*void showTimerBottomSheet(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (BuildContext contexts) {
        return new Column(
          key: Key("timerSheet"),
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.modal-title").isEmpty
                        ? "The slot may not be available anymore"
                        : LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.modal-title"),
                    style: TextStyle(fontSize: 18, fontFamily: "FSJoeyPro Heavy", color: timerColor),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                      LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.modal-desc").isEmpty
                          ? "Please proceed quickly to book the appointment."
                          : LocalizationUtils.getSingleValueString("healthcard", "healthcard.timer.modal-desc"),
                      style: TextStyle(fontSize: 14, fontFamily: "FSJoeyPro", color: thinGrey)),
                ],
              ),
            ),
            PPDivider(),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: PrimaryButton(
                key: Key("buttonProceed"),
                fullWidth: true,
                disabled: false,
                butttonColor: brandColor,
                textColor: whiteColor,
                text: LocalizationUtils.getSingleValueString("common", "common.button.proceed").isEmpty
                    ? "Proceed"
                    : LocalizationUtils.getSingleValueString("common", "common.button.proceed"),
                onPressed: () {
                  setState(() {
                    timerCountDown = null;
                    timerMessageShow = true;
                  });

                  Navigator.pop(contexts);
                },
              ),
            ),
          ],
        );
      },
    );
  }*/

  /*bool getTimerStatus() {
    return widget.appointmentDateTime == null ? true : false;
  }*/

  Widget getStartedFlow(ProfileInsuranceModel profileInsuranceModel, ProfileAddressModel addressModel) {
    _datePhnController.addListener(datePhnControllerListener);
    _monthPhnController.addListener(monthPhnControllerListener);
    _yearPhnController.addListener(yearPhnControllerListener);
    _postalCodeController.addListener(pharmacyControllerListener);
    genderRadioGroup = null;
    initializeContent();
    return Padding(
        padding: EdgeInsets.only(
          left: SMALL_XXX,
          right: SMALL_XXX,
          top: SMALL_XXX,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: SMALL_XXX),
            getPhnField(profileInsuranceModel),
            dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario" ? SizedBox(height: MEDIUM_XX) : Container(),
            dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario" ? getPHNExpiry() : Container(),
            SizedBox(height: MEDIUM_XX),
            getGenderField(),
            (dataStore.readString(DataStoreService.PHONE).toString().isEmpty) ? getPhoneField() : Container(),
            PPUIHelper.verticalSpaceMedium(),
            PPTexts.getFormLabelAsterisk(
                LocalizationUtils.getSingleValueString("address", "signup.fields.address").isEmpty
                    ? "Address"
                    : LocalizationUtils.getSingleValueString("address", "signup.fields.address"),
                fontSize: 16.8,
                fontFamily: "FSJoeyPro Bold",
                color: darkPrimaryColor,
                isAsteriskEnable: false),
            PPUIHelper.verticalSpaceLarge(),
            PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.street"),
                fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
            SizedBox(
              height: 4,
            ),
            getAutoCompleteField(context, addressModel)!,
            stAddressError == null ? SizedBox(height: 2) : PPTexts.getFormError(stAddressError!, color: errorColor),
            stAddressError != null ? PPUIHelper.verticalSpaceXMedium() : Container(),
            /*PPFormFields.getTextField(
                autovalidate: autovalidate,
                minLength: 1,
                textInputAction: TextInputAction.next,
                controller: _stController,
                focusNode: _stFnode,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
                  } else if (value.isNotEmpty && value.contains("#")) {
                    return LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
                        ? "Special character not allowed"
                        : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
                  } else {
                    return null;
                  }
                },
                decoration: PPInputDecor.getDecoration(
                    labelText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
                    hintText: LocalizationUtils.getSingleValueString("address", "address.label.line1"),
                    helperText: "",
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                onFieldSubmitted: (value) {
                  _fieldFocusChange(context, _stFnode, _st2Fnode);
                }),*/
            PPUIHelper.verticalSpaceXMedium(),
            PPFormFields.getTextField(
                autovalidate: autovalidate,
                minLength: 1,
                textInputAction: TextInputAction.next,
                controller: _st2Controller,
                focusNode: _st2Fnode,
                validator: (value) {
                  if (value != null && value.isNotEmpty && value.contains("#")) {
                    return LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
                        ? "Special character not allowed"
                        : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
                  } else {
                    return null;
                  }
                },
                decoration: PPInputDecor.getDecoration(
                    labelText: LocalizationUtils.getSingleValueString("address", "address.label.line2"),
                    hintText: LocalizationUtils.getSingleValueString("address", "address.label.line2"),
                    helperText: "",
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                onFieldSubmitted: (value) {
                  _fieldFocusChange(context, _st2Fnode, _cityFnode);
                }),
            PPUIHelper.verticalSpaceXMedium(),
            PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.city"),
                fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
            SizedBox(
              height: 2,
            ),
            PPFormFields.getTextField(
                autovalidate: autovalidate,
                minLength: 1,
                textInputAction: TextInputAction.done,
                focusNode: _cityFnode,
                controller: _cityController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
                  } else if (value.isNotEmpty && value.contains("#")) {
                    return LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char").isEmpty
                        ? "Special character not allowed"
                        : LocalizationUtils.getSingleValueString("address", "address.fields.address-exclude-char");
                  } else {
                    return null;
                  }
                },
                decoration: PPInputDecor.getDecoration(
                    labelText: LocalizationUtils.getSingleValueString("address", "address.label.city"),
                    hintText: LocalizationUtils.getSingleValueString("address", "address.label.city"),
                    helperText: "",
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                onFieldSubmitted: (value) {
                  _fieldFocusChange(context, _cityFnode, _postalFnode);
                }),
            PPUIHelper.verticalSpaceXMedium(),
            PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.label.province"),
                fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
            SizedBox(
              height: 2,
            ),
            getDropDown(profileInsuranceModel),
            dropDownError == null ? SizedBox(height: 20) : PPTexts.getFormError(dropDownError!, color: errorColor),
            PPUIHelper.verticalSpaceXMedium(),
            PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
            SizedBox(
              height: 2,
            ),
            PPFormFields.getTextField(
              key: Key("addressPin"),
              autovalidate: autovalidate,
              textCapitalization: TextCapitalization.characters,
              minLength: 1,
              textInputAction: TextInputAction.done,
              controller: _postalCodeController,
              focusNode: _postalFnode,
              validator: (value) {
                RegExp exp = new RegExp(r"^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$");
                bool val = exp.hasMatch(value.toString().toUpperCase());
                if (val == true) return null;
                return value.toString().isEmpty
                    ? LocalizationUtils.getSingleValueString("address", "address.fields.required-error")
                    : LocalizationUtils.getSingleValueString("address", "address.fields.postalcode-error");
              },
              onFieldSubmitted: (value) {},
              decoration: PPInputDecor.getDecoration(
                  labelText: LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                  hintText: LocalizationUtils.getSingleValueString("address", "address.fields.postal-input"),
                  helperText: "",
                  floatingLabelBehavior: FloatingLabelBehavior.never),
            ),
            PPUIHelper.verticalSpaceXMedium(),
            CheckboxListTile(
              contentPadding: EdgeInsets.zero,
              title: PPTexts.getFormLabelAsterisk(
                  LocalizationUtils.getSingleValueString("address", "address.label.address-disclaimer").isEmpty
                      ? "I confirm that my details above are correct and that I'm a resident of {{provinceMapName}}."
                          .replaceAll("{{provinceMapName}}", getProvinceNameByLanguage(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString()))
                      : LocalizationUtils.getSingleValueString("address", "address.label.address-disclaimer")
                          .toString()
                          .replaceAll("{{provinceMapName}}", getProvinceNameByLanguage(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString())),
                  style: TextStyle(fontFamily: "FSJoeyPro Medium"),
                  fontSize: 17.3),
              value: defaultValue,
              activeColor: brandColor,
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool? value) => setState(() {
                defaultValue = !defaultValue;
              }),
            ),
            PPUIHelper.verticalSpaceLarge(),
          ],
        ));
  }

  Widget getPHNExpiry() {
    SignUpModel model = SignUpModel();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PPTexts.getFormLabelAsterisk(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.hcexpiry").isEmpty
                ? "Health Card Expiry Date"
                : LocalizationUtils.getSingleValueString("signup", "signup.fields.hcexpiry"),
            fontSize: 15,
            fontFamily: "FSJoeyPro Bold",
            color: darkPrimaryColor),
        SizedBox(height: 8),
        Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: PPFormFields.getNumericFormField(
                        key: Key("dobMM"),
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.phone,
                        maxLength: 2,
                        focusNode: _monthPhnFnode,
                        textInputAction: TextInputAction.next,
                        validator: (value) => validatePhnMonth(value!, showErr: false),
                        decoration: PPInputDecor.getDecoration(
                            collapsed: false,
                            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-month"),
                            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-month"),
                            helperText: ' ',
                            floatingLabelBehavior: FloatingLabelBehavior.never),
                        /*.copyWith(
                          contentPadding: EdgeInsets.only(left: 6, right: 0, top: 0, bottom: 0),
                          errorStyle: TextStyle(height: 1, fontFamily: "FSJoeyPro"),
                            floatingLabelBehavior: FloatingLabelBehavior.never
                        ),*/
                        controller: _monthPhnController,
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _monthPhnFnode, _datePhnFnode);
                        }),
                  ),
                  SizedBox(width: MEDIUM_XXX),
                  Expanded(
                    flex: 1,
                    child: PPFormFields.getNumericFormField(
                        key: Key("dobDD"),
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.phone,
                        maxLength: 2,
                        validator: (value) => validatePhnDate(value!, showErr: false),
                        decoration: PPInputDecor.getDecoration(
                            collapsed: false,
                            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-date"),
                            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-date"),
                            helperText: ' ',
                            floatingLabelBehavior: FloatingLabelBehavior.never),
                        /*.copyWith(
                                contentPadding: EdgeInsets.only(left: 6, right: 0, top: 0, bottom: 0),
                                errorStyle: TextStyle(height: 1, fontFamily: "FSJoeyPro"),
                                ),*/
                        controller: _datePhnController,
                        textInputAction: TextInputAction.next,
                        focusNode: _datePhnFnode,
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _datePhnFnode, _yearPhnFnode);
                        }),
                  ),
                  SizedBox(width: MEDIUM_XXX),
                  Expanded(
                    flex: 2,
                    child: PPFormFields.getNumericFormField(
                        key: Key("dobYYYY"),
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.phone,
                        maxLength: 4,
                        validator: (value) => validatePhnYear(value!, showErr: false),
                        decoration: PPInputDecor.getDecoration(
                          collapsed: false,
                          labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-year"),
                          hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-year"),
                          helperText: ' ',
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                        ),
                        controller: _yearPhnController,
                        textInputAction: TextInputAction.done,
                        focusNode: _yearPhnFnode,
                        onFieldSubmitted: (value) {
                          if (dataStore.readString(DataStoreService.PHONE).toString().isNotEmpty) {
                            _fieldFocusChange(context, _yearPhnFnode, _postalFnode);
                          } else {
                            _fieldFocusChange(context, _yearPhnFnode, _phoneFnode);
                          }
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
        getPhnErrorText(),
      ],
    );
  }

  Widget getPhnField(ProfileInsuranceModel profileInsuranceModel) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PPTexts.getFormLabelAsterisk(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.phn").isEmpty
                ? "Health card Number"
                : LocalizationUtils.getSingleValueString("signup", "signup.fields.phn"),
            fontSize: 15,
            fontFamily: "FSJoeyPro Bold",
            color: darkPrimaryColor),
        SizedBox(
          height: 5,
        ),
        PPFormFields.getTextField(
          key: Key("phNumber"),
          textCapitalization: TextCapitalization.words,
          autovalidate: autovalidate,
          focusNode: _phnFnode,
          controller: _phnController,
          onTextChanged: (value) {
            //_formKey.currentState.validate();
          },
          textInputAction: TextInputAction.next,
          onErrorStr: LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.empty-error"),
          onFieldSubmitted: (value) {
            if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario") {
              _fieldFocusChange(context, _phnFnode, _monthPhnFnode);
            } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().isNotEmpty) {
              _fieldFocusChange(context, _phnFnode, _stFnode);
            } else {
              _fieldFocusChange(context, _phnFnode, _phoneFnode);
            }
          },
          validator: (value) {
            RegExp exp = getRegex();
            bool val = exp.hasMatch(value.toString());
            if (val == true) return null;
            return value.toString().isEmpty
                ? LocalizationUtils.getSingleValueString("address", "address.fields.required-error")
                : LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.invalid-error");
          },
          decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.healthcard-input").isEmpty
                ? "Enter without any spaces and hyphens(-)"
                : LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.healthcard-input"),
            hintText: LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.healthcard-input").isEmpty
                ? "Enter without any spaces and hyphens(-)"
                : LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.healthcard-input"),
            floatingLabelBehavior: FloatingLabelBehavior.never,
          ),
        ),
      ],
    );
  }

  Widget getPhnErrorText() {
    if (autovalidate == false && showPhnDateError) {
      if (!isDatePhnValid()) {
        return Align(
          alignment: Alignment.topLeft,
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.hc-expired").isEmpty
                ? "Your Health Card is Expired."
                : LocalizationUtils.getSingleValueString("signup", "signup.fields.hc-expired"),
            style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
          ),
        );
      } else {
        return Align(
          alignment: Alignment.topLeft,
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc").isEmpty
                ? "Please enter a valid Date"
                : LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc"),
            style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
          ),
        );
      }
    } else if (autovalidate && dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario") {
      if ((_monthPhnController.text.isEmpty || _datePhnController.text.isEmpty || _yearPhnController.text.isEmpty)) {
        return Align(
          alignment: Alignment.topLeft,
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc").isEmpty
                ? "Please enter a valid Date"
                : LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc"),
            style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
          ),
        );
      } else if (showPhnDateError) {
        if (!isDatePhnValid()) {
          return Align(
            alignment: Alignment.topLeft,
            child: Text(
              LocalizationUtils.getSingleValueString("signup", "signup.fields.hc-expired").isEmpty
                  ? "Your Health Card is Expired."
                  : LocalizationUtils.getSingleValueString("signup", "signup.fields.hc-expired"),
              style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
            ),
          );
        } else {
          return Align(
            alignment: Alignment.topLeft,
            child: Text(
              LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc").isEmpty
                  ? "Please enter a valid Date"
                  : LocalizationUtils.getSingleValueString("signup", "signup.fields.invalid-error-hc"),
              style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
            ),
          );
        }
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }

  /*getTimerCounter() {
    return timerCountDown = TimerCountdown(
      enableDescriptions: false,
      format: CountDownTimerFormat.minutesSeconds,
      endTime: DateTime.now().add(
        Duration(
          minutes: 5,
          seconds: 0,
        ),
      ),
      onEnd: () {
        showTimerBottomSheet(context);
        print("Timer finished");
      },
      timeTextStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: "FSJoeyPro Heavy"),
      colonsTextStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: "FSJoeyPro Heavy"),
      spacerWidth: 2,
    );
  }
*/
  Widget getDropDown(ProfileInsuranceModel profileInsuranceModel) {
    List<TelehealthProvinceItem> provinceArray = fetchStateMap(profileInsuranceModel.provinceArrayList);
    dropDownValue = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase().replaceAll(" ", "_");
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("address", "address.field.select-province"),
        fullWidth: true,
        value: dropDownValue,
        items: provinceArray
            .map((state) => DropdownMenuItem<String>(
                value: state.value,
                child: Text(
                  state.label,
                  style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                )))
            .toList(),
        onChanged:
            null /*(selectedItem) => setState(() {
          dropDownValue = getServerSendingValue(selectedItem!);
          dropDownError = null;
        })*/
        ,
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("address", "address.field.select-province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: provinceArray
            .map((state) => DropdownMenuItem<String>(
                value: state.value,
                child: Text(
                  state.label,
                  style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                )))
            .toList(),
        onChanged:
            null /*(selectedItem) => setState(() {
          dropDownValue = getServerSendingValue(selectedItem!);
          dropDownError = null;
        })*/
        ,
      );
  }

  void onClickHealthCardNumber(BuildContext context, ProfileInsuranceModel profileInsuranceModel) async {
    print("im hit healthcard api section");
    if (autovalidate == false) {
      setState(() {
        autovalidate = true;
      });
    }
    if (selectedGender == null || selectedGender!.isEmpty) {
      setState(() {
        genderError = LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
      });
    }
    if (dropDownValue == null) {
      setState(() {
        dropDownError = LocalizationUtils.getSingleValueString("address", "address.fields.required-error");
      });
    }
    if (_formKey.currentState!.validate()) {
      print("im hit healthcard submit section");

      var connectivityResult = await profileInsuranceModel.checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        onFail(context, errMessage: profileInsuranceModel.noInternetConnection);
        return;
      }
      addPhnAndAddress(context, profileInsuranceModel);
      /*var healthCardStatus = await profileInsuranceModel.validateHealthCardNumber(_phnController.text.toString(), getProvinceStorage());
      if (healthCardStatus != null && healthCardStatus == true) {
        addPhnAndAddress(context, profileInsuranceModel);
      } else {
        Fluttertoast.showToast(
            msg: LocalizationUtils.getSingleValueString("insurance", "insurance.retrymodal.invalid-error"), timeInSecForIosWeb: 3, fontSize: 14.0);
      }*/
    }
  }

  String getProvinceStorage() {
    if (dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD) != null &&
        dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD).toString().isNotEmpty) {
      return dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD)!.toLowerCase();
    }
    if (dataStore.readString(DataStoreService.PROVINCE) != null && dataStore.readString(DataStoreService.PROVINCE).toString().isNotEmpty) {
      return dataStore.readString(DataStoreService.PROVINCE)!.toLowerCase();
    } else {
      return '';
    }
  }

  addPhnAndAddress(BuildContext context, ProfileInsuranceModel profileInsuranceModel) async {
    AddPHNAndAddress addPHNAndAddress = AddPHNAndAddress();
    addPHNAndAddress.phn = _phnController.text.toString();
    if (dataStore.readString(DataStoreService.PHONE).toString().isEmpty) {
      addPHNAndAddress.phone = _cellController.text.toString();
    }
    String sendingGender = getGenderEnglish(selectedGender);
    addPHNAndAddress.gender = sendingGender.isEmpty ? dataStore.readString(DataStoreService.GENDER).toString() : getGenderEnglish(selectedGender);
    if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario") {
      addPHNAndAddress.healthCardExpiryDate =
          _yearPhnController.text.toString() + "-" + _monthPhnController.text.toString() + "-" + _datePhnController.text.toString();
    }
    PatientPhnAddress patientPhnAddress = PatientPhnAddress();
    patientPhnAddress.city = _cityController.text.toString();
    patientPhnAddress.province = dropDownValue.toString().replaceAll("_", " ").toTitleCase();
    patientPhnAddress.streetAddress = _stController.text.toString();
    patientPhnAddress.postalCode = _postalCodeController.text.toString();
    patientPhnAddress.streetAddressLineTwo = _st2Controller.text.toString();
    addPHNAndAddress.patientPhnAddress = patientPhnAddress;

    var addCardNumber = await profileInsuranceModel.addPHNWithAddress(addPHNAndAddress);
    if (addCardNumber != null && addCardNumber.item2 == true) {
      print("im hit healthcard submit section");
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_healthcard_submit);
      if (widget.model == null) {
        widget.model = SignUpTransferModel();
      }
      goToNextScreen(widget.model!);
    } else {
      Fluttertoast.showToast(msg: profileInsuranceModel.errorMessage, timeInSecForIosWeb: 3, fontSize: 14.0);
    }
  }

  CallNextStep(BuildContext context, ProfileInsuranceModel profileInsuranceModel) async {
    bool successGetUserInfo = await profileInsuranceModel.getUserInfo(listenable: true);
    if (successGetUserInfo) {
      setState(() {
        if (!["", null].contains(dataStore.readString(DataStoreService.PHN)) && _phnController.text.isEmpty) {
          _phnController.text = dataStore.readString(DataStoreService.PHN) ?? "";
        }
        if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().toLowerCase() == "ontario") {
          String? phnExpiry = dataStore.readString(DataStoreService.PHNExpiry);
          List<String> phnExpiryString = (phnExpiry != null) ? phnExpiry.split("-") : [];
          if (phnExpiryString.length == 3) {
            if (_yearPhnController.text.isEmpty) {
              _yearPhnController.text = phnExpiryString[0];
            }
            if (_monthPhnController.text.isEmpty) {
              _monthPhnController.text = phnExpiryString[1];
            }
            if (_datePhnController.text.isEmpty) {
              _datePhnController.text = phnExpiryString[2];
            }
          }
        }
        /*if (timerCountDown != null) {
          timerMessageShow = true;
        }*/
        showPHN = false;
      });
    } else {
      onFail(context, errMessage: profileInsuranceModel.errorMessage);
    }
  }

  List<TelehealthProvinceItem> fetchStateMap(List<TelehealthProvinceItem> provinceArrayList) {
    List<TelehealthProvinceItem> provinceList = [];
    for (int j = 0; j < provinceArrayList.length; j++) {
      String provinceKey = provinceArrayList[j].label;
      for (int i = 0; i < getProvinceList.length; i++) {
        if (provinceKey == getProvinceList[i]["key"]) {
          TelehealthProvinceItem data = TelehealthProvinceItem(provinceKey.replaceAll(" ", "_").toLowerCase(), getProvinceList[i]["value"]!);
          provinceList.add(data);
          break;
        }
      }
    }
    return provinceList;
  }

  Widget getGenderField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PPUIHelper.verticalSpaceSmall(),
        getGenderUI(),
        genderError == null ? SizedBox(height: 20) : PPTexts.getFormError(genderError!, color: errorColor),
      ],
    );
  }

  Widget getPhoneField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PPUIHelper.verticalSpaceSmall(),
        PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.phone-number"),
            fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
        SizedBox(
          height: 5,
        ),
        PPFormFields.getNumericFormField(
            key: Key("addPhone"),
            autovalidate: autovalidate,
            textInputAction: TextInputAction.next,
            focusNode: _phoneFnode,
            maxLength: 10,
            minLength: 10,
            keyboardType: TextInputType.phone,
            controller: _cellController,
            errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
            decoration: PPInputDecor.getDecoration(
              labelText: "",
              hintText: "",
              helperText: '',
              prefixIcon: Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '+1 |',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "FSJoeyPro Medium",
                    ),
                  )),
            ),
            onFieldSubmitted: (value) {
              _fieldFocusChange(context, _phoneFnode, _cityFnode);
            }),
      ],
    );
  }

  Widget getGenderUI() {
    return genderRadioGroup!;
  }

  void initializeContent() {
    genderRadioGroup = PPRadioGroup(
      key: Key("GenderRadioGroup"),
      isAsterisk: true,
      initialValue: selectedGender,
      radioOptions: [
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-label"),
      onChange: (value) {
        selectedGender = value;
        setState(() {
          genderError = null;
        });
      },
      addBottomSpace: false,
    );
  }

  getGenderEnglish(String? value) {
    if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase()) {
      return "MALE";
    } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase()) {
      return "FEMALE";
    } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()) {
      return "OTHER";
    } else {
      return "";
    }
  }

  RegExp getRegex() {
    if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "BRITISH_COLUMBIA") {
      return RegExp(r"^[0-9]{1,10}$");
    } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NORTHWEST_TERRITORIES") {
      return RegExp(r"^[nN][0-9]{1,7}$");
    } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "ONTARIO") {
      return RegExp(r"^[0-9]{1,10}[A-Za-z]{2}$");
    } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NEWFOUNDLAND_AND_LABRADOR") {
      return RegExp(r"^[0-9]{1,12}$");
    } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "PRINCE_EDWARD_ISLAND") {
      return RegExp(r"^[0-9]{1,8}$");
    } else if (dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE).toString().replaceAll(" ", "_").toUpperCase() == "NOVA_SCOTIA") {
      return RegExp(r"^[0-9]{1,10}$");
    } else {
      return RegExp(r"^[0-9]{1,9}$");
    }
  }
}

String getDescriptionTextImage(String gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-healthcard");
      break;
  }
}
