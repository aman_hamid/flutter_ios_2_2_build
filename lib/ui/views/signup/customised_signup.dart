import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription_arguments.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/telehealth_signup_bottom_sheet.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/utils/analytics_event.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';

import '../../../locator.dart';
import '../base_state.dart';
import '../error_screen.dart';

class CustomisedSignUpWidget extends StatefulWidget {
  static const routeName = 'customised_signup';
  BaseStepperSource? source;
  String? firstName;
  final UserPatient? userPatient;

  CustomisedSignUpWidget({this.source, this.firstName, this.userPatient});

  @override
  _CustomisedSignUpWidgetState createState() => _CustomisedSignUpWidgetState();
}

class _CustomisedSignUpWidgetState extends BaseState<CustomisedSignUpWidget> with WidgetsBindingObserver {
  bool show = false;
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.lead_options);
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      startTimer();
      await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, true);
    });
    super.initState();
  }

  void startTimer() {
    Timer(Duration(seconds: 5), () {
      setState(() {
        show = true;
      });
    });
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SignUpModel>(create: (_) => SignUpModel(httpApiUtils: Provider.of<HttpApiUtils>(context))),
        ChangeNotifierProvider<SignUpTransferModel>(create: (_) => SignUpTransferModel())
      ],
      child: Consumer2<SignUpModel, SignUpTransferModel>(builder: (BuildContext context, SignUpModel model, SignUpTransferModel transferModel, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model, context),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (transferModel.state == ViewState.Busy) {
                return LoadingScreen();
              } else if (snapshot.hasData != null && snapshot.data != null) {
                return WillPopScope(onWillPop: () => _onBackPressed(context), child: getMainView(model, transferModel));
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(SignUpModel model, BuildContext context) async {
    Future<Map<String, dynamic>?> future1 =
        model.getLocalization(["signup", "customize-signup", "forgot", "common", "modal", "modals", "copay", "dropoff"]); // will take 3 secs
    return await Future.wait([future1]);
  }

  Widget getMainView(SignUpModel model, SignUpTransferModel transferModel) {
    return Scaffold(
      key: Key("CustomisedView"),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: isProvinceQuebec() == true
            ? Image.network(
                "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                width: MediaQuery.of(context).size.width * 0.45,
                fit: BoxFit.cover,
              )
            : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
        leading: Container(),
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: MEDIUM_XXX,
          right: MEDIUM_XXX,
          top: REGULAR_XXX,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Avatar(
                displayImage: 'https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg',
              ),
            ),
            SizedBox(
              height: PPUIHelper.VerticalSpaceXMedium,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Text(
                LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.about.description')
                    .replaceAll('{{name}}', dataStore.readString(DataStoreService.FIRSTNAME)!),
                textAlign: TextAlign.center,
                style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
              ),
            ),
            SizedBox(height: REGULAR_XXX),
            getTransferCard(transferModel),
            SizedBox(height: SMALL_XXX),
            getTelehealthView(model, transferModel),
            getUploadCard(transferModel),
            SizedBox(height: LARGE_X),
            show
                ? TextButton(
                    key: Key("moreOption"),
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.all<Color>(Color(0xffc9b7e5)),
                    ),
                    onPressed: () async {
                      //await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
                      showCallAndChatBottomSheet(context, transferModel);
                    },
                    child: Text(
                      LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.cta.looking-for-something-else'),
                      style: TextStyle(
                        fontSize: 14,
                        color: Color(0xff4e2a84),
                      ),
                    ))
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget getTransferCard(SignUpTransferModel transferModel) {
    return InkWell(
      key: Key("transfer_button"),
      onTap: () async {
        await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION, false);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, true);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_customise_transfer_select);
        setState(() {
          show = false;
        });
        bool success = await transferModel.updatePrescriptionFlow("TRANSFER");
        if(success) {
          Navigator.pushNamed(context, TransferWidget.routeName, arguments: TransferArguments(source: BaseStepperSource.NEW_USER)).then((val) {
            startTimer();
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color(0xffcbd7f4).withOpacity(0.8),
              //offset: Offset(3, 3),
              blurRadius: 0.8,
              spreadRadius: 0.1,
            )
          ],
          borderRadius: BorderRadius.circular(10.0),
          color: Color(0xffeff3fe),
          border: Border.all(color: Color(0xffdfe7fc), width: 1.0),
        ),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Color(0xffdfe7fc),
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffb7bcc9).withOpacity(0.5),
                        //offset: Offset(-2.0, -2.0),
                        blurRadius: 0.8,
                        spreadRadius: 0.1,
                      ),
                    ],
                  ),
                  child: Icon(
                    Icons.autorenew,
                    color: Color(0xff4e2a84),
                    size: 40,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.transfer.title'),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          fontFamily: "FSJoeyPro Bold",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 5),
                      child: Text(
                        LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.transfer.subtitle'),
                        style: TextStyle(
                          color: Color(0xff747474),
                          fontWeight: FontWeight.normal,
                          fontSize: 14,
                          fontFamily: "FSJoeyPro",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getUploadCard(SignUpTransferModel transferModel) {
    return InkWell(
      key: Key("prescription_button"),
      onTap: () async {
        await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION, true);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
        await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, false);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_customise_upload_select);
        setState(() {
          show = false;
        });
        bool success = await transferModel.updatePrescriptionFlow("UPLOAD");
        if(success) {
          Navigator.pushNamed(context, UploadPrescription.routeName,
              arguments: UploadPrescriptionArguments(source: BaseStepperSource.NEW_USER, from: 'customize-signup'))
              .then((val) {
            startTimer();
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color(0xfffde0d5).withOpacity(0.8), //
              //offset: Offset(-3, 3),
              blurRadius: 0.8,
              spreadRadius: 0.1,
            )
          ],
          borderRadius: BorderRadius.circular(10.0),
          color: Color(0xfffff3ee),
          border: Border.all(color: Color(0xfffde0d5), width: 1.0),
        ),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(color: Color(0xfffde0d5), borderRadius: BorderRadius.circular(8), boxShadow: [
                    BoxShadow(
                      color: Color(0xfffde0d5).withOpacity(0.5),
                      //offset: Offset(-2.0, -2.0),
                      blurRadius: 0.8,
                      spreadRadius: 0.1,
                    )
                  ]),
                  child: Icon(
                    Icons.photo_camera,
                    color: Color(0xffdd6c45),
                    size: 40,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.upload.title'),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          fontFamily: "FSJoeyPro Bold",
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 5),
                      child: Text(
                        LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.upload.subtitle'),
                        style: TextStyle(
                          color: Color(0xff747474),
                          fontWeight: FontWeight.normal,
                          fontSize: 14,
                          fontFamily: "FSJoeyPro",
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getTelehealthCard(SignUpModel model, SignUpTransferModel transferModel) {
    if (!LocalizationUtils.isProvinceQuebec()) {
      return InkWell(
        key: Key("telehealth_button"),
        onTap: () async {
          setState(() {
            show = false;
          });
          if(dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE) !=null && dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE)!.isNotEmpty){
            String thProvince = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE)??"";
            PPApplication.province = thProvince;
            PPApplication.provinceArrayListMemo != null &&
                PPApplication.provinceArrayListMemo!.any((element) => (element.label.contains(thProvince)))
                ? goToNextScreen(context, transferModel)
                : showProvinceBottomSheet(context, transferModel);
          }else {
            PPApplication.provinceArrayListMemo != null &&
                PPApplication.provinceArrayListMemo!.any((element) => (PPApplication.province != null && element.label.contains(PPApplication.province!)))
                ? goToNextScreen(context, transferModel)
                : showProvinceBottomSheet(context, transferModel);
          }
        },
        child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Color(0xffd9f2ec).withOpacity(0.8), //
                //offset: Offset(-3, 3),
                blurRadius: 0.8,
                spreadRadius: 0.1,
              )
            ],
            borderRadius: BorderRadius.circular(10.0),
            color: Color(0xffeffef6),
            border: Border.all(color: Color(0xffd9f2ec), width: 1.0),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(color: Color(0xffd9f2ec), borderRadius: BorderRadius.circular(8), boxShadow: [
                      BoxShadow(
                          color: Color(0xffd9f2ec).withOpacity(0.5),
                          //offset: Offset(-2.0, -2.0),
                          blurRadius: 0.8,
                          spreadRadius: 0.1)
                    ]), //Color(0xff85dbc5).withOpacity(0.5)
                    child: Icon(
                      Icons.event,
                      color: Color(0xff5e8d87),
                      size: 40,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Align(
                  alignment: Alignment.topRight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text(
                          LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.telehealth.title'),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            fontFamily: "FSJoeyPro Bold",
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 5),
                        child: Text(
                          LocalizationUtils.getSingleValueString('customize-signup', 'customize-signup.telehealth.subtitle'),
                          style: TextStyle(
                            color: Color(0xff747474),
                            fontWeight: FontWeight.normal,
                            fontSize: 14,
                            fontFamily: "FSJoeyPro",
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void goToNextScreen(BuildContext context, SignUpTransferModel model) async {
    bool connectivityResult = await model.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    bool success1 = await model.updatePrescriptionFlow("TELEHEALTH");
    bool success = await model.telehealthProvince(PPApplication.province!);

    if (success) {
      await dataStore.writeBoolean(DataStoreService.CUSTOMIZED_SIGNUP_PENDING, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_PRESCRIPTION, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TRANSFER, false);
      await dataStore.writeBoolean(DataStoreService.PENDING_SIGNUP_TELEHEALTH, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_customise_telehealth_select);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_appointment_province_submit);
      await dataStore.writeString(DataStoreService.TELEHEALTH_PROVINCE, PPApplication.province!);
      Navigator.pushNamed(context, TelehealthPreference.routeName,
          arguments: TelehealthArguments(
            modelSignUp: model,
            source: widget.source,
            userPatient: widget.userPatient,
            from: "telehealth",
          ));
    } else {
      Fluttertoast.showToast(msg: LocalizationUtils.getSingleValueString("common", "common.label.error-message"));
    }
  }

  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
          return SkipSignupSheet(context: context, model: model, source: BaseStepperSource.NEW_USER, userPatient: widget.userPatient, from: "customize-signup");
        });
  }

  showProvinceBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return DraggableScrollableSheet(
            initialChildSize: 0.75,
            minChildSize: 0.75,
            maxChildSize: 0.75,
            expand: false,
            builder: (_, controller) => SelectProvinsSheet(context: context, model: model, source: widget.source, userPatient: widget.userPatient),
          );
        }).then((value) => startTimer());
  }

  Widget getTelehealthView(SignUpModel model, SignUpTransferModel transferModel) {
    return Column(
      children: [
        getTelehealthCard(model, transferModel),
        SizedBox(height: SMALL_XXX),
      ],
    );
  }
}
