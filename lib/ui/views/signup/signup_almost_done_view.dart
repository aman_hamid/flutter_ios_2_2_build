import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/models/transfer/transfer_search_selection_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/customised_signup.dart';
import 'package:pocketpills/ui/views/signup/details/prescription_details_signup.dart';
import 'package:pocketpills/ui/views/signup/details/transfer_details_signup.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:pocketpills/utils/toast/success_toast.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class SignUpAlmostDoneWidget extends StatefulWidget {
  static const routeName = 'signUpAlmostDoneWidget';
  final Function? onSuccess;
  final Function(String)? onShowSnackBar;
  final Function? onBack;
  final BaseStepperSource? source;
  final String? from;
  bool? chambersFlow;
  final String? dialogMsg;
  final List<File>? prescriptionList;
  String? province;

  SignUpAlmostDoneWidget({
    Key? key,
    this.onBack,
    this.onSuccess,
    this.onShowSnackBar,
    this.source,
    this.from,
    this.chambersFlow,
    this.dialogMsg,
    this.prescriptionList,
    province,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpAlmostDoneState();
  }
}

class SignUpAlmostDoneState extends BaseState<SignUpAlmostDoneWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  PPRadioGroup<String>? selfMedicationRadioGroup;
  PPRadioGroup<String>? manageMedicationRadioGroup;
  PPRadioGroup<String>? genderRadioGroup;
  final _aboutUsformKey = GlobalKey<FormState>();
  String? dropDownValue = null;
  String? dropDownError;
  String? dropDownInfo;
  String prevMobileNumber = "";
  String prevEmailAddress = "";
  String phState = '';
  String? selectedGender;

  bool manitobaCheck = true;
  bool showReferral = false;
  bool autovalidate = false;
  bool emAutoValidate = false;
  bool phoneAlreadyRegister = false;
  bool eddCardVisible = false;
  bool emailAlreadyRegister = false;
  String? eddDropDownError;
  String? eddDropDownValue = null;
  String? postalCode;
  String? province;
  String visibleField = "";

  bool completeContactDetails = false;
  bool verified = false;
  bool completeHealthInfo = false;

  static bool gender = false, email = false, referralCode = false;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _invitationCodeController = TextEditingController();

  final TextEditingController _phoneNumberController = TextEditingController();

  final FocusNode _emailFnode = FocusNode();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  static bool selfMedication = false, manageMedication = false, provincial = false, delivery = false;
  late SignUpAboutYouModel model;
  bool initialize = false;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done);
    PPApplication.prescriptionID = -1;
    TELEHEALTH_CONDITION = "";
    TELEHEALTH_COMMENT = "";
    model = SignUpAboutYouModel();
    province = dataStore.getPharmacyProvinceEdd();
    postalCode = dataStore.getPharmacyZipEdd();
    verified = dataStore.readBoolean(DataStoreService.VERIFIED)!;
    _emailController.addListener(emailListener);
    _invitationCodeController.addListener(referralListener);

    if (!["", null].contains(dataStore.readString(DataStoreService.EMAIL)) && _emailController.text.isEmpty) {
      _emailController.text = dataStore.readString(DataStoreService.EMAIL)!;
    }
    if (!["", null].contains(dataStore.readString(DataStoreService.PHONE)) && _phoneNumberController.text.isEmpty) {
      _phoneNumberController.text = dataStore.readString(DataStoreService.PHONE)!;
    }
    String? referralCode = dataStore.readString(DataStoreService.REFERRAL_CODE);
    String? invitationCode = dataStore.readString(DataStoreService.INVITATION_CODE);
    (referralCode != null || invitationCode != null) ? showReferral = true : showReferral = false;
    _invitationCodeController.text = referralCode != null
        ? referralCode
        : invitationCode != null
            ? invitationCode
            : "";
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      showSuccessToast();
    });
    if (widget.from == "telehealth") {
      var province = dataStore.readString(DataStoreService.TELEHEALTH_PROVINCE) ?? "";
      dropDownValue = province.toLowerCase().replaceAll(" ", "_");
    } else {
      String val = getProvinceStorage();
      print(val);
      if (checkValue(val)) {
        print(val);
        if (dropDownValue == null || dropDownValue!.isEmpty) {
          dropDownValue = val.isEmpty ? null : val.toLowerCase().replaceAll(" ", "_");
          //}
        } else {
          List xyz = getStateMap().first.values.toList();
          dropDownValue = xyz[0];
        }
      }
    }
    if (GlobalVariable().gender.isNotEmpty) {
      selectedGender = GlobalVariable().gender;
    }
    if (GlobalVariable().selectedProvince.isNotEmpty) {
      dropDownValue = GlobalVariable().selectedProvince;
    }
    if (GlobalVariable().emailId.isNotEmpty) {
      _emailController.text = GlobalVariable().emailId;
    }
    if (GlobalVariable().selectedEddDay.isNotEmpty) {
      eddDropDownValue = GlobalVariable().selectedEddDay;
    }

    if (GlobalVariable().phone.isNotEmpty) {
      _phoneNumberController.text = GlobalVariable().phone;
    }
  }

  bool checkValue(String val) {
    if (val.isEmpty) {
      return false;
    }
    for (var state in getStateMap()) {
      if (state['key'] == val) {
        return true;
      }
    }
    return false;
  }

  void showSuccessToast() {
    if (widget.dialogMsg != null && widget.from == 'telehealth') {
      _showSuccessToast(context: context, message: widget.dialogMsg!);
    }
  }

  void _showSuccessToast({required BuildContext context, required String message}) {
    OverlayEntry overlayEntry;
    overlayEntry = OverlayEntry(builder: (context) => ToastWidget(msg: message));
    Overlay.of(context)!.insert(overlayEntry);
    Timer(Duration(seconds: 1), () => overlayEntry.remove());
  }

  @override
  void didChangeDependencies() async {
    _phoneNumberController.addListener(phoneNumberListener);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _emailController.removeListener(emailListener);
    _invitationCodeController.removeListener(referralListener);
    super.dispose();
  }

  bool isFormValid() {
    bool ge = genderRadioGroup!.validate();
    return ge;
  }

  emailListener() {
    if (email == false && _emailController.text.isNotEmpty) {
      email = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_email_entered);
    }
  }

  referralListener() {
    if (referralCode == false && _invitationCodeController.text.isNotEmpty) {
      referralCode = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_referralcode_entered);
    }
  }

  phoneNumberListener() async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevMobileNumber);
      if (checkPhone!.redirect == PhoneNumberStateConstant.PHONE_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.PHONE_OTP_FORGOT) {
        setState(() {
          phoneAlreadyRegister = true;
          phState = checkPhone.redirect!;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phoneAlreadyRegister = false;
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;

    if (_phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
    }
  }

  checkEmailAddress(BuildContext context) async {
    if (StringUtils.isEmail(_emailController.text)) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_email_entered);
      prevEmailAddress = _emailController.text;
      PhoneVerification? checkPhone = await Provider.of<SignUpModel>(context, listen: false).verifyIdentifier(prevEmailAddress);
      if (checkPhone!.redirect == PhoneNumberStateConstant.EMAIL_LOGIN || checkPhone.redirect == PhoneNumberStateConstant.EMAIL_OTP_FORGOT) {
        setState(() {
          emailAlreadyRegister = true;
          phState = checkPhone.redirect!;
        });
      }
    } else if (!StringUtils.isEmail(_emailController.text))
      setState(() {
        emailAlreadyRegister = false;
        phState = "";
      });
    prevEmailAddress = _emailController.text;
  }

  Widget getGenderUI() {
    return genderRadioGroup!;
  }

  Widget getTakeMedicationUI() {
    return selfMedicationRadioGroup!;
  }

  Widget getManageMedicationUI() {
    return manageMedicationRadioGroup!;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return WillPopScope(
                child: getProvider(),
                onWillPop: () async {
                  validateBackCheck();
                  return true;
                });
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getProvider() {
    if (!initialize) {
      initializeContent();
      initialize = true;
    }

    genderRadioGroup!.initialValue =
        GlobalVariable().gender.isNotEmpty ? GlobalVariable().gender : getGenderValue(dataStore.readString(DataStoreService.GENDER) ?? "");
    return ChangeNotifierProvider(
      create: (_) => SignUpAboutYouModel(),
      child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget? child) {
        return getMainView(model);
      }),
    );
  }

  Future myFutureMethodOverall(SignUpAboutYouModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "forgot", "common", "modal", "modals", "copay", "login"]);
    Future<bool?> future2 = model.getUserInfo();
    Future<bool?>? future3;
    if (widget.from == "prescription") {
      if (postalCode == null || postalCode.toString().isEmpty) postalCode = dataStore.getUserPostalCode();
      if (province == null || province.toString().isEmpty) province = dataStore.getGeoIpProvince();
      future3 = model.getEDDList(postalCode ?? "", "");
      return await Future.wait([future1, future2, future3]);
    } else if (widget.from == "transfer") {
      if (postalCode == null || postalCode.toString().isEmpty) postalCode = dataStore.getUserPostalCode();
      if (province == null || province.toString().isEmpty) province = dataStore.getGeoIpProvince();
      future3 = model.getEDDList(postalCode ?? "", province ?? "");
      return await Future.wait([future1, future2, future3]);
    } else {
      return await Future.wait([future1, future2]);
    }
  }

  _onBackPressed(BuildContext context) {
    ExitDialog.show(context);
  }

  bool isProvinceQuebec() {
    return PPApplication.quebecCheck ? true : false;
  }

  Widget getMainView(SignUpAboutYouModel model) {
    return BaseScaffold(
      key: Key("AlmostDoneView"),
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? AppBar(
              backgroundColor: Colors.white,
              title: isProvinceQuebec() == true
                  ? Image.network(
                      "https://static.pocketpills.com/webapp/images/logo/logo-full-quebec.png",
                      width: MediaQuery.of(context).size.width * 0.45,
                      fit: BoxFit.cover,
                    )
                  : Image.asset('graphics/logo-horizontal-dark.png', width: MediaQuery.of(context).size.width * 0.45),
              centerTitle: true,
              leading: (widget.from == "transfer" || widget.from == "prescription")
                  ? IconButton(
                      key: Key("back"),
                      icon: Icon(Icons.arrow_back_rounded),
                      color: brandColor,
                      onPressed: () {
                        validateBackCheck();
                      },
                    )
                  : (widget.from == "add others")
                      ? IconButton(
                          key: Key("back"),
                          icon: Icon(Icons.arrow_back_rounded),
                          color: brandColor,
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(context, CustomisedSignUpWidget.routeName, (Route<dynamic> route) => false,
                                arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
                            ;
                          })
                      : Container(),
            )
          : null,
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                children: <Widget>[
                  Expanded(child: SingleChildScrollView(child: getStartedView(context, model))),
                  Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                            child: model.state == ViewState.Busy
                                ? ViewConstants.progressIndicator
                                : widget.from != null && widget.from == "transfer"
                                    ? Row(
                                        children: <Widget>[
                                          /*Expanded(
                                            child: Container(
                                              height: 45,
                                              child: OutlinedButton(
                                                key: Key("done back"),
                                                child: Text(
                                                  LocalizationUtils.getSingleValueString("signup", "signup.almostdone.back").toUpperCase(),
                                                  style: TextStyle(color: darkBlue, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                                ),
                                                style: OutlinedButton.styleFrom(
                                                  primary: Colors.white,
                                                  backgroundColor: whiteColor,
                                                  side: BorderSide(color: darkBlue, width: 2),
                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                                ),
                                                onPressed: () {
                                                  CheckBackProcessTransfer(context);
                                                },
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),*/
                                          Expanded(
                                            child: Container(
                                              height: 45,
                                              child: OutlinedButton(
                                                key: Key("done next"),
                                                child: Text(
                                                  LocalizationUtils.getSingleValueString("signup", "signup.almostdone.next").toUpperCase(),
                                                  style: TextStyle(color: Colors.white, fontFamily: "FSJoeyPro Bold", fontSize: 14.4),
                                                ),
                                                style: OutlinedButton.styleFrom(
                                                  primary: darkBlue,
                                                  backgroundColor: darkBlue,
                                                  side: BorderSide(color: darkBlue, width: 2),
                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(LARGE_XX))),
                                                ),
                                                onPressed: () {
                                                  onNextClick(context, model);
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    : PrimaryButton(
                                        key: Key("buttonNext"),
                                        text: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.next"),
                                        onPressed: () {
                                          onNextClick(context, model);
                                        },
                                        fullWidth: true),
                          ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getStartedView(BuildContext context, SignUpAboutYouModel model) {
    return getBaseDefaultView(context, model);
  }

  Widget getBaseDefaultView(BuildContext context, SignUpAboutYouModel model) {
    return Form(
      key: _aboutUsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX),
                  child: Column(
                    children: <Widget>[
                      widget.source == BaseStepperSource.NEW_USER ? pharmacistFlow() : normalFlow(),
                      SizedBox(height: MEDIUM),
                      (widget.from == "prescription" || widget.from == "transfer") && (postalCode != null && postalCode!.isNotEmpty) ? eddCard() : Container(),
                    ],
                  )),
              Padding(padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX), child: provinceCard()),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX),
                child: getGenderUI(),
              ),
              Padding(
                  padding: EdgeInsets.only(
                    left: MEDIUM_XXX,
                    right: MEDIUM_XXX,
                  ),
                  child: Column(children: <Widget>[
                    SizedBox(height: MEDIUM),
                    getDisplayField(),
                    Padding(padding: EdgeInsets.only(top: SMALL), child: SizedBox(height: SMALL)),
                    PPTexts.getFormLabel(
                        dataStore.readString(DataStoreService.PHONE) == null || dataStore.readString(DataStoreService.PHONE)!.isEmpty
                            ? LocalizationUtils.getSingleValueString("signup", "signup.fields.phone-help")
                            : LocalizationUtils.getSingleValueString("signup", "signup.fields.email-hint"),
                        isBold: false),
                    SizedBox(height: MEDIUM_X),
                  ])),
              getManitobaCheck(),
              SizedBox(height: MEDIUM_XXX),
            ],
          ),
        ],
      ),
    );
  }

  void onClickPharmacySearchScreen() {
    Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
        arguments: TransferArguments(source: widget.source, PharmacyNamePopular: dataStore.getPharmacyName()!));
  }

  Widget provinceCard() {
    return dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.provience-label"),
                  fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
              PPUIHelper.verticalSpaceSmall(),
              //getDropDown(),
              getCustom(),
              dropDownError == null
                  ? Text(
                      LocalizationUtils.getSingleValueString("signup", "signup.fields.province-helptext").isEmpty
                          ? "As mentioned on your provincial health card"
                          : LocalizationUtils.getSingleValueString("signup", "signup.fields.province-helptext"),
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro Medium", color: thinGrey))
                  : PPTexts.getFormError(dropDownError!, color: errorColor),
            ],
          )
        : Container();
  }

  Widget eddCard() {
    return widget.from == "transfer" || widget.from == "prescription"
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.edd-label"),
                  fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
              PPUIHelper.verticalSpaceSmall(),
              //getEDDDropDown(),
              getEDDDropDownNew(),
              eddDropDownError == null ? SizedBox(height: MEDIUM_XXX) : PPTexts.getFormError(eddDropDownError!, color: errorColor),
            ],
          )
        : Container();
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpacexSmall),
          child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description-transfer-exp"),
            style: TextStyle(color: darkPrimaryColor, fontSize: 16.5, height: 1.5, fontFamily: "FSJoeyPro"),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  Widget normalFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.title")),
        SizedBox(height: SMALL_X),
        PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description-telehealth"), isBold: false),
        SizedBox(height: REGULAR_XXX),
      ],
    );
  }

  Widget getReferralCodeView() {
    if (showReferral == true) {
      return AnimatedContainer(
        duration: Duration(seconds: 2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PPDivider(),
            PPUIHelper.verticalSpaceXSmall(),
            Padding(
                padding: EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0),
                child: PPFormFields.getTextField(
                    controller: _invitationCodeController,
                    decoration: PPInputDecor.getDecoration(
                      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-label"),
                      hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-referral-code"),
                    ),
                    validator: (value) {
                      return null;
                    })),
            SizedBox(
              height: 3,
            ),
            Padding(
                padding: EdgeInsets.only(left: 12.0, right: 12.0),
                child: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-hint"),
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro Medium"),
                )),
            SizedBox(
              height: 15,
            )
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  onNextClick(context, SignUpAboutYouModel signUpAboutYouModel) async {
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    onClickNextDefaultBaseView(context, signUpAboutYouModel);
  }

  onClickNextDefaultBaseView(context, SignUpAboutYouModel signUpAboutYouModel) async {
    setState(() {
      autovalidate = true;
    });
    bool dropDownSuccess = true, genderSuccess = true;
    if (!_aboutUsformKey.currentState!.validate()) {
      isFormValid();
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.employer.fill-details"));
    }
    if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false) {
      if (dropDownValue == null || (dropDownValue != null && dropDownValue!.isEmpty)) {
        setState(() {
          dropDownSuccess = false;
          dropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.select-province-error");
        });
      }
      if ((dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED) == false && eddDropDownValue == null) && eddCardVisible ||
          ((eddDropDownValue != null && eddDropDownValue!.isEmpty) && dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED) == false && eddCardVisible)) {
        setState(() {
          dropDownSuccess = false;
          eddDropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.error-required");
        });
      }
    }

    if (!genderRadioGroup!.validate()) {
      genderSuccess = false;
    }

    if (visibleField == "phone") {
      if (_phoneNumberController.text.isEmpty) {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.error-phone-required"));
      } else if (_phoneNumberController.text.length == 10) {
        //print('AMANPRO:$dropDownValue');
        //dropDownValue!.toLowerCase() == "quebec" ? PPApplication.quebecCheck = true : PPApplication.quebecCheck = false;
        if (genderSuccess && dropDownSuccess) {
          callFutureActions(context, signUpAboutYouModel);
        }
      } else if (_phoneNumberController.text.length < 10) {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error1"));
      } else {
        onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error2"));
      }
    } else {
      if (StringUtils.isEmail(_emailController.text)) {
        if (genderSuccess && dropDownSuccess) {
          callFutureActions(context, signUpAboutYouModel);
        }
      } else {
        showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email"));
      }
    }
  }

  callFutureActions(context, SignUpAboutYouModel signUpAboutYouModel) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_almost_done_submit);
    var success = dataStore.readString(DataStoreService.SIGNUP_TYPE) == "EMAIL_BASED"
        ? await signUpAboutYouModel.updateAboutYou(
            province: dropDownValue,
            selfMedication:
                selfMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            isCaregiver:
                manageMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
            gender: getGenderEnglish(genderRadioGroup!.getValue()),
            phone: _phoneNumberController.text,
            referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text,
            latestPrescriptionDeliveryBy: eddDropDownValue)
        : await signUpAboutYouModel.updateAboutYou(
            province: dropDownValue,
            selfMedication:
                selfMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            isCaregiver:
                manageMedicationRadioGroup!.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
            pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
            gender: getGenderEnglish(genderRadioGroup!.getValue()),
            email: _emailController.text,
            referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text,
            latestPrescriptionDeliveryBy: eddDropDownValue);
    if (success) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_ALMOST_DONE, true);
      if (widget.from != null && widget.from == "add others") {
        dataStore.writeBoolean(DataStoreService.SIGNUP_ADD_OTHERS, true);
      } else {
        dataStore.writeBoolean(DataStoreService.SIGNUP_ADD_OTHERS, false);
      }
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification);
      if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) == false) {
        if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, chambersFlow: false));
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.signup);
        analyticsEvents.logCompleteRegistrationEvent();
        analyticsEvents.mixPanelIdentifier();
        bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED)!;
        if (transferSkipped == true) {
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
        } else {
          if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) != true) {
            dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, true);
          }
          Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
          Provider.of<HomeModel>(context, listen: false).clearData();
          Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          //Navigator.of(context).pushNamedAndRemoveUntil(SignUpSuccessWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        }
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification_failed);
      onFail(context, errMessage: signUpAboutYouModel.errorMessage);
    }
  }

  Widget getManitobaCheck() {
    if (dropDownValue == "manitoba") {
      return InkWell(
        onTap: () {
          setState(() {
            manitobaCheck = !manitobaCheck;
          });
        },
        child: Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Checkbox(
                    value: manitobaCheck,
                    activeColor: brandColor,
                    onChanged: (bool? value) {
                      setState(() {
                        manitobaCheck = !manitobaCheck;
                      });
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: SMALL_XXX, right: SMALL_XXX),
                  child: Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.fields.packpermission-label"),
                    overflow: TextOverflow.clip,
                    style: TextStyle(height: 1.4, color: secondaryColor, fontSize: 14, fontWeight: FontWeight.w400, fontFamily: "FSJoeyPro"),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: MEDIUM),
        ]),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        items: getStateMap()
            .map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]!, key: Key(state["key"].toString()))))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (provincial == false) {
            provincial = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_province_entered);
          }
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: getStateMap()
            .map((state) => DropdownMenuItem<String>(
                value: state["key"],
                child: Text(
                  state["value"]!,
                  key: Key(state["key"].toString()),
                )))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
  }

  Widget getCustom() {
    return Container(
      width: double.infinity,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
          borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            key: Key("provinceDropDown"),
            hint: Text(
              LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
              style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
            ),
            value: dropDownValue,
            items: getStateMap()
                .map((state) => DropdownMenuItem<String>(
                    value: state["key"],
                    child: Text(
                      state["value"]!,
                      key: Key(state["key"].toString()),
                      style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
                    )))
                .toList(),
            onChanged: (selectedItem) => setState(() {
              FocusScope.of(context).requestFocus(_emailFnode);
              if (provincial == false && dropDownError == null) {
                provincial = true;
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_province_entered);
              }
              dropDownValue = selectedItem.toString();
              setState(() {
                dropDownError = null;
              });
            }),
          ),
        ),
      ),
    );
  }

  Widget getEDDDropDown() {
    eddCardVisible = true;
    if (eddDropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.select"),
        fullWidth: true,
        value: eddDropDownValue,
        items: model.eddList
            .map((state) => DropdownMenuItem<String>(
                value: state.value,
                child: Text(
                  state.label,
                  key: Key(state.value),
                )))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (delivery == false) {
            delivery = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_deliveryBy_entered);
          }
          eddDropDownValue = selectedItem;
          setState(() {
            eddDropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.select"),
        fullWidth: true,
        value: eddDropDownValue,
        isError: true,
        items: model.eddList
            .map((state) => DropdownMenuItem<String>(key: Key(state.value), value: state.value, child: Text(state.label, key: Key(state.value))))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          eddDropDownValue = selectedItem;
          setState(() {
            eddDropDownError = null;
          });
        }),
      );
  }

  Widget getEDDDropDownNew() {
    eddCardVisible = true;
    return Container(
      width: double.infinity,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
          borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            key: Key("eddDropDown"),
            hint: Text(LocalizationUtils.getSingleValueString("signup", "signup.transfer.select"), style: TextStyle(fontFamily: "FSJoeyPro Heavy")),
            value: eddDropDownValue,
            items: model.eddList
                .map((state) => DropdownMenuItem<String>(
                    value: state.value,
                    child: Text(
                      state.label,
                      style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
                      key: Key(state.value),
                    )))
                .toList(),
            onChanged: (selectedItem) => setState(() {
              FocusScope.of(context).requestFocus(_emailFnode);
              if (delivery == false && eddDropDownError == null) {
                delivery = true;
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_deliveryBy_entered);
              }
              eddDropDownValue = selectedItem.toString();
              setState(() {
                eddDropDownError = null;
              });
            }),
          ),
        ),
      ),
    );
  }

  TextFormField getPhoneField() {
    return PPFormFields.getNumericFormField(
      key: Key("addPhone"),
      maxLength: 10,
      keyboardType: TextInputType.phone,
      minLength: 10,
      textInputAction: TextInputAction.next,
      controller: _phoneNumberController,
      focusNode: _phoneNumberFocusNode,
      onFieldSubmitted: (value) {
        onNextClick(context, model);
      },
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
      decoration: PPInputDecor.getDecoration(
        labelText: "",
        hintText: "",
        helperText: '',
        prefixIcon: Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              '+1 |',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "FSJoeyPro Medium",
              ),
            )),
      ),
    );
  }

  TextFormField getEmailField() {
    return PPFormFields.getTextField(
        key: Key("addEmail"),
        autovalidate: autovalidate,
        keyboardType: TextInputType.emailAddress,
        focusNode: _emailFnode,
        controller: _emailController,
        textInputAction: TextInputAction.done,
        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        decoration: PPInputDecor.getDecoration(labelText: "", hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
        onFieldSubmitted: (value) {
          onNextClick(context, model);
        });
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void initializeContent() {
    selfMedicationRadioGroup = PPRadioGroup(
      key: Key("selfMedicationRadioGroup"),
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no").toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.medications-daily"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        if (selfMedication == false) {
          selfMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_dailymed_entered);
        }
      },
    );

    manageMedicationRadioGroup = PPRadioGroup(
      key: Key("lovedOneRadioGroup"),
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no").toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.loved-one-medications"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
      onChange: (String value) {
        if (manageMedication == false) {
          manageMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_iscaregiver_entered);
        }
      },
    );

    genderRadioGroup = PPRadioGroup(
      key: Key("genderRadioGroup"),
      type: "gender",
      isAsterisk: true,
      radioOptions: [
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-label"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
      onChange: (String value) {
        selectedGender = value;
        if (gender == false) {
          gender = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_gender_entered);
        }
      },
    );
  }

  String getProvinceStorage() {
    if (dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD) != null && dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD)!.isNotEmpty) {
      return dataStore.readString(DataStoreService.PHARMACY_PROVINCE_EDD)!.toLowerCase();
    }
    if (dataStore.readString(DataStoreService.PROVINCE_TRANSFER) != null && dataStore.readString(DataStoreService.PROVINCE_TRANSFER)!.isNotEmpty) {
      return dataStore.readString(DataStoreService.PROVINCE_TRANSFER)!.toLowerCase();
    }
    if (dataStore.readString(DataStoreService.PROVINCE) != null) {
      return dataStore.readString(DataStoreService.PROVINCE)!.toLowerCase();
    } else {
      return '';
    }
  }

  Widget getDisplayField() {
    if (dataStore.readString(DataStoreService.PHONE) == null || dataStore.readString(DataStoreService.PHONE)!.isEmpty) {
      visibleField = "phone";
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.phone"),
              fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
          SizedBox(
            height: 5,
          ),
          getPhoneField(),
        ],
      );
    } else {
      visibleField = "email";
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
              fontWeight: FontWeight.bold, color: darkPrimaryColor, fontSize: 14.5, fontFamily: "FSJoeyPro Bold"),
          PPUIHelper.verticalSpaceSmall(),
          getEmailField(),
        ],
      );
    }
  }

  void CheckBackProcessTransfer(BuildContext context) {
    validateBackCheck();
  }

  CallBackButton(BuildContext context) {
    TransferPrescription transferPrescription =
        TransferPrescription.fromJson(jsonDecode(dataStore.readString(DataStoreService.PRESCRIPTION_FOR_TRANSFER).toString()));
    TransferSearchSelectModel searchSelectModel = TransferSearchSelectModel();
    searchSelectModel.prescriptionState = null;
    searchSelectModel.pharmacyName = transferPrescription.pharmacyName ?? "";
    searchSelectModel.pharmacyAddress = transferPrescription.pharmacyAddress ?? "";
    searchSelectModel.pharmacyPhone = transferPrescription.pharmacyPhone;
    searchSelectModel.selectedPlaceId = transferPrescription.pharmacyGooglePlaceId ?? "";
    searchSelectModel.transferAll = true;
    searchSelectModel.comments = "";
    searchSelectModel.isSignup = true;
    searchSelectModel.isPrescriptionEdited = true;
    searchSelectModel.id = transferPrescription.id;
    searchSelectModel.omsPrescriptionId = transferPrescription.omsPrescriptionId;

    Navigator.pushReplacementNamed(context, TransferWidget.routeName,
        arguments: TransferArguments(source: BaseStepperSource.NEW_USER, selectedModel: searchSelectModel));
  }

  void validateBackCheck() async {
    String name = signUpResponse!.signupDto!.firstName ?? "";
    String dob = signUpResponse!.signupDto!.birthDate ?? "";
    String prescription = signUpResponse!.prescription!.prescriptionState ?? "";
    String transferDetails = dataStore.readString(DataStoreService.PRESCRIPTION_FOR_TRANSFER) ?? "";
    String prescriptionDetails = dataStore.readString(DataStoreService.PRESCRIPTION_FOR_UPLOAD) ?? "";
    GlobalVariable().emailId = _emailController.text.toString();
    GlobalVariable().gender = selectedGender ?? "";
    GlobalVariable().selectedProvince = dropDownValue.toString();
    GlobalVariable().selectedEddDay = eddDropDownValue.toString();
    GlobalVariable().phone = _phoneNumberController.text.toString();
    if (name.isNotEmpty && dob.isNotEmpty && prescription.isNotEmpty && prescription == "FILED" && widget.from == "transfer" && transferDetails.isNotEmpty) {
      Navigator.pushNamedAndRemoveUntil(context, TransferDetailsSignup.routeName, (Route<dynamic> route) => false,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "transfer"));
    } else if (name.isNotEmpty &&
        dob.isNotEmpty &&
        prescription.isNotEmpty &&
        prescription == "FILED" &&
        widget.from == "prescription" &&
        prescriptionDetails.isNotEmpty) {
      Navigator.pushNamedAndRemoveUntil(context, PrescriptionDetailsSignup.routeName, (Route<dynamic> route) => false,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER, from: "prescription", prescriptionPages: widget.prescriptionList ?? []));
    } else if (widget.from == "add others")
      Navigator.pushNamedAndRemoveUntil(context, CustomisedSignUpWidget.routeName, (Route<dynamic> route) => false,
          arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    else {
      _onBackPressed(context);
    }
  }
}

getGenderEnglish(String? value) {
  if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase()) {
    return "MALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase()) {
    return "FEMALE";
  } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()) {
    return "OTHER";
  }
}

String getAddressNoCountry(String str) {
  if (str != null && str.length >= 8) {
    str = str.substring(0, str.length - 8);
  }
  return str;
}

String getGenderValue(String gender) {
  String? genderValue;
  switch (gender) {
    case "MALE":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase();
      break;
    case "FEMALE":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase();
      break;
    case "OTHER":
      genderValue = LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase();
      break;

    default:
      genderValue = "";
  }
  return genderValue;
}
