import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/add_patient_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'about_patient.dart';
import 'add_member_arguments.dart';
import 'cellphone_view.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class AddMemberSignupWidget extends StatefulWidget {
  static const routeName = 'am_signup';

  @override
  State<StatefulWidget> createState() {
    return AddMemberSignupState();
  }
}

class AddMemberSignupState extends BaseState<AddMemberSignupWidget> {
  late Patient newPatient;

  PPRadioGroup<String>? relationRadioGroup;
  late PPRadioGroup<String> genderRadioGroup;

  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _monthController = TextEditingController();
  final TextEditingController _yearController = TextEditingController();

  final FocusNode _firstNameFnode = FocusNode();
  final FocusNode _lastNameFnode = FocusNode();
  final FocusNode _monthFnode = FocusNode();
  final FocusNode _dateFnode = FocusNode();
  final FocusNode _yearFnode = FocusNode();

  final _formKey = GlobalKey<FormState>();

  String? _mmValue;
  String? _ddValue;
  String? _yyyyValue;
  bool autoValidate = false;
  bool showDateError = false;
  bool initialized = false;
  late AddPatientModel model;
  String? gender;

  @override
  void initState() {
    super.initState();
    model = AddPatientModel();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_add);
    _monthController.addListener(monthControllerListener);
    _dateController.addListener(dateControllerListener);
    _yearController.addListener(yearControllerListener);
  }

  @override
  void dispose() {
    _monthController.removeListener(monthControllerListener);
    _dateController.removeListener(dateControllerListener);
    _yearController.removeListener(yearControllerListener);
    super.dispose();
  }

  void monthControllerListener() {
    if (_monthController.text.length == 2 && _mmValue != _monthController.text) {
      _mmValue = _monthController.text;
      String? validationError = validateMonth(_mmValue!, showErr: false);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_dateFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _mmValue = _monthController.text;
  }

  void dateControllerListener() {
    if (_dateController.text.length == 2 && _ddValue != _dateController.text) {
      _ddValue = _dateController.text;
      String? validationError = validateDate(_ddValue, showErr: false);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_yearFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _ddValue = _dateController.text;
  }

  void yearControllerListener() {
    if (_yearController.text.length == 4 && _yyyyValue != _yearController.text) {
      _yyyyValue = _yearController.text;
      String? validationError = validateYear(_yyyyValue, showErr: false);
      if (validationError != null) {
        setDateErrorState(true);
      } else
      //  FocusScope.of(context).requestFocus(_usernameFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    _yyyyValue = _yearController.text;
  }

  bool evaluateDate() {
    if (validateMonth(_mmValue, showErr: true) == null && validateDate(_ddValue, showErr: true) == null && validateYear(_yyyyValue, showErr: true) == null)
      return true;
    else
      return false;
  }

  isDateValid() {
    if (_dateController.text.isNotEmpty && _monthController.text.isNotEmpty && _yearController.text.isNotEmpty) {
      final birthday = DateTime(int.parse(_yearController.text), int.parse(_monthController.text), int.parse(_dateController.text));
      final date2 = DateTime.now();
      final difference = date2.difference(birthday).inDays;
      if (difference > 1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getProvider();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getProvider() {
    if (!initialized) {
      initializeContent();
      initialized = true;
    }

    return ChangeNotifierProvider(
      create: (_) => AddPatientModel(),
      child: Consumer<AddPatientModel>(builder: (BuildContext context, AddPatientModel model, Widget? child) {
        return getMainView(model);
      }),
    );
  }

  Future myFutureMethodOverall(AddPatientModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["common", "addmember", "signup"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(AddPatientModel model) {
    return BaseScaffold(
      appBar: InnerAppBar(
          titleText: LocalizationUtils.getSingleValueString("common", "common.label.add-a-member"),
          appBar: AppBar(),
          backButtonIcon: Icon(
            Icons.home,
            color: whiteColor,
          ),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          }),
      body: Container(
        child: Builder(
          builder: (BuildContext context) {
            return GestureDetector(
                onTap: () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                },
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                PPUIHelper.verticalSpaceLarge(),
                                // PPTexts.getHeading(heading),
                                // PPTexts.getDescription(LocalizationUtils.getSingleValueString("addmember", "addmember.personal.description")),
                                Padding(
                                    padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                                    child: Column(
                                      children: <Widget>[pharmacistFlow()],
                                    )),
                                PPUIHelper.verticalSpaceLarge(),
                                getNameField(),
                                PPUIHelper.verticalSpaceMedium(),
                                getDOB(model),
                                PPUIHelper.verticalSpaceMedium(),
                                getGenderUI(),
                                PPUIHelper.verticalSpaceMedium(),
                                getRelationshipView(),
                                PPUIHelper.verticalSpaceMedium(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                        child: model.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : Row(
                                children: <Widget>[
                                  SecondaryButton(
                                    text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
                                    onPressed: () => Navigator.pop(context),
                                  ),
                                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                  PrimaryButton(
                                    key: Key("addMemberSubmit"),
                                    text: LocalizationUtils.getSingleValueString("addmember", "addmember.personal.continue"),
                                    onPressed: () {
                                      onAddMemberPressed(context, model);
                                    },
                                  )
                                ],
                              ),
                      ),
                    ),
                  ],
                ));
          },
        ),
      ),
    );
  }

  Widget getGenderUI() {
    return genderRadioGroup;
  }

  Widget getDOB(AddPatientModel model) {
    final double gap = 15.0;
    return Column(mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.dob"),fontSize: 15,fontFamily: "FSJoeyPro Bold",color: darkPrimaryColor),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: PPFormFields.getNumericFormField(
                  key: Key("addMemberMonth"),
                  keyboardType: TextInputType.phone,
                  maxLength: 2,
                  textInputAction: TextInputAction.next,
                  focusNode: _monthFnode,
                  autovalidate: autoValidate,
                  validator: (value) => validateMonth(value!, showErr: false),
                  decoration: PPInputDecor.getDecoration(
                    collapsed: false,
                    labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-month"),
                    hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-month"),
                    helperText: ' ',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                  ),/*.copyWith(
                      contentPadding: EdgeInsets.only(left: 8, right: 3, top: 0, bottom: 0),
                      errorStyle: TextStyle(height: 1, fontFamily: "FSJoeyPro"),
                      errorMaxLines: 4),*/
                  controller: _monthController,
                  onFieldSubmitted: (value) {
                    _fieldFocusChange(context, _monthFnode, _dateFnode);
                  }),
            ),
            SizedBox(width: gap),
            Expanded(
              flex: 1,
              child: PPFormFields.getNumericFormField(
                  key: Key("addMemberDate"),
                  keyboardType: TextInputType.phone,
                  maxLength: 2,
                  textInputAction: TextInputAction.next,
                  autovalidate: autoValidate,
                  validator: (value) => validateDate(value!, showErr: false),
                  decoration: PPInputDecor.getDecoration(
                    collapsed: false,
                    labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-date"),
                    hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-date"),
                    helperText: ' ',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                  ),/*.copyWith(
                      contentPadding: EdgeInsets.only(left: 8, right: 3, top: 0, bottom: 0),
                      errorStyle: TextStyle(height: 1, fontFamily: "FSJoeyPro"),
                      errorMaxLines: 4),*/
                  controller: _dateController,
                  focusNode: _dateFnode,
                  onFieldSubmitted: (value) {
                    _fieldFocusChange(context, _dateFnode, _yearFnode);
                  }),
            ),
            SizedBox(width: gap),
            Expanded(
              flex: 2,
              child: PPFormFields.getNumericFormField(
                  key: Key("addMemberYear"),
                  keyboardType: TextInputType.phone,
                  maxLength: 4,
                  textInputAction: TextInputAction.done,
                  autovalidate: autoValidate,
                  validator: (value) => validateYear(value!, showErr: false),
                  decoration: PPInputDecor.getDecoration(
                    collapsed: false,
                    labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-year"),
                    hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-year"),
                    helperText: '',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                  ),/*.copyWith(contentPadding: EdgeInsets.only(left: 8, right: 90), errorStyle: TextStyle(height: 1, fontFamily: "FSJoeyPro"), errorMaxLines: 4),*/
                  controller: _yearController,
                  focusNode: _yearFnode,
                  onFieldSubmitted: (value) {
                    onAddMemberPressed(context, model);
                  }),
            ),
          ],
        ),
        getErrorText(),
      ],
    );
  }

  Widget getErrorText() {
    if ((_monthController.text.isEmpty) && (_dateController.text.isEmpty && _yearController.text.isEmpty) && autoValidate) {
      return Align(
        alignment: Alignment.topLeft,
        child: Text(
          LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
          style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
        ),
      );
        LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
    }else if(showDateError){
      return Align(
        alignment: Alignment.topLeft,
        child: Text(
            LocalizationUtils.getSingleValueString("signup", "signup.fields.dob-error"),
          style: TextStyle(fontSize: 12, color: errorColor, fontFamily: "FSJoeyPro"),
        ),
      );
    } else {
      return Container();
        ;
    }
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("addmember", "addmember.personal.description"),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  setDateErrorState(bool showError) {
    setState(() {
      showDateError = showError;
    });
  }

  Widget getNameField() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.firstname"),fontSize: 15,fontFamily: "FSJoeyPro Bold",color: darkPrimaryColor),
              SizedBox(
                height: 2,
              ),
              PPFormFields.getTextField(
                  key: Key("addMemberFName"),
                  textCapitalization: TextCapitalization.words,
                  controller: _firstnameController,
                  autovalidate: autoValidate,
                  textInputAction: TextInputAction.next,
                  focusNode: _firstNameFnode,
                  onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                  decoration: PPInputDecor.getDecoration(
                    labelText: "",
                    hintText: "",
                  ),
                  onFieldSubmitted: (value) {
                    _fieldFocusChange(context, _firstNameFnode, _lastNameFnode);
                  }),
            ],
          ),
        ),
        SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
        Expanded(
          child: Column(mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("signup", "signup.fields.lastname"),fontSize: 15,fontFamily: "FSJoeyPro Bold",color: darkPrimaryColor),
              SizedBox(
                height: 2,
              ),
              PPFormFields.getTextField(
                  key: Key("addMemberLName"),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  controller: _lastnameController,
                  focusNode: _lastNameFnode,
                  autovalidate: autoValidate,
                  onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                  decoration: PPInputDecor.getDecoration(
                      labelText: "",
                      hintText: ""),
                  onFieldSubmitted: (value) {
                    _fieldFocusChange(context, _lastNameFnode, _monthFnode);
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget getRelationshipView() {
    return relationRadioGroup!;
  }

  String? validateMonth(String? value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.field-error") : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    } else if (n < 1 || n > 12) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    }
    return null;
  }

  String? validateDate(String? value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.field-error") : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    } else if (n < 1 || n > 31) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    }
    return null;
  }

  String? validateYear(String? value, {bool? showErr}) {
    if (value == null || value.isEmpty) return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.field-error") : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    } else if (n < 1900 || n > 2030) {
      return showErr! ? LocalizationUtils.getSingleValueString("addmember", "addmember.personal.error-info") : "";
    }
    return null;
  }

  void onAddMemberPressed(context, AddPatientModel model) async {
    if ((_monthController.text.isEmpty) || (_dateController.text.isEmpty || _yearController.text.isEmpty)) {
      setDateErrorState(true);
    }
    if(!isDateValid()){
      setDateErrorState(true);
    }

    setState(() {
      autoValidate = true;
      relationRadioGroup!.validate();
      genderRadioGroup.validate();
    });

    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (_formKey.currentState!.validate() && relationRadioGroup!.validate() && genderRadioGroup.validate() && isDateValid()) {
      gender = getGenderEnglish(genderRadioGroup.getValue());

      createPatient();

      if (PatientUtils.isPatientMinor(newPatient)) {
        var success = await model.addPatient(patient: newPatient, relation: relationRadioGroup!.getValue());
        if (success != null) {
          Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
          Navigator.pushReplacementNamed(context, AboutPatientWidget.routeName, arguments: TransferArguments(userPatient: success));
        } else
          onFail(context, errMessage: model.errorMessage);
      } else {
        AddMemberRequest request = AddMemberRequest();
        request.relation = getRealationEnglish(relationRadioGroup!.getValue());
        request.patient = newPatient;
        Navigator.pushReplacementNamed(context, CellPhoneWidget.routeName, arguments: AddMemberArguments(addMemberRequest: request, gender: gender));
      }
    }
  }

  createPatient() {
    newPatient = Patient();
    newPatient.firstName = _firstnameController.text;
    newPatient.lastName = _lastnameController.text;
    newPatient.birthDate = _monthController.text + "/" + _dateController.text + "/" + _yearController.text;
    newPatient.gender = getGenderEnglish(genderRadioGroup.getValue());
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  getRealationEnglish(String? value) {
    if (value == LocalizationUtils.getSingleValueString("common", "common.all.relation-spouse").toUpperCase()) {
      return "SPOUSE";
    } else if (value == LocalizationUtils.getSingleValueString("common", "common.all.relation-child").toUpperCase()) {
      return "CHILD";
    } else if (value == LocalizationUtils.getSingleValueString("common", "common.all.relation-other").toUpperCase()) {
      return "OTHER";
    }
  }

  void initializeContent() {
    relationRadioGroup = PPRadioGroup(
      key: Key("RelationRadioGroup"),
      isAsterisk: true,
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.relation-spouse").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.relation-child").toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.relation-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("common", "common.fields.relationship"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
    );

    genderRadioGroup = PPRadioGroup(
      key: Key("GenderRadioGroup"),
      isAsterisk: true,
      radioOptions: [
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase(),
        LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-label"),
      errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
    );
  }

  getGenderEnglish(String? value) {
    if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-male").toUpperCase()) {
      return "MALE";
    } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-female").toUpperCase()) {
      return "FEMALE";
    } else if (value == LocalizationUtils.getSingleValueString("signup", "signup.fields.gender-other").toUpperCase()) {
      return "OTHER";
    }
  }
}
