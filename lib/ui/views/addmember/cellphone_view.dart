import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/core/viewmodels/add_patient_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

import 'about_patient.dart';

class CellPhoneWidget extends StatefulWidget {
  static const routeName = 'cellphone';
  final AddMemberRequest? request;
  final String? gender;

  CellPhoneWidget({this.request, this.gender});

  @override
  State<StatefulWidget> createState() {
    return CellPhoneState();
  }
}

class CellPhoneState extends BaseState<CellPhoneWidget> {
  static const heading = "Contact Details";
  bool autovalidate = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _cellController = TextEditingController();

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_contact_details);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => AddPatientModel(),
      child: Consumer<AddPatientModel>(builder: (BuildContext context, AddPatientModel model, Widget? child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(AddPatientModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["common", "addmember"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(AddPatientModel model) {
    return BaseScaffold(
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("common", "common.label.add-a-member"),
        appBar: AppBar(),
        backButtonIcon: Icon(
          Icons.home,
          color: whiteColor,
        ),
        leadingBackButton: () {
          Navigator.pop(context);
        },
      ),
      body: Builder(
        builder: (BuildContext context) => SafeArea(
          child: Container(
            child: Builder(
              builder: (BuildContext context) {
                return Column(
                  children: <Widget>[
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                PPUIHelper.verticalSpaceLarge(),
                                // PPTexts.getHeading(LocalizationUtils.getSingleValueString("addmember", "addmember.personal.title")),
                                // PPTexts.getDescription(LocalizationUtils.getSingleValueString("addmember", "addmember.contact.description-other")),
                                pharmacistFlow(),
                                PPUIHelper.verticalSpaceLarge(),
                                PPTexts.getFormLabelAsterisk(LocalizationUtils.getSingleValueString("addmember", "addmember.contact.phone-label"),
                                    fontSize: 15, fontFamily: "FSJoeyPro Bold", color: darkPrimaryColor),
                                SizedBox(
                                  height: 5,
                                ),
                                PPFormFields.getNumericFormField(
                                    key: Key("addMemberPhone"),
                                    autovalidate: autovalidate,
                                    textInputAction: TextInputAction.done,
                                    maxLength: 10,
                                    minLength: 10,
                                    keyboardType: TextInputType.phone,
                                    controller: _cellController,
                                    errorText: LocalizationUtils.getSingleValueString("common", "common.label.this-field-required"),
                                    decoration: PPInputDecor.getDecoration(
                                      labelText: "",
                                      hintText: "",
                                      helperText: '',
                                      prefixIcon: Padding(
                                          padding: EdgeInsets.all(15),
                                          child: Text(
                                            '+1 |',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontFamily: "FSJoeyPro Medium",
                                            ),
                                          )),
                                    ),
                                    onFieldSubmitted: (value) {
                                      onClick(context, model);
                                    }),
                                PPUIHelper.verticalSpaceLarge(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                        child: model.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : PrimaryButton(
                                key: Key("addMemberPhoneClick"),
                                fullWidth: true,
                                text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                                onPressed: () {
                                  onClick(context, model);
                                },
                              ),
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  onClick(context, AddPatientModel model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    setState(() {
      autovalidate = true;
    });
    if (_formKey.currentState!.validate()) {
      widget.request!.patient.phone = int.parse(_cellController.text);
      var success = await model.addPatientFromRequest(request: widget.request);
      if (success != null) {
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        Navigator.pushReplacementNamed(context, AboutPatientWidget.routeName, arguments: TransferArguments(userPatient: success, gender: widget.gender));
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("addmember", "addmember.contact.description"),
            style: TextStyle(color: darkPrimaryColor, fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5, fontFamily: "FSJoeyPro Medium"),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }
}
