import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/res/colors.dart';

class InnerAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String? titleText;
  final AppBar? appBar;
  final void Function()? leadingBackButton;
  final Icon backButtonIcon;

  InnerAppBar({this.titleText, Key? key, this.appBar, this.leadingBackButton, this.backButtonIcon = const Icon(Icons.arrow_back)}) : super(key: key);

  @override
  _InnerAppBarState createState() => _InnerAppBarState();

  @override
  Size get preferredSize => appBar != null ? Size.fromHeight(appBar!.preferredSize.height) : Size.fromHeight(kToolbarHeight);
}

class _InnerAppBarState extends State<InnerAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: brandColor,
      elevation: 1,
      brightness: Platform.isIOS == true ? Brightness.light : null,
      iconTheme: IconThemeData(color: Colors.white60 //change your color here
          ),
      centerTitle: false,
      title: Text(widget.titleText == null ? "" : widget.titleText!,style: TextStyle(fontFamily: "FSJoeyPro Bold"),),
      actions: AppBarIcons.getAppBarIcons(context, aboutUs: false),
      leading: widget.leadingBackButton != null
          ? new IconButton(
        key: Key("back"),
              icon: widget.backButtonIcon,
              onPressed: widget.leadingBackButton,
            )
          : null,
    );
  }
}
