import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_health_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/views/about_us.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/profile/profile_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/custom_dropdown_2.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:provider/provider.dart';

class AppBarIcons {
  static final Analytics analyticsEvents = locator<Analytics>();
  static final DataStoreService dataStore = locator<DataStoreService>();
  static String? languageDropDownValue;

  static getAppBarIcons(BuildContext context, {bool aboutUs = true}) {
    List<Widget> ll = [];
    if (aboutUs == true) {
      ll.add(IconButton(
        key: Key("addMemberButton"),
        icon: new ImageIcon(
          new AssetImage('graphics/icons/add_member.png'),
          size: 28,
        ),
        onPressed: () {
          Navigator.pushNamed(context, AddMemberSignupWidget.routeName);
        },
      ));
      ll.add(Center(child: _getLanguagePreference(context,Colors.black)));
      ll.add(IconButton(
        key: Key("supportButton"),
        icon: new ImageIcon(new AssetImage('graphics/icons/call_chat.png')),
        onPressed: () {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_chat);
          Provider.of<HomeModel>(context, listen: false).showCallAndChatBottomSheet(context);
        },
      ));
      ll.add(
        PopupMenuButton<String>(
          key: Key("menuItemTap"),
          icon: Icon(Icons.more_vert),
          onSelected: (value) async {
            switch (value) {
              case "about":
                locator<NavigationService>()
                    .pushNamed(AboutUsWidget.routeName, viewPopEvent: Provider.of<DashboardModel>(context, listen: false).getCurrentScreenName());
                break;
              case "logout":
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_logout);
                //analyticsEvents.reset();
                PPApplication.quebecCheck = Provider.of<DashboardModel>(context, listen: false).quebecCheck;
                PPApplication.logout = true;
                PPApplication.quebecCheck = false;
                await dataStore.writeBoolean(DataStoreService.QUEBEC, false);
                await Provider.of<ProfileHealthModel>(context, listen: false).clearAsyncMemorizer();
                bool logOutRes = await Provider.of<SignUpModel>(context, listen: false).logout();
                if (logOutRes == true) {
                  try {
                    Provider.of<DashboardModel>(context, listen: false).clearAsyncMemoizer();
                  } catch (e) {
                    print(e.toString());
                  }
                  try {
                    Provider.of<PillReminderDayModel>(context, listen: false).cleanAsyncMemoizer();
                  } catch (e) {
                    print(e.toString());
                  }
                  try {
                    Provider.of<HomeModel>(context, listen: false).clearLogout();
                  } catch (e) {
                    print(e.toString());
                  }
                  try {
                    Provider.of<HomeModel>(context, listen: false).updateDeviceDetails();
                  } catch (e) {
                    print(e.toString());
                  }
                  locator<NavigationService>().pushNamedAndRemoveUntil(SplashView.routeName, carouselIndex: 0);
                }
                break;
              case "profile":
                locator<NavigationService>()
                    .pushNamed(ProfileWidget.routeName, viewPopEvent: Provider.of<DashboardModel>(context, listen: false).getCurrentScreenName());
            }
          },
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem<String>(
                value: "profile",
                child: Text(
                  LocalizationUtils.getSingleValueString("common", "common.navbar.profile"),
                  style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                  key: Key("profileTap"),
                ),
              ),
              PopupMenuItem<String>(
                value: "about",
                child: Text(
                  LocalizationUtils.getSingleValueString("common", "common.navbar.about-us"),
                  style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                  key: Key("aboutTap"),
                ),
              ),
              PopupMenuItem<String>(
                value: "logout",
                child: Text(
                  LocalizationUtils.getSingleValueString("common", "common.navbar.logout"),
                  style: TextStyle(fontFamily: "FSJoeyPro Bold"),
                  key: Key("logoutTap"),
                ),
              )
            ];
          },
          offset: Offset(50, 50),
        ),
      );
    } else {
      ll.add(Center(child: _getLanguagePreference(context,Colors.white)));
      ll.add(Center(
        child: TextButton.icon(
          key: Key("supportAction"),
          icon: new ImageIcon(
            new AssetImage('graphics/icons/call_chat.png'),
            color: whiteOpacity,
          ),
          //`Icon` to display
          label: Text(
            LocalizationUtils.getSingleValueString("common", "common.navbar.help").toUpperCase(),
            style: MEDIUM_XX_WHITE_OPACITY_MEDIUM_BOLD,
          ),
          //`Text` to display
          onPressed: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_call_chat);
            Provider.of<HomeModel>(context, listen: false).showCallAndChatBottomSheet(context);
          },
        ),
      ));
    }
    return ll;
  }

  static Widget _getLanguagePreference(BuildContext context,Color color) {
    return CustomDropdownButtonHideUnderline(
      child: CustomDropdownButton(
        iconEnabledColor: color,
        value: languageDropDownValue == null
            ? getSelectedLanguage() == "en"
                ? "en"
                : "fr"
            : languageDropDownValue,
        onChanged: (String? newValue) {
          changeLanguage(context, newValue.toString());
        },
        selectedItemBuilder: (BuildContext context) {
          return ViewConstants.languageMap
              .map((String key, String value) {
                return MapEntry(
                  key,
                  CustomDropdownMenuItem<String>(
                    value: key,
                    child: Text(
                      key.toUpperCase(),
                      style: TextStyle(fontFamily: "FSJoeyPro", color: color,fontSize: 20),
                    ),
                  ),
                );
              })
              .values
              .toList();
        },
        items: ViewConstants.languageMap
            .map((String key, String value) {
              return MapEntry(
                key,
                CustomDropdownMenuItem<String>(
                  value: key,
                  child: Container(
                    width: 80,
                    child: Text(
                      key.toUpperCase(),
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontFamily: "FSJoeyPro",
                      ),
                    ),
                  ),
                ),
              );
            })
            .values
            .toList(),
      ),
    );
  }


}
