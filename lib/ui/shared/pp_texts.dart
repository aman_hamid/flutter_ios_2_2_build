import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPTexts {
  static getHeading(String text,
      {Color? color,
      bool isDisabled = false,
      Widget? child,
      bool? isChildLeading,
      bool? isChildEnd,
      MainAxisSize mainAxisSize = MainAxisSize.max,
      MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
      bool justText = false,
      TextOverflow? textOverflow,
      TextAlign textAlign = TextAlign.left}) {
    Widget retVal = Text(
      text,
      overflow: textOverflow,
      textAlign: textAlign,
      style: TextStyle(
          color: color != null
              ? color
              : isDisabled == false
                  ? primaryColor
                  : tertiaryColor,
          fontSize: PPUIHelper.FontSizeLarge,
          fontWeight: FontWeight.bold,
          fontFamily: "FSJoeyPro Bold"),
    );

    if (isChildEnd != null && isChildEnd == true) {
      isChildLeading = false;
      mainAxisAlignment = MainAxisAlignment.spaceBetween;
    }
    List<Widget> rowChildren = [];
    if (child != null) {
      if (isChildLeading != null && isChildLeading == true) {
        rowChildren.add(child);
        rowChildren.add(SizedBox(width: PPUIHelper.HorizontalSpaceSmall));
      }
      rowChildren.add(retVal);
      if ((isChildLeading == null || isChildLeading == false)) {
        rowChildren.add(SizedBox(width: PPUIHelper.HorizontalSpaceSmall));
        rowChildren.add(child);
      }
    } else
      rowChildren.add(retVal);
    if (justText == true) return retVal;
    return Row(mainAxisSize: mainAxisSize, mainAxisAlignment: mainAxisAlignment, children: rowChildren);
  }

  static getMainViewHeading(String text, {TextAlign textAlign = TextAlign.left, MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start}) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: mainAxisAlignment,
      children: <Widget>[
        Expanded(
          child: Text(
            text,
            textAlign: textAlign,
            style: TextStyle(color: primaryColor, fontSize: PPUIHelper.FontSizeXXLarge, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
          ),
        ),
      ],
    );
  }

  static getHeadingWithStatus(String heading, String status) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          heading,
          textAlign: TextAlign.left,
          style: TextStyle(color: primaryColor, fontSize: PPUIHelper.FontSizeLarge, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
        ),
        Text(
          status,
          textAlign: TextAlign.right,
          style: TextStyle(
              color: ViewConstants.getColorForPrescriptionStatus(status),
              fontSize: PPUIHelper.FontSizeLarge,
              fontWeight: FontWeight.bold,
              fontFamily: "FSJoeyPro Bold"),
        ),
      ],
    );
  }

  static getStatusHeading(String text) {
    return Row(
      children: <Widget>[
        Text(
          text,
          textAlign: TextAlign.right,
          style: TextStyle(color: secondaryColor, fontSize: PPUIHelper.FontSizeLarge, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
        ),
      ],
    );
  }

  static getStandardTextStyle() {
    return TextStyle(fontSize: PPUIHelper.FontSizeLarge, color: primaryColor, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Bold");
  }

  static getSectionHeader({String? text, Widget? child, bool addPadding = false}) {
    Widget? childWidget;
    if (text != null) {
      childWidget = Text(
        text,
        style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
      );
    } else if (child != null) {
      childWidget = child;
    }
    if (addPadding == false)
      return Container(
        color: headerBgColor,
        child: ListTile(title: childWidget),
      );
    else
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: PPUIHelper.HorizontalSpaceSmall),
        child: Container(
          color: headerBgColor,
          child: ListTile(title: childWidget),
        ),
      );
  }

  static getBottomBarHeading({String? text, Widget? child, bool addPadding = false}) {
    Widget? childWidget;
    if (text != null) {
      childWidget = Text(
        text,
        style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold"),
      );
    } else if (child != null) {
      childWidget = child;
    }
    return Container(
      decoration:
          BoxDecoration(color: Colors.white, boxShadow: <BoxShadow>[BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.11), offset: Offset(0, -3), blurRadius: 2.0)]),
      child: ListTile(title: childWidget),
    );
  }

  static getDescription(String text,
      {bool isBold = false,
      TextAlign textAlign = TextAlign.left,
      Widget? child,
      bool? isChildLeading,
      Color color = secondaryColor,
      bool isExpanded = true,
      bool dummy = false,
      double lineHeight = 1.4,
      MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
      bool justText = false,
      double? fontSize,
      FontWeight? fontWeight}) {
    TextStyle ts = TextStyle(
        height: lineHeight,
        color: color,
        fontSize: fontSize != null ? fontSize : PPUIHelper.FontSizeSmall,
        fontWeight: fontWeight != null ? fontWeight : FontWeight.w500,
        fontFamily: "FSJoeyPro Medium");
    if (isBold == true)
      ts = TextStyle(
          height: lineHeight,
          color: color,
          fontWeight: fontWeight != null ? fontWeight : FontWeight.w500,
          fontSize: fontSize != null ? fontSize : PPUIHelper.FontSizeMedium,
          fontFamily: "FSJoeyPro Bold");
    Widget retVal;
    if (isExpanded == true)
      retVal = Expanded(child: Text(text, textAlign: textAlign, style: ts));
    else
      retVal = Text(text, textAlign: textAlign, style: ts);
    if (dummy == true) return retVal;
    List<Widget> rowChildren = [];
    if (child != null) {
      if (isChildLeading != null && isChildLeading == true) rowChildren.add(child);
      rowChildren.add(retVal);
      if ((isChildLeading == null || isChildLeading == false)) rowChildren.add(child);
    } else
      rowChildren.add(retVal);
    if (justText == true) return retVal;
    return Row(
      mainAxisAlignment: mainAxisAlignment,
      children: rowChildren,
    );
  }

  static getText(String text,
      {bool isBold = false,
      isCentered = false,
      Widget? child,
      bool? isChildLeading,
      Color color = secondaryColor,
      bool isExpanded = true,
      bool dummy = false,
      double lineHeight = 1.4}) {
    TextStyle ts = TextStyle(height: lineHeight, color: color, fontSize: PPUIHelper.FontSizeMedium);
    if (isBold == true)
      ts = TextStyle(height: lineHeight, color: secondaryColor, fontWeight: FontWeight.bold, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold");
    Widget retVal;
    if (isExpanded == true)
      retVal = Expanded(child: Text(text, textAlign: TextAlign.left, style: ts));
    else
      retVal = Text(text, textAlign: TextAlign.left, style: ts);
    if (dummy == true) return retVal;
    List<Widget> rowChildren = [];
    if (child != null) {
      if (isChildLeading != null && isChildLeading == true) rowChildren.add(child);
      rowChildren.add(retVal);
      if ((isChildLeading == null || isChildLeading == false)) rowChildren.add(child);
    } else
      rowChildren.add(retVal);
    return Row(
      children: rowChildren,
    );
  }

  static getFormError(String text, {bool isBold = true, Widget? child, padding = true, bool? isChildLeading, Color? color}) {
    if (color == null) color = errorColor;
    TextStyle ts = TextStyle(height: 1, color: color, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro");
    Widget textChild = Text(text, textAlign: TextAlign.left, style: ts);
    if (padding == true)
      textChild = Padding(padding: const EdgeInsets.fromLTRB(12, 6, 0, 0), child: textChild);
    else
      textChild = Padding(padding: const EdgeInsets.fromLTRB(12, 0, 0, 0), child: textChild);
    Widget retVal = Expanded(child: textChild);
    return Row(
      children: [retVal],
    );
  }

  static getFormLabel(String text, {bool isBold = true, Widget? child, bool? isChildLeading, Color? color}) {
    if (color == null) color = secondaryColor;
    TextStyle ts = TextStyle(height: 1.4, color: color, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Medium");
    if (isBold == true)
      ts = TextStyle(height: 1, color: primaryColor, fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold");
    Widget retVal = Expanded(child: Text(text, textAlign: TextAlign.left, style: ts));
    List<Widget> rowChildren = [];
    if (child != null && isChildLeading != null && isChildLeading == true) rowChildren.add(child);
    rowChildren.add(retVal);
    if (child != null && (isChildLeading == null || isChildLeading == false)) rowChildren.add(child);
    return Row(
      children: rowChildren,
    );
  }

  static getFormLabelAsterisk(String text, {Widget? child, bool? isChildLeading, Color? color,TextStyle? style,FontWeight? fontWeight,String? fontFamily,double? fontSize,double? lineHeight,bool? isAsteriskEnable}) {
    if (color == null) color = secondaryColor;
    if (fontWeight == null ) fontWeight = FontWeight.normal;
    if (fontFamily == null ) fontFamily = "FSJoeyPro Medium";
    if (fontSize == null ) fontSize = 14;
    if (lineHeight == null ) lineHeight = 1;
    if (isAsteriskEnable == null ) isAsteriskEnable = true;
    return RichText(
        text: TextSpan(children: [
          TextSpan(
            text: text,
            style: TextStyle(color: color, fontWeight: fontWeight, fontSize: fontSize, height: lineHeight, fontFamily: fontFamily,),
          ),
          isAsteriskEnable ? TextSpan(
            text: ' *',
            style: TextStyle(color: errorColor, fontWeight: FontWeight.bold, fontSize: fontSize, height: lineHeight, fontFamily: "FSJoeyPro Medium"),
          ): TextSpan(text: ''),
        ]),
      textAlign: TextAlign.start,
    );
  }

  static getSecondaryHeading(String text,
      {bool isBold = true,
      Widget? child,
      TextAlign textAlign = TextAlign.left,
      bool? isChildLeading,
      Color? color,
      MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
      double? fontSize,
      String? fontFamily}) {
    if (color == null) color = secondaryColor;
    if (fontFamily == null) fontFamily = "FSJoeyPro";
    TextStyle ts = TextStyle(
        height: 1.4, color: color, fontSize: fontSize == null ? PPUIHelper.FontSizeMedium : fontSize, fontFamily: fontFamily, fontWeight: FontWeight.normal);
    if (fontSize == null) fontSize = PPUIHelper.FontSizeLarge;
    if (isBold == true) ts = TextStyle(height: 1.4, color: color, fontWeight: FontWeight.bold, fontSize: fontSize, fontFamily: fontFamily);
    Widget retVal = Expanded(child: Text(text, textAlign: textAlign, style: ts));
    List<Widget> rowChildren = [];
    if (child != null && isChildLeading != null && isChildLeading == true) rowChildren.add(child);
    rowChildren.add(retVal);
    if (child != null && (isChildLeading == null || isChildLeading == false)) rowChildren.add(child);
    return Row(
      mainAxisAlignment: mainAxisAlignment,
      children: rowChildren,
    );
  }

  static getTertiaryHeading(
    String text, {
    bool isBold = true,
    TextAlign textAlign = TextAlign.left,
    Widget? child,
    bool? isChildLeading,
    Color? color,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
    bool justText = false,
  }) {
    if (color == null) color = primaryColor;
    TextStyle ts = TextStyle(height: 1.4, color: color, fontSize: PPUIHelper.FontSizeSmall, fontFamily: "FSJoeyPro Bold");
    if (isBold == true)
      ts = TextStyle(height: 1.4, color: color, fontWeight: FontWeight.bold, fontSize: PPUIHelper.FontSizeMedium, fontFamily: "FSJoeyPro Bold");
    Widget retVal = Expanded(child: Text(text, textAlign: textAlign, style: ts));
    List<Widget> rowChildren = [];
    if (child != null && isChildLeading != null && isChildLeading == true) rowChildren.add(child);
    rowChildren.add(retVal);
    if (child != null && (isChildLeading == null || isChildLeading == false)) rowChildren.add(child);
    if (justText == true) return retVal;
    return Row(
      mainAxisAlignment: mainAxisAlignment,
      children: rowChildren,
    );
  }

  static getHyperLink(String text, {Function? onPressed, TextAlign textAlign = TextAlign.left}) {
    return InkWell(
      child: Text(
        text,
        textAlign: textAlign,
        style: TextStyle(color: linkColor, fontSize: 14.0, height: 1.2, fontFamily: "FSJoeyPro"),
      ),
      onTap: () => onPressed!(),
    );
  }
}
