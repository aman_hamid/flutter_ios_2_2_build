import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';

class ChoiceChips extends StatefulWidget {
  final List? chipName;
  final Function(String)? selectedItem;
  final double? fontSize;
  ChoiceChips({Key? key, this.chipName, @required this.selectedItem, this.fontSize = 14}) : super(key: key);

  @override
  _ChoiceChipsState createState() => _ChoiceChipsState();
}

class _ChoiceChipsState extends State<ChoiceChips> {
  String _isSelected = "";

  _buildChoiceList() {
    List<Widget> choices = [];
    widget.chipName!.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item,style: TextStyle(fontFamily: "FSJoeyPro Bold",fontSize: widget.fontSize),),
          labelStyle: TextStyle(color: darkPrimaryColor,fontFamily: "FSJoeyPro Bold"),
          backgroundColor: lightBlueColor,
          selectedColor: headerBgColor,
          selected: _isSelected == item,
          onSelected: (selected) {
            setState(() {
              _isSelected = item;
              widget.selectedItem!(_isSelected);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 5.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}
