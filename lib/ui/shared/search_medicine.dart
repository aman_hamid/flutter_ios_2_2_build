import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';

import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SearchMedicine extends StatefulWidget {
  final Function? onSubmit;

  SearchMedicine({
    Key? key,
    this.onSubmit,
  }) : super(key: key);

  @override
  _SearchMedicineState createState() => _SearchMedicineState();
}

class _SearchMedicineState extends BaseState<SearchMedicine> {
  final TextEditingController _medicineNameController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<Medicine>> key = new GlobalKey();

  List<Medicine> medicinePredictions = [];

  AutoCompleteTextField? searchTextField;
  HomeModel? _homeModel;
  FocusNode? _focusNode;

  bool medicineSubmitted = false;

  String? prevMedicineName;

  void initState() {
    super.initState();
    _focusNode = FocusNode();
    _medicineNameController.addListener(pharmacyControllerListener);

    if (kDebugMode && isIntegration) {
      WidgetsBinding.instance!.addPostFrameCallback((_) => {
            Future.delayed(const Duration(milliseconds: 500), () {
              _focusNode!.requestFocus();
              _medicineNameController.text = "tabs";
              Future.delayed(const Duration(milliseconds: 200), () async {
                var item = (await _homeModel!.getMedicine("602884"))!;
                widget.onSubmit!(item);
                _medicineNameController.text = "";
              });
            })
          });
      print("Im here");
    }
  }

  @override
  void dispose() {
    _medicineNameController.removeListener(pharmacyControllerListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeModel>(builder: (BuildContext context, HomeModel homeModel, Widget? child) {
      _homeModel = homeModel;
      return getAutoCompleteField(context, homeModel);
    });
  }

  pharmacyControllerListener() async {
    if (_medicineNameController.text != "Instance of 'Medicine'" &&
        prevMedicineName != null &&
        prevMedicineName != _medicineNameController.text &&
        _medicineNameController.text.isNotEmpty) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_searchbox);
    await getSearchResponse(_medicineNameController.text);
    prevMedicineName = _medicineNameController.text;
    print(_medicineNameController.text);
  }

  Widget getAutoCompleteField(BuildContext context, HomeModel homeModel) {
    searchTextField = AutoCompleteTextField<Medicine>(
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Bold"),
        controller: _medicineNameController,
        suggestions: medicinePredictions,
        submitOnSuggestionTap: true,
        focusNode: _focusNode,
        onFocusChanged: (focus) {
          if (focus) {
            medicineSubmitted = false;
          }
        },
        keyboardType: TextInputType.visiblePassword,
        key: key,
        itemSorter: (a, b) => 0,
        itemFilter: (suggestion, input) => true,
        itemBuilder: (context, item) {
          return Row(
            children: <Widget>[
              Expanded(
                key: Key(item.id.toString()),
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                    child: getAutoCompleteItem(item),
                  ),
                ),
              ),
            ],
          );
        },
        itemSubmitted: (Medicine item) async {
          item = await homeModel.getMedicine(item.id.toString()) ?? item;
          widget.onSubmit!(item);
        },
        decoration: PPInputDecor.getDecoration(
            prefixIcon: Icon(
              Icons.search,
            ),
            hintText: LocalizationUtils.getSingleValueString("search", "search.all.search-type-hint"),
            labelText: LocalizationUtils.getSingleValueString("search", "search.all.input-search-medication")));

    return searchTextField!;
  }

  getSearchResponse(String keyword) async {
    if (_homeModel != null && keyword.isNotEmpty) {
      medicinePredictions = (await _homeModel!.searchMedicines(keyword))!;

      try {
        searchTextField!.updateSuggestions(medicinePredictions);
        Map<String, dynamic> map = Map();
        map["medication_list_length"] = medicinePredictions != null ? medicinePredictions.length : 0;
        map["search_text"] = keyword;
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_searchbox, map);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }
  }

  Widget getAutoCompleteItem(Medicine item) {
    return Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 4), child: PPTexts.getTertiaryHeading(item.name!.toUpperCase(), isBold: true));
  }
}
