import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/buttons/radio_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PPRadioGroup<T> extends StatefulWidget {
  final _RadioGroupState<T> stateObj = _RadioGroupState();
  final List<T>? radioOptions;
  final String? labelText;
  final String? errorText;
  final String? type;
  final bool? isAsterisk;
  final bool? addBottomSpace;
  final Key? key;
  T? initialValue;
  final Function? onChange;
  final Function? onChangeCallback;
  final String? initialSelectedText;

  PPRadioGroup(
      {@required this.radioOptions,
      this.labelText = "Options",
      this.errorText = "This is a required field",
      this.initialValue,
      this.onChange,
      this.key,
      this.type,
      this.isAsterisk = false,
      this.addBottomSpace = true,
      this.onChangeCallback,
      this.initialSelectedText});

  bool validate() {
    return stateObj.validate();
  }

  T? getValue() {
    return stateObj.value;
  }

  @override
  _RadioGroupState<T> createState() => stateObj;
}

class _RadioGroupState<T> extends State<PPRadioGroup<T>> {
  T? value;
  bool? errorState;

  bool validate() {
    if (value == null || value == '') {
      setState(() {
        errorState = true;
      });
      return false;
    }
    return true;
  }

  @override
  void initState() {
    value = (widget.initialValue == null ? "" : widget.initialValue!) as T?;
    super.initState();
  }

  Widget getRowChild(T radioOption, {String? position}) {
    return value != radioOption
        ? RadioButton(
            key: widget.type == null ? Key(radioOption.toString()) : Key(widget.type.toString() + position.toString()),
            text: radioOption.toString(),
            isSelected: false,
            onPressed: () => setState(() {
              value = radioOption;
              errorState = false;
              widget.onChangeCallback;
              if (widget.onChange != null) widget.onChange!(value);
            }),
          )
        : RadioButton(
            key: widget.type == null ? Key(radioOption.toString()) : Key(widget.type.toString() + position.toString()),
            text: radioOption.toString(),
            isSelected: true,
            onPressed: () {
              setState(() {
                value = radioOption;
                errorState = false;
                widget.onChangeCallback;
                if (widget.onChange != null) widget.onChange!(value);
              });
            });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> rowChildren = [];
    for (int i = 0; i < widget.radioOptions!.length; i++) {
      rowChildren.add(getRowChild(widget.radioOptions![i], position: i.toString()));
      if (i < widget.radioOptions!.length - 1) rowChildren.add(SizedBox(width: PPUIHelper.HorizontalSpaceMedium));
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          widget.isAsterisk! ? PPTexts.getFormLabelAsterisk(widget.labelText!, fontWeight: FontWeight.bold,color: darkPrimaryColor,fontSize: 14.5,fontFamily: "FSJoeyPro Bold") : PPTexts.getFormLabel(widget.labelText!, isBold: true),
          PPUIHelper.verticalSpaceSmall(),
          Row(children: rowChildren),
          errorState == true ? PPTexts.getFormError(widget.errorText!, color: errorColor) : widget.addBottomSpace! ? SizedBox(height: MEDIUM_XXX) : Container()
        ],
      ),
    );
  }
}
