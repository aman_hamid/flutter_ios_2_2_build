import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/formfields/pp_passwordfield.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

import 'dart:math' as math;

import 'package:sms_autofill/sms_autofill.dart';

class PPFormFields {
  static TextFormField getNumericFormField(
      {int maxLength = 2,
      int minLength = 0,
      Key? key,
      bool autovalidate = false,
      bool? centeredText,
      TextInputType? keyboardType,
      TextEditingController? controller,
      String? Function(String?)? validator,
      InputDecoration? decoration,
      @required textInputAction: TextInputAction.done,
      FocusNode? focusNode,
      Function(String)? onTextChanged,
      Function(String)? onFieldSubmitted,
      FloatingLabelBehavior? floatingLabelBehavior,
      String? labelText,
      String? errorText}) {
    if (validator == null) {
      validator = (value) {
        if (value!.isEmpty) {
          if (focusNode == null) {
            return errorText == null ? "Required*" : errorText;
          }
        }
        String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
        if (formattedNumber.length < minLength) return "Atleast $minLength digits are required";
        if (formattedNumber.length > maxLength) return "Atmost $maxLength digits allowed";
        return null;
      };
    }

    if (keyboardType == null) keyboardType = TextInputType.numberWithOptions();
    if (controller == null) controller = TextEditingController();
    if (decoration == null) {
      decoration = PPInputDecor.getDecoration(
          labelText: labelText == null ? 'Cell Phone' : labelText,
          hintText: errorText == null ? "Required*" : errorText,
          helperText: "",
          floatingLabelBehavior: floatingLabelBehavior);
    }
    return TextFormField(
      textAlign: centeredText != null ? TextAlign.center : TextAlign.left,
      autovalidate: autovalidate,
      onChanged: onTextChanged,
      style: TextStyle(fontSize: PPUIHelper.FontSizeLarge, fontWeight: FontWeight.bold, color: primaryColor, fontFamily: "FSJoeyPro Bold"),
      scrollPadding: const EdgeInsets.all(100.0),
      keyboardType: keyboardType,
      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
      maxLength: maxLength,
      buildCounter: (BuildContext context, {int? currentLength, int? maxLength, bool? isFocused}) => null,
      validator: validator,
      decoration: decoration,
      controller: controller,
      focusNode: focusNode,
      textInputAction: textInputAction,
      onFieldSubmitted: onFieldSubmitted,
      key: key == null ? Key("key") : key,
    );
  }

  static TextFormField getNumericFormFieldDecimal(
      {int maxLength = 2,
      int minLength = 0,
      bool autovalidate = false,
      bool? centeredText,
      TextInputType? keyboardType,
      TextEditingController? controller,
      String? Function(String?)? validator,
      InputDecoration? decoration,
      @required textInputAction: TextInputAction.done,
      FocusNode? focusNode,
      Function(String?)? onTextChanged,
      Function(String?)? onFieldSubmitted,
      String? labelText,
      String? errorText,
      Key? key}) {
    if (validator == null) {
      validator = (value) {
        if (value!.isEmpty) return errorText == null ? "Required*" : errorText;

        return null;
      };
    }
    if (keyboardType == null)
      keyboardType = TextInputType.numberWithOptions(
        decimal: true,
        signed: false,
      );
    if (controller == null) controller = TextEditingController();
    if (decoration == null) {
      decoration = PPInputDecor.getDecoration(
        labelText: labelText == null ? 'Cell Phone' : labelText,
        hintText: errorText == null ? "Required*" : errorText,
        helperText: "",
      );
    }
    return TextFormField(
      key: key,
      textAlign: centeredText != null ? TextAlign.center : TextAlign.left,
      autovalidate: autovalidate,
      onChanged: onTextChanged,
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,]+')),],
      style: TextStyle(
        fontSize: PPUIHelper.FontSizeLarge,
        fontWeight: FontWeight.bold,
        fontFamily: "FSJoeyPro Bold",
        color: primaryColor,
      ),
      scrollPadding: const EdgeInsets.all(100.0),
      keyboardType: keyboardType,
      maxLength: maxLength,
      buildCounter: (BuildContext context, {int? currentLength, int? maxLength, bool? isFocused}) => null,
      validator: validator,
      decoration: decoration,
      controller: controller,
      focusNode: focusNode,
      textInputAction: textInputAction,
      onFieldSubmitted: onFieldSubmitted,
    );
  }

  static TextFormField getNumericFormFieldWithInitialValue(
      {int maxLength = 2,
      int minLength = 0,
      bool autovalidate = false,
      bool? centeredText,
      TextInputType? keyboardType,
      TextEditingController? controller,
      String? Function(String?)? validator,
      InputDecoration? decoration,
      FocusNode? focusNode,
      @required String? initialValue}) {
    if (validator == null) {
      validator = (value) {
        if (value!.isEmpty) return "Required*";
        String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
        if (formattedNumber.length < minLength) return "Atleast $minLength digits are required";
        if (formattedNumber.length > maxLength) return "Atmost $maxLength digits allowed";
        return null;
      };
    }
    if (keyboardType == null) keyboardType = TextInputType.numberWithOptions();
    if (controller == null) controller = TextEditingController();
    if (decoration == null) {
      decoration = PPInputDecor.getDecoration(labelText: '', hintText: '', helperText: '');
    }
    return TextFormField(
      textAlign: centeredText != null ? TextAlign.center : TextAlign.left,
      autovalidate: autovalidate,
      style: TextStyle(fontSize: PPUIHelper.FontSizeLarge, fontWeight: FontWeight.bold, color: primaryColor, fontFamily: "FSJoeyPro Bold"),
      scrollPadding: const EdgeInsets.all(100.0),
      keyboardType: keyboardType,
      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
      maxLength: maxLength,
      buildCounter: (BuildContext context, {int? currentLength, int? maxLength, bool? isFocused}) => null,
      validator: validator,
      decoration: decoration,
      focusNode: focusNode,
      initialValue: initialValue,
      controller: controller,
    );
  }

  static TextFormField getTextField(
      {TextEditingController? controller,
      Function()? onEditingComplete,
      Key? key,
      String? initialValue,
      bool autovalidate = false,
      bool enabled = true,
      textInputAction: TextInputAction.done,
      TextCapitalization textCapitalization = TextCapitalization.none,
      TextInputType? keyboardType,
      String? onErrorStr,
      int maxLength = 9999,
      int minLength = 0,
      Function(String?)? onTextChanged,
      String? Function(String?)? validator,
      Function? autofocusValue,
      bool autoFocus = false,
      InputDecoration? decoration,
      Function(String?)? onFieldSubmitted,
      FocusNode? focusNode,
      bool autocorrect = false,
      bool enableSuggestion = false}) {
    if (keyboardType == null) keyboardType = TextInputType.text;
    if (decoration == null) {
      decoration = PPInputDecor.getDecoration(labelText: 'Field', hintText: 'Enter Text');
    }
    if (onErrorStr == null) {
      onErrorStr = "Required*";
    }
    String? Function(String?)? vv = (value) {
      if (value!.isEmpty) return onErrorStr;
      if (value.length < minLength) return "Atleast $minLength characters required";
      if (value.length > maxLength) return "Atmost $maxLength characters allowed";
      return null;
    };

    if (validator != null) vv = validator;
    return TextFormField(
      autocorrect: autocorrect,
      enableSuggestions: enableSuggestion,
      textCapitalization: textCapitalization,
      enabled: enabled,
      textInputAction: textInputAction,
      onChanged: onTextChanged,
      initialValue: initialValue,
      style: TextStyle(fontSize: PPUIHelper.FontSizeLarge, fontWeight: FontWeight.bold, color: primaryColor, fontFamily: "FSJoeyPro Bold"),
      keyboardType: TextInputType.visiblePassword,
      validator: vv,
      autofocus: autoFocus,
      onEditingComplete: onEditingComplete,
      autovalidate: autovalidate,
      decoration: decoration,
      controller: controller,
      onFieldSubmitted: onFieldSubmitted,
      focusNode: focusNode,
      key: key,
    );
  }

  static TextFormField getMultiLineTextField(
      {TextEditingController? controller,
      Function(String?)? onTextChanged,
      bool autovalidate = false,
      String? onErrorStr,
      int maxLength = 9999,
      int minLength = 0,
      InputDecoration? decoration,
      int minLine = 3,
      int maxLine = 3,
      FocusNode? focusNode,
      Key? key,
      bool autoFocus = false}) {
    if (decoration == null) {
      decoration = PPInputDecor.getDecoration(labelText: 'Field', hintText: 'Enter Text');
    }
    if (onErrorStr == null) {
      onErrorStr = "Required*";
    }
    return TextFormField(
      validator: (value) {
        if (value!.isEmpty) return onErrorStr;
        if (value.length < minLength) return "Atleast $minLength characters required";
        if (value.length > maxLength) return "Atmost $maxLength characters allowed";
        return null;
      },
      onChanged: onTextChanged,
      autovalidate: autovalidate,
      decoration: decoration,
      controller: controller,
      focusNode: focusNode,
      autofocus: autoFocus,
      style: TextStyle(fontSize: PPUIHelper.FontSizeMedium, fontWeight: FontWeight.normal, color: primaryColor, fontFamily: "FSJoeyPro Bold"),
      key: key,
      minLines: minLine,
      maxLines: maxLine,
      maxLength: null,
      keyboardType: TextInputType.text,
    );
  }

  static Widget getPasswordFormField(
      {Key? key,
      FocusNode? focusNode,
      bool autovalidate = false,
      TextEditingController? controller,
      int maxLength = 9999,
      int minLength = 0,
      textInputAction: TextInputAction.next,
      String onErrorStr = "Please enter your password",
      String labelText = "Password",
      String hintText = 'Enter your password',
      String helperText = "Please enter 8 or more characters",
      String? Function(String?)? validator,
      void Function(String)? onFieldSubmitted}) {
    if (validator == null)
      validator = (value) {
        if (value!.isEmpty) return onErrorStr;
        if (value.length < minLength) return "Atleast $minLength characters required";
        if (value.length > maxLength) return "Atmost $maxLength characters allowed";
        return null;
      };
    return PPPasswordField(
      focusNode: focusNode,
      autovalidate: autovalidate,
      validator: validator,
      fieldKey: key,
      controller: controller!,
      labelText: labelText,
      hintText: hintText,
      helperText: helperText,
      onFieldSubmitted: onFieldSubmitted,
      inputAction: textInputAction,
    );
  }

  static Widget getRadioButtonGroup({@required List<RadioListTile>? radioListTiles}) {
    Widget child = Column(children: radioListTiles!);
    return child;
  }

  static List<RadioListTile> getRadioButtons({@required List<String>? items, @required Function(String?)? onChanged, @required String? groupValue}) {
    List<RadioListTile> ret = items!
        .map((item) => RadioListTile<String>(
            title: Text(
              item,
            ),
            value: item,
            groupValue: groupValue,
            onChanged: onChanged))
        .toList();
    return ret;
  }

  static Widget getDropDown(
      {required void Function(String?)? onChanged,
      required List<DropdownMenuItem<String>>? items,
      required String? value,
      String? labelText = "Choose a value",
      bool? fullWidth,
      Key? key,
      bool isError = false,
      bool? initialize}) {
    Color color = tertiaryColor;
    if (isError == true) color = errorColor;
    if (initialize == true)
      items!.insert(
          0,
          DropdownMenuItem(
              value: "Select",
              child: Text(
                "Select",
                style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
              )));

    Widget child = Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.5, style: BorderStyle.solid, color: color),
          borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
              style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Bold"),
              hint: Text(labelText!,
                  style: TextStyle(fontSize: PPUIHelper.FontSizeLarge, color: secondaryColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro Medium")),
              value: value,
              onChanged: onChanged,
              items: items),
        ),
      ),
    );
    if (fullWidth == true)
      return Row(
        children: <Widget>[Expanded(child: child)],
      );
    else
      return child;
  }

  static Widget getDropDownTransparentBG(
      {@required void Function(String?)? onChanged,
      required List<DropdownMenuItem<String>>? items,
      required String? value,
      String labelText = "Choose a value",
      bool? fullWidth,
      bool isError = false,
      bool? initialize}) {
    if (initialize == true) items!.insert(0, DropdownMenuItem(value: "Select", child: Text("Select")));

    Widget child = Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
              style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeLarge),
              hint: Text(labelText, style: TextStyle(fontSize: PPUIHelper.FontSizeLarge, color: secondaryColor, fontWeight: FontWeight.normal)),
              value: value,
              onChanged: onChanged,
              items: items),
        ),
      ),
    );
    return child;
  }

  static Widget getDropDownBgDard(
      {@required void Function(String?)? onChanged,
      @required List<DropdownMenuItem<String>>? items,
      @required String? value,
      String labelText = "Choose a value",
      bool? fullWidth,
      bool isError = false,
      bool? initialize}) {
    Color color = whiteColor;
    if (isError == true) color = errorColor;
    if (initialize == true) items!.insert(0, DropdownMenuItem(value: "Select", child: Text("Select")));

    Widget child = Container(
      decoration: ShapeDecoration(
        color: Color(0xFF362191),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SMALL_XX)),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            style: MEDIUM_XX_WHITE_BOLD,
            hint: Text(labelText, style: MEDIUM_XX_WHITE_BOLD),
            value: value,
            onChanged: onChanged,
            items: items,
            iconEnabledColor: whiteColor,
            iconDisabledColor: whiteColor,
          ),
        ),
      ),
    );
    if (fullWidth == true)
      return Row(
        children: <Widget>[Expanded(child: child)],
      );
    else
      return child;
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange}) : assert(decimalRange == null || decimalRange > 0);

  final int? decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") && (value.substring(value.indexOf(".") + 1).length > decimalRange! || value.substring(value.indexOf(".") + 1) != "")) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
        truncated = value.substring(0, value.indexOf("."));
        truncated += ".5";
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}
