import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPInputDecor {
  static InputDecoration getDecoration(
      {String? labelText,
      String? hintText,
      bool collapsed = false,
      String? helperText,
      Widget? suffixIcon,
      filled = false,
      Color? labelColor,
      double? fontSize,
      FloatingLabelBehavior? floatingLabelBehavior,
      EdgeInsets? contentPadding,
      Widget? prefixIcon}) {
    TextStyle errorStyle = TextStyle(height: 0.5, fontFamily: "FSJoeyPro");
    TextStyle helperStyle = TextStyle(height: 0.5, fontSize: 14.0, fontFamily: "FSJoeyPro Medium");
    if (collapsed) {
      errorStyle = TextStyle(height: 0.0, fontFamily: "FSJoeyPro");
      helperStyle = TextStyle(height: 0.0, fontFamily: "FSJoeyPro Bold");
    }
    if (contentPadding == null) contentPadding = EdgeInsets.fromLTRB(12, 15, 12, 15);
    if(floatingLabelBehavior == null) {
      floatingLabelBehavior =  FloatingLabelBehavior.auto;
    }else {
      floatingLabelBehavior =FloatingLabelBehavior.never;
    }
    return InputDecoration(
        contentPadding: contentPadding,
        labelText: labelText,
        alignLabelWithHint: true,
        helperText: helperText,
        helperStyle: helperStyle,
        errorStyle: errorStyle,
        labelStyle: labelColor == null
            ? TextStyle(fontWeight: FontWeight.w500, height: 0.5, fontSize: PPUIHelper.FontSizeLarge, fontFamily: "FSJoeyPro Medium")
            : TextStyle(fontWeight: FontWeight.bold, height: 0.5, fontSize: 18, fontFamily: "FSJoeyPro Medium", color: labelColor),
        hintStyle: TextStyle(fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro Medium"),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(color: brandColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(color: tertiaryColor),
        ),
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
        filled: filled,
        fillColor: Colors.white,
        floatingLabelBehavior: floatingLabelBehavior,
        hintText: hintText);
  }
}
