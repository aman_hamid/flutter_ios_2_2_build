import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/logos.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ChatWidget extends StatefulWidget {
  static const routeName = '/chat';
  static const kAndroidUserAgent = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

  @override
  State<StatefulWidget> createState() {
    return ChatState();
  }
}

class ChatState extends State<ChatWidget> {
  String webviewUrl = locator<AppConfig>().webviewBaseUrl + "/chat";
  final String logoutUrl = 'logout';
  WebViewController? _webViewController;

  final DataStoreService dataStore = locator<DataStoreService>();

  bool cookieSet = false;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    String? authCookie = dataStore.getAuthorizationCookie();
    String? userId = dataStore.getUserId().toString();
    String? patientId = dataStore.getPatientId().toString();
    webviewUrl = webviewUrl + "?cookieAuthorization=$authCookie&userId=$userId&patientId=$patientId";
  }

  void _launchUrl(String _url) async {
    setState(() async {
      await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: WebView(
          initialUrl: webviewUrl,
          userAgent: ChatWidget.kAndroidUserAgent,
          javascriptMode: JavascriptMode.unrestricted,

          onWebViewCreated: (WebViewController webViewController) {
            _webViewController = webViewController;
          },
          onProgress: (int progress) {
            print("WebView is loading (progress : $progress%)");
          },
          navigationDelegate: (NavigationRequest request) {
            if (request.url.contains('tel:') || request.url.contains('mailto:') || request.url.contains('sms:')) {
              _launchUrl(request.url);
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
        ),
      ),
    );
  }
}
