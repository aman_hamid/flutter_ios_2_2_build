import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class HeadingWidget extends StatelessWidget {
  String? text;

  HeadingWidget({this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(child: getTextCard()),
      ],
    );
  }

  Widget getTextCard() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 2,
          ),
        ],
      ),
      child: Text(text!, style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor, fontSize: PPUIHelper.FontSizeXLarge, fontFamily: "FSJoeyPro")),
    );
  }
}
