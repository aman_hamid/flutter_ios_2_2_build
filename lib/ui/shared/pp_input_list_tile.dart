import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPInputListTile {
  static Widget getInputListTile(Widget label, Widget secondary, TextEditingController textEditingController, String key) {
    Widget titleSection = Container(
      padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: PPUIHelper.VerticalSpaceSmall),
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [label, secondary],
            ),
          ),
          Expanded(
            child: PPFormFields.getNumericFormFieldDecimal(
              keyboardType: TextInputType.phone,
              maxLength: 4,
              minLength: 0,
              textInputAction: TextInputAction.done,
              decoration: PPInputDecor.getDecoration(labelText: '', hintText: '', helperText: ''),
              controller: textEditingController,
              key: Key(key),
            ),
          ),
        ],
      ),
    );
    return titleSection;
  }
}
