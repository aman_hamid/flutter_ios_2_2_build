import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class SecondaryButton extends StatelessWidget {
  final String? text;
  final void Function()? onPressed;
  final bool? fullWidth;
  final Key? key;
  final bool? isSmall;
  final bool? isExpanded;
  final Color? textColor;
  final bool? isTextUpperCase;
  final double? fontSizeManual;
  final double borderRadius;

  SecondaryButton(
      {@required this.text,
      this.fullWidth = false,
      @required this.onPressed,
      this.isSmall,
      this.isExpanded = true,
      this.textColor,
      this.isTextUpperCase,
      this.fontSizeManual,
      this.borderRadius = SMALL_XX,
      this.key});

  @override
  Widget build(BuildContext context) {
    double fontSize = fontSizeManual == null
        ? isSmall == null
            ? PPUIHelper.FontSizeLarge
            : PPUIHelper.FontSizeSmall
        : fontSizeManual!;
    double horizontalPadding = isSmall == null ? PPUIHelper.HorizontalSpaceMedium : PPUIHelper.HorizontalSpaceSmall;
    double verticalPadding = isSmall == null ? 0 : 0;
    EdgeInsets? ei;
    if (isSmall != null) ei = EdgeInsets.fromLTRB(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
    Widget child = RaisedButton(
      color: Colors.white,
      disabledTextColor: Colors.white,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: tertiaryColor),
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      padding: ei,
      child: Text(
        isTextUpperCase == false ? this.text! : this.text!.toUpperCase(),
        style: TextStyle(fontSize: fontSize, color: textColor != null ? textColor : secondaryColor, fontFamily: "FSJoeyPro Bold"),
        textAlign: TextAlign.center,
      ),
      onPressed: this.onPressed,
    );
    if (isExpanded == true)
      return Expanded(
        child: child,
      );
    if (!this.fullWidth!)
      return child;
    else
      return Row(
        children: <Widget>[child],
      );
  }
}
