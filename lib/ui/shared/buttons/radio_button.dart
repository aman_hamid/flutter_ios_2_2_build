import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class RadioButton extends StatelessWidget {
  final String text;
  final Key? key;
  final void Function()? onPressed;
  final bool isSmall;
  final bool isExpanded;
  final bool isSelected;
  RadioButton({required this.text, required this.onPressed, this.isSelected = false, this.isSmall = true, this.key, this.isExpanded = true});

  @override
  Widget build(BuildContext context) {
    if (isSelected == true) return selectedBuild(context);
    return unselectedBuild(context);
  }

  Widget unselectedBuild(BuildContext context) {
    double fontSize = isSmall == null ? PPUIHelper.FontSizeLarge : PPUIHelper.FontSizeMedium;
    double horizontalPadding = isSmall == null ? PPUIHelper.HorizontalSpaceMedium : PPUIHelper.HorizontalSpaceSmall;
    double verticalPadding = isSmall == null ? 0 : 0;
    EdgeInsets? ei;
    if (isSmall != null) ei = EdgeInsets.fromLTRB(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
    Widget child = RaisedButton(
      color: Colors.white,
      elevation: 0,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: tertiaryColor),
        borderRadius: BorderRadius.circular(SMALL_X),
      ),
      padding: ei,
      child: Text(
        this.text.toUpperCase(),
        style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold, color: primaryColor, fontFamily: "FSJoeyPro Bold"),
      ),
      onPressed: this.onPressed,
    );

    return child;
  }

  selectedBuild(BuildContext context) {
    double fontSize = isSmall == null ? PPUIHelper.FontSizeLarge : PPUIHelper.FontSizeMedium;
    Widget buttonChild = Text(
      this.text.toUpperCase(),
      style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold, color: whiteColor, fontFamily: "FSJoeyPro Bold"),
    );

    Widget button = RaisedButton(
      color: brandColor,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SMALL_X),
      ),
      child: buttonChild,
      onPressed: this.onPressed,
    );
    return button;
  }
}
