import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final Function()? onPressed;
  final bool fullWidth;
  final bool? isSmall;
  final IconData? iconData;
  final bool? isExpanded;
  final Color? butttonColor;
  final Color? textColor;
  final bool? isAllCaps;
  final bool? isLeft;
  bool disabled = false;
  final Key? key;
  double? borderRadius;

  PrimaryButton(
      {required this.text,
      required this.onPressed,
      this.fullWidth = false,
      this.iconData = null,
      this.butttonColor = brandColor,
      this.textColor = whiteColor,
      this.isExpanded = true,
      this.disabled = false,
      this.isSmall,
      this.isAllCaps = true,
      this.isLeft = false,
      this.borderRadius = SMALL_XX,
      this.key});

  @override
  Widget build(BuildContext context) {
    double fontSize = isSmall == null ? PPUIHelper.FontSizeLarge : PPUIHelper.FontSizeMedium;
    String textValue = isAllCaps == true ? this.text.toUpperCase() : this.text;
    Widget buttonChild = Text(
      textValue,
      style: TextStyle(fontSize: fontSize, color: textColor, fontFamily: "FSJoeyPro Bold"),
      textAlign: TextAlign.center,
    );
    if (this.iconData != null)
      buttonChild = isLeft == false ?
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            this.text.toUpperCase(),
            style: TextStyle(fontSize: fontSize, color: textColor, fontFamily: "FSJoeyPro Bold"),
            textAlign: TextAlign.center,
          ),
          SizedBox(width: 8.0),
          new Icon(this.iconData, size: 24, color: textColor),
        ],
      ) : Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(this.iconData, size: 24, color: textColor),
          SizedBox(width: 8.0),
          Text(
            this.text.toUpperCase(),
            style: TextStyle(fontSize: fontSize, color: textColor, fontFamily: "FSJoeyPro Bold"),
            textAlign: TextAlign.center,
          ),

        ],
      );

    Widget button = RaisedButton(
      color: !disabled ? butttonColor : secondaryColor,
      disabledColor: secondaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius!),
      ),
      child: buttonChild,
      onPressed: this.onPressed,
    );
    if (isExpanded!) button = Expanded(child: button);
    if (!this.fullWidth)
      return button;
    else
      return Container(
        color: Colors.white,
        child: Row(
          children: <Widget>[button],
        ),
      );
  }
}
