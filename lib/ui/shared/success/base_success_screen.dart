import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/signup/transaction_successful_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/success/transaction_successful_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/success/base_success_views.dart';
import 'package:pocketpills/ui/shared/success/success_tick_card_view.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class BaseSuccessScreen extends StatefulWidget {
  static const String routeName = 'baseSuccessScreen';

  BaseStepperSource? source;
  TransactionSuccessEnum transactionSuccessEnum;

  BaseSuccessScreen({this.source = BaseStepperSource.UNKNOWN_SCREEN, required this.transactionSuccessEnum});

  @override
  _BaseSuccessScreenState createState() => _BaseSuccessScreenState();
}

class _BaseSuccessScreenState extends State<BaseSuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
          ChangeNotifierProvider<TransactionSuccessfulModel>(create: (_) => TransactionSuccessfulModel())
        ],
        child: Consumer2<TransactionSuccessfulModel, UserContactModel>(
          builder: (BuildContext context, TransactionSuccessfulModel transactionSuccessfulModel, UserContactModel userContactModel, Widget? child) {
            return WillPopScope(
              onWillPop: () {
                Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
                Provider.of<HomeModel>(context, listen: false).clearData();
                Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                return true as Future<bool>;
              },
              child: Scaffold(
                      appBar: widget.source == BaseStepperSource.NEW_USER
                          ? null
                          : InnerAppBar(
                              titleText: widget.source == BaseStepperSource.COPAY_REQUEST
                                  ? "Copay Request"
                                  : PatientUtils.getPatientName(Provider.of<DashboardModel>(context, listen: false).selectedPatient),
                              appBar: AppBar(),
                              backButtonIcon: Icon(
                                Icons.home,
                                color: whiteColor,
                              ),
                              leadingBackButton: () {
                                Provider.of<DashboardModel>(context, listen: false).dashboardIndex = 0;
                                Provider.of<HomeModel>(context, listen: false).clearData();
                                Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                              },
                            ),
                      body: FutureBuilder(
                        future: myFutureMethodOverall(transactionSuccessfulModel),
                        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                          if (snapshot.hasData && snapshot.data != null) {
                            if (widget.source == BaseStepperSource.ADD_PATIENT) {
                              Future.delayed(Duration(seconds: 20), () {
                                Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
                              });
                            }
                            return _afterFutureResolved(context, snapshot.data[0], userContactModel);
                          } else if (snapshot.hasError) {
                            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                            return ErrorScreen();
                          } else {
                            return LoadingScreen();
                          }
                        },
                      )),
            );
          },
        ));
  }

  Future myFutureMethodOverall(TransactionSuccessfulModel model) async {
    Future<TransactionSuccessfulResponse?> future1 =
        model.getTransactionSuccessData(widget.transactionSuccessEnum, memberPatientId: Provider.of<DashboardModel>(context, listen: false).memberPatientId);
    Future<Map<String, dynamic>?> future2 = model.getLocalization(["signup", "common", "dashboard"]);
    return await Future.wait([future1, future2]);
  }

  Widget _afterFutureResolved(BuildContext context, TransactionSuccessfulResponse successfulResponse, UserContactModel userContactModel) {
    final DataStoreService dataStore = locator<DataStoreService>();
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SuccessTickCardView(successfulResponse.successDetails!),
          SizedBox(
            height: REGULAR_XXX,
          ),
          BaseSuccessViews(successfulResponse.successDetails!.fields!, userContactModel, widget.source),
        ],
      ),
    );
  }

  String getAddressNoCountry(String str) {
    if (str != null && str.length >= 8) {
      str = str.substring(0, str.length - 8);
    }
    return str;
  }

  void onClickPharmacySearchScreen(BuildContext context, BaseStepperSource? source) {
    final DataStoreService dataStore = locator<DataStoreService>();
    Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
        arguments: TransferArguments(source: source, PharmacyNamePopular: dataStore.getPharmacyName()!));
  }


}
