import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';

class FreeVitaminsCardView extends BaseStatelessWidget {
  final UserContactModel userContactModel;
  final TransactionSuccessField field;

  FreeVitaminsCardView(this.field, this.userContactModel);

  @override
  Widget build(BuildContext context) {
    return PPCard(
        margin: EdgeInsets.all(MEDIUM_XXX),
        padding: const EdgeInsets.only(bottom: SMALL_XXX),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: MEDIUM_XXX, bottom: SMALL, top: MEDIUM_X),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            field.title!,
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          ),
                          SizedBox(
                            height: SMALL,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: SMALL_XXX),
                            child: RichText(
                              text: TextSpan(
                                text: field.description,
                                style: MEDIUM_XX_SECONDARY,
                                children: <TextSpan>[
                                  TextSpan(
                                    text: "",
                                    style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                  Expanded(
                      flex: 1,
                      child: Image.asset(
                        'graphics/better-care.png',
                      ))
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  color: Colors.white,
                  child: Text(
                    "Get Free Vitamins",
                    style: MEDIUM_XXX_PINK_BOLD,
                  ),
                  onPressed: () {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins);
                    onClickVitaminCard(context, userContactModel);
                  },
                ),
                SizedBox(
                  width: SMALL_XX,
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  color: pinkColor,
                  size: REGULAR,
                )
              ],
            ),
          ],
        ),
        onTap: () {
          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins);
          onClickVitaminCard(context, userContactModel);
        });
  }

  void onClickVitaminCard(BuildContext context, UserContactModel userContactModel) async {
    await userContactModel.putContact();
    Navigator.of(context).pushNamedAndRemoveUntil(VitaminsWidget.routeName, (Route<dynamic> route) => false, arguments: SourceArguments(source: BaseStepperSource.NEW_USER));
  }
}
