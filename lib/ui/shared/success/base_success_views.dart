import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/success/free_vitamin_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_chat_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_deliveryby_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_prefer_contact_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_prefrence_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_text_card_view.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import '../../../locator.dart';

class BaseSuccessViews extends StatelessWidget {
  final List<TransactionSuccessField> fields;
  final UserContactModel userContactModel;
  final BaseStepperSource? source;
  final DataStoreService dataStore = locator<DataStoreService>();

  BaseSuccessViews(this.fields, this.userContactModel, this.source);

  @override
  Widget build(BuildContext context) {
    List<Widget> viewList = [];
    for (int i = 0; i < fields.length; i++) {
      TransactionSuccessField field = fields[i];
      switch (field.getCardType()) {
        case TransactionSuccessCardType.TEXT:
          viewList.add(SuccessTextCardView(field));
          break;
        case TransactionSuccessCardType.CARE_PHARMACIST:
          viewList.add(SuccessChatCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.PRESCRIPTION_PREFERENCE:
          userContactModel.preferenceData = true;
          viewList.add(SuccessPrefrenceCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.WEBAPP_DELIVERY_BY:
          userContactModel.deliveryByData = true;
          print("Here Quebeck");
          /* source == BaseStepperSource.MAIN_SCREEN && LocalizationUtils.isProvinceQuebec() ?  viewList.add(Container(
            margin: const EdgeInsets.only(left: 26.0, right: 16.0),
            decoration: BoxDecoration(color: lightGray, borderRadius: BorderRadius.circular(4.0)),
            child: Padding(
              padding: EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0, bottom: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start, //change here don't //worked
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 210,
                        child: Text(
                          dataStore.getPharmacyName()!,
                          key: Key("pharmacyNameSuccess"),
                          style: TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Heavy"),
                        ),
                      ),
                      Container(
                        width: 210,
                        child: Text(
                          getAddressNoCountry(dataStore.getPharmacyAddress()!),
                          style: TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium"),
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        //onClickPharmacySearchScreen(context,source);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          LocalizationUtils.getSingleValueString("signup", "signup.otp.phone-change"),
                          style: TextStyle(color: brandColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Heavy"),
                        ),
                      ))
                ],
              ),
            ),
          ),):Container();*/
          viewList.add(SuccessDeliveryByCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.PREFERRED_TIMINGS:
          viewList.add(SuccessPreferContactCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.VITAMINS_INFO:
          viewList.add(FreeVitaminsCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.BUTTON:
          viewList.add(
            Padding(
              padding: const EdgeInsets.only(top: MEDIUM_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  PrimaryButton(
                    key: Key("successHome"),
                    isExpanded: true,
                    text: field.title!,
                    onPressed: () {
                      userContactModel.handleSuccessViewRoute(field.getActionType()!, context);
                    },
                  )
                ],
              ),
            ),
          );
          break;
      }
    }
    viewList.add(SizedBox(
      height: LARGE_XX,
    ));

    return Column(
      children: viewList,
    );
  }

  String getAddressNoCountry(String str) {
    if (str != null && str.length >= 8) {
      str = str.substring(0, str.length - 8);
    }
    return str;
  }

  void onClickPharmacySearchScreen(BuildContext context, BaseStepperSource? source) {
    Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
        arguments: TransferArguments(source: source, PharmacyNamePopular: dataStore.getPharmacyName()!));
  }

  Widget getButtonView(TransactionSuccessField field, BuildContext context) {
    if (source != BaseStepperSource.ADD_PATIENT) {
      return Padding(
        padding: const EdgeInsets.only(top: LARGE_XX, left: MEDIUM_XXX, right: MEDIUM_XXX),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            PrimaryButton(
              key: Key("successHome"),
              isExpanded: true,
              text: field.title!,
              onPressed: () {
                userContactModel.handleSuccessViewRoute(field.getActionType()!, context);
              },
            )
          ],
        ),
      );
    } else if (source == BaseStepperSource.ADD_PATIENT && LocalizationUtils.isProvinceQuebec()) {
      return Padding(
        padding: const EdgeInsets.only(top: LARGE_XX, left: MEDIUM_XXX, right: MEDIUM_XXX),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            PrimaryButton(
              key: Key("successHome"),
              isExpanded: true,
              text: field.title!,
              onPressed: () {
                userContactModel.handleSuccessViewRoute(TransactionSuccessActionType.DASHBOARD, context);
              },
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
