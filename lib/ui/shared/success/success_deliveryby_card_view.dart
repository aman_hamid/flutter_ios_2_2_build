import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import '../../../locator.dart';

class SuccessDeliveryByCardView extends BaseStatelessWidget {
  final UserContactModel userContactModel;
  final TransactionSuccessField field;
  final DataStoreService dataStore = locator<DataStoreService>();

  SuccessDeliveryByCardView(this.field, this.userContactModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: MEDIUM_X),
      child: Container(
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                color: bghighlight2,
                width: MEDIUM,
              ),
              SizedBox(
                width: MEDIUM_X,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                    Text(
                      field.title!,
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                    ),
/*                    SizedBox(
                      height: SMALL,
                    ),*/
/*                    Padding(
                      padding: const EdgeInsets.only(right: MEDIUM_XXX),
                      child: Text(
                        field.description ?? "Empty",
                        style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                      ),
                    ),*/
                    SizedBox(
                      height: MEDIUM_X,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 36,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(width: 0.5, style: BorderStyle.solid, color: tertiaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            key: Key("eddDropDown"),
                            value: userContactModel.eddList[userContactModel.index].value,
                            onChanged: (String? newValue) async {
                              userContactModel.setDeliveryTime(newValue!);
                            },
                            items: userContactModel.eddList
                                .map((state) => DropdownMenuItem<String>(
                                    value: state.value,
                                    child: Text(
                                      state.label,
                                      style: TextStyle(fontFamily: "FSJoeyPro Heavy"),
                                      key: Key(state.value),
                                    )))
                                .toList(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String getAddressNoCountry(String str) {
    if (str != null && str.length >= 8) {
      str = str.substring(0, str.length - 8);
    }
    return str;
  }
}
