import 'package:flutter/material.dart';

import '../../res/colors.dart';

class PPhorizontalline extends CustomPainter {
  Paint? _paint;
  bool? reverse;
  double? d;

  PPhorizontalline(this.reverse, double d) {
    this.d = d;
    _paint = Paint()
      ..color = tertiaryColor
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (reverse!) {
      canvas.drawLine(Offset(-d!, 0.0), Offset(-10.0, 0.0), _paint!);
    } else {
      canvas.drawLine(Offset(10.0, 0.0), Offset(d!, 0.0), _paint!);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
