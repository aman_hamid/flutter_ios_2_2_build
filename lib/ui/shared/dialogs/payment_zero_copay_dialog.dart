import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_payment_model.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class PaymentZeroCopayDialog extends BaseStatelessWidget {
  final addressId;
  final Function(String)? showSnackBar;
  final ProfilePaymentModel? profilePaymentModel;
  final Function? onSuccess;

  PaymentZeroCopayDialog({Key? key, this.addressId, this.showSnackBar, this.profilePaymentModel, this.onSuccess}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Container(
          child: Row(
        children: <Widget>[
          Text(LocalizationUtils.getSingleValueString("modal", "modal.confirm.title"),style: TextStyle(fontFamily: "FSJoeyPro Medium"),),
        ],
      )),
      content: Text(getDescriptionText(PatientUtils.getForGender(getUserPatientTuple(context))),style: TextStyle(fontFamily: "FSJoeyPro Medium")),
      actions: <Widget>[
        // usually buttons at the bottom of the dialog
        FlatButton(
          child: Text(LocalizationUtils.getSingleValueString("modal", "modal.all.cancel"),style: TextStyle(fontFamily: "FSJoeyPro Bold")),
          onPressed: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_zero_copay_cancel);
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          key: Key("dialogContinue"),
          child: Text(LocalizationUtils.getSingleValueString("modal", "modal.all.continue"),style: TextStyle(fontFamily: "FSJoeyPro Bold")),
          onPressed: () async {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_zero_copay_continue);
            bool? result = await profilePaymentModel?.zeroCopayOption();
            if (result == true) {
              Navigator.of(context).pop();
              this.onSuccess!();
            }
          },
        ),
      ],
    );
  }

  UserPatient getUserPatientTuple(BuildContext context) {
    UserPatient up = Provider.of<DashboardModel>(context).selectedPatient!;
    return up;
  }

  static Function? show(ProfilePaymentModel profilePaymentModel, BuildContext context, Function onSuccess, {Function(String)? showSnackBar}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return PaymentZeroCopayDialog(
          showSnackBar: showSnackBar,
          profilePaymentModel: profilePaymentModel,
          onSuccess: onSuccess,
        );
      },
    );
  }
}

String getDescriptionText(String gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString("modal", "modal.payment.copay-title-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString("modal", "modal.payment.copay-title-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString("modal", "modal.payment.copay-title-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString("modal", "modal.payment.copay-title");
      break;
  }
}
