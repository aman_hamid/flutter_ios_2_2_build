import 'dart:io';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class PermissionDialog extends StatelessWidget {
  String? accessPermission;

  PermissionDialog(this.accessPermission);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SignUpAboutYouModel(),
      child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget? child) {
        return FutureBuilder(
            future: model.getLocalization(["common"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                String name = getPermissionName(accessPermission);
                return getMainView(model, context, name);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  static Function? show(BuildContext context, String access) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return PermissionDialog(access);
      },
    );
  }

  Widget getMainView(SignUpAboutYouModel model, BuildContext context, String name) {
    return AlertDialog(
      title: Text(
          LocalizationUtils.getSingleValueString("common", "common.permission.title").isEmpty
              ? "Need Permission for {{name}}".replaceAll("{{name}}", name)
              : LocalizationUtils.getSingleValueString("common", "common.permission.title").replaceAll("{{name}}", name),
          style: TextStyle(fontFamily: "FSJoeyPro Bold")),
      titlePadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 5),
      content: Text(
          LocalizationUtils.getSingleValueString("common", "common.permission.description").isEmpty
              ? "This app needs permission to use this feature. You can grant them in app settings."
              : LocalizationUtils.getSingleValueString("common", "common.permission.description"),
          style: TextStyle(fontFamily: "FSJoeyPro Medium")),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: Text(
              LocalizationUtils.getSingleValueString("common", "common.permission.cancel").isEmpty
                  ? "Cancel"
                  : LocalizationUtils.getSingleValueString("common", "common.permission.cancel"),
              style: TextStyle(fontFamily: "FSJoeyPro Bold")),
        ),
        FlatButton(
          onPressed: () async {
            Navigator.of(context).pop(false);
            await openAppSettings();
          },
          child: Text(
              LocalizationUtils.getSingleValueString("common", "common.permission.open_settings").isEmpty
                  ? "Open Settings"
                  : LocalizationUtils.getSingleValueString("common", "common.permission.open_settings"),
              style: TextStyle(fontFamily: "FSJoeyPro Bold")),
        ),
      ],
    );
  }

  String getPermissionName(String? accessPermission) {
    if (accessPermission == "contacts") {
      return LocalizationUtils.getSingleValueString("common", "common.permission.contacts").isEmpty
          ? "Contacts"
          : LocalizationUtils.getSingleValueString("common", "common.permission.contacts");
    } else if (accessPermission == "camera") {
      return LocalizationUtils.getSingleValueString("common", "common.permission.camera").isEmpty
          ? "Camera"
          : LocalizationUtils.getSingleValueString("common", "common.permission.camera");
    } else if (accessPermission == "photos") {
      return LocalizationUtils.getSingleValueString("common", "common.permission.photo").isEmpty
          ? "Photos"
          : LocalizationUtils.getSingleValueString("common", "common.permission.photos");
    } else {
      return LocalizationUtils.getSingleValueString("common", "common.permission.storage").isEmpty
          ? "Storage"
          : LocalizationUtils.getSingleValueString("common", "common.permission.storage");
    }
  }
}
