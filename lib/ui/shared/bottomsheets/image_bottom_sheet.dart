import 'package:flutter/material.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ImageBottomSheet {
  static void getBottomSheet(BuildContext context, {void Function()? onCameraClick, void Function()? onGalleryClick}) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: PPTexts.getHeading(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-an-option")),
            ),
            PPDivider(),
            ListTile(
              key: Key("openCamera"),
              leading: new Icon(Icons.photo_camera),
              title: new Text(
                LocalizationUtils.getSingleValueString("insurance", "insurance.labels.take-photo"),
                style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
              ),
              onTap: onCameraClick,
            ),
            ListTile(
                key: Key("openGallery"),
                leading: new Icon(Icons.photo_library),
                title: new Text(LocalizationUtils.getSingleValueString("insurance", "insurance.labels.choose-gallery"), style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM),
                onTap: onGalleryClick),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }
}
