import 'dart:convert';
import 'dart:io';
import 'package:advertising_id/advertising_id.dart';
import 'package:async/async.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_branch_sdk/flutter_branch_sdk.dart';
import 'package:google_maps_webservice/src/core.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/transfer/ip_location_response.dart';
import 'package:pocketpills/core/services/firebase_messaging.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/csvmapper/event_mapper.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'dart:developer' as developer;

import 'package:pocketpills/utils/navigation_service.dart';

class PPApplication {
  static final oneSignalToken = locator<AppConfig>().oneSignalToken;
  static final Analytics analytics = locator<Analytics>();
  static final DataStoreService dataStore = locator<DataStoreService>();
  static final AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  static final HttpApiUtils httpApiUtils = HttpApiUtils();
  static List<TelehealthProvinceItem>? provinceArrayListMemo;
  static bool quebecCheck = false;
  static Map<String, Events> eventMap = Map();
  static String? ip;
  static String? value;
  static String? province;
  static int? prescriptionID = -1;
  static String? city;
  static bool mapInitialised = false;
  static bool logout = false;

  static void initApplication() async {
    //await initOneSignal();
    await readFileAsync('res/Instrumentation.csv');

    await _initFCM();
    await _initCrashlytics();
    await getAdvertisingId();
    await getProvinceName();
    await getProvinceList();
    try {
      setUpBranch();
    } catch (error) {
      developer.log('Branch Init failed', name: error.toString());
      FirebaseCrashlytics.instance.log(error.toString());
    }
  }

  static Future<dynamic> readFileAsync(String filePath) async {
    String xmlString = await rootBundle.loadString(filePath);
    List<String> newEvents = xmlString.split('\n');
    for (String row in newEvents) {
      Events e;
      List<String> event = row.split(',');
      if (event[1] == null || event[1].isEmpty == true || event[1].contains('Not req')) {
        e = Events(old_name: event[0], new_name: 'N/A', group: event[2], action: event[3]);
      } else {
        e = Events(old_name: event[0], new_name: event[1], group: event[2], action: event[3]);
      }
      eventMap[event[0]] = e;
    }
    return true;
  }

  static void setUpBranch() async {
    FlutterBranchSdk.initSession().listen((string) {
      developer.log('DEEPLINK $string', name: string.toString());
      //Map<dynamic, dynamic>? valueMap = json.decode(string);
      if (string != null) {
        string.forEach((k, v) => {
              if (k == "referralCode") {dataStore.writeString(DataStoreService.REFERRAL_CODE, v)},
              if (k == "invitationCode") {dataStore.writeString(DataStoreService.INVITATION_CODE, v)},
              if (k == "~campaign") {dataStore.writeString(DataStoreService.UTM_CAMPAIGN, v)},
              if (k == "~channel") {dataStore.writeString(DataStoreService.UTM_SOURCE, v)},
              if (k == "~feature") {dataStore.writeString(DataStoreService.UTM_MEDIUM, v)},
              if (k == "utm_content") {dataStore.writeString(DataStoreService.UTM_CONTENT, v)},
              if (k == "utm_term") {dataStore.writeString(DataStoreService.UTM_TERM, v)},
              if (k == "utm_medium") {dataStore.writeString(DataStoreService.UTM_MEDIUM, v)},
              if (k == "utm_campaign") {dataStore.writeString(DataStoreService.UTM_CAMPAIGN, v)},
              if (k == "\$deeplink_path") {showRouteScreen(v)},
              if (k == "\$custom_meta_tags") {showDeepLinkCarouselPage(v)},
              if (k == "+clicked_branch_link" && v == true)
                {
                  if (dataStore.readString(DataStoreService.PP_DISTINCT_ID) == null || dataStore.readString(DataStoreService.PP_DISTINCT_ID) == "")
                    {HttpApiUtils().sendAdvertiseParams()}
                  else
                    {HttpApiUtils().updateAdvertiseParams()}
                },
            });
      }
    });
  }

  static Future<void> getProvinceName() async {
    return _geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          String? subdivisionName = await httpApiUtils.getSubdivisionName();
          quebecCheck = subdivisionName!.toLowerCase().contains(ViewConstants.QUEBEC);
        } catch (ex) {
          FirebaseCrashlytics.instance.log(ex.toString());
        }
      }
    });
  }

  static Future<void> getProvinceList() async {
    provinceArrayListMemo = await httpApiUtils.getProvinceList();
  }

  static void showRouteScreen(String route) {
    locator<NavigationService>().pushNamedAndRemoveUntil(SplashView.routeName, deepLinkRouteName: route, carouselIndex: 0);
  }

  static void showDeepLinkCarouselPage(String index) {
    int carouselIndexPosition = 0;
    if (index != null) {
      try {
        carouselIndexPosition = int.parse(index);
      } catch (ex) {
        FirebaseCrashlytics.instance.log(ex.toString());
      }
    }

    locator<NavigationService>().pushNamedAndRemoveUntil(SplashView.routeName, carouselIndex: carouselIndexPosition);
  }

  static Future<void> _initCrashlytics() async {
    FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    setCrashLyticsIdentifier();
  }

  static Future<void> _initFCM() async {
    var firebase = FirebaseMessagingService();
  }

  static void setCrashLyticsIdentifier() {
    if (dataStore.getUserId() != null) {
      FirebaseCrashlytics.instance.setUserIdentifier(dataStore.getUserId().toString());
    }
  }

  static Future<bool> setupRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    if (!kReleaseMode) {
      remoteConfig.setConfigSettings(RemoteConfigSettings(fetchTimeout: Duration(seconds: 60), minimumFetchInterval: Duration(seconds: 60)));
    }
    try {
      final defaults = <String, dynamic>{
        'getstarted_without_phone_number': "0",
      };

      await remoteConfig.setDefaults(defaults);
      await remoteConfig.fetch();
      await remoteConfig.fetchAndActivate();
      Map<String, String> map = new Map();
      map["e11"] = remoteConfig.getString('getstarted_without_phone_number');
      dataStore.writeString(DataStoreService.GET_STARTED_WITHOUT_PHONE_NUMBER, remoteConfig.getString('getstarted_without_phone_number'));
      String distinctId = await analytics.getMixpanelDistinctId();
      await analytics.mixPanelIdentifier();
      await analytics.sendAnalyticsEvent("abtest", map);
      await analytics.setPeopleProperties(map);
    } catch (e) {
      FirebaseCrashlytics.instance.log(e.toString());
    }

    return true;
  }

  static Future<void> getAdvertisingId() async {
    String? advertisingId = await AdvertisingId.id();
    if (advertisingId != null && advertisingId != "") {
      if (Platform.isIOS) {
        dataStore.writeString(DataStoreService.IDFA_ID, advertisingId);
      } else {
        dataStore.writeString(DataStoreService.ADVERTISING_ID, advertisingId);
      }
    }
    await sendAdvertiseParams();
  }

  static sendAdvertiseParams() {
    final DataStoreService dataStore = locator<DataStoreService>();
    if (dataStore.readString(DataStoreService.PP_DISTINCT_ID) == null || dataStore.readString(DataStoreService.PP_DISTINCT_ID) == "") {
      HttpApiUtils().sendAdvertiseParams();
    }
  }

/*static Future<void> initOneSignal() async {
    try {
      OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
    } catch (ex) {
      FirebaseCrashlytics.instance.log(ex.toString());
    }

    OneSignal.shared.setRequiresUserPrivacyConsent(true);

    OneSignal.shared.consentGranted(true);

    OneSignal.shared.promptUserForPushNotificationPermission();

    OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);

    var settings = {OSiOSSettings.autoPrompt: false, OSiOSSettings.promptBeforeOpeningPushUrl: true};

    await OneSignal.shared.setAppId(oneSignalToken);

    var status = await OneSignal.shared.getPermissionSubscriptionState();
    await OneSignal.shared.setExternalUserId(dataStore.getUserId() != null ? dataStore.getUserId().toString() : "");

    String playerId = status.subscriptionStatus.userId;
    String pushToken = status.subscriptionStatus.pushToken;
    Map<String, String> map = new Map();
    map["\$oneSignalPlayerId"] = playerId;
    map["\$oneSignalPushToken"] = pushToken;
    dataStore.writeString(DataStoreService.ONESIGNAL_TOKEN, pushToken);
    dataStore.writeString(DataStoreService.ONESIGNAL_PLAYERID, playerId);
    await analytics.setPeopleProperties(map);

    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      analytics.sendAnalyticsEvent(AnalyticsEventConstant.notification_received);
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      analytics.sendAnalyticsEvent(AnalyticsEventConstant.notification_open);
    });

    OneSignal.shared.setInAppMessageClickedHandler((OSInAppMessageAction action) {
      analytics.sendAnalyticsEvent(AnalyticsEventConstant.inapp_message_clicked);
    });

    OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  }*/
}
